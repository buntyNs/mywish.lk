<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Lang;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class LanguageSettingController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }

    public function add_language()
    {
         if (Session::has('userid')) 
         {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_SETTINGS');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS');
            }

            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menus');
            $adminfooter      = view('siteadmin.includes.admin_footer');
            

            return view('siteadmin.add_language')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_language_submit()
    {
        if(Session::has('userid'))
        {
        $data = Input::except(array('_token')) ;
        $rule = array(
        'lang_name' => 'required',
        'lang_code' => 'required'
        );
            $validator = Validator::make($data,$rule);          
            if ($validator->fails())
            {
                return Redirect::to('add_language')->withErrors($validator->messages())->withInput();
            }
            else
            {
                $data =array( 
                    'lang_name' => Input::get('lang_name'),
                    'lang_code' => Input::get('lang_code'),
                    'lang_status'=>2,
                    'lang_default'=>0,
                    'pack_lang'=>0,
                 );
                $update = DB::table('nm_language')->insert($data);
                if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY')!= '') 
                { 
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY');
                }  
                else 
                { 
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_INSERTED_SUCCESSFULLY');
                }
              /*  $insert_result = (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY') : trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_INSERTED_SUCCESSFULLY');*/
                  
                    return Redirect::to('manage_language')->with('insert_result',$session_message);
               
            }
        
        }
    }

    public function check_langName_exist()
    {
        $name = Input::get('lang_name');
        $lang_id = Input::get('lang_id');
        $check_name = Settings::check_lang_name_exist($name,$lang_id);

        if(count($check_name) == 0)
        {
            echo '1';//not exist
        }
        else
        {
            echo '2';//already exist
        }


    }

    public function manage_language()
    {

         if (Session::has('userid')) 
         {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_SETTINGS');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS');
            }

            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menus');
            $adminfooter      = view('siteadmin.includes.admin_footer');
            
             $manage_language = Settings::manage_language();
            

            return view('siteadmin.manage_language')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('manage_language',$manage_language);
        } else {
            return Redirect::to('siteadmin');
        }
    }
}
?>