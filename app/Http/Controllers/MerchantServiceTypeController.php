<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Lang;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Country;
use App\Customer;
use App\City;
use App\MerchantServiceType;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class MerchantServiceTypeController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
//add Service Type view function	
    public function mer_add_service_type()
    {
        if (Session::has('merchantid')) {
			if(Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
			
            return view('sitemerchant.add_service_type')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
//manage Service Type function  
    public function mer_manage_service_type()
    {
		
        if (Session::has('merchantid')) {
			if(Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $adminheader       = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $adminleftmenus    = view('sitemerchant.includes.merchant_left_menu_settings');
            $adminfooter       = view('sitemerchant.includes.merchant_footer');
            $manage_service_type     = MerchantServiceType::manage_service_type_list();
            
            return view('sitemerchant.manage_service_type')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('manage_service_type', $manage_service_type);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
//Add Service Type function  
    public function mer_add_service_type_submit()
    {
		
        if (Session::has('merchantid')) 
		{
            $data      = Input::except(array(
                '_token'
            ));
			$rule      = array(
                'service_type' => 'required|max:50',
                'service_status' => 'required'
            );
           
				$validator = Validator::make($data, $rule);
				if ($validator->fails()) 
				{
					return Redirect::to('add_service_type')->withErrors($validator->messages())->withInput();
				} 
				else 
				{
					$inputs = Input::all();
                
                    $entry = array(
                        'service_type_name' => Input::get('service_type'),
						'service_type_status' => Input::get('service_status'),
                    );
					
                    $return = MerchantServiceType::save_service_type($entry);
					if(Lang::has(Session::get('mer_lang_file').'.MER_RECORD_INSERTED_SUCCESSFULLY')!= '') 
					{
						$session_message = trans(Session::get('mer_lang_file').'.MER_RECORD_INSERTED_SUCCESSFULLY');
					}
					else 
					{
						$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_INSERTED_SUCCESSFULLY');
					}
                    return Redirect::to('mer_manage_service_type')->with('success', $session_message);
				}
				
        }
         else 
		{
            return Redirect::to('sitemerchant');
        }
    }
//edit Service type view function
    public function mer_edit_service_type($id)
    {
		
        if (Session::has('merchantid')) {
            $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "settings");
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
            $edit_service_type_list   = MerchantServiceType::edit_service_list($id);
			
            return view('sitemerchant.edit_service_type')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('edit_service_type_list', $edit_service_type_list);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
//update service type function    
    public function mer_edit_service_type_submit()
    {
		
        if (Session::has('merchantid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $id        = Input::get('service_type_id');
		
            $rule      = array(
                'service_type' => 'required|max:50',
								
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_business_category/' . $id)->withErrors($validator->messages());
            } 
                 else {
                    $entry = array(
                         'service_type_name' => Input::get('service_type'),
							'service_type_status' => Input::get('service_type_status'),
                    );
                }
                $return = MerchantServiceType::update_service_type_detail($entry, $id);
                return Redirect::to('mer_manage_service_type')->with('success', 'Record Updated Successfully');
            }
         else {
            return Redirect::to('sitemerchant');
        }
    }
//Service type status function    
    public static function mer_status_service_type_submit($id, $status)
    {
        if (Session::has('merchantid')) {
            $return = MerchantServiceType::status_service_type_submit($id, $status);
            return Redirect::to('mer_manage_service_type')->with('success', 'Status Updated Successfully');
        } else {
            return Redirect::to('sitemerchant');
        }
    }
//delete Service type function   
    public static function mer_delete_service_type($id)
    {
        if (Session::has('merchantid')) {
            $return = MerchantServiceType::delete_service_type($id);
            return Redirect::to('mer_manage_service_type')->with('success', 'Record Deleted Successfully');
        } else {
            return Redirect::to('sitemerchant');
        }
    }
  
}
?>