<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Attributes;
use Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class MerchantattributeController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct(){
       parent::__construct();
       /// set Merchant Panel language
       $this->setLanguageLocaleMerchant();
    }
    

    public function mer_add_size()
    {
        if (Session::has('merchantid')) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            return view('sitemerchant.attributes_addsize')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    
    public function mer_addsizesubmit()
    {
        if (Session::has('merchantid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'file_size' => 'required'
            );
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('mer_add_size')->withErrors($validator->messages())->withInput();
            } else {
                $inputs          = Input::all();
                $sizes           = array(
                    'si_name' => Input::get('file_size')
                );
                $checkname       = Input::get('file_size');
                $checkname_exist = Attributes::check_size_name($checkname);
                if (count($checkname_exist)>0) {
			if (Lang::has(Session::get('mer_lang_file').'.MER_SIZE_NAME_EXIST')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SIZE_NAME_EXIST');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SIZE_NAME_EXIST');
			}

                    return Redirect::to('mer_add_size')->with('exist_result', $session_message)->withInput();
                } else {
                    $return = Attributes::save_size($sizes);
					 if (Lang::has(Session::get('mer_lang_file').'.MER_RECORD_INSERTED_SUCCESSFULLY')!= '')
				{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_RECORD_INSERTED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_INSERTED_SUCCESSFULLY');
			}

                    return Redirect::to('mer_manage_size')->with('insert_result',$session_message);
                }
                
            }
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    
    public function mer_manage_size()
    {
        if (Session::has('merchantid')) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $get_size          = Attributes::get_size();
            return view('sitemerchant.attributes_manage_sizes')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('size_result', $get_size);
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_delete_size($id)
    {
        if (Session::has('merchantid')) {
            $return = Attributes::delete_size($id);
			 if (Lang::has(Session::get('mer_lang_file').'.MER_RECORD_DELETED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_RECORD_DELETED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_DELETED_SUCCESSFULLY');
			}
            return Redirect::to('mer_manage_size')->with('delete_result', $session_message);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    
    public function mer_edit_size($id)
    {
        if (Session::has('merchantid')) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $edit              = Attributes::edit_size($id);
            return view('sitemerchant.attributes_editsize')->with('editdates', $edit)->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    
    public function mer_editsize_submit()
    {
        if (Session::has('merchantid')) {
            $id   = Input::get('file_id');
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'file_size' => 'required'
            );
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('mer_edit_size/' . $id)->withErrors($validator->messages())->withInput();
            } else {
                $inputs          = Input::all();
                $id              = Input::get('file_id');
                $updates         = array(
                    'si_name' => Input::get('file_size')
                );
                $checkname       = Input::get('file_size');
                $checkname_exist = Attributes::check_size($checkname,$id);
                if ($checkname_exist) {
			if (Lang::has(Session::get('mer_lang_file').'.MER_SIZE_NAME_EXIST')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SIZE_NAME_EXIST');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SIZE_NAME_EXIST');
			}
                    return Redirect::to('mer_edit_size/' . $id)->with('exist_result', $session_message)->withInput();
                } else {
                    $return = Attributes::update_size($id, $updates);
					 if (Lang::has(Session::get('mer_lang_file').'.MER_RECORD_UPDATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_RECORD_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_UPDATED_SUCCESSFULLY');
			}
                    return Redirect::to('mer_manage_size')->with('update_result', $session_message);
                }
            }
            
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_add_color()
    {
        if (Session::has('merchantid')) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $color_list        = Attributes::color_list();
            return view('sitemerchant.attributes_addcolor')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('color_list', $color_list);
            
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_manage_color()
    {
        if (Session::has('merchantid')) {
			if (Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $color_added_list  = Attributes::color_added_list();
            return view('sitemerchant.attributes_manage_color')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('color_added_list', $color_added_list);
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_add_color_submit()
    {
        if (Session::has('merchantid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'color' => 'required',
                'color_name' => 'required'
                
            );

            $color_code = Input::get('color');

            $color_exist = DB::table('nm_color')->where('co_code', '=', $color_code)->count();    
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('mer_add_color')->withErrors($validator->messages())->withInput();
            }else   
         if($color_exist){
             if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_ALREADY_EXISTS')!= '') 
            { 

                $session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_ALREADY_EXISTS');

            }  
            else 
            { 
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_ALREADY_EXISTS');
            }   
            return Redirect::to('mer_add_color')->with('color_error',$session_message ); 
            } else {
                $colors = array(
                    'co_name' => Input::get('color_name'),
                    'co_code' => Input::get('color')
                );
               
                 // color fixed insert
                $return = Attributes::add_color($colors);
				 if (Lang::has(Session::get('mer_lang_file').'.MER_RECORD_INSERTED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_RECORD_INSERTED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_INSERTED_SUCCESSFULLY');
			}
                return Redirect::to('mer_manage_color')->with('success', $session_message);
            }
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_edit_color($id)
    {
        if (Session::has('merchantid')) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $edit              = Attributes::edit_color($id);
            $color_added_list  = Attributes::color_list();
            return view('sitemerchant.attributes_editcolor')->with('edit_color', $edit)->with('color_added_list', $color_added_list)->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function mer_editcolor_submit()
    {
        if (Session::has('merchantid')) {
            $id   = Input::get('color_id');
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'color' => 'required',
                'color_name' => 'required'
            );
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('mer_edit_color/' . $id)->withErrors($validator->messages())->withInput();
            } else {
                $inputs  = Input::all();
                $id      = Input::get('color_id');
                $updates = array(
                    'co_name' => Input::get('color_name'),
                    'co_code' => Input::get('color')
                    
                );
                 // color fixed update
                $return  = Attributes::update_color($id, $updates);
				 if (Lang::has(Session::get('mer_lang_file').'.MER_RECORD_UPDATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_RECORD_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_UPDATED_SUCCESSFULLY');
			}
                return Redirect::to('mer_manage_color')->with('success',$session_message);
                
            }
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_deletecolor_submit($id)
    {
        if (Session::has('merchantid')) {
            $return = Attributes::deletecolor_submit($id);
             // delete color fixed
			 if (Lang::has(Session::get('mer_lang_file').'.MER_RECORD_DELETED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_RECORD_DELETED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_DELETED_SUCCESSFULLY');
			}
            return Redirect::to('mer_manage_color')->with('success',$session_message);
            
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    public function mer_attribute_select_color()
    {
        if (Session::has('merchantid')) {
            $id = '#' . $_GET['color_id'];
            
            $color_list = Attributes::selected_color_list($id);
            if ($color_list) {
                echo $color_list[0]->co_name;
            } else {
                echo "no color found";
            }
            
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    
}
