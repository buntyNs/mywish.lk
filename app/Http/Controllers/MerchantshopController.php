<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Lang;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Products;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\MerchantTransactions;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
class MerchantshopController extends Controller
{
    
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    
    public function __construct(){
       parent::__construct();
       /// set Merchant Panel language
       $this->setLanguageLocaleMerchant();
    }
 
    
    public function manage_shop($id)
    {
		if(Lang::has(Session::get('admin_lang_file').'.MER_SHOP')!= '') 
		{
			$session_message = trans(Session::get('admin_lang_file').'.MER_SHOP');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SHOP');
		}
        $merchantid = Session::get('merchantid');
                
        $merchantheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_Shop');
        $merchantfooter    = view('sitemerchant.includes.merchant_footer');
        $store_return      = Merchant::view_merchant_store_details($id, $merchantid);
        
        $store_is_or_not_in_deals   = Merchant::store_is_or_not_in_deals($store_return);
        $store_is_or_not_in_product = Merchant::store_is_or_not_in_product($store_return);
        $store_is_or_not_in_auction = Merchant::store_is_or_not_in_auction($store_return);
        
        return view('sitemerchant.manage_shop')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('store_return', $store_return)->with('store_is_or_not_in_deals', $store_is_or_not_in_deals)->with('store_is_or_not_in_product', $store_is_or_not_in_product)->with('store_is_or_not_in_auction', $store_is_or_not_in_auction);
           
    }
    
    
    public function add_shop($id)
    {
        if (Session::has('merchantid')) {
            $merchantid = Session::get('merchantid');
        }
         
        if ($id == $merchantid) {
            if(Lang::has(Session::get('admin_lang_file').'.MER_SHOP')!= '') 
			{
				$session_message = trans(Session::get('admin_lang_file').'.MER_SHOP');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SHOP');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_Shop');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $country_return    = Merchant::get_country_detail();
            return view('sitemerchant.add_shop')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('country_details', $country_return)->with('id', $id);
            
        } else {
            return Redirect::to('sitemerchant');
            
        }
    }

    public function edit_shop($id, $mer_id)
    {
		if(Lang::has(Session::get('admin_lang_file').'.MER_SHOP')!= '') 
		{
			$session_message = trans(Session::get('admin_lang_file').'.MER_SHOP');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SHOP');
		}
        $merchantheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_Shop');
        $merchantfooter    = view('sitemerchant.includes.merchant_footer');
        $country_return    = Merchant::get_country_detail();
        $store_return      = Merchant::get_induvidual_store_detail_merchant($id, $mer_id);
        
        if ($store_return) {
            return view('sitemerchant.edit_shop')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('country_details', $country_return)->with('id', $id)->with('store_return', $store_return)->with('mer_id', $mer_id);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function add_shop_submit()
    {
		$get_active_lang=$this->get_active_language;
        $merchant_id = Input::get('store_merchant_id');
        $data        = Input::except(array(
            '_token'
        ));
        $rule        = array(
            'store_name' => 'required|regex:/^[A-Z a-z 0-9_-]+$/|max:50',
            'store_pho' => 'required|numeric',
            'store_add_one' => 'required',
            'store_add_two' => 'required',
            'select_country' => 'required',
            'select_city' => 'required',
            'zip_code' => 'required|numeric',
            //'meta_keyword' => 'required',
            //'meta_description' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'stor_website' => 'url',
            'file' => 'required|image|mimes:jpeg,png,jpg|image_size:'.$this->store_width.','.$this->store_height.''
       );
		$lang_entry_rule = array();
		if(!empty($get_active_lang))
		{
			foreach($get_active_lang as $get_lang)
			{
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = strtolower($get_lang->lang_name);

				$lang_entry_rule = array(
				'store_add_one_'.$get_lang_name.'' => 'required',
				'store_add_two_'.$get_lang_name.'' => 'required',
				'store_name_'.$get_lang_name.'' => 'required',
				'meta_keyword_'.$get_lang_name.'' => 'required',
				'meta_description_'.$get_lang_name.'' => 'required'
				); 
				$rule  = array_merge($rule,$lang_entry_rule);
			}
		}
       
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return Redirect::to('merchant_add_shop/' . $merchant_id)->withErrors($validator->messages())->withInput();
        } else {
            $mer_email       = Input::get('email_id');
            $file            = Input::file('file');
            $time			 = time();
            $filename 		 = 'Store_' .$time.'_'. $file->getClientOriginalName();
            $destinationPath = './public/assets/storeimage/';
            Image::make($file)->save('./public/assets/storeimage/'.$filename,$this->image_compress_quality); 
            
            $store_entry = array(
                'stor_name' => Input::get('store_name'),
                'stor_merchant_id' => $merchant_id,
                'stor_phone' => Input::get('store_pho'),
                'stor_address1' => Input::get('store_add_one'),
                'stor_address2' => Input::get('store_add_two'),
                'stor_country' => Input::get('select_country'),
                'stor_city' => Input::get('select_city'),
                'stor_zipcode' => Input::get('zip_code'),
                'stor_metakeywords' => Input::get('meta_keyword'),
                'stor_metadesc' => Input::get('meta_description'),
                'stor_website' => Input::get('website'),
                'stor_latitude' => Input::get('latitude'),
                'stor_longitude' => Input::get('longitude'),
                'stor_img' => $filename,
                'stor_addedby' => 2
            );
			$store_lang_entry = array();
			if(!empty($get_active_lang))
			{
				foreach($get_active_lang as $get_lang)
				{
					$get_lang_code = $get_lang->lang_code;
					$get_lang_name = strtolower($get_lang->lang_name);

					$store_lang_entry = array(
					'stor_address1_'.$get_lang_code.'' => Input::get('store_add_one_'.$get_lang_name),
					'stor_address2_'.$get_lang_code.'' => Input::get('store_add_two_'.$get_lang_name),
					'stor_name_'.$get_lang_code.'' => Input::get('store_name_'.$get_lang_name),
					'stor_metakeywords_'.$get_lang_code.'' => Input::get('meta_keyword_'.$get_lang_name),
					'stor_metadesc_'.$get_lang_code.'' => Input::get('meta_description_'.$get_lang_name)
					); 
					$store_entry  = array_merge($store_entry,$store_lang_entry);
				}
			}
			
            if(Lang::has(Session::get('admin_lang_file').'.MER_STORE_ADDED_SUCCESSFULLY')!= '') 
			{
				$session_message = trans(Session::get('admin_lang_file').'.MER_STORE_ADDED_SUCCESSFULLY');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_STORE_ADDED_SUCCESSFULLY');
			}
            Merchant::insert_store($store_entry);
            return Redirect::to('merchant_manage_shop/' . $merchant_id)->with('result', $session_message);
        }
    }
    
    public function edit_shop_submit()
    {
		$get_active_lang=$this->get_active_language;
        $merchant_id = Input::get('mer_id');
        $store_id    = Input::get('store_id');
        $data        = Input::except(array(
            '_token'
        ));
        $rule        = array(
            'store_name' => 'required|regex:/^[A-Z a-z 0-9_-]+$/|max:50',
            'store_pho' => 'required|numeric',
            'store_add_one' => 'required',
            'store_add_two' => 'required',
            'select_country' => 'required',
            'select_city' => 'required',
            'zip_code' => 'required|numeric',
           // 'meta_keyword' => 'required',
           // 'meta_description' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            
			'file' => 'image|mimes:jpeg,png,jpg|image_size:'.$this->store_width.','.$this->store_height.''
        );
		$lang_entry_rule = array();
		if(!empty($get_active_lang))
		{
			foreach($get_active_lang as $get_lang)
			{
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = strtolower($get_lang->lang_name);

				$lang_entry_rule = array(
				'store_add_one_'.$get_lang_name.'' => 'required',
				'store_add_two_'.$get_lang_name.'' => 'required',
				'store_name_'.$get_lang_name.'' => 'required',
				'meta_keyword_'.$get_lang_name.'' => 'required',
				'meta_description_'.$get_lang_name.'' => 'required'
				); 
				$rule  = array_merge($rule,$lang_entry_rule);
			}
		}
        
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->messages())->withInput();
        } else {
            $file = Input::file('file');
            if ($file == '') {
                $filename = Input::get('file_new');
            } else {
                $time=time();
				$filename = 'Store_' .$time.'_'. $file->getClientOriginalName();
                $old_store_image = Input::get('file_new');
				$destinationPath  = './public/assets/storeimage/';
					if(file_exists($destinationPath.$old_store_image))
					{
						@unlink($destinationPath.$old_store_image);
					}
                Image::make($file)->save('./public/assets/storeimage/'.$filename,$this->image_compress_quality);
            }
            $store_entry = array(
                'stor_name' => Input::get('store_name'),
                'stor_phone' => Input::get('store_pho'),
                'stor_address1' => Input::get('store_add_one'),
                'stor_address2' => Input::get('store_add_two'),
                'stor_country' => Input::get('select_country'),
                'stor_city' => Input::get('select_city'),
                'stor_zipcode' => Input::get('zip_code'),
                'stor_metakeywords' => Input::get('meta_keyword'),
                'stor_metadesc' => Input::get('meta_description'),
                'stor_website' => Input::get('website'),
                'stor_latitude' => Input::get('latitude'),
                'stor_longitude' => Input::get('longitude'),
                'stor_img' => $filename,
            );
			$store_lang_entry = array();
			if(!empty($get_active_lang))
			{
				foreach($get_active_lang as $get_lang)
				{
					$get_lang_code = $get_lang->lang_code;
					$get_lang_name = strtolower($get_lang->lang_name);

					$store_lang_entry = array(
					'stor_address1_'.$get_lang_code.'' => Input::get('store_add_one_'.$get_lang_name),
					'stor_address2_'.$get_lang_code.'' => Input::get('store_add_two_'.$get_lang_name),
					'stor_name_'.$get_lang_code.'' => Input::get('store_name_'.$get_lang_name),
					'stor_metakeywords_'.$get_lang_code.'' => Input::get('meta_keyword_'.$get_lang_name),
					'stor_metadesc_'.$get_lang_code.'' => Input::get('meta_description_'.$get_lang_name)
					); 
					$store_entry  = array_merge($store_entry,$store_lang_entry);
				}
			}
            if(Lang::has(Session::get('admin_lang_file').'.MER_STORE_UPDATED_SUCCESSFULLY')!= '') 
			{
				$session_message = trans(Session::get('admin_lang_file').'.MER_STORE_UPDATED_SUCCESSFULLY');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_STORE_UPDATED_SUCCESSFULLY');
			}
			
            Merchant::edit_store($store_id, $store_entry);
            return Redirect::to('merchant_manage_shop/' . $merchant_id)->with('result', $session_message);
        }
    }
    
    public function block_shop($id, $status, $mer_id)
    {
        $check_country_status = Merchant::check_city_status($id);
            if($check_country_status>0){    //if country is active you can change status otherwise cant able to change mer status
                if ($status == 1) {  $store_city_status = 'A'; }elseif($status==0){ $store_city_status = 'B'; }
                $entry = array(
                'stor_status' => $status,
                'store_city_status'=>$store_city_status,
                );

        
                Merchant::block_store_status($id, $entry);
                if ($status == 1) {
			        if(Lang::has(Session::get('admin_lang_file').'.MER_STORE_ACTIVATED_SUCCESSFULLY')!= ''){
				        $session_message = trans(Session::get('admin_lang_file').'.MER_STORE_ACTIVATED_SUCCESSFULLY');
			        }else{
				        $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_STORE_ACTIVATED_SUCCESSFULLY');
			        }
                    return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', $session_message);
                } elseif($status==0) {
			        if(Lang::has(Session::get('admin_lang_file').'.MER_STORE_BLOCKED_SUCCESSFULLY')!= ''){
				        $session_message = trans(Session::get('admin_lang_file').'.MER_STORE_BLOCKED_SUCCESSFULLY');
			        }else{
				        $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_STORE_BLOCKED_SUCCESSFULLY');
			        }
                    return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', $session_message);
                }
          }//if city is active only they can able to change able to block/unblock status of store
          else{
                 return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', 'Contact your Administrator. Store City is blocked so we cant able to change the status of store');
          }

    }//function

    /* Promote or depromote shop */

    public function promote_shop($id, $status, $mer_id)
    {  
        $check_country_status = Merchant::check_city_status($id);
            if($check_country_status>0){    //if country is active you can change status otherwise cant able to change mer status
                $store_promote_status = Merchant::check_promote_status($mer_id);

                // echo $store_promote_status; exit;
                if ($store_promote_status > 0 )  //Already one store is promoted
                { 
                    $store_id = Merchant::check_promoted($id,$mer_id);
                    if($store_id == 0)
                    {
                      $session_message = "Already one store is promoted";
                        return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', $session_message);
                    }
                    else
                    {
                         $entry = array('promote_status' => $status);
                    }
                }
                elseif($store_promote_status == 0)
                  { 
                    
                $entry = array(
                'promote_status' => $status,
                 );

                }
                Merchant::promote_store_status($id, $entry);
                if ($status == 1) {
                    $session_message = "Store is  promoted";
                    return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', $session_message);
                    
                } 
                elseif($status==0) {
                    $session_message = "Store is  depromoted";
                    return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', $session_message);
                }
          }//if city is active only they can able to change able to block/unblock status of store
          else{
                 return Redirect::to('merchant_manage_shop/' . $mer_id)->with('result', 'Contact your Administrator. Store City is blocked so we cant able to change the status of store');
          }

    }//function
    //Save Edited Image
    public function mer_CropNdUpload_store(){
        //echo 'jhj';
       // var_dump($request->input('_token'));
        $data = Input::except(array(
                '_token'
            ));
       // print_r($data);exit();
        $store_id       = Input::get('product_id');
        $img_id         = Input::get('img_id');
        $mer_id         = Input::get('mer_id');
        $imgfileName    = Input::get('imgfileName'); //old image file name

        $imageData = Input::get('base64_imgData');
        $img_dat = explode(',',Input::get('base64_imgData'));
        $new_name = 'Deal_'.time().rand().'.png';

        $imageData         = base64_decode($img_dat[1]);

        $file_path = './public/assets/storeimage/'.$new_name;
        //Upload image with compression
        $img = Image::make(imagecreatefromstring($imageData))->save($file_path);
        //jpg background color black remove
        list($width, $height) = getimagesize($file_path);
        $output = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($output,  255, 255, 255);
        imagefilledrectangle($output, 0, 0, $width, $height, $white);
        imagecopy($output, imagecreatefromstring($imageData), 0, 0, 0, 0, $width, $height);
        imagejpeg($output, $file_path);
        //image compression
        $img = Image::make($output)->save($file_path,$this->image_compress_quality);

        if(file_exists('public/assets/storeimage/'.$new_name)){

            //upload small image
            list($width,$height)=getimagesize('public/assets/storeimage/'.$new_name);     
            

            //unlink old files starts
            if(file_exists('public/assets/storeimage/'.$imgfileName))
                unlink("public/assets/storeimage/".$imgfileName);
            if(file_exists('public/assets/storeimage/'.$imgfileName))
                unlink("public/assets/storeimage/".$imgfileName);
            //unlink old files ends

            //update in image table 
            $view_store_details = Merchant::get_induvidual_store_detail($store_id);

            $entry = array('stor_img' => $new_name );

             $return     = Merchant::edit_store($store_id, $entry);
             if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_UPDATED_SUCCESSFULLY')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_IMAGE_UPDATED_SUCCESSFULLY');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.MER_IMAGE_UPDATED_SUCCESSFULLY');
            }
            return Redirect::to('merchant_edit_shop/'.$store_id.'/'.$mer_id)->with('block_message', $session_message);
        }

        exit();
    }
    /* Image  Crop , rorate and mamipulation ends */
    
    
}//class

?>
