<?php

namespace App\Http\Middleware;

use Closure;
use DB , Session ;
use App;
use View;
use App\Settings;
use Config;

class MobileLanguage
{
	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
    	   if($request->lang != '')
            {   
                $lang= $request->lang;

                if(file_exists('resources/lang/'.$lang.'/'.$lang.'_mob_lang.php'))
                {
                    /* check language file is active */
                    $check = \DB::table('nm_language')->where(['lang_status' => '1','lang_code' => $lang])->count();
                    if($check == 0)
                    {
                        return response()->json(['status' => '400','message' => 'Invalid language']);
                    }
                }
                else
                {
                    return response()->json(['status' => '400','message' => 'Language file does not exist']);
                }
                
            }
            elseif($request->lang == '')
            {
                return response()->json(['status' => '400','message' => 'Language parameter missing']);
            }
    	 return $next($request);
    }
}
