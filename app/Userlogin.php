<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Userlogin extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_customer';
    
    public static function check_user($pwd, $email)
    {
        return DB::table('nm_customer')->where('cus_email', '=', $email)->where('cus_pwd', '=', $pwd)->where('cus_status','!=',2)->get();
    }

    public static function forgot_check_details_user($email)
    {
        return DB::table('nm_customer')->where('cus_email', '=', $email)->get();
    }

    public static function checkvalidemail($email)
    {
        return DB::table('nm_customer')->where('cus_email', '=', $email)->get();
    }

    public static function save_log($entry)
    {
        return DB::table('nm_login')->insert($entry);
    }

    public static function update_newpwd($cus_id, $confirmpwd)
    {

       return  DB::table('nm_customer')->where('cus_id', '=', $cus_id)->update(array(
            'cus_pwd' => $confirmpwd
        ));
       
        
    }

    public static function get_customer_details($customer_decode_email)
    {
        return DB::table('nm_customer')->where('cus_email', '=', $customer_decode_email)->get();
    }
	
	 public static function randomPassword()
    {
        $alphabet    = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass        = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n      = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
	
	public static function insert_forgetpas($entry,$user_email)
    {
        $check_insert = DB::table('nm_customer')->where('cus_email', '=', $user_email)->update($entry);
        
    }
    
}

?>
