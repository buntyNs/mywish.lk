-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2018 at 08:43 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelecommerce_with_lang_empty`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery_status_chat`
--

CREATE TABLE `delivery_status_chat` (
  `chat_id` int(11) NOT NULL,
  `delStatus_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `mer_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `send_by` int(11) NOT NULL COMMENT '''1''-customer,''2''-merchant,''3''-admin',
  `note` text NOT NULL,
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_aboutus`
--

CREATE TABLE `nm_aboutus` (
  `ap_id` int(11) NOT NULL,
  `ap_description` longtext NOT NULL,
  `ap_description_fr` longtext NOT NULL,
  `ap_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_aboutus`
--

INSERT INTO `nm_aboutus` (`ap_id`, `ap_description`, `ap_description_fr`, `ap_date`) VALUES
(1, 'About Us<br><br>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br>&nbsp;', 'À propos de nous', '2017-06-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `nm_add`
--

CREATE TABLE `nm_add` (
  `ad_id` smallint(5) UNSIGNED NOT NULL,
  `ad_name` varchar(100) NOT NULL,
  `ad_name_fr` varchar(200) NOT NULL,
  `ad_position` tinyint(4) NOT NULL COMMENT '1-left,2-middle,3-right',
  `ad_pages` tinyint(4) NOT NULL COMMENT '1-home,2-product,3-Deal,4-Auction',
  `ad_redirecturl` varchar(150) NOT NULL,
  `ad_img` varchar(150) NOT NULL,
  `ad_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-admin 2 customer',
  `ad_status` tinyint(3) UNSIGNED NOT NULL,
  `ad_read_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_add`
--

INSERT INTO `nm_add` (`ad_id`, `ad_name`, `ad_name_fr`, `ad_position`, `ad_pages`, `ad_redirecturl`, `ad_img`, `ad_type`, `ad_status`, `ad_read_status`) VALUES
(1, 'Mens Fashion', ' Moda de hombres', 1, 1, 'http://google.com', 'Ads_1516431547.jpg', 1, 0, 1),
(3, 'Mobiles', 'Móviles', 2, 1, 'https://www.google.co.in', 'Ads_1516431567.jpg', 1, 1, 1),
(5, 'tyty', '', 3, 1, 'http://www.google.com', 'Ads_1522839997.jpg', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_admin`
--

CREATE TABLE `nm_admin` (
  `adm_id` int(10) UNSIGNED NOT NULL,
  `adm_fname` varchar(150) NOT NULL,
  `adm_lname` varchar(150) NOT NULL,
  `adm_password` varchar(150) NOT NULL,
  `adm_email` varchar(150) NOT NULL,
  `adm_phone` varchar(20) NOT NULL,
  `adm_address1` varchar(150) NOT NULL,
  `adm_address2` varchar(150) NOT NULL,
  `adm_ci_id` int(10) UNSIGNED NOT NULL COMMENT 'city id',
  `adm_co_id` smallint(5) UNSIGNED NOT NULL COMMENT 'country id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_admin`
--

INSERT INTO `nm_admin` (`adm_id`, `adm_fname`, `adm_lname`, `adm_password`, `adm_email`, `adm_phone`, `adm_address1`, `adm_address2`, `adm_ci_id`, `adm_co_id`) VALUES
(1, 'admin', 'admin', 'admin', 'ragulgandhi@pofitec.com', '9790153222', 'chennai', 'chennai', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_adminreply_comments`
--

CREATE TABLE `nm_adminreply_comments` (
  `reply_id` int(11) NOT NULL,
  `reply_blog_id` int(11) NOT NULL,
  `reply_cmt_id` int(11) NOT NULL,
  `reply_msg` text NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_auction`
--

CREATE TABLE `nm_auction` (
  `auc_id` int(10) UNSIGNED NOT NULL,
  `auc_title` varchar(500) NOT NULL,
  `auc_category` int(11) NOT NULL,
  `auc_main_category` int(11) NOT NULL,
  `auc_sub_category` int(11) NOT NULL,
  `auc_second_sub_category` int(11) NOT NULL,
  `auc_original_price` int(11) NOT NULL,
  `auc_auction_price` int(11) NOT NULL,
  `auc_bitinc` smallint(5) UNSIGNED NOT NULL,
  `auc_saving_price` int(11) NOT NULL,
  `auc_start_date` datetime NOT NULL,
  `auc_end_date` datetime NOT NULL,
  `auc_shippingfee` decimal(10,2) NOT NULL,
  `auc_shippinginfo` text NOT NULL,
  `auc_description` text NOT NULL,
  `auc_merchant_id` int(11) NOT NULL,
  `auc_shop_id` int(11) NOT NULL,
  `auc_meta_keyword` varchar(250) NOT NULL,
  `auc_meta_description` varchar(500) NOT NULL,
  `auc_image_count` int(11) NOT NULL,
  `auc_image` varchar(500) NOT NULL,
  `auc_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `auc_posted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_auction`
--

INSERT INTO `nm_auction` (`auc_id`, `auc_title`, `auc_category`, `auc_main_category`, `auc_sub_category`, `auc_second_sub_category`, `auc_original_price`, `auc_auction_price`, `auc_bitinc`, `auc_saving_price`, `auc_start_date`, `auc_end_date`, `auc_shippingfee`, `auc_shippinginfo`, `auc_description`, `auc_merchant_id`, `auc_shop_id`, `auc_meta_keyword`, `auc_meta_description`, `auc_image_count`, `auc_image`, `auc_status`, `auc_posted_date`) VALUES
(1, 'Diamond necklace', 1, 1, 1, 1, 15000, 10500, 10, 4500, '2014-08-09 12:50:47', '2014-09-09 12:50:47', '10.00', 'Ships to india', 'Diamond necklace will take you to a completely different world with \r\nspectacular views. Jewelry diamond necklaces the most beautiful state. \r\nJewelry design is an art. The worldâ€™s most valuable asset is the people.\r\n Charms adorn people. So, Jewelry design is the art of human adornment. \r\nJewelry design is one of the oldest professions. Diamond necklaces are \r\ndesigned by expert designers. We chose the most beautiful diamond \r\nnecklace designs for you. There are two predominant objective in the \r\ndesign of diamond necklaces. Exquisite designs, modesty shows in the \r\nwealth. Large stone adorned with necklaces, are influenced environment. \r\nYou can choose the most special days of the diamond necklace.<br><br><br>', 6, 5, 'Diamond', 'Diamond', 2, 'jewelzc8BoIZr.jpg/**/new2jdMILdT7.jpg/**/new1PhpgKukL.jpg/**/', 1, '2014-08-13 04:11:08'),
(2, 'hand bags', 1, 1, 1, 1, 1500, 1000, 10, 500, '2014-08-13 16:49:34', '2014-08-06 16:49:34', '25.00', 'free', 'asds', 2, 1, 'asd', 'asss', 4, '2YmlkRqJC.png/**/5UCkEi0zv.png/**/4TZEEKtZ7.png/**/1Zfw5yHW8.png/**/6A9AOLRAx.png/**/', 1, '2014-08-11 11:20:54'),
(3, 'hand bags', 1, 1, 1, 1, 1500, 1000, 10, 500, '2014-08-09 12:50:47', '2014-08-12 16:51:17', '25.00', 'swds', 'sdsa', 2, 1, 'sads', 'asdsa', 4, '2w4QZLuE2.png/**/5iJNcTQKA.png/**/62wJiGqDm.png/**/1r11FrvLT.png/**/3hypyZDuW.png/**/', 1, '2014-08-11 11:22:15'),
(4, 'Merchant deal', 2, 2, 2, 2, 10000, 9000, 10, 1000, '2014-08-12 09:38:54', '2014-08-13 15:24:54', '10.00', 'coimbatore vadavalli', 'Meta keywords<br>', 4, 3, 'fdafds', 'dsfds', 2, 'flower3SIH0fbjz.jpg/**/flower1Fk1kQ6Cw.jpg/**/flower2jaBoQuEf.jpg/**/', 1, '2014-08-13 04:10:25'),
(5, 'sasa', 1, 1, 1, 1, 150, 100, 5, 50, '2014-11-21 19:41:48', '2014-11-28 19:41:48', '12.00', 'wewewe', 'weewe', 1, 1, 'wewe', 'wewe', 0, 'DMR_48xTLGZCgG.jpg/**/', 1, '2014-11-20 16:14:58'),
(6, 'BiG Bazar', 2, 2, 0, 0, 1000, 100, 10, 900, '2014-12-10 11:20:30', '2014-12-16 11:20:30', '3.00', 'shipp', 'auction', 5, 4, 'rterte', 'ertert', 0, 'IMG_1269_wUYY6ufcW.jpg/**/', 1, '2014-12-10 10:22:18'),
(7, 'Senbagam', 5, 9, 19, 49, 400, 300, 10, 100, '2015-05-11 08:15:34', '2015-05-12 08:04:34', '0.00', 'item shipping', 'This product which unique<br>', 3, 2, 'keywords', 'keywords', 0, 'ChrysanthemumZVXd9lBr.jpg/**/', 1, '2015-05-11 02:49:23'),
(8, 'shoe sport', 4, 7, 33, 0, 100, 50, 1, 50, '2016-02-17 18:34:37', '2016-02-25 18:34:37', '0.00', 'ship to you', 'is a sport shoe, nike', 1, 1, 'ss', 'ss', 0, 'slide1-2mwb33rdS.png/**/', 1, '2016-02-17 10:36:00'),
(9, 'Test Auc', 5, 8, 14, 35, 320, 250, 10, 70, '2016-03-09 17:40:47', '2016-03-25 17:40:47', '20.00', 'Test', 'Test', 1, 140, 'Test', 'Test', 0, 'imagesRT1JF5cz.jpeg/**/', 1, '2016-03-09 12:11:52');

-- --------------------------------------------------------

--
-- Table structure for table `nm_banner`
--

CREATE TABLE `nm_banner` (
  `bn_id` smallint(5) UNSIGNED NOT NULL,
  `bn_title` varchar(150) NOT NULL,
  `bn_title_fr` varchar(150) NOT NULL,
  `bn_type` varchar(10) NOT NULL COMMENT '1-home,2-product,3-deal,4-auction',
  `bn_img` varchar(150) NOT NULL,
  `bn_status` int(11) NOT NULL COMMENT '1-block,0-unblock',
  `bn_redirecturl` text NOT NULL,
  `bn_slider_position` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_blog`
--

CREATE TABLE `nm_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_title` varchar(50) NOT NULL,
  `blog_title_fr` varchar(100) NOT NULL,
  `blog_desc` text NOT NULL,
  `blog_desc_fr` text NOT NULL,
  `blog_catid` int(11) NOT NULL,
  `blog_image` varchar(100) NOT NULL,
  `blog_metatitle` varchar(100) NOT NULL,
  `blog_metatitle_fr` varchar(150) NOT NULL,
  `blog_metadesc` text NOT NULL,
  `blog_metadesc_fr` text NOT NULL,
  `blog_metakey` varchar(100) NOT NULL,
  `blog_metakey_fr` varchar(150) NOT NULL,
  `blog_tags` varchar(100) NOT NULL,
  `blog_comments` int(5) NOT NULL COMMENT '0-not allow,1-allow',
  `blog_type` int(5) UNSIGNED NOT NULL COMMENT '1-publish,2-drafts',
  `blog_status` tinyint(3) UNSIGNED NOT NULL COMMENT '1-block,0-unblock',
  `blog_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_blogsetting`
--

CREATE TABLE `nm_blogsetting` (
  `bs_id` tinyint(3) UNSIGNED NOT NULL,
  `bs_allowcommt` tinyint(4) NOT NULL,
  `bs_radminapproval` tinyint(4) NOT NULL COMMENT 'Require Admin Approval (1-yes & 0-No)',
  `bs_postsppage` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_blogsetting`
--

INSERT INTO `nm_blogsetting` (`bs_id`, `bs_allowcommt`, `bs_radminapproval`, `bs_postsppage`) VALUES
(1, 1, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `nm_blog_cus_comments`
--

CREATE TABLE `nm_blog_cus_comments` (
  `cmt_id` int(11) NOT NULL,
  `cmt_blog_id` int(11) NOT NULL,
  `cmt_name` varchar(250) NOT NULL,
  `cmt_email` varchar(250) NOT NULL,
  `cmt_website` varchar(250) NOT NULL,
  `cmt_msg` text NOT NULL,
  `cmt_admin_approve` int(11) NOT NULL DEFAULT '0' COMMENT '1 => Approved, 2 => Unapproved',
  `cmt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cmt_msg_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read ,1 Read '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_category_ad`
--

CREATE TABLE `nm_category_ad` (
  `cat_ad_id` int(11) NOT NULL,
  `cat_ad_maincat_id` int(11) NOT NULL,
  `cat_ad_img` varchar(512) NOT NULL,
  `cat_ad_status` int(11) NOT NULL COMMENT '1-block,0-Unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_category_banner`
--

CREATE TABLE `nm_category_banner` (
  `cat_bn_id` int(11) NOT NULL,
  `cat_bn_maincat_id` int(11) NOT NULL,
  `cat_bn_img` varchar(512) NOT NULL,
  `cat_bn_status` int(1) NOT NULL COMMENT '1-block,0-Unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_city`
--

CREATE TABLE `nm_city` (
  `ci_id` int(10) UNSIGNED NOT NULL,
  `ci_name` varchar(100) NOT NULL,
  `ci_name_fr` varchar(150) NOT NULL,
  `ci_con_id` smallint(6) NOT NULL,
  `ci_lati` varchar(150) NOT NULL,
  `ci_long` varchar(150) NOT NULL,
  `ci_default` tinyint(4) NOT NULL,
  `ci_status` tinyint(4) NOT NULL COMMENT '1=>unblock,0=>block'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_cms_pages`
--

CREATE TABLE `nm_cms_pages` (
  `cp_id` smallint(5) UNSIGNED NOT NULL,
  `cp_title` varchar(250) NOT NULL,
  `cp_title_fr` varchar(250) NOT NULL,
  `cp_description` longtext NOT NULL,
  `cp_description_fr` longtext NOT NULL,
  `cp_status` tinyint(4) NOT NULL DEFAULT '1',
  `cp_created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_cms_pages`
--

INSERT INTO `nm_cms_pages` (`cp_id`, `cp_title`, `cp_title_fr`, `cp_description`, `cp_description_fr`, `cp_status`, `cp_created_date`) VALUES
(9, 'Security', '', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', 1, '2018-04-16 16:33:32'),
(13, 'Help', '', 'What Information Do You We Collect<br>We collect information from you when&nbsp;register&nbsp;on our site or fill out our contact form.<br>When ordering or raising&nbsp;enquiry&nbsp;on our website, as appropriate, you may be asked to enter your Name, Email id, Phone&nbsp;number&nbsp;and&nbsp;skype&nbsp;ID.&nbsp;However&nbsp;you could visit Laravel&nbsp;ecommerce&nbsp;website anonymously.<br><br>Special Notice&nbsp;<br>If you are under 13 years old Laravel&nbsp;Ecommerce&nbsp;website is not anticipated at children under 13 years old and we do not collect, use, provide or process in any other form any personal information of children under the age of 13 consciously. We therefore also ask you, if you are under 13 years old, please do not send us your personal information (for example, your name, address and email address).<br><br>Purposes of the collection of your data<br>&nbsp;Laravel&nbsp;Ecommerce&nbsp;is&nbsp;intent&nbsp;to inform you of who we are and what we do. We collect and use personal information (including name, phone&nbsp;number&nbsp;and email ID) to better provide you with the required services, or information. We&nbsp;would therefore&nbsp;use your personal information in order to:<ul><li>Acknowledge to your queries or requests</li><li>Govern our obligations in relation to any agreement you have with us</li><li>Anticipate and resolve problems with any goods or services supplied to you</li><li>Create products or services that may meet your needs</li></ul>Keeping our records accurate<br>&nbsp;We aim to keep our data confidential about you as authentic as possible. If you would like to review, change or delete the details you have provided with us, please contact us via email which is&nbsp;mentioned in&nbsp;our website.<br><br>Security of your personal data<br>&nbsp;As we value your personal information, we will establish sufficient level of protection. We have therefore enforced technology and policies with the objective of protecting your privacy from illegal access and erroneous use and will update these measures as new technology becomes available, as relevant.<br>Cookies policy<h4>Why do we use cookies?</h4>We use browser cookies to learn more about the way you interact with our content and help us to improve your experience when visiting our website.<br>Cookies remember the type of browser you use and which additional browser software you have installed. They also remember your preferences, such as language and region, which remain as your default settings when you revisit the website. Cookies also allow you to rate pages and fill in comment forms.<br>Some of the cookies we use are session cookies and only last until you close your browser, others are persistent cookies which are stored on your computer for longer.<br>Changes on privacy policy<br>&nbsp;We may make&nbsp;changes on&nbsp;our website’s privacy policy at any time. If we make any consequential changes to this privacy policy and the way in which we use your personal data we will post these changes on this page and will do our best to notify you of any significant changes. Kindly often check our privacy policies.', '', 1, '2017-12-18 16:47:27'),
(14, 'Returns Policy', '', '1.merchant terms and condition', '', 1, '2018-01-19 15:17:55'),
(15, 'Privacy', '', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', 1, '2018-04-16 16:33:16'),
(17, 'fgfdh', '', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', 1, '2018-04-16 16:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `nm_cod_commission_paid`
--

CREATE TABLE `nm_cod_commission_paid` (
  `comPaid_id` int(11) NOT NULL,
  `com_merchant_id` int(11) NOT NULL,
  `com_mer_name` varchar(255) NOT NULL,
  `paidAmount` double NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_type` enum('0','1') NOT NULL COMMENT '0-offline,1-paypal',
  `online_payment_returnStatus` varchar(255) NOT NULL,
  `com_status` enum('0','1','2') NOT NULL COMMENT '1 => paid, 0=> Hold,''2''=>Cancelled',
  `com_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_cod_commission_tracking`
--

CREATE TABLE `nm_cod_commission_tracking` (
  `com_id` int(11) NOT NULL,
  `com_merchant_id` int(11) NOT NULL,
  `com_cod_id` int(11) NOT NULL,
  `com_pro_id` int(11) NOT NULL,
  `com_cod_amt` double NOT NULL,
  `cod_currency` varchar(255) NOT NULL,
  `cod_transaction_id` varchar(255) NOT NULL,
  `com_percentAmount` double NOT NULL,
  `exchange_rate` double NOT NULL,
  `exchange_currency` varchar(255) NOT NULL,
  `com_status` enum('0','1') NOT NULL COMMENT '1 => paid, 0=> Hold',
  `com_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_color`
--

CREATE TABLE `nm_color` (
  `co_id` smallint(5) UNSIGNED NOT NULL,
  `co_code` varchar(10) NOT NULL,
  `co_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_contact`
--

CREATE TABLE `nm_contact` (
  `cont_id` int(10) UNSIGNED NOT NULL,
  `cont_name` varchar(100) NOT NULL,
  `cont_email` varchar(150) NOT NULL,
  `cont_no` varchar(50) NOT NULL,
  `cont_message` text NOT NULL,
  `cont_restatus` tinyint(4) NOT NULL,
  `cont_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_country`
--

CREATE TABLE `nm_country` (
  `co_id` smallint(5) UNSIGNED NOT NULL,
  `co_code` varchar(10) NOT NULL,
  `co_name` varchar(30) NOT NULL,
  `co_name_fr` varchar(150) NOT NULL,
  `co_cursymbol` varchar(5) NOT NULL,
  `co_curcode` varchar(10) NOT NULL,
  `co_status` tinyint(4) NOT NULL COMMENT '1-block,0-unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_country`
--

INSERT INTO `nm_country` (`co_id`, `co_code`, `co_name`, `co_name_fr`, `co_cursymbol`, `co_curcode`, `co_status`) VALUES
(1, 'IND', 'India', 'Inde', 'Rs', 'INR', 0),
(5, 'IR', 'Iran', '', 'Rls', 'IRR', 1),
(6, 'CZ', 'czech republic', '', 'K?', 'CZK', 1),
(7, 'USA', 'United States of America', '', '$', 'USD', 0),
(8, 'FR', 'France', '', '€', 'EUR', 0),
(9, 'PK', 'Pakistan', '', 'Rs.', 'PKR', 1),
(10, 'KSA', 'Saudi Arabia', '', '????', 'SAR', 0),
(11, 'PL', 'Poland', '', 'PLN', 'PLN', 0),
(12, 'DE', 'Germany', '', 'EUR', 'EUR', 0),
(13, 'USD', 'USA', '', '$', 'USD', 0),
(14, 'EU', 'Europe', '', '€', 'EUR', 0),
(15, ' IT', 'Italy', 'tests', '€', 'EUR', 0),
(16, ' DZ', 'Algeria', '', '?.?', 'DZD', 0),
(17, ' AO', 'Angola', '', 'Kz', 'AOA', 0),
(18, ' AF', 'Afghanistan', '', '?', 'AFN', 0),
(19, ' KI', 'Kiribati', '', '$', 'AUD', 0),
(20, ' KH', 'Cambodia', '', '?', 'KHR', 0),
(21, ' CU', 'Cuba', '', '$', 'CUC', 0),
(22, ' AL', 'Albania', '', 'L', 'ALL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_coupon`
--

CREATE TABLE `nm_coupon` (
  `id` int(255) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_name` varchar(255) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=>flat, 2=>percentage',
  `value` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1',
  `type_of_coupon` varchar(100) NOT NULL COMMENT 'product coupon->1, user coupon->2',
  `terms` longtext NOT NULL,
  `coupon_per_product` varchar(155) NOT NULL,
  `coupon_per_user` varchar(155) NOT NULL,
  `tot_cart_val` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_coupon_purchage`
--

CREATE TABLE `nm_coupon_purchage` (
  `id` int(100) NOT NULL,
  `coupon_id` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `sold_user` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `pro_qty` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type_of_coupon` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_currency`
--

CREATE TABLE `nm_currency` (
  `cur_id` int(10) UNSIGNED NOT NULL,
  `cur_name` varchar(100) NOT NULL,
  `cur_code` varchar(5) NOT NULL,
  `cur_symbol` varchar(10) NOT NULL,
  `cur_status` tinyint(11) NOT NULL DEFAULT '1',
  `cur_default` tinyint(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_customer`
--

CREATE TABLE `nm_customer` (
  `cus_id` bigint(10) UNSIGNED NOT NULL,
  `cus_name` varchar(100) NOT NULL,
  `facebook_id` varchar(150) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `cus_email` varchar(150) NOT NULL,
  `cus_pwd` varchar(40) NOT NULL,
  `cus_phone` varchar(20) NOT NULL,
  `cus_address1` varchar(150) NOT NULL,
  `cus_address2` varchar(150) NOT NULL,
  `cus_country` varchar(50) NOT NULL,
  `cus_city` varchar(50) NOT NULL,
  `cus_joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cus_logintype` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>Admin user, 2=> Website User, 3=> Facebook User, 4=>Google+ User',
  `cus_status` int(11) NOT NULL COMMENT '0 unblock 1 block',
  `cus_pic` varchar(150) NOT NULL,
  `created_date` date NOT NULL,
  `wallet` bigint(100) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address1` varchar(100) NOT NULL,
  `ship_address2` varchar(100) NOT NULL,
  `ship_ci_id` int(11) NOT NULL,
  `ship_state` varchar(100) NOT NULL,
  `ship_country` int(11) NOT NULL,
  `ship_postalcode` varchar(30) NOT NULL,
  `ship_phone` varchar(25) NOT NULL,
  `ship_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_deals`
--

CREATE TABLE `nm_deals` (
  `deal_id` int(11) NOT NULL,
  `deal_title` varchar(500) NOT NULL,
  `deal_title_fr` varchar(250) NOT NULL,
  `deal_category` int(11) NOT NULL,
  `deal_main_category` int(11) NOT NULL,
  `deal_sub_category` int(11) NOT NULL,
  `deal_second_sub_category` int(11) NOT NULL,
  `deal_original_price` int(11) NOT NULL,
  `deal_discount_price` int(11) NOT NULL,
  `deal_discount_percentage` int(11) NOT NULL,
  `deal_saving_price` int(11) NOT NULL,
  `deal_inctax` varchar(10) NOT NULL DEFAULT '0',
  `deal_shippamt` varchar(10) NOT NULL,
  `deal_start_date` datetime NOT NULL,
  `deal_end_date` datetime NOT NULL,
  `deal_expiry_date` date NOT NULL,
  `deal_description` text NOT NULL,
  `deal_description_fr` text NOT NULL,
  `deal_merchant_id` int(11) NOT NULL,
  `deal_shop_id` int(11) NOT NULL,
  `deal_meta_keyword` varchar(250) NOT NULL,
  `deal_meta_keyword_fr` varchar(250) NOT NULL,
  `deal_meta_description` varchar(500) NOT NULL,
  `deal_meta_description_fr` varchar(500) NOT NULL,
  `deal_min_limit` int(11) NOT NULL,
  `deal_max_limit` int(11) NOT NULL,
  `deal_purchase_limit` int(11) NOT NULL,
  `deal_image_count` int(11) NOT NULL,
  `deal_image` varchar(500) NOT NULL,
  `deal_no_of_purchase` int(11) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `deal_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `deal_posted_date` datetime NOT NULL,
  `deal_delivery` int(11) NOT NULL,
  `allow_cancel` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_return` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_replace` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `cancel_policy` text NOT NULL,
  `cancel_policy_fr` text NOT NULL,
  `return_policy` text NOT NULL,
  `return_policy_fr` text NOT NULL,
  `replace_policy` text NOT NULL,
  `replace_policy_fr` text NOT NULL,
  `cancel_days` int(11) NOT NULL,
  `return_days` int(11) NOT NULL,
  `replace_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_emailsetting`
--

CREATE TABLE `nm_emailsetting` (
  `es_id` tinyint(3) UNSIGNED NOT NULL,
  `es_contactname` varchar(150) NOT NULL,
  `es_contactemail` varchar(150) NOT NULL,
  `es_skype_email_id` varchar(500) NOT NULL,
  `es_webmasteremail` varchar(150) NOT NULL,
  `es_noreplyemail` varchar(150) NOT NULL,
  `es_phone1` varchar(20) NOT NULL,
  `es_phone2` varchar(20) NOT NULL,
  `es_latitude` decimal(18,14) NOT NULL,
  `es_longitude` decimal(18,14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_emailsetting`
--

INSERT INTO `nm_emailsetting` (`es_id`, `es_contactname`, `es_contactemail`, `es_skype_email_id`, `es_webmasteremail`, `es_noreplyemail`, `es_phone1`, `es_phone2`, `es_latitude`, `es_longitude`) VALUES
(1, 'Laravel Ecommerce', 'sales@laravelecommerce.com', 'sales@laravelecommerce.com', 'sales@laravelecommerce.com', 'sales@laravelecommerce.com', '+919790153111', '+1 (972) 591 8222', '7.98591014355452', '80.73921674804683');

-- --------------------------------------------------------

--
-- Table structure for table `nm_enquiry`
--

CREATE TABLE `nm_enquiry` (
  `id` int(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(50) NOT NULL,
  `created_date` varchar(255) NOT NULL,
  `enq_readstatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_estimate_zipcode`
--

CREATE TABLE `nm_estimate_zipcode` (
  `ez_id` int(11) NOT NULL,
  `ez_code_series` int(11) NOT NULL,
  `ez_code_series_end` int(11) NOT NULL,
  `ez_code_days` int(11) NOT NULL,
  `ez_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_estimate_zipcode`
--

INSERT INTO `nm_estimate_zipcode` (`ez_id`, `ez_code_series`, `ez_code_series_end`, `ez_code_days`, `ez_status`) VALUES
(1, 641001, 641051, 2, 1),
(2, 642001, 642001, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_faq`
--

CREATE TABLE `nm_faq` (
  `faq_id` smallint(5) UNSIGNED NOT NULL,
  `faq_name` varchar(256) NOT NULL,
  `faq_name_fr` varchar(250) NOT NULL,
  `faq_ans` text NOT NULL,
  `faq_ans_fr` text NOT NULL,
  `faq_status` tinyint(4) NOT NULL COMMENT '0=>unblock,1=>block'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_generalsetting`
--

CREATE TABLE `nm_generalsetting` (
  `gs_id` tinyint(4) NOT NULL,
  `gs_sitename` varchar(100) NOT NULL,
  `gs_sitename_fr` varchar(200) NOT NULL,
  `gs_sitedescription` varchar(200) NOT NULL,
  `gs_sitedescription_fr` varchar(200) NOT NULL,
  `gs_metatitle` varchar(150) NOT NULL,
  `gs_metatitle_fr` varchar(150) NOT NULL,
  `gs_metakeywords` text NOT NULL,
  `gs_metakeywords_fr` text NOT NULL,
  `gs_metadesc` text NOT NULL,
  `gs_metadesc_fr` text NOT NULL,
  `gs_defaulttheme` tinyint(3) UNSIGNED NOT NULL,
  `gs_defaultlanguage` tinyint(3) UNSIGNED NOT NULL,
  `gs_payment_status` varchar(50) NOT NULL,
  `gs_paypal_payment` varchar(100) NOT NULL,
  `gs_payumoney_status` varchar(100) NOT NULL,
  `gs_themes` varchar(20) NOT NULL,
  `gs_playstore_url` varchar(500) NOT NULL,
  `gs_apple_appstore_url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_generalsetting`
--

INSERT INTO `nm_generalsetting` (`gs_id`, `gs_sitename`, `gs_sitename_fr`, `gs_sitedescription`, `gs_sitedescription_fr`, `gs_metatitle`, `gs_metatitle_fr`, `gs_metakeywords`, `gs_metakeywords_fr`, `gs_metadesc`, `gs_metadesc_fr`, `gs_defaulttheme`, `gs_defaultlanguage`, `gs_payment_status`, `gs_paypal_payment`, `gs_payumoney_status`, `gs_themes`, `gs_playstore_url`, `gs_apple_appstore_url`) VALUES
(1, 'Laravel Ecommerce', 'Laravel Ecommerce french', 'Developed in Native Android & iOS Platform. Well Written Structured Code. Our App is user friendly and gives ample opportunities for guests and hosts ', '', 'testlaravel testlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestla', 'Laravel Ecommerce', 'testlaravel testlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaravel', 'Laravel Ecommerce', 'testlaravel testlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaraveltestlaravel', 'Laravel Ecommerce', 1, 1, 'COD', 'Paypal', 'PayUmoney', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nm_imagesetting`
--

CREATE TABLE `nm_imagesetting` (
  `imgs_id` smallint(6) NOT NULL,
  `imgs_name` varchar(150) NOT NULL,
  `imgs_type` tinyint(4) NOT NULL COMMENT '1- logo,2 -Favicon,3-noimage,4-product,5-deal,6-sores,7-blog_banner,8-upload_banner,9-category,10-ads_blog_image,''11''=>''category'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_imagesetting`
--

INSERT INTO `nm_imagesetting` (`imgs_id`, `imgs_name`, `imgs_type`) VALUES
(1, 'Logo_1521616186_png', 1),
(2, 'Favicon_1517904843_fav_Df.png', 2),
(3, 'No_image_No_image_1509364387_381x215.jpg', 3),
(4, 'No_image_1522327122_800x800', 4),
(5, 'No_image_1509364387_800x800.png', 5),
(6, 'No_image_1509364387_455x378.png', 6),
(7, 'No_image_1509364387_320x190.png', 7),
(8, 'No_image_1509364387_870x350.png', 8),
(9, 'No_image_1522661876_250x200.jpg', 9),
(10, 'No_image_1522660971_800x400', 10),
(11, 'No_image_1522661659_200x200.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `nm_image_sizes`
--

CREATE TABLE `nm_image_sizes` (
  `image_size_id` int(11) NOT NULL,
  `image_size` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_image_sizes`
--

INSERT INTO `nm_image_sizes` (`image_size_id`, `image_size`) VALUES
(1, '{\"product\":{\"width\":800,\"height\":800},\"deals\":{\"width\":800,\"height\":800},\"logo\":{\"width\":180,\"height\":45},\"favicon\":{\"width\":16,\"height\":16},\"no_image\":{\"width\":381,\"height\":215},\"category_advertisment\":{\"width\":170,\"height\":400},\"category_banner\":{\"width\":250,\"height\":200},\"top_category\":{\"width\":200,\"height\":200},\"sub_category\":{\"width\":200,\"height\":200},\"sec_sub_category\":{\"width\":200,\"height\":200},\"ads\":{\"width\":800,\"height\":400},\"store\":{\"width\":455,\"height\":378},\"blog\":{\"width\":320,\"height\":190},\"no_image_banner\":{\"width\":845,\"height\":500}}');

-- --------------------------------------------------------

--
-- Table structure for table `nm_inquiries`
--

CREATE TABLE `nm_inquiries` (
  `iq_id` int(10) UNSIGNED NOT NULL,
  `iq_name` varchar(100) NOT NULL,
  `iq_emailid` varchar(150) NOT NULL,
  `iq_phonenumber` varchar(20) NOT NULL,
  `iq_message` varchar(300) NOT NULL,
  `inq_readstatus` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_language`
--

CREATE TABLE `nm_language` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `lang_name` varchar(30) NOT NULL,
  `lang_status` int(4) NOT NULL COMMENT '1->Active,2->deactive',
  `lang_default` int(11) NOT NULL COMMENT '1->default,',
  `pack_lang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_language`
--

INSERT INTO `nm_language` (`lang_id`, `lang_code`, `lang_name`, `lang_status`, `lang_default`, `pack_lang`) VALUES
(1, 'en', 'English', 1, 1, 1),
(3, 'fr', 'French', 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_login`
--

CREATE TABLE `nm_login` (
  `log_id` int(5) NOT NULL,
  `cus_id` int(5) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-wesite,2 facebook'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_maincategory`
--

CREATE TABLE `nm_maincategory` (
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_name` varchar(100) NOT NULL,
  `mc_name_fr` varchar(100) NOT NULL,
  `mc_type` varchar(10) NOT NULL,
  `mc_img` varchar(150) NOT NULL,
  `mc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_merchant`
--

CREATE TABLE `nm_merchant` (
  `mer_id` int(10) UNSIGNED NOT NULL,
  `addedby` varchar(20) NOT NULL COMMENT '0=>admin',
  `mer_fname` varchar(150) NOT NULL,
  `mer_lname` varchar(150) NOT NULL,
  `mer_password` varchar(150) NOT NULL,
  `mer_email` varchar(150) NOT NULL,
  `mer_phone` varchar(20) NOT NULL,
  `mer_address1` varchar(150) NOT NULL,
  `mer_address2` varchar(150) NOT NULL,
  `mer_ci_id` int(10) UNSIGNED NOT NULL COMMENT 'city id',
  `mer_co_id` smallint(5) UNSIGNED NOT NULL COMMENT 'country id',
  `mer_payment` varchar(100) NOT NULL,
  `mer_payu_key` varchar(250) NOT NULL,
  `mer_payu_salt` varchar(250) NOT NULL,
  `mer_commission` tinyint(4) NOT NULL,
  `created_date` date NOT NULL,
  `mer_staus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-unblock,0-block',
  `mer_pro_status` int(11) NOT NULL COMMENT '1=>unblock, 0=>block',
  `mer_country_status` char(1) NOT NULL DEFAULT 'A',
  `mer_logintype` varchar(30) NOT NULL DEFAULT '1' COMMENT '1=>Admin user, 2=> Website User, 3=> Facebook User'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_merchant_overallorders`
--

CREATE TABLE `nm_merchant_overallorders` (
  `overOrd_id` int(11) NOT NULL,
  `over_mer_id` int(11) NOT NULL,
  `over_tot_ord_amt` decimal(10,2) NOT NULL,
  `over_tot_offline_amt` decimal(10,2) NOT NULL,
  `over_tot_online_amt` decimal(10,2) NOT NULL,
  `over_tot_coupon_amt` decimal(10,2) NOT NULL,
  `over_tot_wallet_amt` decimal(10,2) NOT NULL,
  `commissionAmt` decimal(10,2) NOT NULL,
  `merchantAmt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_modulesettings`
--

CREATE TABLE `nm_modulesettings` (
  `ms_id` int(11) NOT NULL,
  `ms_dealmodule` int(11) NOT NULL,
  `ms_productmodule` int(11) NOT NULL,
  `ms_auctionmodule` int(11) NOT NULL,
  `ms_blogmodule` int(11) NOT NULL,
  `ms_nearmemapmodule` int(11) NOT NULL,
  `ms_storelistmodule` int(11) NOT NULL,
  `ms_pastdealmodule` int(11) NOT NULL,
  `ms_faqmodule` int(11) NOT NULL,
  `ms_cod` int(11) NOT NULL,
  `ms_paypal` int(11) NOT NULL,
  `ms_creditcard` int(11) NOT NULL,
  `ms_googlecheckout` int(11) NOT NULL,
  `ms_shipping` int(11) NOT NULL COMMENT '1=>Free shipping, 2=> Flat shipping, 3=> Product per shippin, 4=> Per Item shipping',
  `ms_newsletter_template` int(11) NOT NULL COMMENT '1=> Temp 1, 2=>Temp 2, 3=> Temp 3, 4=> Temp 4',
  `ms_citysettings` int(11) NOT NULL COMMENT '1=> With city, 0=> Without city'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_newsletter_subscribers`
--

CREATE TABLE `nm_newsletter_subscribers` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `post_dt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order`
--

CREATE TABLE `nm_order` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_cus_id` int(10) UNSIGNED NOT NULL,
  `order_pro_id` int(11) UNSIGNED NOT NULL,
  `order_prod_unitPrice` double NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `transaction_id` varchar(50) NOT NULL,
  `payer_email` varchar(50) NOT NULL,
  `payer_id` varchar(50) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_amt` decimal(15,2) NOT NULL COMMENT '(unit price * quantity)',
  `order_tax` decimal(10,2) NOT NULL COMMENT 'tax per unit (in %)',
  `order_taxAmt` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `token_id` varchar(30) NOT NULL,
  `payment_ack` varchar(10) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` varchar(20) NOT NULL,
  `payer_status` varchar(50) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL DEFAULT '2' COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `order_paytype` smallint(6) NOT NULL DEFAULT '1' COMMENT '1-paypal',
  `order_pro_color` int(11) NOT NULL,
  `order_pro_size` int(11) NOT NULL,
  `order_shipping_amt` varchar(20) NOT NULL,
  `order_shipping_add` text NOT NULL,
  `order_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(255) NOT NULL,
  `coupon_total_amount` varchar(255) NOT NULL,
  `wallet_amount` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_ordercod`
--

CREATE TABLE `nm_ordercod` (
  `cod_id` int(10) UNSIGNED NOT NULL,
  `cod_transaction_id` varchar(60) NOT NULL,
  `cod_cus_id` int(10) UNSIGNED NOT NULL,
  `cod_pro_id` int(11) UNSIGNED NOT NULL,
  `cod_prod_unitPrice` double NOT NULL DEFAULT '0',
  `cod_order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `cod_qty` int(11) NOT NULL,
  `cod_amt` decimal(10,2) NOT NULL COMMENT '(unit_price*quantity)',
  `cod_tax` decimal(10,2) NOT NULL COMMENT 'per_product_tax (in %)',
  `cod_taxAmt` decimal(10,2) NOT NULL,
  `cod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cod_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `created_date` varchar(20) NOT NULL,
  `cod_paytype` smallint(6) NOT NULL COMMENT '0=>cash on delivery',
  `cod_pro_color` tinyint(4) NOT NULL,
  `cod_pro_size` tinyint(4) NOT NULL,
  `cod_shipping_amt` varchar(20) NOT NULL,
  `cod_ship_addr` text NOT NULL,
  `cod_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(100) NOT NULL,
  `coupon_total_amount` varchar(100) NOT NULL,
  `wallet_amount` double NOT NULL,
  `cod_prod_actualprice` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_ordercod_wallet`
--

CREATE TABLE `nm_ordercod_wallet` (
  `cod_transaction_id` varchar(255) NOT NULL,
  `wallet_used` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_auction`
--

CREATE TABLE `nm_order_auction` (
  `oa_id` int(11) NOT NULL,
  `oa_pro_id` int(11) NOT NULL,
  `oa_cus_id` int(11) NOT NULL,
  `oa_cus_name` varchar(150) NOT NULL,
  `oa_cus_email` varchar(250) NOT NULL,
  `oa_cus_address` text NOT NULL,
  `oa_bid_amt` int(11) NOT NULL,
  `oa_bid_shipping_amt` int(11) NOT NULL,
  `oa_original_bit_amt` int(11) NOT NULL,
  `oa_bid_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `oa_bid_winner` int(11) NOT NULL COMMENT '1=> Winner, 0=> Bidders',
  `oa_bid_item_status` int(11) NOT NULL COMMENT '0=> Onprocess, 1=> Send,  3=>Cancelled',
  `oa_delivery_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_delivery_status`
--

CREATE TABLE `nm_order_delivery_status` (
  `delStatus_id` int(11) NOT NULL,
  `order_cust_id` int(11) NOT NULL,
  `cod_order_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `mer_id` int(11) NOT NULL,
  `order_type` int(11) NOT NULL COMMENT '''1''-product,''2''-deal',
  `transaction_id` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL COMMENT '''0''->cod,''1''->paypal',
  `delivery_statuss` int(11) NOT NULL,
  `cancel_status` int(11) NOT NULL COMMENT '''0''->not done,''1''->cancel pending ,''2''->cancelled,''3''->hold,4->"Disapproved"',
  `cancel_notes` text NOT NULL,
  `cancel_date` datetime NOT NULL,
  `cancel_approved_date` datetime NOT NULL,
  `return_status` int(11) NOT NULL COMMENT '''0''-not done,''1''-return pending,''3''->hold,''2''-returned,4->"Disapproved"',
  `return_notes` text NOT NULL,
  `return_date` datetime NOT NULL,
  `return_approved_date` datetime NOT NULL,
  `replace_status` int(11) NOT NULL COMMENT '''0''-not done,''1''-replace pending,''3''->hold,''2''-replaced,4->"Disapproved"',
  `replace_notes` text NOT NULL,
  `replace_date` datetime NOT NULL,
  `replace_approved_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_payu`
--

CREATE TABLE `nm_order_payu` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_cus_id` int(10) UNSIGNED NOT NULL,
  `order_pro_id` int(11) UNSIGNED NOT NULL,
  `order_prod_unitPrice` double NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `transaction_id` varchar(50) NOT NULL,
  `payer_email` varchar(50) NOT NULL,
  `payer_id` varchar(50) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_amt` decimal(15,2) NOT NULL COMMENT '(unit price * quantity)',
  `order_tax` decimal(10,2) NOT NULL COMMENT 'tax per unit (in %)',
  `order_taxAmt` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `token_id` varchar(30) NOT NULL,
  `payment_ack` varchar(10) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` varchar(20) NOT NULL,
  `payer_status` varchar(50) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL DEFAULT '2' COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `order_paytype` smallint(6) NOT NULL DEFAULT '1' COMMENT '1-paypal',
  `order_pro_color` int(11) NOT NULL,
  `order_pro_size` int(11) NOT NULL,
  `order_shipping_amt` varchar(20) NOT NULL,
  `order_shipping_add` text NOT NULL,
  `order_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(255) NOT NULL,
  `coupon_total_amount` varchar(255) NOT NULL,
  `wallet_amount` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_paymentsettings`
--

CREATE TABLE `nm_paymentsettings` (
  `ps_id` tinyint(3) UNSIGNED NOT NULL,
  `ps_flatshipping` decimal(10,2) NOT NULL COMMENT 'shipping Tax Percentage',
  `ps_taxpercentage` tinyint(3) UNSIGNED NOT NULL,
  `ps_extenddays` smallint(5) UNSIGNED NOT NULL COMMENT 'Auction Extend Days',
  `ps_alertdays` int(11) NOT NULL,
  `ps_minfundrequest` int(10) UNSIGNED NOT NULL,
  `ps_maxfundrequest` int(10) UNSIGNED NOT NULL,
  `ps_referralamount` int(11) NOT NULL,
  `ps_countryid` int(11) NOT NULL,
  `ps_countrycode` varchar(10) NOT NULL,
  `ps_cursymbol` varchar(10) NOT NULL,
  `ps_curcode` varchar(10) NOT NULL,
  `ps_paypal_email` varchar(255) NOT NULL,
  `ps_paypalaccount` varchar(150) NOT NULL,
  `ps_paypal_api_pw` varchar(250) NOT NULL,
  `ps_paypal_api_signature` varchar(250) NOT NULL,
  `ps_authorize_trans_key` varchar(250) NOT NULL,
  `ps_authorize_api_id` varchar(250) NOT NULL,
  `ps_payu_key` varchar(250) NOT NULL,
  `ps_payu_salt` varchar(250) NOT NULL,
  `ps_paypal_pay_mode` tinyint(4) NOT NULL COMMENT '0->Test Mode, 1-> Live Mode'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_paymentsettings`
--

INSERT INTO `nm_paymentsettings` (`ps_id`, `ps_flatshipping`, `ps_taxpercentage`, `ps_extenddays`, `ps_alertdays`, `ps_minfundrequest`, `ps_maxfundrequest`, `ps_referralamount`, `ps_countryid`, `ps_countrycode`, `ps_cursymbol`, `ps_curcode`, `ps_paypal_email`, `ps_paypalaccount`, `ps_paypal_api_pw`, `ps_paypal_api_signature`, `ps_authorize_trans_key`, `ps_authorize_api_id`, `ps_payu_key`, `ps_payu_salt`, `ps_paypal_pay_mode`) VALUES
(1, '0.00', 0, 0, 0, 0, 0, 0, 1, 'IND', 'Rs', 'INR', 'venugopal-buyer@pofitec.com', 'venugopal-facilitator_api1.pofitec.com', 'U5BL5KK3ZUZVJRNL', 'AhEqPBa2LPCE3sKdenmfssNtAsh0AF4qYwXaeb9bpLQiA-T83dJ-0KHq', '', '', 'gtKFFx', 'eCwWELxi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_procart`
--

CREATE TABLE `nm_procart` (
  `pc_id` int(10) UNSIGNED NOT NULL,
  `pc_date` datetime NOT NULL,
  `pc_pro_id` int(11) NOT NULL,
  `pc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_procolor`
--

CREATE TABLE `nm_procolor` (
  `pc_id` bigint(20) UNSIGNED NOT NULL,
  `pc_pro_id` int(10) UNSIGNED NOT NULL,
  `pc_co_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_prodelpolicy`
--

CREATE TABLE `nm_prodelpolicy` (
  `pdp_id` bigint(20) UNSIGNED NOT NULL,
  `pdp_pro_id` smallint(5) UNSIGNED NOT NULL,
  `pdp_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_product`
--

CREATE TABLE `nm_product` (
  `pro_no_of_purchase` int(11) NOT NULL,
  `pro_id` int(10) UNSIGNED NOT NULL,
  `pro_title` varchar(150) NOT NULL,
  `pro_title_fr` varchar(150) NOT NULL,
  `pro_mc_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pro_smc_id` smallint(5) UNSIGNED NOT NULL,
  `pro_sb_id` smallint(5) UNSIGNED NOT NULL,
  `pro_ssb_id` smallint(5) UNSIGNED NOT NULL,
  `product_brand_id` int(11) NOT NULL,
  `pro_price` decimal(15,2) NOT NULL,
  `pro_disprice` decimal(15,2) NOT NULL,
  `pro_discount_percentage` varchar(11) NOT NULL,
  `pro_inctax` tinyint(4) NOT NULL,
  `pro_shippamt` decimal(15,2) NOT NULL,
  `pro_desc` longtext NOT NULL,
  `pro_desc_fr` longtext NOT NULL,
  `pro_isspec` tinyint(4) NOT NULL COMMENT '1-yes 2-no',
  `pro_is_size` int(11) NOT NULL COMMENT '0=>yes, 1=>no',
  `pro_is_color` int(11) NOT NULL COMMENT '0=>yes, 1=>no',
  `pro_delivery` smallint(5) UNSIGNED NOT NULL COMMENT 'in days',
  `pro_mr_id` int(10) UNSIGNED NOT NULL,
  `pro_sh_id` smallint(5) UNSIGNED NOT NULL,
  `pro_mkeywords` text NOT NULL,
  `pro_mkeywords_fr` text NOT NULL,
  `pro_mdesc` text NOT NULL COMMENT 'metadescription',
  `pro_mdesc_fr` text NOT NULL,
  `pro_postfacebook` tinyint(4) NOT NULL,
  `pro_Img` varchar(500) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `pro_status` tinyint(4) NOT NULL COMMENT '2=>Delete ,1=> Active, 0 => Block',
  `pro_image_count` int(11) NOT NULL,
  `pro_qty` int(11) NOT NULL,
  `hit_count` int(11) NOT NULL DEFAULT '0',
  `sold_status` int(11) NOT NULL DEFAULT '1',
  `cash_pack` varchar(255) NOT NULL,
  `allow_cancel` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_return` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_replace` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `cancel_policy` text NOT NULL,
  `cancel_policy_fr` text NOT NULL,
  `return_policy` text NOT NULL,
  `return_policy_fr` text NOT NULL,
  `replace_policy` text NOT NULL,
  `replace_policy_fr` text NOT NULL,
  `cancel_days` int(11) NOT NULL,
  `return_days` int(11) NOT NULL,
  `replace_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_prosize`
--

CREATE TABLE `nm_prosize` (
  `ps_id` bigint(20) UNSIGNED NOT NULL,
  `ps_pro_id` int(10) UNSIGNED NOT NULL,
  `ps_si_id` smallint(5) UNSIGNED NOT NULL,
  `ps_volume` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_prospec`
--

CREATE TABLE `nm_prospec` (
  `spc_id` bigint(20) UNSIGNED NOT NULL,
  `spc_pro_id` int(10) UNSIGNED NOT NULL,
  `spc_spg_id` int(11) NOT NULL,
  `spc_sp_id` smallint(5) UNSIGNED NOT NULL,
  `spc_value` text NOT NULL,
  `spc_value_fr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_referaluser`
--

CREATE TABLE `nm_referaluser` (
  `ruse_id` int(10) UNSIGNED NOT NULL,
  `ruse_date` datetime NOT NULL,
  `ruse_name` varchar(100) NOT NULL,
  `ruse_emailid` varchar(150) NOT NULL,
  `ruse_userid` int(10) UNSIGNED NOT NULL,
  `ruse_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_review`
--

CREATE TABLE `nm_review` (
  `comment_id` int(50) NOT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `deal_id` varchar(255) DEFAULT NULL,
  `store_id` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `ratings` varchar(255) NOT NULL,
  `status` int(50) NOT NULL,
  `review_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sam1` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_save_cart`
--

CREATE TABLE `nm_save_cart` (
  `cart_id` int(11) NOT NULL,
  `cart_product_id` int(11) NOT NULL,
  `cart_deal_id` int(11) NOT NULL,
  `cart_product_qty` int(11) NOT NULL,
  `cart_type` int(1) NOT NULL COMMENT '1-Products,2-Deals',
  `cart_pro_siz_id` int(11) NOT NULL,
  `cart_pro_col_id` int(11) NOT NULL,
  `cart_user_id` int(11) NOT NULL,
  `addedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_secmaincategory`
--

CREATE TABLE `nm_secmaincategory` (
  `smc_id` smallint(5) UNSIGNED NOT NULL,
  `smc_name` varchar(100) NOT NULL,
  `smc_name_fr` varchar(100) NOT NULL,
  `smc_mc_id` smallint(5) UNSIGNED NOT NULL,
  `smc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_secsubcategory`
--

CREATE TABLE `nm_secsubcategory` (
  `ssb_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_name` varchar(100) NOT NULL,
  `ssb_name_fr` varchar(100) NOT NULL,
  `ssb_sb_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_smc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_shipping`
--

CREATE TABLE `nm_shipping` (
  `ship_id` int(10) UNSIGNED NOT NULL,
  `ship_name` varchar(100) NOT NULL,
  `ship_address1` varchar(200) NOT NULL,
  `ship_address2` varchar(200) NOT NULL,
  `ship_ci_id` varchar(50) NOT NULL,
  `ship_state` varchar(100) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_postalcode` varchar(20) NOT NULL,
  `ship_phone` varchar(20) NOT NULL,
  `ship_email` varchar(255) NOT NULL,
  `ship_order_id` varchar(50) NOT NULL,
  `ship_trans_id` varchar(50) NOT NULL,
  `ship_cus_id` bigint(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_size`
--

CREATE TABLE `nm_size` (
  `si_id` smallint(5) UNSIGNED NOT NULL,
  `si_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_smtp`
--

CREATE TABLE `nm_smtp` (
  `sm_id` tinyint(4) NOT NULL,
  `sm_host` varchar(150) NOT NULL,
  `sm_port` varchar(20) NOT NULL,
  `sm_uname` varchar(30) NOT NULL,
  `sm_pwd` varchar(30) NOT NULL,
  `sm_isactive` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_smtp`
--

INSERT INTO `nm_smtp` (`sm_id`, `sm_host`, `sm_port`, `sm_uname`, `sm_pwd`, `sm_isactive`) VALUES
(1, 'smtp.gmail.com', '587', 'kailashkumar.r@pofitec.com', '1234567890', 1),
(2, 'Send Grid', '149', 'send grid', 'sendgrid', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_social_media`
--

CREATE TABLE `nm_social_media` (
  `sm_id` int(11) NOT NULL,
  `sm_fb_app_id` varchar(100) NOT NULL,
  `sm_fb_sec_key` varchar(100) NOT NULL,
  `sm_fb_page_url` varchar(250) NOT NULL,
  `sm_fb_like_page_url` varchar(250) NOT NULL,
  `sm_twitter_url` varchar(250) NOT NULL,
  `sm_twitter_app_id` varchar(250) NOT NULL,
  `sm_twitter_sec_key` varchar(250) NOT NULL,
  `sm_linkedin_url` varchar(250) NOT NULL,
  `sm_youtube_url` varchar(250) NOT NULL,
  `sm_gmap_app_key` varchar(250) NOT NULL,
  `sm_android_page_url` varchar(250) NOT NULL,
  `sm_iphone_url` varchar(250) NOT NULL,
  `sm_analytics_code` text NOT NULL,
  `sm_gl_client_id` varchar(250) NOT NULL,
  `sm_gl_sec_key` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_social_media`
--

INSERT INTO `nm_social_media` (`sm_id`, `sm_fb_app_id`, `sm_fb_sec_key`, `sm_fb_page_url`, `sm_fb_like_page_url`, `sm_twitter_url`, `sm_twitter_app_id`, `sm_twitter_sec_key`, `sm_linkedin_url`, `sm_youtube_url`, `sm_gmap_app_key`, `sm_android_page_url`, `sm_iphone_url`, `sm_analytics_code`, `sm_gl_client_id`, `sm_gl_sec_key`) VALUES
(1, '108134949325990', 'c29a8a4e205b942082e44b5cf01df6d9', 'http://192.168.0.62/laravelecommerce_ver5.5/', 'https://www.facebook.com', 'https://www.facebook.com', 'dsf1dsfsd232d1f21dsf21ds2f1dsf', 'sd2f1sd2f13sfgsd543df3ds1fds1f', 'https://www.facebook.com', 'https://www.facebook.com', 'AIzaSyCsDoY1OPjAqu1PlQhH3UljYsfw-81bLkI', '', '', '<script>\r\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');\r\n\r\n  ga(\'create\', \'UA-62671250-4\', \'auto\');\r\n  ga(\'send\', \'pageview\');\r\n</script>', '782885230420-rbpe9m9044krsto1dqchhr3p6am81ggh.apps.googleusercontent.com', 'O40k7cbT7lnLaESYQ0npsY5c');

-- --------------------------------------------------------

--
-- Table structure for table `nm_specification`
--

CREATE TABLE `nm_specification` (
  `sp_id` smallint(5) UNSIGNED NOT NULL,
  `sp_name` text NOT NULL,
  `sp_name_fr` text NOT NULL,
  `sp_spg_id` smallint(5) UNSIGNED NOT NULL,
  `sp_order` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_spgroup`
--

CREATE TABLE `nm_spgroup` (
  `spg_id` smallint(5) UNSIGNED NOT NULL,
  `spg_name` varchar(150) NOT NULL,
  `spg_name_fr` varchar(500) NOT NULL,
  `spg_order` smallint(6) NOT NULL,
  `sp_mc_id` int(11) NOT NULL,
  `sp_smc_id` int(11) NOT NULL,
  `show_on_filter` enum('0','1') NOT NULL COMMENT '0:No;1:yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_store`
--

CREATE TABLE `nm_store` (
  `stor_id` int(10) UNSIGNED NOT NULL,
  `stor_merchant_id` int(10) NOT NULL,
  `stor_name` varchar(100) NOT NULL,
  `stor_name_fr` varchar(100) NOT NULL,
  `stor_phone` varchar(20) NOT NULL,
  `stor_address1` varchar(150) NOT NULL,
  `stor_address1_fr` varchar(200) NOT NULL,
  `stor_address2` varchar(150) NOT NULL,
  `stor_address2_fr` varchar(200) NOT NULL,
  `stor_country` smallint(5) UNSIGNED NOT NULL,
  `stor_city` int(10) UNSIGNED NOT NULL,
  `stor_zipcode` varchar(20) NOT NULL,
  `stor_metakeywords` text NOT NULL,
  `stor_metakeywords_fr` text NOT NULL,
  `stor_metadesc` text NOT NULL,
  `stor_metadesc_fr` text NOT NULL,
  `stor_website` text NOT NULL,
  `stor_latitude` decimal(18,14) NOT NULL,
  `stor_longitude` decimal(18,14) NOT NULL,
  `stor_img` varchar(200) NOT NULL,
  `stor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>unblock,0=>block',
  `created_date` varchar(20) NOT NULL,
  `stor_addedby` int(5) NOT NULL DEFAULT '1' COMMENT '1-admin,2 -merchant',
  `store_city_status` char(1) NOT NULL DEFAULT 'A',
  `promote_status` tinyint(4) NOT NULL,
  `stor_slogan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_subcategory`
--

CREATE TABLE `nm_subcategory` (
  `sb_id` smallint(5) UNSIGNED NOT NULL,
  `sb_name` varchar(100) NOT NULL,
  `sb_name_fr` varchar(100) NOT NULL,
  `sc_img` varchar(250) NOT NULL,
  `sb_smc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `sb_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_subscription`
--

CREATE TABLE `nm_subscription` (
  `sub_id` int(10) UNSIGNED NOT NULL,
  `sub_cus_id` int(10) UNSIGNED NOT NULL COMMENT 'customer id',
  `sub_mc_id` smallint(5) UNSIGNED NOT NULL,
  `sub_status` tinyint(4) NOT NULL,
  `sub_readstatus` int(11) NOT NULL DEFAULT '0' COMMENT '-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_terms`
--

CREATE TABLE `nm_terms` (
  `tr_id` int(11) NOT NULL,
  `tr_description` text NOT NULL,
  `tr_description_fr` longtext NOT NULL,
  `tr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_terms`
--

INSERT INTO `nm_terms` (`tr_id`, `tr_description`, `tr_description_fr`, `tr_date`) VALUES
(1, '<b>Introduction</b><br>These terms and conditions govern your use of <a href=\"http://http://www.rapamber.com/\" target=\"\" rel=\"\">http://www.rapamber.com/</a>; by using this website, you accept these terms and conditions in full. If you disagree with these terms and conditions or any part of these terms and conditions, you must not use this website.<br>You must be at least 18 years of age to use this website. By using this website and by agreeing to these terms and conditions you warrant and represent that you are at least 18 years of age.<br>This website uses cookies. By using this website and agreeing to these terms and conditions, you consent to use of cookies in accordance with the terms of <a href=\"http://www.laravelecommerce.com\" target=\"\" rel=\"\">www.laravelecommerce.com</a><br>\r\n             <b>LICENSE TO USE WEBSITE</b><br>Unless otherwise stated, Laravel Ecommerce and/or its licensors own the intellectual property rights in the website and material on the website. Subject to the license below, all these intellectual property rights are reserved.<br>You may view, download for caching purposes only, and print pages [or [OTHER CONTENT]] from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms and conditions.<br><br>YOU MUST NOT:&nbsp;&nbsp; <br><ul><li>Sell, rent or sub license material from the website;</li><li>Show any material from the website in public;</li><li>Reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;</li><li>Edit or otherwise modify any material on the website;</li><li>Redistribute material from this website.</li></ul><b>ACCEPTABLE USE </b><ul><li>You must not use this website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.</li><li>You must not use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software.</li><li>You must not conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to this website without Laravel Ecommerce’s express written consent.</li><li>You must not use this website to transmit or send unsolicited commercial communications.</li><li>You must not use this website for any purposes related to marketing without Laravel Ecommerce’s express written consent.</li></ul><b>RESTRICTED ACCESS </b><br>[Access to certain areas of this website is restricted.] Laravel Ecommerce’s reserves the right to restrict access to [other] areas of this website, or indeed this entire website, at Laravel Ecommerce discretion.<br>If Laravel Ecommerce’s provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the user ID and password are kept confidential.<br>Laravel Ecommerce’s may disable your user ID and password in Laravel Ecommerce’s sole discretion without notice or explanation.]<br><br><b>USER CONTENT</b> <br>In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose.<br>You grant to Laravel Ecommerce a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media. You also grant to the right to sub-license these rights, and the right to bring an action for infringement of these rights.<br>Your user content must not be illegal or unlawful, must not infringe any third party’s legal rights, and must not be capable of giving rise to legal action whether against you or Laravel Ecommerce or a third party.<br>You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.<br>Laravel Ecommerce reserves the right to edit or remove any material submitted to this website, or stored on Laravel Ecommerce’s servers, or hosted or published upon this website. <br><br><b>LIMITATIONS OF LIABILITY</b> <br>Laravel Ecommerce will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:<br>For any indirect, special or consequential loss; or For any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.<br>These limitations of liability apply even if Laravel Ecommerce has been expressly advised of the potential loss.<br><br><b>REASONABLENESS</b> <br>By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable.<br>If you do not think they are reasonable, you must not use this website.<br><br><b>UNENFORCEABLE PROVISIONS<br></b>&nbsp;If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforceability of the other provisions of this website disclaimer.<br><br><b>BREACHES OF THESE TERMS AND CONDITIONS<br></b>&nbsp;Without prejudice to Laravel Ecommerce’s other rights under these terms and conditions, if you breach these terms and conditions in any way, Laravel Ecommerce may take such action as Laravel Ecommerce deems appropriate to deal with the breach, including suspending your access to this website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.<br><br><b>VARIATION<br></b>&nbsp;Laravel Ecommerce may revise these terms and conditions from time-to-time. Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website. Please check this page regularly to ensure you are familiar with the current version.<br><br><b>ASSIGNMENT<br></b>&nbsp;Laravel Ecommerce may transfer, sub-contract or otherwise deal with Laravel Ecommerce rights and/or obligations under these terms and conditions without notifying you or obtaining your consent.<br>You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms and conditions.<br><br><b>SEVERABILITY<br></b>&nbsp;If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect. If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.<br><br><b>LAW AND JURISDICTION<br></b>&nbsp;These terms and conditions will be governed by and construed in accordance with Indian law, and any disputes relating to these terms and conditions will be subject to the exclusive jurisdiction of the courts of Coimbatore.<br><br><br><br>', 'introduction\r\nCes termes et conditions régissent votre utilisation de http://www.rapamber.com/; En utilisant ce site, vous acceptez ces termes et conditions dans leur intégralité. Si vous n\'êtes pas d\'accord avec ces termes et conditions ou une partie de ces termes et conditions, vous ne devez pas utiliser ce site.\r\nVous devez avoir au moins 18 ans pour utiliser ce site. En utilisant ce site et en acceptant ces termes et conditions, vous garantissez et vous déclare que vous avez au moins 18 ans.\r\nCe site utilise des cookies. En utilisant ce site et en acceptant ces termes et conditions, vous consentez à utiliser des cookies conformément aux termes de www.laravelecommerce.com\r\nLICENCE D\'UTILISATION DU SITE WEB\r\nSauf indication contraire, Laravel Ecommerce et / ou ses concédants possèdent les droits de propriété intellectuelle sur le site et le matériel sur le site. Sous réserve de la licence ci-dessous, tous ces droits de propriété intellectuelle sont réservés.\r\nVous pouvez visualiser, télécharger uniquement à des fins de mise en cache et imprimer des pages [ou [AUTRES CONTENUS]] à partir du site Web pour votre propre usage, sous réserve des restrictions énoncées ci-dessous et ailleurs dans ces termes et conditions.', '2017-12-09 09:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `nm_theme`
--

CREATE TABLE `nm_theme` (
  `the_id` smallint(5) UNSIGNED NOT NULL,
  `the_Name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_wishlist`
--

CREATE TABLE `nm_wishlist` (
  `ws_id` bigint(20) UNSIGNED NOT NULL,
  `ws_pro_id` bigint(20) UNSIGNED NOT NULL,
  `ws_cus_id` bigint(20) UNSIGNED NOT NULL,
  `ws_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_withdraw_request`
--

CREATE TABLE `nm_withdraw_request` (
  `wd_id` int(11) NOT NULL,
  `wd_mer_id` int(11) NOT NULL,
  `wd_total_wd_amt` decimal(10,2) NOT NULL,
  `wd_submited_wd_amt` decimal(10,2) NOT NULL,
  `wd_status` int(11) NOT NULL COMMENT '1 => paid, 0=> Hold',
  `wd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_withdraw_request_paypal`
--

CREATE TABLE `nm_withdraw_request_paypal` (
  `wr_id` int(11) NOT NULL,
  `wr_mer_id` int(11) NOT NULL,
  `wr_mer_name` varchar(250) NOT NULL,
  `wr_mer_payment_email` varchar(250) NOT NULL,
  `wr_paid_amount` varchar(250) NOT NULL,
  `wr_txn_id` varchar(250) NOT NULL,
  `wr_status` varchar(100) NOT NULL,
  `wr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery_status_chat`
--
ALTER TABLE `delivery_status_chat`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `nm_aboutus`
--
ALTER TABLE `nm_aboutus`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `nm_add`
--
ALTER TABLE `nm_add`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `nm_admin`
--
ALTER TABLE `nm_admin`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indexes for table `nm_adminreply_comments`
--
ALTER TABLE `nm_adminreply_comments`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `nm_auction`
--
ALTER TABLE `nm_auction`
  ADD PRIMARY KEY (`auc_id`);

--
-- Indexes for table `nm_banner`
--
ALTER TABLE `nm_banner`
  ADD PRIMARY KEY (`bn_id`);

--
-- Indexes for table `nm_blog`
--
ALTER TABLE `nm_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `nm_blogsetting`
--
ALTER TABLE `nm_blogsetting`
  ADD PRIMARY KEY (`bs_id`);

--
-- Indexes for table `nm_blog_cus_comments`
--
ALTER TABLE `nm_blog_cus_comments`
  ADD PRIMARY KEY (`cmt_id`);

--
-- Indexes for table `nm_category_ad`
--
ALTER TABLE `nm_category_ad`
  ADD PRIMARY KEY (`cat_ad_id`);

--
-- Indexes for table `nm_category_banner`
--
ALTER TABLE `nm_category_banner`
  ADD PRIMARY KEY (`cat_bn_id`);

--
-- Indexes for table `nm_city`
--
ALTER TABLE `nm_city`
  ADD PRIMARY KEY (`ci_id`);

--
-- Indexes for table `nm_cms_pages`
--
ALTER TABLE `nm_cms_pages`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `nm_cod_commission_paid`
--
ALTER TABLE `nm_cod_commission_paid`
  ADD PRIMARY KEY (`comPaid_id`);

--
-- Indexes for table `nm_cod_commission_tracking`
--
ALTER TABLE `nm_cod_commission_tracking`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `nm_color`
--
ALTER TABLE `nm_color`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `nm_contact`
--
ALTER TABLE `nm_contact`
  ADD PRIMARY KEY (`cont_id`);

--
-- Indexes for table `nm_country`
--
ALTER TABLE `nm_country`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `nm_coupon`
--
ALTER TABLE `nm_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_coupon_purchage`
--
ALTER TABLE `nm_coupon_purchage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_currency`
--
ALTER TABLE `nm_currency`
  ADD PRIMARY KEY (`cur_id`);

--
-- Indexes for table `nm_customer`
--
ALTER TABLE `nm_customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `nm_deals`
--
ALTER TABLE `nm_deals`
  ADD PRIMARY KEY (`deal_id`);

--
-- Indexes for table `nm_emailsetting`
--
ALTER TABLE `nm_emailsetting`
  ADD PRIMARY KEY (`es_id`);

--
-- Indexes for table `nm_enquiry`
--
ALTER TABLE `nm_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_estimate_zipcode`
--
ALTER TABLE `nm_estimate_zipcode`
  ADD PRIMARY KEY (`ez_id`);

--
-- Indexes for table `nm_faq`
--
ALTER TABLE `nm_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `nm_generalsetting`
--
ALTER TABLE `nm_generalsetting`
  ADD PRIMARY KEY (`gs_id`);

--
-- Indexes for table `nm_imagesetting`
--
ALTER TABLE `nm_imagesetting`
  ADD PRIMARY KEY (`imgs_id`);

--
-- Indexes for table `nm_image_sizes`
--
ALTER TABLE `nm_image_sizes`
  ADD PRIMARY KEY (`image_size_id`);

--
-- Indexes for table `nm_inquiries`
--
ALTER TABLE `nm_inquiries`
  ADD PRIMARY KEY (`iq_id`);

--
-- Indexes for table `nm_language`
--
ALTER TABLE `nm_language`
  ADD UNIQUE KEY `id` (`lang_id`);

--
-- Indexes for table `nm_login`
--
ALTER TABLE `nm_login`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `nm_maincategory`
--
ALTER TABLE `nm_maincategory`
  ADD PRIMARY KEY (`mc_id`),
  ADD KEY `mc_status` (`mc_status`);

--
-- Indexes for table `nm_merchant`
--
ALTER TABLE `nm_merchant`
  ADD PRIMARY KEY (`mer_id`),
  ADD KEY `mer_pro_status` (`mer_pro_status`),
  ADD KEY `mer_staus` (`mer_staus`),
  ADD KEY `mer_co_id` (`mer_co_id`),
  ADD KEY `mer_ci_id` (`mer_ci_id`),
  ADD KEY `mer_logintype` (`mer_logintype`);

--
-- Indexes for table `nm_merchant_overallorders`
--
ALTER TABLE `nm_merchant_overallorders`
  ADD PRIMARY KEY (`overOrd_id`);

--
-- Indexes for table `nm_modulesettings`
--
ALTER TABLE `nm_modulesettings`
  ADD PRIMARY KEY (`ms_id`);

--
-- Indexes for table `nm_newsletter_subscribers`
--
ALTER TABLE `nm_newsletter_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_order`
--
ALTER TABLE `nm_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_cus_id` (`order_cus_id`,`order_pro_id`,`order_status`,`delivery_status`);

--
-- Indexes for table `nm_ordercod`
--
ALTER TABLE `nm_ordercod`
  ADD PRIMARY KEY (`cod_id`);

--
-- Indexes for table `nm_order_auction`
--
ALTER TABLE `nm_order_auction`
  ADD PRIMARY KEY (`oa_id`);

--
-- Indexes for table `nm_order_delivery_status`
--
ALTER TABLE `nm_order_delivery_status`
  ADD PRIMARY KEY (`delStatus_id`);

--
-- Indexes for table `nm_order_payu`
--
ALTER TABLE `nm_order_payu`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_cus_id` (`order_cus_id`,`order_pro_id`,`order_status`,`delivery_status`);

--
-- Indexes for table `nm_paymentsettings`
--
ALTER TABLE `nm_paymentsettings`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `nm_procart`
--
ALTER TABLE `nm_procart`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `nm_procolor`
--
ALTER TABLE `nm_procolor`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `nm_product`
--
ALTER TABLE `nm_product`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `nm_prosize`
--
ALTER TABLE `nm_prosize`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `nm_prospec`
--
ALTER TABLE `nm_prospec`
  ADD PRIMARY KEY (`spc_id`);

--
-- Indexes for table `nm_referaluser`
--
ALTER TABLE `nm_referaluser`
  ADD PRIMARY KEY (`ruse_id`);

--
-- Indexes for table `nm_review`
--
ALTER TABLE `nm_review`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `nm_save_cart`
--
ALTER TABLE `nm_save_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `nm_secmaincategory`
--
ALTER TABLE `nm_secmaincategory`
  ADD PRIMARY KEY (`smc_id`),
  ADD KEY `smc_status` (`smc_status`),
  ADD KEY `smc_mc_id` (`smc_mc_id`);

--
-- Indexes for table `nm_secsubcategory`
--
ALTER TABLE `nm_secsubcategory`
  ADD PRIMARY KEY (`ssb_id`),
  ADD KEY `ssb_status` (`ssb_status`),
  ADD KEY `ssb_sb_id` (`ssb_sb_id`),
  ADD KEY `ssb_smc_id` (`ssb_smc_id`),
  ADD KEY `mc_id` (`mc_id`);

--
-- Indexes for table `nm_shipping`
--
ALTER TABLE `nm_shipping`
  ADD PRIMARY KEY (`ship_id`);

--
-- Indexes for table `nm_size`
--
ALTER TABLE `nm_size`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `nm_smtp`
--
ALTER TABLE `nm_smtp`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `nm_social_media`
--
ALTER TABLE `nm_social_media`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `nm_specification`
--
ALTER TABLE `nm_specification`
  ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `nm_spgroup`
--
ALTER TABLE `nm_spgroup`
  ADD PRIMARY KEY (`spg_id`);

--
-- Indexes for table `nm_store`
--
ALTER TABLE `nm_store`
  ADD PRIMARY KEY (`stor_id`),
  ADD KEY `stor_merchant_id` (`stor_merchant_id`),
  ADD KEY `stor_status` (`stor_status`);

--
-- Indexes for table `nm_subcategory`
--
ALTER TABLE `nm_subcategory`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `nm_subscription`
--
ALTER TABLE `nm_subscription`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `nm_terms`
--
ALTER TABLE `nm_terms`
  ADD PRIMARY KEY (`tr_id`);

--
-- Indexes for table `nm_theme`
--
ALTER TABLE `nm_theme`
  ADD PRIMARY KEY (`the_id`);

--
-- Indexes for table `nm_wishlist`
--
ALTER TABLE `nm_wishlist`
  ADD PRIMARY KEY (`ws_id`);

--
-- Indexes for table `nm_withdraw_request`
--
ALTER TABLE `nm_withdraw_request`
  ADD PRIMARY KEY (`wd_id`);

--
-- Indexes for table `nm_withdraw_request_paypal`
--
ALTER TABLE `nm_withdraw_request_paypal`
  ADD PRIMARY KEY (`wr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery_status_chat`
--
ALTER TABLE `delivery_status_chat`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_aboutus`
--
ALTER TABLE `nm_aboutus`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_add`
--
ALTER TABLE `nm_add`
  MODIFY `ad_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nm_admin`
--
ALTER TABLE `nm_admin`
  MODIFY `adm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_adminreply_comments`
--
ALTER TABLE `nm_adminreply_comments`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_auction`
--
ALTER TABLE `nm_auction`
  MODIFY `auc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `nm_banner`
--
ALTER TABLE `nm_banner`
  MODIFY `bn_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_blog`
--
ALTER TABLE `nm_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_blogsetting`
--
ALTER TABLE `nm_blogsetting`
  MODIFY `bs_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_blog_cus_comments`
--
ALTER TABLE `nm_blog_cus_comments`
  MODIFY `cmt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_category_ad`
--
ALTER TABLE `nm_category_ad`
  MODIFY `cat_ad_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_category_banner`
--
ALTER TABLE `nm_category_banner`
  MODIFY `cat_bn_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_city`
--
ALTER TABLE `nm_city`
  MODIFY `ci_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_cms_pages`
--
ALTER TABLE `nm_cms_pages`
  MODIFY `cp_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `nm_cod_commission_paid`
--
ALTER TABLE `nm_cod_commission_paid`
  MODIFY `comPaid_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_cod_commission_tracking`
--
ALTER TABLE `nm_cod_commission_tracking`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_color`
--
ALTER TABLE `nm_color`
  MODIFY `co_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_contact`
--
ALTER TABLE `nm_contact`
  MODIFY `cont_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_country`
--
ALTER TABLE `nm_country`
  MODIFY `co_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `nm_coupon`
--
ALTER TABLE `nm_coupon`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_coupon_purchage`
--
ALTER TABLE `nm_coupon_purchage`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_currency`
--
ALTER TABLE `nm_currency`
  MODIFY `cur_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_customer`
--
ALTER TABLE `nm_customer`
  MODIFY `cus_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_deals`
--
ALTER TABLE `nm_deals`
  MODIFY `deal_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_emailsetting`
--
ALTER TABLE `nm_emailsetting`
  MODIFY `es_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_enquiry`
--
ALTER TABLE `nm_enquiry`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_estimate_zipcode`
--
ALTER TABLE `nm_estimate_zipcode`
  MODIFY `ez_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nm_faq`
--
ALTER TABLE `nm_faq`
  MODIFY `faq_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_generalsetting`
--
ALTER TABLE `nm_generalsetting`
  MODIFY `gs_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_imagesetting`
--
ALTER TABLE `nm_imagesetting`
  MODIFY `imgs_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `nm_image_sizes`
--
ALTER TABLE `nm_image_sizes`
  MODIFY `image_size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_inquiries`
--
ALTER TABLE `nm_inquiries`
  MODIFY `iq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_language`
--
ALTER TABLE `nm_language`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nm_login`
--
ALTER TABLE `nm_login`
  MODIFY `log_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_maincategory`
--
ALTER TABLE `nm_maincategory`
  MODIFY `mc_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_merchant`
--
ALTER TABLE `nm_merchant`
  MODIFY `mer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_merchant_overallorders`
--
ALTER TABLE `nm_merchant_overallorders`
  MODIFY `overOrd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_modulesettings`
--
ALTER TABLE `nm_modulesettings`
  MODIFY `ms_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_newsletter_subscribers`
--
ALTER TABLE `nm_newsletter_subscribers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order`
--
ALTER TABLE `nm_order`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_ordercod`
--
ALTER TABLE `nm_ordercod`
  MODIFY `cod_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order_auction`
--
ALTER TABLE `nm_order_auction`
  MODIFY `oa_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order_delivery_status`
--
ALTER TABLE `nm_order_delivery_status`
  MODIFY `delStatus_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order_payu`
--
ALTER TABLE `nm_order_payu`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_paymentsettings`
--
ALTER TABLE `nm_paymentsettings`
  MODIFY `ps_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_procart`
--
ALTER TABLE `nm_procart`
  MODIFY `pc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_procolor`
--
ALTER TABLE `nm_procolor`
  MODIFY `pc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_product`
--
ALTER TABLE `nm_product`
  MODIFY `pro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_prosize`
--
ALTER TABLE `nm_prosize`
  MODIFY `ps_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_prospec`
--
ALTER TABLE `nm_prospec`
  MODIFY `spc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_referaluser`
--
ALTER TABLE `nm_referaluser`
  MODIFY `ruse_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_review`
--
ALTER TABLE `nm_review`
  MODIFY `comment_id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_save_cart`
--
ALTER TABLE `nm_save_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_secmaincategory`
--
ALTER TABLE `nm_secmaincategory`
  MODIFY `smc_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_secsubcategory`
--
ALTER TABLE `nm_secsubcategory`
  MODIFY `ssb_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_shipping`
--
ALTER TABLE `nm_shipping`
  MODIFY `ship_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_size`
--
ALTER TABLE `nm_size`
  MODIFY `si_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_smtp`
--
ALTER TABLE `nm_smtp`
  MODIFY `sm_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nm_social_media`
--
ALTER TABLE `nm_social_media`
  MODIFY `sm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_specification`
--
ALTER TABLE `nm_specification`
  MODIFY `sp_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_spgroup`
--
ALTER TABLE `nm_spgroup`
  MODIFY `spg_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_store`
--
ALTER TABLE `nm_store`
  MODIFY `stor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_subcategory`
--
ALTER TABLE `nm_subcategory`
  MODIFY `sb_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_subscription`
--
ALTER TABLE `nm_subscription`
  MODIFY `sub_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_terms`
--
ALTER TABLE `nm_terms`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_theme`
--
ALTER TABLE `nm_theme`
  MODIFY `the_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_wishlist`
--
ALTER TABLE `nm_wishlist`
  MODIFY `ws_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_withdraw_request`
--
ALTER TABLE `nm_withdraw_request`
  MODIFY `wd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_withdraw_request_paypal`
--
ALTER TABLE `nm_withdraw_request_paypal`
  MODIFY `wr_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
