-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2019 at 06:03 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mywishlk_multivendor`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery_status_chat`
--

CREATE TABLE `delivery_status_chat` (
  `chat_id` int(11) NOT NULL,
  `delStatus_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `mer_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `send_by` int(11) NOT NULL COMMENT '''1''-customer,''2''-merchant,''3''-admin',
  `note` text NOT NULL,
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_aboutus`
--

CREATE TABLE `nm_aboutus` (
  `ap_id` int(11) NOT NULL,
  `ap_description` longtext NOT NULL,
  `ap_description_fr` longtext NOT NULL,
  `ap_description_ar` longtext CHARACTER SET utf8,
  `ap_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_aboutus`
--

INSERT INTO `nm_aboutus` (`ap_id`, `ap_description`, `ap_description_fr`, `ap_description_ar`, `ap_date`) VALUES
(1, 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br>&nbsp;<br>', 'À propos de nous', '<h1><ul><li><b><i><u>عربى</u></i></b></li></ul></h1><pre>عربى</pre><pre>777</pre><pre><pre>fgdg</pre></pre><pre>ghfghfh\r\ngdfgfdg\r\n<ul><li>ghfh</li><li>gdfg</li><li>hfgh</li></ul></pre>', '2017-06-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `nm_add`
--

CREATE TABLE `nm_add` (
  `ad_id` smallint(5) UNSIGNED NOT NULL,
  `ad_name` varchar(100) NOT NULL,
  `ad_name_fr` varchar(200) NOT NULL,
  `ad_name_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ad_position` tinyint(4) NOT NULL COMMENT '1-left,2-middle,3-right',
  `ad_pages` tinyint(4) NOT NULL COMMENT '1-home,2-product,3-Deal,4-Auction',
  `ad_redirecturl` varchar(150) NOT NULL,
  `ad_img` varchar(150) NOT NULL,
  `ad_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-admin 2 customer',
  `ad_status` tinyint(3) UNSIGNED NOT NULL,
  `ad_read_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_admin`
--

CREATE TABLE `nm_admin` (
  `adm_id` int(10) UNSIGNED NOT NULL,
  `adm_fname` varchar(150) NOT NULL,
  `adm_lname` varchar(150) NOT NULL,
  `adm_password` varchar(150) NOT NULL,
  `adm_email` varchar(150) NOT NULL,
  `adm_phone` varchar(20) NOT NULL,
  `adm_address1` varchar(150) NOT NULL,
  `adm_address2` varchar(150) NOT NULL,
  `adm_ci_id` int(10) UNSIGNED NOT NULL COMMENT 'city id',
  `adm_co_id` smallint(5) UNSIGNED NOT NULL COMMENT 'country id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_admin`
--

INSERT INTO `nm_admin` (`adm_id`, `adm_fname`, `adm_lname`, `adm_password`, `adm_email`, `adm_phone`, `adm_address1`, `adm_address2`, `adm_ci_id`, `adm_co_id`) VALUES
(1, 'admin', 'admin', 'admin', 'kathirvel@pofitec.com', '9790153222', 'chennai', 'chennai', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_adminreply_comments`
--

CREATE TABLE `nm_adminreply_comments` (
  `reply_id` int(11) NOT NULL,
  `reply_blog_id` int(11) NOT NULL,
  `reply_cmt_id` int(11) NOT NULL,
  `reply_msg` text NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_auction`
--

CREATE TABLE `nm_auction` (
  `auc_id` int(10) UNSIGNED NOT NULL,
  `auc_title` varchar(500) NOT NULL,
  `auc_category` int(11) NOT NULL,
  `auc_main_category` int(11) NOT NULL,
  `auc_sub_category` int(11) NOT NULL,
  `auc_second_sub_category` int(11) NOT NULL,
  `auc_original_price` int(11) NOT NULL,
  `auc_auction_price` int(11) NOT NULL,
  `auc_bitinc` smallint(5) UNSIGNED NOT NULL,
  `auc_saving_price` int(11) NOT NULL,
  `auc_start_date` datetime NOT NULL,
  `auc_end_date` datetime NOT NULL,
  `auc_shippingfee` decimal(10,2) NOT NULL,
  `auc_shippinginfo` text NOT NULL,
  `auc_description` text NOT NULL,
  `auc_merchant_id` int(11) NOT NULL,
  `auc_shop_id` int(11) NOT NULL,
  `auc_meta_keyword` varchar(250) NOT NULL,
  `auc_meta_description` varchar(500) NOT NULL,
  `auc_image_count` int(11) NOT NULL,
  `auc_image` varchar(500) NOT NULL,
  `auc_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `auc_posted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_banner`
--

CREATE TABLE `nm_banner` (
  `bn_id` smallint(5) UNSIGNED NOT NULL,
  `bn_title` varchar(150) NOT NULL,
  `bn_title_fr` varchar(150) NOT NULL,
  `bn_title_ar` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `bn_type` varchar(10) NOT NULL COMMENT '1-home,2-product,3-deal,4-auction',
  `bn_img` varchar(150) NOT NULL,
  `bn_status` int(11) NOT NULL COMMENT '1-block,0-unblock',
  `bn_redirecturl` text NOT NULL,
  `bn_slider_position` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_banner`
--

INSERT INTO `nm_banner` (`bn_id`, `bn_title`, `bn_title_fr`, `bn_title_ar`, `bn_type`, `bn_img`, `bn_status`, `bn_redirecturl`, `bn_slider_position`) VALUES
(1, 'Banner', '', NULL, '1,1,1', 'Banner1551784408.png', 0, 'https://mywish.lk/', '1');

-- --------------------------------------------------------

--
-- Table structure for table `nm_blog`
--

CREATE TABLE `nm_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_title` varchar(50) NOT NULL,
  `blog_title_fr` varchar(100) NOT NULL,
  `blog_title_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `blog_desc` text NOT NULL,
  `blog_desc_fr` text NOT NULL,
  `blog_desc_ar` text CHARACTER SET utf8,
  `blog_catid` int(11) NOT NULL,
  `blog_image` varchar(100) NOT NULL,
  `blog_metatitle` varchar(100) NOT NULL,
  `blog_metatitle_fr` varchar(150) NOT NULL,
  `blog_metatitle_ar` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `blog_metadesc` text NOT NULL,
  `blog_metadesc_fr` text NOT NULL,
  `blog_metadesc_ar` text CHARACTER SET utf8,
  `blog_metakey` varchar(100) NOT NULL,
  `blog_metakey_fr` varchar(150) NOT NULL,
  `blog_metakey_ar` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `blog_tags` varchar(100) NOT NULL,
  `blog_comments` int(5) NOT NULL COMMENT '0-not allow,1-allow',
  `blog_type` int(5) UNSIGNED NOT NULL COMMENT '1-publish,2-drafts',
  `blog_status` tinyint(3) UNSIGNED NOT NULL COMMENT '1-block,0-unblock',
  `blog_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_blogsetting`
--

CREATE TABLE `nm_blogsetting` (
  `bs_id` tinyint(3) UNSIGNED NOT NULL,
  `bs_allowcommt` tinyint(4) NOT NULL,
  `bs_radminapproval` tinyint(4) NOT NULL COMMENT 'Require Admin Approval (1-yes & 0-No)',
  `bs_postsppage` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_blog_cus_comments`
--

CREATE TABLE `nm_blog_cus_comments` (
  `cmt_id` int(11) NOT NULL,
  `cmt_blog_id` int(11) NOT NULL,
  `cmt_name` varchar(250) NOT NULL,
  `cmt_email` varchar(250) NOT NULL,
  `cmt_website` varchar(250) NOT NULL,
  `cmt_msg` text NOT NULL,
  `cmt_admin_approve` int(11) NOT NULL DEFAULT '0' COMMENT '1 => Approved, 2 => Unapproved',
  `cmt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cmt_msg_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read ,1 Read '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_category_ad`
--

CREATE TABLE `nm_category_ad` (
  `cat_ad_id` int(11) NOT NULL,
  `cat_ad_maincat_id` int(11) NOT NULL,
  `cat_ad_img` varchar(512) NOT NULL,
  `cat_ad_status` int(11) NOT NULL COMMENT '1-block,0-Unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_category_banner`
--

CREATE TABLE `nm_category_banner` (
  `cat_bn_id` int(11) NOT NULL,
  `cat_bn_maincat_id` int(11) NOT NULL,
  `cat_bn_img` varchar(512) NOT NULL,
  `cat_bn_status` int(1) NOT NULL COMMENT '1-block,0-Unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_city`
--

CREATE TABLE `nm_city` (
  `ci_id` int(10) UNSIGNED NOT NULL,
  `ci_name` varchar(100) NOT NULL,
  `ci_name_fr` varchar(150) NOT NULL,
  `ci_name_ar` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `ci_con_id` smallint(6) NOT NULL,
  `ci_lati` varchar(150) NOT NULL,
  `ci_long` varchar(150) NOT NULL,
  `ci_default` tinyint(4) NOT NULL,
  `ci_status` tinyint(4) NOT NULL COMMENT '1=>unblock,0=>block'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_city`
--

INSERT INTO `nm_city` (`ci_id`, `ci_name`, `ci_name_fr`, `ci_name_ar`, `ci_con_id`, `ci_lati`, `ci_long`, `ci_default`, `ci_status`) VALUES
(1, 'Ratnapura', '', NULL, 1, '6.7055742', '80.38473449999992', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_cms_pages`
--

CREATE TABLE `nm_cms_pages` (
  `cp_id` smallint(5) UNSIGNED NOT NULL,
  `cp_title` varchar(250) NOT NULL,
  `cp_title_fr` varchar(250) NOT NULL,
  `cp_title_ar` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `cp_description` longtext NOT NULL,
  `cp_description_fr` longtext NOT NULL,
  `cp_description_ar` longtext CHARACTER SET utf8,
  `cp_status` tinyint(4) NOT NULL DEFAULT '1',
  `cp_created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_cms_pages`
--

INSERT INTO `nm_cms_pages` (`cp_id`, `cp_title`, `cp_title_fr`, `cp_title_ar`, `cp_description`, `cp_description_fr`, `cp_description_ar`, `cp_status`, `cp_created_date`) VALUES
(9, 'Security', '', 'عربى', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br><br>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br>', '', '<h1><b>dffhasjdhfk</b></h1><ul><li><h2>عربى</h2></li><li>sdfs<h2>عربى</h2></li><li>sdfsd<h2>عربى</h2></li><li>df</li><li>f</li><li>dfd</li><li>fd</li><li>fdf</li><li>dfdfadf</li><li>d</li><li>fa</li><li>df</li><li>df</li><li>fd</li></ul><h1><br></h1>', 1, '2019-02-14 15:47:18'),
(13, 'Help', '', 'عربى', 'What Information Do You We Collect<br>We collect information from you when&nbsp;register&nbsp;on our site or fill out our contact form.<br>When ordering or raising&nbsp;enquiry&nbsp;on our website, as appropriate, you may be asked to enter your Name, Email id, Phone&nbsp;number&nbsp;and&nbsp;skype&nbsp;ID.&nbsp;However&nbsp;you could visit Laravel&nbsp;ecommerce&nbsp;website anonymously.<br><br><br><b>Special Notice&nbsp;</b><br><ul><li>If you are under 13 years old Laravel&nbsp;Ecommerce&nbsp;website is not anticipated at children under 13 years old and we do not collect, use, provide or process in any other form any personal information of children under the age of 13 consciously. We therefore also ask you, if you are under 13 years old, please do not send us your personal information (for example, your name, address and email address).</li></ul><br>Purposes of the collection of your data<br>&nbsp;Laravel&nbsp;Ecommerce&nbsp;is&nbsp;intent&nbsp;to inform you of who we are and what we do. We collect and use personal information (including name, phone&nbsp;number&nbsp;and email ID) to better provide you with the required services, or information. We&nbsp;would therefore&nbsp;use your personal information in order to:<ul><li>Acknowledge to your queries or requests</li><li>Govern our obligations in relation to any agreement you have with us</li><li>Anticipate and resolve problems with any goods or services supplied to you</li><li>Create products or services that may meet your needs</li></ul>Keeping our records accurate<br>&nbsp;We aim to keep our data confidential about you as authentic as possible. If you would like to review, change or delete the details you have provided with us, please contact us via email which is&nbsp;mentioned in&nbsp;our website.<br><br>Security of your personal data<br>&nbsp;As we value your personal information, we will establish sufficient level of protection. We have therefore enforced technology and policies with the objective of protecting your privacy from illegal access and erroneous use and will update these measures as new technology becomes available, as relevant.<br>Cookies policy<h4>Why do we use cookies?</h4>We use browser cookies to learn more about the way you interact with our content and help us to improve your experience when visiting our website.<br>Cookies remember the type of browser you use and which additional browser software you have installed. They also remember your preferences, such as language and region, which remain as your default settings when you revisit the website. Cookies also allow you to rate pages and fill in comment forms.<br>Some of the cookies we use are session cookies and only last until you close your browser, others are persistent cookies which are stored on your computer for longer.<br>Changes on privacy policy<br>&nbsp;We may make&nbsp;changes on&nbsp;our website’s privacy policy at any time. If we make any consequential changes to this privacy policy and the way in which we use your personal data we will post these changes on this page and will do our best to notify you of any significant changes. Kindly often check our privacy policies.', '', '<div>ما هي المعلومات التي نجمعها؟</div><div>نقوم بتجميع المعلومات منك عند التسجيل على موقعنا أو ملء نموذج الاتصال الخاص بنا.</div><div>عند طلب أو رفع استفسار على موقعنا ، حسب الاقتضاء ، قد يطلب منك إدخال اسمك ، معرف البريد الإلكتروني ، رقم الهاتف ومعرف السكايب. ومع ذلك يمكنك زيارة موقع التجارة الإلكترونية Laravel مجهول.</div><div><br></div><div><br></div><div>اشعار خاص</div><div>إذا كان عمرك أقل من 13 عامًا لموقع ويب Laravel Ecommerce غير متوقع للأطفال دون سن 13 عامًا ونحن لا نجمع أو نستخدم أو نقدم أو نتعامل بأي شكل آخر مع أي معلومات شخصية خاصة بالأطفال دون سن 13 عامًا. لذلك نطلب منك أيضًا ، إذا كان عمرك أقل من 13 عامًا ، يُرجى عدم إرسال معلوماتك الشخصية (على سبيل المثال ، اسمك وعنوانك وعنوان بريدك الإلكتروني).</div><div><br></div><div>أغراض جمع البيانات الخاصة بك</div><div>&nbsp;تهدف شركة Laravel Ecommerce إلى إعلامك بما نحن عليه وما نفعله. نقوم بجمع واستخدام المعلومات الشخصية (بما في ذلك الاسم ورقم الهاتف ومعرف البريد الإلكتروني) لتوفير أفضل الخدمات أو المعلومات المطلوبة لك. لذلك نستخدم معلوماتك الشخصية من أجل:</div><div>الإقرار بالاستفسارات أو الطلبات الخاصة بك</div><div>تحكم التزاماتنا فيما يتعلق بأي اتفاق لديك معنا</div><div>توقع وحل المشاكل مع أي سلع أو خدمات مقدمة لك</div><div>إنشاء منتجات أو خدمات قد تلبي احتياجاتك</div><div>الحفاظ على سجلاتنا دقيقة</div><div>&nbsp;نهدف إلى الحفاظ على سرية بياناتنا عنك بأصالة قدر الإمكان. إذا كنت ترغب في مراجعة أو تغيير أو حذف التفاصيل التي قدمتها لنا ، يرجى الاتصال بنا عبر البريد الإلكتروني المذكور في موقعنا.</div><div><br></div><div>أمن بياناتك الشخصية</div><div>&nbsp;نظرًا لأننا نقدر معلوماتك الشخصية ، فسنعمل على توفير مستوى كافٍ من الحماية. ولذلك قمنا بتطبيق التكنولوجيا والسياسات بهدف حماية خصوصيتك من الوصول غير المشروع والاستخدام الخاطئ ، وسوف نقوم بتحديث هذه الإجراءات عندما تصبح التكنولوجيا الجديدة متوفرة ، حسب الاقتضاء.</div><div>اتفاقية ملفات تعريف الارتباط</div><div>لماذا نستخدم ملفات تعريف الارتباط؟</div><div>نستخدم ملفات تعريف الارتباط للمتصفح لمعرفة المزيد حول الطريقة التي تتفاعل بها مع المحتوى الخاص بنا ومساعدتنا في تحسين تجربتك عند زيارة موقعنا الإلكتروني.</div><div>تتذكر ملفات تعريف الارتباط نوع المتصفح الذي تستخدمه وأي برنامج متصفح إضافي قمت بتثبيته. كما أنهم يتذكرون تفضيلاتك ، مثل اللغة والمنطقة ، والتي تبقى كإعدادات افتراضية عند زيارتك لموقع الويب. تسمح لك ملفات تعريف الارتباط أيضًا بتقييم الصفحات وملء نماذج التعليقات.</div><div>بعض ملفات تعريف الارتباط التي نستخدمها هي ملفات تعريف الارتباط الخاصة بالجلسة وتستمر فقط حتى تقوم بإغلاق المتصفح ، والبعض الآخر ملفات تعريف الارتباط الثابتة التي يتم تخزينها على جهاز الكمبيوتر الخاص بك لفترة أطول.</div><div>التغييرات على سياسة الخصوصية</div><div>&nbsp;قد نقوم بإجراء تغييرات على سياسة الخصوصية لموقعنا الإلكتروني في أي وقت. إذا قمنا بإجراء أي تغييرات لاحقة على سياسة الخصوصية هذه والطريقة التي نستخدم بها بياناتك الشخصية ، سنقوم بنشر هذه التغييرات على هذه الصفحة وسنبذل قصارى جهدنا لإعلامك بأي تغييرات مهمة. يرجى التحقق من سياسات الخصوصية الخاصة بنا.</div>', 1, '2019-02-14 16:31:49'),
(14, 'Returns Policy', '', 'عربى', '1.merchant terms and conditions<br>sdfsdf<br><br><h2>sdf</h2>sdf<br><h1>sdf</h1>', '', '<h1><b>عربى</b></h1><ul><li>fsdfd</li><li>sdf</li><li>sdf</li><li>sdfdsd</li><li>sf</li><li>dsfsdf</li><li>dsf</li><li>sdf</li><li>sdfdsf</li><li>sdf</li><li>sdfsdgdg</li><li>fsdf</li><li>sdf</li><li>dfgdfg</li></ul>', 1, '2019-02-14 15:44:10'),
(15, 'Privacy', '', 'عربى', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '<pre>  يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب<br>يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب<br>يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب<br>يترجم الكلمات والعبارات وصفحات الويبيترجم الكلمات والعبارات وصفحات الويب يترجم الكلمات والعبارات وصفحات الويب</pre><br><br><br><br><br><br><br><br><br><br>', 1, '2019-02-14 15:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `nm_cod_commission_paid`
--

CREATE TABLE `nm_cod_commission_paid` (
  `comPaid_id` int(11) NOT NULL,
  `com_merchant_id` int(11) NOT NULL,
  `com_mer_name` varchar(255) NOT NULL,
  `paidAmount` double NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_type` enum('0','1') NOT NULL COMMENT '0-offline,1-paypal',
  `online_payment_returnStatus` varchar(255) NOT NULL,
  `com_status` enum('0','1','2') NOT NULL COMMENT '1 => paid, 0=> Hold,''2''=>Cancelled',
  `com_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_cod_commission_tracking`
--

CREATE TABLE `nm_cod_commission_tracking` (
  `com_id` int(11) NOT NULL,
  `com_merchant_id` int(11) NOT NULL,
  `com_cod_id` int(11) NOT NULL,
  `com_pro_id` int(11) NOT NULL,
  `com_cod_amt` double NOT NULL,
  `cod_currency` varchar(255) NOT NULL,
  `cod_transaction_id` varchar(255) NOT NULL,
  `com_percentAmount` double NOT NULL,
  `exchange_rate` double NOT NULL,
  `exchange_currency` varchar(255) NOT NULL,
  `com_status` enum('0','1') NOT NULL COMMENT '1 => paid, 0=> Hold',
  `com_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_color`
--

CREATE TABLE `nm_color` (
  `co_id` smallint(5) UNSIGNED NOT NULL,
  `co_code` varchar(10) NOT NULL,
  `co_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_contact`
--

CREATE TABLE `nm_contact` (
  `cont_id` int(10) UNSIGNED NOT NULL,
  `cont_name` varchar(100) NOT NULL,
  `cont_email` varchar(150) NOT NULL,
  `cont_no` varchar(50) NOT NULL,
  `cont_message` text NOT NULL,
  `cont_restatus` tinyint(4) NOT NULL,
  `cont_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_country`
--

CREATE TABLE `nm_country` (
  `co_id` smallint(5) UNSIGNED NOT NULL,
  `co_code` varchar(10) NOT NULL,
  `co_name` varchar(30) NOT NULL,
  `co_name_fr` varchar(150) NOT NULL,
  `co_name_ar` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `co_cursymbol` varchar(5) CHARACTER SET utf8mb4 NOT NULL,
  `co_curcode` varchar(10) NOT NULL,
  `co_status` tinyint(4) NOT NULL COMMENT '1-block,0-unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_country`
--

INSERT INTO `nm_country` (`co_id`, `co_code`, `co_name`, `co_name_fr`, `co_name_ar`, `co_cursymbol`, `co_curcode`, `co_status`) VALUES
(1, ' LK', 'Sri Lanka', '', NULL, 'Rs', 'LKR', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_coupon`
--

CREATE TABLE `nm_coupon` (
  `id` int(255) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_name` varchar(255) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=>flat, 2=>percentage',
  `value` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1',
  `type_of_coupon` varchar(100) NOT NULL COMMENT 'product coupon->1, user coupon->2',
  `terms` longtext NOT NULL,
  `coupon_per_product` varchar(155) NOT NULL,
  `coupon_per_user` varchar(155) NOT NULL,
  `tot_cart_val` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_coupon_purchage`
--

CREATE TABLE `nm_coupon_purchage` (
  `id` int(100) NOT NULL,
  `coupon_id` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `sold_user` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `pro_qty` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type_of_coupon` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_currency`
--

CREATE TABLE `nm_currency` (
  `cur_id` int(10) UNSIGNED NOT NULL,
  `cur_name` varchar(100) NOT NULL,
  `cur_code` varchar(5) NOT NULL,
  `cur_symbol` varchar(10) NOT NULL,
  `cur_status` tinyint(11) NOT NULL DEFAULT '1',
  `cur_default` tinyint(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_customer`
--

CREATE TABLE `nm_customer` (
  `cus_id` bigint(10) UNSIGNED NOT NULL,
  `cus_name` varchar(100) NOT NULL,
  `facebook_id` varchar(150) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `cus_email` varchar(150) NOT NULL,
  `cus_pwd` varchar(40) NOT NULL,
  `cus_phone` varchar(20) NOT NULL,
  `cus_address1` varchar(150) NOT NULL,
  `cus_address2` varchar(150) NOT NULL,
  `cus_country` varchar(50) NOT NULL,
  `cus_city` varchar(50) NOT NULL,
  `cus_joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cus_logintype` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>Admin user, 2=> Website User, 3=> Facebook User, 4=>Google+ User',
  `cus_status` int(11) NOT NULL COMMENT '0 unblock 1 block',
  `cus_pic` varchar(150) NOT NULL,
  `created_date` date NOT NULL,
  `wallet` bigint(100) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address1` varchar(100) NOT NULL,
  `ship_address2` varchar(100) NOT NULL,
  `ship_ci_id` int(11) NOT NULL,
  `ship_state` varchar(100) NOT NULL,
  `ship_country` int(11) NOT NULL,
  `ship_postalcode` varchar(30) NOT NULL,
  `ship_phone` varchar(25) NOT NULL,
  `ship_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_customer`
--

INSERT INTO `nm_customer` (`cus_id`, `cus_name`, `facebook_id`, `google_id`, `cus_email`, `cus_pwd`, `cus_phone`, `cus_address1`, `cus_address2`, `cus_country`, `cus_city`, `cus_joindate`, `cus_logintype`, `cus_status`, `cus_pic`, `created_date`, `wallet`, `ship_name`, `ship_address1`, `ship_address2`, `ship_ci_id`, `ship_state`, `ship_country`, `ship_postalcode`, `ship_phone`, `ship_email`) VALUES
(1, 'Upashantha', '', '', 'buddhikaupashantha@gmail.com', '8603963fb1160d5f1a8e809483125b6d', '0772991827', '', '', '1', '1', '2019-03-05 12:49:29', 2, 0, '', '2019-03-05', 0, 'Upashantha', '', '', 1, '', 1, '', '0772991827', 'buddhikaupashantha@gmail.com'),
(2, 'Muppi', '', '', 'muppidathi@pofitec.com', 'e10adc3949ba59abbe56e057f20f883e', '909090909', '22 ccc', '33 ddd', '1', '1', '2019-03-06 09:47:22', 1, 0, '', '2019-03-06', 0, 'xxx', '22 ccc', '33 ddd', 1, 'Tamil nadu', 1, '343434', '909090909', 'xxx@gmail.com'),
(3, 'Mani Kandan', '2147483647', '', 'mani.visolve@gmail.com', '', '', '', '', '', '', '2019-03-08 10:19:49', 3, 0, '', '0000-00-00', 0, '', '', '', 0, '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nm_deals`
--

CREATE TABLE `nm_deals` (
  `deal_id` int(11) NOT NULL,
  `deal_title` varchar(500) NOT NULL,
  `deal_title_fr` varchar(250) NOT NULL,
  `deal_title_ar` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `deal_category` int(11) NOT NULL,
  `deal_main_category` int(11) NOT NULL,
  `deal_sub_category` int(11) NOT NULL,
  `deal_second_sub_category` int(11) NOT NULL,
  `deal_original_price` int(11) NOT NULL,
  `deal_discount_price` int(11) NOT NULL,
  `deal_discount_percentage` int(11) NOT NULL,
  `deal_saving_price` int(11) NOT NULL,
  `deal_inctax` varchar(10) NOT NULL DEFAULT '0',
  `deal_shippamt` varchar(10) NOT NULL,
  `deal_start_date` datetime NOT NULL,
  `deal_end_date` datetime NOT NULL,
  `deal_expiry_date` date NOT NULL,
  `deal_description` text NOT NULL,
  `deal_description_fr` text NOT NULL,
  `deal_description_ar` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `deal_merchant_id` int(11) NOT NULL,
  `deal_shop_id` int(11) NOT NULL,
  `deal_meta_keyword` varchar(250) NOT NULL,
  `deal_meta_keyword_fr` varchar(250) NOT NULL,
  `deal_meta_keyword_ar` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `deal_meta_description` varchar(500) NOT NULL,
  `deal_meta_description_fr` varchar(500) NOT NULL,
  `deal_meta_description_ar` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `deal_min_limit` int(11) NOT NULL,
  `deal_max_limit` int(11) NOT NULL,
  `deal_purchase_limit` int(11) NOT NULL,
  `deal_image_count` int(11) NOT NULL,
  `deal_image` varchar(500) NOT NULL,
  `deal_no_of_purchase` int(11) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `deal_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `deal_posted_date` datetime NOT NULL,
  `deal_delivery` int(11) NOT NULL,
  `allow_cancel` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_return` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_replace` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `cancel_policy` text NOT NULL,
  `cancel_policy_fr` text NOT NULL,
  `cancel_policy_ar` text CHARACTER SET utf8,
  `return_policy` text NOT NULL,
  `return_policy_fr` text NOT NULL,
  `return_policy_ar` text CHARACTER SET utf8,
  `replace_policy` text NOT NULL,
  `replace_policy_fr` text NOT NULL,
  `replace_policy_ar` text CHARACTER SET utf8,
  `cancel_days` int(11) NOT NULL,
  `return_days` int(11) NOT NULL,
  `replace_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_emailsetting`
--

CREATE TABLE `nm_emailsetting` (
  `es_id` tinyint(3) UNSIGNED NOT NULL,
  `es_contactname` varchar(150) NOT NULL,
  `es_contactemail` varchar(150) NOT NULL,
  `es_skype_email_id` varchar(500) NOT NULL,
  `es_webmasteremail` varchar(150) NOT NULL,
  `es_noreplyemail` varchar(150) NOT NULL,
  `es_phone1` varchar(20) NOT NULL,
  `es_phone2` varchar(20) NOT NULL,
  `es_latitude` decimal(18,14) NOT NULL,
  `es_longitude` decimal(18,14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_emailsetting`
--

INSERT INTO `nm_emailsetting` (`es_id`, `es_contactname`, `es_contactemail`, `es_skype_email_id`, `es_webmasteremail`, `es_noreplyemail`, `es_phone1`, `es_phone2`, `es_latitude`, `es_longitude`) VALUES
(1, 'Laravel Ecommerce', 'salestest@laravelecommerce.com', 'sales@laravelecommerce.com', 'sales@laravelecommerce.com', 'sales@laravelecommerce.com', '+919790153222', '+1 (972) 591 8222', '7.98591014355452', '80.73921674804683');

-- --------------------------------------------------------

--
-- Table structure for table `nm_enquiry`
--

CREATE TABLE `nm_enquiry` (
  `id` int(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(50) NOT NULL,
  `created_date` varchar(255) NOT NULL,
  `enq_readstatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_enquiry`
--

INSERT INTO `nm_enquiry` (`id`, `name`, `email`, `phone`, `message`, `status`, `created_date`, `enq_readstatus`) VALUES
(1, 'MableAntef', 'melody_23@gmail.com', '81296182856', 'Hello, I want to work in your company on a voluntary basis, can you offer me anything? \r\na little about me:https://about.me/ruthmcfarlane/', 1, '2019-04-16', 0),
(2, 'de chabe', 'gregorAnaette@gmail.com', '720-791-3210', 'We offer you the opportunity to advertise your products and services. \r\n \r\nHi! Look at an interesting offering for you. I can send your commercial offers or messages through feedback forms. The advantage of this method is that the messages sent through the feedback forms are included in the white list. This method increases the chance that your message will be read. The same way you received this message. \r\nSending via Feedback Forms to any domain zones of the world. (more than 1000 domain zones.). \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. \r\nDomain zone .com - (12 million messages sent) - $399 instead of $699 \r\nAll us sites that have a feedback form. (10 million messages sent) - $349 instead of $649 \r\nAll domain zones in Europe- (8 million messages sent) - $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\n \r\n \r\nDiscounts are valid until April 20. \r\nFeedback and warranty! \r\nDelivery report! \r\n \r\nIn the process of sending messages, we do not violate the rules of GDRP. \r\nThis message is created automatically use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\n \r\n \r\nThank you for your attention. \r\n \r\nThis message is created automatically use our contacts for communication.', 1, '2019-04-17', 0),
(3, 'Megan Rodriguez', 'meganmg96krodriguez@aol.com', '(805) 372-1751', 'Hi,\r\n  I had a look at your Instagram Instagram and it looks pretty good.  \r\n \r\nThe question I have is this, is your Instagram giving you the traffic and engagement that you deserve?  \r\n \r\nWe help people organically increase their followers and engagement.  Discover how we can help you get the genuine followers you deserve:\r\n\r\nhttps://www.leadaccelerator.online/insta?=mywish.lk\r\n\r\nRegards,\r\nMegan\r\nInstagram Growth Specialist\r\nSEO Vale\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n500 Westover Dr #12733\r\nSanford, NC 27330\r\n\r\nIf you prefer not to receive commercial messages regarding Instagram for your website, please opt out here: https://www.leadaccelerator.online/out.php/?site=mywish.lk\r\n', 1, '2019-04-28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_estimate_zipcode`
--

CREATE TABLE `nm_estimate_zipcode` (
  `ez_id` int(11) NOT NULL,
  `ez_code_series` int(11) NOT NULL,
  `ez_code_series_end` int(11) NOT NULL,
  `ez_code_days` int(11) NOT NULL,
  `ez_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_faq`
--

CREATE TABLE `nm_faq` (
  `faq_id` smallint(5) UNSIGNED NOT NULL,
  `faq_name` varchar(256) NOT NULL,
  `faq_name_fr` varchar(250) NOT NULL,
  `faq_name_ar` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `faq_ans` text NOT NULL,
  `faq_ans_fr` text NOT NULL,
  `faq_ans_ar` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `faq_status` tinyint(4) NOT NULL COMMENT '0=>unblock,1=>block'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_generalsetting`
--

CREATE TABLE `nm_generalsetting` (
  `gs_id` tinyint(4) NOT NULL,
  `gs_sitename` varchar(100) NOT NULL,
  `gs_sitename_fr` varchar(200) DEFAULT NULL,
  `gs_sitename_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gs_sitedescription` varchar(200) NOT NULL,
  `gs_sitedescription_fr` varchar(200) DEFAULT NULL,
  `gs_sitedescription_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gs_metatitle` varchar(150) NOT NULL,
  `gs_metatitle_fr` varchar(150) DEFAULT NULL,
  `gs_metatitle_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gs_metakeywords` text NOT NULL,
  `gs_metakeywords_fr` text,
  `gs_metakeywords_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gs_metadesc` text NOT NULL,
  `gs_metadesc_fr` text,
  `gs_metadesc_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gs_defaulttheme` tinyint(3) UNSIGNED NOT NULL,
  `gs_defaultlanguage` tinyint(3) UNSIGNED NOT NULL,
  `gs_payment_status` varchar(50) NOT NULL,
  `gs_store_status` varchar(20) DEFAULT NULL,
  `gs_stripe` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `gs_paypal_payment` varchar(100) NOT NULL,
  `gs_payumoney_status` varchar(100) NOT NULL,
  `gs_themes` varchar(20) NOT NULL,
  `gs_playstore_url` varchar(500) NOT NULL,
  `gs_apple_appstore_url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_generalsetting`
--

INSERT INTO `nm_generalsetting` (`gs_id`, `gs_sitename`, `gs_sitename_fr`, `gs_sitename_ar`, `gs_sitedescription`, `gs_sitedescription_fr`, `gs_sitedescription_ar`, `gs_metatitle`, `gs_metatitle_fr`, `gs_metatitle_ar`, `gs_metakeywords`, `gs_metakeywords_fr`, `gs_metakeywords_ar`, `gs_metadesc`, `gs_metadesc_fr`, `gs_metadesc_ar`, `gs_defaulttheme`, `gs_defaultlanguage`, `gs_payment_status`, `gs_store_status`, `gs_stripe`, `gs_paypal_payment`, `gs_payumoney_status`, `gs_themes`, `gs_playstore_url`, `gs_apple_appstore_url`) VALUES
(1, 'MY WISH', 'fr', 'Laravel Ecommerce', 'Developed in Native Android & iOS Platform.Well Written Structured Code. Our App is user friendly and gives ample opportunities for guests and hosts ', 'fr', 'footer description', 'MY WISH', 'Laravel Ecommerce fr', ' meta title', 'MY WISH', 'Laravel Ecommerce', 'Laravel Ecommerce', 'Laravel Ecommerce Online shopping Laravel Ecommerce Online shopping  Laravel Ecommerce Online shopping Laravel Ecommerce Online shopping Laravel Ecommerce Online shopping Laravel Ecommerce Online shopping ', 'Laravel Ecommerce', 'Laravel Ecommerce', 1, 1, 'COD', 'Store', 'Stripe', 'Paypal', '', '', 'https://googgle.co.in', 'https://googgle.com');

-- --------------------------------------------------------

--
-- Table structure for table `nm_imagesetting`
--

CREATE TABLE `nm_imagesetting` (
  `imgs_id` smallint(6) NOT NULL,
  `imgs_name` varchar(150) NOT NULL,
  `imgs_type` tinyint(4) NOT NULL COMMENT '1- logo,2 -Favicon,3-noimage,4-product,5-deal,6-sores,7-blog_banner,8-upload_banner,9-category,10-ads_blog_image,''11''=>''category'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_imagesetting`
--

INSERT INTO `nm_imagesetting` (`imgs_id`, `imgs_name`, `imgs_type`) VALUES
(1, 'Logo_1551782693.png', 1),
(2, 'Favicon_1551793244_mywish_logo.png', 2),
(3, 'No_image_1550205994_381x215.jpg', 3),
(4, 'No_image_1522327122_800x800', 4),
(5, 'No_image_1550206016_800x800.jpg', 5),
(6, 'No_image_1509364387_455x378.png', 6),
(7, 'No_image_1509364387_320x190.png', 7),
(8, 'No_image_1550206111_845x500.jpg', 8),
(9, 'No_image_1550206204_250x200.jpg', 9),
(10, 'No_image_1550206259_800x400.jpg', 10),
(11, 'No_image_1550206307_200x200.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `nm_image_sizes`
--

CREATE TABLE `nm_image_sizes` (
  `image_size_id` int(11) NOT NULL,
  `image_size` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_image_sizes`
--

INSERT INTO `nm_image_sizes` (`image_size_id`, `image_size`) VALUES
(1, '{\"product\":{\"width\":800,\"height\":800},\"deals\":{\"width\":800,\"height\":800},\"logo\":{\"width\":185,\"height\":45},\"favicon\":{\"width\":16,\"height\":16},\"no_image\":{\"width\":381,\"height\":215},\"category_advertisment\":{\"width\":170,\"height\":400},\"category_banner\":{\"width\":250,\"height\":200},\"top_category\":{\"width\":200,\"height\":200},\"sub_category\":{\"width\":200,\"height\":200},\"sec_sub_category\":{\"width\":200,\"height\":200},\"ads\":{\"width\":800,\"height\":400},\"store\":{\"width\":455,\"height\":378},\"blog\":{\"width\":320,\"height\":190},\"no_image_banner\":{\"width\":845,\"height\":500}}');

-- --------------------------------------------------------

--
-- Table structure for table `nm_inquiries`
--

CREATE TABLE `nm_inquiries` (
  `iq_id` int(10) UNSIGNED NOT NULL,
  `iq_name` varchar(100) NOT NULL,
  `iq_emailid` varchar(150) NOT NULL,
  `iq_phonenumber` varchar(20) NOT NULL,
  `iq_message` varchar(300) NOT NULL,
  `inq_readstatus` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_language`
--

CREATE TABLE `nm_language` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `lang_name` varchar(30) NOT NULL,
  `lang_status` int(4) NOT NULL COMMENT '1->Active,2->deactive, 3->delete',
  `lang_default` int(11) NOT NULL COMMENT '1->default,',
  `pack_lang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_language`
--

INSERT INTO `nm_language` (`lang_id`, `lang_code`, `lang_name`, `lang_status`, `lang_default`, `pack_lang`) VALUES
(1, 'en', 'English', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_login`
--

CREATE TABLE `nm_login` (
  `log_id` int(5) NOT NULL,
  `cus_id` int(5) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-wesite,2 facebook'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_login`
--

INSERT INTO `nm_login` (`log_id`, `cus_id`, `log_date`, `log_type`) VALUES
(1, 1, '2019-03-05 12:51:21', 1),
(2, 2, '2019-03-06 12:31:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_maincategory`
--

CREATE TABLE `nm_maincategory` (
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_name` varchar(100) NOT NULL,
  `mc_name_fr` varchar(100) NOT NULL,
  `mc_name_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `mc_type` varchar(10) NOT NULL,
  `mc_img` varchar(150) NOT NULL,
  `mc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_maincategory`
--

INSERT INTO `nm_maincategory` (`mc_id`, `mc_name`, `mc_name_fr`, `mc_name_ar`, `mc_type`, `mc_img`, `mc_status`) VALUES
(1, 'Electronics', '', NULL, '1,1,1', 'Top_category_1551784884.png', 1),
(2, 'Mens', '', NULL, '1,1,1', 'Top_category_1551785323.jpg', 1),
(3, 'Womens', '', NULL, '1,1,1', 'Top_category_1551785501.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_merchant`
--

CREATE TABLE `nm_merchant` (
  `mer_id` int(10) UNSIGNED NOT NULL,
  `addedby` varchar(20) NOT NULL COMMENT '0=>admin',
  `mer_fname` varchar(150) NOT NULL,
  `mer_lname` varchar(150) NOT NULL,
  `mer_password` varchar(150) NOT NULL,
  `mer_email` varchar(150) NOT NULL,
  `mer_phone` varchar(20) NOT NULL,
  `mer_address1` varchar(150) NOT NULL,
  `mer_address2` varchar(150) NOT NULL,
  `mer_ci_id` int(10) UNSIGNED NOT NULL COMMENT 'city id',
  `mer_co_id` smallint(5) UNSIGNED NOT NULL COMMENT 'country id',
  `mer_payment` varchar(100) NOT NULL,
  `mer_payu_key` varchar(250) NOT NULL,
  `mer_payu_salt` varchar(250) NOT NULL,
  `mer_commission` tinyint(4) NOT NULL,
  `created_date` date NOT NULL,
  `mer_staus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-unblock,0-block',
  `mer_pro_status` int(11) NOT NULL COMMENT '1=>unblock, 0=>block',
  `mer_country_status` char(1) NOT NULL DEFAULT 'A',
  `mer_logintype` varchar(30) NOT NULL DEFAULT '1' COMMENT '1=>Admin user, 2=> Website User, 3=> Facebook User'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_merchant`
--

INSERT INTO `nm_merchant` (`mer_id`, `addedby`, `mer_fname`, `mer_lname`, `mer_password`, `mer_email`, `mer_phone`, `mer_address1`, `mer_address2`, `mer_ci_id`, `mer_co_id`, `mer_payment`, `mer_payu_key`, `mer_payu_salt`, `mer_commission`, `created_date`, `mer_staus`, `mer_pro_status`, `mer_country_status`, `mer_logintype`) VALUES
(1, '0', 'Test', 'Merchant', 'u3xHbQW5', 'testmerchant@gmail.com', '1234567895', 'Address1', 'Address2', 1, 1, '', '', '', 10, '2019-03-05', 1, 1, 'A', '1');

-- --------------------------------------------------------

--
-- Table structure for table `nm_merchant_overallorders`
--

CREATE TABLE `nm_merchant_overallorders` (
  `overOrd_id` int(11) NOT NULL,
  `over_mer_id` int(11) NOT NULL,
  `over_tot_ord_amt` decimal(10,2) NOT NULL,
  `over_tot_offline_amt` decimal(10,2) NOT NULL,
  `over_tot_online_amt` decimal(10,2) NOT NULL,
  `over_tot_coupon_amt` decimal(10,2) NOT NULL,
  `over_tot_wallet_amt` decimal(10,2) NOT NULL,
  `commissionAmt` decimal(10,2) NOT NULL,
  `merchantAmt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_merchant_overallorders`
--

INSERT INTO `nm_merchant_overallorders` (`overOrd_id`, `over_mer_id`, `over_tot_ord_amt`, `over_tot_offline_amt`, `over_tot_online_amt`, `over_tot_coupon_amt`, `over_tot_wallet_amt`, `commissionAmt`, `merchantAmt`) VALUES
(1, 1, '340.00', '320.00', '20.00', '0.00', '0.00', '32.00', '18.00');

-- --------------------------------------------------------

--
-- Table structure for table `nm_modulesettings`
--

CREATE TABLE `nm_modulesettings` (
  `ms_id` int(11) NOT NULL,
  `ms_dealmodule` int(11) NOT NULL,
  `ms_productmodule` int(11) NOT NULL,
  `ms_auctionmodule` int(11) NOT NULL,
  `ms_blogmodule` int(11) NOT NULL,
  `ms_nearmemapmodule` int(11) NOT NULL,
  `ms_storelistmodule` int(11) NOT NULL,
  `ms_pastdealmodule` int(11) NOT NULL,
  `ms_faqmodule` int(11) NOT NULL,
  `ms_cod` int(11) NOT NULL,
  `ms_paypal` int(11) NOT NULL,
  `ms_creditcard` int(11) NOT NULL,
  `ms_googlecheckout` int(11) NOT NULL,
  `ms_shipping` int(11) NOT NULL COMMENT '1=>Free shipping, 2=> Flat shipping, 3=> Product per shippin, 4=> Per Item shipping',
  `ms_newsletter_template` int(11) NOT NULL COMMENT '1=> Temp 1, 2=>Temp 2, 3=> Temp 3, 4=> Temp 4',
  `ms_citysettings` int(11) NOT NULL COMMENT '1=> With city, 0=> Without city'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_newsletter_subscribers`
--

CREATE TABLE `nm_newsletter_subscribers` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `post_dt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order`
--

CREATE TABLE `nm_order` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_cus_id` int(10) UNSIGNED NOT NULL,
  `order_pro_id` int(11) UNSIGNED NOT NULL,
  `order_prod_unitPrice` double NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `transaction_id` varchar(50) NOT NULL,
  `payer_email` varchar(50) NOT NULL,
  `payer_id` varchar(50) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_amt` decimal(15,2) NOT NULL COMMENT '(unit price * quantity)',
  `order_tax` decimal(10,2) NOT NULL COMMENT 'tax per unit (in %)',
  `order_taxAmt` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `token_id` varchar(30) NOT NULL,
  `payment_ack` varchar(10) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` varchar(20) NOT NULL,
  `payer_status` varchar(50) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL DEFAULT '2' COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `order_paytype` smallint(6) NOT NULL DEFAULT '1' COMMENT '1-paypal',
  `order_pro_color` int(11) NOT NULL,
  `order_pro_size` int(11) NOT NULL,
  `order_shipping_amt` varchar(20) NOT NULL,
  `order_shipping_add` text NOT NULL,
  `order_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(255) NOT NULL,
  `coupon_total_amount` varchar(255) NOT NULL,
  `wallet_amount` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_order`
--

INSERT INTO `nm_order` (`order_id`, `order_cus_id`, `order_pro_id`, `order_prod_unitPrice`, `order_type`, `transaction_id`, `payer_email`, `payer_id`, `payer_name`, `order_qty`, `order_amt`, `order_tax`, `order_taxAmt`, `currency_code`, `token_id`, `payment_ack`, `order_date`, `created_date`, `payer_status`, `order_status`, `delivery_status`, `order_paytype`, `order_pro_color`, `order_pro_size`, `order_shipping_amt`, `order_shipping_add`, `order_merchant_id`, `coupon_code`, `coupon_type`, `coupon_amount_type`, `coupon_amount`, `coupon_total_amount`, `wallet_amount`, `mer_commission_amt`, `mer_amt`) VALUES
(1, 2, 4, 20, 1, 'PAY-NONETWORKPAYIDEXAMPLE123', '', '', '', 1, '20.00', '0.00', '0.00', 'LKR', '', 'Success', '2019-04-13 00:13:46', '', 'verified', 1, 1, 1, 0, 0, '0', 'Xxx,22 ccc,33 ddd,Tamil nadu,343434,909090909,xxx@gmail.com', 1, '', '', '', '', '', 0, '2.00', '18.00');

-- --------------------------------------------------------

--
-- Table structure for table `nm_ordercod`
--

CREATE TABLE `nm_ordercod` (
  `cod_id` int(10) UNSIGNED NOT NULL,
  `cod_transaction_id` varchar(60) NOT NULL,
  `cod_cus_id` int(10) UNSIGNED NOT NULL,
  `cod_pro_id` int(11) UNSIGNED NOT NULL,
  `cod_prod_unitPrice` double NOT NULL DEFAULT '0',
  `cod_order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `cod_qty` int(11) NOT NULL,
  `cod_amt` decimal(10,2) NOT NULL COMMENT '(unit_price*quantity)',
  `cod_tax` decimal(10,2) NOT NULL COMMENT 'per_product_tax (in %)',
  `cod_taxAmt` decimal(10,2) NOT NULL,
  `cod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cod_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `created_date` varchar(20) NOT NULL,
  `cod_paytype` smallint(6) NOT NULL COMMENT '0=>cash on delivery',
  `cod_pro_color` tinyint(4) NOT NULL,
  `cod_pro_size` tinyint(4) NOT NULL,
  `cod_shipping_amt` varchar(20) NOT NULL,
  `cod_ship_addr` text NOT NULL,
  `cod_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(100) NOT NULL,
  `coupon_total_amount` varchar(100) NOT NULL,
  `wallet_amount` double NOT NULL,
  `cod_prod_actualprice` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_ordercod`
--

INSERT INTO `nm_ordercod` (`cod_id`, `cod_transaction_id`, `cod_cus_id`, `cod_pro_id`, `cod_prod_unitPrice`, `cod_order_type`, `cod_qty`, `cod_amt`, `cod_tax`, `cod_taxAmt`, `cod_date`, `cod_status`, `delivery_status`, `created_date`, `cod_paytype`, `cod_pro_color`, `cod_pro_size`, `cod_shipping_amt`, `cod_ship_addr`, `cod_merchant_id`, `coupon_code`, `coupon_type`, `coupon_amount_type`, `coupon_amount`, `coupon_total_amount`, `wallet_amount`, `cod_prod_actualprice`, `mer_commission_amt`, `mer_amt`) VALUES
(1, 'ORDER15517905091', 1, 4, 20, 1, 5, '100.00', '0.00', '0.00', '2019-03-05 00:55:09', 3, 1, '', 0, 0, 1, '0', 'Upashantha,no03 near court,new town,embilipitiya,07000,0772991827,buddhikaupashantha@gmail.com,Ratnapura,Sri Lanka', 1, '0', '0', '0', '0', '0', 0, 50, '10.00', '90.00'),
(2, 'ORDER15518761533', 2, 2, 100, 1, 1, '100.00', '0.00', '0.00', '2019-03-06 00:42:33', 1, 4, '', 0, 0, 0, '0', 'xxx,22 ccc,33 ddd,Tamil nadu,343434,909090909,xxx@gmail.com,Ratnapura,Sri Lanka', 1, '0', '0', '0', '0', '0', 0, 110, '10.00', '90.00'),
(3, 'ORDER15518761533', 2, 4, 20, 1, 1, '20.00', '0.00', '0.00', '2019-03-06 00:42:33', 1, 8, '', 0, 0, 1, '0', 'xxx,22 ccc,33 ddd,Tamil nadu,343434,909090909,xxx@gmail.com,Ratnapura,Sri Lanka', 1, '0', '0', '0', '0', '0', 0, 50, '2.00', '18.00'),
(4, 'ORDERV5Ctwfsc', 3, 2, 100, 1, 1, '100.00', '0.00', '0.00', '2019-03-07 22:20:42', 3, 1, '', 0, 0, 0, '0.00', 'Mani Kandan,buikd,chjn,tanik,641689,9658253610,manikandan@pofitec.com', 1, '', '', '', '', '', 0, 0, '10.00', '90.00');

-- --------------------------------------------------------

--
-- Table structure for table `nm_ordercod_wallet`
--

CREATE TABLE `nm_ordercod_wallet` (
  `cod_transaction_id` varchar(255) NOT NULL,
  `wallet_used` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_auction`
--

CREATE TABLE `nm_order_auction` (
  `oa_id` int(11) NOT NULL,
  `oa_pro_id` int(11) NOT NULL,
  `oa_cus_id` int(11) NOT NULL,
  `oa_cus_name` varchar(150) NOT NULL,
  `oa_cus_email` varchar(250) NOT NULL,
  `oa_cus_address` text NOT NULL,
  `oa_bid_amt` int(11) NOT NULL,
  `oa_bid_shipping_amt` int(11) NOT NULL,
  `oa_original_bit_amt` int(11) NOT NULL,
  `oa_bid_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `oa_bid_winner` int(11) NOT NULL COMMENT '1=> Winner, 0=> Bidders',
  `oa_bid_item_status` int(11) NOT NULL COMMENT '0=> Onprocess, 1=> Send,  3=>Cancelled',
  `oa_delivery_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_delivery_status`
--

CREATE TABLE `nm_order_delivery_status` (
  `delStatus_id` int(11) NOT NULL,
  `order_cust_id` int(11) NOT NULL,
  `cod_order_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `mer_id` int(11) NOT NULL,
  `order_type` int(11) NOT NULL COMMENT '''1''-product,''2''-deal',
  `transaction_id` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL COMMENT '''0''->cod,''1''->paypal',
  `delivery_statuss` int(11) NOT NULL,
  `cancel_status` int(11) NOT NULL COMMENT '''0''->not done,''1''->cancel pending ,''2''->cancelled,''3''->hold,4->"Disapproved"',
  `cancel_notes` text NOT NULL,
  `cancel_date` datetime NOT NULL,
  `cancel_approved_date` datetime NOT NULL,
  `return_status` int(11) NOT NULL COMMENT '''0''-not done,''1''-return pending,''3''->hold,''2''-returned,4->"Disapproved"',
  `return_notes` text NOT NULL,
  `return_date` datetime NOT NULL,
  `return_approved_date` datetime NOT NULL,
  `replace_status` int(11) NOT NULL COMMENT '''0''-not done,''1''-replace pending,''3''->hold,''2''-replaced,4->"Disapproved"',
  `replace_notes` text NOT NULL,
  `replace_date` datetime NOT NULL,
  `replace_approved_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_payu`
--

CREATE TABLE `nm_order_payu` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_cus_id` int(10) UNSIGNED NOT NULL,
  `order_pro_id` int(11) UNSIGNED NOT NULL,
  `order_prod_unitPrice` double NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `transaction_id` varchar(50) NOT NULL,
  `payer_email` varchar(50) NOT NULL,
  `payer_id` varchar(50) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_amt` decimal(15,2) NOT NULL COMMENT '(unit price * quantity)',
  `order_tax` decimal(10,2) NOT NULL COMMENT 'tax per unit (in %)',
  `order_taxAmt` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `token_id` varchar(30) NOT NULL,
  `payment_ack` varchar(10) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` varchar(20) NOT NULL,
  `payer_status` varchar(50) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL DEFAULT '2' COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `order_paytype` smallint(6) NOT NULL DEFAULT '1' COMMENT '1-paypal',
  `order_pro_color` int(11) NOT NULL,
  `order_pro_size` int(11) NOT NULL,
  `order_shipping_amt` varchar(20) NOT NULL,
  `order_shipping_add` text NOT NULL,
  `order_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(255) NOT NULL,
  `coupon_total_amount` varchar(255) NOT NULL,
  `wallet_amount` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_order_stripe`
--

CREATE TABLE `nm_order_stripe` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_cus_id` int(10) UNSIGNED NOT NULL,
  `order_pro_id` int(11) UNSIGNED NOT NULL,
  `order_prod_unitPrice` double NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `transaction_id` varchar(50) NOT NULL,
  `payer_email` varchar(50) NOT NULL,
  `payer_id` varchar(50) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_amt` decimal(15,2) NOT NULL COMMENT '(unit price * quantity)',
  `order_tax` decimal(10,2) NOT NULL COMMENT 'tax per unit (in %)',
  `order_taxAmt` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `token_id` varchar(30) NOT NULL,
  `payment_ack` varchar(10) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` varchar(20) NOT NULL,
  `payer_status` varchar(50) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `delivery_status` int(11) NOT NULL DEFAULT '2' COMMENT '1->order_placed,2->order_packed,3->Dispatched,4->Delivered,5->cancel pending,6->cancelled,7->return pending ,8->returned,9->replace pending,10->replaced',
  `order_paytype` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-COD,1-paypal,2-payu,3-stripe',
  `order_pro_color` int(11) NOT NULL,
  `order_pro_size` int(11) NOT NULL,
  `order_shipping_amt` varchar(20) NOT NULL,
  `order_shipping_add` text NOT NULL,
  `order_merchant_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_amount_type` varchar(255) NOT NULL,
  `coupon_amount` varchar(255) NOT NULL,
  `coupon_total_amount` varchar(255) NOT NULL,
  `wallet_amount` double NOT NULL,
  `mer_commission_amt` decimal(10,2) NOT NULL,
  `mer_amt` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_paymentsettings`
--

CREATE TABLE `nm_paymentsettings` (
  `ps_id` tinyint(3) UNSIGNED NOT NULL,
  `ps_flatshipping` decimal(10,2) NOT NULL COMMENT 'shipping Tax Percentage',
  `ps_taxpercentage` tinyint(3) UNSIGNED NOT NULL,
  `ps_extenddays` smallint(5) UNSIGNED NOT NULL COMMENT 'Auction Extend Days',
  `ps_alertdays` int(11) NOT NULL,
  `ps_minfundrequest` int(10) UNSIGNED NOT NULL,
  `ps_maxfundrequest` int(10) UNSIGNED NOT NULL,
  `ps_referralamount` int(11) NOT NULL,
  `ps_countryid` int(11) NOT NULL,
  `ps_countrycode` varchar(10) NOT NULL,
  `ps_cursymbol` varchar(10) CHARACTER SET utf8 NOT NULL,
  `ps_curcode` varchar(10) NOT NULL,
  `ps_paypal_email` varchar(255) NOT NULL,
  `ps_paypalaccount` varchar(150) NOT NULL,
  `ps_paypal_api_pw` varchar(250) NOT NULL,
  `ps_paypal_api_signature` varchar(250) NOT NULL,
  `ps_authorize_trans_key` varchar(250) NOT NULL,
  `ps_authorize_api_id` varchar(250) NOT NULL,
  `ps_stripe_secretkey` varchar(250) NOT NULL,
  `ps_stripe_publishkey` varchar(250) NOT NULL,
  `ps_payu_key` varchar(250) NOT NULL,
  `ps_payu_salt` varchar(250) NOT NULL,
  `ps_paypal_pay_mode` tinyint(4) NOT NULL COMMENT '0->Test Mode, 1-> Live Mode'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_paymentsettings`
--

INSERT INTO `nm_paymentsettings` (`ps_id`, `ps_flatshipping`, `ps_taxpercentage`, `ps_extenddays`, `ps_alertdays`, `ps_minfundrequest`, `ps_maxfundrequest`, `ps_referralamount`, `ps_countryid`, `ps_countrycode`, `ps_cursymbol`, `ps_curcode`, `ps_paypal_email`, `ps_paypalaccount`, `ps_paypal_api_pw`, `ps_paypal_api_signature`, `ps_authorize_trans_key`, `ps_authorize_api_id`, `ps_stripe_secretkey`, `ps_stripe_publishkey`, `ps_payu_key`, `ps_payu_salt`, `ps_paypal_pay_mode`) VALUES
(1, '0.00', 0, 0, 0, 0, 0, 0, 1, ' LK', 'Rs', 'LKR', '', 'venugopal-facilitator_api1.pofitec.com', 'U5BL5KK3ZUZVJRNL', 'AhEqPBa2LPCE3sKdenmfssNtAsh0AF4qYwXaeb9bpLQiA-T83dJ-0KHq', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_procart`
--

CREATE TABLE `nm_procart` (
  `pc_id` int(10) UNSIGNED NOT NULL,
  `pc_date` datetime NOT NULL,
  `pc_pro_id` int(11) NOT NULL,
  `pc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_procolor`
--

CREATE TABLE `nm_procolor` (
  `pc_id` bigint(20) UNSIGNED NOT NULL,
  `pc_pro_id` int(10) UNSIGNED NOT NULL,
  `pc_co_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_prodelpolicy`
--

CREATE TABLE `nm_prodelpolicy` (
  `pdp_id` bigint(20) UNSIGNED NOT NULL,
  `pdp_pro_id` smallint(5) UNSIGNED NOT NULL,
  `pdp_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_product`
--

CREATE TABLE `nm_product` (
  `pro_no_of_purchase` int(11) NOT NULL,
  `pro_id` int(10) UNSIGNED NOT NULL,
  `pro_title` varchar(150) NOT NULL,
  `pro_title_fr` varchar(150) DEFAULT NULL,
  `pro_title_ar` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `pro_mc_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pro_smc_id` smallint(5) UNSIGNED NOT NULL,
  `pro_sb_id` smallint(5) UNSIGNED NOT NULL,
  `pro_ssb_id` smallint(5) UNSIGNED NOT NULL,
  `product_brand_id` int(11) NOT NULL,
  `pro_price` decimal(15,2) NOT NULL,
  `pro_disprice` decimal(15,2) NOT NULL,
  `pro_discount_percentage` varchar(11) NOT NULL,
  `pro_inctax` tinyint(4) NOT NULL,
  `pro_shippamt` decimal(15,2) NOT NULL,
  `pro_desc` longtext NOT NULL,
  `pro_desc_fr` longtext,
  `pro_desc_ar` longtext CHARACTER SET utf8,
  `pro_isspec` tinyint(4) NOT NULL COMMENT '1-yes 2-no',
  `pro_is_size` int(11) NOT NULL COMMENT '0=>yes, 1=>no',
  `pro_is_color` int(11) NOT NULL COMMENT '0=>yes, 1=>no',
  `pro_delivery` smallint(5) UNSIGNED NOT NULL COMMENT 'in days',
  `pro_mr_id` int(10) UNSIGNED NOT NULL,
  `pro_sh_id` smallint(5) UNSIGNED NOT NULL,
  `pro_mkeywords` text NOT NULL,
  `pro_mkeywords_fr` text,
  `pro_mkeywords_ar` text CHARACTER SET utf8,
  `pro_mdesc` text NOT NULL COMMENT 'metadescription',
  `pro_mdesc_fr` text,
  `pro_mdesc_ar` text CHARACTER SET utf8,
  `pro_postfacebook` tinyint(4) NOT NULL,
  `pro_Img` varchar(500) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `pro_status` tinyint(4) NOT NULL COMMENT '2=>Delete ,1=> Active, 0 => Block',
  `pro_image_count` int(11) NOT NULL,
  `pro_qty` int(11) NOT NULL,
  `pro_sku_number` varchar(20) DEFAULT NULL,
  `hit_count` int(11) NOT NULL DEFAULT '0',
  `sold_status` int(11) NOT NULL DEFAULT '1',
  `cash_pack` varchar(255) NOT NULL,
  `allow_cancel` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_return` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `allow_replace` enum('0','1') NOT NULL COMMENT '0-No,1-Yes',
  `cancel_policy` text NOT NULL,
  `cancel_policy_fr` text,
  `cancel_policy_ar` text CHARACTER SET utf8,
  `return_policy` text NOT NULL,
  `return_policy_fr` text,
  `return_policy_ar` text CHARACTER SET utf8,
  `replace_policy` text NOT NULL,
  `replace_policy_fr` text,
  `replace_policy_ar` text CHARACTER SET utf8,
  `cancel_days` int(11) NOT NULL,
  `return_days` int(11) NOT NULL,
  `replace_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_product`
--

INSERT INTO `nm_product` (`pro_no_of_purchase`, `pro_id`, `pro_title`, `pro_title_fr`, `pro_title_ar`, `pro_mc_id`, `pro_smc_id`, `pro_sb_id`, `pro_ssb_id`, `product_brand_id`, `pro_price`, `pro_disprice`, `pro_discount_percentage`, `pro_inctax`, `pro_shippamt`, `pro_desc`, `pro_desc_fr`, `pro_desc_ar`, `pro_isspec`, `pro_is_size`, `pro_is_color`, `pro_delivery`, `pro_mr_id`, `pro_sh_id`, `pro_mkeywords`, `pro_mkeywords_fr`, `pro_mkeywords_ar`, `pro_mdesc`, `pro_mdesc_fr`, `pro_mdesc_ar`, `pro_postfacebook`, `pro_Img`, `created_date`, `pro_status`, `pro_image_count`, `pro_qty`, `pro_sku_number`, `hit_count`, `sold_status`, `cash_pack`, `allow_cancel`, `allow_return`, `allow_replace`, `cancel_policy`, `cancel_policy_fr`, `cancel_policy_ar`, `return_policy`, `return_policy_fr`, `return_policy_ar`, `replace_policy`, `replace_policy_fr`, `replace_policy_ar`, `cancel_days`, `return_days`, `replace_days`) VALUES
(0, 1, 'Sony MDRZX110A OnEar Stereo Headphones ', NULL, NULL, 1, 1, 0, 0, 0, '1000.00', '900.00', '10', 0, '0.00', '<h1>Sony MDR-ZX110A On-Ear Stereo Headphones&nbsp;</h1><br><br><ul><li>30mm dynamic driver unit for clear sound</li><li>High energy neodymium magnets deliver powerful sound</li><li>Slim, folding design for easy portability</li><li>Pressure relieving earpads for extended comfort</li><li>Warranty: 1 Year</li><li>30mm dynamic driver unit for clear sound</li><li>High energy neodymium magnets deliver powerful sound</li><li>Slim, folding design for easy portability</li><li>Pressure relieving earpads for extended comfort</li><li>Warranty: 1 Year</li></ul>', NULL, NULL, 2, 1, 1, 22, 1, 1, '30mm dynamic driver unit for clear sound\r\nHigh energy neodymium magnets deliver powerful sound\r\nSlim, folding design for easy portability\r\nPressure relieving earpads for extended comfort\r\nWarranty: 1 Year', NULL, NULL, '30mm dynamic driver unit for clear sound\r\nHigh energy neodymium magnets deliver powerful sound\r\nSlim, folding design for easy portability\r\nPressure relieving earpads for extended comfort\r\nWarranty: 1 Year', NULL, NULL, 0, 'Product_1551785046481672912.png/**/', '2019-03-05', 1, 1, 10000, '0', 35, 1, '0 ', '0', '0', '0', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, 0, 0, 0),
(3, 2, 'Mens Basic Polyester Polo Tshirts', NULL, NULL, 2, 2, 0, 0, 0, '110.00', '100.00', '9', 0, '0.00', 'Men\'s Basic Polyester Polo T-shirts<br><ul><li>100 % Polyester</li><li>Ideal for team wear and work uniforms<br></li><li>T-shirt Colour : Black , White , Royal Blue</li></ul>Customisation technology: Embroidery<br>', NULL, NULL, 2, 1, 1, 4, 1, 1, '', NULL, NULL, '', NULL, NULL, 0, 'Product_1551785424647215231.png/**/', '2019-03-05', 1, 1, 10000, '0', 162, 1, '0 ', '0', '0', '0', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, 0, 0, 0),
(0, 3, 'HOT Fashion Womens Dress Sexy Lady Summer Dress Solid Womens Short Sleeve Loose Denim Sundress VNeck Female Casual Mini Dress', NULL, NULL, 3, 3, 0, 0, 0, '1000.00', '500.00', '50', 0, '0.00', 'HOT Fashion Women\'s Dress Sexy Lady Summer Dress Solid Womens Short Sleeve Loose Denim Sundress V-Neck Female Casual Mini Dress', NULL, NULL, 2, 1, 1, 23, 1, 1, '', NULL, NULL, '', NULL, NULL, 0, 'Product_1551785658792240991.jpg/**/', '2019-03-05', 1, 1, 4000, '0', 46, 1, '0 ', '0', '0', '0', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, 0, 0, 0),
(13, 4, 'Fastrack  NG38022PP03 Tees Watch  For Men  Women', NULL, NULL, 1, 1, 0, 0, 0, '50.00', '20.00', '60', 0, '0.00', 'Fastrack&nbsp;NG38022PP03 Tees Watch - For Men &amp; WomenFastrack&nbsp;<br>NG38022PP03 Tees Watch - For Men &amp; WomenFastrack&nbsp;<br>NG38022PP03 Tees Watch - For Men &amp; WomenFastrack&nbsp;<br>NG38022PP03 Tees Watch - For Men &amp; Women', NULL, NULL, 2, 1, 1, 3, 1, 1, '', NULL, NULL, '', NULL, NULL, 0, 'Product_1551785794616255983.jpg/**/', '2019-03-05', 1, 1, 10000, '0', 119, 1, '0 ', '0', '0', '0', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `nm_prosize`
--

CREATE TABLE `nm_prosize` (
  `ps_id` bigint(20) UNSIGNED NOT NULL,
  `ps_pro_id` int(10) UNSIGNED NOT NULL,
  `ps_si_id` smallint(5) UNSIGNED NOT NULL,
  `ps_volume` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_prosize`
--

INSERT INTO `nm_prosize` (`ps_id`, `ps_pro_id`, `ps_si_id`, `ps_volume`) VALUES
(3, 4, 1, 1),
(7, 3, 1, 1),
(9, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_prospec`
--

CREATE TABLE `nm_prospec` (
  `spc_id` bigint(20) UNSIGNED NOT NULL,
  `spc_pro_id` int(10) UNSIGNED NOT NULL,
  `spc_spg_id` int(11) NOT NULL,
  `spc_sp_id` smallint(5) UNSIGNED NOT NULL,
  `spc_value` text NOT NULL,
  `spc_value_fr` text NOT NULL,
  `spc_value_ar` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_referaluser`
--

CREATE TABLE `nm_referaluser` (
  `ruse_id` int(10) UNSIGNED NOT NULL,
  `ruse_date` datetime NOT NULL,
  `ruse_name` varchar(100) NOT NULL,
  `ruse_emailid` varchar(150) NOT NULL,
  `ruse_userid` int(10) UNSIGNED NOT NULL,
  `ruse_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_review`
--

CREATE TABLE `nm_review` (
  `comment_id` int(50) NOT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `deal_id` varchar(255) DEFAULT NULL,
  `store_id` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `ratings` varchar(255) NOT NULL,
  `status` int(50) NOT NULL,
  `review_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sam1` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_save_cart`
--

CREATE TABLE `nm_save_cart` (
  `cart_id` int(11) NOT NULL,
  `cart_product_id` int(11) NOT NULL,
  `cart_deal_id` int(11) NOT NULL,
  `cart_product_qty` int(11) NOT NULL,
  `cart_type` int(1) NOT NULL COMMENT '1-Products,2-Deals',
  `cart_pro_siz_id` int(11) NOT NULL,
  `cart_pro_col_id` int(11) NOT NULL,
  `cart_user_id` int(11) NOT NULL,
  `addedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_secmaincategory`
--

CREATE TABLE `nm_secmaincategory` (
  `smc_id` smallint(5) UNSIGNED NOT NULL,
  `smc_name` varchar(100) NOT NULL,
  `smc_name_fr` varchar(100) NOT NULL,
  `smc_name_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `smc_mc_id` smallint(5) UNSIGNED NOT NULL,
  `smc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_secmaincategory`
--

INSERT INTO `nm_secmaincategory` (`smc_id`, `smc_name`, `smc_name_fr`, `smc_name_ar`, `smc_mc_id`, `smc_status`) VALUES
(1, 'Headset', '', NULL, 1, 1),
(2, 'Tshirts', '', NULL, 2, 1),
(3, 'Dress', '', NULL, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nm_secsubcategory`
--

CREATE TABLE `nm_secsubcategory` (
  `ssb_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_name` varchar(100) NOT NULL,
  `ssb_name_fr` varchar(100) NOT NULL,
  `ssb_name_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ssb_sb_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_smc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_shipping`
--

CREATE TABLE `nm_shipping` (
  `ship_id` int(10) UNSIGNED NOT NULL,
  `ship_name` varchar(100) NOT NULL,
  `ship_address1` varchar(200) NOT NULL,
  `ship_address2` varchar(200) NOT NULL,
  `ship_ci_id` varchar(50) NOT NULL,
  `ship_state` varchar(100) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_postalcode` varchar(20) NOT NULL,
  `ship_phone` varchar(20) NOT NULL,
  `ship_email` varchar(255) NOT NULL,
  `ship_order_id` varchar(50) NOT NULL,
  `ship_trans_id` varchar(50) NOT NULL,
  `ship_cus_id` bigint(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_shipping`
--

INSERT INTO `nm_shipping` (`ship_id`, `ship_name`, `ship_address1`, `ship_address2`, `ship_ci_id`, `ship_state`, `ship_country`, `ship_postalcode`, `ship_phone`, `ship_email`, `ship_order_id`, `ship_trans_id`, `ship_cus_id`, `date_time`) VALUES
(1, 'Upashantha', 'no03 near court', 'new town', 'Ratnapura', 'embilipitiya', 'Sri Lanka', '07000', '0772991827', 'buddhikaupashantha@gmail.com', '1', 'ORDER15517905091', 1, '2019-03-05 12:55:09'),
(2, 'xxx', '22 ccc', '33 ddd', 'Ratnapura', 'Tamil nadu', 'Sri Lanka', '343434', '909090909', 'xxx@gmail.com', '3', 'ORDER15518761533', 2, '2019-03-06 12:42:33'),
(3, 'Mani Kandan', '', '', '', '', '', '', '', '', '', '', 3, '2019-03-08 10:19:49'),
(4, 'Mani Kandan', 'buikd', 'chjn', 'Ratnapura', 'tanik', 'Sri Lanka', '641689', '9658253610', 'manikandan@pofitec.com', '4', 'ORDERV5Ctwfsc', 3, '2019-03-08 10:20:42'),
(5, 'Xxx', '22 ccc', '33 ddd', 'Ratnapura', 'Tamil nadu', 'Sri Lanka', '343434', '909090909', 'xxx@gmail.com', '1', 'PAY-NONETWORKPAYIDEXAMPLE123', 2, '2019-04-13 12:13:46');

-- --------------------------------------------------------

--
-- Table structure for table `nm_size`
--

CREATE TABLE `nm_size` (
  `si_id` smallint(5) UNSIGNED NOT NULL,
  `si_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_size`
--

INSERT INTO `nm_size` (`si_id`, `si_name`) VALUES
(1, 'no size');

-- --------------------------------------------------------

--
-- Table structure for table `nm_smtp`
--

CREATE TABLE `nm_smtp` (
  `sm_id` tinyint(4) NOT NULL,
  `sm_host` varchar(150) NOT NULL,
  `sm_port` varchar(20) NOT NULL,
  `sm_uname` varchar(30) NOT NULL,
  `sm_pwd` varchar(30) NOT NULL,
  `sm_isactive` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_social_media`
--

CREATE TABLE `nm_social_media` (
  `sm_id` int(11) NOT NULL,
  `sm_fb_app_id` varchar(100) NOT NULL,
  `sm_fb_sec_key` varchar(100) NOT NULL,
  `sm_fb_page_url` varchar(250) NOT NULL,
  `sm_fb_like_page_url` varchar(250) NOT NULL,
  `sm_twitter_url` varchar(250) NOT NULL,
  `sm_twitter_app_id` varchar(250) NOT NULL,
  `sm_twitter_sec_key` varchar(250) NOT NULL,
  `sm_linkedin_url` varchar(250) NOT NULL,
  `sm_youtube_url` varchar(250) NOT NULL,
  `sm_gmap_app_key` varchar(250) NOT NULL,
  `sm_android_page_url` varchar(250) NOT NULL,
  `sm_iphone_url` varchar(250) NOT NULL,
  `sm_analytics_code` text NOT NULL,
  `sm_gl_client_id` varchar(250) NOT NULL,
  `sm_gl_sec_key` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_social_media`
--

INSERT INTO `nm_social_media` (`sm_id`, `sm_fb_app_id`, `sm_fb_sec_key`, `sm_fb_page_url`, `sm_fb_like_page_url`, `sm_twitter_url`, `sm_twitter_app_id`, `sm_twitter_sec_key`, `sm_linkedin_url`, `sm_youtube_url`, `sm_gmap_app_key`, `sm_android_page_url`, `sm_iphone_url`, `sm_analytics_code`, `sm_gl_client_id`, `sm_gl_sec_key`) VALUES
(1, '2061310464145758', '2bf864193bf35640985298f43f40ad44', 'https://www.facebook.com', 'https://www.facebook.com', 'https://www.facebook.com', 'dsf1dsfsd232d1f21dsf21ds2f1dsf', 'sd2f1sd2f13sfgsd543df3ds1fds1f', 'https://www.facebook.com', 'https://www.facebook.com', 'AIzaSyCsDoY1OPjAqu1PlQhH3UljYsfw-81bLkI', '', '', '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-62671250-4\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-62671250-4\');\r\n</script>\r\n', '782885230420-rbpe9m9044krsto1dqchhr3p6am81ggh.apps.googleusercontent.com', 'O40k7cbT7lnLaESYQ0npsY5c');

-- --------------------------------------------------------

--
-- Table structure for table `nm_specification`
--

CREATE TABLE `nm_specification` (
  `sp_id` smallint(5) UNSIGNED NOT NULL,
  `sp_name` text NOT NULL,
  `sp_name_fr` text NOT NULL,
  `sp_name_ar` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `sp_spg_id` smallint(5) UNSIGNED NOT NULL,
  `sp_order` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_spgroup`
--

CREATE TABLE `nm_spgroup` (
  `spg_id` smallint(5) UNSIGNED NOT NULL,
  `spg_name` varchar(150) NOT NULL,
  `spg_name_fr` varchar(500) NOT NULL,
  `spg_name_ar` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `spg_order` smallint(6) NOT NULL,
  `sp_mc_id` int(11) NOT NULL,
  `sp_smc_id` int(11) NOT NULL,
  `show_on_filter` enum('0','1') NOT NULL COMMENT '0:No;1:yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_store`
--

CREATE TABLE `nm_store` (
  `stor_id` int(10) UNSIGNED NOT NULL,
  `stor_merchant_id` int(10) NOT NULL,
  `stor_name` varchar(100) NOT NULL,
  `stor_name_fr` varchar(100) NOT NULL,
  `stor_name_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `stor_phone` varchar(20) NOT NULL,
  `stor_address1` varchar(150) NOT NULL,
  `stor_address1_fr` varchar(200) NOT NULL,
  `stor_address1_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `stor_address2` varchar(150) NOT NULL,
  `stor_address2_fr` varchar(200) NOT NULL,
  `stor_address2_ar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `stor_country` smallint(5) UNSIGNED NOT NULL,
  `stor_city` int(10) UNSIGNED NOT NULL,
  `stor_zipcode` varchar(20) NOT NULL,
  `stor_metakeywords` text NOT NULL,
  `stor_metakeywords_fr` text NOT NULL,
  `stor_metakeywords_ar` text CHARACTER SET utf8,
  `stor_metadesc` text NOT NULL,
  `stor_metadesc_fr` text NOT NULL,
  `stor_metadesc_ar` text CHARACTER SET utf8,
  `stor_website` text NOT NULL,
  `stor_latitude` decimal(18,14) NOT NULL,
  `stor_longitude` decimal(18,14) NOT NULL,
  `stor_img` varchar(200) NOT NULL,
  `stor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>unblock,0=>block',
  `created_date` varchar(20) NOT NULL,
  `stor_addedby` int(5) NOT NULL DEFAULT '1' COMMENT '1-admin,2 -merchant',
  `store_city_status` char(1) NOT NULL DEFAULT 'A',
  `promote_status` tinyint(4) NOT NULL,
  `stor_slogan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_store`
--

INSERT INTO `nm_store` (`stor_id`, `stor_merchant_id`, `stor_name`, `stor_name_fr`, `stor_name_ar`, `stor_phone`, `stor_address1`, `stor_address1_fr`, `stor_address1_ar`, `stor_address2`, `stor_address2_fr`, `stor_address2_ar`, `stor_country`, `stor_city`, `stor_zipcode`, `stor_metakeywords`, `stor_metakeywords_fr`, `stor_metakeywords_ar`, `stor_metadesc`, `stor_metadesc_fr`, `stor_metadesc_ar`, `stor_website`, `stor_latitude`, `stor_longitude`, `stor_img`, `stor_status`, `created_date`, `stor_addedby`, `store_city_status`, `promote_status`, `stor_slogan`) VALUES
(1, 1, 'Store Test', '', NULL, '1234567895', 'address1', '', NULL, 'address2', '', NULL, 1, 1, '124256', '', '', NULL, '', '', NULL, 'https://mywish.lk/', '6.70557420000000', '80.38473449999992', 'Store_1551784741.jpg', 1, '03/05/2019', 1, 'A', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `nm_subcategory`
--

CREATE TABLE `nm_subcategory` (
  `sb_id` smallint(5) UNSIGNED NOT NULL,
  `sb_name` varchar(100) NOT NULL,
  `sb_name_fr` varchar(100) NOT NULL,
  `sb_name_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `sc_img` varchar(250) NOT NULL,
  `sb_smc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `sb_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_subscription`
--

CREATE TABLE `nm_subscription` (
  `sub_id` int(10) UNSIGNED NOT NULL,
  `sub_cus_id` int(10) UNSIGNED NOT NULL COMMENT 'customer id',
  `sub_mc_id` smallint(5) UNSIGNED NOT NULL,
  `sub_status` tinyint(4) NOT NULL,
  `sub_readstatus` int(11) NOT NULL DEFAULT '0' COMMENT '-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_terms`
--

CREATE TABLE `nm_terms` (
  `tr_id` int(11) NOT NULL,
  `tr_description` text NOT NULL,
  `tr_description_fr` longtext NOT NULL,
  `tr_description_ar` longtext CHARACTER SET utf8,
  `tr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_theme`
--

CREATE TABLE `nm_theme` (
  `the_id` smallint(5) UNSIGNED NOT NULL,
  `the_Name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_wishlist`
--

CREATE TABLE `nm_wishlist` (
  `ws_id` bigint(20) UNSIGNED NOT NULL,
  `ws_pro_id` bigint(20) UNSIGNED NOT NULL,
  `ws_cus_id` bigint(20) UNSIGNED NOT NULL,
  `ws_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_withdraw_request`
--

CREATE TABLE `nm_withdraw_request` (
  `wd_id` int(11) NOT NULL,
  `wd_mer_id` int(11) NOT NULL,
  `wd_total_wd_amt` decimal(10,2) NOT NULL,
  `wd_submited_wd_amt` decimal(10,2) NOT NULL,
  `wd_status` int(11) NOT NULL COMMENT '1 => paid, 0=> Hold',
  `wd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nm_withdraw_request_paypal`
--

CREATE TABLE `nm_withdraw_request_paypal` (
  `wr_id` int(11) NOT NULL,
  `wr_mer_id` int(11) NOT NULL,
  `wr_mer_name` varchar(250) NOT NULL,
  `wr_mer_payment_email` varchar(250) NOT NULL,
  `wr_paid_amount` varchar(250) NOT NULL,
  `wr_txn_id` varchar(250) NOT NULL,
  `wr_status` varchar(100) NOT NULL,
  `wr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery_status_chat`
--
ALTER TABLE `delivery_status_chat`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `nm_aboutus`
--
ALTER TABLE `nm_aboutus`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `nm_add`
--
ALTER TABLE `nm_add`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `nm_admin`
--
ALTER TABLE `nm_admin`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indexes for table `nm_adminreply_comments`
--
ALTER TABLE `nm_adminreply_comments`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `nm_auction`
--
ALTER TABLE `nm_auction`
  ADD PRIMARY KEY (`auc_id`);

--
-- Indexes for table `nm_banner`
--
ALTER TABLE `nm_banner`
  ADD PRIMARY KEY (`bn_id`);

--
-- Indexes for table `nm_blog`
--
ALTER TABLE `nm_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `nm_blogsetting`
--
ALTER TABLE `nm_blogsetting`
  ADD PRIMARY KEY (`bs_id`);

--
-- Indexes for table `nm_blog_cus_comments`
--
ALTER TABLE `nm_blog_cus_comments`
  ADD PRIMARY KEY (`cmt_id`);

--
-- Indexes for table `nm_category_ad`
--
ALTER TABLE `nm_category_ad`
  ADD PRIMARY KEY (`cat_ad_id`);

--
-- Indexes for table `nm_category_banner`
--
ALTER TABLE `nm_category_banner`
  ADD PRIMARY KEY (`cat_bn_id`);

--
-- Indexes for table `nm_city`
--
ALTER TABLE `nm_city`
  ADD PRIMARY KEY (`ci_id`);

--
-- Indexes for table `nm_cms_pages`
--
ALTER TABLE `nm_cms_pages`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `nm_cod_commission_paid`
--
ALTER TABLE `nm_cod_commission_paid`
  ADD PRIMARY KEY (`comPaid_id`);

--
-- Indexes for table `nm_cod_commission_tracking`
--
ALTER TABLE `nm_cod_commission_tracking`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `nm_color`
--
ALTER TABLE `nm_color`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `nm_contact`
--
ALTER TABLE `nm_contact`
  ADD PRIMARY KEY (`cont_id`);

--
-- Indexes for table `nm_country`
--
ALTER TABLE `nm_country`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `nm_coupon`
--
ALTER TABLE `nm_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_coupon_purchage`
--
ALTER TABLE `nm_coupon_purchage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_currency`
--
ALTER TABLE `nm_currency`
  ADD PRIMARY KEY (`cur_id`);

--
-- Indexes for table `nm_customer`
--
ALTER TABLE `nm_customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `nm_deals`
--
ALTER TABLE `nm_deals`
  ADD PRIMARY KEY (`deal_id`);

--
-- Indexes for table `nm_emailsetting`
--
ALTER TABLE `nm_emailsetting`
  ADD PRIMARY KEY (`es_id`);

--
-- Indexes for table `nm_enquiry`
--
ALTER TABLE `nm_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_estimate_zipcode`
--
ALTER TABLE `nm_estimate_zipcode`
  ADD PRIMARY KEY (`ez_id`);

--
-- Indexes for table `nm_faq`
--
ALTER TABLE `nm_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `nm_generalsetting`
--
ALTER TABLE `nm_generalsetting`
  ADD PRIMARY KEY (`gs_id`);

--
-- Indexes for table `nm_imagesetting`
--
ALTER TABLE `nm_imagesetting`
  ADD PRIMARY KEY (`imgs_id`);

--
-- Indexes for table `nm_image_sizes`
--
ALTER TABLE `nm_image_sizes`
  ADD PRIMARY KEY (`image_size_id`);

--
-- Indexes for table `nm_inquiries`
--
ALTER TABLE `nm_inquiries`
  ADD PRIMARY KEY (`iq_id`);

--
-- Indexes for table `nm_language`
--
ALTER TABLE `nm_language`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `id` (`lang_id`);

--
-- Indexes for table `nm_login`
--
ALTER TABLE `nm_login`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `nm_maincategory`
--
ALTER TABLE `nm_maincategory`
  ADD PRIMARY KEY (`mc_id`),
  ADD KEY `mc_status` (`mc_status`);

--
-- Indexes for table `nm_merchant`
--
ALTER TABLE `nm_merchant`
  ADD PRIMARY KEY (`mer_id`),
  ADD KEY `mer_pro_status` (`mer_pro_status`),
  ADD KEY `mer_staus` (`mer_staus`),
  ADD KEY `mer_co_id` (`mer_co_id`),
  ADD KEY `mer_ci_id` (`mer_ci_id`),
  ADD KEY `mer_logintype` (`mer_logintype`);

--
-- Indexes for table `nm_merchant_overallorders`
--
ALTER TABLE `nm_merchant_overallorders`
  ADD PRIMARY KEY (`overOrd_id`);

--
-- Indexes for table `nm_modulesettings`
--
ALTER TABLE `nm_modulesettings`
  ADD PRIMARY KEY (`ms_id`);

--
-- Indexes for table `nm_newsletter_subscribers`
--
ALTER TABLE `nm_newsletter_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_order`
--
ALTER TABLE `nm_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_cus_id` (`order_cus_id`,`order_pro_id`,`order_status`,`delivery_status`);

--
-- Indexes for table `nm_ordercod`
--
ALTER TABLE `nm_ordercod`
  ADD PRIMARY KEY (`cod_id`);

--
-- Indexes for table `nm_order_auction`
--
ALTER TABLE `nm_order_auction`
  ADD PRIMARY KEY (`oa_id`);

--
-- Indexes for table `nm_order_delivery_status`
--
ALTER TABLE `nm_order_delivery_status`
  ADD PRIMARY KEY (`delStatus_id`);

--
-- Indexes for table `nm_order_payu`
--
ALTER TABLE `nm_order_payu`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_cus_id` (`order_cus_id`,`order_pro_id`,`order_status`,`delivery_status`);

--
-- Indexes for table `nm_order_stripe`
--
ALTER TABLE `nm_order_stripe`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_cus_id` (`order_cus_id`,`order_pro_id`,`order_status`,`delivery_status`);

--
-- Indexes for table `nm_paymentsettings`
--
ALTER TABLE `nm_paymentsettings`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `nm_procart`
--
ALTER TABLE `nm_procart`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `nm_procolor`
--
ALTER TABLE `nm_procolor`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `nm_product`
--
ALTER TABLE `nm_product`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `nm_prosize`
--
ALTER TABLE `nm_prosize`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `nm_prospec`
--
ALTER TABLE `nm_prospec`
  ADD PRIMARY KEY (`spc_id`);

--
-- Indexes for table `nm_referaluser`
--
ALTER TABLE `nm_referaluser`
  ADD PRIMARY KEY (`ruse_id`);

--
-- Indexes for table `nm_review`
--
ALTER TABLE `nm_review`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `nm_save_cart`
--
ALTER TABLE `nm_save_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `nm_secmaincategory`
--
ALTER TABLE `nm_secmaincategory`
  ADD PRIMARY KEY (`smc_id`),
  ADD KEY `smc_status` (`smc_status`),
  ADD KEY `smc_mc_id` (`smc_mc_id`);

--
-- Indexes for table `nm_secsubcategory`
--
ALTER TABLE `nm_secsubcategory`
  ADD PRIMARY KEY (`ssb_id`),
  ADD KEY `ssb_status` (`ssb_status`),
  ADD KEY `ssb_sb_id` (`ssb_sb_id`),
  ADD KEY `ssb_smc_id` (`ssb_smc_id`),
  ADD KEY `mc_id` (`mc_id`);

--
-- Indexes for table `nm_shipping`
--
ALTER TABLE `nm_shipping`
  ADD PRIMARY KEY (`ship_id`);

--
-- Indexes for table `nm_size`
--
ALTER TABLE `nm_size`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `nm_smtp`
--
ALTER TABLE `nm_smtp`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `nm_social_media`
--
ALTER TABLE `nm_social_media`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `nm_specification`
--
ALTER TABLE `nm_specification`
  ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `nm_spgroup`
--
ALTER TABLE `nm_spgroup`
  ADD PRIMARY KEY (`spg_id`);

--
-- Indexes for table `nm_store`
--
ALTER TABLE `nm_store`
  ADD PRIMARY KEY (`stor_id`),
  ADD KEY `stor_merchant_id` (`stor_merchant_id`),
  ADD KEY `stor_status` (`stor_status`);

--
-- Indexes for table `nm_subcategory`
--
ALTER TABLE `nm_subcategory`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `nm_subscription`
--
ALTER TABLE `nm_subscription`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `nm_terms`
--
ALTER TABLE `nm_terms`
  ADD PRIMARY KEY (`tr_id`);

--
-- Indexes for table `nm_theme`
--
ALTER TABLE `nm_theme`
  ADD PRIMARY KEY (`the_id`);

--
-- Indexes for table `nm_wishlist`
--
ALTER TABLE `nm_wishlist`
  ADD PRIMARY KEY (`ws_id`);

--
-- Indexes for table `nm_withdraw_request`
--
ALTER TABLE `nm_withdraw_request`
  ADD PRIMARY KEY (`wd_id`);

--
-- Indexes for table `nm_withdraw_request_paypal`
--
ALTER TABLE `nm_withdraw_request_paypal`
  ADD PRIMARY KEY (`wr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery_status_chat`
--
ALTER TABLE `delivery_status_chat`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_aboutus`
--
ALTER TABLE `nm_aboutus`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_add`
--
ALTER TABLE `nm_add`
  MODIFY `ad_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_admin`
--
ALTER TABLE `nm_admin`
  MODIFY `adm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_adminreply_comments`
--
ALTER TABLE `nm_adminreply_comments`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_auction`
--
ALTER TABLE `nm_auction`
  MODIFY `auc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_banner`
--
ALTER TABLE `nm_banner`
  MODIFY `bn_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_blog`
--
ALTER TABLE `nm_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_blogsetting`
--
ALTER TABLE `nm_blogsetting`
  MODIFY `bs_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_blog_cus_comments`
--
ALTER TABLE `nm_blog_cus_comments`
  MODIFY `cmt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_category_ad`
--
ALTER TABLE `nm_category_ad`
  MODIFY `cat_ad_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_category_banner`
--
ALTER TABLE `nm_category_banner`
  MODIFY `cat_bn_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_city`
--
ALTER TABLE `nm_city`
  MODIFY `ci_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_cms_pages`
--
ALTER TABLE `nm_cms_pages`
  MODIFY `cp_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `nm_cod_commission_paid`
--
ALTER TABLE `nm_cod_commission_paid`
  MODIFY `comPaid_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_cod_commission_tracking`
--
ALTER TABLE `nm_cod_commission_tracking`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_color`
--
ALTER TABLE `nm_color`
  MODIFY `co_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_contact`
--
ALTER TABLE `nm_contact`
  MODIFY `cont_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_country`
--
ALTER TABLE `nm_country`
  MODIFY `co_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_coupon`
--
ALTER TABLE `nm_coupon`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_coupon_purchage`
--
ALTER TABLE `nm_coupon_purchage`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_currency`
--
ALTER TABLE `nm_currency`
  MODIFY `cur_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_customer`
--
ALTER TABLE `nm_customer`
  MODIFY `cus_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nm_deals`
--
ALTER TABLE `nm_deals`
  MODIFY `deal_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_emailsetting`
--
ALTER TABLE `nm_emailsetting`
  MODIFY `es_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_enquiry`
--
ALTER TABLE `nm_enquiry`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nm_estimate_zipcode`
--
ALTER TABLE `nm_estimate_zipcode`
  MODIFY `ez_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_faq`
--
ALTER TABLE `nm_faq`
  MODIFY `faq_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_generalsetting`
--
ALTER TABLE `nm_generalsetting`
  MODIFY `gs_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_imagesetting`
--
ALTER TABLE `nm_imagesetting`
  MODIFY `imgs_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `nm_image_sizes`
--
ALTER TABLE `nm_image_sizes`
  MODIFY `image_size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_inquiries`
--
ALTER TABLE `nm_inquiries`
  MODIFY `iq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_language`
--
ALTER TABLE `nm_language`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_login`
--
ALTER TABLE `nm_login`
  MODIFY `log_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nm_maincategory`
--
ALTER TABLE `nm_maincategory`
  MODIFY `mc_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nm_merchant`
--
ALTER TABLE `nm_merchant`
  MODIFY `mer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_merchant_overallorders`
--
ALTER TABLE `nm_merchant_overallorders`
  MODIFY `overOrd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_modulesettings`
--
ALTER TABLE `nm_modulesettings`
  MODIFY `ms_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_newsletter_subscribers`
--
ALTER TABLE `nm_newsletter_subscribers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order`
--
ALTER TABLE `nm_order`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_ordercod`
--
ALTER TABLE `nm_ordercod`
  MODIFY `cod_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nm_order_auction`
--
ALTER TABLE `nm_order_auction`
  MODIFY `oa_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order_delivery_status`
--
ALTER TABLE `nm_order_delivery_status`
  MODIFY `delStatus_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order_payu`
--
ALTER TABLE `nm_order_payu`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_order_stripe`
--
ALTER TABLE `nm_order_stripe`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_paymentsettings`
--
ALTER TABLE `nm_paymentsettings`
  MODIFY `ps_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_procart`
--
ALTER TABLE `nm_procart`
  MODIFY `pc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_procolor`
--
ALTER TABLE `nm_procolor`
  MODIFY `pc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_product`
--
ALTER TABLE `nm_product`
  MODIFY `pro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nm_prosize`
--
ALTER TABLE `nm_prosize`
  MODIFY `ps_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `nm_prospec`
--
ALTER TABLE `nm_prospec`
  MODIFY `spc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_referaluser`
--
ALTER TABLE `nm_referaluser`
  MODIFY `ruse_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_review`
--
ALTER TABLE `nm_review`
  MODIFY `comment_id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_save_cart`
--
ALTER TABLE `nm_save_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_secmaincategory`
--
ALTER TABLE `nm_secmaincategory`
  MODIFY `smc_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nm_secsubcategory`
--
ALTER TABLE `nm_secsubcategory`
  MODIFY `ssb_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_shipping`
--
ALTER TABLE `nm_shipping`
  MODIFY `ship_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nm_size`
--
ALTER TABLE `nm_size`
  MODIFY `si_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_smtp`
--
ALTER TABLE `nm_smtp`
  MODIFY `sm_id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_social_media`
--
ALTER TABLE `nm_social_media`
  MODIFY `sm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_specification`
--
ALTER TABLE `nm_specification`
  MODIFY `sp_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_spgroup`
--
ALTER TABLE `nm_spgroup`
  MODIFY `spg_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_store`
--
ALTER TABLE `nm_store`
  MODIFY `stor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nm_subcategory`
--
ALTER TABLE `nm_subcategory`
  MODIFY `sb_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_subscription`
--
ALTER TABLE `nm_subscription`
  MODIFY `sub_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_terms`
--
ALTER TABLE `nm_terms`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_theme`
--
ALTER TABLE `nm_theme`
  MODIFY `the_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_wishlist`
--
ALTER TABLE `nm_wishlist`
  MODIFY `ws_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_withdraw_request`
--
ALTER TABLE `nm_withdraw_request`
  MODIFY `wd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nm_withdraw_request_paypal`
--
ALTER TABLE `nm_withdraw_request_paypal`
  MODIFY `wr_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
