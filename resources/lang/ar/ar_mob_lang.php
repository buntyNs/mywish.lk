<?php

return

    [

        /*Front end static pages*/

        /*Home page started*/

        'LOGO'  => 'شعار',

        'CATEGORIES' => 'الاقسام',

        'MORE_CATEGORIES' => 'المزيد من التصنيفات',

        'NEW_PRODUCTS' => 'نتائج البحث',

        'COMPARE' => 'قارن',

        'SOLD' => 'تم البيع',

        'ADD_TO_CART' => 'أضف إلى السلة',

        'NO_PRODUCTS_AVAILABLE' => 'لا توجد منتجات متاحة',

        'CLOSE_COMPARISON_OVERLAY' => 'وثيقة المقارنة تراكب',

        'HI' => 'مرحبا',

        'PAYMENT_STATUS' => 'حالة السداد',

        'MY_DEAL_COD' => 'صفقتي COD',

        'MY_DEAL_PAYPAL' => 'صفقتي PayPal',

        'MY_PRODUCT_PAYPAL' => 'المنتج الخاص بي PayPal',

        'MY_PRODUCT_COD' => 'المنتج الخاص بي COD',

        'DEAL_TITLE' => 'عنوان الصفقة',

        'IMAGE' => 'صورة',

        'NO_PRODUCTS_INCOMPARE'=>'لا توجد منتجات للمقارنة',

        'DEALS_OF_THE_DAY' => 'فقات اليوم',

        'TOP_OFFERS' => 'أعلى العروض',

        'MOST_POPULAR_PRODUCTS'=>'المنتج الأكثر شعبية',

        'REVIEWS_AND_RATINGS' => 'التعليقات والتقييمات',

        'NO_RATING_FOR_THIS_PRODUCT'=>'لا تقييم لهذا المنتج',

        'AVAILABLE_STOCK'=> 'المخزون المتوفر',

        'CLICK_HERE' => 'انقر هنا',

        'NO_ORDER_COD'=>'لا يوجد ترتيب في COD',

        'NO_ORDER_PAYPAL'=>'لا يوجد ترتيب في Paypal',

        'NO_ORDER_DEAL_COD'=>'لا يوجد أي صفقة الطلب في COD',

        'NO_ORDER_DEAL_PAYPAL'=>'لا يوجد أي صفقة الطلب في Paypal',

        'NO_WHISLIST'=>'لا يوجد منتج في الأماني',

        /*End Home page*/

        /*Footer page start*/

        'REQUEST_FOR_ADVERTISEMENT' => 'طلب إعلان',

        'AD_TITLE' => 'عنوان الاعلان',

        'ADS_POSITION' => 'موضع الإعلانات',

        'SELECT_POSITION' => 'حدد الموقف',

        'HEADER_RIGHT' => 'رأس الحق',

        'LEFT_SIDE_BAR' => 'شريط الجانب الأيسر',

        'BOTTOM_FOOTER' => 'أسفل الذيل',

        'PAGES' => 'صفحات',

        'SELECT_ANY_PAGE' => 'اختر أي صفحة',

        'HOME' => 'الصفحة الرئيسية',

        'SPORTS' => 'رياضات',

        'ELECTRONICS' => 'إلكترونيات',

        'FLOWER_POT' => 'اناء للزهور',

        'HEALTH' => 'الصحة',

        'BEAUTY' => 'جمال',

        'REDIRECT_URL' => 'إعادة توجيه URL',

        'UPLOAD_IMAGES' => 'تحميل الصور',

        'UPLOAD' => 'رفع',

        'COUPON_AMOUNT' => 'مبلغ القسيمة',

        'CLOSE' => 'قريب',

        'CONTACT' => 'اتصل بنا',

        'CONTACT_US' => 'اتصل بنا',

        'ABOUT_COMPANY' => 'عن الشركة',

        'BLOG' => 'مدونة',

        'ABOUT_US' => 'معلومات عنا',

        'MERCHANT_LOGIN' => 'دخول المتجول',

        'REGISTER' => 'تسجيل',

        'LOGIN' => 'تسجيل الدخول',

        'SOCIAL_MEDIA' => 'الاعلام الاجتماعي',

        'NEWS_LETTER_SUBSCRIPTION' => 'الاشتراك في النشرة الإخبارية',

        'SUBSCRIBE_TO_RECEIVE_THE_LATEST_NEWS_STRAIGHT_TO_YOUR_INBOX._BY_SUBSCRIBING_YOU_WILL_BE_ABLE_TO' => 'اشترك لتلقي آخر الأخبار مباشرة إلى صندوق البريد الوارد الخاص بك. عن طريق الاشتراك سوف تكون قادرة على',

        'SUBSCRIBE_HERE' => 'اشترك هنا',

        'PAYMENT_METHOD' => 'طريقة الدفع او السداد',

        'OUR_SERVICES' => 'خدماتنا',

        'TERMS_&_CONDITIONS' => 'البنود و الظروف',

        'HELP' => 'مساعدة',

        'ALL_RIGHTS_RESERVED' => 'كل الحقوق محفوظة',

        'SUCCESSFULLY_SUBSCRIBED' => 'اشتركت بنجاح',

        'ITEMS_IN_YOUR_CART' => 'الوحدات الموجودة فى سلة التسوق الخاصة بك',

        'SELECT_CATEGORY' => 'اختر الفئة',

        'DEALS' => 'صفقات',

        'SOLD_OUT' => 'بيعت كلها',

        'STORES' => 'مخازن',

        'NEAR_BY_STORE' => 'بالقرب من المتجر',

        'SEARCH_PRODUCT_NAME' => 'بحث المنتج / اسم الصفقة',

        'CUSTOMER_SUPPORT' => 'دعم العملاء',

        /*End footer page*/

        /*Navbar started*/

        'FORGOT_PASSWORD' => 'هل نسيت كلمة المرور',

        'CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP' => 'قم بتوصيل حساب Facebook الخاص بك للاشتراك',

        'DONT_HAVE_AN_ACCOUNT_YET' => 'لا تملك حسابا حتى الآن',

        'SIGN_UP' => 'سجل',

        'E-MAIL' => 'البريد الإلكتروني',

        'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO' => 'تسجيل الدخول باستخدام حساب Facebook الخاص بك: قم بتوصيل حساب Facebook الخاص بك لتسجيل الدخول إلى',

        'SIGN_UP_WITH_FACEBOOK' => 'اشترك عبر حساب فايسبوك',

        'ALREADY_A_MEMBER' => 'عضوا فعلا',

        'RESET_PASSWORD' => 'إعادة ضبط كلمة المرور',

        'NEW_PASSWORD' => 'كلمة السر الجديدة',

        'CONFIRM_PASSWORD' => 'تأكيد كلمة المرور',

        'SELECT_COUNTRY' => 'حدد الدولة',

        'SELECT_CITY' => 'اختر مدينة',

        'BY_CLICKING_SIGN_UP_YOU_AGREE_TO' => 'بالنقر على "تسجيل" ، فإنك توافق على ذلك',

        'TERMS_AND_CONDITIONS' => 'الأحكام والشروط',

        'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO_LARAVEL_ECOMMERCE_MULTIVENDOR' => 'تسجيل الدخول باستخدام حساب facebook الخاص بك: قم بتوصيل حساب facebook الخاص بك لتسجيل الدخول إلى Laravel Ecommerce Multivendor',

        'LOG_IN_WITH_FACEBOOK' => 'تسجيل الدخول باستخدام الفيسبوك',

        'SIGN_IN' => 'تسجيل الدخول',

        'HELPER::CUSTOMER_SUPPORT_NUMBER' => 'المساعد :: رقم دعم العملاء',

        'MIMIMUM_6_CHARACTERS' => 'الحد الأقصى 6 أحرف',

        'ENTER_YOUR_EMAIL' => 'أدخل بريدك الالكتروني',

        'ENTER_YOUR_NEW_PASSWORD' => 'أدخل كلمة المرور الجديدة',

        'CONFIRM_YOUR_NEW_PASSWORD' => 'تأكيد كلمة المرور الجديدة',

        'ENTER_YOUR_NAME_HERE' => 'أدخل اسمك هنا',

        'ENTER_YOUR_EMAIL_HERE' => 'أدخل عنوان بريدك الإلكتروني هنا',

        'SUBMIT' => 'خضع',

        'CANCEL' => 'إلغاء',

        'LOGIN_SUCCESSFULLY' => 'تسجيل الدخول بنجاح',

        'INVALID_LOGIN_CREDENTIALS' => 'اعتماد تسجيل الدخول غير صالح',

        'ALREADY_EMAIL_EXISTS' => 'البريد الإلكتروني موجود بالفعل',

        'PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS' => 'البريد الإلكتروني موجود بالفعل',

        'EMAIL_ID_DOES_NOT_EXIST' => 'معرف البريد الإلكتروني غير موجود',

        'PASSWORD_CHANGED_SUCCESS' => 'كلمة المرور تغيرت النجاح',

        'INVALID_USER' => 'مستخدم غير صالح',

        /*Navbar started*/

        /*Start loginnav bar*/

        'WELCOME' => 'أهلا بك',

        'MY_WISHLIST' => 'قائمة امنياتي',

        'LOG_OUT' => 'الخروج',

        /*End lagin Nav bar*/

        /*Start About Us.php page*/



        /*End About Us.php page*/

        /*Start App.php page*/

        'LARAVEL' => 'Laravel',

        'TOGGLE_NAVIGATION' => 'تبديل التنقل',

        /*End App.php page*/

        /*Start Action blade.php*/

        'SPECIALS' => 'العروض الخاصة',

        'LAST_BIDDER' => 'آخر مزايد',

        'BIT_AMOUNT' => 'كمية البت',

        'BID_NOW' => 'المزايدة الآن',

        'MORE_FILTERS' => 'المزيد من الفلاتر',

        'RESET' => 'إعادة تعيين',

        'PER_PAGE' => 'لكل صفحة',

        'VIEW_ALL' => 'عرض الكل',

        'LIKES_ASC' => 'يحب تصاعدي',

        'SORT_BY' => 'ترتيب حسب',

        'PRICE_LOW' => 'السعر منخفض',

        'HIGH' => 'متوسط',

        'PRICE_HIGH' => 'سعر مرتفع',

        'LOW' => 'منخفض',

        'TITLE' => 'عنوان',

        'A' => 'A',

        'Z' => 'Z',

        'DESCRIPTION' => 'وصف',

        'PAGE' => 'صفحة',

        'OF' => 'من',

        'NO_RESULTS_FOUND' => 'لا توجد نتائج',

        'LIST_ACTIONS' => 'قائمة الإجراءات',

        'LIKES_DESC' => 'يحب ديس',

        'DATE_ASC' => 'تاريخ تصاعدي',

        'DATE_DESC' => 'التاريخ desc',

        'STYLE_SELECTOR' => 'اختيار النمط',

        'OREGIONAL_SKIN' => 'جلد اورينتال',

        'BOOTSWATCH_SKINS' => 'أحذية ووتش جلود',

        'THESE_ARE_JUST' => 'هذه مجرد أمثلة ويمكنك بناء نظام الألوان الخاص بك في الخلفية',

        'FILTER_BY_TITLE' => 'تصفية حسب العنوان',

        /*End Action blade.php*/

        /*Start Actionview blade.php*/

        'AUCTION' => 'مزاد علني',

        'DAYS' => 'أيام',

        'HOURS' => 'ساعات',

        'MIN' => 'دقيقة',

        'SEC' => 'ثانية',

        'BID_NOW' => 'المزايدة الآن',

        'BID_FROM' => 'محاولة من',

        'BID_INCREMENT' => 'زيادة العطاء',

        'RETAIL_PRICE' => 'سعر التجزئة',

        'BID_HISTORIES' => 'تاريخ العروض',

        'LATEST_BIDDER(S)' => 'أحدث مزايد (s)',

        'NOT_YET_BID' => 'أحدث مزايد (عروض)',

        'PRODUCT_DETAILS' => 'تفاصيل المنتج',

        'RELATED_PRODUCTS' => 'منتجات ذات صله',

        'PRODUCT_INFORMATION' => 'وصف المنتج',

        'PRODUCT_SPECIFICATION'=> 'مواصفات المنتج',

        'CATEGORY' => 'الفئة',

        'FEATURES' => 'المميزات',

        'NEW' => 'الجديد',

        'AVAILABLE' => 'متاح',

        'VIEW_DETAILS' => 'عرض التفاصيل',

        'YOUR_BID_AMOUNT_IS_NO_LONGER_ENOUGHTO_WIN' => 'مبلغ المزايدة الخاص بك لم يعد كافيا للفوز',

        'PLEASE_RE-ENTER_YOUR_BID' => 'يرجى إعادة إدخال العرض الخاص بك',

        'NEW_MAX_BID' => 'جديد ماكس مزايدة',

        'OR_MORE' => 'او اكثر',

        'TWITTER_TWEET_BUTTON' => 'تويتر زر Tweet',

        /*End Actionview blade.php */

        /*start bid_payment blade.php*/

        'BID_AMOUNT' => 'قيمة المزايدة',

        'BID_SUMMARY' => 'ملخص العطاء',

        'YOUR_BID_AMOUNT' => 'لديك مبلغ العرض',

        'SHIPPING_AMOUNT' => 'كمية الشحن',

        'YOUR_SHIPPING_ADDRESS' => 'عنوان الشحن الخاص بك',

        'GOOD_NEWS' => 'أخبار جيدة',

        'A_PRODUCT_OF_NEXEMERCHANT' => 'منتج من NexEmerchant. بيع منتجات متخصصة للهنود الذين يعيشون في أستراليا',

        'BACKGROUND_PATTERNS' => 'أنماط الخلفية',

        'AMELIA' => 'اميليا',

        'SPRUCE' => 'تأنق',

        'SUPERHERO' => 'خارقة',

        /*End bid_payment blade.php*/

        /*start blog.blade.php*/

        'POSTED_BY' => 'منشور من طرف',

        'ADMIN' => 'مشرف',

            'COMMENTS' => 'تعليقات',

        'CONTINUE_READING' => 'أكمل القراءة',

        'NO_BLOGS_AVAILABLE' => 'لا توجد المدونات المتاحة',

        'POPULAR_POSTS' => 'منشورات شائعة',

        /*End blog.blade.php*/

        /*Start blogcomment.blade.php*/

        'YOU_HAVE_ERRORS_WHILE_PROVIDING_COMMENT' => 'لديك أخطاء أثناء تقديم التعليق',

        'CUSTOMER_COMMENTS' => 'تعليقات العملاء',

        'LEAVE_A_REPLY' => 'اترك رد',

        'WEBSITE' => 'موقع الكتروني',

        'ENTER_YOUR_NAME' => 'أدخل أسمك',

        'ENTER_YOUR_EMAIL_ID' => 'أدخل معرف البريد الإلكتروني الخاص بك',

        'ENTER_YOUR_LINK' => 'أدخل الرابط الخاص بك',

        'ENTER_YOUR_MESSAGE' => 'أدخل رسالتك',

        'EMAIL_ID'   					=> 	'عنوان الايميل',

        /*End blogcomment.blade.php*/

        /*Start blog view.blade.php*/



        /*End blog view.blade.php*/

        /*Start cart.blade.php*/

        'SHOPPING_CART' => 'عربة التسوق',

        'ITEM(S)' => 'بند(s)',

        'CONTINUE_SHOPPING' => 'مواصلة التسوق',

        'USER_COUPON' => 'قسيمة المستخدم',

        'PRODUCT_COUPON' => 'قسيمة المنتج',

        'S.NO' => 'S.No',

        'PRODUCT' => 'المنتج',

        'COLOR' => 'اللون المتوفر',

        'SIZE' => 'الحجم متوفر',

        'QUANTITY' => 'كمية',

        'UPDATE' => 'تحديث',

        'REMOVE' => 'إزالة',

        'PRICE' => 'السعر',

        'SUB_TOTAL' => 'المجموع الفرعي',

        'COUPON_CODE' => 'رمز القسيمة',

        'N' => 'N',

        'ESTIMATE_YOUR_SHIPPING' => 'تقدير الشحن الخاص بك',

        'CHECK_PRODUCT_AVAILABILITY_AT_YOUR_LOCATION' => 'تحقق من توافر المنتجات في موقعك',

        'POST_CODE' => 'الرمز البريدي',

        'ZIPCODE' => 'الرمز البريدي',

        'VERIFY' => 'التحقق',

        'NO_ITEMS_IN_CART' => 'لا توجد عناصر في العربة',

        'PROCEED_TO_CHECKOUT' => 'باشرالخروج من الفندق',

        'SPECIAL_COUPON_CODE' => 'رمز القسيمة الخاص',

        'PLEASE_FILL_ALL_FIELDS' => 'لو سمحت أملأ كل الحقول',

        'ENTER_CORRECT_COUPON_CODE' => 'أدخل رمز القسيمة الصحيح',

        'APLICAPLE_ONLY' => 'ينطبق فقط',

        'QUANTITY_OF_PRODUCT' => 'كمية من المنتج',

        'USE_THIS_COUPON_ONLY_FOR' => 'استخدم هذه القسيمة فقط من أجل',

        'COUPON_IS_NOT_VALID' => 'الكوبون غير صالح',

        'COUPON_DATE_NOT_STARTED' => 'تاريخ القسيمة لم تبدأ',

        'COUPON_DATE_EXPIRED' => 'تاريخ القسيمة منتهية الصلاحية',

        'COUPON_CODE_ALREADY_EXIST' => 'رمز القسيمة بالفعل موجود',

        'ALL_COUPONS_ARE_PURCHASED' => 'يتم شراء جميع الكوبونات',

        'YOUR_COUPON_LIMIT_EXIT' => 'حد القسيمة الخاص بك',

        'CANNOT_APPLY_MULTIPLE_PRODUCT_COUPON_IN_SAME_CART' => 'لا يمكن تطبيق قسيمة منتج متعددة في العربة نفسها',

        'PLEASE_ENTER_VALID_COUPON_CODE' => 'يرجى إدخال رمز القسيمة ساري المفعول',

        'PLEASE_TRY_AGAIN_ITS_NOT_VALID_COUPON_CODE_APPLIED_FOR_YOU' => 'حاول مرة اخرى. قانون القسيمة غير صالح التطبيقية بالنسبة لك',

        'NOT_HAVE_SUFFICIENT_CART' => 'ليس لديك قيمة كافية عربة كافية لتطبيق قانون القسيمة',

        'COUPON_CODE_ALREADY_EXSIST' => 'رمز القسيمة بالفعل موجود',

        'YOUR_COUPON_NOT_STARTED' => 'الكوبون الخاص بك لم تبدأ',

        'APPLY' => 'تطبيق',

        'CANCEL_USER_COUPON' => 'إلغاء قسيمة المستخدم',

        'USER_COUPON1' => 'قسيمة المستخدم',

        'PRODUCT_COUPON1' => 'قسيمة المنتج',

        /*End cart.blade.php*/

        /*start categorylist.blade.php*/

        'MOST_VISITED_PRODUCT' => 'الأكثر زيارة المنتج',

        /*End categorylist.blade.php*/

        /*start category list all.blade.php*/

        'ALL_CATEGORIES' => 'جميع الفئات',

        /*End category list all.blade.php*/

        /*Start Checkout.blade.php*/

        'CHECKOUT' => 'الدفع',

        'SHIPPING_ADDRESS' => 'عنوان الشحن',

        'LOAD_SHIPPING_ADDRESS' => 'تحميل عنوان الشحن للحصول على تفاصيل الملف الشخصي',

        'YES' => 'نعم فعلا',

        'NO' => 'لا',

        'NAME_FIELD_IS_REQUIRED' => 'حقل الاسم مطلوب',

        'ADDRESS_LINE1' => 'العنوان السطر 1',

        'ADDRESS_FIELD_IS_REQUIRED' => 'حقل العنوان مطلوب',

        'ADDRESS_LINE2' => 'سطر العنوان 2',

        'CITY' => 'مدينة',

        'STATE' => 'مكان',

        'COUNTRY' => 'بلد',

        'PHONE_NUMBER' => 'رقم الهاتف',

        'PHONE_NUMBER_FIELD_IS_REQUIRED' => 'حقل رقم الهاتف مطلوب',

        'SELECT_PAYMENT_METHOD' => 'اختار طريقة الدفع',

        'PAYPAL' => 'Paypal',

        'CASH_ON_DELIVERY' => 'الدفع عن الاستلام',

        'ORDER_SUMMARY' => 'ملخص الطلب',

        'SHIPMENT' => 'شحنة',

        'ESTIMATED_DELIVERY' => 'Estimated Delivery',

        'SHIPPING' => 'الشحن',

        'TYPE_OF_COUPON' => 'نوع الكوبون',

        'VALUE' => 'القيمة',

        'DEAL_DETAILS' => 'تفاصيل الصفقة',

        'USE_WALLET' => 'استخدم المحفظة',

        'ORDER_SUBTOTAL' => 'اطلب المجموع الفرعي',

        'ORDER_SHIPPING' => 'طلب الشحن',

        'ORDER_TAX' => 'طلب الضريبة',

        'ORDER_TOTAL' => 'الطلب الكلي',

        'PLACE_ORDER' => 'مكان الامر',

        'NO_ORDERS_PLACED' => 'لا توجد طلبات',

        'ENTER_FIRST_NAME' => 'أدخل الاسم الأول',

        'ENTER_LAST_NAME' => 'إدخال اسم آخر',

        'ENTER_ADDRESS_LINE1' => 'أدخل سطر العنوان 1',

        'ENTER_ADDRESS_LINE2' => 'أدخل سطر العنوان 2',

        'ENTER_YOUR_CITY' => 'أدخل مدينتك',

        'ENTER_YOUR_STATE' => 'أدخل موقعك',

        'ENTER_YOUR_PHONE_NO' => 'أدخل رقم هاتفك',

        'ENTER_YOUR_VALID_PHONE_NO' => 'أدخل رقم الهاتف الخاص بك',

        'ENTER_YOUR_ZIPCODE' => 'أدخل رمزك البريدي',

        'ENTER_YOUR_VALID_ZIPCODE' => 'أدخل الرمز البريدي الخاص بك صالح',

        'ENTER_YOUR_FIRST_NAME' => 'أدخل اسمك الأول',

        'ENTER_YOUR_LAST_NAME' => 'أدخل اسمك الأخير',

        'ENTER_YOUR_ADDRESS_LINE1' => 'أدخل سطر العنوان 1',

        'ENTER_YOUR_ADDRESS_LINE2' => 'أدخل سطر العنوان 2',

        'ENTER_YOUR_COUNTRY' => 'أدخل بلدك',

        'ENTER_YOUR_PHONENUMBER' => 'أدخل بلدك أدخل رقم هاتفك',

        'ENTER_ZIP_CODE' => 'أدخل الرمز البريدي',

        /*End Checkout.blade.php*/

        /*start cms blade.php*/

        'NO_DATA_FOUND' => 'لاتوجد بيانات',

        /*End cms blade.php*/

        /*start compare.blade.php*/



        /*end compare.blade.php*/

        /*start compare_product.blade.php*/

        'COMPARE_PRODUCTS' => 'قارن بين المنتجات',

        'ORIGIANL_PRICE' => 'السعر الأصلي',

        'DISCOUNT' => 'خصم',

        'RATING' => 'تقييم',

        'RATINGS' => 'تصنيفات',

        'DELIVERY_WITH_IN' => 'التسليم في',

        'SIZES' => 'الأحجام',

        'DESC' => 'تفاصيل',

        'SPEC' => 'المواصفات',

        'STORE_NAME' => 'اسم المتجر',

        'CLEAR_LIST' => 'لائحة خالية',

        /*start compare_product.blade.php*/

        /*start contactus.blade.php*/

        'CONTACT_DETAILS' => 'بيانات المتصل',

        'EMAIL_US' => 'راسلنا',

        'PLEASE_ENTER_YOUR_FIRSTNAME' => 'يرجى ادخال الاسم الاول',

        'PLEASE_ENTER_YOUR_LASTNAME' => 'يرجى إدخال اسم العائلة الخاص بك',

        'PLEASE_PROVIDE_A_PASSWORD' => 'يرجى تقديم كلمة مرور',

        'OUR_PASSWORD_MUST_BE_AT_LEAST_5_CHARACTERS_LONG' => 'يجب أن تتكون كلمة المرور الخاصة بنا من 5 أحرف على الأقل',

        'PLEASE_ENTER_THE_VALID_EMAIL_ADDRESS' => 'الرجاء إدخال عنوان البريد الإلكتروني الصحيح',

        'PLEASE_ACCEPT_OUR_POLICY' => 'يرجى قبول سياستنا',

        'ENTER_YOUR_NAME' => 'أدخل أسمك',

        'ENTER_YOUR_VALID_EMAIL_ID' => 'أدخل معرف صالح البريد الإلكتروني الخاص بك',

        'ENTER_YOUR_PHONE_NUMBER' => 'أدخل رقم هاتفك',

        'ENTER_QUERIES' => 'أدخل الاستعلامات',

        'SEND_MESSAGE' => 'ارسل رسالة',

        /*End contactus.blade.php*/

        /*Start customer_profile.blade.php*/

        'MY_PROFILE' => 'ملفي',

        'MY_ACCOUNT' => 'حسابي',

        'MY_COD_DETAILS' => 'بلدي COD التفاصيل',

        'MY_BUYS' => 'شراء بلدي',

        'MY_WISH_LIST' => 'قائمة امنياتي',

        'MY_SHIPPING_ADDRESS' => 'عنوان الشحن الخاص بي',

        'EDIT' => 'تصحيح',

        'PROFILE_IMAGES' => 'صور الملف الشخصي',

        'CANCEL' => 'إلغاء',

        'SELECT_COUNTRY' => 'حدد الدولة',

        'SELECT_CITY' => 'اختر مدينة',

        'TOTAL_WALLET_BALANCE_AMOUNT' => 'إجمالي رصيد رصيد المحفظة',

        'ORDERID' => 'رقم التعريف الخاص بالطلب',

        'TOT._PRODUCT' => 'إجمالي المنتج',

        'TOTAL_ORDER_AMOUNT' => 'مجموع مبلغ الطلب',

        'ORDER_DATE' => 'مجموع مبلغ الطلب',

        'USED_WALLET' => 'محفظة مستعملة',

        'STATUS' => 'الحالة',

        'INVOICE' => 'فاتورة',

        'TAX_INVOICE' => 'فاتورة ضريبية',

        'CASH_ON_DELIVERY' => 'الدفع عن الاستلام',

        'AMOUNT_PAID' => 'المبلغ المدفوع',

        'INCLUSIVE_OF_ALL_CHARGES' => 'تشمل جميع الرسوم',

        'SHIPPING_ADDRESS' => 'عنوان الشحن',

        'INVOICE_DETAILS' => 'تفاصيل الفاتورة',

        'THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS' => 'هذه الشحنة تحتوي على العناصر التالية',

        'PRODUCT_TITLE' => 'عنوان المنتج',

        'ORIGINAL_PRICE' => 'السعر الأصلي',

        'SHIPMENT_VALUE' => 'قيمة الشحن',

        'WALLET' => 'محفظة نقود',

        'AMOUNT' => 'كمية',

        'PRODUCT_NAMES' => 'أسماء المنتجات',

        'PRODUCT_PRICE' => 'سعر المنتج',

        'PRODUCT_IMAGE' => 'صورة المنتج',

            'ACTION' => 'عمل',

        'ALL_FIELDS_ARE_MANDATORY' => 'كل الحقول إلزامية',

        'FULL_NAME' => 'الاسم الكامل',

        'ADDRESS2' => 'العنوان 2',

        'DEALS_NAMES' => 'أسماء الصفقات',

        'CHANGE_PROFILE_PICTURE' => 'تغيير صورة الملف الشخصي',

        'IMAGE_UPLOAD_SIZE_1' => 'حجم تحميل الصور 1[MB]',

        'PLEASE_PROVIDE_PHONENUMBER' => 'يرجى تقديم رقم الهاتف',

        'PHONENUMBER_CHANGED_SUCCESSFULLY' => 'تم تغيير رقم الهاتف بنجاح',

        'PLEASE_PROVIDE_ANY_ONE_OF_THE_ADDRESS_FIELDS' => 'يرجى تقديم أي واحد من حقول العنوان',

        'ADDRESS_CHANGED_SUCCESSFULLY' => 'تم تغيير العنوان بنجاح',

        'CITY_AND_COUNTRY_CHANGED_SUCCESSFULLY' => 'تم تغيير المدينة والبلد بنجاح',

        'PLEASE_PROVIDE_VALID_PHONE_NUMBER' => 'يرجى تقديم رقم هاتف صالح',

        'SHIPPING_DETAILS_UPDATED_SUCCESSFULLY' => 'تفاصيل الشحن المحدثة بنجاح',

        'COUNTRY_CHANGED_SUCCESSFULLY' => 'البلد تغيرت بنجاح',

        'ENTER_YOUR_OLD_PASSWORD' => 'أدخل كلمة المرور القديمة الخاصة بك',

        'ENTER_YOUR_NEW_PASSWORD' => 'أدخل كلمة المرور الجديدة',

        'ENTER_CONFIRM_PASSWORD' => 'أدخل تأكيد كلمة المرور',

        'FRUIT_BALL' => 'كرة الفاكهة',

        'ENTER_YOUR_PHONE_NUMBER' => 'أدخل رقم هاتفك',

        'PROVIDE_ADDRESS1' => 'توفير العنوان 1',

        'PROVIDE_ADDRESS2' => 'تقديم العنوان 2',

        'ENTER_YOUR_ADDRESS' => 'أدخل عنوانك',

        'ENTER_YOUR_MOBILE_NUMBER' => 'أدخل رقم هاتفك المحمول',

        'ENTER_YOUR_EMAIL_ID' => 'أدخل معرف البريد الإلكتروني الخاص بك',

        'ENTER_YOUR_ZIP_CODE' => 'أدخل رمزك البريدي',

        'UPDATE' => 'تحديث',

        'IMAGE_FIELD_REQUIRED' => 'حقل الصورة مطلوب',

        'NAME_UPDATED_SUCCESSFULLY' => 'تم تحديث الاسم بنجاح',

        'PASSWORD_CHANGED_SUCCESSFULLY' => 'تم تغيير الرقم السري بنجاح',

        'BOTH_PASSWORDS_DO_NO_MATCH' => 'كلا كلمات المرور غير متطابقة',

        'OLD_PASSWORD_DOES_NOT_MATCH' => 'كلمة المرور القديمة غير متطابقة',

        'MOBILE' => 'التليفون المحمول',

        /*End customer_profile.blade.php*/

        /*Start deals.blade.php*/

        'MOST_VISITED_DEALS' => 'أكثر العروض التي تمت زيارتها',

        'NO_DEALS_AVAILABLE' => 'لا توجد صفقات متاحة',

        'SHIPPING_DELIVERY' => 'الشحن و التسليم',

        /*End deals.blade.php*/

        /*start deals_shipping_details.blade.php*/

        'PRODUCTS_NAME' => 'اسم المنتجات',

        'DATE' => 'تاريخ',

        'DETAILS' => 'تفاصيل',

        'DELIVERY_STATUS' => 'حالة التسليم',

        'EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE' => 'مثال على نص المساعدة على مستوى الكتلة هنا',

        'SEND' => 'إرسال',

        'SENNEX_ECARTD' => 'نيكس ه العربة',

        /*end Deals_shipping_details.blade.php*/

        /*Start enquirey.blade.php*/

        'ENTER_SUBJECT' => 'أدخل الموضوع',

        /*end enquirey.blade.php*/

        /*Start dealview.blade.php*/

        'RELATED_DEALS'      			=> 		'الصفقات ذات الصلة',

        'OUT_OF_STOCK' 					=> 		'إنتهى من المخزن',

        'OFFER' 						=> 		'عرض',

        'DICOUNT' 						=> 		'خصم',

        'YOU_SAVE' 						=> 		'أنت أحفظ',

        'STORE_DETAILS' 				=> 		'تفاصيل المتجر',

        'WRITE_A_POST_COMMENTS' 		=> 		'اكتب تعليقًا نشرًا',



        'ENTER_COMMENT_TITLE' 			=> 		'أدخل عنوان التعليق',

        'STAR' 							=> 		'نجمة',

        'REVIEWS' 						=> 		'التعليقات',

        'NO_REVIEW_RATINGS' 			=> 		'لا تصنيف التقييمات',

        'WRITE_A_REVIEW' 				=> 		'أكتب مراجعة',

        'ENTER_COMMENTS_QUERIES' 		=> 		'أدخل تعليقاتك',

        'VIEW_STORE' 					=> 		'عرض المتجر',

        'PRATEEK' 						=> 		'Prateek',

        /*end dealview.blade.php*/

        /*start faq.blade.php*/

        'FAQ' 							=> 		'التعليمات',

        /*End faq.blade.php*/

        /*start forgptpassword.blade.php*/

        'HELLO' 						=> 		'مرحبا',

        'PASSWORD_RESET_LINK' 			=> 		'رابط إعادة تعيين كلمة المرور',

        'PASSWORD_LINK' 				=> 		'رابط كلمة المرور',

        'USERNAME' 						=> 		'اسم المستخدم',

        'PASSWORD_LINK' 				=> 		'رابط كلمة المرور',

        'PLEASE_CLICK_THIS_LINK_TO_RESET_YOUR_PASSWORD' 				=> 		'يرجى النقر على هذا الرابط لإعادة تعيين كلمة المرور الخاصة بك',



        /*end forgptpassword.blade.php*/

        /*start help.blade.php*/



        /*end help.blade.php*/

        /*start manage_deal_review.blade.php*/

        'MANAGE_REVIEWS' 				=> 		'إدارة الاستعراضات',

        'MANAGE_PRODUCTS' 				=> 		'إدارة المنتجات',

        'REVIW_TITLE' 	 				=> 		'مراجعة العنوان',

        'CUSTOMER_NAME' 	 			=> 		'اسم الزبون',

        'ACTIONS' 	 					=> 		'أفعال',

        'CLICK_FOR_PREVIOUS_MONTHS' 	=> 		'انقر للأشهر السابقة',

        'CLICK_FOR_NEXT_MONTHS' 		=> 		'انقر للأشهر القادمة',

        /*End manage_deal_review.blade.php*/

        /*start manage_dealcason_delivery_details.blade.php*/



        /*end manage_dealcason_delivery_details.blade.php*/

        /*start merchant_signup.blade.php*/

        'MERCHANT_SIGN_UP' 				=> 		'التاجر اشترك',

        'WELCOME_TO_MERCHANT_SIGN_UP' 	=> 		'مرحبًا بك في تسجيل التاجر',

        'CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE' 	=> 		'سيتم إرشادك الآن من خلال بضع خطوات لإنشاء متجرك الشخصي عبر الإنترنت',

        'CREATE_YOUR_STORE' 			=> 		'إنشاء متجرك',

        'ADDRESS1' 						=> 		'العنوان 1',

        'ADDRESS2' 						=> 		'العنوان 2',

        'ZIPCODE' 						=> 		'الرمز البريدي',

        'META_KEYWORDS' 				=> 		'كلمات دلالية',

        'META_DESCRIPTION' 				=> 		'ميتا الوصف',

        'UPLOAD_FILE' 					=> 		'رفع ملف',

        'ENTER_YOUR_STORE_LOCATION' 	=> 		'أدخل موقع المتجر الخاص بك',

        'GOOGLE_MAPS' 					=> 		'خرائط جوجل',

        'LATITUDE' 						=> 		'خط العرض',

        'LONGTITUDE' 					=> 		'خط الطول',

        'PERSONAL_DETAILS' 				=> 		'تفاصيل شخصية',

        'FIRST_NAME' 					=> 		'الاسم الاول',

        'LAST_NAME' 					=> 		'الكنية',

        'CONTACT_NUMBER' 				=> 		'رقم الاتصال',

        'PAYMENT_EMAIL' 				=> 		'دفع البريد الإلكتروني',

        'PAYMENT_EMAIL_KEY' 				=> 		'PayUMoney الرئيسية',

        'PAYMENT_EMAIL_SALT' 				=> 		'PayUMoney الملح',

        'NUMBERS_ONLY_ALLOWED' 			=> 		'أرقام مسموح بها فقط',

        'NOT_A_VALID_EMAIL_ADDRESS'     => 		'البريد الإلكتروني غير صالح',

        'PLEASE_ENTER_YOUR_STORE_NAME'  => 		'يرجى إدخال اسم متجرك',

        'PLEASE_ENTER_YOUR_PHONE_NO'  	=> 		'يرجى إدخال رقم هاتفك',

        'PLEASE_ENTER_ADDRESS1_FIELD'  	=> 		'يرجى إدخال حقل العنوان 1',

        'PLEASE_ENTER_ADDRESS2_FIELD'  	=> 		'يرجى إدخال حقل العنوان 2',

        'PLEASE_ENTER_ZIPCODE'  		=> 		'يرجى إدخال الرمز البريدي',

        'PLEASE_ENTER_META_KEYWORDS'  	=> 		'الرجاء إدخال الكلمات المفتاحية',

        'PLEASE_ENTER_META_DESCRIPTION' => 		'يرجى إدخال وصف التعريف',

        'PLEASE_ENTER_WEBSITE' 			=> 		'يرجى إدخال الموقع',

        'PLEASE_ENTER_LOCATION' 		=> 		'من فضلك ادخل الموقع',

        'PLEASE_ENTER_COMMISSION' 		=> 		'يرجى إدخال العمولة',

        'PLEASE_CHOOSE_YOUR_UPLOAD_FILE'=> 		'يرجى اختيار ملف التحميل الخاص بك',

        'PLEASE_SELECT_COUNTRY'         => 		'يرجى اختيار الدولة',

        'PLEASE_SELECT_CITY'         	=> 		'يرجى اختيار المدينة',

        'PLEASE_ENTER_AN_EMAIL_ADDRESS' => 		'الرجاء إدخال عنوان البريد الإلكتروني',

        'PLEASE_ENTER_YOUR_FIRST_NAME'  => 		'يرجى ادخال الاسم الاول',

        'PLEASE_ENTER_CITY' 		 	=> 		'من فضلك ادخل المدينة',

        'PLEASE_ENTER_ADDREESS1_FIELD'  => 		'يرجى إدخال حقل العنوان 1',

        'PLEASE_ENTER_PAYMENT_ACCOUNT'  => 		'يرجى إدخال حساب الدفع',

        'PLEASE_ENTER_LAST_NAME'  		=> 		'يرجى إدخال الاسم الأخير',

        'PLEASE_ENTER_PHONE_NO'  		=> 		'يرجى إدخال رقم الهاتف',

        'ENTER_YOUR_STORE_NAME'  		=> 		'أدخل اسم المتجر الخاص بك',

        'ENTER_YOUR_CONTACT_NUMBER'  	=> 		'أدخل رقم الاتصال الخاص بك',

        'ENTER_YOUR_ADDRESS1'  			=> 		'أدخل عنوانك 1',

        'ENTER_YOUR_ADDRESS2'  			=> 		'أدخل عنوانك 2',

        'ENTER_YOUR_ZIPCODE'  			=> 		'أدخل رمزك البريدي',

        'ENTER_YOUR_META_KEYWORDS'  	=> 		'أدخل كلماتك التعريفية',

        'ENTER_YOUR_META_DESCRIPTION'  	=> 		'أدخل وصفك الفوقية',

        'ENTER_YOUR_STORE_WEBSITE'  	=> 		'أدخل موقع الويب الخاص بك المتجر',

        'TYPE_YOUR_LOCATION_HERE'  	    => 		'اكتب موقعك هنا',

        'ENTER_YOUR_FIRST_NAME'  	    => 		'أدخل اسمك الأول',

        'ENTER_YOUR_LAST_NAME'  	    => 		'أدخل اسمك الأخير',

        'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS'  	=> 		'أدخل تفاصيل حسابك الدفع',

        'SEARCH'  						=> 		'بحث',

        'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY'  	=> 		'أدخل مفتاح الدفع Payumoney الخاص بك',

        'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT'  	=> 		'أدخل ملحقتك Payumoney الدفع',

        /*end merchant_signup.blade.php*/

        /*Start newsletter.blade.php*/

        'NEWS_LETTER'  					=> 		'رسالة إخبارية',

        'SUBSCRIBE_YOUR_MAIL_ID'  		=> 		'اشترك معرف البريد الخاص بك',

        'ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION'  		=> 		'أدخل معرف البريد الخاص بك للحصول على اشتراك البريد الإلكتروني',

        'SUBSCRIBE'  					=> 		'الاشتراك',

        'WELCOME_TO_SUBSCRIBE_YOUR_NEWS_LETTER_SUBSCRIPTION'  					=> 		'مرحبًا بك في الاشتراك في اشتراكك في الرسالة الإخبارية',

        /*end newsletter.blade.php*/

        /*start pages.blade.php*/

        /*end pages.blade.php*/

        /*start paymentresult.blade.php*/

        'PAYMENT_RESULT'  				=> 		'نتيجة الدفع',

        'YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED'  					=> 		'اكتملت عملية الدفع بنجاح',

        'PLEASE_NOTE_THE_TRANSACTION_DETAILS'  							=> 		'يرجى ملاحظة تفاصيل الصفقة',

        'YOUR_PAYMENT_PROCESS_FAILED'  	=> 		'فشلت عملية الدفع الخاصة بك',

        'YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR'  	=> 		'تم إيقاف عملية الدفع نظرًا لوجود بعض الأخطاء',

        'TRANSACTION_DETAILS'  			=> 		'تفاصيل الصفقه',

        'THANK_YOU_FOR_SHOPPING_WITH'  	=> 		'شكرا للتسوق مع',

        'PAYER_NAME'  					=> 		'اسم المدفوع',

        'TRANSACTIONID'  				=> 		'معرف المعاملة',

        'TOKENID'  						=> 		'الرمز المميز معرف',

        'PAYER_EMAIL'  					=> 		'البريد المدفوع',

        'PAYER_ID'  					=> 		'معرف الدافع',

        'ACKNOWLEDGEMENT'  				=> 		'إعتراف',

        'PAYERSTATUS'  					=> 		'حالة الدافع',

        'RODUCT_DETAILS_FOR_CURRENT_TRANSACTION'  					=> 		'تفاصيل المنتج للمعاملة الحالية',

        'PRODUCT_QUANTITY'  			=> 		'كمية المنتج',

        /*end paymentresult.blade.php*/

        /*Start paymentcod.blade.php*/

        'YOUR_ORDER_SUCCESSFULLY_PLACED'  			=> 		'طلبك وضعت بنجاح',

        'TRANSACTION_ID'  				=> 		'معرف المعاملة',

        'PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION'  				=> 		'تفاصيل المنتج للمعاملة الحالية',

        'SUB-TOTAL'  					=> 		'المجموع الفرعي',

        'SHIPPING_TOTAL'  				=> 		'مجموع الشحن',

        /*end paymentcod.blade.php*/

        /*start place_bid_payment.blade.php*/

        'CONGRATULATIONS'  				=> 		'تهانينا',

        'YOUR_BID_WAS_PLACED_SUCCESSFULLY'  				=> 		'تم تقديم عرضك بنجاح',

        'YOU_WILL_RECEIVE_AN_EMAIL_CONFIRMATION_SHORTLY'  				=> 		'سوف تتلقى رسالة تأكيد بالبريد الإلكتروني قريبا',

        'YOUR_BID_AMOUNT'  				=> 		'لديك مبلغ العرض',

        'ESTIMATED_SHIPPING_CHARGE'  	=> 		'تقدير رسوم الشحن',

        'AUCTION_TIME_REMAINING'    	=> 		'وقت المزاد المتبقي',

        'CLOSE_WINDOW'        	    	=> 		'أغلق النافذة',

            'ALTERNATIVE_INFORMATION'       => 		'إذا لم يتم تقديم أي معلومات بديلة خلال 24 ساعة من إغلاق المزاد ، سيتم استخدام بطاقة الائتمان الافتراضية الخاصة بالمزايدة الفائزة وعنوان إرسال الفواتير لشحن المكاسب',

        'CREDIT_CARD_WILL_NOT_BE_CHARGED'   => 		'لن يتم تحصيل أي رسوم من بطاقتك الائتمانية حتى انتهاء المزاد ، وسنقوم فقط بفرض الحد الأدنى للمبلغ اللازم للفوز',

        'RANSACTION_FEE' 			  	=> 		'سيتم إضافة رسوم معاملات بنسبة 3٪ بحد أدنى 1.99 دولار أمريكي و 9.99 دولارات أمريكية كحد أقصى لكل طلب في حالة الفوز',

        'ORDER_IS_CANCELLED' 			=> 		'إذا تم إلغاء طلبك بسبب رفض بطاقة الائتمان ، فسيتم تقييم رسوم 25 دولارًا أمريكيًا',

        'APPLICABLE_SALES'	 			=> 		'سيتم إضافة ضريبة المبيعات المطبقة إلى إلينوي',

        'TENNESSEE_ORDERS'	 			=> 		'أوامر تينيسي',

        /*end place_bid_payment.blade.php*/

        /*start products.blade.php*/

        'MOST_VISITED_PRODUCTS'	 		=> 		'المنتجات الأكثر زيارة',

        /*end products.blade.php*/

        /*start productview.blade.php*/

        'CLICK_TO_VIEW_COUPON_CODE'	 	=> 		'انقر لعرض رمز القسيمة',

        'CASHBACK'	 					=> 		'استرداد النقود',

        'ADD_TO_WISHLIST'	 			=> 		'أضف إلى قائمة الامنيات',

        'IN_STOCK'  		 			=> 		'في المخزن',

        'SELECT'  		 				=> 		'تحديد',

        'WRITE_A_REVIEW_POST'  		 	=> 		'أكتب مراجعة',

        'RATE_THIS_PRODUCT'             =>      'تقييم هذا المنتج',

        'REVIEW_THIS_PRODUCT'             =>      'مراجعة هذا المنتج',

        'LIMITED_QUANTITY_AVAILABLE'    => 		'كمية محدودة متاحة',

            'SELECT_COLOR'   				=> 		'إختر لون',

        'SELECT_SIZE'   				=> 		'أختر الحجم',

        'PRODUCT_ADDED_TO_WISHLIST'     => 		'وأضاف المنتج إلى قائمة الأماني',

        'PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST'     => 		'المنتج موجود بالفعل في قائمة الامنيات الخاصة بك',

        'POST_COMMENTS'     			=> 		'اكتب تعليقا',

        'SUBMIT_REVIEW'					=>      'خضع',

        'REVIEW_TITLE'                  =>      'مراجعة العنوان',

        'REVIEW_DESCRIPTION'            =>      'مراجعة الوصف',

        /*end productview.blade.php*/

        /*start register.blade.php*/

        'WELCOME_TO_USER_REGISTRATION'  => 		'مرحبًا بك في تسجيل المستخدم',

        'CREATE_NEW_USER_ACCOUNT'     	=> 		'إنشاء حساب مستخدم جديد',

        'ENTER_YOUR_PASSWORD'    	 	=> 		'ادخل رقمك السري',

        'BY_CLICKING_SIGNUP_I_AGREE_TO' => 		'من خلال النقر على الاشتراك ، أوافق على ذلك',

        'THIS_EMAIL_ID_ALREADY_REGISTERED'     => 		'هذا البريد الإلكتروني معرف مسجل بالفعل',

        'LESSER_MOBILE_NO'    	 		=> 		'أقل المحمول رقم',

        'CORRECT_MOB_NO'     			=> 		'صحيح الغوغاء لا',

        'ENTER_10_DIGITS_OF_MOBILE_NO'  => 		'أدخل 10 - أرقام من رقم الجوال',

        'CORRECT_MOBILE_NO'     		=> 		'المحمول الصحيح لا',

        'PLEASE_ENTER_VALID_MOBILE_NUMBER'     => 		'يرجى إدخال رقم هاتف محمول صحيح',

        /*end register blade.php*/

        /*start search.blade.php*/

        'SEARCH_FOR_PRODUCTS'     		=> 		'البحث عن المنتجات',

        'SEARCH_FOR_DEALS'     			=> 		'ابحث عن صفقات',

        /*end search.blade.php*/

        /*start shipping.blade.php*/

        'POSTAL_CODE'     				=> 		'الرمز البريدي',

        /*end shipping.blade.php*/

        /*Start Sold.blade.php*/

        'SOLD_PRODUCTS'     			=> 		'بيع المنتجات',

        'NO_RECORDS_FOUND_UNDER_PRODUCTS'     			=> 		'لا توجد سجلات تحت المنتجات',

        'SOLD_DEALS'     				=> 		'صفقات مباعة',

        'NO_RECORDS_FOUND_UNDER_DEALS'  => 		'لا توجد سجلات تحت صفقات',

        /*end sold.blade.php*/

        /*start storview.blade.php*/

        'NO_RATING_FOR_THIS_STORE'     	=> 		'لا يوجد تقييم لهذا المتجر',

        'MOBILE_NO'     				=> 		'رقم الموبايل',

        'NO_RECORDS_FOUND_UNDER'     	=> 		'لا توجد سجلات تحت',

        'PRODUCTS'     					=> 		'منتجات',

        'BRANCHES'     					=> 		'الفروع',

        'BACK_YOUR_DEAL_STORE_REVIEW_POST_SUCCESSFULLY' => 'تم التعليق بنجاح',

        /*end storview.blade.php*/

        /*start submission.blade.php*/

        'THANK_YOU_FOR_MERCHANT_REGISTRATION'     	=> 		'شكرا لك على تسجيل التاجر',

        'THANK_YOU_TO_MERCHANT_SIGN_UP'     		=> 		'شكرا لتوقيع التاجر',

        'YOU_GOT_YOUR_USERNAME_AND_PASSWORD'     	=> 		'لقد حصلت على اسم المستخدم وكلمة المرور من خلال بريدك الإلكتروني ، ويرجى العثور عليه والتحقق منه',

        /*start stores.blade.php*/

        'NO_STORES_FOUND'     			=> 		'لم يتم العثور على متاجر',

        /*end stores.php*/

        /*Front end static pages*/





#############################################  START EMAIL TEMPLATES  ###########################################





        /*start admin_passwordrecoverymail.blade.php*/

        'CONTACT_INFORMATION'     							=> 		'معلومات للتواصل',

        /*End admin_passwordrecoverymail.blade.php*/



        /*Start merchantmail.blade.php*/

        'MAIL_EMAIL_TEMPLATE'     							=> 		'قالب البريد الإلكتروني',

        'MAIL_HI'     										=> 		'مرحبا',

        'MAIL_YOUR_LOGIN_CREDENTIALS_ARE'     				=> 		'بيانات تسجيل الدخول الخاصة بك هي',

        'MAIL_USER_NAME'     								=> 		'اسم المستخدم',

        'MAIL_PASSWORD'     								=> 		'كلمه السر',

        'MAIL_LOGIN_YOUR_ACCOUNT'     						=> 		'تسجيل الدخول حسابك',

        'MOBILE_YOUR_MERCHANT_ACCOUNT_WAS_CREATED_SUCCESSFULLY'    => 		'تم إنشاء حساب التاجر الخاص بك بنجاح',



        /*End merchantmail.blade.php*/

        /*Start ordermail.blade.php*/

        'WALLET_APPLIED'     								=> 		'محفظة التطبيقية',

        'COUPON_APPLIED'     								=> 		'كوبون التطبيقية',

        'DISCOUNT_PRICE'     								=> 		'سعر الخصم',

        'ACTUAL_PRICE'     									=> 		'السعر الفعلي',

        /*End ordermail.blade.php*/



        /* Start edit_merchant_account.blade.php */

        'PASSWORD_RECOVERY_DETAILS_FOR_USER'=> 			'تفاصيل استرداد كلمة المرور للمستخدم',

        'USER_NAME'							=> 			'اسم المستخدم',

        'EMAIL_LINK'						=> 			'رابط البريد الإلكتروني',

        'PLEASE_CLICK_THE_LINK_TO_RESET_YOUR_PASSWORD'			=> 			'يرجى النقر على الرابط لإعادة تعيين كلمة المرور الخاصة بك',

        /* close edit_merchant_account.blade.php */



        /* Start edit_merchant_account.blade.php */

        'SUBSCRIPTION_EMAIL_CREATED_SUCCESSFULLY_FOR_YOUR_MAIL_ID'			=> 			'تم إنشاء بريد إلكتروني للاشتراك بنجاح لمعرف البريد الخاص بك',

        'THANK_YOU_CREDENTIALS_ARE'			=> 			'شكرا لك وثائق التفويض',

        'HAI_THANK_YOU'						=> 			'هاي شكرا لك',

        'SUBSCRIPTION_EMAIL_IN_LARAVEL_ECOMMERCE'			=> 			'البريد الإلكتروني الاشتراك في Laravel التجارة الإلكترونية',

        /* close edit_merchant_account.blade.php */



        /* Start registermail.blade.php */

        'YOUR_REGISTER_ACCOUNT_WAS_CREATED_SUCCESSFULLY'			=> 			'تم إنشاء حساب السجل الخاص بك بنجاح',

        'YOUR_LOGIN_CREDENTIALS_ARE'		=> 			'بيانات تسجيل الدخول الخاصة بك هي',

        'PASSWORD'							=> 			'كلمه السر',

        'LOGIN_YOUR_ACCOUNT'				=> 			'تسجيل الدخول حسابك',

        'LARAVEL_ECOMMERCE_ALL_RIGHTS_RESERVED'			=> 			'جميع الحقوق محفوظة لارادل التجارة الإلكترونية ',

        /* close registermail.blade.php */



        /* Start product_enquiry.blade.php */

        'PRODUCT_ENQUIRY'					=> 			'الإستفسار عن المنتج',

        'NAME'								=> 			'اسم',

        'PRODUCT_NAME'						=> 			'اسم المنتج',

        'MESSAGE'							=> 			'رسالة',

        /* close product_enquiry.blade.php */



        /* Start order-merchantmail.blade.php */

        'THANK_YOU_FOR_YOUR_ORDER'			=> 			'شكرا لطلبك',

        'YOUR_TRANSACTION_ID'				=> 			'معرف المعاملة الخاص بك',

        'VIEW_YOUR_ORDER'					=> 			'عرض طلبك',

        'S_NO'								=> 			'S.no',

        'QTY'								=> 			'الكمية',

        'ITEM_PRICE'						=> 			'سعر البند',

        'TAX'								=> 			'ضريبة',

            'SHIP_AMOUNT'						=> 			'كمية السفينة',

        'SUBTOTAL'							=> 			'حاصل الجمع',

        'TOTAL'								=> 			'مجموع',

        '2_TO_3_DAYS'						=> 			'2 إلى 3 أيام',

        'WILL_BE_DELIVERED_BY'				=> 			'سيتم تسليمها',

        'DELIVERY_ADDRESS'					=> 			'عنوان التسليم',

        'ITEM_NAME'							=> 			'اسم العنصر',

        'STATE'								=> 			'مكان',

        'ZIP_CODE'							=> 			'الرمز البريدي',

        'PHONE'								=> 			'هاتف',

        'ADDRESS'							=> 			'عنوان',

        'EMAIL'								=> 			'البريد الإلكتروني',

        'ANY_QUESTIONS'						=> 			'أي أسئلة',

        'GET_IN_TOUCH_WITH_OUR_24X7_CUSTOMER_CARE_TEAM'					=> 			'تواصل مع فريق خدمة العملاء على مدار 24 ساعة طوال أيام الأسبوع',

        'MEANWHILE_YOU_CAN_CHECK_THE_STATUS_OF_YOUR_ORDER_ON'				=> 			'في هذه الأثناء ، يمكنك التحقق من حالة الطلب الخاص بك',

        'WE_WILL_SEND_YOU_ANOTHER_EMAIL_ONCE_THE_ITEMS_IN_YOUR_ORDER_HAVE_BEEN_SHIPPED'					=> 			'سوف نرسل لك بريدًا إلكترونيًا آخر بمجرد شحن العناصر الموجودة في طلبك',

        /* close order-merchantmail.blade.php */

        #############################################  END EMAIL TEMPLATES  ########################################



#################################################### START CONTROLLER WORK ########################################



        /* Start RegisterController.php */

        'NO_DATAS_FOUND'									=> 		'لم يتم العثور على بيانات',

        /* close RegisterController.php */

        /* Start ServicelistingController.php */

        'ALREADY_EXISTS_IN_CART'							=> 		'موجود بالفعل في سلة التسوق .. !!',

        'ADDED_TO_CART_SUCCESSFULLY'						=> 		'وأضاف إلى السلة بنجاح .. !!',

        'ADD_TO_CART_FAILED_DUE_TO_SOME_ERROR'				=> 		'أضف إلى السلة فشل بسبب بعض الأخطاء .. !!',

        'LOGIN_TO_PROCEED'									=> 		'تسجيل الدخول للمتابعة .. !!',



        /* close ServicelistingController.php */

        /* Start UserloginController.php */

        'LOGIN_SUCCESS'									=> 		'النجاح في تسجيل الدخول',

        'PASSWORD_RECOVERY_DETAILS'						=> 		'تفاصيل استرداد كلمة المرور',

        /* close UserloginController.php */

        /*Start FooterController.php*/



        'YOUR_COMMENTS_WILL_BE_SHOWN_AFTER_ADMIN_APPROVAL' 	=> 'سيتم عرض تعليقاتك بعد موافقة المشرف.',

        'YOUR_ENQUIRY_DETAILS' 	=> 'تفاصيل التحقيق الخاصة بك بنجاح ، سوف نتصل بك قريبا',

        'YOUR_PRODUCT_ENQUIRY' => 	'الإستفسار عن المنتج',



        /*End FooterController.php*/

#################################################### END CONTROLLER WORK ########################################

        /*Home controller merchant mail*/

        'BACK_HI_MERCHANT_YOUR_PRODUCT_PURCHASED'        =>        'مرحبًا يا تاجر ، تم شراء منتجك.',

        'BACK_HI_CUSTOMER_YOUR_PRODUCT_PURCHASED'        =>        'تفاصيل تأكيد طلبك تم وضعها بنجاح',

        /* Register success message */

        'BACK_REGISTER_ACCOUNT_CREATED_SUCCESSFULLY'			=> 'تسجيل حساب تم إنشائه بنجاح',

        /*End Email templates*/

        /* order success page - new languages */

        'INCLUDING'        	=>        'بما فيها',

        'TAXES'        		=>        'ضريبة',



        /*** Mobile Session Message starts  ***/

        'API_PARAMETER_MISSING'        						=>        'المعلمة مفقودة!',

        'API_USER_BLOCKED'									=>        'المستخدم المحظور!',

        'API_USER_DETAILS_AVAILABLE'						=>        'تفاصيل المستخدم المتاحة!',

        'API_USER_NOT_AVAILABLE'							=>        'المستخدم غير متوفر!',

        'API_EMAIL_ALREADY_EXIST'							=>        'البريد الالكتروني موجود بالفعل!',

        'API_REGISTERED_SUCESSFULLY'						=>        'مسجل بنجاح',

        'API_REGISTER_ACCOUNT_CREATED_SUCESSFULLY'			=>        'تسجيل حساب تم إنشائه بنجاح',

        'API_PWD_RECOVERT_DETAILS'							=>        'تفاصيل استرداد كلمة المرور',

        'API_COUNTRY_LIST_AVAILBLE'							=>        'قائمة البلد المتاحة!',

        'API_COUNTRY_LIST_NOT_AVAILBLE'						=>        'قائمة الدول غير متاحة!',

        'API_LIST_NOT_AVAILBLE'								=>        'قائمة غير متوفرة!',

        'API_HOMELIST_NOT_AVAILBLE'							=>        'الصفحة الرئيسية قائمة المتاحة!',

        'API_PWD_CHANGED_SUCCESSFULLY'						=>        'تم تغيير الرقم السري بنجاح!',

        'API_CATEGORIES_FOUND'								=>        'الفئات وجدت!',

        'API_NO_CATEGORIES_FOUND'							=>        'لا توجد فئات!',

        'API_INVALID_VALUE'									=>        'قيمة غير صالحة!',

        'API_INVALID_DATA'									=>        'بيانات غير صالحة!',

        'API_INVALID_USER_DETAILS'							=>        'تفاصيل المستخدم غير صالحة!',

        'API_NO_LISTING_AVAILABLE'							=>        'لا قوائم المتاحة',

        'API_PRODUCT_AVAILABLE'								=>        'المنتج متاح',

        'API_NOT_PRODUCT_AVAILABLE'							=>        'المنتج غير متوفر',

        'API_NO_PRODUCT_AVAILABLE'							=>        'لا يوجد منتج متاح',

        'API_INVALID_PORDUCT_DETAILS'	=> 'تفاصيل المنتج غير صالحة!',
        'API_NO_DEAL_AVAILABLE'	    =>        'لا توجد صفقات متاحة',

        'API_SIZE_NOT_AVAILABLE'	=>        'الحجم غير متوفر',

        'API_COLOR_NOT_AVAILABLE'	=>    'اللون غير متوفر',

        'API_WISHLIST_ADDED_SUCCESSFULLY'	=>  'تمت إضافة قائمة الرغبات بنجاح!',

        'API_WISHLIST_DELETED_SUCCESSFULLY'	=>   'تم حذف قائمة الرغبات بنجاح!',

        'API_ALREADY_IN_CART' =>   'بالفعل في عربة!',

        'API_ORDER_SUCCESSFUL'		=>   'امر ناجح!',

        'API_ORDER_NOT_AVAILABLE'		=>    'النظام غير متوفر!',

        'API_CART_NOT_AVAILABLE'	=>  'سلة التسوق غير متوفرة!',

        'API_ORDER_AVAILABLE'								=>        'النظام متاح!',

        'API_CART_AVAILABLE'								=>        'العربة المتاحة!',

        'API_CART_DETAILS_AVAILABLE'		=>  'تفاصيل سلة التسوق المتاحة!',

        'API_CART_UPDATED_SUCCESSFULLY'	=>   'سلة التسوق المحدثة بنجاح!',

        'API_CART_ADDED_SUCCESSFULLY'		=>  'تمت إضافة السلة بنجاح!',

        'API_REMOVED_SUCCESSFULLY' =>     'تمت إزالته بنجاح!',

        'API_NO_DATA_AVAILABLE'								=>        'لا تتوافر بيانات!',

        'API_NO_DEAL_AVAILABLE'		=>        'لا صفقة متاحة!',

        'API_DEAL_AVAILABLE'		=>        'صفقة متاحة!',

        'API_DEAL_NOT_AVAILABLE'		=>    'الصفقة غير متوفرة!',

        'API_DEALID_PARAMETER_MISSING'	=>    'المعلمة المفقودة!',

        'API_INVALID_COUNTRY_DATA' =>   'بيانات البلد غير صالحة!',

        'API_INVALID_SHIP_COUNTRY_DATA'	 		=>       'بيانات بلد السفينة غير صالحة!',

        'API_INVALID_SHIP_CITY_DATA'		=>     'بيانات مدينة السفينة غير صالحة!',

        'API_INVALID_CITY_DATA'		=>    'بيانات المدينة غير صالحة!',

        'API_USER_SHIPPING_DETAILS_NOT_AVAILABLE'	=>   'تفاصيل شحن المستخدم غير متوفرة!',

        'API_USER_DETAILS_NOT_AVAILABLE'					=>        'تفاصيل المستخدم غير متوفرة!',

        'API_STORE_AVAILABLE'				=>        'متجر المتاحة!',

        'API_NO_STORE_AVAILABLE'		=>        'لا يوجد متجر متاح!',

        'API_NO_DEALS_AVAILABLE'		=>        'لا توجد صفقات متاحة!',

        'API_REVIWE_SUCCESSFULLY_ADDED'		=>        'تمت إضافة المراجعة بنجاح',

        'API_INVALID_DEAL_DETAILS'		=>        'تفاصيل الصفقة غير صالحة!',

        'API_INVALID_STORE_DETAILS'		=>        'تفاصيل المتجر غير صالحة!',

        'API_CITY_LIST_AVAILABLE' 	=>        'قائمة المدينة المتاحة!',


        /* Order status */

        'API_SUCCESS'		=>    'نجاح',

        'API_COMPLETED'			=>        'منجز',

        'API_HOLD'		=>   'معلق',

        'API_FAILED'	=>   'فشل',
        'API_SETTING_LIST_NOT_AVAILBLE'		=>  'إعدادات الدفع غير متاحة!',

        //Order merchant mail

        'MERCHANT_YOUR_PRODUCT_PURCHASED_FOR_DETAILS'	=>  'تم شراء منتجك. يرجى الاطلاع على التفاصيل. ',

        'SIGN_UP_TODAY_AND_U_WILL'     =>'اشترك اليوم وستتمكن من:',
        'SPEED_UR_WAY_THROUGH'     =>'السرعة طريقك من خلال الخروج',
        'TRACK_UR_ORDERS_EASILY'     =>'تتبع طلباتك بسهولة',
        'KEEP_RECORD_UR_PURCHASES'     =>'احتفظ بسجل لجميع مشترياتك',
        'STORE_LOCATOR'   => 'فروعنا',
        'AND_ABOVE'   => 'و ما فوق',
        'BACK'   => 'الى الخلف',
        'PLS_CHECK_YOUR_MAIL'   => 'رجاء تفحص بريدك الإلكتروني',
        'API_QTY_NOT_PRODUCT_AVAILABLE' => 'كمية المنتج غير متوفرة',

        'QTY_EXCEED_AVAILABLE_COUNT'=>'تتجاوز الكمية العدد المتاح',
        /*** Mobile Session Message ends  ***/

        'HAS_EXCEED_MAX_LIMIT'=> ' تجاوز الحد الأقصى',

        'ENTER_REASON_FOR_cANCEL'=>'أدخل سبب الإلغاء',

        'PAYPAL_ACCOUNT_DETAILS'=> 'تفاصيل حساب Paypal',
        'ENTER_PAYPAL_ACCOUNT_DETAILS'=> 'أدخل تفاصيل حساب Paypal',
        'BANK_DETAILS'=> 'تفاصيل الحساب البنكي',
        'ENTER_BANK_DETAILS'=> 'أدخل تفاصيل الحساب البنكي',
        'IN'=>'في',
    ];

?>