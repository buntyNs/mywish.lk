<?php
return [																					
/*Front	end	*/																	
/*Home	page	*/																	
'LOGO'				الشعار																		
'CATEGORIES'	=>			الأصناف																		
'MORE_CATEGORIES'				المزيد من الأصناف																		
'NEW_PRODUCTS'	=>			نتائج البحث																		
'COMPARE'				قارن																		
'SOLD'	=>			مباع																		
'ADD_TO_CART'	=>			الإضافة للسلة																		
'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY'	=>																					
'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT'	=>																					
//'NO_PRODUCTS_AVAILABLE'	=>			لا تتوفر منتجات في البحث																		
'NO_PRODUCTS_AVAILABLE'				لا يوجد منتجات مع خصائص البحث																		
'CLOSE_COMPARISON_OVERLAY'	=>																					
'HI'	=>			مرحبا																		
'PAYMENT_EMAIL_KEY'	=>																					
'PAYMENT_EMAIL_SALT'	=>																					
'PAYMENT_STATUS'	=>			حالة الدفع																		
MY_DEAL_COD	=>																					
MY_DEAL_PAYPAL	=>																					
MY_PRODUCT_PAYPAL	=>																					
MY_PRODUCT_COD	=>																					
DEAL_TITLE	=>			عنوان العرض																		
'IMAGE'	=>			الصورة																		
'MY_PRODUCT_PAYUMONEY' =>'My	Product																					
'MY_DEAL_PAYUMONEY'	#ERROR!																					
'NO_PRODUCTS_INCOMPARE' =>'No	Products																					
'DEALS_OF_THE_DAY'	=>			عروض اليوم																		
'TOP_OFFERS'	=>			أفضل العروض																		
'MOST_POPULAR_PRODUCTS' =>'Most	Popular			منتج																		
'REVIEWS_AND_RATINGS'	=>			التقييم و المراجعات																		
'NO_REVIEWS_AND_RATINGS'	=>			ليس هناك مراجعات و تقييمات																		
'NO_RATING_FOR_THIS_PRODUCT' =>'No	Rating																					
'AVAILABLE_STOCK' =>	Available Stock,																					
'CLICK_HERE'	=>			إضغط هنا																		
'NO_ORDER_COD' =>'There	is																					
'NO_ORDER_PAYPAL' =>'There	is																					
'NO_ORDER_PAYUMONEY' =>'There	is																					
'NO_ORDER_DEAL_COD' =>'There	is																					
'NO_ORDER_DEAL_PAYPAL' =>'There	is																					
'NO_ORDER_DEAL_PAYUMONEY' =>'There	is																					
'NO_WHISLIST'	=>'There																					
'NO_SHIPPING_ADDRESS' =>'',																						
'REVIEW_AWESOME' =>'Awesome	-			خمسة نجوم																		
'REVIEW_PRETTY_GOOD' =>'Pretty	good			أربعة نجوم																		
'REVIEW_NOT_BAD' =>'Not	bad			ثلاثة نجوم																		
'REVIEW_BAD2' =>'Bad	-			نجمتين																		
'REVIEW_VERY_BAD' =>'Very	Bad			نجمة واحدة																		
																						
																						
/*End	Home	*/		الصفحة																		
/*Footer	page	*/		ابدأ																		
'REQUEST_FOR_ADVERTISEMENT'	=>																					
'AD_TITLE'	=>																					
'UPTO'	=>																					
	DOWNLOAD_OUR_APP																					
'ADS_POSITION'	=>																					
'SELECT_POSITION'	=>																					
'HEADER_RIGHT'	=>																					
'LEFT_SIDE_BAR'	=>																					
'BOTTOM_FOOTER'	=>																					
'PAGES'	=>																					
'SELECT_ANY_PAGE'	=>			اختر اي صفحة																		
'HOME'	=>			الصفحة الرئيسية																		
'SPORTS'	=>																					
'ELECTRONICS'	=>																					
'FLOWER_POT'	=>																					
'HEALTH'	=>			الصحة																		
'BEAUTY'	=>			الجمال																		
'REDIRECT_URL'	=>																					
'UPLOAD_IMAGES'	=>																					
'UPLOAD'	=>																					
'COUPON_AMOUNT'	=>																					
'CLOSE'	=>			مغلق																		
'CONTACT'	=>			تواصل معنا																		
'CONTACT_US'	=>			تواصل معنا																		
'ABOUT_COMPANY'	=>			عن الشركة																		
'BLOG'	=>																					
'ABOUT_US' =>	=>																					
'MERCHANT_LOGIN'	=>																					
'MERCHANT'	=>																					
'REGISTER'	=>			انشاء حساب جديد																		
'LOGIN'	=>			تسجيل الدخول																		
'SOCIAL_MEDIA'	=>			مواقع التواصل الإلكتروني																		
'NEWS_LETTER_SUBSCRIPTION'	=>																					
'SUBSCRIBE_TO_RECEIVE_THE_LATEST_NEWS_STRAIGHT_TO_YOUR_INBOX._BY_SUBSCRIBING_YOU_WILL_BE_ABLE_TO'	=>																					
'SUBSCRIBE_HERE'	=>			اشترك هنا																		
'PAYMENT_METHOD'	=>			طريقة الدفع																		
'OUR_SERVICES'	=>			خدماتنا																		
'TERMS_&_CONDITIONS'	=>			الاحكام والشروط																		
'HELP'	=>			للمساعدة																		
'ALL_RIGHTS_RESERVED'	=>			جميع الحقوق محفوظة																		
'SUCCESSFULLY_SUBSCRIBED'	=>			تم الاشتراك بنجاح																		
'ITEMS_IN_YOUR_CART'	=>			المنتجات في عربتك																		
'SELECT_CATEGORY'	=>			اختار الصنف																		
'DEALS'	=>			العروض																		
'SOLD_OUT'	=>			بيعت بالكامل																		
'STORES'	=>			المتاجر																		
'NEAR_BY_STORE'	=>			قريب من المتجر																		
'SEARCH_PRODUCT_NAME'	=>																					
'CUSTOMER_SUPPORT'	=>			خدمة العملاء																		
																						
/*For	Language	*/																				
'SELECT_LANGUAGE'	=>			اختر اللغة																		
																						
/*End	footer	*/		الصفحة																		
/*Navbar	started*/																					
'FORGOT_PASSWORD'	=>			نسيت كلمة المرور؟																		
'CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP'	=>																					
'DONT_HAVE_AN_ACCOUNT_YET'	=>			لا تملك حسابا بعد؟																		
'SIGN_UP'	=>			تسجيل حساب جديد																		
'E-MAIL'	=>			البريد الإلكتروني																		
'E-MAIL_ID'	=>																					
'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO'	=>			تسجيل الدخول عن طريق الفيس بوك																		
'SIGN_UP_WITH_FACEBOOK'	=>			التجسيل عن طريق الفيس بوك																		
'ALREADY_A_MEMBER'	=>			لديك حساب بالفعل																		
'RESET_PASSWORD'	=>			إعادة تعين كلمة المرور																		
'NEW_PASSWORD'	=>			كلمة السر الجديدة																		
'CONFIRM_PASSWORD'	=>			تأكيد كلمة المرور																		
'SELECT_COUNTRY'	=>			اختر البلد																		
'SELECT_CITY'	=>			اختر المدينة																		
'BY_CLICKING_SIGN_UP_YOU_AGREE_TO'	=>			بالضغط على انشاء الحساب انت توافق على																		
'TERMS_AND_CONDITIONS'	=>			الاحكام والشروط																		
'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO_LARAVEL_ECOMMERCE_MULTIVENDOR'	=>																					
'LOG_IN_WITH_FACEBOOK'	=>			تسجيل الدخول عن طريق الفيس بوك																		
'SIGN_IN'	=>			تسجيل الدخول																		
'HELPER::CUSTOMER_SUPPORT_NUMBER'	=>																					
'MIMIMUM_6_CHARACTERS'	=>			ستة حروف على الاقل																		
'ENTER_YOUR_EMAIL'	=>			ادخل بريدك الإلكتروني																		
'ENTER_YOUR_NEW_PASSWORD'	=>			ادخل كلمة المرور الجديدة																		
'CONFIRM_YOUR_NEW_PASSWORD'	=>			اكد كلمة المرور																		
'ENTER_YOUR_NAME_HERE'	=>			ادخل اسمك هنا																		
'ENTER_YOUR_EMAIL_HERE'	=>			ادخل بريدك الإلكتروني هنا																		
'SUBMIT'	=>			رفع																		
'CANCEL'	=>			إلغاء																		
'LOGIN_SUCCESSFULLY'	=>			تم تجسيل الدخول بنجاح																		
'INVALID_LOGIN_CREDENTIALS'	=>			اسم المستخدم او كلمة المرور خاطئة																		
'ALREADY_EMAIL_EXISTS'	=>			البريد الإلكتروني موجود بالفعل																		
'PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS'	=>			رجاءا تحقق من بريدك الإلكتروني للمزيد من المعلومات																		
'EMAIL_ID_DOES_NOT_EXIST'	=>			اسم المستخدم غير موجود																		
'PASSWORD_CHANGED_SUCCESS'	=>			تم تغير كلمة المرور بنجاح																		
'INVALID_USER'	=>			اسم مستخدم غير صالح																		
'INVALID_EMAIL'	=>			بريد إلكتروني غير صالح																		
/*Navbar	started*/																					
/*Start	loginnav		*/																			
'WELCOME'	=>			مرحبا																		
'MY_WISHLIST'	=>			قائمة الامنيات																		
'LOG_OUT'	=>			تسجيل الخروج																		
/*End	lagin	*/																				
/*Start	About*/					*/																
																						
/*End	About	*/																				
/*Start	App.php	*/		الصفحة																		
'LARAVEL'	=>																					
'TOGGLE_NAVIGATION'	=>																					
/*End	App.php	*/																				
/*Start	Action	*/																				
'SPECIALS'	=>			العروض																		
'LAST_BIDDER'	=>			اخر مزايد																		
'BIT_AMOUNT'	=>			قيمة المزاد																		
'BID_NOW'	=>			المزايدة الان																		
'MORE_FILTERS'	=>			المزيد من الفلاتر																		
'RESET'	=>			اعادة ضبط																		
'PER_PAGE'	=>			لكل صفحة																		
'VIEW_ALL'	=>			عرض الكل																		
'LIKES_ASC'	=>																					
'SORT_BY'	=>			ترتيب حسب																		
'PRICE_LOW'	=>			سعر منخفض																		
'HIGH'	=>			عالي																		
'PRICE_HIGH'	=>			سعر عالي																		
'LOW'	=>			منخفض																		
'TITLE'	=>			العنوان																		
'A'	=>			أ																		
'Z'	=>			ي																		
'DESCRIPTION'	=>			الوصف																		
'PAGE'	=>			الصفحة																		
'OF'	=>																					
'NO_RESULTS_FOUND'	=>			لا توجد نتائج																		
'LIST_ACTIONS'	=>																					
'LIKES_DESC'	=>																					
'DATE_ASC'	=>																					
'DATE_DESC'	=>																					
'STYLE_SELECTOR'	=>																					
'OREGIONAL_SKIN'	=>																					
'BOOTSWATCH_SKINS'	=>																					
'THESE_ARE_JUST'	=>																					
'FILTER_BY_TITLE'	=>																					
/*End	Action	*/																				
/*Start	Actionview	*/																				
'AUCTION'	=>																					
'DAYS'	=>			ايام																		
'HOURS'	=>			ساعات																		
'MIN'	=>			دقيقة																		
'SEC'	=>			ثانية																		
'BID_NOW'	=>			المزايدة الان																		
'BID_FROM'	=>			المزايدة من																		
'BID_INCREMENT'	=>																					
'RETAIL_PRICE'	=>			سعر التجزئة																		
'BID_HISTORIES'	=>																					
'LATEST_BIDDER(S)'	=>																					
'NOT_YET_BID'	=>			لم تتم المزايدة بعد																		
'PRODUCT_DETAILS'	=>			تفاصيل المنتج																		
'SPECIAL_PRODUCT'	=>			منتج مميز																		
'RELATED_PRODUCTS'	=>			منتجات ذات صلة																		
'PRODUCT_INFORMATION'	=>			وصف المنتج																		
'PRODUCT_SPECIFICATION'=>	Product Specification,																					
'CATEGORY'	=>			الصنف																		
'FEATURES'	=>			المميزات																		
'NEW'	=>			جديد																		
'UP_SELL_PRODUCT'	=>																					
'AVAILABLE'	=>			متاح																		
'VIEW_DETAILS'	=>			عرض التفاصيل																		
'YOUR_BID_AMOUNT_IS_NO_LONGER_ENOUGHTO_WIN'	=>			قيمة عرضك لم تعد تكفي للفوز																		
'PLEASE_RE-ENTER_YOUR_BID'	=>			اعد ادخال مزايدتك من فضلك																		
'NEW_MAX_BID'	=>																					
'OR_MORE'	=>			او المزيد																		
'TWITTER_TWEET_BUTTON'	=>																					
/*End	Actionview	*/																				
/*start	bid_payment	*/																				
'BID_AMOUNT'	=>			قيمة المزاد																		
'BID_SUMMARY'	=>			مخلص المزاد																		
'YOUR_BID_AMOUNT'	=>			قيمة مزايدتك																		
'SHIPPING_AMOUNT'	=>			كمية الشحن																		
'YOUR_SHIPPING_ADDRESS'	=>			عنوان الشحن																		
'GOOD_NEWS'	=>			اخبار جيدة																		
'A_PRODUCT_OF_NEXEMERCHANT'	=>																					
'BACKGROUND_PATTERNS'	=>																					
'AMELIA'	=>																					
'SPRUCE'	=>																					
'SUPERHERO'	=>																					
/*End	bid_payment		*/																			
/*start	blog.blade.php*/																					
'POSTED_BY'	=>			نشر من قبل																		
'ADMIN'	=>			مشرف																		
'COMMENTS'	=>			التعليقات																		
'CONTINUE_READING'	=>			قراءة المزيد																		
'NO_BLOGS_AVAILABLE'	=>			ليس هناك مدونات متاحة																		
'POPULAR_POSTS'	=>			منشورات ذات شعبية																		
/*End	blog.blade.php*/																					
/*Start	blogcomment.blade.php*/																					
'YOU_HAVE_ERRORS_WHILE_PROVIDING_COMMENT'	=>			لديك بعض الاخطاء اثناء ارسال التعليق																		
'CUSTOMER_COMMENTS'	=>			تعليقات العميل																		
'LEAVE_A_REPLY'	=>			اترك ردا																		
'WEBSITE'	=>			الموقع الإلكتروني																		
'ENTER_YOUR_NAME'	=>			ادخل اسمك																		
'ENTER_YOUR_EMAIL_ID'	=>			ادخل اسم المستخدم																		
'ENTER_YOUR_LINK'	=>			ادخل رابطك																		
'ENTER_YOUR_MESSAGE'	=>			ادخل رسالتك																		
/*End	blogcomment.blade.php*/																					
/*Start	blog																					
																						
/*End	blog																					
/*Start	cart.blade.php*/																					
'SHOPPING_CART'	=>			عربة التسوق																		
'ITEM(S)'	=>			المنتجات																		
'CONTINUE_SHOPPING'	=>			اكمال التسوق																		
'USER_COUPON'	=>			كوبون المستخدم																		
'PRODUCT_COUPON'	=>																					
'S.NO'	=>																					
'PRODUCT'	=>			المنتج																		
'COLOR'	=>			اللون																		
'SIZE'	=>			المقاس																		
'QUANTITY'	=>			الكمية																		
'UPDATE'	=>			تحديث																		
'REMOVE'	=>			إزالة																		
'PRICE'	=>			السعر																		
'SUB_TOTAL'	=>																					
'COUPON_CODE'	=>			الكوبون																		
'NO_COUPON_AVAI'	=>			ليس هنالك كوبونا متاح																		
'N'	=>																					
'ESTIMATE_YOUR_SHIPPING'	=>			تقدير الشحن																		
'CHECK_PRODUCT_AVAILABILITY_AT_YOUR_LOCATION'	=>			تاكد من توفر منتجك في موقعك																		
'POST_CODE'	=>			عنوان البريد																		
'ZIPCODE'	=>																					
'VERIFY'	=>			توثيق																		
'NO_ITEMS_IN_CART'	=>			السلة فارغة																		
'NO_ITEM'	=>																					
'PROCEED_TO_CHECKOUT'	=>																					
'SPECIAL_COUPON_CODE'	=>																					
'PLEASE_FILL_ALL_FIELDS'	=>			رجاءا املئ جميع الحقول																		
'ENTER_CORRECT_COUPON_CODE'	=>			ادخل الكوبون الصحيح																		
'APLICAPLE_ONLY'	=>																					
'QUANTITY_OF_PRODUCT'	=>			كمية المنتج																		
'USE_THIS_COUPON_ONLY_FOR'	=>			استخدم الكوبون فقط ل																		
'COUPON_IS_NOT_VALID'	=>			الكوبون غير صالح																		
'COUPON_DATE_NOT_STARTED'	=>																					
'COUPON_DATE_EXPIRED'	=>			انتهت صلاحية الكوبون																		
'COUPON_CODE_ALREADY_EXIST'	=>			تم ادخال الكوبون بالفعل																		
'ALL_COUPONS_ARE_PURCHASED'	=>																					
'YOUR_COUPON_LIMIT_EXIT'	=>																					
'CANNOT_APPLY_MULTIPLE_PRODUCT_COUPON_IN_SAME_CART'	=>																					
'PLEASE_ENTER_VALID_COUPON_CODE'	=>			رجاءا ادخل كوبونا صالحا																		
'PLEASE_TRY_AGAIN_ITS_NOT_VALID_COUPON_CODE_APPLIED_FOR_YOU'	=>																					
'NOT_HAVE_SUFFICIENT_CART'	=>																					
'COUPON_CODE_ALREADY_EXSIST'	=>																					
'YOUR_COUPON_NOT_STARTED'	=>																					
'APPLY'	=>			تقديم																		
'CANCEL_USER_COUPON'	=>																					
'USER_COUPON1'	=>																					
'PRODUCT_COUPON1'	=>																					
/*End	cart.blade.php*/																					
/*start	categorylist.blade.php*/																					
'MOST_VISITED_PRODUCT'	=>			المنتج الاكثر زيارة																		
/*End	categorylist.blade.php*/																					
/*start	category																					
'ALL_CATEGORIES'	=>			جميع الاصناف																		
/*End	category																					
/*Start	Checkout.blade.php*/																					
'CHECKOUT'	=>			تفقد																		
'SHIPPING_ADDRESS'	=>			عنوان الشحن																		
'LOAD_SHIPPING_ADDRESS'	=>																					
'YES'	=>			نعم																		
'NO'	=>			لا																		
'NAME_FIELD_IS_REQUIRED'	=>			اسم الحقل مطلوب																		
'ADDRESS_LINE1'	=>																					
'ADDRESS_FIELD_IS_REQUIRED'	=>			حقل العنوان مطلوب																		
'ADDRESS_LINE2'	=>																					
'CITY'	=>			المدينة																		
'STATE'	=>			المحافظة																		
'COUNTRY'	=>			البلد																		
'PHONE_NUMBER'	=>			رقم الهاتف																		
'PHONE_NUMBER_FIELD_IS_REQUIRED'	=>			حقل رقم الهاتف مطلوب																		
'SELECT_PAYMENT_METHOD'	=>			اختر طريقة الدفع																		
'PAYPAL'	=>			باي بال																		
'PAYUMONEY'	=>																					
'CASH_ON_DELIVERY'	=>			الدفع عند الاستلام																		
'ORDER_SUMMARY'	=>			ملخص الطلب																		
'SHIPMENT'	=>			الشحنة																		
'ESTIMATED_DELIVERY'	=>			التوصيل المتوقع																		
'SHIPPING'	=>			الشحن																		
'TYPE_OF_COUPON'	=>			نوع الكوبون																		
'COUPON_VALUE'	=>			قيمة الكوبون																		
'VALUE'	=>			القيمة																		
'DEAL_DETAILS'	=>			تفاصيل العرض																		
'USE_WALLET'	=>			استخدم المحفظة																		
'ORDER_SUBTOTAL'	=>																					
'ORDER_SUBTOTAL_TAX_INCL'	=>																					
'ORDER_SHIPPING'	=>																					
'ORDER_TAX'	=>																					
'ORDER_TOTAL'	=>			الطلب الكلي																		
'PLACE_ORDER'	=>																					
'NO_ORDERS_PLACED'	=>																					
																						
'ORDERS_PLACED'	=>																					
'ORDERS_PACKED'	=>																					
'ORDERS_DISPATCHED'	=>			مرسل																		
'ORDERS_DELIVERED'	=>			تم التوصيل																		
'ORDERS_CENCELED'	=>			ملغى																		
'ORDERS_RETURN_PENDING'	=>																					
'ORDERS_RETURNED'	=>			مسترجع																		
'ORDERS_REPLACED'	=>			مستبدل																		
'ORDERS_REPLACE_PENDING'	=>																					
'ORDERS_CANCEL_PENDING'	=>																					
																						
'ENTER_FIRST_NAME'	=>			ادخل الاسم الاول																		
'ENTER_LAST_NAME'	=>			ادخل الاسم الاخير																		
'ENTER_ADDRESS_LINE1'	=>			ادخل العنوان١																		
'ENTER_ADDRESS_LINE2'	=>			ادخل العنوان ٢																		
'ENTER_YOUR_CITY'	=>			ادخل مدينتك																		
'ENTER_YOUR_STATE'	=>			ادخل محافظتك																		
'ENTER_YOUR_PHONE_NO'	=>			ادخل رقم هاتفك																		
'ENTER_YOUR_VALID_PHONE_NO'	=>			ادخل رقم هاتفك																		
'ENTER_YOUR_ZIPCODE'	=>																					
'ENTER_YOUR_VALID_ZIPCODE'	=>																					
'ENTER_YOUR_FIRST_NAME'	=>			ادخل اسمك الاول																		
'ENTER_YOUR_LAST_NAME'	=>			ادخل اسمك الاخير																		
'ENTER_YOUR_ADDRESS_LINE1'	=>																					
'ENTER_YOUR_ADDRESS_LINE2'	=>																					
'ENTER_YOUR_COUNTRY'	=>			ادخل البلد																		
'ENTER_YOUR_PHONENUMBER'	=>			ادخل رقم هاتفك																		
'ENTER_ZIP_CODE'	=>																					
/*End	Checkout.blade.php*/																					
/*start	cms																					
'NO_DATA_FOUND'	=>			لم يتم العثور على البيانات																		
'NO_CATEGORY_FOUND'	=>			لم يتم العثور على التصنيف																		
/*End	cms																					
/*start	compare.blade.php*/																					
																						
/*end	compare.blade.php*/																					
/*start	compare_product.blade.php*/																					
'COMPARE_PRODUCTS'	=>			قارن المنتجات																		
'ORIGIANL_PRICE'	=>			السعر الاصلي																		
'DISCOUNT'	=>			الخصم																		
'RATING'	=>			التقييم																		
'RATINGS'	=>			التقييمات																		
'DELIVERY_WITH_IN'	=>			توصل في																		
'DEAL_ENDS_IN'	=>			ينتهي العرض في																		
'SIZES'	=>			المقاسات																		
'DESC'	=>																					
'SPEC'	=>																					
'STORE_NAME'	=>			اسم المتجر																		
'CLEAR_LIST'	=>			حذف القائمة																		
/*start	compare_product.blade.php*/																					
/*start	contactus.blade.php*/																					
'CONTACT_DETAILS'	=>			تفاصيل التواصل																		
'EMAIL_US'	=>			تواصل معنا عبر البريد الإلكتروني																		
'PLEASE_ENTER_YOUR_FIRSTNAME'	=>			فضلا ادخل اسمك الاول																		
'PLEASE_ENTER_YOUR_LASTNAME'	=>			فضلا ادخل اسمك الاخير																		
'PLEASE_PROVIDE_A_PASSWORD'	=>			فضلا قم بانشاء كلمة مرور																		
'OUR_PASSWORD_MUST_BE_AT_LEAST_5_CHARACTERS_LONG'	=>			على كلمة المرور ان تحتوي على الاقل على خمس خانات																		
'PLEASE_ENTER_THE_VALID_EMAIL_ADDRESS'	=>																					
'PLEASE_ACCEPT_OUR_POLICY'	=>																					
'ENTER_YOUR_NAME'	=>			ادخل اسمك																		
'ENTER_YOUR_VALID_EMAIL_ID'	=>																					
'ENTER_YOUR_PHONE_NUMBER'	=>			ادخل رقم هاتفك																		
'ENTER_QUERIES'	=>																					
'SEND_MESSAGE'	=>			ارسل رسالة																		
/*End	contactus.blade.php*/																					
/*Start	customer_profile.blade.php*/																					
'MY_PROFILE'	=>			ملفي الشخصي																		
'MY_ACCOUNT'	=>			حسابي																		
'MY_COD_DETAILS'	=>																					
'MY_BUYS'	=>																					
'MY_WISH_LIST'	=>																					
'MY_SHIPPING_ADDRESS'	=>																					
'EDIT'	=>			تعديل																		
'PROFILE_IMAGES'	=>			صورة الملف الشخصي																		
'CANCEL'	=>			إلغاء																		
'SELECT_COUNTRY'	=>			اختر البلد																		
'SELECT_CITY'	=>			اختر المدينة																		
'TOTAL_WALLET_BALANCE_AMOUNT'	=>			رصيد المحفظة الكلي																		
'ORDERID'	=>			رقم الطلب																		
'TOT._PRODUCT'	=>																					
'TOTAL_ORDER_AMOUNT'	=>			مجموع مبلغ الطلب																		
'ORDER_DATE'	=>			تاريخ الطلب																		
'USED_WALLET'	=>																					
'STATUS'	=>																					
'INVOICE'	=>			الفاتورة																		
'TAX_INVOICE'	=>			فاتورة الضريبة																		
'CASH_ON_DELIVERY'	=>			الدفع عند الاستلام																		
'AMOUNT_PAID'	=>			المبلغ المدفوع																		
'INCLUSIVE_OF_ALL_CHARGES'	=>			شاملة جميع الرسوم																		
'SHIPPING_ADDRESS'	=>			عنوان الشحن																		
'INVOICE_DETAILS'	=>			تفاصيل الفاتورة																		
'THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS'	=>			الشحنة تحتوي على المنتجات الاتية																		
'PRODUCT_TITLE'	=>			اسم المنتج																		
'PRODUCT_TITLE_HERE'	=>			اسم المنتج هنا																		
'ORIGINAL_PRICE'	=>			السعر الاصلي																		
'SHIPMENT_VALUE'	=>			قيمة الشحنة																		
'WALLET'	=>			المحفظة																		
'AMOUNT'	=>																					
'PRODUCT_NAMES'	=>			اسماء المنتج																		
'PRODUCT_PRICE'	=>			سعر المنتج																		
'PRODUCT_IMAGE'	=>			صورة المنتج																		
'ACTION'	=>																					
'ALL_FIELDS_ARE_MANDATORY'	=>			جميع الحقول إلزامية																		
'FULL_NAME'	=>			الاسم كامل																		
'ADDRESS2'	=>			العنوان٢																		
'DEALS_NAMES'	=>			أسماء العروض																		
'CHANGE_PROFILE_PICTURE'	=>			تغير صورة العرض																		
'IMAGE_UPLOAD_SIZE_1'	=>			حجم تحميل الصورة 1MB																		
'PLEASE_PROVIDE_PHONENUMBER'	=>			ادخل رقم الهاتف																		
'PHONENUMBER_CHANGED_SUCCESSFULLY'	=>			تم تغير رقم الهاتف بنجاح																		
'PLEASE_PROVIDE_ANY_ONE_OF_THE_ADDRESS_FIELDS'	=>																					
'ADDRESS_CHANGED_SUCCESSFULLY'	=>			تم تعير العنوان بنجاح																		
'CITY_AND_COUNTRY_CHANGED_SUCCESSFULLY'	=>			تم تغير الدولة و المدينة بنجاح																		
'PLEASE_PROVIDE_VALID_PHONE_NUMBER'	=>			الرجاء ارفاق رقم هاتف صالح																		
'SHIPPING_DETAILS_UPDATED_SUCCESSFULLY'	=>			تم تحديث تفاصيل الشحن بنجاح																		
'COUNTRY_CHANGED_SUCCESSFULLY'	=>			تم تغير الدولة بنجاح																		
'ENTER_YOUR_OLD_PASSWORD'	=>			ادخل كلمة المرور القديمة																		
'ENTER_YOUR_NEW_PASSWORD'	=>			ادخل كلمة المرور الجديدة																		
'ENTER_CONFIRM_PASSWORD'	=>			قم بتأكيد كلمة المرور																		
'FRUIT_BALL'	=>																					
'ENTER_YOUR_PHONE_NUMBER'	=>			ادخل رقم هاتفك																		
'PROVIDE_ADDRESS1'	=>																					
'PROVIDE_ADDRESS2'	=>																					
'ENTER_YOUR_ADDRESS'	=>			ادخل عنوانك																		
'ENTER_YOUR_MOBILE_NUMBER'	=>			ادخل رقم هاتفك																		
'ENTER_YOUR_EMAIL_ID'	=>																					
'ENTER_YOUR_ZIP_CODE'	=>																					
'UPDATE'	=>			تحديث																		
'IMAGE_FIELD_REQUIRED'	=>			حقل الصورة مطلوب																		
'NAME_UPDATED_SUCCESSFULLY'	=>			تم تحديث الاسم بنجاح																		
'UPDATED_SUCCESSFULLY'	=>			تم التحديث بنجاح																		
'NO_DATA_UPDATED'	=>																					
'PASSWORD_CHANGED_SUCCESSFULLY'	=>			تم تغيير كلمة المرور بنجاح																		
'BOTH_PASSWORDS_DO_NO_MATCH'	=>			كلمات المرور غير متطابقة																		
'OLD_PASSWORD_DOES_NOT_MATCH'	=>			كلمة المرور القديمة لا تتطابق																		
'MOBILE'	=>																					
/*End	customer_profile.blade.php*/																					
/*Start	deals.blade.php*/																					
'MOST_VISITED_DEALS'	=>			العروض الاكثر زيارة																		
'NO_DEALS_AVAILABLE'	=>			ليس هناك اي عروض متاحة																		
'SHIPPING_DELIVERY'	=>			الشحن والتسليم																		
/*End	deals.blade.php*/																					
/*start	deals_shipping_details.blade.php*/																					
'PRODUCTS_NAME'	=>																					
'DATE'	=>			التاريخ																		
'DETAILS'	=>			التفاصيل																		
'DELIVERY_STATUS'	=>																					
'EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE'	=>																					
'SEND'	=>			ارسل																		
'SENNEX_ECARTD'	=>																					
/*end	Deals_shipping_details.blade.php*/																					
/*Start	enquirey.blade.php*/																					
'ENTER_SUBJECT'	=>			ادخل الموضوع																		
/*end	enquirey.blade.php*/																					
/*Start	dealview.blade.php*/																					
'RELATED_DEALS'	=>			عروض ذات صلة																		
'OUT_OF_STOCK'	=>			نفذت من المخزن																		
'OFFER'	=>			العرض																		
'DICOUNT'	=>			الخصم																		
'YOU_SAVE'	=>			وفرت																		
'STORE_DETAILS'	=>			تفاصيل المتجر																		
'WRITE_A_POST_COMMENTS'	=>																					
																						
'ENTER_COMMENT_TITLE'	=>			ادخل عنوان التعليق																		
'STAR'	=>			نجمة																		
'REVIEWS'	=>			التعليقات																		
'NO_REVIEW_RATINGS'	=>																					
'WRITE_A_REVIEW'	=>			اكتب تعليقا																		
'ENTER_COMMENTS_QUERIES'	=>			ادخل تعليقاتك																		
'VIEW_STORE'	=>			عرض المتجر																		
'PRATEEK'	=>																					
/*end	dealview.blade.php*/																					
/*start	faq.blade.php*/																					
'FAQ'	=>			الاسئلة الاكثر تكرارا																		
'PLAYSTORE'	=>			متجر بلاي																		
'APPLE_APP_STORE'	=>			متجر أبل																		
/*End	faq.blade.php*/																					
/*start	forgptpassword.blade.php*/																					
'HELLO'	=>			مرحبا																		
'PASSWORD_RESET_LINK'	=>			رابط اعادة تعين كلمة المرور																		
'PASSWORD_LINK'	=>			رابط كلمة المرور																		
'USERNAME'	=>			اسم المستخدم																		
'PASSWORD_LINK'	=>			رابط كلمة المرور																		
'PLEASE_CLICK_THIS_LINK_TO_RESET_YOUR_PASSWORD'	=>	reset	your	قم بالضغط على هذا الرابط لاعادة ضبط كلمة المرور																		
																						
/*end	forgptpassword.blade.php*/																					
/*start	help.blade.php*/																					
																						
/*end	help.blade.php*/																					
/*start	manage_deal_review.blade.php*/																					
'MANAGE_REVIEWS'	=>																					
'MANAGE_PRODUCTS'	=>																					
'REVIW_TITLE'																						
'CUSTOMER_NAME'																						
'ACTIONS'																						
'CLICK_FOR_PREVIOUS_MONTHS'	=>																					
'CLICK_FOR_NEXT_MONTHS'	=>																					
/*End	manage_deal_review.blade.php*/																					
/*start	manage_dealcason_delivery_details.blade.php*/																					
																						
/*end	manage_dealcason_delivery_details.blade.php*/																					
/*start	merchant_signup.blade.php*/																					
'MERCHANT_SIGN_UP'	=>																					
'WELCOME_TO_MERCHANT_SIGN_UP'	=>																					
'CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE'	=>	through	a	store',																		
'CREATE_YOUR_STORE'	=>			أنشئ متجرك																		
'ADDRESS1'	=>			العنوان١																		
'ADDRESS2'	=>			العنوان٢																		
'ZIPCODE'	=>			الرمز البريدي																		
'META_KEYWORDS'	=>																					
'META_DESCRIPTION'	=>																					
'UPLOAD_FILE'	=>			تحديث الملف																		
'ENTER_YOUR_STORE_LOCATION'	=>			ادخل موقع متجرك																		
'GOOGLE_MAPS'	=>			خرائط قوقل																		
'LATITUDE'	=>			خط العرض																		
'LONGTITUDE'	=>			خط الطول																		
'PERSONAL_DETAILS'	=>			معلومات شخصية																		
'FIRST_NAME'	=>			الاسم الاول																		
'LAST_NAME'	=>			الاسم الاخير																		
'CONTACT_NUMBER'	=>			رقم التواصل																		
'PAYMENT_EMAIL'	=>																					
'NUMBERS_ONLY_ALLOWED'	=>			مسموع بالارقام فقط																		
'MAX_PRICE_SHOULD_LESS_MIN'	=>	Minimum	Price!',																			
'NOT_A_VALID_EMAIL_ADDRESS'	=>																					
'PLEASE_ENTER_YOUR_STORE_NAME'	=>			فضلا ادخل اسم متجرك																		
'PLEASE_ENTER_YOUR_PHONE_NO'	=>			فضلا ادخل رقم هاتفك																		
'PLEASE_ENTER_ADDRESS1_FIELD'	=>			فضلا ادخل عنوانك١																		
'PLEASE_ENTER_ADDRESS2_FIELD'	=>			فضلا ادخل عنوانك٢																		
'PLEASE_ENTER_ZIPCODE'	=>			فضلا ادخل الرمز البريدي																		
'PLEASE_ENTER_META_KEYWORDS'	=>																					
'PLEASE_ENTER_META_DESCRIPTION'	=>																					
'PLEASE_ENTER_WEBSITE'	=>			فضلا ادخل الموقع الإلكتروني																		
'PLEASE_ENTER_LOCATION'	=>			فضلا ادخل الموقع																		
'PLEASE_ENTER_COMMISSION'	=>																					
'PLEASE_CHOOSE_YOUR_UPLOAD_FILE'=>	'Please			اختر ملف التحميل																		
'PLEASE_SELECT_COUNTRY'	=>			اختر الدولة																		
'PLEASE_SELECT_CITY'	=>			اختر المدينة																		
'PLEASE_ENTER_AN_EMAIL_ADDRESS'	=>			ادخل البريد الالكتروني																		
'PLEASE_ENTER_YOUR_FIRST_NAME'	=>			ادخل اسمك الاول																		
'PLEASE_ENTER_CITY'				ادخل المدينة																		
'PLEASE_ENTER_ADDREESS1_FIELD'	=>			ادخل عنوانك١																		
'PLEASE_ENTER_PAYMENT_ACCOUNT'	=>			ادخل حساب الدفع																		
'PLEASE_ENTER_LAST_NAME'	=>			ادخل اسمك الاخير																		
'PLEASE_ENTER_PHONE_NO'	=>			ادخل رقم هاتفك																		
'ENTER_YOUR_STORE_NAME'	=>			ادخل اسم متجرك																		
'ENTER_YOUR_CONTACT_NUMBER'	=>			ادخل رقم التواصل لديك																		
'ENTER_YOUR_ADDRESS1'	=>			ادخل عنوانك١																		
'ENTER_YOUR_ADDRESS2'	=>			ادخل عنوانك٢																		
'ENTER_YOUR_ZIPCODE'	=>			ادخل رمزك البريدي																		
'ENTER_YOUR_META_KEYWORDS'	=>																					
'ENTER_YOUR_META_DESCRIPTION'	=>																					
'ENTER_YOUR_STORE_WEBSITE'	=>			ادخل موقع المتجر الإلكتروني																		
'TYPE_YOUR_LOCATION_HERE'																						
'ENTER_YOUR_FIRST_NAME'				ادخل اسمك الاول																		
'ENTER_YOUR_LAST_NAME'				ادخل اسمك الاخير																		
'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS'	=>			ادخل تفاصيل حساب الدفع																		
'SEARCH'	=>			بحث																		
'MERCHANT_EMAIL_EXIST' =>	'This																					
/*end	merchant_signup.blade.php*/																					
/*Start	newsletter.blade.php*/																					
'NEWS_LETTER'	=>			النشرة الاخبارية																		
'SUBSCRIBE_YOUR_MAIL_ID'	=>																					
'ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION'	=>	Email	Subscription',																			
'SUBSCRIBE'	=>			اشترك																		
'WELCOME_TO_SUBSCRIBE_YOUR_NEWS_LETTER_SUBSCRIPTION'	=>	Letter	subscription',																			
/*end	newsletter.blade.php*/																					
/*start	pages.blade.php*/																					
/*end	pages.blade.php*/																					
/*start	paymentresult.blade.php*/																					
'PAYMENT_RESULT'	=>			نتيجة الدفع																		
'YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED'	=>			تمت عملية الدفع بنجاح																		
'PLEASE_NOTE_THE_TRANSACTION_DETAILS'	=>			بيانات العملية																		
'YOUR_PAYMENT_PROCESS_FAILED'	=>			فشل عملية الدفع																		
'YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR'	=>	stopped	Due	تم ايقاف عملية الدفع																		
'TRANSACTION_DETAILS'	=>			تفاصيل العملية																		
'THANK_YOU_FOR_SHOPPING_WITH'	=>			شكرا لتسوقك مع																		
'PAYER_NAME'	=>			اسم المشتري																		
'TRANSACTIONID'	=>			رقم العملية																		
'TOKENID'	=>			رقم																		
'PAYER_EMAIL'	=>			البريد الإلكتروني للمشتري																		
'PAYER_ID'	=>			رقم المشتري																		
'ACKNOWLEDGEMENT'	=>																					
'PAYERSTATUS'	=>																					
'RODUCT_DETAILS_FOR_CURRENT_TRANSACTION'	=>																					
'PRODUCT_QUANTITY'	=>			كمية المنتج																		
'PRODUCT_DEAL_QUANTITY'	=>			المنتج / كمية العروض																		
/*end	paymentresult.blade.php*/																					
/*Start	paymentcod.blade.php*/																					
'YOUR_ORDER_SUCCESSFULLY_PLACED'	=>																					
'TRANSACTION_ID'	=>																					
'PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION'	=>																					
'SUB-TOTAL'	=>																					
'SHIPPING_TOTAL'	=>			الشحن الكلي																		
/*end	paymentcod.blade.php*/																					
/*start	place_bid_payment.blade.php*/																					
'CONGRATULATIONS'	=>			تهانينا																		
'YOUR_BID_WAS_PLACED_SUCCESSFULLY'	=>																					
'YOU_WILL_RECEIVE_AN_EMAIL_CONFIRMATION_SHORTLY'	=>	confirmation	shortly',	سوف تتلقى رسالة التاكيد في البريد الإلكتروني قريبا																		
'YOUR_BID_AMOUNT'	=>																					
'ESTIMATED_SHIPPING_CHARGE'	=>																					
'AUCTION_TIME_REMAINING'	=>																					
'CLOSE_WINDOW'																						
'ALTERNATIVE_INFORMATION'	=>	provided	within	bidders	default	credit	card	and	billing	address	will	be	used	to	ship	the	winnings',					
'CREDIT_CARD_WILL_NOT_BE_CHARGED'	=>	be	charged	charge	the	minimum	amount	needed	to	win',												
'RANSACTION_FEE'		with	$1.99 	per	order	if	you	win',														
'ORDER_IS_CANCELLED'	=>	due	to	be	assessed',																	
'APPLICABLE_SALES'	=>	added	to																			
'TENNESSEE_ORDERS'	=>																					
/*end	place_bid_payment.blade.php*/																					
/*start	products.blade.php*/																					
'MOST_VISITED_PRODUCTS'	=>			المنتجات الأكثر زيارة																		
/*end	products.blade.php*/																					
/*start	productview.blade.php*/																					
'CLICK_TO_VIEW_COUPON_CODE'	=>			انقر لعرض رمز الكوبون																		
'CASHBACK'	=>																					
'ADD_TO_WISHLIST'	=>																					
'IN_STOCK'				في المخزن																		
'SELECT'				اختر																		
'WRITE_A_REVIEW_POST'				اكتب مراجعة																		
'RATE_THIS_PRODUCT'	=>			قيم المنتج																		
'REVIEW_THIS_PRODUCT'	=>			تقييم المنتج																		
'LIMITED_QUANTITY_AVAILABLE'	=>			كمية محدودة																		
'SELECT_COLOR'	=>			اختر اللون																		
'SELECT_SIZE'	=>			اختر المقاس																		
'PRODUCT_ADDED_TO_WISHLIST'	=>																					
'PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST'	=>	Wishlist',																				
'POST_COMMENTS'	=>			التعلقيات المنشورة																		
'SUBMIT_REVIEW' =>	Submit,																					
'REVIEW_TITLE'	=>			عنوان المراجعة																		
'REVIEW_DESCRIPTION'	=>			وصف المراجعة																		
/*end	productview.blade.php*/																					
/*start	register.blade.php*/																					
'WELCOME_TO_USER_REGISTRATION'	=>																					
'CREATE_NEW_USER_ACCOUNT'	=>			انشىء حساب مستخدم جديد																		
'ENTER_YOUR_PASSWORD'				ادخل كلمة المرور																		
'BY_CLICKING_SIGNUP_I_AGREE_TO'	=>	to',		بنقرك على تجسيل انت توافق على																		
'THIS_EMAIL_ID_ALREADY_REGISTERED'	=>																					
'LESSER_MOBILE_NO'																						
'CORRECT_MOB_NO'	=>																					
'ENTER_10_DIGITS_OF_MOBILE_NO'	=>	MOBILE_NO',		ادخل عشرة خانات لرقم الهاتف																		
'CORRECT_MOBILE_NO'	=>																					
'PLEASE_ENTER_VALID_MOBILE_NUMBER'	=>																					
'EMAIL_ID'	=>																					
/*end	register																					
/*start	search.blade.php*/																					
'SEARCH_FOR_PRODUCTS'	=>			ابحث عن المنتجات																		
'SEARCH_FOR_DEALS'	=>			ابحث عن العروض																		
/*end	search.blade.php*/																					
/*start	shipping.blade.php*/																					
'POSTAL_CODE'	=>			الرمز البريدي																		
/*end	shipping.blade.php*/																					
/*Start	Sold.blade.php*/																					
'SOLD_PRODUCTS'	=>			المنتجات المباعة																		
'Nearbyshop'	=>			لم يتم العثور على متاجر قريبة من المدينة																		
'NO_RECORDS_FOUND_UNDER_PRODUCTS'	=>																					
'SOLD_DEALS'	=>			العروض المباعة																		
'NO_RECORDS_FOUND_UNDER_DEALS'	=>																					
/*end	sold.blade.php*/																					
/*start	storview.blade.php*/																					
'NO_RATING_FOR_THIS_STORE'	=>			ليس هناك تقيم لهذا المتجر																		
'MOBILE_NO'	=>			رقم الهاتف																		
'NO_RECORDS_FOUND_UNDER'	=>																					
'PRODUCTS'	=>			المنتجات																		
'BRANCHES'	=>			الفروع																		
'BACK_YOUR_DEAL_STORE_REVIEW_POST_SUCCESSFULLY'	=>			تم نشر المراجعة بنجاح																		
/*end	storview.blade.php*/																					
/*start	submission.blade.php*/																					
'THANK_YOU_FOR_MERCHANT_REGISTRATION'	=>																					
'THANK_YOU_TO_MERCHANT_SIGN_UP'	=>	up',																				
'YOU_GOT_YOUR_USERNAME_AND_PASSWORD'	=>	password	through																			
/*End	submission.blade.php*/																					
/*start	stores.blade.php*/																					
'NO_STORES_FOUND'	=>			لم يتم العثور على اي متاجر																		
/*end	stores.php*/																					
/*Front	end			صفحات ثابتة																		
																						
																						
#############################################	START																					
																						
																						
/*start	admin_passwordrecoverymail.blade.php*/																					
'CONTACT_INFORMATION'	=>			معلومات التواصل																		
/*End	admin_passwordrecoverymail.blade.php*/																					
																						
/*Start	merchantmail.blade.php*/																					
'MAIL_EMAIL_TEMPLATE'	=>																					
'MAIL_HI'	=>			مرحبا																		
'MAIL_YOUR_LOGIN_CREDENTIALS_ARE'	=>																					
'MAIL_USER_NAME'	=>			اسم المستخدم																		
'MAIL_PASSWORD'	=>			كلمة المرور																		
'MAIL_LOGIN_YOUR_ACCOUNT'	=>			قم بتسجيل الدخول الى حسابك																		
'MOBILE_YOUR_MERCHANT_ACCOUNT_WAS_CREATED_SUCCESSFULLY'	=>	Successfully',																				
																						
/*End	merchantmail.blade.php*/																					
/*Start	ordermail.blade.php*/																					
'WALLET_APPLIED'	=>																					
'COUPON_APPLIED'	=>																					
'DISCOUNT_PRICE'	=>			سعر الخصم																		
'ACTUAL_PRICE'	=>			السعر الاصلي																		
/*End	ordermail.blade.php*/																					
																						
/*	Start																					
'PASSWORD_RECOVERY_DETAILS_FOR_USER'=>	'Password																					
'USER_NAME' =>	'User			اسم																		
'EMAIL_LINK' =>	'Email			رابط																		
'PLEASE_CLICK_THE_LINK_TO_RESET_YOUR_PASSWORD' =>	'Please	your	password',	انقر الرابط لاعادة تعين كلمة المرور																		
/*	close																					
																						
/*	Start																					
'SUBSCRIPTION_EMAIL_CREATED_SUCCESSFULLY_FOR_YOUR_MAIL_ID' =>	'Subscription	mail	id',																			
'THANK_YOU_CREDENTIALS_ARE' =>	'Thank																					
'HAI_THANK_YOU' =>	'Hai			شكرا لك																		
'SUBSCRIPTION_EMAIL_IN_LARAVEL_ECOMMERCE' =>	Subscription Email in Laravel eCommerce,																					
'SUBSCRIPTION_EMAIL_IN' =>	to subscribe in,																					
'THANK_YOU_FOR_SUBSCRIPTION'	=>			شكرا للاشتراك بالنشرة الإخبارية																		
/*	close																					
																						
/*	Start																					
'YOUR_REGISTER_ACCOUNT_WAS_CREATED_SUCCESSFULLY' =>	'Your																					
'YOUR_LOGIN_CREDENTIALS_ARE' =>	'Your																					
'PASSWORD' =>	'Password',																					
'LOGIN_YOUR_ACCOUNT' =>	'Login			حسابك																		
'LARAVEL_ECOMMERCE_ALL_RIGHTS_RESERVED' =>	'All	'Product	Enquiry',																			
'NAME' =>	'Name',																					
'PRODUCT_NAME' =>	'Product			الاسم																		
'PRODUCT_DEAL_NAME' => 'Product/Deal	Name',																					
'MESSAGE' =>	'Message',																					
/*	close																					
																						
/*	Start																					
'THANK_YOU_FOR_YOUR_ORDER' =>	'Thank																					
'YOUR_TRANSACTION_ID' =>	'Your																					
'VIEW_YOUR_ORDER' =>	'VIEW			طلبك																		
'S_NO' =>	'S.no',																					
'QTY' =>	'Qty',																					
'ITEM_PRICE' =>	'Item			السعر																		
'TAX' =>	'Tax',																					
'SHIP_AMOUNT' =>	'Ship			الكمية																		
'SUBTOTAL' =>	'Subtotal',																					
'TOTAL' =>	'Total',																					
'2_TO_3_DAYS' =>	'2			الى ثلاثة ايام																		
'WILL_BE_DELIVERED_BY' =>	'Will																					
'DELIVERED_ON'	=>																					
'DELIVERY_ADDRESS' =>	'DELIVERY			العنوان																		
'ITEM_NAME' =>	'Item			الاسم																		
'STATE' =>	'State',																					
'ZIP_CODE' =>	'Zip			الرمز																		
'PHONE' =>	'Phone',																					
'ADDRESS' =>	'Address',																					
'ADDRESS1' =>	'Address1',																					
'ADDRESS2' =>	'Address2',																					
'EMAIL' =>	'Email',																					
'ANY_QUESTIONS' =>	'Any			اسئلة																		
'GET_IN_TOUCH_WITH_OUR_24X7_CUSTOMER_CARE_TEAM' =>	'Get	Customer	Care	تواصل مع خدمة العملاء على مدار ٢٤ ساعة																		
'MEANWHILE_YOU_CAN_CHECK_THE_STATUS_OF_YOUR_ORDER_ON' =>	'Meanwhile,	of	your	يمكنك تفقد حالة طلبك																		
'WE_WILL_SEND_YOU_ANOTHER_EMAIL_ONCE_THE_ITEMS_IN_YOUR_ORDER_HAVE_BEEN_SHIPPED' =>	'We	once	the																			
/*	close																					
#############################################	END																					
																						
####################################################	START																					
																						
/*	Start																					
'NO_DATAS_FOUND' =>	'No	in	Cart..!!',																			
'ADDED_TO_CART_SUCCESSFULLY' =>	'Added			الى السلة بنجاح																		
'ADD_TO_CART_FAILED_DUE_TO_SOME_ERROR' =>	'Add	some	error..!!',																			
'LOGIN_TO_PROCEED' =>	'Login			للمعالجة																		
																						
/*	close																					
/*	Start																					
'LOGIN_SUCCESS' =>	'Login			نجح																		
'PASSWORD_RECOVERY_DETAILS' =>	'Password			تفاصيل الاستعادة																		
/*	close																					
/*Start	FooterController.php*/																					
																						
'YOUR_COMMENTS_WILL_BE_SHOWN_AFTER_ADMIN_APPROVAL'	=>	after	admin																			
'YOUR_ENQUIRY_DETAILS'	=>	We	will																			
'YOUR_PRODUCT_ENQUIRY'	=>																					
																						
/*End	FooterController.php*/																					
####################################################	END																					
/*Home	controller																					
	BACK_HI_MERCHANT_YOUR_PRODUCT_PURCHASED																					
	BACK_HI_CUSTOMER_YOUR_PRODUCT_PURCHASED																					
/*End	Email																					
/*	order	*/																				
'INCLUDING'	=>																					
'TAXES'	=>			الضريبة																		
'MAX'	=>			الحد الاقصى																		
'MIN'	=>			دقيقة																		
'GO'	=>																					
'ADD_TO_COMPARE'	=>																					
'EMAIL_TO_FRIEND'	=>			ارسلها لصديق																		
/*new*/																						
REGISTERED_SUCCESSFULLY	=>			تم التسجيل بنجاح																		
YOUR_COD_SUCCESSFULLY_REGISTERED	=>																					
COD_ACKNOWLEDGEMENT	=>																					
YOUR_COD_PAYMENT_IS_SUCCESS	=>																					
YOUR_ORDER_CONFIRMATION_DETAILS_PLACED_SUCCESSFULLY	=>																					
HI_MERCHANT_YOUR_PRODUCT_PURCHASED	=>																					
																						
PAYMENT_RECEIVED_YOUR_PRODUCT_WILL_BE_SENT_TO_YOU_VERY_SOON	=>																					
YOUR_PAYMENT_HAS_BEEN_COMPLETED_SUCCESSFULLY	=>			تم الدفع بنجاح																		
YOUR_PAYMENT_SUCCESSFULLY_COMPLETED	=>																					
PAYMENT_ACKNOWLEDGEMENT	=>																					
YOUR_PAYMENT_HAS_BEEN_FAILED	=>			فشل عملية الدفع																		
SOME_ERROR_OCCURED_DURING_PAYMENT	=>																					
YOUR_PAYMENT_HAS_BEEN_CANCELLED	=>			تم الغاء الدفع																		
ERROR_ALREADY_AUCTION_HAS_BID_TRY_WITH_NEW_AMOUNT	=>																					
ALREADY_EMAIL_EXIST	=>			الحساب موجود بالفعل																		
ALREADY_USE_EMAIL_EXIST	=>																					
EMAIL_HAS_BEEN_SUBSCRIPTION_SUCCESSFULLY	=>																					
YOUR_EMAIL_SUBSCRIBED_SUCCESSFULLY	=>																					
YOUR_PRODUCT_REVIEW_POST_SUCCESSFULLY	=>			تم نشر مراجعة المنتج بنجاح																		
YOUR_DEAL_PRODUCT_REVIEW_POST_SUCCESSFULLY	=>																					
YOUR_DEAL_STORE_REVIEW_POST_SUCCESSFULLY	=>																					
RATE_FOR_THIS_STORE	=>			فضلا قم بتقيم هذا المتجر																		
NO_RESULT_FOUND	=>			لا توجد نتائج مطابقة																		
																						
/*	Product																					
REMOVE_FROM_WISHLIST	=>																					
																						
/*	CHECKOUT			صفحة																		
NO_PAYMENT_METHOD_AVAILABLE	=>																					
/*	if																					
PLEASE_LOGIN_TO_PROCEED	=>																					
MER_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY =>	'Merchant			تم انشاء الحساب																		
MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY =>	'Merchant			تم انشاء الحساب																		
																						
/*	CArt			الصفحة																		
//	Product			رسالة غير متاحة																		
PRODUCT_QUANTITY_NOT_AVAILABLE	=>																					
CART_UPDATED	=>			تم تحديث السلة																		
PRODUCT_QUANTITY_SHOULD_BE_GREATER_THAN_ZERO =>																						
DEAL_QUANTITY_SHOULD_BE_GREATER_THAN_ZERO =>																						
																						
DEAL_QUANTITY_NOT_AVAILABLE =>																						
PLEASE_SELECT_MINIMUM_PAYMENT_METHOD =>																						
PRODUCT_QUANTITY_NOT_AVAILABLE =>																						
/*	Register																					
BACK_REGISTER_ACCOUNT_CREATED_SUCCESSFULLY =>	'Register			تم انشاء الحساب بنجاح																		
																						
																						
																						
/*	mail	*/		اضف تفاصيل الكوبون والمحفظة																		
UNIT_PRICE => 'Unit	Price',																					
SUCCESS	=> 'Success',																					
COMPLETED	=> 'Completed',																					
HOLD => 'HoldFFFF',																						
FAILED => 'Failed',																						
WALLET_AMOUNT => 'Wallet',																						
GRAND_TOTAL => 'Grand	Total',																					
//Order	merchant			البريد																		
MERCHANT_YOUR_PRODUCT_PURCHASED_FOR_DETAILS =>																						
//Policy																						
CANCELLATION_ONLY =>																						
RETURN_ONLY =>																						
REPLACEMENT_ONLY =>																						
CANCELLATION =>																						
RETURN =>																						
REPLACE =>																						
ORDER_CANCELLATION =>																						
ORDER_RETURN =>																						
ORDER_REPLACE =>																						
SELECT_ITEM_TO_CANCEL =>																						
SELECT_ITEM_TO_RETURN =>																						
SELECT_ITEM_TO_REPLACE =>																						
REASON_FOR_CANCEL =>																						
REASON_FOR_RETURN =>																						
REASON_FOR_REPLACE =>																						
ENTER_YOUR_REASON_FOR_CANCEL =>																						
ENTER_YOUR_REASON_FOR_RETURN =>																						
ENTER_YOUR_REASON_FOR_REPLACE =>																						
CONFIRM_CANCEL =>																						
CONFIRM_RETURN =>																						
CONFIRM_REPLACE =>																						
ORDER_NOT_VALID =>																						
REASON_TO_CANCEL =>																						
REASON_TO_RETURN =>																						
REASON_TO_REPLACE =>																						
ORDER_ID =>																						
CANCEL_DATE =>																						
REQUEST_DATE =>																						
RETURN_DATE =>																						
REPLACE_DATE =>																						
APPROVED_OR_DISAPPROVED_DATE =>																						
APPROVED_OR_DISAPPROVED_REASON =>																						
ORDER_CANCELLATION_REQUEST =>																						
ORDER_RETURN_REQUEST =>																						
ORDER_REPLACE_REQUEST =>																						
YOU_ARE_SEND_A_CANCEL_REQUEST_FOR_THE_FOLLOWING_PRODUCT =>																						
YOU_ARE_SEND_A_RETURN_REQUEST_FOR_THE_FOLLOWING_PRODUCT =>																						
YOU_ARE_SEND_A_REPLACE_REQUEST_FOR_THE_FOLLOWING_PRODUCT =>																						
																						
YOUR_RETURN_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS =>																						
YOUR_REPLACE_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS =>																						
CUSTOMER_RETURN_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS =>																						
CUSTOMER_REPLACE_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS =>																						
																						
CUSTOMER_SEND_A_CANCEL_REQUEST_FOR_THE_FOLLOWING_PRODUCT =>																						
CUSTOMER_SEND_A_RETURN_REQUEST_FOR_THE_FOLLOWING_PRODUCT =>																						
CUSTOMER_SEND_A_REPLACE_REQUEST_FOR_THE_FOLLOWING_PRODUCT =>																						
																						
ADMIN_DELIVERY_STATUS_PRODUCT_PACKED =>																						
ORDER_PACKED =>																						
ADMIN_DELIVERY_STATUS_PRODUCT_DISPATCHED =>																						
ORDER_DISPATCHED =>																						
ADMIN_DELIVERY_STATUS_PRODUCT_DELIVERED =>																						
ORDER_DELIVERED =>																						
ADMIN_DELIVERY_STATUS_PRODUCT_CANCELLED =>																						
ORDER_CANCELLED =>																						
ADMIN_DELIVERY_STATUS_PRODUCT_REPLACED =>																						
ORDER_REPLACED =>																						
ADMIN_DELIVERY_STATUS_PRODUCT_RETURNED =>																						
ORDER_RETURNED =>																						
DATE =>																						
																						
																						
																						
//Coupon	error																					
COUPON_EXCEEDS_PRODUCT_PURCHASE_AMOUNT	=>			مبلغ الكوبون يتجاوز اجمالي طلبك. لا يمكن استخدام الكوبون حاليا.																		
CUSTOMER_BLOCKED	=>			يبدو ان حسابك ليس نشطا توصل معنا من اجل المزيد من التفاصيل																		
ALL_RIGHTS_RESERVED	=>			جميع الحقوق محفوظة																		
WALLET_USED	=>			تم استخدام المحفظة																		
																						
																						
//NEW	THEME			تحديث																		
																						
CALL_US	=>			اتصل بنا																		
HELP_CENTER	=>			مرحبا مركز																		
EMAIL_ADDRESS	=>			عنوان البريد الإلكتروني																		
WELCOME_SIGN_IN_ACCOUNT	=>			اهلا بك مجددا! قم بتسجيل الدخول الى حسابك																		
LOST_PASSWORD	=>			فقدت كلمة المرور																		
REGISTER	=>			تسجيل																		
REGISTER	=>			تسجيل																		
COPYRIGHT	=>			حقوق النشر																		
SHOW	=>			اعرض																		
ALL	=>			كل																		
FILTER	=>			تصفية النتائج																		
COLOR_FILTER	=>			اللون																		
SIZE_FILTER	=>			المقاس																		
DISCOUNT	=>			خصم																		
PRICE_FILTER	=>			الاسعار																		
SHOP_BY	=>			تسوق وفق																		
DISCOUNT	=>			خصم																		
CLEAR_ALL	=>			حذف الجميع																		
SEARCH_FOR_PRODUCTS	=>			ابحث عن المنتجات																		
SEARCH_FOR_DEALS	=>			ابحث عن العروض																		
NO_RATING_DEALS	=>			لا يوجد تقيم لهذا العرض																		
OVERVIEW	=>			نظرة عامة سريعة																		
POLICY_DETAILS	=>			شروط العروض																		
SPECIFICATION	=>			خصائص																		
CUST_REVIEW	=>			مراجعات العميل																		
RATE_FOR_PRODUCT	=>			كيف تقيم هذا المنتج؟																		
REVIEW_SUMMARY	=>			ملخص مراجعتك																		
DISCOUNT	=>			خصم																		
ORDERS_LIST	=>			قائمة الطلبات																		
ORDER_NUMBER	=>			رقم الطلب																		
ACC_DASHBOARD	=>			قائمة الحساب																		
WORLD_SHIP	=>			التسوق حول العالم																		
GET_ANYWH	=>			الشراء من اي مكان.																		
SECURE	=>			دفع امن ١٠٠%																		
BANKNG	=>			ائتمان / عملية بنكية																		
SUPPORT	=>			دعم على مدار الساعة																		
CALL_US	=>			اتصل بنا																		
MEM_DISCOUNT	=>			خصم العضو																		
USE_COUPON	=>			الشراء عن طريق الكوبونات																		
BEST_SELLING	=>			افضل العروض																		
SPECIAL_OFFERS	=>			عروض حتى ٥٠%																		
SHOP_NOW	=>			تسوق الان																		
BLOG	=>			المدونة																		
BLOG_POST	=>			نشر المدونة																		
TOP_RATE	=>			الافضل تقيما																		
FEATURED_STORE	=>			المتجر																		
NEW_PRODUCT	=>			منتجات جديدة																		
JOIN_NEWSLETTER	=>			انضم الى نشرتنا الاخبارية																		
ENTR_MAIL	=>			ادخل عنوان بريدك الإلكتروني لتحصل على																		
LATEST_NEWS	=>			اخر الاخبار																		
TO_INBOX	=>			على صندوق الوارد																		
CUS_COMMENT	=>			رائي العملاء																		
GET_NOW	=>			احصل عليها الان																		
UP_TO	=>			حتى																		
OFF	=>			خصم																		
BIG_SALE	=>			خصم كبير																		
SALE	=>			خصومات																		
BIG_SALE	=>			خصم كبير																		
BIG_SALE	=>			خصم كبير																		
WISHLIST	=>			قائمة الامنتيات																		
MY_CART	=>			سلتي																		
ACCOUNT	=>			حساب																		
ORDER_TRACKING	=>			تتبع الطلب																		
COMMENT	=>			تعليق																		
NO_SPECIFICATION_AVAILABLE	=>			الخصائص غير متوفره																		
STOCK	=>			مخزون																		
NOT_AVAILABLE	=>			غير متاح																		
RATE_FOR_THIS_PRODUCT	=>			فضلا قم بتقيم هذا المنتج																		
RATE_FOR_THIS_DEAL	=>			فضلا قم بتقيم هذا العرض																		
GREAT_SAVING	=>			تخفضيات كبيرة																		
GREAT_SALE	=>			عروض رائعه																		
HURRY_UP	=>			اسرع! عرض خاص																		
EASY_BUY	=>			سهولة الشراء																		
ESHOP_BUSINESS	=>			التسوق إلالكتروني																		
GET_START	=>			إبدأ																		
NO_PRODUCTS	=>			ليس هناك منتجات متاحة																		
NO_REVIEWS_FOUND	=>			ليس هناك مراجعات																		
BLOG_DETAILS	=>																					
ARE_U_SURE_CLEAR_ALL_FIELD	=>															];						
?>