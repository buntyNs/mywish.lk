 <?php $i = 1; ?>
                 <ul class="products-grid">
                   
                  @if(count($most_popular_product) != 0)
                
                  @foreach($most_popular_product as $product_det) 
                   
                     @if($i % 2 == 0 ) 
                     <?php  $ul = '<ul  class="products-grid">';
                      echo $ul;  ?>
                    @endif
                    
                 

                  <li class="item" alt="{{ $i }}">
                  
                      <div class="item-inner">
                 {{-- product name  --}}
                 <?php 
                 $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
               $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
               $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
               $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
               // product id  
               $res = base64_encode($product_det->pro_id);
              //product image  
               $product_image = explode('/**/',$product_det->pro_Img);
               //product price  
               $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
               // product dicount percentage  ?>
               
                
                {{-- product title  --}} 
               @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                  @php
                 $title = 'pro_title';
                  @endphp
               @else 
                 @php  
                 $title = 'pro_title_'.Session::get('lang_code'); 
                 @endphp 
               @endif
                {{-- product image --}}  
               @php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               $img_data = "public/assets/product/".$product_image[0];
               @endphp
               @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                 @php 
                 $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                  @endphp       
               @else  
                   @if(isset($DynamicNoImage['productImg']))
                   @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                         @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                         @php
                          $prod_path = url('')."/".$dyanamicNoImg_path;
                           @endphp 
                         @endif
                   @endif
               @endif
               @php $discount_percent = $alt_text = ''; @endphp
               {{-- Alt text--}}
               @php 
               $alt_text   = substr($product_det->$title,0,25);
               $alt_text  .= strlen($product_det->$title)>25?'..':''; 
               @endphp
               <div class="item-img">
                @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 
                <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="{{$alt_text}}"> 
                  <img alt="{{$alt_text}}" src="{{$prod_path}}">
                 </a> 
                @endif <!-- /*//if*/ --> 
                @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                        <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="{{$alt_text}}">
                          <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                           @endif <!-- //if -->
                        @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 
                        <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="{{$alt_text}}">
                          <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                           @endif 
                            @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                        <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="{{$alt_text}}">
                          <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a>
                          @endif 
              </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> 
                              {{ substr($product_det->$title,0,25) }}
                           {{ strlen($product_det->$title)>25?'..':'' }} </a> </div>
                            <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>
                            </div>
                            <div class="pro-action">
                              @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                  <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                  <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                              @endif
                              @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                               <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" class="product-image">
                                <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                              @endif
                              @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                                 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" class="product-image">
                                  <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                              @endif
                              @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                                   <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" class="product-image">
                                    <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                              @endif
                            </div>
                            <div class="pr-button-hover">
                              <div class="mt-button ="> <a href="wishlist.html" class="product-image add_to_wishlist"> <i class="fa fa-heart-o"></i> </a> </div>
                             
                            </div>
                          </div>
                        </div>
                      
                   
                   </div>
                   </li>
                   
                    <?php 
                    
                     $i += 1;
                 ?>
                      
                   @endforeach
                   @else
                  <p> @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif </p>
                   @endif

                    </ul>*/?>