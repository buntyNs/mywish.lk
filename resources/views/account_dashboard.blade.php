	<!DOCTYPE html>

		<html lang="en">			

			{!! $navbar !!}

			{!! $header !!}

			<style>

			.box-authentication {

				/*width: 37%;

				padding: 15px;

				border: 1px solid #ccc;

				margin: 2px;*/

			}

			.spanhelp { float:left;width:100%}

			.spanlabel {width:40%}

			.clearfix { margin-top: 5px; }

			.labelheader{float:left;width:100% }

			.editiconLabel{ float:right; }

			</style>
			<body class="myaccount_page">

				<!--[if lt IE 8]>

				<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

				<![endif]--> 

				<div id="page"> 

					@foreach($customerdetails as $customer_info)

					@endforeach

					<!-- start my work -->

					<section class="main-container col2-right-layout">

						<div class="main container">

							<div class="row">

								<div class="col-main col-sm-9 col-xs-12">

									<div class="my-account">

										<div class="page-title">

											<h4>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '') ? trans(Session::get('lang_file').'.MY_ACCOUNT') : trans($OUR_LANGUAGE.'.MY_ACCOUNT') }} </h4>

										</div>

									</div>

									<!-- Main Container -->

									<section class="main-container col1-layout">

										<div class="">

											<div class="page-content">

												<div class="account-login">

													<div class="box-authentication">

														<!-- USER NAME SECTION -->

														<span class="cusnameSuccess" style="position: absolute;font-size: 15px;left: 30%;margin-top: -8px;color: #156f0b;font-weight: 600;"></span>

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.NAME')!= '') {{  trans(Session::get('lang_file').'.NAME')}}  @else {{ trans($OUR_LANGUAGE.'.NAME')}} @endif <span class="editiconLabel" style="cursor:pointer;" id="toggleDiv"><i class="fa fa-pencil"></i> @if (Lang::has(Session::get('lang_file').'.EDIT')!= '') {{  trans(Session::get('lang_file').'.EDIT')}}  @else {{ trans($OUR_LANGUAGE.'.EDIT')}} @endif</span></label>

														<span class="help-block spanhelp" >

															<span class="spanlabel" id="cusname">{{ $customer_info->cus_name }}</span>									
													</span>

														<div style="display:none" id="username_div">

															<input id="username1" type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_NAME')}} @endif">

															<div class="submit-text">

															  <button class="button button-compare" id="update_username" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

															  <button class="button button-clear" style="color:#000" id="cancel_username">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.CANCEL')!= '') {{  trans(Session::get('lang_file').'.CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CANCEL')}} @endif</span></button>

															</div>

														</div>														

														<div class="clearfix"></div>

														<hr />

														<!-- EMAIL -->

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.EMAIL')!= '') {{  trans(Session::get('lang_file').'.EMAIL')}}  @else {{ trans($OUR_LANGUAGE.'.EMAIL')}} @endif</label>

														<span class="help-block spanhelp"><span class="spanlabel">{{ $customer_info->cus_email }}</span> 

														<div class="clearfix"><hr /></div>

														<div style="clear:both;"></div>

														<!-- PASSWORD SECTION -->

														<span class="passwordupdate" style="position: absolute;font-size: 15px;left: 30%;margin-top: -8px;color: #156f0b;font-weight: 600;"></span>

														<span class="passwordupdatefails" style="position: absolute;font-size: 15px;left: 30%;margin-top: -8px;color: #F00;font-weight: 600;"></span>
														<span class="oldpasswordupdatefails" style="position: absolute;font-size: 15px;left: 30%;margin-top: -8px;color: #F00;font-weight: 600;"></span>

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PASSWORD')}}  @else {{ trans($OUR_LANGUAGE.'.PASSWORD')}} @endif <span class="editiconLabel"  style="cursor:pointer;" id="toggleDiv1"><i class="fa fa-pencil"></i> @if (Lang::has(Session::get('lang_file').'.EDIT')!= '') {{  trans(Session::get('lang_file').'.EDIT')}}  @else {{ trans($OUR_LANGUAGE.'.EDIT')}} @endif</span></label>

														<span class="help-block spanhelp" ><span class="spanlabel" id="Password">*******</span> 

														</span>



														<div style="display:none" id="password_div">

															<input id="oldpwd" type="password" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_OLD_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_OLD_PASSWORD')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_OLD_PASSWORD')}} @endif"><br />

															<input type="password" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NEW_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_NEW_PASSWORD')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_NEW_PASSWORD')}} @endif" id="pwd"><br />	

															<input type="password" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_CONFIRM_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.ENTER_CONFIRM_PASSWORD')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_CONFIRM_PASSWORD')}} @endif" id="confirmpwd"><br />

															<div class="submit-text">

															  <button class="button button-compare" id="update_password" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

															  <button class="button button-clear" style="color:#000" id="cancel_password">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.CANCEL')!= '') {{  trans(Session::get('lang_file').'.CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CANCEL')}} @endif</span></button>

															</div>

														</div>

														<div class="clearfix"></div>

														<hr />

														<!-- PROFILE IMAGE SECTION -->

														<span class="profile_success" style="position: absolute;font-size: 15px;left: 30%;margin-top: -8px;color: #156f0b;font-weight: 600;"></span>

														<span class="profile_error" style="position: absolute;font-size: 15px;left: 30%;margin-top: -8px;color: #F00;font-weight: 600;"></span>

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.PROFILE_IMAGES')!= '') {{  trans(Session::get('lang_file').'.PROFILE_IMAGES')}}  @else {{ trans($OUR_LANGUAGE.'.PROFILE_IMAGES')}} @endif <span class="editiconLabel" style="cursor:pointer;" id="toggleImage"><i class="fa fa-pencil"></i> @if (Lang::has(Session::get('lang_file').'.EDIT')!= '') {{  trans(Session::get('lang_file').'.EDIT')}}  @else {{ trans($OUR_LANGUAGE.'.EDIT')}} @endif</span></label>

														<div style="display:none" id="image_div">

															{!! Form::open(array('url'=>'profile_image_submit','class'=>'form-horizontal loginFrm','enctype'=>'multipart/form-data')) !!}

																<div class="controls"> *Jpeg,Png

																<input  type="file" class="input-file" name ="imgfile" id="imgfile">

																<span id="error_image"> </span>

																</div>

																<br>

																<span>@if (Lang::has(Session::get('lang_file').'.IMAGE_UPLOAD_SIZE_1')!= '') {{  trans(Session::get('lang_file').'.IMAGE_UPLOAD_SIZE_1') }} @else {{ trans($OUR_LANGUAGE.'.IMAGE_UPLOAD_SIZE_1') }} @endif</span><br><br>

																<div class="submit-text">

																  <button class="button button-compare" id="update_image" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

																  <button class="button button-clear" type="button" style="color:#000" id="cancel_image">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.CANCEL')!= '') {{  trans(Session::get('lang_file').'.CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CANCEL')}} @endif</span></button>

																</div>

															</form>

														</div>

														 <br>

														   @if($customer_info->cus_pic!='')

														   <?php  $imgpath="public/assets/profileimage/".$customer_info->cus_pic;

															  ?>

														   @else

														   <?php 

															  $imgpath="themes/images/products/man.png";

															  ?>

														   @endif

														   <img src="{{ $imgpath }}" alt="" width="100px" height="auto">

														<div class="clearfix"></div>

														<div style="clear:both;"></div>

														<!-- end div -->

													</div>

													

													<div class="box-authentication">

														<!-- PHONE NUMBER SECTION -->

														<span id="success_mobile" style="position: absolute;font-size: 15px;left: 68%;margin-top: -15px;color: #156f0b;font-weight: 600;"></span>

														<span id="error_mobile" style="position: absolute;font-size: 15px;left: 68%;margin-top: -15px;color: #f00;font-weight: 600;"></span>

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.PHONE_NUMBER')!= '') {{  trans(Session::get('lang_file').'.PHONE_NUMBER')}}  @else {{ trans($OUR_LANGUAGE.'.PHONE_NUMBER')}} @endif <span class="editiconLabel"  style="cursor:pointer;" id="toggleDiv2"><i class="fa fa-pencil"></i> @if (Lang::has(Session::get('lang_file').'.EDIT')!= '') {{  trans(Session::get('lang_file').'.EDIT')}}  @else {{ trans($OUR_LANGUAGE.'.EDIT')}} @endif</span></label>

														<span class="help-block spanhelp">

															<span class="spanlabel" id="cusphone">{{ $customer_info->cus_phone }}</span> 

														</span>



														<div style="display:none" id="phonenumber_div">

															 <input type="number" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_PHONE_NUMBER')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_PHONE_NUMBER')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_PHONE_NUMBER')}} @endif" id="phonenum"> 

															<div class="submit-text">

															  <button class="button button-compare" id="update_phonenumber" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

															  <button class="button button-clear" style="color:#000" id="cancel_phonenumber">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.CANCEL')!= '') {{  trans(Session::get('lang_file').'.CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CANCEL')}} @endif</span></button>

															</div>

														</div>

														

														<div class="clearfix"></div>

														<hr />

														<!-- ADDRESS SECTION -->

														<span id="success_address" style="position: absolute;font-size: 15px;left: 68%;margin-top: -15px;color: #156f0b;font-weight: 600;"></span>

														<span id="error_address" style="position: absolute;font-size: 15px;left: 68%;margin-top: -15px;color: #f00;font-weight: 600;"></span>

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.ADDRESS')!= '') {{  trans(Session::get('lang_file').'.ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.ADDRESS')}} @endif <span class="editiconLabel"  style="cursor:pointer;" id="toggleDiv3"><i class="fa fa-pencil"></i> @if (Lang::has(Session::get('lang_file').'.EDIT')!= '') {{  trans(Session::get('lang_file').'.EDIT')}}  @else {{ trans($OUR_LANGUAGE.'.EDIT')}} @endif</span></label>

														<span class="help-block spanhelp">

															<span class="spanlabel" id="address1">{{ $customer_info->cus_address1 }}</span> <br />

															<span class="spanlabel" id="address2">{{ $customer_info->cus_address2 }}</span>

															

														</span>



														<div style="display:none" id="address_div">

															 <input type="text" class="form-control" placeholder=" @if (Lang::has(Session::get('lang_file').'.PROVIDE_ADDRESS1')!= '') {{  trans(Session::get('lang_file').'.PROVIDE_ADDRESS1')}}  @else {{ trans($OUR_LANGUAGE.'.PROVIDE_ADDRESS1')}} @endif" id="addr1"><br />

															 <input type="text" class="form-control" placeholder=" @if (Lang::has(Session::get('lang_file').'.PROVIDE_ADDRESS2')!= '') {{  trans(Session::get('lang_file').'.PROVIDE_ADDRESS2')}}  @else {{ trans($OUR_LANGUAGE.'.PROVIDE_ADDRESS2')}} @endif" id="addr2">

															<div class="submit-text">

															  <button class="button button-compare" id="update_address" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

															  <button class="button button-clear" style="color:#000" id="cancel_address">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.CANCEL')!= '') {{  trans(Session::get('lang_file').'.CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CANCEL')}} @endif</span></button>

															</div>

														</div>

														

														<div class="clearfix"></div>

														<hr />

														<!-- COUNTRY AND CITY -->

														<span id="success_country" style="position: absolute;font-size: 15px;left: 68%;margin-top: -15px;color: #156f0b;font-weight: 600;"></span>

														<span id="error_country" style="position: absolute;font-size: 15px;left: 68%;margin-top: -15px;color: #f00;font-weight: 600;"></span>

														<label for="emmail_login" class="labelheader">@if (Lang::has(Session::get('lang_file').'.COUNTRY')!= '') {{  trans(Session::get('lang_file').'.COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.COUNTRY')}} @endif & @if (Lang::has(Session::get('lang_file').'.CITY')!= '') {{  trans(Session::get('lang_file').'.CITY')}}  @else {{ trans($OUR_LANGUAGE.'.CITY')}} @endif  <span class="editiconLabel"  style="cursor:pointer;" id="toggleDiv5"><i class="fa fa-pencil"></i> @if (Lang::has(Session::get('lang_file').'.EDIT')!= '') {{  trans(Session::get('lang_file').'.EDIT')}}  @else {{ trans($OUR_LANGUAGE.'.EDIT')}} @endif</span></label>

														<span class="help-block spanhelp" >

															<span class="spanlabel" id="cuscountry">

																@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

																<?php	$co_name = 'co_name'; ?>

																@else  <?php $co_name = 'co_name_'.Session::get('lang_code'); ?> @endif 

																{{ $customer_info->$co_name }},

															</span> <br />

															<span class="spanlabel" id="cuscity">

																@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

																<?php	$ci_name = 'ci_name'; ?>

																@else  <?php  $ci_name = 'ci_name_'.Session::get('lang_code');  ?>  @endif

																{{ $customer_info->$ci_name }}

															</span>

															

														</span>

														<div style="display:none" id="country_div">

															 <label>@if (Lang::has(Session::get('lang_file').'.COUNTRY')!= '') {{  trans(Session::get('lang_file').'.COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.COUNTRY')}} @endif</label>

															<select class="form-control" id="selectcountry1"  onChange="get_city_list1(this.value)">

															   <option value="0">--@if (Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= '') {{  trans(Session::get('lang_file').'.SELECT_COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.SELECT_COUNTRY')}} @endif--</option>

															   @foreach ($country_details as $country)

															   <option  value="<?php echo $country->co_id;?>" <?php if($country->co_id==$customer_info->cus_country){ ?>selected<?php } ?>>

																  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

																  <?php 	$co_name = 'co_name'; ?>

																  @else    <?php $co_name = 'co_name_'.Session::get('lang_code'); ?>  @endif

																  {!!$country->$co_name!!}

															   </option>

															   @endforeach 

															</select>

															<label>{{ (Lang::has(Session::get('lang_file').'.CITY')!= '') ? trans(Session::get('lang_file').'.CITY') : trans($OUR_LANGUAGE.'.CITY') }}</label><?php //print_r($city_details); ?>

															<select class="form-control" id="selectcity1">

															   <option value="0">--select city--</option>

															   @foreach ($city_det as $city)

															   <option value="<?php echo $city->ci_id;?>"<?php if($city->ci_id==$customer_info->cus_city){ ?>selected<?php } ?>>

																  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

																  <?php  	$ci_name = 'ci_name'; ?>

																  @else  <?php   $ci_name = 'ci_name_'.Session::get('lang_code'); ?> @endif

																  {!!$city->$ci_name!!}

															   </option>

															   @endforeach 

															</select>

															<div class="submit-text">

															  <button class="button button-compare" id="update_city" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

															  <button class="button button-clear" style="color:#000" id="cancel_country">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.CANCEL')!= '') {{  trans(Session::get('lang_file').'.CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CANCEL')}} @endif</span></button>

															</div>

														</div>

													</div>

												</div>

											</div>

										</div>

									</section>

								</div>

								 @include('dashboard_sidebar')

							</div>

						</div>

					</section>

					<!-- IMAGE UPLOAD SECTION -->

					<!--<div id="upload_pic" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >

					   <div class="modal-header">

						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

						  <h3>@if (Lang::has(Session::get('lang_file').'.CHANGE_PROFILE_PICTURE')!= '') {{  trans(Session::get('lang_file').'.CHANGE_PROFILE_PICTURE') }} @else {{ trans($OUR_LANGUAGE.'.CHANGE_PROFILE_PICTURE') }} @endif</h3>

					   </div>

					   <div class="modal-body">

						  <div style="float:left">

							 {!! Form::open(array('url'=>'profile_image_submit','class'=>'form-horizontal loginFrm','enctype'=>'multipart/form-data')) !!}

							 <div class="controls"> *Jpeg,Png

								<input  type="file" class="input-file" name ="imgfile" id="imgfile">

								<span id="error_image"> </span>

							 </div>

							 <br>

							 <span>@if (Lang::has(Session::get('lang_file').'.IMAGE_UPLOAD_SIZE_1')!= '') {{  trans(Session::get('lang_file').'.IMAGE_UPLOAD_SIZE_1') }} @else {{ trans($OUR_LANGUAGE.'.IMAGE_UPLOAD_SIZE_1') }} @endif</span><br><br>

							 <input type="submit" id="file_submit" class="button" value="@if (Lang::has(Session::get('lang_file').'.UPLOAD')!= '') {{  trans(Session::get('lang_file').'.UPLOAD') }} @else {{ trans($OUR_LANGUAGE.'.UPLOAD') }} @endif" />

							 </form>

						  </div>

					   </div>

					</div>-->

					<!-- EOF IMAGE UPLOAD SECTION -->

					<!-- end of my work --> 

				</div>

				<script>

					$(document).ready(function(){

						//USERNAME SECTION

						var uname=$('#username1');

						$('#toggleDiv').click(function()

						{

							$('#username_div').toggle();

						});

						$('#update_username').click(function(){

							if(uname.val()=='')

							{

								uname.css('border', '1px solid red'); 

								uname.focus(); 

								return false;

							}

							else

							{

								uname.css('border', ''); 

								cname=uname.val();

								var  passdata= 'cname='+cname;



								$.ajax( {

									type: 'get',

									data: passdata,

									url: '<?php echo url('update_username_ajax'); ?>',

									success: function(responseText){  

										var result=responseText.split(",");

										if(result[0]=="success")

										{ 	

											$('#error_name').show();

											$('#error_name').html('<?php if (Lang::has(Session::get('lang_file').'.NAME_UPDATED_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.NAME_UPDATED_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.NAME_UPDATED_SUCCESSFULLY');} ?>');

											$('#error_name').fadeOut(3000);	

											$('#username_div').hide(); 

											$('#cusname').html(result[1]);

											$('#username1').val('');

											$('.cusnameSuccess').html("Updated Successfully!!!");

											$('.cusnameSuccess').fadeOut(2000);	

										}

									}		

								});		

							}

						});

						$('#cancel_username').click(function(){

							$('#username_div').hide();

						});

						

						//PASSWORD SECTION

						$('#toggleDiv1').click(function(){

							$('#password_div').toggle();

						});

						$('#cancel_password').click(function(){

							$('#password_div').hide();

						});

					   

						$('#update_password').click(function(){

							if($('#oldpwd').val()=="")

							{	

								$('#oldpwd').css('border', '1px solid red'); 

								$('#oldpwd').focus(); 

								//oldpwd.focus();

								return false;

							}

							else if($('#pwd').val()=="")

							{

								$('#oldpwd').css('border', ''); 

								 $('#pwd').css('border', '1px solid red'); 

								$('#pwd').focus(); 

								//pwd.focus();

								return false;

							}

							else if($('#confirmpwd').val()=="")

							{

								 $('#pwd').css('border', ''); 

								 $('#confirmpwd').css('border', '1px solid red'); 

								$('#confirmpwd').focus(); 

								//confirmpwd.focus();

								return false;

							}

							else

							{

								$('#confirmpwd').css('border', ''); 

								var pwd= $('#pwd').val(); 

								var confirmpwd=$('#confirmpwd').val(); 

								var oldpwd=$('#oldpwd').val(); 
                                
								var passdata = 'oldpwd='+oldpwd+"&newpwd="+pwd+"&confirmpwd="+confirmpwd;

					   

								$.ajax( {

									type: 'post',

									data: passdata,

									url: '<?php echo url('update_password_ajax'); ?>',

									success: function(responseText)

										{  

											//alert(responseText);

											var result=responseText.split(",");

											if(result[0]=="success")

											{ 	

											$('#oldpwd').val('');

											$('#pwd').val('');

											$('#confirmpwd').val('');

											//$('#error_name').show();

											/*$('#error_name').html('<?php if (Lang::has(Session::get('lang_file').'.PASSWORD_CHANGED_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.PASSWORD_CHANGED_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.PASSWORD_CHANGED_SUCCESSFULLY');} ?>');

											$('#error_name').fadeOut(3000);	*/

											$('#password_div').hide(2000); 

											$('.passwordupdate').html("{{ (Lang::has(Session::get('lang_file').'.UPDATED_SUCCESSFULLY')!= '') ? trans(Session::get('lang_file').'.UPDATED_SUCCESSFULLY') : trans($OUR_LANGUAGE.'.UPDATED_SUCCESSFULLY') }}");

											$('.passwordupdate').fadeOut(2000);	

											}

											if(result[0]=="fail1")

											{ 	

											//$('#error_name').show();

											$('.passwordupdatefails').html('<?php if (Lang::has(Session::get('lang_file').'.BOTH_PASSWORDS_DO_NO_MATCH')!= '') { echo  trans(Session::get('lang_file').'.BOTH_PASSWORDS_DO_NO_MATCH');}  else { echo trans($OUR_LANGUAGE.'.BOTH_PASSWORDS_DO_NO_MATCH');} ?>');

											$('.passwordupdatefails').fadeOut(3000);	

											}

											if(result[0]=="fail2")

											{ 	

											//$('#error_name').show();

											$('.oldpasswordupdatefails').html('<?php if (Lang::has(Session::get('lang_file').'.OLD_PASSWORD_DOES_NOT_MATCH')!= '') { echo  trans(Session::get('lang_file').'.OLD_PASSWORD_DOES_NOT_MATCH');}  else { echo trans($OUR_LANGUAGE.'.OLD_PASSWORD_DOES_NOT_MATCH');} ?>');

											$('.oldpasswordupdatefails').fadeOut(3000);	

											}

										}		

									});		

							}

						});

						//PROFILE SECTION

						$('#file_submit').click(function(){

							if($('#imgfile').val()=='')

							{

								$('#error_image').html('<?php if (Lang::has(Session::get('lang_file').'.IMAGE_FIELD_REQUIRED')!= '') { echo  trans(Session::get('lang_file').'.IMAGE_FIELD_REQUIRED');}  else { echo trans($OUR_LANGUAGE.'.IMAGE_FIELD_REQUIRED');} ?>!');

								return false;



							}

							if($('#imgfile').val()!='') {

								var checkimage = /\.(jpe?g|png)$/i.test($('#imgfile').val()); 

								if (!checkimage) {

									$('#error_image').html('upload image like jpg,png,jpeg format');

									return false;

								} 

							} 

						});

						

						//PHONE NUMBER

						$('#toggleDiv2').click(function(){

							$('#phonenumber_div').toggle();

						});

						$('#cancel_phonenumber').click(function(){

							$('#phonenumber_div').hide();

						});

   

						$('#update_phonenumber').click(function(){

					   

							if($('#phonenum').val()=="")

							{

								//$('#error_mobile').html("Mobile number should not be empty");

								$('#error_mobile').html('<?php if (Lang::has(Session::get('lang_file').'.PLEASE_PROVIDE_PHONENUMBER')!= '') { echo  trans(Session::get('lang_file').'.PLEASE_PROVIDE_PHONENUMBER');}  else { echo trans($OUR_LANGUAGE.'.PLEASE_PROVIDE_PHONENUMBER');} ?>');

								$('#error_mobile').fadeOut(3000);

								$('#phonenum').focus();

								return false;

							}

							else

							{

								var phonenum= $('#phonenum').val(); 

								var passdata = 'phonenum='+phonenum;

					   

								$.ajax({

									type: 'get',

									data: passdata,

									url: '<?php echo url('update_phonenumber_ajax'); ?>',

									success: function(responseText)

										{  

											var result=responseText.split(",");

											if(result[0]=="success")

											{ 	

											$('#success_mobile').show();

											$('#success_mobile').html('<?php if (Lang::has(Session::get('lang_file').'.PHONENUMBER_CHANGED_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.PHONENUMBER_CHANGED_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.PHONENUMBER_CHANGED_SUCCESSFULLY');} ?>');

											$('#success_mobile').fadeOut(3000);	

											$('#phonenumber_div').hide(); 

											$('#cusphone').html(result[1]);

											}

					   

										}		

									});		

							}

						});

						//ADDRESS

						$('#toggleDiv3').click(function(){

							$('#address_div').toggle();

						});

						

						$('#cancel_address').click(function(){

							$('#address_div').hide();

						});

   

						$('#update_address').click(function(){

					   

							if($('#addr1').val()=="" && $('#addr2').val()=="")

							{

								$('#error_address').show();

								$('#error_address').html('<?php if (Lang::has(Session::get('lang_file').'.PLEASE_PROVIDE_ANY_ONE_OF_THE_ADDRESS_FIELDS')!= '') { echo  trans(Session::get('lang_file').'.PLEASE_PROVIDE_ANY_ONE_OF_THE_ADDRESS_FIELDS');}  else { echo trans($OUR_LANGUAGE.'.PLEASE_PROVIDE_ANY_ONE_OF_THE_ADDRESS_FIELDS');} ?>');

								$('#error_address').fadeOut(3000);

								$('#phonenum').focus();

								return false;

							}

							else

							{

								var addr1= $('#addr1').val(); 

								var addr2= $('#addr2').val(); 

					   

								var passdata ='addr1='+addr1+"&addr2="+addr2;

					   

								$.ajax( {

									type: 'get',

									data: passdata,

									url: '<?php echo url('update_address_ajax'); ?>',

									success: function(responseText){  

										var result=responseText.split(",");

										if(result[0]=="success"){ 	

											$('#success_address').show();

											$('#success_address').html('<?php if (Lang::has(Session::get('lang_file').'.ADDRESS_CHANGED_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.ADDRESS_CHANGED_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.ADDRESS_CHANGED_SUCCESSFULLY');} ?>');

											$('#success_address').fadeOut(3000);	

											$('#address_div').hide(); 

											$('#address1').html(result[1]);

											$('#address2').html(result[2]);

										}



									}		

								});		

							}

						});

						//COUNTRY AND CITY

						$('#toggleDiv5').click(function(){

							$('#country_div').toggle();

						});

						$('#cancel_country').click(function(){

							$('#country_div').hide();

						});



						

						$('#update_city').click(function(){

							var cityid=$("#selectcity1 option:selected").val();

							var countryid=$("#selectcountry1 option:selected").val();

							var passdata ='cityid='+cityid+"&countryid="+countryid;

					   

							$.ajax( {

								type: 'get',

								data: passdata,

								url: '<?php echo url('update_city_ajax'); ?>',

								success: function(responseText){  



									var result=responseText.split(",");

									if(result[0]=="success")

									{ 	



										$('#success_country').show();

										$('#success_country').html('<?php if (Lang::has(Session::get('lang_file').'.CITY_AND_COUNTRY_CHANGED_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.CITY_AND_COUNTRY_CHANGED_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.CITY_AND_COUNTRY_CHANGED_SUCCESSFULLY');} ?>');

										$('#success_country').fadeOut(3000);	

										$('#cuscountry').html(result[1]);

										$('#cuscity').html(result[2]);

										$('#city_div').hide(); 

										$('#country_div').hide();



									}



								}		

							});		

					   

						});

						//IMAGE

						$('#toggleImage').click(function(){

							$('#image_div').toggle();

						});

						

						$('#cancel_image').click(function(){

							$('#image_div').hide();

						});

						

						$('#update_image').click(function(){

							if($('#imgfile').val()=='')

							{

								$('#profile_error').html('<?php if (Lang::has(Session::get('lang_file').'.IMAGE_FIELD_REQUIRED')!= '') { echo  trans(Session::get('lang_file').'.IMAGE_FIELD_REQUIRED');}  else { echo trans($OUR_LANGUAGE.'.IMAGE_FIELD_REQUIRED');} ?>!');

								return false;



							}

							if($('#imgfile').val()!='') {

								var checkimage = /\.(jpe?g|png)$/i.test($('#imgfile').val()); 

								if (!checkimage) {

									$('#profile_error').html('upload image like jpg,png,jpeg format');

									return false;

								} 

							} 

						});

					});

					function get_city_list1(id)

					{



						var passcityid = 'id='+id;

						$.ajax( {

							type: 'get',

							data: passcityid,

							url: '<?php echo url('register_getcountry'); ?>',

							success: function(responseText){  

								if(responseText)

								{ 	 

									$('#city_div').show();



									$('#selectcity1').html(responseText);					   

								}

							}		

						});		

					}

				</script>

				<!-- Footer -->

				{!!  $footer !!}

				<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



				<!-- End Footer --> 

				<!-- JS --> 





			</body>



			

		</html>