	<!DOCTYPE html>

		<html lang="en">



			

			{!! $navbar !!}

			{!! $header !!}

			<style>

			.box-authentication {

				/*width: 37%;

				padding: 15px;

				border: 1px solid #ccc;

				margin: 2px;*/

			}

			.spanhelp { float:left;width:100%}

			.spanlabel {width:40%}

			.clearfix { margin-top: 5px; }

			.labelheader{float:left;width:100% }

			.editiconLabel{ float:right; }

			</style>



			<body class="myaccount_page">

				<!--[if lt IE 8]>

				<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

				<![endif]--> 



				<div id="page"> 

					@foreach($customerdetails as $customer_info)

					@endforeach



					<!-- start my work -->

					<section class="main-container col2-right-layout">

						<div class="main container">

							<div class="row">

								<div class="col-main col-sm-9 col-xs-12">

									<div class="my-account">

										<div class="page-title">

											<h4>{{ (Lang::has(Session::get('lang_file').'.SHIPPING_ADDRESS')!= '') ? trans(Session::get('lang_file').'.SHIPPING_ADDRESS') : trans($OUR_LANGUAGE.'.SHIPPING_ADDRESS') }}</h4>

										</div>

									</div>

									<!-- Main Container -->

									<section class="main-container col1-layout">

										<div class="">

											<div class="page-content">

												<div class="account-login accnt-shppng">

													<span class="shipsuccess_name" style="position: absolute;font-size: 15px;color: #156f0b;font-weight: 600;text-align: right;width: 96%;"></span>

													<span class="shiperror_name" style="position: absolute;font-size: 15px;color: #F00;font-weight: 600;text-align: right;width: 96%;"></span><br>

													<div class="box-authentication">

														<p class="mandarory_txt"><span style="color: #F00;">* <strong>@if (Lang::has(Session::get('lang_file').'.ALL_FIELDS_ARE_MANDATORY')!= '') {{ trans(Session::get('lang_file').'.ALL_FIELDS_ARE_MANDATORY') }} @else {{ trans($OUR_LANGUAGE.'.ALL_FIELDS_ARE_MANDATORY') }} @endif</strong></span></p>

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.FULL_NAME')!= '') {{  trans(Session::get('lang_file').'.FULL_NAME') }} @else {{ trans($OUR_LANGUAGE.'.FULL_NAME') }} @endif<span class="required">*</span></label>

														<input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME')!= '') {{ trans(Session::get('lang_file').'.ENTER_YOUR_NAME') }}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_NAME') }} @endif" name="shippingcusname" id="shippingcusname"  maxlength ="50" value="{{ $customer_info->ship_name }}">

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.ADDRESS')!= '') {{  trans(Session::get('lang_file').'.ADDRESS') }} @else {{ trans($OUR_LANGUAGE.'.ADDRESS') }} @endif<span class="required">*</span></label>

														<input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS') }} @endif" name="shipaddr1" id="shipaddr1" maxlength="90" value="{{ $customer_info->ship_address1 }}">

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.ADDRESS2')!= '') {{  trans(Session::get('lang_file').'.ADDRESS2') }} @else {{ trans($OUR_LANGUAGE.'.ADDRESS2') }} @endif<span class="required">*</span></label>

														<input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS') }} @endif" name="shipaddr2" id="shipaddr2" maxlength="90"  value="{{ $customer_info->ship_address2 }}">

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.MOBILE')!= '') {{ trans(Session::get('lang_file').'.MOBILE') }} @else {{ trans($OUR_LANGUAGE.'.MOBILE') }} @endif<span class="required">*</span></label>

														<input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MOBILE_NUMBER')!= '') {{ trans(Session::get('lang_file').'.ENTER_YOUR_MOBILE_NUMBER') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MOBILE_NUMBER') }} @endif" name="shipcusmobile" id="shipcusmobile" maxlength="16"  value="{{ $customer_info->ship_phone }} " />

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.EMAIL')!= '') {{ trans(Session::get('lang_file').'.EMAIL') }} @else {{ trans($OUR_LANGUAGE.'.EMAIL') }} @endif<span class="required">*</span></label>

														<input type="email" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_ID') }} @endif" name="shipcusemail" id="shipcusemail"  maxlength="50"  value="{{ $customer_info->ship_email }}" /> <span id="shipcusemail_error"> </span>

														

														

														<!-- end div -->

													</div>

													

													<div class="box-authentication">

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.COUNTRY')!= '') {{ trans(Session::get('lang_file').'.COUNTRY') }} @else {{ trans($OUR_LANGUAGE.'.COUNTRY') }} @endif <span class="required">*</span></label>

														<select class="form-control" name="shippingcountry" id="shippingcountry" onChange="get_city_listshipping(this.value)">

														 <option value="0">--@if (Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= '') {{  trans(Session::get('lang_file').'.SELECT_COUNTRY') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_COUNTRY') }} @endif--</option>

														 @foreach ($country_details as $country) 

														 <option  value="<?php echo $country->co_id;?>"  <?php if($hasship==1){ if($customer_info->ship_country==$country->co_id){ echo 'selected'; } }?>>

															@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

															@php	$co_name = 'co_name'; @endphp

															@else @php  $co_name = 'co_name_'.Session::get('lang_code'); @endphp @endif

															{!!$country->$co_name!!} 

														 </option>

														 @endforeach 

													  </select>

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.CITY')!= '') {{  trans(Session::get('lang_file').'.CITY') }} @else {{ trans($OUR_LANGUAGE.'.CITY') }} @endif<span class="required">*</span></label>

														<select class="form-control" id="shippingcity" name="shippingcity">

														 <option value="0">--select city--</option>

														 @foreach ($city_shipping as $city) 

														 <option value="{{ $city->ci_id }}"<?php if($city->ci_id==$customer_info->ship_ci_id){ ?>selected<?php } ?>>

															@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

															@php	$ci_name = 'ci_name'; @endphp

															@else @php  $ci_name = 'ci_name_'.Session::get('lang_code'); @endphp @endif

															{!!$city->$ci_name!!}

														 </option>

														 @endforeach 

													  </select>

													  

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.STATE')!= '') {{ trans(Session::get('lang_file').'.STATE') }} @else {{ trans($OUR_LANGUAGE.'.STATE') }} @endif<span class="required">*</span></label>

														<input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STATE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_STATE') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_STATE') }} @endif" name="shippingstate" id="shippingstate"  maxlength="50" value="{{ $customer_info->ship_state }}">

														

														<label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.ZIPCODE')!= '') {{  trans(Session::get('lang_file').'.ZIPCODE') }} @else {{ trans($OUR_LANGUAGE.'.ZIPCODE') }} @endif<span class="required">*</span></label>

														<input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_ZIP_CODE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_ZIP_CODE') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_ZIP_CODE') }} @endif" name="zipcode" id="zipcode"   value="<?php echo $customer_info->ship_postalcode;//if($hasship==1){echo $shipping_info->ship_postalcode;}?>" maxlength="6">

														

														<div class="submit-text">

														  <button class="button button-compare" id="update_shippinginfo" ><span>@if (Lang::has(Session::get('lang_file').'.UPDATE')!= '') {{  trans(Session::get('lang_file').'.UPDATE')}}  @else {{ trans($OUR_LANGUAGE.'.UPDATE')}} @endif</span></button>

														  <button class="button button-clear" type="reset" style="color:#000" id="cancel_shippinginfo">&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.RESET')!= '') {{  trans(Session::get('lang_file').'.RESET') }} @else {{ trans($OUR_LANGUAGE.'.RESET') }} @endif</span></button>

														</div>

													</div>

												</div>

											</div>

										</div>

									</section>

								</div>

								 @include('dashboard_sidebar')

							</div>

						</div>

					</section>

					<!-- IMAGE UPLOAD SECTION -->

					<!--<div id="upload_pic" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >

					   <div class="modal-header">

						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

						  <h3>@if (Lang::has(Session::get('lang_file').'.CHANGE_PROFILE_PICTURE')!= '') {{  trans(Session::get('lang_file').'.CHANGE_PROFILE_PICTURE') }} @else {{ trans($OUR_LANGUAGE.'.CHANGE_PROFILE_PICTURE') }} @endif</h3>

					   </div>

					   <div class="modal-body">

						  <div style="float:left">

							 {!! Form::open(array('url'=>'profile_image_submit','class'=>'form-horizontal loginFrm','enctype'=>'multipart/form-data')) !!}

							 <div class="controls"> *Jpeg,Png

								<input  type="file" class="input-file" name ="imgfile" id="imgfile">

								<span id="error_image"> </span>

							 </div>

							 <br>

							 <span>@if (Lang::has(Session::get('lang_file').'.IMAGE_UPLOAD_SIZE_1')!= '') {{  trans(Session::get('lang_file').'.IMAGE_UPLOAD_SIZE_1') }} @else {{ trans($OUR_LANGUAGE.'.IMAGE_UPLOAD_SIZE_1') }} @endif</span><br><br>

							 <input type="submit" id="file_submit" class="button" value="@if (Lang::has(Session::get('lang_file').'.UPLOAD')!= '') {{  trans(Session::get('lang_file').'.UPLOAD') }} @else {{ trans($OUR_LANGUAGE.'.UPLOAD') }} @endif" />

							 </form>

						  </div>

					   </div>

					</div>-->

					<!-- EOF IMAGE UPLOAD SECTION -->

					<!-- end of my work --> 

				</div>

				<script>

					function get_city_listshipping(id)

					{

						var passcityid = 'id='+id;

						$.ajax( {

							type: 'get',

							data: passcityid,

							url: '<?php echo url('register_getcountry'); ?>',

							success: function(responseText){  

								if(responseText)

								{ 	 

									$('#shippingcity').html(responseText);					   

								}

							}		

						});		

					}

					$(document).ready(function(){

						$('#cancel_shippinginfo').click(function(){

							//if(confirm('Are you sure want to clear all fields?'))
							if(confirm('{{ (Lang::has(Session::get('lang_file').'.ARE_U_SURE_CLEAR_ALL_FIELD')!= '') ? trans(Session::get('lang_file').'.ARE_U_SURE_CLEAR_ALL_FIELD') : trans($OUR_LANGUAGE.'.ARE_U_SURE_CLEAR_ALL_FIELD') }}'))

							{

								$('input').val('');

								$('option').attr('selected', false);

							}

						});

						$('#update_shippinginfo').click(function(){	

							var citysel=$("#shippingcity  option:selected").val();

							var countrysel = $("#shippingcountry option:selected").val();

							if($('#shippingcusname').val()=="")

							{	

								$('#shippingcusname').css('border', '1px solid red').focus();

								return false;

							}

							else if($('#shipaddr1').val()=="")

							{

								$('#shipaddr1').css('border', '1px solid red').focus();

								return false;

							}

							else if($('#shipaddr2').val()=="")

							{

								$('#shipaddr2').css('border', '1px solid red').focus();

								return false;

							}

							else if($('#shipcusmobile').val()=="")

							{	

								$('#shipcusmobile').css('border', '1px solid red').focus();

								return false;

							}

							else if($('#shipcusemail').val()=="")

							{	

								$('#shipcusemail').css('border', '1px solid red').focus();

								return false;

							}

							else if($('#shipcusmobile').val().length<8)

							{	

								$('.shiperror_name').show();

								$('.shiperror_name').html('<?php if (Lang::has(Session::get('lang_file').'.PLEASE_PROVIDE_VALID_PHONE_NUMBER')!= '') { echo  trans(Session::get('lang_file').'.PLEASE_PROVIDE_VALID_PHONE_NUMBER');}  else { echo trans($OUR_LANGUAGE.'.PLEASE_PROVIDE_VALID_PHONE_NUMBER');} ?>');

								$('.shiperror_name').fadeOut(3000);	

								$('#shipcusmobile').focus();

								return false;

							}

							else if(countrysel==0)

							{

								alert("{{ (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= '') ? trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY') : trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY') }}");

								//$('#shippingcountry').focus();

								return false;

							}

							else if(citysel==0)

							{

								alert("{{ (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CITY')!= '') ? trans(Session::get('lang_file').'.PLEASE_SELECT_CITY') : trans($OUR_LANGUAGE.'.PLEASE_SELECT_CITY') }}");

								//$('#shippingcity').focus();

								return false;

							}



							else if($('#shippingstate').val()=="")

							{

								$('#shippingstate').focus();

								return false;

							}

							else if($('#zipcode').val()=="")

							{

								$('#zipcode').focus();

								return false;

							}



							if($('#shipcusemail').val()!="")

							{	

								var shipcusemail= $('#shipcusemail').val();

								var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

								if(!regex.test(shipcusemail)){

									$('#shipcusemail').focus();

									$('#shipcusemail_error').html("{{ (Lang::has(Session::get('lang_file').'.INVALID_EMAIL')!= '') ? trans(Session::get('lang_file').'.INVALID_EMAIL') : trans($OUR_LANGUAGE.'.INVALID_EMAIL') }}");

									setTimeout(function(){$('#shipcusemail_error').hide();},3000); //Timeout for error message

									return false;

								}



								else

								{

									var shipcus= $('#shippingcusname').val(); 

									var shipaddr1=$('#shipaddr1').val(); 

									var shipaddr2=$('#shipaddr2').val(); 

									var shipcusmobile= $('#shipcusmobile').val();

									var shipcusemail= $('#shipcusemail').val();  

									var shippingstate=$('#shippingstate').val(); 

									var zipcode=$('#zipcode').val(); 

									var cityid=$("#shippingcity option:selected").val();

									var countryid=$("#shippingcountry option:selected").val();



									var passdata = 'shipcus='+shipcus+"&shipaddr1="+shipaddr1+"&shipaddr2="+shipaddr2+"&shipcusmobile="+shipcusmobile+"&shipcusemail="+shipcusemail+"&shippingstate="+shippingstate+"&zipcode="+zipcode+"&shippingcity="+cityid+"&shippingcountry="+countryid;



									$.ajax( {

										type: 'get',

										data: passdata,

										url: '<?php echo url('update_shipping_ajax'); ?>',

										success: function(responseText){  

											//var result=responseText.split(",");

											if(responseText=="success")

											{ 	

												$('.shipsuccess_name').show();

												$('.shipsuccess_name').html('<?php if (Lang::has(Session::get('lang_file').'.SHIPPING_DETAILS_UPDATED_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.SHIPPING_DETAILS_UPDATED_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.SHIPPING_DETAILS_UPDATED_SUCCESSFULLY');} ?>');

												$('.shipsuccess_name').fadeOut(3000);	

												alert('{{ (Lang::has(Session::get('lang_file').'.UPDATED_SUCCESSFULLY')!= '') ? trans(Session::get('lang_file').'.UPDATED_SUCCESSFULLY') : trans($OUR_LANGUAGE.'.UPDATED_SUCCESSFULLY') }}');

											}else{

												alert('{{ (Lang::has(Session::get('lang_file').'.NO_DATA_UPDATED')!= '') ? trans(Session::get('lang_file').'.NO_DATA_UPDATED') : trans($OUR_LANGUAGE.'.NO_DATA_UPDATED') }}');

											}

										}

									}); 

								}

							}

						});

					});

				</script>

				<!-- Footer -->

				{!!  $footer !!}

				<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



				<!-- End Footer --> 

				<!-- JS --> 





			</body>



			

		</html>