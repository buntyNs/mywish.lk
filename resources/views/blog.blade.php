<?php /* <body style="overflow-x: hidden;"> */ ?>
{!! $navbar !!}
<!-- Navbar ================================================== -->
{!! $header !!}
<!-- Header End====================================================================== -->
<div class="span12">
  <div class="breadcrumbs">
    <div class="container"> 
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li>
            <li><strong>@if (Lang::has(Session::get('lang_file').'.BLOG')!= '')  {{ trans(Session::get('lang_file').'.BLOG') }}  @else {{ trans($OUR_LANGUAGE.'.BLOG') }}  @endif</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  </div>
<section class="blog_post">
    <div class="container"> 
      
      <!-- Center colunm-->
      <div class="blog-wrapper">
        <div class="page-title">
          <h2>{{ (Lang::has(Session::get('lang_file').'.BLOG_POST')!= '') ? trans(Session::get('lang_file').'.BLOG_POST') : trans($OUR_LANGUAGE.'.BLOG_POST') }}</h2>
        </div>

        <ul class="blog-posts">

        @if(count($get_blog_list) > 0)
        	@foreach($get_blog_list as $fetchblog_list)
	        	<li class="post-item col-md-4">
	            <div class="blog-box">
	            	@php  $product_image     = $fetchblog_list->blog_image;             
            		$prod_path  = url('').'/public/assets/default_image/No_image_blog.png';
            		$img_data   = "public/assets/blogimage/".$product_image; @endphp            
           			@if(file_exists($img_data) && $product_image!='' )
           				@php   $prod_path = url('public/assets/blogimage/').'/'.$product_image; @endphp                 
            		@else  
                		@if(isset($DynamicNoImage['blog']))                
                 			@php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['blog']; @endphp
                   			@if($DynamicNoImage['blog']!='' && file_exists($dyanamicNoImg_path)) 
                      		@php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp 
                      		@endif
                		@endif
            		@endif 
            		<img class="primary-img" width="320" height="190" src="{{ $prod_path }}" alt="{{ $fetchblog_list->blog_title }}">
	                <div class="blog-btm-desc">
	                	<div class="blog-top-desc">
	                    	<div class="blog-date" style="height: auto;">
	                    		<?php   $created_date =  $fetchblog_list->blog_created_date;
										$explode_date = explode(" ",$created_date);
										echo  date('jS \of M Y', strtotime($explode_date[0])); ?>
							</div>
	                    	<h4>
	                    		@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
								@php	$blog_title = 'blog_title'; @endphp
								@else @php  $blog_title = 'blog_title_'.Session::get('lang_code'); @endphp @endif
								{{ substr($fetchblog_list->$blog_title,0,20) }}
							</a></h4>
	                    	<div class="jtv-entry-meta"> 
	                    		<i class="fa fa-user-o"></i> <strong>@if (Lang::has(Session::get('lang_file').'.POSTED_BY')!= '') {{  trans(Session::get('lang_file').'.POSTED_BY') }} @else {{ trans($OUR_LANGUAGE.'.POSTED_BY') }} @endif  <a>@if (Lang::has(Session::get('lang_file').'.ADMIN')!= '') {{  trans(Session::get('lang_file').'.ADMIN') }} @else {{ trans($OUR_LANGUAGE.'.ADMIN') }} @endif</a></strong> <a href="{{ url('blog_comment/'. $fetchblog_list->blog_id) }}"><i class="fa fa-commenting-o"></i> <strong>{{ $get_blog_list_count[$fetchblog_list->blog_id] }} 
   								@if (Lang::has(Session::get('lang_file').'.COMMENTS')!= '') {{ trans(Session::get('lang_file').'.COMMENTS') }} @else {{ trans($OUR_LANGUAGE.'.COMMENTS') }} @endif</strong></a>
   							</div>
	                  	</div>
	                  	<p> @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
						   		@php  $blog_desc = 'blog_desc'; @endphp
						 	@else @php  $blog_desc = 'blog_desc_'.Session::get('lang_code'); @endphp 
						 	@endif
   							{{ substr($fetchblog_list->$blog_desc,0,10) }}<b>....</b>
   						</p>
	                  	<a class="read-more" href="{{ url('blog_view/'.$fetchblog_list->blog_id) }}">
	                  		@if (Lang::has(Session::get('lang_file').'.CONTINUE_READING')!= '') {{  trans(Session::get('lang_file').'.CONTINUE_READING') }} @else {{ trans($OUR_LANGUAGE.'.CONTINUE_READING') }} @endif
	                  	</a> 
	                </div>
	            </div>
	            </li>
            @endforeach      
        @else
        	<h4 style="color:#F00;">@if (Lang::has(Session::get('lang_file').'.NO_BLOGS_AVAILABLE')!= '') {{  trans(Session::get('lang_file').'.NO_BLOGS_AVAILABLE') }} @else {{ trans($OUR_LANGUAGE.'.NO_BLOGS_AVAILABLE') }} @endif...</h4>
        @endif

        </ul>
        <div class="sortPagiBar">
          <div class="pagination-area">
            <ul>
         	{{ $get_blog_list->render() }}

            </ul>
          </div>
        </div>
      </div>
      <!-- ./ Center colunm --> 
      
    </div>
  </section>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
<!-- <script src="{{ url('') }}/themes/js/jquery-1.10.0.min.js"></script> -->
	{!! $footer !!}
<!-- Placed at the end of the document so the pages load faster ============================================= -->
<!-- <script src="{{ url('') }}/themes/js/jquery.js" type="text/javascript"></script> -->
 @if($get_blog_comment_check) 
 @foreach($get_blog_comment_check as $check_comment_by_admin)  @endforeach  @php $check_comment_byadmin = $check_comment_by_admin->bs_postsppage;  @endphp
 @else @php $check_comment_byadmin=0;@endphp @endif
<script>
 $(function() {
 $('#log_grid_1.paginated').each(function() {
 var currentPage = 0;
 var numPerPage = <?php echo $check_comment_byadmin; ?>;
 var $grid = $(this);
 $grid.bind('repaginate', function() {
 $grid.find('.log_grid_1_record').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
 });
 $grid.trigger('repaginate');
 var numRows = $grid.find('.log_grid_1_record').length;
 var numPages = Math.ceil(numRows / numPerPage);
 var $pager = $('<div> <b style="color:#F60" > Pages >></b> </div>');
 for (var page = 0; page < numPages; page++) {
 $('<span style="cursor:pointer;padding-left:5px;color:green;font-weight:800;" ></span>').text(page + 1).bind('click', {
 newPage: page
 }, function(event) {
 currentPage = event.data['newPage'];
 $grid.trigger('repaginate');
 $(this).addClass('active').siblings().removeClass('active');
 }).appendTo($pager).addClass('clickable');
 }
 $pager.insertBefore($grid).find('span.page-number:first').addClass('active');
 });
 });
</script>
<!--     <script src="{{ url('') }}/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ url('') }}/themes/js/google-code-prettify/prettify.js"></script>
	
	<script src="{{ url('') }}/themes/js/bootshop.js"></script>
    <script src="{{ url('') }}/themes/js/jquery.lightbox-0.5.js"></script>  -->
    
<!--<script src="{{ url('') }}/plug-k/demo/js/jquery-1.8.0.min.js" type="text/javascript"></script> -->
     <script type="text/javascript">
$(document).ready(function(){

$("#searchbox").keyup(function(e) 
{

var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;
if(searchbox=='')
{
$("#display").html("").hide();	
}
else
{
	var passData = 'searchword='+ searchbox;
	 $.ajax( {
			      type: 'GET',
				  data: passData,
				  url: '<?php echo url('autosearch'); ?>',
				  success: function(responseText){  
				  $("#display").html(responseText).show();	
}
});
}
return false;    

});
});
</script>
    <script src="{{ url('') }}/plug-k/js/bootstrap-typeahead.js" type="text/javascript"></script>
    
    <script src="{{ url('') }}/plug-k/demo/js/demo.js" type="text/javascript"></script>
	

<!-- <script type="text/javascript" src="{{ url('') }}/themes/js/jquery-1.5.2.min.js"></script> -->
	<script type="text/javascript" src="{{ url('') }}/themes/js/scriptbreaker-multiple-accordion-1.js"></script> 
    <script language="JavaScript">
    
    $(document).ready(function() {
        $(".topnav").accordion({
            accordion:true,
            speed: 500,
            closedSign: '<span class="icon-chevron-right"></span>',
            openedSign: '<span class="icon-chevron-down"></span>'
        });
    });
    
    </script>
	
	<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
	</script>
</body>
</html>