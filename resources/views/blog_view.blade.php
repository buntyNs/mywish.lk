{!! $navbar !!}
<!-- Navbar ================================================== -->
{!! $header !!}
<!-- Header End====================================================================== -->
<section class="blog_post">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-9">
        @if($get_blog_list)
        @foreach($get_blog_list as $fetchblog_list) @endforeach
          <div class="entry-detail">            
            <div class="page-title"><h1>@if (Lang::has(Session::get('lang_file').'.BLOG_DETAILS')!= '') {{ trans(Session::get('lang_file').'.BLOG_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.BLOG_DETAILS') }} @endif</h1></div>
            <div class="entry-photo">
                 @php     $product_image     = $fetchblog_list->blog_image;            
                $prod_path  = url('').'/public/assets/default_image/No_image_blog.png';
                $img_data   = "public/assets/blogimage/".$product_image; @endphp            
                @if(file_exists($img_data) && $product_image!='' )             
                    @php  $prod_path = url('public/assets/blogimage/').'/'.$product_image;   @endphp               
                @else  
                    @if(isset($DynamicNoImage['blog']))                 
                        @php    $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['blog']; @endphp
                        @if($DynamicNoImage['blog']!='' && file_exists($dyanamicNoImg_path)) 
                        @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp 
                        @endif
                    @endif
                @endif  
              <figure><img src="{{ $prod_path }}" alt="Blog"></figure>
            </div>
            <div class="entry-meta-data"> 
            
            <div class="blog-top-desc">
                    <div class="blog-date"> 
                        <?php   $created_date =  $fetchblog_list->blog_created_date;
                            $explode_date = explode(" ",$created_date);
                            echo  date('jS \of M Y', strtotime($explode_date[0])); ?> 
                    </div>
                    <h1><a href="">
                        @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                            @php    $blog_title = 'blog_title'; @endphp
                        @else @php  $blog_title = 'blog_title_'.Session::get('lang_code'); @endphp @endif
                        {{ $fetchblog_list->$blog_title }}
                    </a></h1>
                    <div class="jtv-entry-meta"> <i class="fa fa-user-o"></i> 
                        <strong>@if (Lang::has(Session::get('lang_file').'.POSTED_BY')!= '') {{  trans(Session::get('lang_file').'.POSTED_BY') }} @else {{ trans($OUR_LANGUAGE.'.POSTED_BY') }} @endif    @if (Lang::has(Session::get('lang_file').'.ADMIN')!= '') {{  trans(Session::get('lang_file').'.ADMIN') }} @else {{ trans($OUR_LANGUAGE.'.ADMIN') }} @endif</strong> 
                        <a href="{{ url('blog_comment/'. $fetchblog_list->blog_id) }}"><i class="fa fa-commenting-o"></i> <strong>{{ $get_blog_list_count[$fetchblog_list->blog_id] }} 
                        @if (Lang::has(Session::get('lang_file').'.COMMENTS')!= '') {{ trans(Session::get('lang_file').'.COMMENTS') }} @else {{ trans($OUR_LANGUAGE.'.COMMENTS') }} @endif</strong></a>                        
                    </div>
                  </div>
              
            </div>
            <div class="content-text clearfix">
              <p>
                    @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                        @php    $blog_desc = 'blog_desc'; @endphp
                    @else @php  $blog_desc = 'blog_desc_'.Session::get('lang_code'); @endphp  
                    @endif
                    {{ $fetchblog_list->$blog_desc }}
                </p>
            </div>
            
          </div>
          
          <!-- Comment -->  
        @foreach($get_blog_comment_check as $check_comment_by_admin) @endforeach 
        @if($check_comment_by_admin->bs_allowcommt == 1)        
        <div class="single-box comment-box">
        <div class="best-title text-left">
            <h2>@if (Lang::has(Session::get('lang_file').'.LEAVE_A_REPLY')!= '') {{ trans(Session::get('lang_file').'.LEAVE_A_REPLY') }} @else {{ trans($OUR_LANGUAGE.'.LEAVE_A_REPLY') }} @endif</h2>
        </div>
            {!! Form::open(array('url'=>'blog_comment_submit','class'=>'form-horizontal')) !!}
            <div class="coment-form">              
              <div class="row">
                <div class="col-sm-6">
                    <label for="name">@if (Lang::has(Session::get('lang_file').'.NAME')!= '') {{  trans(Session::get('lang_file').'.NAME') }} @else {{ trans($OUR_LANGUAGE.'.NAME') }} @endif</label>
                    <input type="hidden" name="check_cmt_approval" value="<?php echo $check_comment_by_admin->bs_radminapproval; ?>">
                    <input placeholder="" class="form-control " name="cmt_page" type="hidden" value="1">
                    <input placeholder="" class="form-control " name="cmt_id" type="hidden" value="<?php echo $fetchblog_list->blog_id; ?>">
                    <input placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_NAME') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_NAME') }} @endif" class="form-control " type="text" name="name" value="{!! Input::old('name') !!}" required>
                </div>
                <div class="col-sm-6">
                    <label for="email">@if (Lang::has(Session::get('lang_file').'.EMAIL')!= '') {{  trans(Session::get('lang_file').'.EMAIL') }} @else {{ trans($OUR_LANGUAGE.'.EMAIL') }} @endif</label>
                    <input placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_ID') }} @endif" class="form-control " type="email" name="email" value="{!! Input::old('email') !!}" required>
                </div>
                <div class="col-sm-12">
                    <label for="website">@if (Lang::has(Session::get('lang_file').'.WEBSITE')!= '') {{  trans(Session::get('lang_file').'.WEBSITE') }} @else {{ trans($OUR_LANGUAGE.'.WEBSITE') }} @endif</label>
                    <input placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_LINK')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_LINK') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_LINK') }} @endif" class="form-control " type="text" name="website" value="{!! Input::old('website') !!}" >
                </div>
                <div class="col-sm-12">
                    <label for="message">@if (Lang::has(Session::get('lang_file').'.MESSAGE')!= '') {{ trans(Session::get('lang_file').'.MESSAGE') }} @else {{ trans($OUR_LANGUAGE.'.MESSAGE') }} @endif</label>
                    <textarea  rows="8" class="form-control" name="message" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MESSAGE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MESSAGE') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MESSAGE') }} @endif" required>{!! Input::old('message') !!}</textarea>
                </div>
              </div>
              <button class="button"><span>@if (Lang::has(Session::get('lang_file').'.COMMENTS')!= '') {{  trans(Session::get('lang_file').'.COMMENTS') }} @else {{ trans($OUR_LANGUAGE.'.COMMENTS') }} @endif</span></button>
            </div>
            {!! Form::close() !!}
          </div>
          <!-- ./Comment --> 
        @endif 
        @else
            <h4 style="color:#F00;">@if (Lang::has(Session::get('lang_file').'.NO_BLOGS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_BLOGS_AVAILABLE') }} @else {{ trans($OUR_LANGUAGE.'.NO_BLOGS_AVAILABLE') }} @endif...</h4>
        @endif
        </div>

        <!-- right colunm -->
        <!--<aside class="sidebar col-xs-12 col-sm-3">--> 
          <!-- Blog category -->
           <!--<div class="block blog-module">
            <div class="sidebar-bar-title">
              <h3>Blog Categories</h3>
            </div>
            <div class="block_content">-->  
              <!-- layered -->
              <!--<div class="layered layered-category">
                <div class="layered-content">
                  <ul class="tree-menu">
                    <li><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Images</a></li>
                    <li><i class="fa fa-angle-right"></i>&nbsp; <a href="#">Audio</a></li>
                    <li><i class="fa fa-angle-right"></i>&nbsp; <a href="#">Photos</a></li>
                    <li><i class="fa fa-angle-right"></i>&nbsp; <a href="#">Diet &amp; Fitness</a></li>
                    <li><i class="fa fa-angle-right"></i>&nbsp; <a href="#">Slider</a></li>
                  </ul>
                </div>
              </div>--> 
              <!-- ./layered --> 
             <!--</div>
          </div> --> 
          <!-- ./blog category  --> 
          <!-- Popular Posts -->
          <!--<div class="block blog-module">
            <div class="sidebar-bar-title">
              <h3>Popular Posts</h3>
            </div>
            <div class="block_content">--> 
              <!-- layered -->
               <!--<div class="layered">
                <div class="layered-content">
                  <ul class="blog-list-sidebar">
                    <li>
                      <div class="post-thumb"> <a href="#"><img src="images/blog-img1.jpg" alt="Blog"></a> </div>
                      <div class="post-info">
                        <h5 class="entry_title"><a href="#">Lorem ipsum dolor</a></h5>
                        <div class="post-meta"> <span class="date"><i class="pe-7s-date"></i> 2014-08-05</span> <span class="comment-count"> <i class="pe-7s-comment"></i> 3 </span> </div>
                      </div>
                    </li>
                    <li>
                      <div class="post-thumb"> <a href="#"><img src="images/blog-img2.jpg" alt="Blog"></a> </div>
                      <div class="post-info">
                        <h5 class="entry_title"><a href="#">Lorem ipsum dolor</a></h5>
                        <div class="post-meta"> <span class="date"><i class="pe-7s-date"></i> 2014-08-05</span> <span class="comment-count"> <i class="pe-7s-comment"></i> 3 </span> </div>
                      </div>
                    </li>
                    <li>
                      <div class="post-thumb"> <a href="#"><img src="images/blog-img3.jpg" alt="Blog"></a> </div>
                      <div class="post-info">
                        <h5 class="entry_title"><a href="#">Lorem ipsum dolor</a></h5>
                        <div class="post-meta"> <span class="date"><i class="pe-7s-date"></i> 2014-08-05</span> <span class="comment-count"> <i class="pe-7s-comment"></i> 3 </span> </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>--> 
              <!-- ./layered --> 
            <!--</div>
          </div>-->
          <!-- ./Popular Posts -->         
          
          
          
        </aside>
        <!-- ./right colunm --> 
      </div>
    </div>
  </section>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->

	{!! $footer !!}

     <script type="text/javascript">
$(document).ready(function(){

$("#searchbox").keyup(function(e) 
{

var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;
if(searchbox=='')
{
$("#display").html("").hide();	
}
else
{
	var passData = 'searchword='+ searchbox;
	 $.ajax( {
			      type: 'GET',
				  data: passData,
				  url: '<?php echo url('autosearch'); ?>',
				  success: function(responseText){  
				  $("#display").html(responseText).show();	
}
});
}
return false;    

});
});
</script>
   <script src="<?php echo url(''); ?>/plug-k/js/bootstrap-typeahead.js" type="text/javascript"></script>
    <script src="<?php echo url(''); ?>/plug-k/demo/js/demo.js" type="text/javascript"></script>
	

 <!-- <script type="text/javascript" src="<?php echo url(''); ?>/themes/js/jquery-1.5.2.min.js"></script> -->
	<script type="text/javascript" src="<?php echo url(''); ?>/themes/js/scriptbreaker-multiple-accordion-1.js"></script> 
    <script language="JavaScript">
    
    $(document).ready(function() {
        $(".topnav").accordion({
            accordion:true,
            speed: 500,
            closedSign: '<span class="icon-chevron-right"></span>',
            openedSign: '<span class="icon-chevron-down"></span>'
        });
    });
    
    </script>
</body>
</html>