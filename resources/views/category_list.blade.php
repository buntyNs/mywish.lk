{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

<!-- Header End====================================================================== -->



<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <?php /* <div class="category-description std">

            <div class="slider-items-products">

              <div id="category-desc-slider" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img1.jpg"></a>

                    <div class="inner-info">

                      <div class="cat-img-title"> <span>Best Product 2017</span>

                        <h2 class="cat-heading">Best Selling Brand</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

                        <a class="info" href="#">Shop Now</a> </div>

                    </div>

                  </div>

                  <!-- End Item --> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img2.jpg"></a> </div>

                  

                  <!-- End Item --> 

                  

                </div>

              </div>

            </div>

          </div> */ ?>

		   @if(count($product_details) != 0) 

          <div class="shop-inner">

            <div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.SEARCH_FOR_PRODUCTS') }} @endif</h2>

            </div>

            <div class="product-grid-area">

            	<div class="toolbar">              

              <div class="sorter">

                <div class="short-by">

        				@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

        					@php  $title = 'pro_title'; @endphp

        				@else 

        					@php  $title = 'pro_title_'.Session::get('lang_code'); @endphp 

        				@endif

                  <label>@if (Lang::has(Session::get('lang_file').'.SORT_BY')!= '') {{  trans(Session::get('lang_file').'.SORT_BY') }} @else {{ trans($OUR_LANGUAGE.'.SORT_BY') }} @endif:</label>

                  <select name="filtertypes" >

                    <option value="">{{ (Lang::has(Session::get('lang_file').'.SORT_BY')!= '') ? trans(Session::get('lang_file').'.SORT_BY') : trans($OUR_LANGUAGE.'.SORT_BY') }}</option>

                    <option value="1" >@if (Lang::has(Session::get('lang_file').'.PRICE_LOW')!= '') {{  trans(Session::get('lang_file').'.PRICE_LOW') }} @else {{ trans($OUR_LANGUAGE.'.PRICE_LOW') }} @endif - @if (Lang::has(Session::get('lang_file').'.HIGH')!= '') {{  trans(Session::get('lang_file').'.HIGH') }} @else {{ trans($OUR_LANGUAGE.'.HIGH') }} @endif</option>

                    <option value="2" >@if (Lang::has(Session::get('lang_file').'.PRICE_HIGH')!= '') {{  trans(Session::get('lang_file').'.PRICE_HIGH') }} @else {{ trans($OUR_LANGUAGE.'.PRICE_HIGH') }} @endif -@if (Lang::has(Session::get('lang_file').'.LOW')!= '') {{  trans(Session::get('lang_file').'.LOW') }} @else {{ trans($OUR_LANGUAGE.'.LOW') }} @endif</option>

                    <option  value="3" >@if (Lang::has(Session::get('lang_file').'.TITLE')!= '') {{  trans(Session::get('lang_file').'.TITLE') }} @else {{ trans($OUR_LANGUAGE.'.TITLE') }} @endif @if (Lang::has(Session::get('lang_file').'.A')!= '') {{ trans(Session::get('lang_file').'.A') }}  @else {{ trans($OUR_LANGUAGE.'.A') }} @endif-@if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif</option>

                    <option value="4">@if (Lang::has(Session::get('lang_file').'.TITLE')!= '') {{  trans(Session::get('lang_file').'.TITLE') }} @else {{ trans($OUR_LANGUAGE.'.TITLE') }} @endif @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif-@if (Lang::has(Session::get('lang_file').'.A')!= '') {{  trans(Session::get('lang_file').'.A') }} @else {{ trans($OUR_LANGUAGE.'.A') }} @endif</option>

                    <option value="5">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{  trans(Session::get('lang_file').'.DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif @if (Lang::has(Session::get('lang_file').'.A')!= '') {{ trans(Session::get('lang_file').'.A') }}  @else {{ trans($OUR_LANGUAGE.'.A') }} @endif- @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif</option>

                    <option  value="6">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{ trans(Session::get('lang_file').'.DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{  trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif- @if (Lang::has(Session::get('lang_file').'.A')!= '') {{  trans(Session::get('lang_file').'.A') }} @endif</option>

                  </select>

                </div>

                <div class="short-by page">

                  <label>@if (Lang::has(Session::get('lang_file').'.SHOW')!= '') {{  trans(Session::get('lang_file').'.SHOW') }} @else {{ trans($OUR_LANGUAGE.'.SHOW') }} @endif @if (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') {{  trans(Session::get('lang_file').'.PER_PAGE') }} @else {{ trans($OUR_LANGUAGE.'.PER_PAGE') }} @endif:</label>

                  <select name="perpagenumber"  >

          					<option value="9">9 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="18">18 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="36">36 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="all">@if (Lang::has(Session::get('lang_file').'.ALL')!= '') {{  trans(Session::get('lang_file').'.ALL') }} @else {{ trans($OUR_LANGUAGE.'.ALL') }} @endif </option>

                  </select>

                </div>

                   

              </div>

            </div>

              <ul class="products-grid clearfix">

			  @foreach($product_details as $product_det)

				 @php $product_image = explode('/**/',$product_det->pro_Img);

				 $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

				 $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); @endphp

				 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				 @php $pro_title = 'pro_title'; @endphp

				 @else @php  $pro_title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

				 @php	$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));

				 $res = base64_encode($product_det->pro_id);

				 $prod_image    = $product_image[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/product/".$prod_image; @endphp

				 @if(file_exists($img_data) && $prod_image !='')   

				 @php $prod_path = url('').'/public/assets/product/' .$prod_image;  @endphp                

				 @else  

				 @if(isset($DynamicNoImage['productImg']))

				 @php $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp

				 @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

				 @php $prod_path = $dyanamicNoImg_path; @endphp @endif

				 @endif

				 @endif

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					   @if($product_det->pro_discount_percentage!='' && round($product_det->pro_discount_percentage)!=0)

                        <div class="icon-new-label new-left">{{ substr($product_det->pro_discount_percentage,0,2) }}%</div>@endif

                        <div class="pr-img-area">

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')

						<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> <img class="first-img" alt="{{substr($product_det->$pro_title,0,25) }} {{  strlen($product_det->$pro_title)>25?'..':'' }}" src="{{ $prod_path }}">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   @endif



					   @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">

                          <figure> <img class="first-img" alt="{{substr($product_det->$pro_title,0,25) }}{{  strlen($product_det->$pro_title)>25?'..':'' }}" src="{{ $prod_path }}">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   @endif

					   

					   @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" >

                          <figure> <img class="first-img" alt="{{substr($product_det->$pro_title,0,25) }}	{{  strlen($product_det->$pro_title)>25?'..':'' }}" src="{{ $prod_path }}">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   @endif

					   

					   </div>

                        {{-- <div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>

                            <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>

                            <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>

                          </div>

                        </div> --}}

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">{{substr($product_det->$pro_title,0,25) }}

						  {{strlen($product_det->$pro_title)>25?'..':'' }}</a> </div>

                          <div class="item-content">

                           

                               @php           

                $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();

                $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();

                $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();

                $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();

                $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();

                

                

                $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

                $multiple_countone = $one_count *1;

                $multiple_counttwo = $two_count *2;

                $multiple_countthree = $three_count *3;

                $multiple_countfour = $four_count *4;

                $multiple_countfive = $five_count *5;

                $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp



                         <div class="rating">

             @if($product_count)

             @php   $product_divide_count = $product_total_count / $product_count; @endphp

             @if($product_divide_count <= '1') 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '1') 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '2') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

             @elseif($product_divide_count >= '3') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '4') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '5') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

             @else

              

            @endif

          @else

             <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

          @endif  

           </div>



                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>

                            </div>

							

                            <div class="pro-action">

							@if($product_det->pro_no_of_purchase < $product_det->pro_qty) 

								

								@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							    </span></button></a>

							    @endif

								

							   @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							    </span></button></a>

							   @endif

							   

							   @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							    </span></button></a>

								@endif

							  @endif

								

								@if($product_det->pro_no_of_purchase >= $product_det->pro_qty) 

								<button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span></button> 

								@endif								

							  

							  

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

		  

			@endforeach

			    </ul>

			</div> 	

	    </div>

		  @else

			<div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.SEARCH_FOR_PRODUCTS') }} @endif</h2>

              </div>

			<p><center><h4>
{{ (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }}</h4></center> </p>

			

		   @endif

            

            

		 

		

		

			<!--For Deals-->

		 @if(count($deals_details) > 0) 

		   <div class="shop-inner">

            <div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.SEARCH_FOR_DEALS')!= '') {{  trans(Session::get('lang_file').'.SEARCH_FOR_DEALS') }} @else {{ trans($OUR_LANGUAGE.'.SEARCH_FOR_DEALS') }} @endif</h2>

            </div>

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			 @foreach($deals_details as $fetch_deals)  

			 @php  $deal_img = explode('/**/', $fetch_deals->deal_image);

			 $mcat = strtolower(str_replace(' ','-',$fetch_deals->mc_name));

			 $smcat = strtolower(str_replace(' ','-',$fetch_deals->smc_name));

			 $sbcat = strtolower(str_replace(' ','-',$fetch_deals->sb_name));

			 $ssbcat = strtolower(str_replace(' ','-',$fetch_deals->ssb_name)); 

			 $res = base64_encode($fetch_deals->deal_id);

			 $product_image     = $deal_img[0];

			 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

			 $img_data   = "public/assets/deals/".$product_image; @endphp

			 @if(file_exists($img_data) && $product_image !='')   

			 @php  $prod_path = url('').'/public/assets/deals/' .$product_image;                  @endphp

			 @else  

			 @if(isset($DynamicNoImage['dealImg']))

			 @php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp

			 @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

			 @php    $prod_path = $dyanamicNoImg_path; @endphp @endif

			 @endif

			 @endif  

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					   @if($fetch_deals->deal_discount_percentage!='' && round($fetch_deals->deal_discount_percentage)!=0) 

                        <div class="icon-new-label new-left">{{ substr($fetch_deals->deal_discount_percentage,0,2) }}%</div>@endif

                        <div class="pr-img-area">

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  

						<a href="{{ url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}">

                          <figure> <img class="first-img" src="{{ $prod_path }}" alt="" class="product__image">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   @endif



					  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						<a href="{{ url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">

                          <figure> <img class="first-img" src="{{ $prod_path }}" alt="">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   @endif

					   

					  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						<a href="{{ url('dealview/'.$mcat.'/'.$smcat.'/'.$res) }}" >

                          <figure> <img class="first-img" alt="" src="{{ $prod_path }}">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   @endif

					   

					   </div>

                        {{-- <div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>

                            <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>

                            <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>

                          </div>

                        </div> --}}

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

						  @php $deal_title = 'deal_title'; @endphp

						  @else @php  $deal_title = 'deal_title_'.Session::get('lang_code'); @endphp @endif

						  {{ substr($fetch_deals->$deal_title,0,50) }}...</a> </div>

                          <div class="item-content">

                            @php

              

              $one_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 1)->count();

              $two_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 2)->count();

              $three_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 3)->count();

              $four_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 4)->count();

              $five_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 5)->count();

              

              

              $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

              $multiple_countone = $one_count *1;

              $multiple_counttwo = $two_count *2;

              $multiple_countthree = $three_count *3;

              $multiple_countfour = $four_count *4;

              $multiple_countfive = $five_count *5;

              $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

          

                          <div class="rating">

            @if($product_count)

               @php   $product_divide_count = $product_total_count / $product_count; @endphp

               @if($product_divide_count <= '1') 

               

               <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

               @elseif($product_divide_count >= '1') 

               

               <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

               @elseif($product_divide_count >= '2') 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

               @elseif($product_divide_count >= '3') 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

               @elseif($product_divide_count >= '4') 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>

               @elseif($product_divide_count >= '5') 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>

               @else

                

              @endif

            @else

               <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

              @endif  

            </div>

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $fetch_deals->deal_discount_price }}</span> </span> </div>

                            </div>

							

                            <div class="pro-action">

							 @if($fetch_deals->deal_no_of_purchase < $fetch_deals->deal_max_limit)  

								

								@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

								<a href="{{ url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							    </span></button></a>

							    @endif

								

							   @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

								<a href="{{ url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							    </span></button></a>

							   @endif

							   

							    @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

								<a href="{{ url('dealview/'.$mcat.'/'.$smcat.'/'.$res) }}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							    </span></button></a>

								@endif

							  @endif

								

								@if($fetch_deals->deal_no_of_purchase >= $fetch_deals->deal_max_limit) 

								<button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span></button> 

								@endif								

							 

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

		

			@endforeach

			  </ul>

			 </div>

			</div>

			@elseif(count($deals_details) == 0)

			

        <div class="page-title">

			 <h2>@if (Lang::has(Session::get('lang_file').'.SEARCH_FOR_DEALS')!= '') {{  trans(Session::get('lang_file').'.SEARCH_FOR_DEALS') }} @else {{ trans($OUR_LANGUAGE.'.SEARCH_FOR_DEALS') }} @endif</h2>

			 </div>

			<p><center><h4> {{(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} </h4></center></p>

			 @endif

             

            </div>

			

			

            <!--<div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

			

	

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

			<div class="block-content">

              <p class="block-subtitle">{{ (Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES') }}</p>

               @if(count($main_category)!=0)

				<div id="divMenu">

					<ul>

					  @foreach($main_category as $fetch_main_cat)  

					   @php	$check_main_product_exists = DB::table('nm_product')->where('pro_mc_id', '=', $fetch_main_cat->mc_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); @endphp

					   @if($check_main_product_exists)

					   @php	$pass_cat_id1 = "1,".$fetch_main_cat->mc_id;  @endphp

					   @if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0) 						

						<li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1) }}">

							 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

							  @php $mc_name = 'mc_name'; @endphp

							  @else @php  $mc_name = 'mc_name_code'; @endphp @endif

							  {{ $fetch_main_cat->$mc_name }}</a> 

						   <ul>

							@foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)   

							 @php 

               $check_sub_product_exists = DB::table('nm_product')->where('pro_smc_id', '=', $fetch_sub_main_cat->smc_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); 

               @endphp

							 @if($check_sub_product_exists)

							 @php	$pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp

								<li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2) }}"> 

								 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

									@php	$smc_name = 'smc_name'; @endphp

									@else @php  $smc_name = 'smc_name_code'; @endphp @endif

									{{ $fetch_sub_main_cat->$smc_name }} </a>

										

									@if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0)  

									 <ul>

									   @foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)  

									   @php	

                     $check_sec_main_product_exists = DB::table('nm_product')->where('pro_sb_id', '=', $fetch_sub_cat->sb_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); @endphp

									   @if($check_sec_main_product_exists)

									   @php	$pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp

									   @if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0)

										<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"> 

										@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

										  @php $sb_name = 'sb_name'; @endphp

										  @else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

										  {{ $fetch_sub_cat->$sb_name }}</a> 

											<ul>	

												@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  

												 @php	$check_sec_sub_main_product_exists = DB::table('nm_product')->where('pro_ssb_id', '=', $fetch_secsub_cat->ssb_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); @endphp

												 @if($check_sec_sub_main_product_exists)

												 @php	$pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp  

												<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)}}">

												@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

												@php $ssb_name = 'ssb_name'; @endphp

												@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

												{{ $fetch_secsub_cat->$ssb_name }}</a>

												</li>

												 @endif

													@endforeach 

											</ul>

											 @endif

										</li>

										@endif @endforeach

									</ul>

									 @endif

								</li>

								 @endif @endforeach

						   </ul>

						   @endif

						</li>

						@endif  @endforeach

					</ul>

					@else 

						<ul> <li>@if (Lang::has(Session::get('lang_file').'.NO_CATEGORY_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_CATEGORY_FOUND') }} @else {{ trans($OUR_LANGUAGE.'.NO_CATEGORY_FOUND') }}! @endif </li></ul> @endif

				</div>

            </div>

		<div class="clearfix"></div>

	 <br/>

         

        @if(count($most_visited_product)>0) 

          <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3>@if (Lang::has(Session::get('lang_file').'.MOST_VISITED_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.MOST_VISITED_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.MOST_VISITED_PRODUCT') }} @endif</h3>

            </div>

            <div class="block-content">

              <ul>

			  @foreach($most_visited_product as $fetch_most_visit_pro) 

			  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				@php $pro_title = 'pro_title'; @endphp

				@else @php  $pro_title = 'pro_title_langCode'; @endphp @endif

				@php	$mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				$smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				$sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				$ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name));

				$res = base64_encode($fetch_most_visit_pro->pro_id);

				$mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

				$mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

				$mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);

				$product_image    = $mostproduct_img[0];

				$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				$img_data   = "public/assets/product/".$product_image; @endphp

				@if(file_exists($img_data) && $product_image !='')   

				@php  $prod_path = url('').'/public/assets/product/' .$product_image;  @endphp                 

				@else  

				@if(isset($DynamicNoImage['productImg']))

				@php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp

				@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

				@php   $prod_path = $dyanamicNoImg_path; @endphp @endif

				@endif

				@endif  

                <li class="item">

                  <div class="products-block-left">

				  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

				  <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}" class="product-image"><img src="{{ $prod_path }}" alt="{{substr($fetch_most_visit_pro->$pro_title,0,25) }}{{  strlen($fetch_most_visit_pro->$pro_title)>25?'..':'' }}"> </a>

				  @endif

				  

				  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

				  <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}" class="product-image"><img src="{{ $prod_path }}" alt="{{substr($fetch_most_visit_pro->$pro_title,0,25) }}{{  strlen($fetch_most_visit_pro->$pro_title)>25?'..':'' }}"></a>

				  @endif

				  

				  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

				  <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}" class="product-image"><img src="{{ $prod_path }}" alt="{{substr($fetch_most_visit_pro->$pro_title,0,25) }} {{  strlen($fetch_most_visit_pro->$pro_title)>25?'..':'' }}"></a>

				  @endif

				  

				  </div>

                  <div class="products-block-right">

                    <p class="product-name"> <a>{{substr($fetch_most_visit_pro->$pro_title,0,25) }}

                     {{  strlen($fetch_most_visit_pro->$pro_title)>25?'..':'' }} </a> </p>

                    <span class="price">{{ Helper::cur_sym() }} {{ $fetch_most_visit_pro->pro_disprice }}</span>

                   

				   

				  @php					  

				  $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 5)->count();

				  

				  

				  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

				  $multiple_countone = $one_count *1;

				  $multiple_counttwo = $two_count *2;

				  $multiple_countthree = $three_count *3;

				  $multiple_countfour = $four_count *4;

				  $multiple_countfive = $five_count *5;

				  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

				   

				   <div class="rating">

				@if($product_count)

					 @php   $product_divide_count = $product_total_count / $product_count; @endphp

					 @if($product_divide_count <= '1') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '1') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '2') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

					 @elseif($product_divide_count >= '3') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '4') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '5') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

					 @else

						

					@endif

				@else

				     <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

				@endif	

			    </div>

				   

				   

				   

					<br>

					

					@if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty)

						

					@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

					<a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')  

					<a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

					<a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					@endif

					

					@if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty)  

					<a class="link-all">@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{  trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</a>

					@endif

					

					

                  </div>

                </li>

			  @endforeach

              </ul>

            </div>

          </div>  

		  @endif

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   @include('service_section')

{!! $footer !!}



