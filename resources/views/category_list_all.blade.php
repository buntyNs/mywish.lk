{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

<!-- Header End====================================================================== -->

<div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li> 

            <li><strong><?php if (Lang::has(Session::get('lang_file').'.ALL_CATEGORIES')!= '') { echo  trans(Session::get('lang_file').'.ALL_CATEGORIES');}  else { echo trans($OUR_LANGUAGE.'.ALL_CATEGORIES');} ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

<link rel="stylesheet" href="public/assets/css/new_css.css" />

<section class="container">    

    <div class="sitemap-page"><div class="page-title">

      <h2><?php if (Lang::has(Session::get('lang_file').'.ALL_CATEGORIES')!= '') { echo  trans(Session::get('lang_file').'.ALL_CATEGORIES');}  else { echo trans($OUR_LANGUAGE.'.ALL_CATEGORIES');} ?></h2>

    </div>

    <div class="col-xs-6 col-sm-3 col-md-4">     

     <ul class="simple-list arrow-list bold-list">

		<?php 

		$main = 1;

		foreach($main_category as $fetch_main_cat)

		{

			$pass_cat_id1 = "1,".$fetch_main_cat->mc_id; 

			if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0)

			{

			?>

				<ul>

					<a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>">

					<?php 

					if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') 

					{ 

						$mc_name = 'mc_name';

					}

					else

					{  

						$mc_name = 'mc_name_code'; 

					}

						echo $fetch_main_cat->$mc_name; ?>

					</a>

				</ul>

				<ul>

				<?php 

					foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)

					{

						$pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id;

					?>

					<li><a  href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"><?php 

						if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') 

						{ 

							$smc_name = 'smc_name';

						}

						else

						{  

							$smc_name = 'smc_name_code'; 

						}

							echo $fetch_sub_main_cat->$smc_name ; ?> </a>

					</li>

						<?php

							if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0)

							{ ?>

					

						<?php 

							foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sec_sub_main_cat)

							{

								$pass_cat_id3 = "3,".$fetch_sec_sub_main_cat->sb_id;

												

							?>

								<li class="fa fa-angle-double-right" style="margin-left: 15px;"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"><?php 

									if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') 

									{ 

										$sb_name = 'sb_name';

									}

									else

									{  

										$sb_name = 'sb_name_langCode'; 

									}

									echo $fetch_sec_sub_main_cat->$sb_name ; ?> </a>

								</li><br>

							<?php

							}

							?>

					

					<?php

					}

					}

					?>

				</ul>

				<?php 

				}

				if($main=='4'){

				echo '</ul></div><div class="col-xs-6 col-sm-3 col-md-4"><ul class="simple-list arrow-list bold-list">';

				$main ='';

				}

				$main++; 

				}

				?>

	</ul>

	</div>

     <!-- <div class="col-xs-12 col-sm-4 col-md-4"> <img class="img-responsive animate scale" src="images/large-icon-sitemap.png" alt="HTML template"> </div> -->

    </div>

  </section>

   @include('service_section')



<!-- MainBody End ============================= -->

<!-- Footer ================================================================== -->

{!! $footer !!}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">

	$.ajaxSetup({

		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

	});

</script>  

</body>

</html>