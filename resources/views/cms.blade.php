{!! $navbar !!}


<!-- Navbar ================================================== -->
{!! $header !!}
<!-- Header End====================================================================== -->
<div class="breadcrumbs">
    <div class="container">	
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li>
           
          </ul>
        </div>
      </div>
    </div>
  </div>
<div id="mainBody faq_main">
<div class="container cms-page">
@if($cms_result) 
 @foreach($cms_result as $cms) 
	@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') 
		@php $cp_title = 'cp_title'; 
		$cp_description = 'cp_description'; @endphp
	 @else  
		@php $cp_title = 'cp_title_'.Session::get('lang_code'); 
		$cp_description = 'cp_description_'.Session::get('lang_code');  @endphp
	@endif	
   @php $cms_desc = $cms->$cp_description;   @endphp

   <br>
   <div class="page-title">
          <h2><?php echo  $cms->$cp_title; ?></h2>
        </div>
  
  <div id="legalNotice">
	
  {!! $cms_desc !!}
  </div>
 @endforeach
    @else 
 <h1 style="color:#ff8400;"></h1>
 <legend></legend>
 <div id="legalNotice">
	 @if (Lang::has(Session::get('lang_file').'.NO_DATA_FOUND')!= '') 
	  {{  trans(Session::get('lang_file').'.CATEGORIES') }}  
	  @else {{ trans($OUR_LANGUAGE.'.NO_DATA_FOUND') }} !
	 @endif
	</div>	
 </div>
@endif 
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->

	{!! $footer  !!}

	

</body>
</html>