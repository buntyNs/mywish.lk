<!DOCTYPE html>

<html lang="en">

{!! $navbar!!}

{!! $header !!}



<body class="compare_page">

<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

  <!-- Header -->

  

  <!-- end header -->

  

  

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="index-2.html">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li>

            <li><strong>{{ (Lang::has(Session::get('lang_file').'.COMPARE')!= '') ?  trans(Session::get('lang_file').'.COMPARE'): trans($OUR_LANGUAGE.'.COMPARE') }}</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="col-main">

        <div class="compare-list">

          <div class="page-title">

            <h2>@if (Lang::has(Session::get('lang_file').'.COMPARE_PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.COMPARE_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.COMPARE_PRODUCTS') }} @endif</h2>

          </div>

          <div class="table-responsive">

            

              <table class="table table-bordered table-compare"> 

             

             

             @if(count($product_details)!=0)

              

              <tr>

                <td class="compare-label">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_IMAGE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_IMAGE') : trans($OUR_LANGUAGE.'.PRODUCT_IMAGE') }}</td> 

                

               @foreach($product_details as $pro_det)



                <td class="compare-pro">

                  {{-- product image --}}

                  @php      $product_image = explode('/**/',$pro_det->pro_Img); @endphp

                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                     @php $pro_title = 'pro_title'; @endphp

                     @else @php  $pro_title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

                    

                  </div>

                  @php   $product_image   = $product_image[0];

                  $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                  $img_data   = "public/assets/product/".$product_image; @endphp

                  @if(file_exists($img_data) && $product_image!='' )  

                  @php  $prod_path = url('public/assets/product/').'/'.$product_image;  @endphp       

                  @else 

                  @if(isset($DynamicNoImage['productImg'])) 

                  @php  $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

                  @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)) 

                  @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

                  @endif

                  @endif

                 

                  <a href="#"><img src="{{ $prod_path }}" alt="" width="260"></a></td>

                  @endforeach
                {{-- <td class="compare-pro"><a href="#"><img src="images/products/product-8.jpg" alt="Product" width="260"></a></td> --}}

              </tr>  

              <tr>

                <td class="compare-label" >{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAMES')!= '') ? trans(Session::get('lang_file').'.PRODUCT_NAMES') : trans($OUR_LANGUAGE.'.PRODUCT_NAMES') }}</td>

                

               @foreach($product_details as $pro_det)

                <td>

                  {{-- Product title --}}

                  @php      $product_image = explode('/**/',$pro_det->pro_Img); @endphp

                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                     @php $pro_title = 'pro_title'; @endphp

                     @else @php  $pro_title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

                  <a href="#">{{ $pro_det->$pro_title }} </a></td>

                  @endforeach

                  

               {{--  <td><a href="#">Donec Ac Tempus</a></td> --}}

                

              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.RATING')!= '') {{  trans(Session::get('lang_file').'.RATING') }} @else {{ trans($OUR_LANGUAGE.'.RATING') }} @endif</td>

                {{-- Rating--}}

               

               @foreach($product_details as $pro_det)

                @php    $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

                     $multiple_countone = $one_count *1;

                     $multiple_counttwo = $two_count *2;

                     $multiple_countthree = $three_count *3;

                     $multiple_countfour = $four_count *4;

                     $multiple_countfive = $five_count *5;

                     $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

                     @if($product_count)

                     @php $product_divide_count = $product_total_count / $product_count; @endphp

                      @if($product_divide_count <= '1')

                <td><div class="rating"> <img src="{{ url('images/stars-1.png') }}" alt="" width=""></div></td>

               

                 @elseif($product_divide_count >= '1')

                 <td><div class="rating"> <img src="{{ url('images/stars-1.png') }}" alt="" width=""></div></td>

                

                @elseif($product_divide_count >= '2')

                 <td><div class="rating"> <img src="{{ url('images/stars-2.png') }}" alt="" width=""></div></td>

                

                 @elseif($product_divide_count >= '3')

                 <td><div class="rating"> <img src="{{ url('images/stars-3.png') }}" alt="" width=""></div></td>

               

                 @elseif($product_divide_count >= '4')

                 <td><div class="rating"> <img src="{{ url('images/stars-4.png') }}" alt="" width=""></div></td>

                

                @elseif($product_divide_count >= '5')

                <td><div class="rating"> <img src="{{ url('images/stars-5.png') }}" alt="" width=""></div></td>

               

                 @else

                 <td><div class="rating"> @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT') }} @endif</div></td>

                @endif

                @elseif($product_count)

                     @php $product_divide_count = $product_total_count / $product_count; @endphp

                     @else  

                     <td> @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT') }} @endif

                     @endif </td>

                @endforeach

               

               

              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.DISCOUNT')!= '') {{ trans(Session::get('lang_file').'.DISCOUNT') }} @else {{ trans($OUR_LANGUAGE.'.DISCOUNT') }} @endif</td>

               

               @foreach($product_details as $pro_det)



                <td class="price">{{ $pro_det->pro_disprice }}</td>

                @endforeach

              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.DESC')!= '') {{  trans(Session::get('lang_file').'.DESC') }} @else {{ trans($OUR_LANGUAGE.'.DESC') }} @endif</td>              

               @foreach($product_details as $pro_det)
                <td>@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                           @php $pro_desc = 'pro_desc'; @endphp

                           @else @php  $pro_desc = 'pro_desc_'.Session::get('lang_code'); @endphp @endif

                           {!! html_entity_decode($pro_det->$pro_desc) !!}</td>

                @endforeach            

              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.STORE_NAME')!= '') {{  trans(Session::get('lang_file').'.STORE_NAME') }} @else {{ trans($OUR_LANGUAGE.'.STORE_NAME') }} @endif</td>
               @foreach($product_details as $pro_det)

                <td>@foreach($store as $store_det) 

                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                     @php $stor_name = 'stor_name'; @endphp

                     @else @php  $stor_name = 'stor_name_'.Session::get('lang_code'); @endphp @endif

                     {{ $store_det->$stor_name }} 

                     @endforeach</td>

                     @endforeach


              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.SIZES')!= '') {{  trans(Session::get('lang_file').'.SIZES') }} @else {{ trans($OUR_LANGUAGE.'.SIZES') }} @endif  </td>

              

               

                 @if(count($product_color_details) > 0) 

                 @foreach($product_size_details as $product_size_det) 

                  <td>  

                     @foreach($product_size_det as $size_det)

                   {{ $size_det->si_name.',' }}

                   @endforeach

                    </td>

                     @endforeach

                     @endif 

              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.COLOR')!= '') {{  trans(Session::get('lang_file').'.COLOR') }} @else {{ trans($OUR_LANGUAGE.'.COLOR') }} @endif</td>

                @if(count($product_color_details) > 0) 

                     @foreach($product_color_details as $product_color_det) 

                     <td>

                       @foreach($product_color_det as $color_det)

                      {{ $color_det->co_name.',' }}

                      @endforeach

                       </td>

                     @endforeach


                      @endif


              </tr>

              <tr>

                <td class="compare-label" > @if (Lang::has(Session::get('lang_file').'.SPEC')!= '') {{  trans(Session::get('lang_file').'.SPEC') }} @else {{ trans($OUR_LANGUAGE.'.SPEC') }} @endif</td>


               @foreach($product_spec_details as $product_det)  

                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                     @php $sp_name = 'sp_name';

                     $spc_value = 'spc_value'; @endphp

                     @else   

                     @php $sp_name = 'sp_name_'.Session::get('lang_code'); 

                     $spc_value = 'spc_value_'.Session::get('lang_code');  @endphp

                     @endif 

                     <td><ul>

                       @foreach($product_det as $product)

                       <li>

                      {{ $product->$sp_name.' : '.$product->$spc_value }} </li> 

                      @endforeach

                      </ul>

                       </td>

                     @endforeach

              </tr>

              <tr>

                <td class="compare-label" >@if (Lang::has(Session::get('lang_file').'.DELIVERY_WITH_IN')!= '') {{  trans(Session::get('lang_file').'.DELIVERY_WITH_IN') }} @else {{ trans($OUR_LANGUAGE.'.DELIVERY_WITH_IN') }} @endif</td>

               

               @foreach($product_details as $pro_det)



                <td>{{ $pro_det->pro_delivery.'days' }}</td>

                @endforeach

              </tr>

              <tr>

                <td class="compare-label" >{{ (Lang::has(Session::get('lang_file').'.ACTION')!= '') ? trans(Session::get('lang_file').'.ACTION') : trans($OUR_LANGUAGE.'.ACTION') }}</td>

                

               @foreach($product_details as $pro_det)



               <td class="action">

                 {{-- <a href="{{ url('') }}"><button class="add-cart button button-sm"><i class="fa fa-shopping-basket"></i></button>

                  <button class="button button-sm"><i class="fa fa-heart-o"></i></button> --}}

                  @if (in_array($pro_det->pro_id, $_SESSION['compare_product'])) 

                  <button class="button button-sm close_compare" onclick="comparefunc(<?php echo $pro_det->pro_id.','.'0'; ?>);" value="0" name="compare" id="compare"><i class="fa fa-close"></i></button></td>

                   @endif

                  @endforeach

              </tr>

              @else

               @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_INCOMPARE')!= '') {{  trans(Session::get('lang_file').'.NO_PRODUCTS_INCOMPARE') }} @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_INCOMPARE') }}


                @endif 

               @endif

             </table>

          </div>

        </div>

      </div>

    </div>

  </section>

  

  <!-- service section -->

   @include('service_section')

  

  <!-- Footer -->

 

  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

{!! $footer!!}

<!-- JS --> 

<script>

   function comparefunc(pid,value){

    //var value = ('#compare').val();

    //alert(value);

    

    var pid = pid;

         $.ajax( {

              type: 'get',

            data: 'pid='+pid + '&value=' +value,

            url: 'remove_compare_product',

            success: function(responseText){  

             if(responseText)

             {  

              alert(responseText);

              location.reload();

            //$('#compare').html(responseText);            

             }

          }   

        }); 

    

   }

</script>


</html>