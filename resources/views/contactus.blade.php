<!DOCTYPE html>

<html lang="en">

{!! $navbar !!}

{!! $header !!}



<body class="contact_us_page">

<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

  <!-- Header -->

  

  <!-- end header -->

  

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li> 

            <li><strong>{{ (Lang::has(Session::get('lang_file').'.CONTACT_US')!= '') ?  trans(Session::get('lang_file').'.CONTACT_US'): trans($OUR_LANGUAGE.'.CONTACT_US') }}</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="row">

        <section class="col-main col-sm-12">

          <div id="contact" class="page-content page-contact">

            <div class="page-title">

              <h2>{{ (Lang::has(Session::get('lang_file').'.CONTACT')!= '') ?  trans(Session::get('lang_file').'.CONTACT'): trans($OUR_LANGUAGE.'.CONTACT') }}</h2>

            </div>

            <div id="message-box-conact"></div>

            <div class="row">

              <div class="col-xs-12 col-sm-6" id="contact_form_map">

                <h3 class="page-subheading"></h3>

               

                @foreach($get_contact_det as $enquiry_det)

                  @endforeach

                <ul class="store_info">

                  <li><i class="fa fa-envelope"></i>{{ $enquiry_det->es_contactemail }}</li>

                  <li><i class="fa fa-skype"></i>{{ $enquiry_det->es_skype_email_id }}</li>

                  <li><i class="fa fa-phone"></i><span>{{ $enquiry_det->es_phone1 }}, {{ $enquiry_det->es_phone2 }}</span></li>

                  <li><a target="_blank" href="{{ url('') }}"><i class="fa fa-globe"></i><span>{{ url('') }}</span></a></li>

                 

                </ul>

              </div>

              <div class="col-sm-6">

                <h3 class="page-subheading">@if (Lang::has(Session::get('lang_file').'.EMAIL_US')!= '') {{ trans(Session::get('lang_file').'.EMAIL_US') }}  @else {{ trans($OUR_LANGUAGE.'.EMAIL_US') }} @endif</h3>

                <div id="error_name"  style="color:#F00;font-weight:400"  > </div>

            @if (Session::has('success'))

            <div class="alert alert-warning alert-dismissable">{!! Session::get('success') !!}

               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            </div>

            @endif

            {!! Form::open(array('url'=>'enquiry_submit','class'=>'form-horizontal')) !!}

                <div class="contact-form-box">

                  <div class="form-selector">

                    <label>@if (Lang::has(Session::get('lang_file').'.NAME')!= '') {{ trans(Session::get('lang_file').'.NAME') }} @else {{ trans($OUR_LANGUAGE.'.NAME')}} @endif</label>

                    <input type="text" class="form-control input-sm"  name="name" id="inquiry_name" maxlength ="100" required />

                  </div>

                  <div class="form-selector">

                    <label>@if (Lang::has(Session::get('lang_file').'.EMAIL')!= '') {{  trans(Session::get('lang_file').'.EMAIL') }} @else {{ trans($OUR_LANGUAGE.'.EMAIL') }} @endif</label>

                    <input type="email" class="form-control input-sm"  name="email" id="inquiry_email" maxlength="50" required />

                  </div>

                  <div class="form-selector">

                    <label>@if (Lang::has(Session::get('lang_file').'.PHONE')!= '') {{  trans(Session::get('lang_file').'.PHONE') }} @else {{ trans($OUR_LANGUAGE.'.PHONE') }} @endif</label>

                    <input type="text" class="form-control input-sm" name="phone" id="inquiry_phone" maxlength="16" required />

                  </div>

                  <div class="form-selector">

                    <label>@if (Lang::has(Session::get('lang_file').'.MESSAGE')!= '') {{  trans(Session::get('lang_file').'.MESSAGE') }} @else {{ trans($OUR_LANGUAGE.'.MESSAGE') }} @endif</label>

                    <textarea class="form-control input-sm" rows="10" name="message" id="inquiry_desc" required></textarea>

                  </div>

                  <div class="form-selector">

                    <button class="button"><i class="icon-paper-plane icons"></i>&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.SEND_MESSAGE')!= '') {{ trans(Session::get('lang_file').'.SEND_MESSAGE') }}  @else {{ trans($OUR_LANGUAGE.'.SEND_MESSAGE') }} @endif</span></button>

                    </div>

                </div>

                {!!  Form::close() !!}

              </div>

            </div>

          </div>

        </section>

      </div>

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- service section -->

   @include('service_section')

  

  <!-- Footer -->

  {!! $footer !!}

  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<!-- JS --> 





</body>


</html>