<aside class="right sidebar col-sm-3 col-xs-12">

                  <div class="sidebar-account block">

                    <div class="sidebar-bar-title">

                      <h3>@if (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '') {{  trans(Session::get('lang_file').'.MY_ACCOUNT')}}  @else {{ trans($OUR_LANGUAGE.'.MY_ACCOUNT')}} @endif</h3>

                    </div>

                    <div class="block-content">

                      <ul>

                      <ul>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'acc_dashboard') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{url('acc_dashboard') }}" >@if (Lang::has(Session::get('lang_file').'.ACC_DASHBOARD')!= '') {{  trans(Session::get('lang_file').'.ACC_DASHBOARD')}}  @else {{ trans($OUR_LANGUAGE.'.ACC_DASHBOARD')}} @endif</a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'product_paypal') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{url('product_paypal') }}">@if (Lang::has(Session::get('lang_file').'.MY_PRODUCT_PAYPAL')!= '') {{  trans(Session::get('lang_file').'.MY_PRODUCT_PAYPAL')}}  @else {{ trans($OUR_LANGUAGE.'.MY_PRODUCT_PAYPAL')}} @endif</a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'product_payumoney') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('product_payumoney')}}">@if (Lang::has(Session::get('lang_file').'.MY_PRODUCT_PAYUMONEY')!= '') {{  trans(Session::get('lang_file').'.MY_PRODUCT_PAYUMONEY')}}  @else {{ trans($OUR_LANGUAGE.'.MY_PRODUCT_PAYUMONEY')}} @endif</a></li>
                        
                        @if($GENERAL_SETTING->gs_payment_status == 'COD')
                          <!-- check COD payment enable or disable to view COD details -->
                        <li <?php if(Route::getCurrentRoute()->uri() == 'product_cod') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('product_cod')}}">@if (Lang::has(Session::get('lang_file').'.MY_PRODUCT_COD')!= '') {{  trans(Session::get('lang_file').'.MY_PRODUCT_COD')}}  @else {{ trans($OUR_LANGUAGE.'.MY_PRODUCT_COD')}} @endif</a>
                        </li>
                        @else
                        @endif

                        <li <?php if(Route::getCurrentRoute()->uri() == 'deal_paypal') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('deal_paypal') }}">@if (Lang::has(Session::get('lang_file').'.MY_DEAL_PAYPAL')!= '') {{  trans(Session::get('lang_file').'.MY_DEAL_PAYPAL')}}  @else {{ trans($OUR_LANGUAGE.'.MY_DEAL_PAYPAL')}} @endif</a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'deal_payumoney') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('deal_payumoney') }}">@if (Lang::has(Session::get('lang_file').'.MY_DEAL_PAYUMONEY')!= '') {{  trans(Session::get('lang_file').'.MY_DEAL_PAYUMONEY')}}  @else {{ trans($OUR_LANGUAGE.'.MY_DEAL_PAYUMONEY')}} @endif</a></li>

                         @if($GENERAL_SETTING->gs_payment_status == 'COD')
                         <!-- check COD payment enable or disable to view COD details -->
                        <li <?php if(Route::getCurrentRoute()->uri() == 'deal_cod') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('deal_cod')}}">@if (Lang::has(Session::get('lang_file').'.MY_DEAL_COD')!= '') {{  trans(Session::get('lang_file').'.MY_DEAL_COD')}}  @else {{ trans($OUR_LANGUAGE.'.MY_DEAL_COD')}} @endif</a>
                        </li>
                        @else
                        @endif

                        <li <?php if(Route::getCurrentRoute()->uri() == 'user_wishlist') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('user_wishlist')}}">@if (Lang::has(Session::get('lang_file').'.MY_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.MY_WISHLIST')}}  @else {{ trans($OUR_LANGUAGE.'.MY_WISHLIST')}} @endif</a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'shipping_address') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="{{ url('shipping_address')}}"> @if (Lang::has(Session::get('lang_file').'.MY_SHIPPING_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.MY_SHIPPING_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.MY_SHIPPING_ADDRESS')}} @endif</a></li>



                      </ul>

                    </div>

                  </div>



                </aside>