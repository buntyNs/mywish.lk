<!DOCTYPE html>
<html lang="en">

{!!  $navbar !!}
{!!  $header !!}

<body>
	<?php  $j=1; $all_tax_amt=0; $all_shipping_amt=0; ?>
               @foreach($get_deal_Payumoney as $coddetails1_paypal)
               <?php  
                  $item_total=0;  $coupon_amount=0;
                  $subtotal =0;
                  $wallet_amt = $wallet = 0;
                  
                  $tax_amt = (($coddetails1_paypal->order_amt * $coddetails1_paypal->order_tax)/100);
                  $all_tax_amt+=  (($coddetails1_paypal->order_amt * $coddetails1_paypal->order_tax)/100);
                  
                  $shipping_amt = $coddetails1_paypal->order_shipping_amt;
                  $all_shipping_amt+= $coddetails1_paypal->order_shipping_amt; ?>
               @if($coddetails1_paypal->coupon_amount !=0)
               <?php 
                  $coupon_value = $coddetails1_paypal->coupon_amount; ?>
               @else
               <?php
                  $coupon_value = 0; ?>
               @endif
               <?php
                  /* $wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$coddetails1_paypal->transaction_id)->get();
                  
                   if(count($wallet)!=0){
                       $wallet_amt = $wallet[0]->wallet_used;
                   }else{
                       $wallet_amt = 0;
                   }*/
                   
                   $item_total = $coddetails1_paypal->order_amt;
                   
                   $grand_total = (($item_total+$tax_amt+$shipping_amt)-$wallet_amt)-$coupon_value;
                  ?>   
               <div  >
                 <div class="cod-invoice-table">
                     <div class="" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">
                       <div class="container">
                          
                         
                         <div class="Front-inv-topleft">
                              <?php 
                                 //$logo = url().'/public/assets/default_image/Logo.png';
                                 //if(file_exists($SITE_LOGO))
                                   $logo = $SITE_LOGO;
                                 ?> 
                              <img src="{{ $logo }}" alt="" style="">
                           </div>
                           <div class="Front-inv-topright" style=""><strong>@if (Lang::has(Session::get('lang_file').'.TAX_INVOICE')!= '') {{  trans(Session::get('lang_file').'.TAX_INVOICE')}}  @else {{ trans($OUR_LANGUAGE.'.TAX_INVOICE')}} @endif </strong></div>
                        </div></div>
                       
                        <div class="container">
                           <div class="col-lg-12">
                              <div class="Front-inv-address-left" style="text-align:left;">
                                 <h4>@if (Lang::has(Session::get('lang_file').'.PAYUMONEY')!= '') {{  trans(Session::get('lang_file').'.PAYUMONEY')}}  @else {{ trans($OUR_LANGUAGE.'.PAYUMONEY')}} @endif</h4>
                                 <b>@if (Lang::has(Session::get('lang_file').'.AMOUNT_PAID')!= '') {{  trans(Session::get('lang_file').'.AMOUNT_PAID')}}  @else {{ trans($OUR_LANGUAGE.'.AMOUNT_PAID')}} @endif :{{Helper::cur_sym()}} 
                                 <?php
                                    $customerid   = Session::get('customerid');
                                      $product_titles=DB::table('nm_order_payu')
                                      ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
                                      ->leftjoin('nm_color', 'nm_order_payu.order_pro_color', '=', 'nm_color.co_id')
                                      ->leftjoin('nm_size', 'nm_order_payu.order_pro_size', '=', 'nm_size.si_id')
                                      ->where('transaction_id', '=', $coddetails1_paypal->transaction_id)
                                      ->where('nm_order_payu.order_cus_id', '=', $customerid)
                                      ->where('nm_order_payu.order_type','=',2)
                                      ->get(); 
                                      $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
                                 @foreach($product_titles as $prd_tit) 
                                 <?php
                                    $subtotal=$prd_tit->order_amt; 
                                                $tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);
                                             
                                                $total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 
                                                $total_ship_amt+= $prd_tit->order_shipping_amt;
                                                $total_item_amt+=$prd_tit->order_amt;
                                                $coupon_amount+= $prd_tit->coupon_amount;
                                                $prodct_id = $prd_tit->order_pro_id;
                                                
                                          $item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);
                                             
                                          
                                              $ship_amt = $prd_tit->order_shipping_amt;
                                             
                                          
                                              //$item_tax = $codorderdet->cod_tax;
                                             /*if($prd_tit->coupon_amount != 0)
                                             {
                                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                                             }
                                             else
                                             {
                                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
                                             }*/
                                    
                                             $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                                             
                                    
                                             $wallet_amt +=  $prd_tit->wallet_amount;
                                                                                    $wallet     += $prd_tit->wallet_amount;
                                    ?>
                                 @endforeach
                                 {{  $grand_total-$wallet_amt }}</b>
                                 <br>
                                 <span>((@if (Lang::has(Session::get('lang_file').'.INCLUSIVE_OF_ALL_CHARGES')!= '') {{  trans(Session::get('lang_file').'.INCLUSIVE_OF_ALL_CHARGES')}}  @else {{ trans($OUR_LANGUAGE.'.INCLUSIVE_OF_ALL_CHARGES')}} @endif) )</span>
                                 <br>
                                 <span>@if (Lang::has(Session::get('lang_file').'.ORDERID')!= '') {{  trans(Session::get('lang_file').'.ORDERID')}}  @else {{ trans($OUR_LANGUAGE.'.ORDERID')}} @endif : {{ $coddetails1_paypal->transaction_id }}</span><br>
                                 <span>@if (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '') {{  trans(Session::get('lang_file').'.ORDER_DATE')}}  @else {{ trans($OUR_LANGUAGE.'.ORDER_DATE')}} @endif: {{ $coddetails1_paypal->order_date }}</span>
                              </div>
                              <div class="Front-inv-address-right" style="border-left:1px solid #eeeeee;text-align:left;">
                                 <h4>@if (Lang::has(Session::get('lang_file').'.SHIPPING_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.SHIPPING_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.SHIPPING_ADDRESS')}} @endif</h4>
                                 <strong>@if (Lang::has(Session::get('lang_file').'.NAME')!= '') {{  trans(Session::get('lang_file').'.NAME')}}  @else {{ trans($OUR_LANGUAGE.'.NAME')}} @endif : </strong>{{ $coddetails1_paypal->ship_name }}<br>
                                 <strong>@if (Lang::has(Session::get('lang_file').'.PHONE')!= '') {{  trans(Session::get('lang_file').'.PHONE')}}  @else {{ trans($OUR_LANGUAGE.'.PHONE')}} @endif  : </strong>{{ $coddetails1_paypal->ship_phone}} <br>
                                 <strong>@if (Lang::has(Session::get('lang_file').'.EMAIL')!= '') {{  trans(Session::get('lang_file').'.EMAIL')}}  @else {{ trans($OUR_LANGUAGE.'.EMAIL')}} @endif : </strong>{{ $coddetails1_paypal->ship_email}}  <br>
                                 <strong>@if (Lang::has(Session::get('lang_file').'.ADDRESS1')!= '') {{  trans(Session::get('lang_file').'.ADDRESS1')}}  @else {{ trans($OUR_LANGUAGE.'.ADDRESS1')}} @endif : </strong>{{ $coddetails1_paypal->ship_address1 }}<br>
                                 <strong>@if (Lang::has(Session::get('lang_file').'.ADDRESS2')!= '') {{  trans(Session::get('lang_file').'.ADDRESS2')}}  @else {{ trans($OUR_LANGUAGE.'.ADDRESS2')}} @endif : </strong>{{  $coddetails1_paypal->ship_address2 }} <br>
                                 @if($coddetails1_paypal->ship_ci_id)
                                 <strong>@if (Lang::has(Session::get('lang_file').'.CITY')!= '') {{  trans(Session::get('lang_file').'.CITY')}}  @else {{ trans($OUR_LANGUAGE.'.CITY')}} @endif  : </strong>{{ $coddetails1_paypal->ship_ci_id }} <br>
                                 @endif
                                 @if($coddetails1_paypal->ship_state)
                                 <strong>@if (Lang::has(Session::get('lang_file').'.STATE')!= '') {{  trans(Session::get('lang_file').'.STATE')}}  @else {{ trans($OUR_LANGUAGE.'.STATE')}} @endif  : </strong>{{  $coddetails1_paypal->ship_state}}<br>
                                 @endif
                                 @if($coddetails1_paypal->ship_country)
                                 <strong>@if (Lang::has(Session::get('lang_file').'.COUNTRY')!= '') {{  trans(Session::get('lang_file').'.COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.COUNTRY')}} @endif : </strong>{{ $coddetails1_paypal->ship_country }} <br>
                                 @endif
                                 <strong>@if (Lang::has(Session::get('lang_file').'.ZIPCODE')!= '') {{  trans(Session::get('lang_file').'.ZIPCODE')}}  @else {{ trans($OUR_LANGUAGE.'.ZIPCODE')}} @endif : </strong>{{ $coddetails1_paypal->ship_postalcode }} <br>
                              </div>
                           </div>
                           
                      <hr style="clear: both;">
                      
                        <div class="" style="padding: 15px;">
                           <div class="span12" style=" text-align:center;">
                              <h4 class="text-center">@if (Lang::has(Session::get('lang_file').'.INVOICE_DETAILS')!= '') {{ trans(Session::get('lang_file').'.INVOICE_DETAILS')}}  @else {{ trans($OUR_LANGUAGE.'.INVOICE_DETAILS')}} @endif</h4>
                              <span style=" text-align:center;">@if (Lang::has(Session::get('lang_file').'.THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')!= '') {{  trans(Session::get('lang_file').'.THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')}}  @else {{ trans($OUR_LANGUAGE.'.THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')}} @endif </span>
                           </div>
                        </div>
                     
                       
                     <!--    <h4 class="text-center" style="text-align:center;padding-top: 20px; margin-bottom: 0px;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_DETAILS')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_DETAILS')}}  @else {{ trans($OUR_LANGUAGE.'.PRODUCT_DETAILS')}} @endif</h4> -->
                        <div class="table-responsive">
                        <table class="inv-table" style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">
                               <tr style="border-bottom:1px solid #666; background:#f5f5f5;">
                                 <td width="">@if (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_TITLE')}}  @else {{ trans($OUR_LANGUAGE.'.PRODUCT_TITLE')}} @endif</td>
                                 <!--td  width="13%" align="center">Color</td>&nbsp;
                                    <td  width="13%" align="center">Size</td-->
                                 <td  width="" align="center">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{  trans(Session::get('lang_file').'.QUANTITY')}}  @else {{ trans($OUR_LANGUAGE.'.QUANTITY')}} @endif</td>
                                 <td  width="" align="center">@if (Lang::has(Session::get('lang_file').'.PRICE')!= '') {{  trans(Session::get('lang_file').'.PRICE')}}  @else {{ trans($OUR_LANGUAGE.'.PRICE')}} @endif</td>
                                 <td  width="" align="center">@if (Lang::has(Session::get('lang_file').'.SUB_TOTAL')!= '') {{  trans(Session::get('lang_file').'.SUB_TOTAL')}}  @else {{ trans($OUR_LANGUAGE.'.SUB_TOTAL')}} @endif</td>
                                 <td  width="" align="center">@if (Lang::has(Session::get('lang_file').'.PAYMENT_STATUS')!= '') {{  trans(Session::get('lang_file').'.PAYMENT_STATUS')}}  @else {{ trans($OUR_LANGUAGE.'.PAYMENT_STATUS')}} @endif</td>
                                 <br>
                                 <td  width="" align="center">@if (Lang::has(Session::get('lang_file').'.DELIVERY_STATUS')!= '') {{  trans(Session::get('lang_file').'.DELIVERY_STATUS')}}  @else {{ 
                                    trans($OUR_LANGUAGE.'.DELIVERY_STATUS')}} @endif
                                 </td>
                              </tr>
                              <?php
                                 $customerid   = Session::get('customerid');
                                 $product_titles=DB::table('nm_order_payu')
                                 ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
                                 ->leftjoin('nm_color', 'nm_order_payu.order_pro_color', '=', 'nm_color.co_id')
                                 ->leftjoin('nm_size', 'nm_order_payu.order_pro_size', '=', 'nm_size.si_id')
                                 ->where('transaction_id', '=', $coddetails1_paypal->transaction_id)
                                 ->where('nm_order_payu.order_cus_id', '=', $customerid)
                                 ->where('nm_order_payu.order_type','=',2)
                                 ->get(); 
                                 $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0;  ?>
                              @foreach($product_titles as $prd_tit) 
                              <?php 
                                 $status=$prd_tit->delivery_status; ?>
                               @if($prd_tit->delivery_status==1)
                                    @php
                                    $orderstatus= (Lang::has(Session::get('lang_file').'.ORDERS_PLACED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PLACED') : trans($OUR_LANGUAGE.'.ORDERS_PLACED') ;
                                    @endphp
                                    @elseif($prd_tit->delivery_status==2) 
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_PACKED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PACKED') : trans($OUR_LANGUAGE.'.ORDERS_PACKED');
                                    @endphp
                                    @elseif($prd_tit->delivery_status==3) 
                                    @php
                                    $orderstatus= (Lang::has(Session::get('lang_file').'.ORDERS_DISPATCHED')!= '') ? trans(Session::get('lang_file').'.ORDERS_DISPATCHED') : trans($OUR_LANGUAGE.'.ORDERS_DISPATCHED') ;
                                    @endphp
                                    @elseif($prd_tit->delivery_status==4) 
                                    @php
                                    $orderstatus= (Lang::has(Session::get('lang_file').'.ORDERS_DELIVERED')!= '') ? trans(Session::get('lang_file').'.ORDERS_DELIVERED') : trans($OUR_LANGUAGE.'.ORDERS_DELIVERED');
                                    @endphp
                                    @elseif($prd_tit->delivery_status==5)
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_CANCEL_PENDING')!= '') ? trans(Session::get('lang_file').'.ORDERS_CANCEL_PENDING') : trans($OUR_LANGUAGE.'.ORDERS_CANCEL_PENDING') ;
                                    @endphp
                                    @elseif($prd_tit->delivery_status==6) 
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_CENCELED')!= '') ? trans(Session::get('lang_file').'.ORDERS_CENCELED') : trans($OUR_LANGUAGE.'.ORDERS_CENCELED') ;
                                    @endphp
                                    @elseif($prd_tit->delivery_status==7) 
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_RETURN_PENDING')!= '') ? trans(Session::get('lang_file').'.ORDERS_RETURN_PENDING') : trans($OUR_LANGUAGE.'.ORDERS_RETURN_PENDING');
                                    @endphp
                                    @elseif($prd_tit->delivery_status==8) 
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_RETURNED')!= '') ? trans(Session::get('lang_file').'.ORDERS_RETURNED') : trans($OUR_LANGUAGE.'.ORDERS_RETURNED');
                                    @endphp
                                    @elseif($prd_tit->delivery_status==9) 
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_REPLACE_PENDING')!= '') ? trans(Session::get('lang_file').'.ORDERS_REPLACE_PENDING') : trans($OUR_LANGUAGE.'.ORDERS_REPLACE_PENDING');
                                    @endphp
                                    @elseif($prd_tit->delivery_status==10) 
                                    @php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_REPLACED')!= '') ? trans(Session::get('lang_file').'.ORDERS_REPLACED') : trans($OUR_LANGUAGE.'.ORDERS_REPLACED') ;
                                    @endphp
                                    @else
                                    @php
                                    $orderstatus = '';
                                    @endphp
                                    @endif 
                              @if($prd_tit->order_status==1)
                              @php
                              $payment_status=(Lang::has(Session::get('lang_file').'.SUCCESS')!= '') ? trans(Session::get('lang_file').'.SUCCESS') : trans($OUR_LANGUAGE.'.SUCCESS');
                              @endphp
                              @elseif($prd_tit->order_status==2) 
                              @php
                              $payment_status=(Lang::has(Session::get('lang_file').'.ORDERS_PACKED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PACKED') : trans($OUR_LANGUAGE.'.ORDERS_PACKED');
                              @endphp
                              @elseif($prd_tit->order_status==3) 
                              @php
                              $payment_status=(Lang::has(Session::get('lang_file').'.HOLD')!= '') ? trans(Session::get('lang_file').'.HOLD') : trans($OUR_LANGUAGE.'.HOLD');
                              @endphp
                              @elseif($prd_tit->order_status==4) 
                              @php
                              $payment_status=(Lang::has(Session::get('lang_file').'.FAILED')!= '') ? trans(Session::get('lang_file').'.FAILED') : trans($OUR_LANGUAGE.'.FAILED');
                              @endphp
                              @endif
                              <?php 
                                 $subtotal=$prd_tit->order_amt; 
                                             $tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);
                                          
                                             $total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 
                                             $total_ship_amt+= $prd_tit->order_shipping_amt;
                                             $total_item_amt+=$prd_tit->order_amt;
                                             $coupon_amount+= $prd_tit->coupon_amount;
                                             $prodct_id = $prd_tit->order_pro_id;
                                             
                                       $item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);
                                          
                                       
                                           $ship_amt = $prd_tit->order_shipping_amt;
                                          
                                       
                                           //$item_tax = $codorderdet->cod_tax;
                                          /*if($prd_tit->coupon_amount != 0)
                                          {
                                             $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                                          }
                                          else
                                          {
                                             $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
                                          }*/
                                 
                                          $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                                                
                                 ?>
                              <tr>
                                 <td  width="" align="center"><?php 
                                    if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
                                    $deal_title = 'deal_title';
                                    }else {  $deal_title = 'deal_title_'.Session::get('lang_code'); }
                                                      echo $prd_tit->$deal_title."<br/>";?>
                                 </td>
                                          
                                 <!--td  width="13%" align="center"><?php //echo $coddetails1->co_name;?></td>&nbsp;
                                    <td  width="13%" align="center"><?php //echo $coddetails1->si_name;?></td-->
                                 <td  width="" align="center">{{ $prd_tit->order_qty }} </td>
                                 <td  width="" align="center">{{ Helper::cur_sym() }} {{ $prd_tit->order_amt }} </td>
                                 <td  width="" align="center">{{ $subtotal - $prd_tit->coupon_amount }}</td>
                                 <td  width="" align="center">{{ $payment_status }}</td>
                                 <td  width="" align="center">{{ $orderstatus}}</td>
                              </tr>
                              @endforeach
                           </table>
                        </div>
                        <br>
                        <hr>
                        <div class="" style="padding: 15px;">
                           <div class="col-lg-6"></div>
                           <div class="col-lg-6">
                              <span>@if (Lang::has(Session::get('lang_file').'.SHIPMENT_VALUE')!= '') {{  trans(Session::get('lang_file').'.SHIPMENT_VALUE')}}  @else {{ trans($OUR_LANGUAGE.'.SHIPMENT_VALUE')}} @endif<b class="pull-right" style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $total_ship_amt  }} </b></span><br>
                              <span>@if (Lang::has(Session::get('lang_file').'.TAX')!= '') {{  trans(Session::get('lang_file').'.TAX')}}  @else {{ trans($OUR_LANGUAGE.'.TAX')}} @endif<b class="pull-right" style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $total_tax_amt }}</b></span><br>
                              @php  $subtotal1 = 0; @endphp
                              @if(count($wallet)!=0) 
                              <span>
                                 wallet :
                                 <b class="pull-right" style="margin-right:15px;">{{ Helper::cur_sym() }} -<?php echo $wallet_amt;?></b>
                              </span>
                              <br>
                              @endif
                              <hr>
                              <?php $totamt_paypal=$subtotal1+$coddetails1_paypal->order_tax;?>
                              <span>@if (Lang::has(Session::get('lang_file').'.AMOUNT')!= '') {{  trans(Session::get('lang_file').'.AMOUNT')}}  @else {{ trans($OUR_LANGUAGE.'.AMOUNT')}} @endif<b class="pull-right"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $grand_total-$wallet_amt }}</b></span>
                           </div>
                        </div>
                          </div>
                         <div class="modal-footer" style="border-bottom:none; overflow: hidden;background: #f5f5f5;"><div class="container">
                                   
                                 </div>
                        </div>
                     
                   
                  </div>
               </div>
               <?php $j=$j+1;  ?>
               @endforeach   
                {!!  $footer !!}
                <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>

</body>
</html>