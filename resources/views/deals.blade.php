{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

 <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li> 

            <li><strong>{{ (Lang::has(Session::get('lang_file').'.DEALS')!= '') ?  trans(Session::get('lang_file').'.DEALS'): trans($OUR_LANGUAGE.'.DEALS') }}</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>



  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">



	 <center> @if (Session::has('success1'))

		<div class="alert alert-warning alert-dismissable">{!! Session::get('success1') !!}

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

		</div>

		@endif

	</center>

	

	 <div id="success_msg"></div>

					

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <?php /* <div class="category-description std">

            <div class="slider-items-products">

              <div id="category-desc-slider" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 

                  

                  <!-- Item -->

                 <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img1.jpg"></a>

                    <div class="inner-info">

                      <div class="cat-img-title"> <span>Best Product 2017</span>

                        <h2 class="cat-heading">Best Selling Brand</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

                        <a class="info" href="#">Shop Now</a> </div>

                    </div>

                  </div>

                  <!-- End Item --> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img2.jpg"></a> </div> 

                  

                  <!-- End Item --> 

                  

                </div>

              </div>

            </div>

          </div> */ ?>

          <div class="shop-inner" id="deals_ajax_display">

            <div class="page-title">

              <h2>{{ (Lang::has(Session::get('lang_file').'.DEALS')!= '') ?  trans(Session::get('lang_file').'.DEALS'): trans($OUR_LANGUAGE.'.DEALS') }}</h2>

            </div>

            @if($maincategory_id == '') 

            <div class="toolbar">              

              <div class="sorter">

                <div class="short-by">

                  <label>@if (Lang::has(Session::get('lang_file').'.SORT_BY')!= '') {{  trans(Session::get('lang_file').'.SORT_BY') }} @else {{ trans($OUR_LANGUAGE.'.SORT_BY') }} @endif:</label>

                  <select name="filtertypes" onchange="displayproductrecords('<?php echo $page_limit; ?>','<?php echo $pagenum; ?>',this.options[this.selectedIndex].value);">

                  	<option value="">{{ (Lang::has(Session::get('lang_file').'.SORT_BY')!= '') ? trans(Session::get('lang_file').'.SORT_BY') : trans($OUR_LANGUAGE.'.SORT_BY') }}</option>

                    <option value="1">@if (Lang::has(Session::get('lang_file').'.PRICE_LOW')!= '') {{  trans(Session::get('lang_file').'.PRICE_LOW') }} @else {{ trans($OUR_LANGUAGE.'.PRICE_LOW') }} @endif - @if (Lang::has(Session::get('lang_file').'.HIGH')!= '') {{  trans(Session::get('lang_file').'.HIGH') }} @else {{ trans($OUR_LANGUAGE.'.HIGH') }} @endif</option>

                    <option value="2">@if (Lang::has(Session::get('lang_file').'.PRICE_HIGH')!= '') {{  trans(Session::get('lang_file').'.PRICE_HIGH') }} @else {{ trans($OUR_LANGUAGE.'.PRICE_HIGH') }} @endif -@if (Lang::has(Session::get('lang_file').'.LOW')!= '') {{  trans(Session::get('lang_file').'.LOW') }} @else {{ trans($OUR_LANGUAGE.'.LOW') }} @endif</option>

                    <option value="3">@if (Lang::has(Session::get('lang_file').'.TITLE')!= '') {{  trans(Session::get('lang_file').'.TITLE') }} @else {{ trans($OUR_LANGUAGE.'.TITLE') }} @endif @if (Lang::has(Session::get('lang_file').'.A')!= '') {{ trans(Session::get('lang_file').'.A') }}  @else {{ trans($OUR_LANGUAGE.'.A') }} @endif-@if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif</option>

                    <option value="4">@if (Lang::has(Session::get('lang_file').'.TITLE')!= '') {{  trans(Session::get('lang_file').'.TITLE') }} @else {{ trans($OUR_LANGUAGE.'.TITLE') }} @endif @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif-@if (Lang::has(Session::get('lang_file').'.A')!= '') {{  trans(Session::get('lang_file').'.A') }} @else {{ trans($OUR_LANGUAGE.'.A') }} @endif</option>

                    <option value="5">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{  trans(Session::get('lang_file').'.DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif @if (Lang::has(Session::get('lang_file').'.A')!= '') {{ trans(Session::get('lang_file').'.A') }}  @else {{ trans($OUR_LANGUAGE.'.A') }} @endif- @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif</option>

                    <option value="6">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{ trans(Session::get('lang_file').'.DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{  trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif- @if (Lang::has(Session::get('lang_file').'.A')!= '') {{  trans(Session::get('lang_file').'.A') }}  else {{ trans($OUR_LANGUAGE.'.A') }} @endif</option>

                  </select>

                </div>

                <div class="short-by page">

                  <label>@if (Lang::has(Session::get('lang_file').'.SHOW')!= '') {{  trans(Session::get('lang_file').'.SHOW') }} @else {{ trans($OUR_LANGUAGE.'.SHOW') }} @endif @if (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') {{  trans(Session::get('lang_file').'.PER_PAGE') }} @else {{ trans($OUR_LANGUAGE.'.PER_PAGE') }} @endif:</label>

                 <select name="perpagenumber" onchange="displayproductrecords(this.options[this.selectedIndex].value,'<?php echo $pagenum; ?>','<?php echo $filter; ?>')" >

          					<option value="9">9 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="18">18 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="36">36 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="all">@if (Lang::has(Session::get('lang_file').'.ALL')!= '') {{  trans(Session::get('lang_file').'.ALL') }} @else {{ trans($OUR_LANGUAGE.'.ALL') }} @endif </option>

                  </select>

                </div>

              </div>

            </div>

			@endif

		
			

            <div class="product-grid-area">

              <ul class="products-grid">

			   @if(count($product_details) != 0)

				@foreach($product_details as $product_det)				 

					

						<!-- //should not show this if datas -->

					@if($product_det->deal_no_of_purchase < $product_det->deal_max_limit) 



					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					 @php   $deal_title = 'deal_title'; @endphp

					@else @php   $deal_title = 'deal_title_langCode'; @endphp @endif



				 @php $mostproduct_img = explode('/**/',$product_det->deal_image);

					$product_discount_percentage = $product_det->deal_discount_percentage;

					$date = date('Y-m-d H:i:s');



					$product_image     = $mostproduct_img[0];

				 

					$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

					$img_data   = "public/assets/deals/".$product_image; @endphp



					@if(file_exists($img_data) && $product_image !='')  



				  @php  $prod_path = url('').'/public/assets/deals/' .$product_image; @endphp                

					@else  

					@if(isset($DynamicNoImage['dealImg']))

					  @php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp

					   @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

						 @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

					@endif

					@endif  

				   <!--  /* Image Path ends */   

					//Alt text -->

				 @php   $alt_text   = substr($product_det->$deal_title,0,25);

					$alt_text  .= strlen($product_det->$deal_title)>25?'..':''; @endphp  



			  

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					  @if($product_det->deal_discount_percentage!='' && round($product_det->deal_discount_percentage)!=0)

                        <div class="icon-sale-label sale-left">{{ substr($product_det->deal_discount_percentage,0,2) }}%</div>@endif

					

						@php 

						$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

						$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));

						$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));

						$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 

						$res = base64_encode($product_det->deal_id);



						$date2 = $product_det->deal_end_date;

						$deal_end_year = date('Y',strtotime($date2));

						$deal_end_month = date('m',strtotime($date2));

						$deal_end_date = date('d',strtotime($date2));

						$deal_end_hours = date('H',strtotime($date2));  

						$deal_end_minutes = date('i',strtotime($date2));    

						$deal_end_seconds = date('s',strtotime($date2));  @endphp

					

                        <div class="pr-img-area">

						

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')

						<a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> <img class="first-img" alt="{{ $alt_text }}"  src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif  

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')

						<a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">

                          <figure> <img class="first-img" alt="{{ $alt_text }}"  src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif

						@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')

						<a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">

                          <figure> <img class="first-img" alt="{{ $alt_text }}"  src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif

						@if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 

						<a href="{!! url('dealview').'/'.$mcat.'/'.$res!!}">

                          <figure> <img class="first-img" alt="{{ $alt_text }}"  src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif

						

						</div>

                        <div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> 

							

						 <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->deal_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>

                   

							@if(Session::has('customerid'))

								@if(count($prodInWishlist)==0)

								<input type="hidden" name="_token" value="{!! csrf_token() !!}" />

								{{ Form::hidden('pro_id','$product_det->deal_id') }}        

					   

								  <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">

								  <a href="" onclick="addtowish({{ $product_det->deal_id }},{{ Session::get('customerid') }})" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >

								  <input type="hidden" id="wishlisturl" value="{{ url('user_profile') }}">

									<i class="fa fa-heart-o" aria-hidden="true"></i> 

								  </a>

									@else

									<?php /* remove wishlist */?>   

										

									<a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">

										 <i class="fa fa-heart" aria-hidden="true"></i> 

									</a> 

									<?php /*remove link:remove_wish_product/wishlist table_id*/ ?>

										

								@endif  

						   @else 

							  <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">

							  

								<i class="fa fa-heart-o" aria-hidden="true"></i> 

							  

							  </a>

						  @endif



							</div>

                            <div class="mt-button quick-view"> <a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap{{ $product_det->deal_id }}"> <i class="fa fa-search"></i> </a> </div>

                          </div>

                        </div>

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="" href="">{{substr($product_det->$deal_title,0,25) }} {{  strlen($product_det->$deal_title)>25?'..':'' }} </a> </div>

                          <div class="item-content">

						  

						  

						  @php

						  

						  $one_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 1)->count();

						  $two_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 2)->count();

						  $three_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 3)->count();

						  $four_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 4)->count();

						  $five_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 5)->count();

						  

						  

						  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

						  $multiple_countone = $one_count *1;

						  $multiple_counttwo = $two_count *2;

						  $multiple_countthree = $three_count *3;

						  $multiple_countfour = $four_count *4;

						  $multiple_countfive = $five_count *5;

						  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

				  

                          <div class="rating">

						@if($product_count)

							 @php   $product_divide_count = $product_total_count / $product_count; @endphp

							 @if($product_divide_count <= '1') 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 @elseif($product_divide_count >= '1') 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 @elseif($product_divide_count >= '2') 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

							 @elseif($product_divide_count >= '3') 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 @elseif($product_divide_count >= '4') 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>

							 @elseif($product_divide_count >= '5') 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>

							 @else

								

							@endif

						@else

						   <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					    @endif	

					  </div>

							

							

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }}

                                       {{ $product_det->deal_discount_price }}</span> </span> </div>

                            </div>

                            <div class="pro-action">

							   @if($date >= $product_det->deal_end_date)  

								   <button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span> </button>

                                @else          

									@php $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

									 $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));

									 $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));

									 $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 

									 $res = base64_encode($product_det->deal_id); @endphp

							

								@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

								<a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

								@endif

							   

							    @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

							    <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							    @endif

								

								@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

							    <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							    @endif

								

								@if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')  

							    <a href="{!! url('dealview').'/'.$mcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							    @endif

								

						<script>

						$(function(){

							//year = <?php echo $deal_end_year;?>; month = <?php echo $deal_end_month;?>; day = <?php echo $deal_end_date;?>;hour= <?php echo $deal_end_hours;?>; min= <?php echo $deal_end_minutes;?>; sec= <?php echo $deal_end_seconds;?>;

						   

						countProcess(<?php echo $product_det->deal_id; ?>);

						});

						</script>

				            @endif

							

							

								

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

				@endif

                     @endforeach

					  @else <center><div class="list box text-shadow">{{ (Lang::has(Session::get('lang_file').'.NO_RESULTS_FOUND')!= '') ? trans(Session::get('lang_file').'.NO_RESULTS_FOUND') : trans($OUR_LANGUAGE.'.NO_RESULTS_FOUND') }}</div></center>

				@endif

              </ul>

            </div>

			

            @if($maincategory_id=='')

            <div class="pagination-area">			

              <?php

              if ( ($pagenum-1) > 0) 

              {

                $prpre =$pagenum-1;

              ?>

              <div class="" >

                <button style="vertical-align: top; margin-top: 0px;" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo 1; ?>','<?php echo $filter;  ?>');" data-type="first">«</button>

                <button style="vertical-align: top; margin-top: 0px;" data-number="0" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $pagenum-1; ?>','<?php echo $filter;  ?>');">‹</button> 

              </div>

              <?php

              } 

              ?>

              <span  class="">

              <?php

              $links=$pagenum+4; 

              for($i=$pagenum; $i<=$links; $i++)

              {

                if($i<=$last)

                {

                  if ($i == $pagenum ) 

                  {

                  ?>

                    <button style="vertical-align: top; margin-top: 0px;" type="button" class="active" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $i; ?>','<?php echo $filter;  ?>');" ><?php echo $i; ?></button> 

                  <?php

                  }

                  else

                  {

                  ?>

                    <button style="vertical-align: top; margin-top: 0px;" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $i; ?>','<?php echo $filter;  ?>');" ><?php echo $i; ?></button>  

                  <?php

                  }

                }

              }

              ?>  

              </span>

              

              <span class="pagina-nav" >

              <?php 

              if ( ($pagenum+1) <= $last)

              {

              ?>

                <button style="vertical-align: top;" data-number="1" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $pagenum+1; ?>','<?php echo $filter;  ?>');" >›</button>

                <button style="vertical-align: top;" data-number="3" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $last; ?>','<?php echo $filter;  ?>');" >»</button>

              <?php

              }

              ?>

              </span>

            </div>

            @endif

          </div>







        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

          

          <div class="block shop-by-side">

            <div class="sidebar-bar-title">

              <h3>{{ (Lang::has(Session::get('lang_file').'.SHOP_BY')!= '') ?  trans(Session::get('lang_file').'.SHOP_BY'): trans($OUR_LANGUAGE.'.SHOP_BY') }}</h3>

            </div>

            <div class="block-content">

              <p class="block-subtitle">{{ (Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES') }}</p>

               @if(count($main_category)!=0)

				<div id="divMenu">

					<ul>

					 @foreach($main_category as $fetch_main_cat)

						@php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; @endphp		

						@if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0) 						

						<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id1) }}">

							@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

							@php	$mc_name = 'mc_name'; @endphp

							@else  @php $mc_name = 'mc_name_code'; @endphp @endif

							{{ $fetch_main_cat->$mc_name }}</a> 

						   <ul>

							@foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)  

								@php $check_sub_deal_exists = DB::table('nm_deals')->where('deal_main_category', '=', $fetch_sub_main_cat->smc_id)->where('deal_status','=',1)->whereRaw('deal_no_of_purchase < deal_max_limit')->get(); @endphp

								@if($check_sub_deal_exists)

										

								@php $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp

								<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id2) }}">

								 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

										@php	$smc_name = 'smc_name'; @endphp

										@else @php  $smc_name = 'smc_name_code'; @endphp @endif

										{{ $fetch_sub_main_cat->$smc_name }} </a>

										

										 @if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0) 

									 <ul>

									  @foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)

													 

										@php 

										$check_sec_main_deal_exists = DB::table('nm_deals')->where('deal_sub_category', '=', $fetch_sub_cat->sb_id)->where('deal_status','=',1)->whereRaw('deal_no_of_purchase < deal_max_limit')->get(); 

										@endphp

										@if($check_sec_main_deal_exists)	

									    @php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp

										<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id3) }}">

										@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

										@php	$sb_name = 'sb_name'; @endphp

										@else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

										{{ $fetch_sub_cat->$sb_name }}</a>

										

										 @if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0) 

											<ul>	

												@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)

																

													@php $check_sec_sub_main_deal_exists = DB::table('nm_deals')->where('deal_second_sub_category', '=', $fetch_secsub_cat->ssb_id)->where('deal_status','=',1)->whereRaw('deal_no_of_purchase < deal_max_limit')->get(); @endphp

													@if($check_sec_sub_main_deal_exists)	

													@php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp

												<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)}}">

												@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

													@php $ssb_name = 'ssb_name'; @endphp

														@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

														{{ $fetch_secsub_cat->$ssb_name }}</a>

												</li>

												 @endif

													@endforeach 

											</ul>

											 @endif

										</li>

										@endif @endforeach

									</ul>

									 @endif

								</li>

								 @endif @endforeach

						   </ul>

						   @endif

						</li>

						@endforeach

					</ul>

					@else 

						<ul> <li>@if (Lang::has(Session::get('lang_file').'.NO_CATEGORY_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_CATEGORY_FOUND') }} @else {{ trans($OUR_LANGUAGE.'.NO_CATEGORY_FOUND') }}! @endif </li></ul> @endif

				</div>

              

            </div>

          </div>

		  

		  @php $deals_avail = DB::table('nm_deals')->where('deal_status','=','1')->count(); @endphp

		  @if(Request::segment(1)=="catdeals")

           @if($deals_avail>0)

		   <div class="block product-price-range ">

            <div class="sidebar-bar-title">

              <h3>@if (Lang::has(Session::get('lang_file').'.DISCOUNT')!= '') {{ trans(Session::get('lang_file').'.DISCOUNT') }} @else {{ trans($OUR_LANGUAGE.'.DISCOUNT') }}! @endif</h3>

            </div>

			

			@php	$discount_filter=array();

				$filter_discount="";

				$price_from="";

				$price_to=""; @endphp

				@if(isset($_GET["filter_discount"]))

				

				@php	$filter_discount=base64_decode($_GET["filter_discount"]);

					$discount_filter=explode(",",$filter_discount); @endphp

				@endif

				@if(isset($_GET["price_from"]))

				

					@php $price_from=$_GET["price_from"];

					$price_to=$_GET["price_to"]; @endphp

				@endif

				

            <div class="block-content">

              <div class="slider-range">

                <ul class="check-box-list">

					<?php

					$label="";

					for($i=1;$i<=6;$i++)

					{

						if($i==1)

							$label=((Lang::has(Session::get('lang_file').'.UPTO')!= '') ? trans(Session::get('lang_file').'.UPTO') : trans($OUR_LANGUAGE.'.UPTO'))." 10%";

						if($i==2)

							$label="10% - 20%";

						if($i==3)

							$label="20% - 30%";

						if($i==4)

							$label="30% - 40%";

						if($i==5)

							$label="40% - 50%";

						if($i==6)

							$label="50% ".((Lang::has(Session::get('lang_file').'.AND_ABOVE')!= '') ? trans(Session::get('lang_file').'.AND_ABOVE') : trans($OUR_LANGUAGE.'.AND_ABOVE'));

					?>

                  <li>

                    <input type="radio" name="discount_filter" value="<?php echo $i; ?>" <?php if(in_array($i,$discount_filter)){echo "checked";} ?> onclick="javascript:make_filter()"> 

                    <label for="p1"> <span class="button"></span>{{ $label }}</label>

                  </li>

				  <?php	} ?>

                </ul>

				

				@if(isset($_GET["filter_discount"]) AND $_GET["filter_discount"]!="")

				<button type="button" onclick="javascript:clear_filter('filter_for_values_discount')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

			    @endif 

		

              </div>

            </div>

          </div>

		  

		  

         <div class="block product-price-range ">

		   <div class="sidebar-bar-title">

              <h3>{{ (Lang::has(Session::get('lang_file').'.PRICE_FILTER')!= '') ?  trans(Session::get('lang_file').'.PRICE_FILTER') : trans($OUR_LANGUAGE.'.PRICE_FILTER') }}</h3>

           </div>

			 <div class="block-content">

			   <div class="slider-range">

					<form name="filter_form_to_generate" id="filter_form" method="get">

					{{ Form::hidden('filter_discount','$filter_discount',array('id'=>'filter_for_values_discount'))}}

					

					<ul>

					 <li class="pri-filter-input">

            <div class="pri-filter-sec">

          <span style="color:#e83f33;">{{ (Lang::has(Session::get('lang_file').'.MIN')!= '') ? trans(Session::get('lang_file').'.MIN') : trans($OUR_LANGUAGE.'.MIN') }}</span>

          <input type="text"  placeholder="100" name="price_from" id="price_from" value="<?php echo $price_from; ?>" style="width:75px"/></div>

          <div class="pri-filter-sec">

					<span style="color:#e83f33;">{{ (Lang::has(Session::get('lang_file').'.MAX')!= '') ? trans(Session::get('lang_file').'.MAX') : trans($OUR_LANGUAGE.'.MAX') }}</span><input type="text" placeholder="10000" name="price_to" id="price_to" value="{{ $price_to }}" style="width:75px"/> </div>

					<div class="pri-filter-sec"><button type="button" onclick="javacript:make_filter();" class="button button-compare"> <span>{{ (Lang::has(Session::get('lang_file').'.GO')!= '') ? trans(Session::get('lang_file').'.GO') : trans($OUR_LANGUAGE.'.GO') }}</span></button></div>

					 </li>

					</ul>

					<br>

					@if($price_from !="" && $price_to !="") <!-- //Clear all for discount price -->

					    <button type="button" onclick="javascript:clear_discount_filter('price_from','price_to')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

					@endif

					<!--<input type="hidden" name="from" value="<?php // echo Request::segment(3); ?>">-->

				</form>

			  </div>

			</div>

		</div>

		@endif

		@endif	

        

		

		@if(count($get_special_product)!=0)

			@php $date = date('Y-m-d H:i:s'); @endphp

          <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3>@if (Lang::has(Session::get('lang_file').'.MOST_VISITED_DEALS')!= '') {{ trans(Session::get('lang_file').'.MOST_VISITED_DEALS') }} @else {{ trans($OUR_LANGUAGE.'.MOST_VISITED_DEALS') }} @endif</h3>

            </div>

            <div class="block-content">

			 <ul>

				@foreach($get_special_product as $fetch_most_visit_pro)   

				

				@if($fetch_most_visit_pro->deal_no_of_purchase < $fetch_most_visit_pro->deal_max_limit)
				@php

				$mostproduct_discount_percentage = $fetch_most_visit_pro->deal_discount_percentage;

				$mostproduct_img = explode('/**/', $fetch_most_visit_pro->deal_image);

				$mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				$smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				$sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				$ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

				$res = base64_encode($fetch_most_visit_pro->deal_id); @endphp

			   

				@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				   @php  $title = 'deal_title'; @endphp

				@else  @php  $title = 'deal_title_langCode'; @endphp @endif



				@php  $product_image     = $mostproduct_img[0];

				 

				$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				$img_data   = "public/assets/deals/".$product_image; @endphp

				

				@if(file_exists($img_data) && $product_image !='')   

				

			   @php $prod_path = url('').'/public/assets/deals/' .$product_image;   @endphp                

				@else  

					@if(isset($DynamicNoImage['dealImg']))

					@php    $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp

					   @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

						 @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

					@endif

				@endif   

			   <!--  /* Image Path ends */   

				//Alt text -->

				@php   $alt_text   = substr($fetch_most_visit_pro->$title,0,25);

				$alt_text  .= strlen($fetch_most_visit_pro->$title)>25?'..':'';    @endphp

			

                <li class="item">

                  <div class="products-block-left">

				  <?php /* @if($fetch_most_visit_pro->deal_discount_percentage!='' && round($fetch_most_visit_pro->deal_discount_percentage)!=0)		

                  <div class="icon-sale-label sale-left">{{ substr($fetch_most_visit_pro->deal_discount_percentage,0,2) }}%</div>@endif */ ?>



				   @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  

				   <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" class="product-image"><img src="{{ $prod_path }}" alt="{{ $alt_text }}"></a>

			       @endif

				  

				  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

				   <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" class="product-image"><img src="{{ $prod_path }}" alt="{{ $alt_text }}"></a>

			      @endif

				  

				 @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

				   <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" class="product-image"><img src="{{ $prod_path }}" alt="{{ $alt_text }}"></a>

			      @endif

				  

				   @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 

				   <a href="{!! url('dealview').'/'.$mcat.'/'.$res!!}" class="product-image"><img src="{{ $prod_path }}" alt="{{ $alt_text }}"></a>

			       @endif

				  

				  </div>

                  <div class="products-block-right">

                    <p class="product-name"> <a>{{substr($fetch_most_visit_pro->$title,0,25) }}    {{  strlen($fetch_most_visit_pro->$title)>25?'..':'' }}</a> </p>

                    <span class="price">{{ Helper::cur_sym() }} {{ $fetch_most_visit_pro->deal_discount_price }}</span>

                   

					

				  @php					  

				  $one_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 5)->count();

				  

				  

				  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

				  $multiple_countone = $one_count *1;

				  $multiple_counttwo = $two_count *2;

				  $multiple_countthree = $three_count *3;

				  $multiple_countfour = $four_count *4;

				  $multiple_countfive = $five_count *5;

				  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

					

					

				<div class="rating">

				@if($product_count)

					 @php   $product_divide_count = $product_total_count / $product_count; @endphp

					 @if($product_divide_count <= '1') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '1') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '2') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

					 @elseif($product_divide_count >= '3') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '4') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '5') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

					 @else

						

					@endif

				@else

				     <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

				@endif	

			  </div>

			    <br>

				

					@if($date > $fetch_most_visit_pro->deal_end_date)

				    <a class="link-all">@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }}  @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</a>

				    @else

						

					@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  

				    <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" class="link-all">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				    @endif

					

					@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

				    <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" class="link-all">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				    @endif

					

					@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

				    <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" class="link-all">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				    @endif

					

					@if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')

				    <a href="{!! url('dealview').'/'.$mcat.'/'.$res!!}" class="link-all">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				    @endif

				

				@endif

				

                  </div>

                </li>

				 @endif 

                @endforeach 

              </ul>

			  @endif

              </div>

          </div>

          

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   @include('service_section')
  

  <!--popup deal quick view-->

			@if(count($quickview_details) != 0) 

				@foreach($quickview_details as $product_det)

						<!-- //should not show this if datas -->

					@if($product_det->deal_no_of_purchase < $product_det->deal_max_limit)



					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					 @php   $deal_title = 'deal_title'; @endphp

					@else @php   $deal_title = 'deal_title_langCode'; @endphp @endif

					

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					 @php   $deal_description = 'deal_description'; @endphp

					@else @php   $deal_description = 'deal_description_langCode'; @endphp @endif

					

					

				 @php $count = $product_det->deal_max_limit - $product_det->deal_no_of_purchase; @endphp

				 

				  @php   $product_img= explode('/**/',trim($product_det->deal_image,"/**/")); 

					$img_count = count($product_img); @endphp

			

				 @php $mostproduct_img = explode('/**/',$product_det->deal_image);

				    

					$product_discount_percentage = $product_det->deal_discount_percentage;

					$date = date('Y-m-d H:i:s');



					$product_image     = $mostproduct_img[0];

				 

					$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

					$img_data   = "public/assets/deals/".$product_image; @endphp



					@if(file_exists($img_data) && $product_image !='')  



				  @php  $prod_path = url('').'/public/assets/deals/' .$product_image; @endphp                

					@else  

					@if(isset($DynamicNoImage['dealImg']))

					  @php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp

					   @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

						 @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

					@endif

					@endif   

				   <!--  /* Image Path ends */   

					//Alt text -->

				 @php   $alt_text   = substr($product_det->$deal_title,0,25);

					$alt_text  .= strlen($product_det->$deal_title)>25?'..':''; @endphp 
					

				 <input type="hidden" id="pro_qty_hidden_{{ $product_det->deal_id }}" name="pro_qty_hidden" value="<?php echo  $product_det->deal_max_limit; ?>" />

				 <input type="hidden" id="pro_purchase_hidden_{{ $product_det->deal_id }}" name="pro_purchase_hidden" value="<?php echo  $product_det->deal_no_of_purchase; ?>" />

					

					<div style="display: none;"  class="quick_view_popup-wrap deal-view-full" id="quick_view_popup-wrap{{ $product_det->deal_id }}">

					<div id="quick_view_popup-overlay"></div>

					  <div id="quick_view_popup-outer">

						<div id="quick_view_popup-content">

						  <div style="width:auto;height:auto;overflow: auto;position:relative;">

							<div class="product-view-area">

							  <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">							

								<div class="large-image">

								<a href="{{ $prod_path }}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="{{ $prod_path }}"> 

								</a>


								</div>
								

								<div class="flexslider flexslider-thumb">

								  <ul class="previews-list slides">

								  

								   @for($i=0;$i < $img_count;$i++)  

								   @php $product_image     = $mostproduct_img[$i];

								   $prod_path  = url('').'/public/assets/default_image/No_image_deal.png';

								   $img_data   = "public/assets/deals/".$product_image; @endphp

								   @if(file_exists($img_data) && $product_image !='')   

								   @php $prod_path = url('').'/public/assets/deals/' .$product_image; @endphp                

								   @else  

								   @if(isset($DynamicNoImage['dealImg']))

								   @php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp

								   @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

								   @php    $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

								   @endif

								   @endif 

				   

									<li style="width: 95px; float: left; display: block;"><a href='{{ $prod_path }}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{ $prod_path }}' "><img src="{{ $prod_path }}" alt = "Thumbnail 2"/></a></li>

									

									@endfor

								  </ul>

								</div>
								

								<!-- end: more-images --> 
							  </div>

							  <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">

								<div class="product-details-area">

								  <div class="product-name">

									<h1>{{substr($product_det->$deal_title,0,25) }} {{  strlen($product_det->$deal_title)>25?'..':'' }} </h1>

								  </div>

								  <div class="price-box">

									<p class="special-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $product_det->deal_discount_price }} </span> </p>

									<p class="old-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }}  {{ $product_det->deal_original_price }}</span> </p>

								  </div>

								  <div class="ratings">

									<div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>

									<p class="availability in-stock pull-right">@if (Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= '') {{ trans(Session::get('lang_file').'.AVAILABLE_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE_STOCK') }} @endif :  <span>{{  ($product_det->deal_max_limit)-($product_det->deal_no_of_purchase) }} @if (Lang::has(Session::get('lang_file').'.IN_STOCK')!= '') {{ trans(Session::get('lang_file').'.IN_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.IN_STOCK') }} @endif</span></p>

								  </div>

								  <div class="short-description">

									<h2>@if (Lang::has(Session::get('lang_file').'.OVERVIEW')!= '') {{ trans(Session::get('lang_file').'.OVERVIEW') }} @else {{ trans($OUR_LANGUAGE.'.OVERVIEW') }} @endif</h2>

										<p>{{ $product_det->$deal_description }}</p> 

								  </div>

								  <div class="product-color-size-area">

									<div class="color-area">

									  <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.DEAL_ENDS_IN')!= '') {{  trans(Session::get('lang_file').'.DEAL_ENDS_IN') }} @else {{ trans($OUR_LANGUAGE.'.DEAL_ENDS_IN') }} @endif</h2>

									  <div class="color">

										<p id="demo" class="stock-list"></p>

									  </div>

									</div>

								  </div>

								  <div class="product-variation">

								   {!! Form::open(array('url' => 'addtocart_deal','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}

									<form action="#" method="post">

									  <div class="cart-plus-minus">

										<label for="qty">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.QUANTITY') }} @endif:</label>

										<div class="numbers-row">

										  <div onClick="remove_quantity('{{ $product_det->deal_id }}')" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>

										  <input type="number" class="qty" min="1" value="1" max="{{  ($product_det->deal_max_limit - $product_det->deal_no_of_purchase) }}" id="addtocart_qty_{{ $product_det->deal_id }}" name="addtocart_qty" readonly required >

										  {{ Form::hidden('addtocart_deal_id',$product_det->deal_id)}}

										  {{ Form::hidden('addtocart_type','deals')}}

										  

										<div onClick="add_quantity('{{$product_det->deal_id}}')" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>

										

										</div>

									  </div>

									  

									  <input type="hidden" name="return_url" value="<?php //echo $product_det->mc_name.'/'.base64_encode(base64_encode(base64_encode($product_det->deal_category))); ?>" />

									  

									  @if(Session::has('customerid')) 

									   @if($count > 0)



								  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i>  @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{  trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button>

								  @else 

								  <button class="button pro-add-to-cart" title="Add to Cart" ><span>  @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif</span></button>

								  @endif

								  @else

								  <a href="" role="button" data-toggle="modal" data-target="#loginpop">

								  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i>  @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{  trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button></a>

								  @endif

								  <input type="hidden" id="enddate" name="enddate" value="{{ $product_det->deal_end_date }}">

								  <input type="hidden" id="enddate_format" name="enddate_format" value="<?php echo date('F j, Y, G:i:s',strtotime($product_det->deal_end_date)); ?>">

							   {!! Form::close() !!}

									 

								  </div>

								  

								 

								

								</div>

							  </div>

							</div>

							<!--product-view--> 

							

						  </div>

						</div>

						<a style="display: inline;" id="quick_view_popup-close" href="{{ url('') }}/deals"><i class="icon pe-7s-close"></i></a> </div>

					</div>

					

					@endif

                     @endforeach

				

				@endif


  <!--end popup deal quick view-->

  

{!! $footer !!}



<script type="text/javascript">



    $(document).ready(function() {

        $(document).on("click", ".customCategories .topfirst b", function() {

            $(this).next("ul").css("position", "relative");



            $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

            $(this).next("ul").toggle();

        });



        $(document).on("click", ".morePage", function() {

            $(".nextPage").slideToggle(200);

        });



        $(document).on("click", "#smallScreen", function() {

            $(this).toggleClass("customMenu");

        });



        $(window).scroll(function() {

            if ($(this).scrollTop() > 250) {

                $('#comp_myprod').show();

            } else {

                $('#comp_myprod').hide();

            }

        });

		

		var price_from = $('#price_from');

		var price_to = $('#price_to');

		

		 $('#price_from').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            alert("{{ (Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') ? trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED') : trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED') }}");

			return false;

        }

      });



	  $('#price_to').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            alert("{{ (Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') ? trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED') : trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED') }}");

			return false;

        }

      });



    });

</script>

      

 <!-- Count Down Coding -->

<script type="text/javascript">

@if(count($product_details) != 0)

function countProcess(deal_id){

    <?php 

foreach($product_details as $product_det){?>

var pro_deal_id = <?php echo $product_det->deal_id;?>;



if(deal_id==pro_deal_id){



<?php 

  $date2 = $product_det->deal_end_date;

  $deal_end_year = date('Y',strtotime($date2));

  $deal_end_month = date('m',strtotime($date2));

  $deal_end_date = date('d',strtotime($date2));

  $deal_end_hours = date('H',strtotime($date2));  

  $deal_end_minutes = date('i',strtotime($date2));    

  $deal_end_seconds = date('s',strtotime($date2)); 

?>

year = <?php echo $deal_end_year;?>; month = <?php echo $deal_end_month;?>; day = <?php echo $deal_end_date;?>;hour= <?php echo $deal_end_hours;?>; min= <?php echo $deal_end_minutes;?>; sec= <?php echo $deal_end_seconds;?>;

}

<?php 

}//foreach

?>



var timezone = new Date()

month        = --month;

dateFuture   = new Date(year,month,day,hour,min,sec);

//alert(deal_id);

        dateNow = new Date();                                                            

        amount  = dateFuture.getTime() - dateNow.getTime()+5;               

        delete dateNow;



        /* time is already past */

        //if(amount < 0){

                //output ="<span class='countDays'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='countDiv countDiv0'></span><span class='countHours'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span><span class='countDiv countDiv1'></span><span class='countMinutes'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span><span class='countDiv countDiv2'></span><span class='countSeconds'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span>" ;

                

 

        /* date is still good */

        //else{

                days=0; hours=0; mins=0; secs=0; output="";



                amount = Math.floor(amount/1000); /* kill the milliseconds */



                days   = Math.floor(amount/86400); /* days */

                amount = amount%86400;



                hours  = Math.floor(amount/3600); /* hours */

                amount = amount%3600;



                mins   = Math.floor(amount/60); /* minutes */

                amount = amount%60;



                secs   = Math.floor(amount); /* seconds */



				fdays = parseInt(days/10);	

				sdays = days%10;

				fhours = parseInt(hours/10);	

				shours = hours%10;

				fmins = parseInt(mins/10);	

				smins = mins%10;

				fsecs = parseInt(secs/10);	

				ssecs = secs%10;

               output="<span class='countDays'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>" + fdays +"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>" + sdays +"</span></span><span class='countDiv countDiv0'></span><span class='countHours'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fhours+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+shours+"</span></span></span><span class='countDiv countDiv1'></span><span class='countMinutes'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fmins+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+smins+"</span></span></span><span class='countDiv countDiv2'></span><span class='countSeconds'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fsecs+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+ssecs+"</span></span></span>" ;

                     
                $('#countdown'+deal_id).html(output);

            

           //alert(sdays); 



                setTimeout(function() { countProcess(deal_id); }, 1000);

        //}

        

}

 @endif 

function make_filter()

{

	var filter_items_by_discount=[];

	var price_from=$("#price_from").val();

	var price_to=$("#price_to").val();

	var price_from=parseInt(price_from);

	var price_to=parseInt(price_to);

	var price_radio=$("input[name='radio_price_filter']:checked").val();

	//alert(price_to);

	if(price_radio)

	{

		var min_max_price=price_radio.split('-');

		$("#price_from").val(min_max_price[0]);

		$("#price_to").val(min_max_price[1]);

		

	}

	

	$("input:radio[name=discount_filter]:checked").each(function(){

		filter_items_by_discount.push($(this).val());

	});

	//alert(price_to);

	var enc_filter_by_discount = window.btoa(filter_items_by_discount);

	$("#filter_for_values_discount").val(enc_filter_by_discount);

	if(price_from > price_to)

	{

		alert("{{ (Lang::has(Session::get('lang_file').'.MAX_PRICE_SHOULD_LESS_MIN')!= '') ? trans(Session::get('lang_file').'.MAX_PRICE_SHOULD_LESS_MIN') : trans($OUR_LANGUAGE.'.MAX_PRICE_SHOULD_LESS_MIN') }}");

		$("#price_to").focus();

		return false;

	}

	

   document.filter_form_to_generate.submit();             // Submit the page

    

}

function clear_filter(id_to_clear)

{

	$("#"+id_to_clear).val("");

	document.filter_form_to_generate.submit();

}

/*clear filter for discount price*/

function clear_discount_filter(from_price,to_price)

{

	$("#"+from_price).val("");

	$("#"+to_price).val("");

	document.filter_form_to_generate.submit();

}

</script>



 <script type="text/javascript">

  function addtowish(pro_id,cus_id){

    var wishlisturl = document.getElementById('wishlisturl').value;



    $.ajax({

          type: "get",   

          url:"<?php echo url('addtowish'); ?>",

          data:{'pro_id':pro_id,'cus_id':cus_id},

          success:function(response){

            //alert(response);

            if(response==0){

              $('#success_msg').html('<p>Added to wishlist</p>')

            <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>

                        $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');

              //window.location=wishlisturl;

                            window.location.reload();

                            

            }else{

              alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');

              //window.location=wishlisturl;

            }

          }

        });

  }

</script>



  <script>

function add_quantity(id)

{

  //alert();

  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/

  var quantity=$("#addtocart_qty_"+id).val();

  var pro_qty_hidden=$("#pro_qty_hidden_"+id).val();

  var pro_purchase_hidden=$("#pro_purchase_hidden_"+id).val();

  var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);

  if(quantity<remaining_product)

  {

    var new_quantity=parseInt(quantity)+1;

    $("#addtocart_qty_"+id).val(new_quantity);

  }

  //alert();

}



function remove_quantity(id)

{

  //alert();

  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/



  var quantity=$("#addtocart_qty_"+id).val();

  var quantity=parseInt(quantity);

  if(quantity>1)

  {

    var new_quantity=quantity-1;

    $("#addtocart_qty_"+id).val(new_quantity);

  }

  //alert();

}

</script>







<script>

// Set the date we're counting down to



var getenddate = document.getElementById("enddate").value;

var getenddate_format = document.getElementById("enddate_format").value;

var countDownDate = new Date(getenddate_format).getTime();



// Update the count down every 1 second

var x = setInterval(function() {



  // Get todays date and time

  var now = new Date().getTime();



  // Find the distance between now an the count down date

  var distance = countDownDate - now;



  // Time calculations for days, hours, minutes and seconds

  var days = Math.floor(distance / (1000 * 60 * 60 * 24));

  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

  var seconds = Math.floor((distance % (1000 * 60)) / 1000);



  // Display the result in the element with id="demo"

  document.getElementById("demo").innerHTML = days + "<span style='color:#e74c3c;'>d</span> -" + hours + "<span style='color:#e74c3c;'>h </span>-"

  + minutes + "<span style='color:#e74c3c;'>m</span> -" + seconds + "<span style='color:#e74c3c;'>s</span> ";



  // If the count down is finished, write some text 

  if (distance < 0) {

    clearInterval(x);

    document.getElementById("demo").innerHTML = "EXPIRED";

  }

}, 1000);

</script>

<script type="text/javascript"> 

     function displayproductrecords(numRecords,pageNum,filter)

     {  

        var path = '<?php echo url('deals_ajax_pagination'); ?>';

        $.ajax({

                type: "GET",

                url: path,

                data: "show=" + numRecords + "&pagenum=" + pageNum + "&filter=" + filter,

                cache: false,

                datatype: "html",

                success: function(result) {

                    $("#deals_ajax_display").html(result);

                }

        });

       

    }

</script>







</body>



</html>

