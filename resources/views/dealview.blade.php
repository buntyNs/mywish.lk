<?php 
         if(count($product_details_by_id)>0){ //print_r($product_details_by_id);
               foreach($product_details_by_id as $pro_details_by_id) { } 
               if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
                 $deal_title = 'deal_title';
                 $deal_metakey = 'deal_meta_keyword';
                 $deal_metadesc = 'deal_meta_description';
               }else {  
                   $deal_title = 'deal_title_'.Session::get('lang_code');
                   $deal_metakey = 'deal_meta_keyword_'.Session::get('lang_code');
                   $deal_metadesc = 'deal_meta_description_'.Session::get('lang_code');
                }
         
         ?>

      <title>@if($deal_title != '') {{ $pro_details_by_id->$deal_title  }} @else  @endif</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="{{ $pro_details_by_id->$deal_metadesc  }}">
      <meta name="keywords" content="{{ $pro_details_by_id->$deal_metakey  }}">
      <meta name="author" content="{{ $pro_details_by_id->$deal_metakey  }}">
      <?php } ?>
	  {!! $navbar !!}
      {!! $header !!}
	 
  <!-- Main Container -->
  
     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
      @php $deal_title = 'deal_title'; @endphp
      @else @php  $deal_title = 'deal_title_'.Session::get('lang_code'); @endphp @endif
      @foreach($product_details_by_id as $pro_details_by_id) @endforeach
      @php $product_img= explode('/**/',trim($pro_details_by_id->deal_image,"/**/"));   
      $img_count = count($product_img);
      $count = $pro_details_by_id->deal_max_limit - $pro_details_by_id->deal_no_of_purchase;
      $date2 = $pro_details_by_id->deal_end_date;
      $deal_end_year = date('Y',strtotime($date2));
      $deal_end_month = date('m',strtotime($date2));
      $deal_end_date = date('d',strtotime($date2));
      $deal_end_hours = date('H',strtotime($date2));  
      $deal_end_minutes = date('i',strtotime($date2));    
      $deal_end_seconds = date('s',strtotime($date2)); 
      $product_image     = $product_img[0];
      $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
      $img_data   = "public/assets/deals/".$product_image; @endphp
      @if(file_exists($img_data) && $product_image !='')   
      @php $prod_path = url('').'/public/assets/deals/' .$product_image;  @endphp               
      @else  
      @if(isset($DynamicNoImage['dealImg']))
      @php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp
      @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))
      @php $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
      @endif
      @endif   
      <!-- /* Image Path ends */   
         //Alt text -->
      @php $alt_text   = substr($pro_details_by_id->$deal_title,0,25);
      $alt_text  .= strlen($pro_details_by_id->$deal_title)>25?'..':''; @endphp

      <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li>
            <li class=""> <a title="" href="{{ url('deals')}}">{{ (Lang::has(Session::get('lang_file').'.DEALS')!= '') ?  trans(Session::get('lang_file').'.DEALS'): trans($OUR_LANGUAGE.'.DEALS') }}</a></li>
           
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="main-container col1-layout">
    <div class="container">
	    <center>
               @if (Session::has('success1'))
               <div class="alert alert-warning alert-dismissable">{!! Session::get('success1') !!}
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               </div>
               @endif
            </center>
      <div class="row">
        <div class="col-main">
          <div class="product-view-area">
            <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                @php $product_image     = $product_img[0]; @endphp
                  <!-- /* Image Path */ -->
                  @php $prod_path  = url('').'/public/assets/default_image/No_image_deal.png';
                  $img_data   = "public/assets/deals/".$product_image; @endphp
                  @if(file_exists($img_data) && $product_image !='')   
                  @php $prod_path = url('').'/public/assets/deals/' .$product_image;  @endphp                
                  @else  
                  @if(isset($DynamicNoImage['dealImg']))
                  @php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp
                  @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))
                  @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
                  @endif
                  @endif    
              <div class="large-image"> <a href="{{ $prod_path }}" title="{{ $alt_text }}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="{!! $prod_path !!}" alt="Deals"> </a> </div>
              <div class="flexslider flexslider-thumb">
                <ul class="previews-list slides">
				 @for($i=0;$i < $img_count;$i++)  
				   @php $product_image     = $product_img[$i];
				   $prod_path  = url('').'/public/assets/default_image/No_image_deal.png';
				   $img_data   = "public/assets/deals/".$product_image; @endphp
				   @if(file_exists($img_data) && $product_image !='')   
				   @php $prod_path = url('').'/public/assets/deals/' .$product_image; @endphp                
				   @else  
				   @if(isset($DynamicNoImage['dealImg']))
				   @php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp
				   @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))
				   @php    $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
				   @endif
				   @endif 
				   
                 <li style="width: 100px; float: left; display: block;"><a href="{{ $prod_path }}" class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: {{ $prod_path }} "><img src="{{ $prod_path }}" alt = "{{ $alt_text }}"/></a></li>
				 
		  
				   @endfor
                </ul>
              </div>
              
              <!-- end: more-images --> 
              
            </div>
				   <h2> 
                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                     @php $deal_title = 'deal_title'; @endphp
                     @else @php  $deal_title = 'deal_title_'.Session::get('lang_code'); @endphp @endif
                    
                  </h2>
				  @php
                  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                  $multiple_countone = $one_count *1;
                  $multiple_counttwo = $two_count *2;
                  $multiple_countthree = $three_count *3;
                  $multiple_countfour = $four_count *4;
                  $multiple_countfive = $five_count *5;
                  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp
            <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
              <div class="product-name">
                <h1>{{ $pro_details_by_id->$deal_title }}</h1>
              </div>
              <div class="price-box">
                <p class="special-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $pro_details_by_id->deal_discount_price }} </span> </p>
                <p class="old-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $pro_details_by_id->deal_original_price }}</span> </p>
				@if($pro_details_by_id->deal_discount_percentage!='') 
				<span class="special-price">(<?php echo round($pro_details_by_id->deal_discount_percentage);?>% off)</span>
				@endif
              </div>
              <div class="ratings">
                <div class="rating"> 
				@if($product_count)
                     @php   $product_divide_count = $product_total_count / $product_count; @endphp
                     @if($product_divide_count <= '1') 
                     @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }}  @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif
                     <img src="{{ url('./images/stars-1.png') }}" style='margin-bottom:10px;'> 
                     @elseif($product_divide_count >= '1') 
                     @if(Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{ trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif
                     <img src="{{ url('./images/stars-1.png')}}" style='margin-bottom:10px;'> 
                     @elseif($product_divide_count >= '2') 
                     @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif
                     <img src="{{ url('./images/stars-2.png') }}" style='margin-bottom:10px;'>  
                     @elseif($product_divide_count >= '3') 
                     @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif
                     <img src="{{ url('./images/stars-3.png') }}" style='margin-bottom:10px;'>
                     @elseif($product_divide_count >= '4') 
                     @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif
                     <img src="{{ url('./images/stars-4.png') }}" style='margin-bottom:10px;'> 
                     @elseif($product_divide_count >= '5') 
                     @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{ trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif
                     <img src="{{ url('./images/stars-5.png') }}" style='margin-bottom:10px;'>
                     @else
						 
                     @if (Lang::has(Session::get('lang_file').'.NO_RATING_DEALS')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_DEALS')}}  
                     @else {{ trans($OUR_LANGUAGE.'.NO_RATING_DEALS') }} @endif
					 
                     @endif
                     @elseif($product_count)
                     @php $product_divide_count = $product_total_count / $product_count; @endphp
                     @else 
					 
					 @if (Lang::has(Session::get('lang_file').'.NO_RATING_DEALS')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_DEALS')}}  
                     @else {{ trans($OUR_LANGUAGE.'.NO_RATING_DEALS') }} @endif
					 
					 @endif
                     <p class="rating-links"> ({{ $count_review_rating }} Review & Ratings ) </strong></span>
				</div>
                <p class="availability in-stock pull-right">@if (Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= '') {{ trans(Session::get('lang_file').'.AVAILABLE_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE_STOCK') }} @endif : <span>{{  ($pro_details_by_id->deal_max_limit)-($pro_details_by_id->deal_no_of_purchase) }} @if (Lang::has(Session::get('lang_file').'.IN_STOCK')!= '') {{ trans(Session::get('lang_file').'.IN_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.IN_STOCK') }} @endif</span></p>
              </div>
              <div class="short-description">
                <h2>@if (Lang::has(Session::get('lang_file').'.OVERVIEW')!= '') {{ trans(Session::get('lang_file').'.OVERVIEW') }} @else {{ trans($OUR_LANGUAGE.'.OVERVIEW') }} @endif</h2>
                <p>{!! html_entity_decode(substr($pro_details_by_id->deal_description,0,200)) !!}</p>
              </div>
              @if($GENERAL_SETTING->gs_store_status != 'Store')
               <div class="store-name">

                <h5>@if (Lang::has(Session::get('lang_file').'.STORE_NAME')!= '') {{ trans(Session::get('lang_file').'.STORE_NAME') }} @else {{ trans($OUR_LANGUAGE.'.STORE_NAME') }} @endif</h5>

                <p>
                    @foreach($get_store as $storerow)

                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                    @php  $stor_name = 'stor_name';  @endphp

                    @else   

                    @php  $stor_name = 'stor_name_'.Session::get('lang_code'); @endphp 

                    @endif
                    {{ $storerow->$stor_name }}
                    @endforeach
                </p>

              </div> 
              @else

              @endif
			  <div class="product-color-size-area">
                <div class="color-area">
                  <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.DEAL_ENDS_IN')!= '') {{  trans(Session::get('lang_file').'.DEAL_ENDS_IN') }} @else {{ trans($OUR_LANGUAGE.'.DEAL_ENDS_IN') }} @endif</h2>
                  <div class="color">
                    <p id="demo" class="stock-list"></p>
                  </div>
                </div>
              </div>
			  
			  <div class="product-variation">
                {!! Form::open(array('url' => 'addtocart_deal','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}
                @if($count > 0)
                  <div class="cart-plus-minus">
                    <label for="qty">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.QUANTITY') }} @endif:</label>
                    <div class="numbers-row">
                    
                      <div onClick="remove_quantity()" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                      <input type="number" class="qty" min="1" value="1" max="{{  ($pro_details_by_id->deal_max_limit - $pro_details_by_id->deal_no_of_purchase) }}" id="addtocart_qty" name="addtocart_qty" readonly required >
                      {{ Form::hidden('addtocart_deal_id',$pro_details_by_id->deal_id)}}
                      {{ Form::hidden('addtocart_type','deals')}}
                      <div onClick="add_quantity()" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                    </div>
                 </div>
                 @endif
                    <input type="hidden" name="return_url" value="<?php echo $pro_details_by_id->mc_name.'/'.base64_encode(base64_encode(base64_encode($pro_details_by_id->deal_category))); ?>" />
                     @if(Session::has('customerid')) 
                           @if($count > 0)

					  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i>  @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{  trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button>
					  @else 
					  <button  class="btn btn-danger" type="button"><span>  @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif</span></button>
					  @endif
					  @else
					  <a href="" role="button" data-toggle="modal" data-target="#loginpop">
					  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i>  @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{  trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button></a>
					  @endif
					   <input type="hidden" id="enddate" name="enddate" value="{{ $pro_details_by_id->deal_end_date }}">
					<input type="hidden" id="enddate_format" name="enddate_format" value="<?php echo date('F j, Y, G:i:s',strtotime($pro_details_by_id->deal_end_date)); ?>">
				   {!! Form::close() !!}
              </div>
			  
			  
			 
             
			   @php $delivery_date = '+'.$pro_details_by_id->deal_delivery.'Days'; @endphp
              <div class="pro-tags">
                   <div class="pro-tags-title"><p style="float: left; padding-right: 5px;">@if (Lang::has(Session::get('lang_file').'.DELIVERY_WITH_IN')!= '') {{  trans(Session::get('lang_file').'.DELIVERY_WITH_IN') }} @else {{ trans($OUR_LANGUAGE.'.DELIVERY_WITH_IN') }} @endif:</p>
				    <p style="float: left;"> {{ date('D d, M Y', strtotime($delivery_date)) }}</p> </div>
				    
				   <!-- 
				   <div class="pro-tags">
                     <div class="pro-tags-inner">
                        <div class="pro-tags-title">@if (Lang::has(Session::get('lang_file').'.SHARE_THIS')!= '') {{ trans(Session::get('lang_file').'.SHARE_THIS') }} @else {{ trans($OUR_LANGUAGE.'.SHARE_THIS') }} @endif:</div>
						
						
                       <a  target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}">
							<img width="32px" height="30px" src="{{url('')}}/public/themes/image/Socialmedia/fb-icon.jpg" title="share in facebook">
						</a>
						
						<a href="https://twitter.com/home?status={{Request::url()}}')">
							<img width="32px" height="30px" src="{{url('')}}/public/themes/image/Socialmedia/tw-icon.png" title="share in twitter">
						</a>
						
                     </div>
                  </div>-->
				  
					
			@if($pro_details_by_id->allow_cancel=='1' || $pro_details_by_id->allow_return=='1' || $pro_details_by_id->allow_replace=='1')
              <div class="share-box">
                <div class="title"><p>@if (Lang::has(Session::get('lang_file').'.POLICY_DETAILS')!= '') {{  trans(Session::get('lang_file').'.POLICY_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.POLICY_DETAILS') }} @endif</p></div>
                <div class="socials-box">
				    @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
               @php    $cancel_policy  = 'cancel_policy';
               $return_policy  = 'return_policy';
               $replace_policy = 'replace_policy'; @endphp
               @else   
               @php    $cancel_policy  = 'cancel_policy_'.Session::get('lang_code'); 
               $return_policy  = 'return_policy_'.Session::get('lang_code'); 
               $replace_policy = 'replace_policy_'.Session::get('lang_code'); @endphp
               @endif
               <div class="cancel-policy">
                  @if($pro_details_by_id->allow_cancel=='1')
                  <p class="policy-title"> @if($pro_details_by_id->cancel_days>0) 
                     {{ $pro_details_by_id->cancel_days }}
                     @if($pro_details_by_id->cancel_days>1) 
                     {{ (Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS') }}
                     @else 
                     {{ (Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY') }}
                     @endif
                     @if (Lang::has(Session::get('lang_file').'.CANCELLATION_ONLY')!= '') {{  trans(Session::get('lang_file').'.CANCELLATION_ONLY') }}  @else {{ trans($OUR_LANGUAGE.'.CANCELLATION_ONLY') }} @endif
                     <span class="policy-quest" id="policyclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                     <br>
                     @if($pro_details_by_id->$cancel_policy!='')
                  <div class="policy-container dev_cancel" style="display: none;"> 
                     {!! $pro_details_by_id->$cancel_policy !!}    
                  </div>
                  @endif    
                  @endif
                  </p>
                  @endif
                  @if($pro_details_by_id->allow_return=='1')
                  <p class="policy-title"> @if($pro_details_by_id->return_days>0) 
                     {{ $pro_details_by_id->return_days }}
                     @if($pro_details_by_id->return_days>1) 
                     {{ (Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS') }}
                     @else 
                      {{ (Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY') }}
                     @endif
                     @if (Lang::has(Session::get('lang_file').'.RETURN_ONLY')!= '') {{ trans(Session::get('lang_file').'.RETURN_ONLY') }} @else {{ trans($OUR_LANGUAGE.'.RETURN_ONLY') }} @endif
                     <span class="policy-quest" id="returnclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                     <br>
                     @if($pro_details_by_id->$return_policy!='')
                  <div class="policy-container dev_return" style="display: none;"> 
                     {!! $pro_details_by_id->$return_policy !!}    
                  </div>
                  @endif        
                  @endif
                  </p>
                  @endif 
                  @if($pro_details_by_id->allow_replace=='1')
                  <p class="policy-title"> @if($pro_details_by_id->replace_days>0) 
                     {{ $pro_details_by_id->replace_days }}
                     @if($pro_details_by_id->replace_days>1) 
                      {{ (Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS') }}
                     @else 
                      {{ (Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY') }}
                     @endif
                     @if (Lang::has(Session::get('lang_file').'.REPLACEMENT_ONLY')!= '') {{ trans(Session::get('lang_file').'.REPLACEMENT_ONLY') }}  @else {{ trans($OUR_LANGUAGE.'.REPLACEMENT_ONLY') }} @endif
                     <span class="policy-quest" id="replaceclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                     <br>
                     @if($pro_details_by_id->$replace_policy!='')
                  <div class="policy-container dev_replace" style="display: none;"> 
                     {!! $pro_details_by_id->$replace_policy !!}    
                  </div>
                  @endif        
                  @endif
                  </p>
                  @endif    
               </div>
                  </div>
              </div>
			@endif
            </div>
          </div>
        </div>
        <div class="product-overview-tab">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="product-tab-inner">
                  <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                    <li class="active"> <a href="#description" data-toggle="tab">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{ trans(Session::get('lang_file').'.DESCRIPTION') }}  @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif</a> </li>
                    <li> <a href="#reviews"  data-toggle="tab">@if (Lang::has(Session::get('lang_file').'.REVIEWS')!= '') {{ trans(Session::get('lang_file').'.REVIEWS') }}  @else {{ trans($OUR_LANGUAGE.'.REVIEWS') }} @endif</a> </li>
                  
                  </ul>
                  <div id="productTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="description" style="max-height: 300px;overflow-y: auto;">
                      <div class="std">
                        <p>{!! $pro_details_by_id->deal_description !!}</p>
                      </div>
                    </div>
                    <div id="reviews" class="tab-pane fade">
                      <div class="col-sm-5 col-lg-5 col-md-5">
                        <div class="reviews-content-left">
                          <h2>@if (Lang::has(Session::get('lang_file').'.CUST_REVIEW')!= '') {{ trans(Session::get('lang_file').'.CUST_REVIEW') }}  @else {{ trans($OUR_LANGUAGE.'.CUST_REVIEW') }} @endif</h2>
						   @if(count($review_details)!=0)
							@php $r=0; @endphp
							@foreach($review_details as $col)
							@php    $r++; 
							$customer_name = $col->cus_name;
							$customer_mail = $col->cus_email;
							$customer_img = $col->cus_pic;
							$customer_comments = $col->comments;
							$customer_date = $col->review_date;
							$customer_product = $col->deal_id; 
							$comment_date = date('F j, Y', strtotime($col->review_date) );
							$customer_title = $col->title;
							$customer_name_arr = str_split($customer_name);
							$start_letter = 's';//strtolower($customer_name_arr[0]);
							$customer_ratings = $col->ratings; @endphp
                          <div class="review-ratting">
                            <p><a>{{ $customer_title }}</a> by {{ $customer_name }}</p>
                            <table>
                              <tbody>
                                <tr>
                                  <th>@if (Lang::has(Session::get('lang_file').'.RATING')!= '') {{ trans(Session::get('lang_file').'.RATING') }}  @else {{ trans($OUR_LANGUAGE.'.RATING') }} @endif</th>
                                  <td>
								  
								   <div class="rating">
                                     @if($customer_ratings=='1')
								  <img src='{{ url('./images/stars-1.png')}}' style='margin-bottom:10px;'>
								  @elseif($customer_ratings=='2')
								  <img src='{{ url('./images/stars-2.png') }}' style='margin-bottom:10px;'>
								  @elseif($customer_ratings=='3')
								  <img src='{{ url('./images/stars-3.png') }}' style='margin-bottom:10px;'>
								  @elseif($customer_ratings=='4')
								  <img src='{{ url('./images/stars-4.png') }}' style='margin-bottom:10px;'>
								  @elseif($customer_ratings=='5')
								  <img src='{{ url('./images/stars-5.png') }}' style='margin-bottom:10px;'>
								   @endif   </div>
												   </td>
												</tr>
												
											  </tbody>
											</table>
											<p class="author"> {{ $customer_comments }}<small> (Posted on {{ $comment_date }})</small> </p>
										  </div>
										   @endforeach
										   @else
										 @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT') }} 
										 @endif @endif	  
								 </div>
							</div>
								 
								 <!---write review-->
								 <div class="col-sm-7 col-lg-7 col-md-7">
                        <div class="reviews-content-right">
                          <h2>@if(Lang::has(Session::get('lang_file').'.WRITE_A_REVIEW')!= '') {{  trans(Session::get('lang_file').'.WRITE_A_REVIEW') }} @else {{ trans($OUR_LANGUAGE.'.WRITE_A_REVIEW') }} @endif</h2>
                         {!! Form::open(array('url'=>'dealcomments','class'=>'form-horizontal loginFrm')) !!}
                           {{--  <h3>You're reviewing: <span>Donec Ac Tempus</span></h3> --}}
                            <h4>@if(Lang::has(Session::get('lang_file').'.RATE_FOR_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.RATE_FOR_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.RATE_FOR_PRODUCT') }} @endif<em>*</em></h4>
                             <input type="hidden" name="customer_id" value="{{ Session::get('customerid') }}">
							  <input type="hidden" name="deal_id" value="{!!$pro_details_by_id->deal_id!!}">
							  <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="table-responsive reviews-table">
                             <fieldset class="rating disableRating addReviews">
							  <input id="review_ratings_5" name="ratings" type="radio" value="5" > 
							  <label for="review_ratings_5" title="{{ (Lang::has(Session::get('lang_file').'.REVIEW_AWESOME')!= '') ? trans(Session::get('lang_file').'.REVIEW_AWESOME') : trans($OUR_LANGUAGE.'.REVIEW_AWESOME') }}"> </label>
							  <input id="review_ratings_4" name="ratings" type="radio" value="4" > 
							  <label for="review_ratings_4" title="{{ (Lang::has(Session::get('lang_file').'.REVIEW_PRETTY_GOOD')!= '') ? trans(Session::get('lang_file').'.REVIEW_PRETTY_GOOD') : trans($OUR_LANGUAGE.'.REVIEW_PRETTY_GOOD') }}"> </label>
							  <input id="review_ratings_3" name="ratings" type="radio" value="3" > 
							  <label for="review_ratings_3" title="{{ (Lang::has(Session::get('lang_file').'.REVIEW_NOT_BAD')!= '') ? trans(Session::get('lang_file').'.REVIEW_NOT_BAD') : trans($OUR_LANGUAGE.'.REVIEW_NOT_BAD') }}"> </label>
							  <input id="review_ratings_2" name="ratings" type="radio" value="2" > 
							  <label for="review_ratings_2" title="{{ (Lang::has(Session::get('lang_file').'.REVIEW_BAD2')!= '') ? trans(Session::get('lang_file').'.REVIEW_BAD2') : trans($OUR_LANGUAGE.'.REVIEW_BAD2') }}"> </label>
							  <input id="review_ratings_1" name="ratings" type="radio" value="1" > 
							  <label for="review_ratings_1" title="{{ (Lang::has(Session::get('lang_file').'.REVIEW_VERY_BAD')!= '') ? trans(Session::get('lang_file').'.REVIEW_VERY_BAD') : trans($OUR_LANGUAGE.'.REVIEW_VERY_BAD') }}"> </label>
						   </fieldset>
                            </div>
                            <div class="form-area">
                             
                              <div class="form-element">
                                <label>@if (Lang::has(Session::get('lang_file').'.REVIEW_SUMMARY')!= '') {{  trans(Session::get('lang_file').'.REVIEW_SUMMARY') }} @else {{ trans($OUR_LANGUAGE.'.REVIEW_SUMMARY') }} @endif<em>*</em></label>
                                <input type="text" name="title" value="" placeholder="@if (Lang::has(Session::get('lang_file').'.REVIEW_TITLE')!= '') {{  trans(Session::get('lang_file').'.REVIEW_TITLE') }} @else {{ trans($OUR_LANGUAGE.'.REVIEW_TITLE') }} @endif" required>
                              </div>
                              <div class="form-element">
                                <label>@if (Lang::has(Session::get('lang_file').'.REVIEWS')!= '') {{ trans(Session::get('lang_file').'.REVIEWS') }}  @else {{ trans($OUR_LANGUAGE.'.REVIEWS') }} @endif <em>*</em></label>
                                <textarea name="comments" placeholder="@if (Lang::has(Session::get('lang_file').'.REVIEW_DESCRIPTION')!= '') {{ trans(Session::get('lang_file').'.REVIEW_DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.REVIEW_DESCRIPTION') }} @endif" required></textarea>
                              </div>
                              <div class="buttons-set">
							  
							  
                                @if(Session::has('customerid'))
                                <button class="button submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i>@if (Lang::has(Session::get('lang_file').'.SUBMIT_REVIEW')!= '') {{  trans(Session::get('lang_file').'.SUBMIT_REVIEW') }} @else {{ trans($OUR_LANGUAGE.'.SUBMIT_REVIEW') }} @endif</span></button>
							
							   @else 
                                <a href="" role="button" data-toggle="modal" data-target="#loginpop"><button class="button" title="Submit Review" type="button"><span><i class="fa fa-thumbs-up"></i>@if (Lang::has(Session::get('lang_file').'.SUBMIT_REVIEW')!= '') {{  trans(Session::get('lang_file').'.SUBMIT_REVIEW') }} @else {{ trans($OUR_LANGUAGE.'.SUBMIT_REVIEW') }} @endif</span></button></a>
							  @endif
							
							
                              </div>
                            </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
								 
								 <!--end write review-->
								 
								
                          </div>
						  
                        </div>
                      </div>
                        
                    </div>
					
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Main Container End --> 
   <div class="container">
  <!-- Related Product Slider -->
    @if(count($get_related_product)!=0)  
     @php   $i=0; @endphp
 
    <div class="row">
      <div class="col-xs-12">
        <div class="related-product-area">
          <div class="page-header">
            <h2>@if (Lang::has(Session::get('lang_file').'.RELATED_PRODUCTS')!= '') {{ trans(Session::get('lang_file').'.RELATED_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.RELATED_PRODUCTS') }} @endif</h2>
          </div>
          <div class="related-products-pro">
            <div class="slider-items-products">
              <div id="related-product-slider" class="product-flexslider hidden-buttons">
                <div class="re-pr-slider owl-carousel owl-theme fadeInUp">
				
				 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                  @php   $deal_title = 'deal_title'; @endphp
                  @else @php  $deal_title = 'deal_title_'.Session::get('lang_code'); @endphp @endif
                  @foreach($get_related_product as $product_det) 
                  @php  $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                  $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                  $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                  $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
                  $res = base64_encode($product_det->deal_id);
                  $product_image = explode('/**/',$product_det->deal_image);
                  $product_saving_price = $product_det->deal_original_price - $product_det->deal_discount_price;
                  $product_discount_percentage = round(($product_saving_price/ $product_det->deal_original_price)*100,2);
                  $product_img     = $product_image[0]; 
                  $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                  $img_data   = "public/assets/deals/".$product_img; @endphp
                  @if(file_exists($img_data) && $product_img !='')   
                  @php $prod_path = url('').'/public/assets/deals/' .$product_img;  @endphp                
                  @else  
                  @if(isset($DynamicNoImage['dealImg']))
                  @php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp
                  @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))
                  @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
                  @endif
                  @endif   
                  @php  $alt_text   = substr($product_det->$deal_title,0,25);
                  $alt_text  .= strlen($product_det->$deal_title)>25?'..':''; @endphp
                  @if($product_det->deal_no_of_purchase < $product_det->deal_max_limit) 
                  @php    $i++;
                  $redirect_url ='';     @endphp                 
                  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  
                  @php    $redirect_url =  url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; @endphp
                  @endif 
                  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')  
                  @php   $redirect_url =  url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res ; @endphp
                  @endif 
                  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                  @php     $redirect_url =  url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; @endphp
                  @endif
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="pr-img-area"> <a href="{{ $redirect_url }}">
                          <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}">
						  <!--<img class="hover-img" src="images/products/product-6.jpg" alt="HTML template">--></figure>
                          </a> </div>
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title"> <a title="Product title here" href="{{ $redirect_url }}">{{ substr ($product_det->$deal_title,0,20)}}</a> </div>
                          <div class="item-content">
                            <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->deal_discount_price }}</span> </span> </div>
                            </div>
                           <!-- <div class="pro-action">
                              <button type="button" class="add-to-cart"><span> Add to Cart</span> </button>
                            </div>-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif 
                  @endforeach
				  
				  @if($i==0){{ "No Related Products Available." }} 
               @else 
				  @endif
			  
			   @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
  <!-- Related Product Slider End --> 
    <div class="container deal-view-page">
  <!-- Store section -->
      @php $store_lat ='';$store_lan =''; @endphp
            @if($GENERAL_SETTING->gs_store_status == 'Store')
              <div class="store-details">
                <h5 class="page-header" style="font-size: 18px;font-weight: 600;">@if (Lang::has(Session::get('lang_file').'.STORE_DETAILS')!= '') {{  trans(Session::get('lang_file').'.STORE_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.STORE_DETAILS') }} @endif</h5>


             @php $store_lat ='';$store_lan =''; @endphp
                              @foreach($get_store as $storerow)
                              @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                              @php  $stor_name = 'stor_name';
                              $stor_address1 = 'stor_address1';
                              $stor_address2 = 'stor_address2'; @endphp
                              @else   
                              @php  $stor_name = 'stor_name_'.Session::get('lang_code'); 
                              $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 
                              $stor_address2 = 'stor_address2_'.Session::get('lang_code'); @endphp
                              @endif
                              @php $store_name = $storerow->$stor_name;
                              $store_address = $storerow->$stor_address1;
                              $store_address2 = $storerow->$stor_address2;
                              $store_zip = $storerow->stor_zipcode;
                              $store_phone = $storerow->stor_phone;
                              $store_web = $storerow->stor_website;
                              $store_lat = $storerow->stor_latitude;
                              $store_lan = $storerow->stor_longitude; 
                              $store_image = $storerow->stor_img; 
                              $prod_path  = url('').'/public/assets/default_image/No_image_store.png';
                              $img_data   = "public/assets/storeimage/".$store_image; @endphp
                              @if(file_exists($img_data) && $store_image !='')   
                              @php $prod_path = url('').'/public/assets/storeimage/' .$store_image; @endphp                  
                              @else  
                              @if(isset($DynamicNoImage['store']))
                              @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['store']; @endphp
                              @if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))
                              @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
                              @endif
                              @endif   
                              @php $alt_text   = $store_name; @endphp
               <div class="main container">
             
                 
                  <div class="row">
                    <div class="col-xs-12 col-sm-6"> 
                      
                      <h1 style="font-weight: 600;font-size: 22px;"> <span style="color: #e83f33;">{{ $store_name }}</span></h1>
                      <p> <a title="View Store" target="_blank" href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($storerow->stor_id)))); ?>">
                  <img src="{{ $prod_path }}" alt="{{ $alt_text }}" style="width:70px; height:70px;">
                </a> </p>
                <ul style="color: #999;  font-size: 15px; list-style-type:none;display: block;  margin: 1.2em 0 0;">
                        <li>{{ $store_address }},</li>
                        <li>{{ $store_address2 }},</li>
                        <li>{{ $store_zip }}</li>
                        <li>@if (Lang::has(Session::get('lang_file').'.MOBILE')!= '') {{  trans(Session::get('lang_file').'.MOBILE') }} @else {{ trans($OUR_LANGUAGE.'.MOBILE') }} @endif : {{ $store_phone }}</li>
                        <li>@if (Lang::has(Session::get('lang_file').'.WEBSITE')!= '') {{ trans(Session::get('lang_file').'.WEBSITE') }} @else {{ trans($OUR_LANGUAGE.'.WEBSITE') }} @endif : {{ $store_web }}</li>
                      </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="">
                        <div > 
                          
                          <!-- Wrapper for slides -->
                          <div >
                            <div id="us3" style="width: 100% !important; height: 240px;margin-bottom:10px;background-size: cover;  text-align: center;  padding: 80px 0 100px;"> </div>
                           </div>
                        </div>
                      </div>
                    
                  </div>
                  </div>

              </div>
            @endforeach

            </div> 
            @else

            @endif
  <!-- service section -->
   @include('service_section')
  
</div>
</body>

{!! $footer !!}
<a href="" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>

 <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
      <script src="<?php echo url(''); ?>/public/assets/js/locationpicker.jquery.js"></script>
      <script>
         $('#us3').locationpicker({
                    
                 location: {latitude: <?php echo $store_lat; ?>, longitude: <?php echo $store_lan; ?>},
                    
                 radius: 200,
                 inputBinding: {
                     latitudeInput: $('#us3-lat'),
                     longitudeInput: $('#us3-lon'),
                     radiusInput: $('#us3-radius'),
                     locationNameInput: $('#us3-address')
                 },
                 enableAutocomplete: true,
                 onchanged: function (currentLocation, radius, isMarkerDropped) {
                     // Uncomment line below to show alert on each Location Changed event
                     //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                 }
             });
      </script>
	  
	  <script>
function add_quantity()
{
  //alert();
  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
  var quantity=$("#addtocart_qty").val();
  var remaining_product=parseInt(<?php echo  $pro_details_by_id->deal_max_limit-$pro_details_by_id->deal_no_of_purchase;?>);
  if(quantity<remaining_product)
  {
    var new_quantity=parseInt(quantity)+1;
    $("#addtocart_qty").val(new_quantity);
  }
  //alert();
}

function remove_quantity()
{
  //alert();
  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/

  var quantity=$("#addtocart_qty").val();
  var quantity=parseInt(quantity);
  if(quantity>1)
  {
    var new_quantity=quantity-1;
    $("#addtocart_qty").val(new_quantity);
  }
  //alert();
}
</script>

<script>
// Set the date we're counting down to

var getenddate = document.getElementById("enddate").value;
var getenddate_format = document.getElementById("enddate_format").value;
var countDownDate = new Date(getenddate_format).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML = days + "<span style='color:#e74c3c;'>d</span> -" + hours + "<span style='color:#e74c3c;'>h </span>-"
  + minutes + "<span style='color:#e74c3c;'>m</span> -" + seconds + "<span style='color:#e74c3c;'>s</span> ";

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>

<script>
  $('.re-pr-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: false,
                dots: false,
                 <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>               
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 3
                  },
                  992: {
                    items: 4
                  },
                  1200: {
                    items: 4
                  }
                }
     })
</script>

</html>