<meta charset="utf-8">
<?php

if ($metadetails) {
        foreach ($metadetails as $metainfo) {
            if(Session::get('lang_code') == '' || Session::get('lang_code') == 'en')
            {
                $metaname = $metainfo->gs_metatitle;
                $metakeywords = $metainfo->gs_metakeywords;
                $metadesc = $metainfo->gs_metadesc;
            }
            else
            {
                $get_lang_title = 'gs_metatitle_'.Session::get('lang_code');
                $get_lang_keywords = 'gs_metakeywords_'.Session::get('lang_code');
                $get_lang_desc = 'gs_metadesc_'.Session::get('lang_code');
                $metaname = $metainfo->$get_lang_title;
                $metakeywords = $metainfo->$get_lang_keywords;
                $metadesc = $metainfo->$get_lang_desc;
            }
        }

    }
         else {
            $metaname = "";
            $metakeywords = "";
            $metadesc = "";
        }
?>
<title><?php echo $metaname;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $metadesc;?>">
<meta name="keywords" content="<?php echo $metakeywords;?>">
<meta name="author" content="<?php echo $metaname;?>">
<meta name="_token" content="{!! csrf_token() !!}"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
        });
    });
</script>
<!-- Bootstrap style -->
<!-- CSS Style -->
<link href="<?php echo url(''); ?>/public/themes/css/bootstrap.min.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/simple-line-icons.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/font-awesome.min.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/jquery-ui.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/owl.carousel.min.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/owl.theme.default.min.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/owl.transitions.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/pe-icon-7-stroke.min.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/animate.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/blog.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/flexslider.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/quick_view_popup.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/revolution-slider.css" rel="stylesheet"/>
<link href="<?php echo url(''); ?>/public/themes/css/shortcode.css" rel="stylesheet"/>






<link href="<?php echo url(''); ?>/public/themes/css/responsive.css" rel="stylesheet"/>
 <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); 
     foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/<?php echo $fav->imgs_name; ?>"> 
@if(Session::has('customerid') == 1)
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
      integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
@endif

@if(Session::get('lang_code') == '' || Session::get('lang_code') == 'ar')
<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/style-arabic.css">
<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/bootstrap-rtl.css">  
@else
<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/style.css">
@endif
 


