 @if(isset($err_msge))
    @if($err_msge!='')
        <div class="alert-box success" style="position: absolute;left: 50%; background: #d82672; color: #ffffff; padding: 5px 30px; border-radius: 3px;top: 5px;" > {{ $err_msge }} </div>
    @endif
@endif
<div class="alert-box success" id="success" style="display:none; ">@if (Lang::has(Session::get('lang_file').'.SUCCESSFULLY_SUBSCRIBED')!= '') {{  trans(Session::get('lang_file').'.SUCCESSFULLY_SUBSCRIBED') }} @else {{ trans($OUR_LANGUAGE.'.SUCCESSFULLY_SUBSCRIBED') }} @endif!!!</div>
 
 <style>
.popup1, .popup2{
  vertical-align: top;
  background: #fff;

    box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
    display: none;
    text-align: left;
    z-index: 3000;
    padding: 10px 20px;
    font-size: 13px;
    margin-left: 21px;
    margin-top: -11px;
}

 .popup1, .popup1, .popup2{
	 display: none;
	 background : #fff;
 }
 .popup li.active .popup1{
	display: block; 
 }
.popup1:hover .popup2{
		display:block;
}	
 </style>
<!--  <link rel="stylesheet" href="{{ url('')}}/public/themes/css/style.css"> -->



      <!-- header inner -->
      <div class="header-inner">
        <div class="container">
          <div class="row">
            <div class="col-sm-3 col-xs-12 jtv-logo-block"> 
              
              <!-- Header Logo -->
              <div class="logo" style="padding: 10px; margin: 0;"> <a href="{{ url('index') }}"><img style="height: 80px;" src="{{ $SITE_LOGO }}" alt="@if (Lang::has(Session::get('lang_file').'.LOGO')!= '') {{  trans(Session::get('lang_file').'.LOGO') }} @else {{ trans($OUR_LANGUAGE.'.LOGO') }} @endif"/></a> </div>
			  
            </div>
            <div class="col-xs-12 col-sm-5 col-md-6 jtv-top-search"> 
              
              <!-- Search -->
              <?php 
                $header_category = DB::table('nm_maincategory')->where('mc_status', '=', 1)->get(); 
               
                ?>
              <div class="top-search">
                <div id="search">
               <form action="{!!action('HomeController@searching')!!}" class="form-inline navbar-search searBoxStyle">  
                    <div class="input-group">
                      <select class="cate-dropdown hidden-xs hidden-sm" name="category">

                @if(count($header_category)>0)
                <option value="0">{{ (Lang::has(Session::get('lang_file').'.ALL_CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.ALL_CATEGORIES') : trans($OUR_LANGUAGE.'.ALL_CATEGORIES')}}</option>
                <?php 
               // if(count($header_category)>0){ ?>
                    @foreach ($header_category as $main_cat) 
                        <?php
                        $main_cat_result = DB::table('nm_secmaincategory')->where('smc_status', '=', 1)->where('smc_mc_id', '=', $main_cat->mc_id)->get();
                            ?>
                        @if ($main_cat_result) 
                            <?php $sub_main_category[$main_cat->mc_id] = $main_cat_result; ?>
                        @else 
                          <?php   $sub_main_category[$main_cat->mc_id] = Array(); ?>
                        @endif
                    @endforeach
               @else  <option value="0">{{ (Lang::has(Session::get('lang_file').'.NO_CATEGORY_FOUND')!= '') ?  trans(Session::get('lang_file').'.NO_CATEGORY_FOUND') : trans($OUR_LANGUAGE.'.NO_CATEGORY_FOUND') }} </option>  @endif
                
                @foreach($header_category as $category_list)  
                        @if(count($sub_main_category[$category_list->mc_id])> 0)
                
                <option value="<?php echo base64_encode($category_list->mc_id); ?>" <?php if(isset($cid)){if($category_list->mc_id == $cid){ echo "selected";} }?>>
                    <?php 
                if((Session::get('lang_code'))== '' || (Session::get('lang_code')) == 'en' ) { 
                        $mc_name = 'mc_name';
                }else {  $mc_name = 'mc_name_'.Session::get('lang_code'); }
                    echo $category_list->$mc_name; ?></option>
                    @endif
                      @endforeach 
                      </select>
              
					   <input type="text" id="searchbox" value="<?php if(isset($search_text)){ echo $search_text; }?>" placeholder="{{ (Lang::has(Session::get('lang_file').'.SEARCH_PRODUCT_NAME')!= '') ?  trans(Session::get('lang_file').'.SEARCH_PRODUCT_NAME') : trans($OUR_LANGUAGE.'.SEARCH_PRODUCT_NAME') }}" autocomplete="on" style="font-family:lato !important;border-radius: 0px; float: left;" name="q" class="form-control"/>
					   
                      <button class="btn-search" name="submit" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                 {{ Form::close() }}
                </div>
              </div>
              
              <!-- End Search --> 
              
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 top-cart">
              <div class="link-wishlist head-wishlist"> 
                @if(Session::has('customerid'))
                <a href="{{ url('wishlist')}}"> <i class="icon-heart icons"></i><span> @if (Lang::has(Session::get('lang_file').'.WISHLIST')!= '') {{ trans(Session::get('lang_file').'.WISHLIST') }}  @else {{ trans($OUR_LANGUAGE.'.WISHLIST') }} @endif
</span></a> 
  @else
  <a href="" role="button" data-toggle="modal" data-target="#loginpop"><i class="icon-heart icons"></i><span> @if (Lang::has(Session::get('lang_file').'.WISHLIST')!= '') {{ trans(Session::get('lang_file').'.WISHLIST') }}  @else {{ trans($OUR_LANGUAGE.'.WISHLIST') }} @endif
    
</span></a> 
@endif
</div>
              <!-- top cart -->
              <div class="top-cart-contain">
                <div class="mini-cart">
                  <!-- removed data-toggle="dropdown"  -->
                  <div data-hover="dropdown" class="basket dropdown-toggle"> 
                    @if(Session::has('customerid'))
                    <a href="<?php echo url('addtocart'); ?>">
                    <div class="cart-icon"><i class="icon-basket-loaded icons"></i><span class="cart-total">
					 @if(isset($_SESSION['cart']))
						<?php   
						$item_count_header1 = count($_SESSION['cart']); 
						?>
					@else 
					 <?php 
						$item_count_header1 = 0; 
						?>
						@endif          
						@if(isset($_SESSION['deal_cart']))
							<?php    
								$item_count_header2 = count($_SESSION['deal_cart']); 
						?>
						@else 
						 <?php 
							$item_count_header2 = 0; 
							?>
							@endif       
						   <?php   $item_count_header = $item_count_header1 + $item_count_header2;   ?>
						@if($item_count_header != 0) 
						 <?php 
						 echo $item_count_header; 
						 ?>
						 @else 
						  <?php 
							echo 0; 
								
							?>
							@endif 		
					</span></div></a>
          @else
          <a href="" role="button" data-toggle="modal" data-target="#loginpop"><div class="cart-icon"><i class="icon-basket-loaded icons"></i></div>
          </a>
          @endif
                    <div class="shoppingcart-inner hidden-xs"> 
                       @if(Session::has('customerid'))
                      <a href="<?php echo url('addtocart'); ?>"><span class="cart-title">@if (Lang::has(Session::get('lang_file').'.MY_CART')!= '') {{ trans(Session::get('lang_file').'.MY_CART') }}  @else {{ trans($OUR_LANGUAGE.'.MY_CART') }} @endif
</span></a>
                      @else
                      <a href="" role="button" data-toggle="modal" data-target="#loginpop"><span class="cart-title">@if (Lang::has(Session::get('lang_file').'.MY_CART')!= '') {{ trans(Session::get('lang_file').'.MY_CART') }}  @else {{ trans($OUR_LANGUAGE.'.MY_CART') }} @endif</span></a>
                      @endif
                       </div>
                    </div>

                    <div class="menuIcon" onclick="myFunction(this)">
					  <div class="bar1"></div>
					  <div class="bar2"></div>
					  <div class="bar3"></div>
					</div>
                  <div>

                  	
                   <!--  <div class="top-cart-content"> -->
                      <!-- <div class="block-subtitle hidden">Recently added items</div> -->
                      <!-- <ul id="cart-sidebar" class="mini-products-list">
                        <li class="item odd"> <a href="shopping_cart.html" title="Product title here" class="product-image"><img src="<?php echo url(''); ?>/public/themes/images/products/product-9.jpg" alt="html Template" width="65"></a>
                          <div class="product-details"> <a href="#" title="Remove This Item" class="remove-cart"><i class="pe-7s-trash"></i></a>
                            <p class="product-name"><a href="shopping_cart.html">Lorem ipsum dolor sit amet Consectetur</a> </p>
                            <strong>1</strong> x <span class="price">$20.00</span> </div>
                        </li>
                        <li class="item even"> <a href="shopping_cart.html" title="Product title here" class="product-image"><img src="<?php echo url(''); ?>/public/themes/images/products/product-11.jpg" alt="html Template" width="65"></a>
                          <div class="product-details"> <a href="#" title="Remove This Item" class="remove-cart"><i class="pe-7s-trash"></i></a>
                            <p class="product-name"><a href="shopping_cart.html">Consectetur utes anet adipisicing elit</a> </p>
                            <strong>1</strong> x <span class="price">$230.00</span> </div>
                        </li>
                        <li class="item last odd"> <a href="shopping_cart.html" title="Product title here" class="product-image"><img src="<?php echo url(''); ?>/public/themes/images/products/product-10.jpg" alt="html Template" width="65"></a>
                          <div class="product-details"> <a href="#" title="Remove This Item" class="remove-cart"><i class="pe-7s-trash"></i></a>
                            <p class="product-name"><a href="shopping_cart.html">Sed do eiusmod tempor incidist</a> </p>
                            <strong>2</strong> x <span class="price">$420.00</span> </div>
                        </li>
                      </ul> -->
                     <!--  <div class="top-subtotal">Subtotal: <span class="price">$520.00</span></div> -->
                      <!-- <div class="actions">
                        <button class="btn-checkout" type="button" onClick="location.href='checkout.html'"><i class="fa fa-check"></i><span>Checkout</span></button>
                        <a href="{{ url('addtocart')}}"><button class="view-cart" type="button" ><i class="fa fa-shopping-cart"></i><span>View Cart</span></button> </a>
                      </div> -->
                   <!--  </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </header>
  <!-- end header -->
 <nav>
    <div class="container">
      <div class="row">
        <div class="mm-toggle-wrap">
          <div class="mm-toggle"><i class="fa fa-align-justify"></i> </div>
          <span class="mm-label">@if (Lang::has(Session::get('lang_file').'.ALL_CATEGORIES')!= '') {{ trans(Session::get('lang_file').'.ALL_CATEGORIES') }}  @else {{ trans($OUR_LANGUAGE.'.ALL_CATEGORIES') }} @endif
</span> </div>

        <div class="col-md-3 col-sm-3 mega-container hidden-xs">
          <div class="navleft-container">
            <div class="mega-menu-title">
              <h3><span>@if (Lang::has(Session::get('lang_file').'.ALL_CATEGORIES')!= '') {{ trans(Session::get('lang_file').'.ALL_CATEGORIES') }}  @else {{ trans($OUR_LANGUAGE.'.ALL_CATEGORIES') }} @endif</span></h3>
            </div>
             @php
				$prod_path_loader = url('').'/public/assets/noimage/product_loading.gif';
				$image_exist_count="";
				$i=1; 
       
     
        @endphp 
            <!-- Shop by category -->
            <div class="mega-menu-category">
               @if(count($main_category_header)>0)
			 <ul class="nav">
				@foreach($main_category_header as $fetch_main_cat)
				@php	$pass_cat_id1 = "1,".$fetch_main_cat->mc_id; @endphp
				@if($i<=7)
         @if((Session::get('lang_code') == '' || (Session::get('lang_code')) == 'en') )  
           @php  $mc_name = 'mc_name'; @endphp
           @else   @php $mc_name = 'mc_name_code';
          @endphp @endif
          @if($mc_name != '')
                <li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1) }}"> {{ $fetch_main_cat->$mc_name }} </a>
				   @else
           @endif

				  @if(count($sub_main_category_header[$fetch_main_cat->mc_id]) > 0) 
				  <div class="wrap-popup column1">
                    <div class="popup">
                      <ul class="nav">
					   @foreach($sub_main_category_header[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)
					   @php	$pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp
                        <li><a href=" {{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2) }} ">
                           @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'  && Session::get('lang_code2') == '')  
						   @php $smc_name = 'smc_name'; @endphp
						   @else @php $smc_name = 'smc_name_code'; @endphp @endif	  
						   {{ $fetch_sub_main_cat->$smc_name  }}
                           </a>
						 @if(count($second_main_category_header[$fetch_sub_main_cat->smc_id])> 0)
							<div class="wrap-popup column1">
								<div class="popup1">
								  <ul class="nav">
								  @foreach($second_main_category_header[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)
								  @php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp
									<li><a href=" {{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3) }} "><span>
									@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'  && Session::get('lang_code2') == '')  
									@php $sb_name = 'sb_name'; @endphp
									@else  @php $sb_name = 'sb_name_langCode'; @endphp @endif
									{{ $fetch_sub_cat->$sb_name }}
									</span></a>
										@if(count($second_sub_main_category_header[$fetch_sub_cat->sb_id])> 0)
										<div class="wrap-popup column1">
											<div class="popup2">
											  <ul class="nav">
											  @foreach($second_sub_main_category_header[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  
											  @php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp
												<li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4)}}"><span>
												@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'  && Session::get('lang_code2') == '')  
												@php	$ssb_name = 'ssb_name'; @endphp
												@else  @php $ssb_name = 'ssb_name_langCode'; @endphp @endif
												{{ $fetch_secsub_cat->$ssb_name }}
												</span></a></li>
											  @endforeach
											  </ul>
											</div>
										</div>
									   @endif
									</li>
									@endforeach
								  </ul>
							   </div>
						   </div>
						 @endif
						</li>  
						@endforeach
                      </ul>
                    </div>
                  </div>
				 @endif
                </li>
                 @endif
				 @php $i++; @endphp
				@endforeach 
            <li><a href=" {{ url('category_list_all') }} "><span>
                @if (Lang::has(Session::get('lang_file').'.MORE_CATEGORIES')!= '') {{ trans(Session::get('lang_file').'.MORE_CATEGORIES') }}  @else {{ trans($OUR_LANGUAGE.'.MORE_CATEGORIES') }} @endif

                  </span></a>
            </li>
              </ul>
			   @endif
            </div>
          </div>
        </div>
		
		
        <div class="col-md-9 col-sm-9 jtv-megamenu">
          <div class="mtmegamenu">
            <ul id="res_menu"> <!-- Removed hidden-xs -->
               <li <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?>class="active"
					<?php } else {?> class="" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('index')}}">
                  <div class="title title_font"><span class="title-text">
                    
                  {{ (Lang::has(Session::get('lang_file').'.HOME')!= '')  ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')}}</span></div>
                 <!--  <div class="title title_font">
                    <span class="title-text">
                    @php $locale = App::getLocale(); @endphp
                    {{ $locale.'/'.Session::get('lang_code')}}
                    </span>
                  </div> -->

                  </a></div>
              </li>
			
              <!--<li class="mt-root">
                <div class="mt-root-item"><a href="#">
                  <div class="title title_font"><span class="title-text">Categories</span></div>
                  </a></div>
                <ul class="menu-items col-xs-12">
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="#">Fashion</a></div>
                    <ul class="submenu">
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Women</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Men</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Kids</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Clothings</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Shoes</a></div>
                      </li>
                    </ul>
                  </li>
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="#">Electronics </a></div>
                    <ul class="submenu">
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Mobiles</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Computers</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Headphones</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Laptops</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Appliances</a></div>
                      </li>
                    </ul>
                  </li>
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="#">Beauty & Health</a></div>
                    <ul class="submenu">
                      <li class="menu-item depth-2 category ">
                        <div class="title"> <a href="shop_grid.html">Face Care</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Skin Care</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Minerals</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Body Care</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Cosmetic</a></div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>-->

			   <li <?php if(Route::getCurrentRoute()->uri() == 'products' || Route::getCurrentRoute()->uri() == '/') { ?> class="active mt-root"
					<?php } else {?> class="mt-root" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('products')}}">
                  <div class="title title_font"><span class="title-text">{{ (Lang::has(Session::get('lang_file').'.PRODUCTS')!= '')  ?  trans(Session::get('lang_file').'.PRODUCTS'): trans($OUR_LANGUAGE.'.PRODUCTS')}}</span></div>
                  </a></div>
              </li>
			  
			   <li <?php if(Route::getCurrentRoute()->uri() == 'deals' || Route::getCurrentRoute()->uri() == '/') { ?> class="active mt-root"
					<?php } else {?> class="mt-root" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('deals')}}">
                  <div class="title title_font"><span class="title-text">{{ (Lang::has(Session::get('lang_file').'.DEALS')!= '')  ?  trans(Session::get('lang_file').'.DEALS'): trans($OUR_LANGUAGE.'.DEALS')}}</span></div>
                  </a></div>
              </li>
			  
			  <li <?php if(Route::getCurrentRoute()->uri() == 'sold' || Route::getCurrentRoute()->uri() == '/') { ?> class="active mt-root"
					<?php } else {?> class="mt-root" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('sold')}}">
                  <div class="title title_font"><span class="title-text">{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span></div>
                  </a></div>
              </li>
              <!-- if Store Enable or disable to show the store details -->
			@if($GENERAL_SETTING->gs_store_status == 'Store')
			<li <?php if(Route::getCurrentRoute()->uri() == 'stores' || Route::getCurrentRoute()->uri() == '/') { ?> class="active mt-root"
					<?php } else {?> class="mt-root" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('stores')}}">
                  <div class="title title_font"><span class="title-text">{{ (Lang::has(Session::get('lang_file').'.STORES')!= '')  ?  trans(Session::get('lang_file').'.STORES'): trans($OUR_LANGUAGE.'.STORES')}}</span></div>
                  </a></div>
              </li>
			  
			   <li <?php if(Route::getCurrentRoute()->uri() == 'nearbystore' || Route::getCurrentRoute()->uri() == '/') { ?> class="active mt-root"
					<?php } else {?> class="mt-root" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('nearbystore')}}">
                  <div class="title title_font"><span class="title-text">{{ (Lang::has(Session::get('lang_file').'.NEAR_BY_STORE')!= '')  ?  trans(Session::get('lang_file').'.NEAR_BY_STORE'): trans($OUR_LANGUAGE.'.NEAR_BY_STORE')}}</span></div>
                  </a></div>
              </li> 
        @else

        @endif
			  
			  <li <?php if(Route::getCurrentRoute()->uri() == 'contactus' || Route::getCurrentRoute()->uri() == '/') { ?> class="active mt-root"
					<?php } else {?> class="mt-root" <?php } ?>>
					<div class="mt-root-item"><a href="{{ url('contactus')}}">
                  <div class="title title_font"><span class="title-text">{{ (Lang::has(Session::get('lang_file').'.CONTACT_US')!= '')  ?  trans(Session::get('lang_file').'.CONTACT_US'): trans($OUR_LANGUAGE.'.CONTACT_US')}}</span></div>
                  </a></div>
              </li>
		
            </ul>
            
          </div>
        </div>
      </div>
    </div>
  </nav> 
  
  <script>
		function myFunction(x) {
		    x.classList.toggle("change");
		    var element = document.getElementById("res_menu");
    		element.classList.toggle("active");
		}
	</script>