<!DOCTYPE>
<html lang="en">
   <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
	
	 <?php 
    $table_data = DB::table('nm_social_media')->first();
	
	$segment1 =  Request::segment(1); 
	
	/* if($segment1 == 'productview') {
		
		$metaname = '';
		$metakeywords="";
		$metadesc="";
		
	}  */
		
	
			if($metadetails){
			foreach($metadetails as $metainfo) {
			   if(Session::get('lang_code') == '' || Session::get('lang_code') == 'en')
					{
						$metaname = $metainfo->gs_metatitle;
						$metakeywords = $metainfo->gs_metakeywords;
						$metadesc = $metainfo->gs_metadesc;
						
					}
					else
					{
						$get_lang_title = 'gs_metatitle_'.Session::get('lang_code');
						$get_lang_keywords = 'gs_metakeywords_'.Session::get('lang_code');
						$get_lang_desc = 'gs_metadesc_'.Session::get('lang_code');
						$metaname = $metainfo->$get_lang_title;
						$metakeywords = $metainfo->$get_lang_keywords;
						$metadesc = $metainfo->$get_lang_desc;
						
					}
				 }
				 }
			else
			{
				 $metaname="";
				 $metakeywords="";
				  $metadesc="";
			}
	
	
    ?>
	
	 <title><?php echo $metaname;?></title>	
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="description" property="og:description" content="<?php echo $metadesc  ;?>">
     <meta name="keywords"  content="<?php echo  $metakeywords ;?>">
     <meta name="author" content="<?php echo  $metaname ;?>">
     <meta name="_token" content="{!! csrf_token() !!}"/>
	 <meta name="csrf-token" content="{{ csrf_token() }}" />
	 <meta name="title" property="og:title" content="<?php echo $metaname;?>" />
    <meta name="image" property="og:image" content="{{Request::url()}}" />
    <meta property="og:url" content="<?php echo url('')  ;?>" />
  
	 <script>
	 $(function () {
		$.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
		});
	});
	 </script>

<!-- Bootstrap style --> 

<!-- CSS Style -->

	<link href="<?php echo url(''); ?>/public/themes/css/bootstrap.min.css" rel="stylesheet"/>  
    <!-- <link href="<?php echo url(''); ?>/public/themes/css/style.css" rel="stylesheet"/>  --> 
    <link href="<?php echo url(''); ?>/public/themes/css/simple-line-icons.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/font-awesome.min.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/jquery-ui.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/owl.carousel.min.css" rel="stylesheet"/>
    <link href="<?php echo url(''); ?>/public/themes/css/owl.theme.default.min.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/owl.transitions.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/pe-icon-7-stroke.min.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/animate.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/blog.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/flexslider.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/quick_view_popup.css" rel="stylesheet"/>   
    <link href="<?php echo url(''); ?>/public/themes/css/revolution-slider.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/shortcode.css" rel="stylesheet"/> 
    
     
    <link href="<?php echo url(''); ?>/public/themes/css/responsive.css" rel="stylesheet"/> 
    @if(Session::get('lang_code') == '' || Session::get('lang_code') == 'ar')
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/style-arabic.css">
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/bootstrap-rtl.css">     
    @else
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/style.css">
    @endif

     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); 
     foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/<?php echo $fav->imgs_name; ?>">

</head>
<body>

<!--Loader & alert-->
<div id="loader" style="position: absolute; display: none;"><div class="loader-inner"></div>
 
  <div class="loader-section"></div>
</div>


<div id="loadalert" class="alert-success" style="margin-top:18px; display: none; position: fixed; z-index: 9999; width: 50%; text-align: center; left: 25%; padding: 10px;">
<a href="#" class="close" >×</a>
  <strong>{{ (Lang::has(Session::get('lang_file').'.PLS_CHECK_YOUR_MAIL')!= '') ? trans(Session::get('lang_file').'.PLS_CHECK_YOUR_MAIL') : trans($OUR_LANGUAGE.'.PLS_CHECK_YOUR_MAIL') }}</strong>
</div>

     <!--Loader & alert-->

<div class="loginSuccess">
  
</div>
<div id="mobile-menu">
        
  <ul>

    @php
          $prod_path_loader = url('').'/public/assets/noimage/product_loading.gif';
          $image_exist_count="";
        $i=1; 
       @endphp 
   
  @if(count($main_category_header)>0)
        @foreach($main_category_header as $fetch_main_cat)
        @php  $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; @endphp
        @if($i<=7)
                <li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1) }}">
           @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
           @php  $mc_name = 'mc_name'; @endphp
           @else   @php $mc_name = 'mc_name_code'; @endphp @endif
           {{ $fetch_main_cat->mc_name }}
           </a>
           
          @if(count($sub_main_category_header[$fetch_main_cat->mc_id])> 0) 
          
                      <ul class="">
             @foreach($sub_main_category_header[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)
             @php 
             
             $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp
                        <li><a href=" {{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2) }} ">
                           @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                                 @php  $smc_name = 'smc_name'; @endphp
                           @else 
                              @php  $smc_name = 'smc_name_code';

                          @endphp

                         @endif   
                         {{ $fetch_sub_main_cat->$smc_name  }}
                          </a>
             @if(count($second_main_category_header[$fetch_sub_main_cat->smc_id])> 0)
              
                  <ul class="">
                  @foreach($second_main_category_header[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)
                  @php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp
                  <li><a href=" {{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3) }} ">
                  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                  @php $sb_name = 'sb_name'; @endphp
                  @else  @php $sb_name = 'sb_name_langCode'; @endphp @endif
                  {{ $fetch_sub_cat->$sb_name }}
                  </a>
                    @if(count($second_sub_main_category_header[$fetch_sub_cat->sb_id])> 0)
                    
                        <ul class="">
                        @foreach($second_sub_main_category_header[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  
                        @php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp
                        <li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4)}}">
                        @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                        @php  $ssb_name = 'ssb_name'; @endphp
                        @else  @php $ssb_name = 'ssb_name_langCode'; @endphp @endif
                        {{ $fetch_secsub_cat->$ssb_name }}
                        </a></li>
                        @endforeach
                        </ul>
                     @endif
                  </li>
                  @endforeach
                  </ul>
             @endif
            </li>  
            @endforeach
                      </ul>
         @endif
                </li>
                 @endif
         @php $i++; @endphp
        @endforeach 
            <li><a href=" {{ url('category_list_all') }} ">
                @if (Lang::has(Session::get('lang_file').'.MORE_CATEGORIES')!= '') {{ trans(Session::get('lang_file').'.MORE_CATEGORIES') }}  @else {{ trans($OUR_LANGUAGE.'.MORE_CATEGORIES') }} @endif
                </a>
            </li>

         @endif

    
  </ul>
</div>
<div id="page"> 

<header>

<div class="header-container">
      <div class="header-top">
        <div class="container">
            
          <div class="row">
            <div class="col-sm-4 col-md-4 col-xs-12"> 
              <!-- Default Welcome Message -->
             <!--<div class="welcome-msg hidden-xs hidden-sm">Default welcome msg! </div>-->
              <!-- Language &amp; Currency wrapper -->
              <div class="language-currency-wrapper">
                  
                <div class="inner-cl">
                    
                  <!--<div class="block block-language form-language">
                    <div class="lg-cur"><span><img src="images/flag-default.jpg" alt="French"><span class="lg-fr">French</span><i class="fa fa-angle-down"></i></span></div>
                    <ul>
                      <li><a class="selected" href="#"><img src="images/flag-english.jpg" alt="english"><span>English</span></a></li>
                      <li><a href="#"><img src="images/flag-default.jpg" alt="French"><span>French</span></a></li>
                      <li><a href="#"><img src="images/flag-german.jpg" alt="German"><span>German</span></a></li>
                      <li><a href="#"><img src="images/flag-brazil.jpg" alt="Brazil"><span>Brazil</span></a></li>
                      <li><a href="#"><img src="images/flag-chile.jpg" alt="Chile"><span>Chile</span></a></li>
                      <li><a href="#"><img src="images/flag-spain.jpg" alt="Spain"><span>Spain</span></a></li>
                    </ul>
                  </div>
                  <div class="block block-currency">
                    <div class="item-cur"><span>USD</span><i class="fa fa-angle-down"></i></div>
                    <ul>
                      <li><a href="#"><span class="cur_icon">€</span>EUR</a></li>
                      <li><a href="#"><span class="cur_icon">¥</span>JPY</a></li>
                      <li><a class="selected" href="#"><span class="cur_icon">$</span>USD</a></li>
                    </ul>
                  </div>-->
                </div>
              </div>
            </div>
            
            <!-- top links -->
            <div class="headerlinkmenu col-md-8 col-sm-8 col-xs-12"> <span class="phone  hidden-xs hidden-sm">@if (Lang::has(Session::get('lang_file').'.CALL_US')!= '') {{ trans(Session::get('lang_file').'.CALL_US') }} @else {{ trans($OUR_LANGUAGE.'.CALL_US') }} @endif:{{ Helper::customer_support_number() }}</span>
              <ul class="links">
                <li class="hidden-xs"><a title="Help Center" href="{{ url('help')}}"><span>@if (Lang::has(Session::get('lang_file').'.HELP_CENTER')!= '') {{ trans(Session::get('lang_file').'.HELP_CENTER') }} @else {{ trans($OUR_LANGUAGE.'.HELP_CENTER') }} @endif</span></a>
                </li>
               <!-- <li><a title="Store Locator" href="#"><span>Store Locator</span></a></li>
                <li><a title="Checkout" href="checkout.html"><span>Checkout</span></a></li>-->
                 <!-- <li><a href="#merchant"><span>{{ (Lang::has(Session::get('lang_file').'.MERCHANT')!= '') ?  trans(Session::get('lang_file').'.MERCHANT') : trans($OUR_LANGUAGE.'.MERCHANT') }}</span></a>
                  (<a title="Merchant Login" href="<?php echo url('sitemerchant');?>">{{ (Lang::has(Session::get('lang_file').'.LOGIN')!= '') ?  trans(Session::get('lang_file').'.LOGIN') : trans($OUR_LANGUAGE.'.LOGIN') }}
                  </a>
                     <a title="Merchant Registers" href="<?php echo url('merchant_signup');?>"><span>{{ (Lang::has(Session::get('lang_file').'.REGISTER')!= '') ?  trans(Session::get('lang_file').'.REGISTER') : trans($OUR_LANGUAGE.'.REGISTER') }}</span>
                    </a>)
                </li> -->
                <li><a href="<?php echo url('login'); ?>"><span>@if (Lang::has(Session::get('lang_file').'.LOGIN')!= '') {{ trans(Session::get('lang_file').'.LOGIN') }} @else {{ trans($OUR_LANGUAGE.'.LOGIN') }} @endif</span></a></li>
               <!--  <li><a href="" data-toggle="modal" data-target="#loginpop">login1</a></li> -->
	        			<li><a href="<?php echo url('registers'); ?>"><span>@if (Lang::has(Session::get('lang_file').'.REGISTER')!= '') {{ trans(Session::get('lang_file').'.REGISTER') }} @else {{ trans($OUR_LANGUAGE.'.REGISTER') }} @endif</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>



<div class="login-top">
 <!-- Modal -->
  <div class="modal fade" id="loginpop" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align: center;">

          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">@if (Lang::has(Session::get('lang_file').'.LOGIN')!= '') {{ trans(Session::get('lang_file').'.LOGIN') }} @else {{ trans($OUR_LANGUAGE.'.LOGIN') }} @endif</h4>
        </div>
        <div class="modal-body">
         <section class="col1-layout">
    <div class="main">
      <div style="overflow: hidden;">
        <div class="account-login">
		 <div class="col-sm-6 col-md-6 col-xs-12"> 
          <div class="box-authentication box-authentication-popup">
        <p class="before-login-text">@if (Lang::has(Session::get('lang_file').'.WELCOME_SIGN_IN_ACCOUNT')!= '') {{ trans(Session::get('lang_file').'.WELCOME_SIGN_IN_ACCOUNT') }} @else {{ trans($OUR_LANGUAGE.'.WELCOME_SIGN_IN_ACCOUNT') }} @endif</p>
        
          <label for="emmail_login">@if (Lang::has(Session::get('lang_file').'.EMAIL_ADDRESS')!= '') {{ trans(Session::get('lang_file').'.EMAIL_ADDRESS') }} @else {{ trans($OUR_LANGUAGE.'.EMAIL_ADDRESS') }} @endif<span class="required">*</span></label>
           <!-- <input id="loginemail" name="loginemail" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE')!= '') {{ trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_HERE') }} @endif" type="text" class="form-control">-->
            <input id="loginemail" name="loginemail" value="" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE')!= '') {{ trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_HERE') }} @endif" type="text" class="form-control">
          
          <label for="password_login">@if (Lang::has(Session::get('lang_file').'.PASSWORD')!= '') {{ trans(Session::get('lang_file').'.PASSWORD') }} @else {{ trans($OUR_LANGUAGE.'.PASSWORD') }} @endif<span class="required">*</span></label>
          
           <!-- <input id="loginpassword" name="loginpassword" placeholder="@if (Lang::has(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS')!= '') {{ trans(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS') }} @else {{ trans($OUR_LANGUAGE.'.MIMIMUM_6_CHARACTERS') }} @endif" type="password" class="form-control">-->
             <input id="loginpassword" value="" name="loginpassword" placeholder="@if (Lang::has(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS')!= '') {{ trans(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS') }} @else {{ trans($OUR_LANGUAGE.'.MIMIMUM_6_CHARACTERS') }} @endif" type="password" class="form-control"><br>
            
          <div id="logerror_msg" style="color: red;"></div>
          
          <p class="forgot-pass"><a href="{{ url('')}}/forget_password">@if (Lang::has(Session::get('lang_file').'.LOST_PASSWORD')!= '') {{ trans(Session::get('lang_file').'.LOST_PASSWORD') }} @else {{ trans($OUR_LANGUAGE.'.LOST_PASSWORD') }} @endif ?</a></p>
          
          <button id="login_submit" class="button"><i class="icon-lock icons"></i>&nbsp; <span>@if (Lang::has(Session::get('lang_file').'.LOGIN')!= '') {{ trans(Session::get('lang_file').'.LOGIN') }} @else {{ trans($OUR_LANGUAGE.'.LOGIN') }} @endif</span></button>
           
          <input type="hidden" id="return_url" value="<?php echo url('');?>" />
       
           <!--<label class="inline" for="rememberme">
            <input type="checkbox" value="forever" id="rememberme" name="rememberme">
            Remember me </label>-->
          </div>
		 </div>  

		  
	  <div class="col-sm-6 col-md-6 col-xs-12"> 
        <div class="box-authentication">
        <p>@if (Lang::has(Session::get('lang_file').'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')!= '') {{ trans(Session::get('lang_file').'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP') }} @else {{ trans($OUR_LANGUAGE.'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP') }} @endif <?php echo $SITENAME; ?>.</p>

        <a  onclick="fb_login()" class="facebook_login" style="margin-top:5px; margin-bottom:5px" >&nbsp;</a>
        <div class="register-benefits">
          <h5>@if (Lang::has(Session::get('lang_file').'.DONT_HAVE_AN_ACCOUNT_YET')!= '') {{ trans(Session::get('lang_file').'.DONT_HAVE_AN_ACCOUNT_YET') }} @else {{ trans($OUR_LANGUAGE.'.DONT_HAVE_AN_ACCOUNT_YET') }} @endif ?
           <a href="<?php echo url("registers");?>" style="color:#ff8400;">@if (Lang::has(Session::get('lang_file').'.SIGN_UP')!= '') {{ trans(Session::get('lang_file').'.SIGN_UP') }} @else {{ trans($OUR_LANGUAGE.'.SIGN_UP') }} @endif
        </div>
        </div> 
      </div> 
		  
		</div>
       </div>
      </div>
    </div>
  </section>
        </div>
      </div>
      
    </div>
  </div>
  </div>


	  <!-- JS --> 

<!-- jquery js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.min.js"></script> 

<!-- bootstrap js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/bootstrap.min.js"></script> 

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/owl.carousel.min.js"></script> 

<!-- jquery.mobile-menu js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/mobile-menu.js"></script> 

<!--jquery-ui.min js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery-ui.js"></script> 

<!-- main js -->  

<!-- countdown js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/countdown.js"></script> 



<!-- Slider Js --> 
<!-- <script type="text/javascript" src="<?php //echo url(''); ?>/public/themes/js/revolution-slider.js"></script> 
<script type='text/javascript'>
        jQuery(document).ready(function(){
            jQuery('#rev_slider_4').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 865,
	            startheight: 450,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',
                
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
            
                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'off',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'off',
                forceFullWidth: 'on',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,
            
                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,
					

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
        </script> -->

        <?php if(isset($email_login)) {
               if($email_login)  { ?>
  <script>
    $(window).load(function() {
   $("#login").modal("show");
    });

  </script>


  <?php }} ?>
  
<script src="//connect.facebook.net/en_US/sdk.js" type="text/javascript"></script>
<script type="text/javascript">
window.fbAsyncInit = function() {
    FB.init({
        appId   : '<?php echo $table_data->sm_fb_app_id;?>',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true, // parse XFBML
        version    : 'v2.8' // use graph api version 2.8
    });

  };

function fb_login()
{ 
  console.log( 'fb_login function initiated' );
	  FB.login(function(response) {

      console.log( 'login response' );
      console.log( response );
      console.log( 'Response Status' + response.status );
		//top.location.href=http://demo.Sundaroecommerce.com/;
      if (response.authResponse) {

        console.log( 'Auth success' );

    		FB.api("/me",'GET',{'fields':'id,email,verified,name'}, function(me){

      		if (me.id) {


            //console.log( 'Retrived user details from FB.api', me );

             var id = me.id; 
		var email = me.email;
            	var name = me.name;
                var live ='';
				if (me.hometown!= null)
				{			
					var live = me.hometown.name;
				}        
            	
    var passData = 'fid='+ id + '&email='+ email + '&name='+ name + '&live='+ live ;
 //alert(passData);
            //console.log('data', passData);
          
            $.ajax({
             type: 'GET',
            data: passData,
             //data: $.param(passData),
             global: false,
             url: '<?php echo url('facebooklogin'); ?>',
             success: function(responseText){ 
              console.log( responseText ); 
            
             location.reload();
             },
             error: function(xhr,status,error){
               console.log(status, status.responseText);
             }
           }); 

        }else{

          console.log('There was a problem with FB.api', me);

        }
      });

    }else{
      console.log( 'Auth Failed' );
    }

  }, {scope: 'email'});
}
function logout() {

        try {
        if (FB.getAccessToken() != null) {
            FB.logout(function(response) {
                // user is now logged out from facebook do your post request or just redirect
                window.location = "<?php echo url('facebook_logout'); ?>";
            });
        } else {
            // user is not logged in with facebook, maybe with something else
            window.location = "<?php echo url('facebook_logout'); ?>";
        }
    } catch (err) {
        // any errors just logout
        window.location = "<?php echo url('facebook_logout'); ?>";
    }
           /*  FB.logout(function(response) {
				    
                FB.getAuthResponse(null, 'unknown');
                //FB.Auth.getAuthResponse(null, 'unknown');
                 window.location = "<?php //echo url('facebook_logout'); ?>";
              //FB.logout();
               				console.log(response);

            }); */
}
</script>

<script>
  $( document ).ready(function() {
    <?php if(Session::has('reset_userid')){ ?>
      $('#reset_pass_click').click();
      
      <?php } ?>
      var cname    = $('#inputregisterName');
      var cemail     = $('#inputregisterEmail');
      var cpwd     = $('#inputregisterPassword');
      var loginemail    =$('#loginemail');
      var loginpassword =$('#loginpassword');
      var selectcity = $('#selectcity');
      var selectcountry = $('#selectcountry');
      var return_url = $('#return_url');

     $('#login_submit').click(function() {
   

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(loginemail.val() == "")
    {
      loginemail.css('border', '1px solid red'); 
       
      loginemail.focus();
      return false;
    }
    else
    {
      loginemail.css('border', ''); 
       
    }

     if(!emailReg.test(loginemail.val()))
        {
          loginemail.css('border', '1px solid red'); 
           
          loginemail.focus();
          return false;
          
        }

    else
      {
      loginemail.css('border', ''); 
       

       }
    if(loginpassword.val() == "")
    {
      loginpassword.css('border', '1px solid red'); 
       
      loginpassword.focus();
      return false;
    }
    else
    {
        
      loginpassword.css('border', ''); 
      $('#logerror_msg').html('');

      var logemail=loginemail.val();
      var logpassword=loginpassword.val();
      var returl = return_url.val()
      
          var passemail = 'email='+logemail+"&pwd="+logpassword;
        
             $.ajax( {
               type: 'get',
               data: passemail,
               url: '<?php echo url('user_login_submit_popup');?>',
           
            success: function(responseText,status){  
			/* alert(status+passemail);exit; */
     
            if(responseText)
            {  
				if(responseText.trim()=="success")
				{
		
					 //$(".loginSuccess").html("<?php //if (Lang::has(Session::get('lang_file').'.LOGIN_SUCCESSFULLY')!= '') { echo  trans(Session::get('lang_file').'.LOGIN_SUCCESSFULLY');}  else { echo trans($OUR_LANGUAGE.'.LOGIN_SUCCESSFULLY');} ?>");

				//window.location=returl;

					/* $("#login").modal("hide"); */

					window.location.reload();
				}else if(responseText.trim()=="block"){
                $('#logerror_msg').html('<?php if (Lang::has(Session::get('lang_file').'.CUSTOMER_BLOCKED')!= '') { echo  trans(Session::get('lang_file').'.CUSTOMER_BLOCKED');}  else { echo trans($OUR_LANGUAGE.'.CUSTOMER_BLOCKED');} ?>');
            }
				else
				{
					$('#logerror_msg').html('<?php if (Lang::has(Session::get('lang_file').'.INVALID_LOGIN_CREDENTIALS')!= '') { echo  trans(Session::get('lang_file').'.INVALID_LOGIN_CREDENTIALS');}  else { echo trans($OUR_LANGUAGE.'.INVALID_LOGIN_CREDENTIALS');} ?>');
				}
                     
            }
        }
      });   

    }
    
   
    });
});
</script>