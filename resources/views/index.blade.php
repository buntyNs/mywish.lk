<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]--> 
<!-- mobile menu -->
{!! $navbar !!}
{!! $header !!} 

<div class="preloader-wrapper">
   <div class="preloader">
     <!--   <img src="http://prorabbi.website/nila/assets/images/preloader.gif" alt="NILA">-->
         </div>
   </div>
   
    <script>
 $(document).ready(function() {
 setTimeout(function(){
          $('.preloader-wrapper').fadeOut();
   }, 3);
  });
 </script>
<!-- Slideshow  -->

<div class="main-slider" id="home">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-sm-3 col-xs-12 banner-left hidden-xs">
            <br> <br>
           
               @if(count($product_details) != 0)
               @for($i = 0; $i < 1 ;$i++) 
               <div class="jtv-banner3">
               {{-- product name  --}}
               <?php 
                  $mcat = strtolower(str_replace(' ','-',$product_details[0]->mc_name));
                  $smcat = strtolower(str_replace(' ','-',$product_details[0]->smc_name));
                  $sbcat = strtolower(str_replace(' ','-',$product_details[0]->sb_name));
                  $ssbcat = strtolower(str_replace(' ','-',$product_details[0]->ssb_name)); 
                  // product id  
                  $res = base64_encode($product_details[0]->pro_id);
                  //product image  
                  $product_image = explode('/**/',$product_details[0]->pro_Img);
                  //product price  
                  $product_saving_price = $product_details[0]->pro_price - $product_details[0]->pro_disprice;
                  // product dicount percentage  
                  $product_discount_percentage = round(($product_saving_price/ $product_details[0]->pro_price)*100,2); ?>
               {{-- product title  --}} 
               @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
               @php
               $title = 'pro_title';
               @endphp
               @else 
               @php  
               $title = 'pro_title_langCode'; 
               @endphp 
               @endif
               {{-- product image --}}  
               @php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               $img_data = "public/assets/product/".$product_image[0];
               @endphp
               @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
               @php 
               $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
               @endphp       
               @else  
               @if(isset($DynamicNoImage['productImg']))
               @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
               @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
               @php
               $prod_path = url('')."/".$dyanamicNoImg_path;
               @endphp 
               @endif
               @endif
               @endif
               @php $discount_percent = $alt_text = ''; @endphp
               {{-- Alt text--}}
               @php 
               $alt_text   = substr($product_details[0]->$title,0,25);
               $alt_text  .= strlen($product_details[0]->$title)>25?'..':''; @endphp
               {{-- Discount percentage--}}
               @if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
               @php
               $discount_percent = round($product_discount_percentage);
               @endphp       
               @endif
               <div class="jtv-banner3-inner"><img src="{{ $prod_path }}" alt="{{ $alt_text}}">
               </div>
            </div>
           
               <br><br>
               <div class="hover_content">
                  <div class="hover_data">
                     <div class="title">@if (Lang::has(Session::get('lang_file').'.BIG_SALE')!= '') {{ trans(Session::get('lang_file').'.BIG_SALE') }}  @else {{ trans($OUR_LANGUAGE.'.BIG_SALE') }} @endif
                     </div>
                     <div class="desc-text">@if (Lang::has(Session::get('lang_file').'.UP_TO')!= '') {{ trans(Session::get('lang_file').'.UP_TO') }}  @else {{ trans($OUR_LANGUAGE.'.UP_TO') }} @endif
                        {{ $discount_percent }} @if (Lang::has(Session::get('lang_file').'.OFF')!= '') {{ trans(Session::get('lang_file').'.OFF') }}  @else {{ trans($OUR_LANGUAGE.'.OFF') }} @endif
                     </div>
                     @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                     <p class="big-sale"> <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                        @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                        </a>
                     </p>
                     @endif 
                     @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                     <p class="big-sale">   <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                        @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                        </a> 
                     </p>
                     @endif <!-- //if -->
                     @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 
                     <p class="big-sale"> <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                        @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                        </a> 
                     </p>
                     @endif 
                     @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')  
                     <p class="big-sale">  <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="{{ $alt_text }}" class="shop-now">
                        @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                        </a> 
                     </p>
                     @endif
                  </div>
               </div>
              
               @endfor
               @else
               @php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               @endphp
               @if(isset($DynamicNoImage['productImg']))
               @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
               @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
               @php
               $prod_path = url('')."/".$dyanamicNoImg_path;
               @endphp 
               @endif
               @endif
               <div>
               <img src="{{ $prod_path }}" alt=""> </div>
               @endif

            </div>
            <div class="col-sm-9 col-md-9 col-lg-9 col-xs-12 jtv-slideshow">
               <div id="jtv-slideshow">
                  <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
                     <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                        @if(count($bannerimagedetails) > 0 )
                        <ul>
                           @foreach($bannerimagedetails as $banner)
                           <?php 
                              $pro_img = $banner->bn_img;
                              $prod_path = url('').'/public/assets/default_image/No_image_banner.png'; ?>
                           <?php   $img_data = "public/assets/bannerimage/".$pro_img; ?>
                           @if(file_exists($img_data) && $pro_img != '')
                           <?php     $prod_path = url('').'/public/assets/bannerimage/'.$pro_img; ?>
                           @else
                           @if(isset($DynamicNoImage['banner'])) 
                           <?php               
                              $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['banner']; ?>
                           @if($DynamicNoImage['banner'] !='' && file_exists($dyanamicNoImg_path))
                           <?php $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['banner']; ?>
                           @endif
                           @endif
                           @endif
                           <li data-transition='fade' data-slotamount='7' data-masterspeed='1000' data-thumb=''>
                             <a href="{{$banner->bn_redirecturl}}" target="new"> <img src="{{ $prod_path }}" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/></a>
                              <div class="caption-inner">
                                 <div class='tp-caption LargeTitle sft  tp-resizeme' data-x='250'  data-y='110'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'>@if (Lang::has(Session::get('lang_file').'.ESHOP_BUSINESS')!= '') {{  trans(Session::get('lang_file').'.ESHOP_BUSINESS') }}  @else {{ trans($OUR_LANGUAGE.'.ESHOP_BUSINESS') }} @endif</div>
                                 <div class='tp-caption ExtraLargeTitle sft  tp-resizeme' data-x='200'  data-y='160'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap; color:#fff; text-shadow:none;'>@if (Lang::has(Session::get('lang_file').'.EASY_BUY')!= '') {{  trans(Session::get('lang_file').'.EASY_BUY') }}  @else {{ trans($OUR_LANGUAGE.'.EASY_BUY') }} @endif</div>
                                 <div class='tp-caption' data-x='310'  data-y='230'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap; color:#f8f8f8;'> </div>
                                 <div class='tp-caption sfb  tp-resizeme ' data-x='370'  data-y='280'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='<?php echo url('products'); ?>' class="buy-btn">@if (Lang::has(Session::get('lang_file').'.GET_START')!= '') {{  trans(Session::get('lang_file').'.GET_START') }}  @else {{ trans($OUR_LANGUAGE.'.GET_START') }} @endif</a> </div>
                              </div>
                           </li>
                           @endforeach
                        </ul>
                        @else
                        <img src="{{ url('')}}/public/assets/noimage/No_image_1509364387_870x350.png " data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                        @endif
                        <div class="tp-bannertimer"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- service section -->
   @include('service_section')
   <!-- All products-->
   <div class="container">
      <div class="home-tab">
         <div class="tab-title text-left">
            <h2>@if (Lang::has(Session::get('lang_file').'.BEST_SELLING')!= '') {{  trans(Session::get('lang_file').'.BEST_SELLING') }}  @else {{ trans($OUR_LANGUAGE.'.BEST_SELLING') }} @endif</h2>
            {{-- 
            <ul class="nav home-nav-tabs home-product-tabs">
               <li class="active"><a href="#computer" data-toggle="tab" aria-expanded="false">Computer</a></li>
               <li><a href="#smartphone" data-toggle="tab" aria-expanded="false">Smartphone</a></li>
               <li><a href="#watches" data-toggle="tab" aria-expanded="false">Watches</a></li>
               <li><a href="#photo" data-toggle="tab" aria-expanded="false">Photo & Camera</a></li>
            </ul>
            --}}
         </div>
         <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="computer">
               <div class="featured-pro">
                  <div class="slider-items-products">                    
                     <div id="computer-slider" class="product-flexslider hidden-buttons">
                        <div class="owl-carousel owl-theme computer-slider">
                           @if(count($product_details) != 0)
                           @foreach($product_details as $product_det) 
                           {{-- product name  --}}
                           <?php 
                              $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                              $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                              $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                              $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                              // product id  
                              $res = base64_encode($product_det->pro_id);
                              //product image  
                              $product_image = explode('/**/',$product_det->pro_Img);
                              //product price  
                              $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                              // product dicount percentage  
                              $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
                           {{-- product title  --}} 
                           @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                           @php
                           $title = 'pro_title';
                           @endphp
                           @else 
                           @php  
                           $title = 'pro_title_langCode'; 
                           @endphp 
                           @endif
                           {{-- product image --}}  
                           @php 
                           $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                           $img_data = "public/assets/product/".$product_image[0];
                           @endphp
                           @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                           @php 
                           $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                           @endphp       
                           @else  
                           @if(isset($DynamicNoImage['productImg']))
                           @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                           @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                           @php
                           $prod_path = url('')."/".$dyanamicNoImg_path;
                           @endphp 
                           @endif
                           @endif
                           @endif
                           @php $discount_percent = $alt_text = ''; @endphp
                           {{-- Alt text--}}
                           @php 
                           $alt_text   = substr($product_det->$title,0,25);
                           $alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp
                           {{-- Discount percentage--}}
                           @if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
                           @php
                           $discount_percent = round($product_discount_percentage);
                           @endphp       
                           @endif
                           <div class="product-item">
                              <div class="item-inner">
                                 <div class="product-thumbnail">
                                    <div class="icon-new-label new-left">{{$discount_percent}}%</div>
                                    <div class="pr-img-area">
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"> </figure>
                                       </a>
                                       @endif <!-- /*//if*/ --> 
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"></figure>
                                       </a>
                                       @endif <!-- //if -->
                                       @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"></figure>
                                       </a>
                                       @endif 
                                       @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"></figure>
                                       </a>
                                       @endif
                                    </div>
                                    <div class="pr-info-area">
                                       <div class="pr-button">
                                          <div class="mt-button add_to_wishlist"> 
                                             {{-- Add to wishlist--}}
                                             <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>
                                             @if(Session::has('customerid'))
                                             @if(count($prodInWishlist)==0)
                                             <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                             {{ Form::hidden('pro_id','$product_det->pro_id') }}        
                                             <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                             <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                             <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             @else
                                             <?php /* remove wishlist */?>   
                                             <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                             <i class="fa fa-heart" aria-hidden="true"></i> 
                                             </a> 
                                             <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                             @endif  
                                             @else 
                                             <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             @endif
                                          </div>
                                          <div class="mt-button quick-view" ><a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap{{ $product_det->pro_id }}"> <i class="fa fa-search"></i> </a> </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item-info">
                                    <div class="info-inner">
                                       <div class="item-title">
                                          <a title="" >
                                             {{ substr($product_det->$title,0,25) }}
                                             {{ strlen($product_det->$title)>25?'..':'' }} 
                                       </div>
                                       <div class="item-content">
                                       {{-- <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div> --}}
                                       <div class="item-price">
                                       <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                       @if($product_det->pro_no_of_purchase >= $product_det->pro_qty)
                                       <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{  trans(Session::get('lang_file').'.SOLD') }}  @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span> </button>
                                       @else  
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button> </a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button> </a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button> </a>
                                       @endif
                                       @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button>
                                       </a>
                                       @endif
                                       @endif
                                       </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                           @elseif(count($product_details) == 0)
                           <p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">
                              @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif
                           </p>
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>



   <div class="inner-box">
      <div class="container">
         <div class="row">
            <!-- Banner -->
            <div class="col-md-3 top-banner hidden-sm">
               <div class="jtv-banner3">
                  @if(count($product_details) != 0 && isset($product_details[1]) )
                  @for($i = 0; $i < 1 ;$i++) 
                  {{-- product name  --}}
                  <?php 
                     $mcat = strtolower(str_replace(' ','-',$product_details[1]->mc_name));
                     $smcat = strtolower(str_replace(' ','-',$product_details[1]->smc_name));
                     $sbcat = strtolower(str_replace(' ','-',$product_details[1]->sb_name));
                     $ssbcat = strtolower(str_replace(' ','-',$product_details[1]->ssb_name)); 
                     // product id  
                     $res = base64_encode($product_details[1]->pro_id);
                     //product image  
                     $product_image = explode('/**/',$product_details[1]->pro_Img);
                     //product price  
                     $product_saving_price = $product_details[1]->pro_price - $product_details[1]->pro_disprice;
                     // product dicount percentage  
                     $product_discount_percentage = round(($product_saving_price/ $product_details[1]->pro_price)*100,2); ?>
                  {{-- product title  --}} 
                  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                  @php
                  $title = 'pro_title';
                  @endphp
                  @else 
                  @php  
                  $title = 'pro_title_langCode'; 
                  @endphp 
                  @endif
                  {{-- product image --}}  
                  @php 
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                  $img_data = "public/assets/product/".$product_image[0];
                  @endphp
                  @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                  @php 
                  $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                  @endphp       
                  @else  
                  @if(isset($DynamicNoImage['productImg']))
                  @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                  @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                  @php
                  $prod_path = url('')."/".$dyanamicNoImg_path;
                  @endphp 
                  @endif
                  @endif
                  @endif
                  @php $discount_percent = $alt_text = ''; @endphp
                  {{-- Alt text--}}
                  @php 
                  $alt_text   = substr($product_details[1]->$title,0,25);
                  $alt_text  .= strlen($product_details[1]->$title)>25?'..':''; @endphp
                  {{-- Discount percentage--}}
                  @if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
                  @php
                  $discount_percent = round($product_discount_percentage);
                  @endphp       
                  @endif
                  <div class="jtv-banner3-inner">
                     <img src="{{ $prod_path }}" alt="{{ $alt_text}}">
                     <div class="hover_content">
                        <div class="hover_data">
                           <div class="title">@if (Lang::has(Session::get('lang_file').'.BIG_SALE')!= '') {{ trans(Session::get('lang_file').'.BIG_SALE') }}  @else {{ trans($OUR_LANGUAGE.'.BIG_SALE') }} @endif
                           </div>
                           <div class="desc-text">@if (Lang::has(Session::get('lang_file').'.UP_TO')!= '') {{ trans(Session::get('lang_file').'.UP_TO') }}  @else {{ trans($OUR_LANGUAGE.'.UP_TO') }} @endif
                              {{ $discount_percent }} @if (Lang::has(Session::get('lang_file').'.OFF')!= '') {{ trans(Session::get('lang_file').'.OFF') }}  @else {{ trans($OUR_LANGUAGE.'.OFF') }} @endif
                           </div>
                           @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                           <p> <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="{{ $alt_text }}" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a>
                           </p>
                           @endif <!-- /*//if*/ --> 
                           @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
                           <p>   <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a> 
                           </p>
                           @endif <!-- //if -->
                           @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
                           <p> <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a> 
                           </p>
                           @endif 
                           @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')
                           <p>  <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a> 
                           </p>
                           @endif
                        </div>
                     </div>
                  </div>
                  @endfor
                  @else
                  @php 
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                  @endphp
                  @if(isset($DynamicNoImage['productImg']))
                  @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                  @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                  @php
                  $prod_path = url('')."/".$dyanamicNoImg_path;
                  @endphp 
                  @endif
                  @endif
                  <div class="jtv-banner3-inner"><img src="{{ $prod_path }}" alt=""></div>
                  @endif
               </div>
            </div>
            <!-- Best Sale -->
            <div class="col-sm-12 col-md-9 jtv-best-sale special-pro">
               <div class="jtv-best-sale-list">
                  <div class="wpb_wrapper">
                     <div class="best-title text-left">
                        <h2>@if (Lang::has(Session::get('lang_file').'.SPECIAL_OFFERS')!= '') {{ trans(Session::get('lang_file').'.SPECIAL_OFFERS') }}  @else {{ trans($OUR_LANGUAGE.'.SPECIAL_OFFERS') }} @endif</h2>
                     </div>
                  </div>
                  <div class="slider-items-products">
                     <div id="jtv-best-sale-slider" class="product-flexslider">
                        <div class="owl-carousel owl-theme best-sale-slider">
                           @if(count($product_details) != 0)
                           @foreach($product_details as $product_det) 
                           {{-- product name  --}}
                           <?php 
                              $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                              $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                              $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                              $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                              // product id  
                              $res = base64_encode($product_det->pro_id);
                              //product image  
                              $product_image = explode('/**/',$product_det->pro_Img);
                              //product price  
                              $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                              // product dicount percentage  
                              $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
                           @if($product_discount_percentage<= 50)
                           @if($product_det->pro_no_of_purchase < $product_det->pro_qty) 
                           {{-- product title  --}} 
                           @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                           @php
                           $title = 'pro_title';
                           @endphp
                           @else 
                           @php  
                           $title = 'pro_title_langCode'; 
                           @endphp 
                           @endif
                           {{-- product image --}}  
                           @php 
                           $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                           $img_data = "public/assets/product/".$product_image[0];
                           @endphp
                           @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                           @php 
                           $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                           @endphp       
                           @else  
                           @if(isset($DynamicNoImage['productImg']))
                           @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                           @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                           @php
                           $prod_path = url('')."/".$dyanamicNoImg_path;
                           @endphp 
                           @endif
                           @endif
                           @endif
                           @php $discount_percent = $alt_text = ''; @endphp
                           {{-- Alt text--}}
                           @php 
                           $alt_text   = substr($product_det->$title,0,25);
                           $alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp
                           {{-- Discount percentage--}}
                           @if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
                           @php
                           $discount_percent = round($product_discount_percentage);
                           @endphp       
                           @endif
                           <div class="product-item">
                              <div class="item-inner">
                                 <div class="product-thumbnail">
                                    <div class="icon-new-label new-left">{{$discount_percent}}%</div>
                                    <div class="pr-img-area">
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="{{ $alt_text}}">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="{{ $alt_text}}"></figure>
                                       </a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="{{ $alt_text}}">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="{{ $alt_text}}"> </figure>
                                       </a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="{{ $alt_text}}">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="{{ $alt_text}}"> </figure>
                                       </a>
                                       @endif
                                       @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="{{ $alt_text}}">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="{{ $alt_text}}"></figure>
                                       </a>
                                       @endif
                                    </div>
                                    <div class="pr-info-area">
                                       <div class="pr-button">
                                          <div class="mt-button add_to_wishlist"> 
                                             {{-- Add to wishlist--}}
                                             <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>
                                             @if(Session::has('customerid'))
                                             @if(count($prodInWishlist)==0)
                                             <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                             {{ Form::hidden('pro_id','$product_det->pro_id') }}        
                                             <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                             <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                             <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             @else
                                             <?php /* remove wishlist */?>   
                                             <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                             <i class="fa fa-heart" aria-hidden="true"></i> 
                                             </a> 
                                             <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                             @endif  
                                             @else 
                                             <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             @endif
                                          </div>
                                          <div class="mt-button quick-view"> <a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap_offer{{ $product_det->pro_id }}"> <i class="fa fa-search"></i> </a> </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item-info">
                                    <div class="info-inner">
                                       <div class="item-title"> <a title="" >{{ substr($product_det->$title,0,25) }}
                                          {{ strlen($product_det->$title)>25?'..':'' }}</a> 
                                       </div>
                                       <div class="item-content">
                                          {{-- 
                                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                          --}}
                                          <div class="item-price">
                                             <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>
                                          </div>
                                          <div class="pro-action">
                                             @if($product_det->pro_no_of_purchase >= $product_det->pro_qty)
                                             <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{  trans(Session::get('lang_file').'.SOLD') }}  @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span> </button>
                                             @else  
                                             @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                             <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                             <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button> </a>
                                             @endif
                                             @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                             <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">
                                             <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button> </a>
                                             @endif
                                             @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
                                             <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">
                                             <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button> </a>
                                             @endif
                                             @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')
                                             <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}">
                                             <button type="button" class="add-to-cart"><span> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }}  @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button>
                                             </a>
                                             @endif
                                             @endif
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endif
                           @endif
                           @endforeach
                           @else
                           <p> @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif </p>
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="row">
         <div class="daily-deal-section">
            <!-- daily deal section-->
            <div class="col-md-7 daily-deal">
               <h3 class="deal-title">@if (Lang::has(Session::get('lang_file').'.DEALS_OF_THE_DAY')!= '') {{ trans(Session::get('lang_file').'.DEALS_OF_THE_DAY') }}  @else {{ trans($OUR_LANGUAGE.'.DEALS_OF_THE_DAY') }} @endif</h3>
               <div class="title-divider"><span></span></div>
               <p>@if (Lang::has(Session::get('lang_file').'.GREAT_SAVING')!= '') {{  trans(Session::get('lang_file').'.GREAT_SAVING') }}  @else {{ trans($OUR_LANGUAGE.'.GREAT_SAVING') }} @endif</p>
               <div class="hot-offer-text">@if (Lang::has(Session::get('lang_file').'.GREAT_SALE')!= '') {{  trans(Session::get('lang_file').'.GREAT_SALE') }}  @else {{ trans($OUR_LANGUAGE.'.GREAT_SALE') }} @endif<span>{{ date('Y') }}</span></div>
               <div class="box-timer">
                  <span class="des-hot-deal">@if (Lang::has(Session::get('lang_file').'.HURRY_UP')!= '') {{  trans(Session::get('lang_file').'.HURRY_UP') }}  @else {{ trans($OUR_LANGUAGE.'.HURRY_UP') }} @endif</span>
                  {{-- deal timing--}}
                  @if(count($dealsof_day_details) > 0)
                  @foreach($dealsof_day_details as $fetch_most_visit_pro)
                  <?php   $deal_timing = array($fetch_most_visit_pro->deal_end_date); ?>
                  @endforeach
                  <div class="time" data-countdown="countdown" data-date="{{ date('m-d-Y-h-i-s',strtotime(max($deal_timing)))}}"></div>
                  <a href="{{ url('')."/deals"}}" class="link">@if (Lang::has(Session::get('lang_file').'.SHOP_NOW')!= '') {{ trans(Session::get('lang_file').'.SHOP_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.SHOP_NOW') }} @endif</a>
                  @else
                  <p> @if (Lang::has(Session::get('lang_file').'.NO_DEALS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_DEALS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_DEALS_AVAILABLE') }} @endif</p>
                  @endif
               </div>
            </div>
            <div class="col-md-5 hot-pr-img-area">
               <div id="daily-deal-slider" class="product-flexslider hidden-buttons">
                  <div class="owl-carousel owl-theme hot-pr-img-slider">
                     {{-- deal image--}}
                     @if(count($dealsof_day_details) > 0)
                     @foreach($dealsof_day_details as $fetch_most_visit_pro)
                     <?php
                        $mostproduct_img = explode('/**/', $fetch_most_visit_pro->deal_image);
                        $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));
                            $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));
                            $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));
                            $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 
                            $res = base64_encode($fetch_most_visit_pro->deal_id);
                        //Deal image/
                                        $prod_path = url('').'/public/assets/default_image/No_image_deal.png';
                                        
                                        $img_data = "public/assets/deals/".$mostproduct_img[0]; ?>
                     @if(file_exists($img_data) && $mostproduct_img[0] )  
                     @php $prod_path = url('').'/public/assets/deals/' .$mostproduct_img[0]; @endphp
                     @else  
                     @if(isset($DynamicNoImage['dealImg']))
                     @php $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg'];@endphp
                     @if($DynamicNoImage['dealImg']!='' && file_exists(trim($dyanamicNoImg_path)))
                     @php $prod_path = url('')."/".$dyanamicNoImg_path; @endphp @endif
                     @endif
                     @endif 
                     {{-- deal name--}}
                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                     @php  $deal_title = 'deal_title'; @endphp
                     @else  @php $deal_title = 'deal_title_langCode'; @endphp @endif
                     {{-- Alt text--}}
                     @php $alt_txt  = substr($fetch_most_visit_pro->$deal_title,0,25);
                     $alt_txt   .= strlen($fetch_most_visit_pro->$deal_title)>25?'..':''; @endphp
                     <div class="pr-img-area">
                        @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                        <a title="{{$alt_txt}}" href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                           <figure> <img class="first-img" src="{{ $prod_path}}" alt="{{$alt_txt}}"></figure>
                        </a>
                        @endif <!-- //if  -->
                        @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
                        <a title="{{$alt_txt}}" href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">
                           <figure> <img class="first-img" src="{{ $prod_path}}" alt="{{$alt_txt}}"></figure>
                        </a>
                        <!-- //if --> @endif
                        @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 
                        <a title="{{$alt_txt}}" href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">
                           <figure> <img class="first-img" src="{{ $prod_path}}" alt="{{$alt_txt}}"></figure>
                        </a>
                        @endif
                        @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')  
                        <a title="{{$alt_txt}}" href="{!! url('dealview').'/'.$mcat.'/'.$res!!}">
                           <figure> <img class="first-img" src="{{ $prod_path}}" alt="{{$alt_txt}}"></figure>
                        </a>
                        @endif
                     </div>
                     @endforeach
                     @else  {{-- no deal available --}}
                     @if(isset($DynamicNoImage['dealImg']))
                     @php $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg'];@endphp
                     @if($DynamicNoImage['dealImg']!='' && file_exists(trim($dyanamicNoImg_path)))
                     @php $prod_path = url('')."/".$dyanamicNoImg_path; @endphp @endif
                     @endif
                     <figure> <img class="first-img" src="{{ $prod_path}}" alt=""></figure>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="banner-section">
      <div class="container">
         <div class="row">
            @if(count($addetails) >0)
            @foreach($addetails as $adinfo) 
            {{-- For No image path--}}
            <?php $noimgpath="public/assets/noimage/No_image_1509364387_380x215.png"; ?>
            @if(isset($DynamicNoImage['ads']))
            @php $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['ads']; @endphp
            @if($DynamicNoImage['ads']!='' && file_exists(trim($dyanamicNoImg_path)))
            @php  $noimgpath =url('')."/".$dyanamicNoImg_path; @endphp @endif
            @endif
            {{-- For Ad image path--}} 
            <?php $imgpath="public/assets/adimage/".$adinfo->ad_img; ?>
            @php $adurl=$adinfo->ad_redirecturl;  @endphp
            {{-- For Ad name--}} 
            @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
            <?php $ad_name = 'ad_name'; ?>
            @else  
            @php 
            $ad_name = 'ad_name_'.Session::get('lang_code');  
            @endphp
            @endif
            <div class="col-sm-4">
               <a href="{{ $adurl}}" target="_blank" title="{{$adinfo->$ad_name}}">
                  @if(file_exists($imgpath))
                  <figure> <img src="{{ url('').'/'.$imgpath}}" alt="{{ $ad_name }}"></figure>
               </a>
               @else 
               <figure> <img src="{{ $noimgpath}}" alt="{{ $ad_name }}"></figure>
               </a>
               @endif  
            </div>
            @endforeach 
            @endif
            @if(count($addetails) == 1)
            <div class="col-sm-4">
               <figure> <img src="{{ $noimgpath}}" alt="ad_image"></figure>
            </div>
            <div class="col-sm-4">
               <figure> <img src="{{ $noimgpath}}" alt="ad_image"></figure>
            </div>
            @elseif(count($addetails) == 2)
            <div class="col-sm-4">
               <figure> <img src="{{ $noimgpath}}" alt=" ad_image "></figure>
            </div>
            @endif 
         </div>
      </div>
   </div>
   
   <div class="featured-products">
      <div class="container">
         <div class="row">
            <!-- Best Sale -->
            <div class="col-sm-12 col-md-4 jtv-best-sale">
               <div class="jtv-best-sale-list">
                  <div class="wpb_wrapper">
                     <div class="best-title text-left">
                        <h2>@if (Lang::has(Session::get('lang_file').'.TOP_RATE')!= '') {{ trans(Session::get('lang_file').'.TOP_RATE') }}  @else {{ trans($OUR_LANGUAGE.'.TOP_RATE') }} @endif</h2>
                     </div>
                  </div>
                  <div class="slider-items-products">
                     <div id="toprate-products-slider" class="product-flexslider">
                        <div class="owl-carousel owl-theme toprate-pr-slider">
                           <!-- start -->
                           @if(count($most_popular_product) != 0)
                           <?php $i=0;?>
                           @foreach($most_popular_product as $product_det) 
                           @if($i % 2 == 0 )  <?php  $ul = '<ul  class="products-grid">'; echo $ul;  ?> @endif
                           <li class="item">
                              <div class="item-inner">
                                 {{-- product name  --}}
                                 <?php 
                                    $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                                    $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                                    $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                                    $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                                    // product id  
                                    $res = base64_encode($product_det->pro_id);
                                    //product image  
                                    $product_image = explode('/**/',$product_det->pro_Img);
                                    //product price  
                                    $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                                    // product dicount percentage  ?>
                                 {{-- product title  --}} 
                                 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                                 @php
                                 $title = 'pro_title';
                                 @endphp
                                 @else 
                                 @php  
                                 $title = 'pro_title_langCode'; 
                                 @endphp 
                                 @endif
                                 {{-- product image --}}  
                                 @php 
                                 $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                                 $img_data = "public/assets/product/".$product_image[0];
                                 @endphp
                                 @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                                 @php 
                                 $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                                 @endphp       
                                 @else  
                                 @if(isset($DynamicNoImage['productImg']))
                                 @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                                 @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                                 @php
                                 $prod_path = url('')."/".$dyanamicNoImg_path;
                                 @endphp 
                                 @endif
                                 @endif
                                 @endif
                                 @php $discount_percent = $alt_text = ''; @endphp
                                 {{-- Alt text--}}
                                 @php 
                                 $alt_text   = substr($product_det->$title,0,25);
                                 $alt_text  .= strlen($product_det->$title)>25?'..':''; 
                                 @endphp
                                 <div class="item-img">
                                    @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 
                                    <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="{{$alt_text}}"> 
                                    <img alt="{{$alt_text}}" src="{{$prod_path}}">
                                    </a> 
                                    @endif <!-- /*//if*/ --> 
                                    @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                    <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="{{$alt_text}}">
                                    <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                                    @endif <!-- //if -->
                                    @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 
                                    <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="{{$alt_text}}">
                                    <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                                    @endif 
                                    @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                                    <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="{{$alt_text}}">
                                    <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a>
                                    @endif 
                                 </div>
                                 <div class="item-info">
                                    <div class="info-inner">
                                       <div class="item-title"> 
                                          {{ substr($product_det->$title,0,25) }}
                                          {{ strlen($product_det->$title)>25?'..':'' }} </a> 
                                       </div>
                                       {{-- 
                                       <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                       --}}
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                          @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                          <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          @endif
                                          @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                          <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" class="product-image">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          @endif
                                          @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                                          <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" class="product-image">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          @endif
                                          @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                                          <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" class="product-image">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          @endif
                                       </div>
                                       <div class="pr-button-hover">
                                          <div class="mt-button ="> 
                                             {{-- Add to wishlist--}}
                                             <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first();  ?>
                                             @if(Session::has('customerid'))
                                             @if(count($prodInWishlist)==0)
                                             <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                             {{ Form::hidden('pro_id','$product_det->pro_id') }}        
                                             <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                             <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                             <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             @else
                                             <?php /* remove wishlist */?>   
                                             <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                             <i class="fa fa-heart" aria-hidden="true"></i> 
                                             </a> 
                                             <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                             @endif  
                                             @else 
                                             <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             @endif
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <?php 
                              $i++;
                              if($i%2==0)
                              {
                                echo '</ul>';
                              }
                                          ?>
                           @endforeach
                           @else
                           <p> @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif </p>
                           @endif
                           <!-- end -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Banner -->
            <div class="col-sm-12 col-md-4 top-banner hidden-sm">
               <div class="jtv-banner3">
                  @if(count($product_details) != 0 && isset($product_details[2]))
                  @for($i = 0; $i < 1 ;$i++) 
                  {{-- product name  --}}
                  <?php 
                     $mcat = strtolower(str_replace(' ','-',$product_details[2]->mc_name));
                     $smcat = strtolower(str_replace(' ','-',$product_details[2]->smc_name));
                     $sbcat = strtolower(str_replace(' ','-',$product_details[2]->sb_name));
                     $ssbcat = strtolower(str_replace(' ','-',$product_details[2]->ssb_name)); 
                     // product id  
                     $res = base64_encode($product_details[2]->pro_id);
                     //product image  
                     $product_image = explode('/**/',$product_details[2]->pro_Img);
                     //product price  
                     $product_saving_price = $product_details[2]->pro_price - $product_details[2]->pro_disprice;
                     // product dicount percentage  
                     $product_discount_percentage = round(($product_saving_price/ $product_details[2]->pro_price)*100,2); ?>
                  {{-- product title  --}} 
                  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                  @php
                  $title = 'pro_title';
                  @endphp
                  @else 
                  @php  
                  $title = 'pro_title_langCode'; 
                  @endphp 
                  @endif
                  {{-- product image --}}  
                  @php 
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                  $img_data = "public/assets/product/".$product_image[0];
                  @endphp
                  @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                  @php 
                  $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                  @endphp       
                  @else  
                  @if(isset($DynamicNoImage['productImg']))
                  @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                  @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                  @php
                  $prod_path = url('')."/".$dyanamicNoImg_path;
                  @endphp 
                  @endif
                  @endif
                  @endif
                  @php $discount_percent = $alt_text = ''; @endphp
                  {{-- Alt text--}}
                  @php 
                  $alt_text   = substr($product_details[2]->$title,0,25);
                  $alt_text  .= strlen($product_details[2]->$title)>25?'..':''; @endphp
                  {{-- Discount percentage--}}
                  @if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
                  @php
                  $discount_percent = round($product_discount_percentage);
                  @endphp       
                  @endif
                  <div class="jtv-banner3-inner">
                     <img src="{{ $prod_path }}" alt="{{ $alt_text}}">
                     <div class="hover_content">
                        <div class="hover_data">
                           <div class="title">@if (Lang::has(Session::get('lang_file').'.BIG_SALE')!= '') {{ trans(Session::get('lang_file').'.BIG_SALE') }}  @else {{ trans($OUR_LANGUAGE.'.BIG_SALE') }} @endif
                           </div>
                           <div class="desc-text">@if (Lang::has(Session::get('lang_file').'.UP_TO')!= '') {{ trans(Session::get('lang_file').'.UP_TO') }}  @else {{ trans($OUR_LANGUAGE.'.UP_TO') }} @endif
                              {{ $discount_percent }} @if (Lang::has(Session::get('lang_file').'.OFF')!= '') {{ trans(Session::get('lang_file').'.OFF') }}  @else {{ trans($OUR_LANGUAGE.'.OFF') }} @endif
                           </div>
                           @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                           <p> <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a>
                           </p>
                           @endif <!-- /*//if*/ --> 
                           @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
                           <p>   <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a> 
                           </p>
                           @endif <!-- //if -->
                           @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
                           <p> <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a> 
                           </p>
                           @endif 
                           @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')
                           <p>  <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" title="<?php echo $alt_text; ?>" class="shop-now">
                              @if (Lang::has(Session::get('lang_file').'.GET_NOW')!= '') {{ trans(Session::get('lang_file').'.GET_NOW') }}  @else {{ trans($OUR_LANGUAGE.'.GET_NOW') }} @endif
                              </a> 
                           </p>
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
               @endfor
               @else
               @php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               @endphp
               @if(isset($DynamicNoImage['productImg']))
               @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
               @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
               @php
               $prod_path = url('')."/".$dyanamicNoImg_path;
               @endphp 
               @endif
               @endif
               <div class="jtv-banner3-inner"><img src="{{ $prod_path }}" alt=""> </div>
            </div>
            @endif
         </div>
         <div class="col-sm-12 col-md-4 jtv-best-sale">
            <div class="jtv-best-sale-list">
               <div class="wpb_wrapper">
                  <div class="best-title text-left">
                     <h2>@if (Lang::has(Session::get('lang_file').'.NEW_PRODUCT')!= '') {{ trans(Session::get('lang_file').'.NEW_PRODUCT') }}  @else {{ trans($OUR_LANGUAGE.'.NEW_PRODUCT') }} @endif</h2>
                  </div>
               </div>
               <div class="slider-items-products">
                  <div id="new-products-slider" class="product-flexslider">
                     <div class="owl-carousel owl-theme new-pr-slider">
                        @if(count($new_product) != 0)
                        <?php $i = 0; ?>
                        @foreach($new_product as $product_det) 
                        @if($i % 2 == 0 )  <?php  $ul = '<ul  class="products-grid">'; echo $ul;  ?> @endif
                        <li class="item">
                           <div class="item-inner">
                              {{-- product name  --}}
                              <?php 
                                 $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                                 $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                                 $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                                 $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                                 // product id  
                                 $res = base64_encode($product_det->pro_id);
                                 //product image  
                                 $product_image = explode('/**/',$product_det->pro_Img);
                                 //product price  
                                 $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                                 // product dicount percentage  ?>
                              {{-- product title  --}} 
                              @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
                              @php
                              $title = 'pro_title';
                              @endphp
                              @else 
                              @php  
                              $title = 'pro_title_langCode'; 
                              @endphp 
                              @endif
                              {{-- product image --}}  
                              @php 
                              $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                              $img_data = "public/assets/product/".$product_image[0];
                              @endphp
                              @if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
                              @php 
                              $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                              @endphp       
                              @else  
                              @if(isset($DynamicNoImage['productImg']))
                              @php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                              @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                              @php
                              $prod_path = url('')."/".$dyanamicNoImg_path;
                              @endphp 
                              @endif
                              @endif
                              @endif
                              @php $discount_percent = $alt_text = ''; @endphp
                              {{-- Alt text--}}
                              @php 
                              $alt_text   = substr($product_det->$title,0,25);
                              $alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp
                              <div class="item-img">
                                 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 
                                 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                 <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                                 @endif <!-- /*//if*/ --> 
                                 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" >
                                 <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                                 @endif <!-- //if -->
                                 @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 
                                 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" >
                                 <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                                 @endif 
                                 @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                                 <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" >
                                 <img alt="{{$alt_text}}" src="{{$prod_path}}"> </a> 
                                 @endif
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> 
                                     @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                       {{ substr($product_det->$title,0,25) }}
                                       {{ strlen($product_det->$title)>25?'..':'' }}</a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">
                                       {{ substr($product_det->$title,0,25) }}
                                       {{ strlen($product_det->$title)>25?'..':'' }}</a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">
                                       {{ substr($product_det->$title,0,25) }}
                                       {{ strlen($product_det->$title)>25?'..':'' }}</a>
                                       @endif
                                       @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}">
                                       {{ substr($product_det->$title,0,25) }}
                                       {{ strlen($product_det->$title)>25?'..':'' }}</a>
                                       @endif
                                     
                                    </div>
                                    {{--  
                                    <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                    --}}
                                    <div class="item-price">
                                       <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>
                                    </div>
                                    <div class="pro-action">
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       @endif
                                       @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 
                                       <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       @endif
                                    </div>
                                    <div class="pr-button-hover">
                                       <div class="mt-button"> 
                                          {{-- Add to wishlist--}}
                                          <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>
                                          @if(Session::has('customerid'))
                                          @if(count($prodInWishlist)==0)
                                          <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                          {{ Form::hidden('pro_id','$product_det->pro_id') }}        
                                          <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                          <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                          <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                                          <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                          </a>
                                          @else
                                          <?php /* remove wishlist */?>   
                                          <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                          <i class="fa fa-heart" aria-hidden="true"></i> 
                                          </a> 
                                          <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                          @endif  
                                          @else 
                                          <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                          <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                          </a>
                                          @endif
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <?php 
                           $i++;
                           if($i%2==0)
                           {
                            echo '</ul>';
                           }
                                       ?>
                        @endforeach
                        @else
                        <p> @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif </p>
                        @endif
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- our clients Slider -->
<?php /* <div class="container">
   <div class="row">
     <div class="col-md-12 col-xs-12">
       <div class="our-clients">
         <div class="slider-items-products">
           <div id="our-clients-slider" class="product-flexslider hidden-buttons">
             <div class="slider-items slider-width-col6">
               <div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand1.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand2.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand3.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand4.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand5.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand6.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand7.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand8.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand9.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand10.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand11.png" alt="Image"></a> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> */ ?>
<!-- BANNER-AREA-START -->
<section class="banner-area">
   <div class="container">
   <div class="best-title text-left">
      <h2>@if (Lang::has(Session::get('lang_file').'.FEATURED_STORE')!= '') {{ trans(Session::get('lang_file').'.FEATURED_STORE') }}  @else {{ trans($OUR_LANGUAGE.'.FEATURED_STORE') }} @endif</h2>
   </div>
      @if(count($get_store_details)>0) 
      <div class="row">
         @foreach($get_store_details as $store_details)
         {{-- Store name,country nmae,City name--}}
         <?php $stor_name = '';?>
         @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
         @php  $stor_name = 'stor_name'; 
         $co_name = 'co_name';
         $ci_name = 'ci_name';
         @endphp
         @else 
         @php  
         $stor_name = 'stor_name_'.Session::get('lang_code'); 
         $co_name = 'co_name_'.Session::get('lang_code'); 
         $ci_name = 'ci_name_'.Session::get('lang_code'); 
         @endphp 
         @endif
         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="banner-block">
               {{-- store name--}}
               <?php $st_name = DB::table('nm_store')->select('*')->where('stor_id','=',$store_details->stor_id)->first(); ?>
               {{-- Store image--}}
               @php
               $product_image     = $store_details->stor_img;
               $prod_path  = url('').'/public/assets/default_image/No_image_store.png';
               $img_data   = "public/assets/storeimage/".$product_image; @endphp
               @if(file_exists($img_data) && $product_image !='')   {{-- Image exists--}}
               @php $prod_path = url('').'/public/assets/storeimage/' .$product_image;  @endphp              
               @else  {{-- Image not exists --}}
               @if(isset($DynamicNoImage['store']))
               @php  $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; @endphp
               @if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))
               @php $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
               @endif
               @endif
               <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($store_details->stor_id)))); ?>"> <img src="{{$prod_path}}" alt="banner sunglasses"> </a>
               <div class="text-des-container">
                  <div class="text-des">
                     <h2>{{$st_name->$stor_name}}</h2>
                     <p>{{$st_name->stor_slogan }} </p>
                  </div>
               </div>
            </div>
         </div>
         @endforeach
      </div>
      @else
      <p> @if (Lang::has(Session::get('lang_file').'.NO_STORES_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_STORES_FOUND') }}  @else {{ trans($OUR_LANGUAGE.'.NO_STORES_FOUND') }} @endif </p>
      @endif
   </div>
</section>
<!-- BANNER-AREA-END -->
<!-- Blog -->
   <section class="blog-post-wrapper">
      <div class="container">
         <div class="best-title text-left">
            <h2>@if (Lang::has(Session::get('lang_file').'.BLOG')!= '') {{ trans(Session::get('lang_file').'.BLOG') }}  @else {{ trans($OUR_LANGUAGE.'.BLOG') }} @endif</h2>
         </div>
         <div class="slider-items-products">
            <div id="latest-news-slider" class="product-flexslider hidden-buttons">
               <div class="owl-carousel owl-theme ltst-nws-slider">
                  @if(count($get_blog_list) > 0)  
                  @foreach($get_blog_list as $fetchblog_list) 
                  {{-- blog title--}} 
                  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                  @php  $blog_title = 'blog_title'; @endphp
                  @else
                  @php  $blog_title = 'blog_title_'.Session::get('lang_code'); @endphp
                  @endif
                  {{-- blog created date --}} 
                  @php   $created_date =  $fetchblog_list->blog_created_date;
                  $explode_date = explode(" ",$created_date);
                  $date =  date(' jS M Y', strtotime($explode_date[0]));
                  @endphp
                  {{-- Blog image--}}
                  @php  $product_image     = $fetchblog_list->blog_image;
                  $prod_path  = url('').'/public/assets/default_image/No_image_blog.png';
                  $img_data   = "public/assets/blogimage/".$product_image; @endphp
                  @if(file_exists($img_data) && $product_image!='' )   
                  @php   $prod_path = url('public/assets/blogimage/').'/'.$product_image; @endphp                 
                  @else  
                  @if(isset($DynamicNoImage['blog'])) 
                  @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['blog']; @endphp
                  @if($DynamicNoImage['blog']!='' && file_exists($dyanamicNoImg_path)) 
                  @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
                  @endif
                  @endif
                  {{-- Nlog description --}}
                  @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                  @php  $blog_desc = 'blog_desc'; @endphp
                  @else
                  @php  $blog_desc = 'blog_desc_'.Session::get('lang_code'); @endphp
                  @endif
                  <div class="item">
                     <div class="blog-box">
                        <a href="{{ url('blog_view/'.$fetchblog_list->blog_id) }}" title="{{ $fetchblog_list->$blog_title}}"> <img class="primary-img" src="{{ $prod_path }}" </a>
                        <div class="blog-btm-desc">
                           <div class="blog-top-desc">
                              <div class="blog-date"> {{$date}} </div>
                              <!--   <h5><a href="{{ url('blog_view/'.$fetchblog_list->blog_id) }}" {{ $fetchblog_list->$blog_title}}</a></h5> -->
                              <div class="jtv-entry-meta">
                                 <div class="hom-blog-title"><a href="{{ url('blog_view/'.$fetchblog_list->blog_id) }}">
                                    @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                                    @php    $blog_title = 'blog_title'; @endphp
                                    @else @php  $blog_title = 'blog_title_'.Session::get('lang_code'); @endphp @endif
                                    {{ $fetchblog_list->$blog_title }}
                                    </a>
                                 </div>
                                 <i class="fa fa-user-o"></i>
                                 <strong>@if (Lang::has(Session::get('lang_file').'.ADMIN')!= '') {{  trans(Session::get('lang_file').'.ADMIN') }} @else {{ trans($OUR_LANGUAGE.'.ADMIN') }} @endif</strong> 
                                 <a href="{{ url('blog_comment/'. $fetchblog_list->blog_id) }}"><i class="fa fa-commenting-o"></i> <strong>
                                 {{ $get_blog_list_count[$fetchblog_list->blog_id] }} 
                                 @if (Lang::has(Session::get('lang_file').'.COMMENTS')!= '') {{ trans(Session::get('lang_file').'.COMMENTS') }} @else {{ trans($OUR_LANGUAGE.'.COMMENTS') }} @endif
                                 </strong></a>
                              </div>
                           </div>
                           <div style="display: block; width: 100%; clear: both;">
                              <p>{{ substr($fetchblog_list->$blog_desc,0,100) }} {{ (strlen($fetchblog_list->$blog_desc) > 100) ?'..':'' }}</p>
                           </div>
                           <a class="read-more" href="{{ url('blog_view/'.$fetchblog_list->blog_id) }}"> @if (Lang::has(Session::get('lang_file').'.CONTINUE_READING')!= '') {{  trans(Session::get('lang_file').'.CONTINUE_READING') }} @else {{ trans($OUR_LANGUAGE.'.CONTINUE_READING') }} @endif</a> 
                        </div>
                     </div>
                  </div>
                  @endforeach
                  @else
                  <p>
                     @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif 
                  </p>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </section>
<div class="footer-newsletter">
   <div class="container">
      <div class="row">
         <!-- Newsletter -->
         <div class="col-md-6 col-sm-6">
            <h3>@if (Lang::has(Session::get('lang_file').'.JOIN_NEWSLETTER')!= '') {{ trans(Session::get('lang_file').'.JOIN_NEWSLETTER') }}  @else {{ trans($OUR_LANGUAGE.'.JOIN_NEWSLETTER') }} @endif</h3>
            <div class="title-divider"><span></span></div>
            <span class="sub-text">@if (Lang::has(Session::get('lang_file').'.ENTR_MAIL')!= '') {{ trans(Session::get('lang_file').'.ENTR_MAIL') }}  @else {{ trans($OUR_LANGUAGE.'.ENTR_MAIL') }} @endif
            </span>
            <p class="sub-title text-center">@if (Lang::has(Session::get('lang_file').'.LATEST_NEWS')!= '') {{ trans(Session::get('lang_file').'.LATEST_NEWS') }}  @else {{ trans($OUR_LANGUAGE.'.LATEST_NEWS') }} @endif</p>
            <span class="sub-text1">@if (Lang::has(Session::get('lang_file').'.TO_INBOX')!= '') {{ trans(Session::get('lang_file').'.TO_INBOX') }}  @else {{ trans($OUR_LANGUAGE.'.TO_INBOX') }} @endif
            </span>
            <div class="newsletter-inner">
               <input class="newsletter-email" id="sub_email" type="email" required name='email' placeholder='@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION')!= '') {{ trans(Session::get('lang_file').'.ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION') }} @endif'/>
               <button class="button subscribe" id="subscribe_submit" title="Subscribe">@if(Lang::has(Session::get('lang_file').'.SUBSCRIBE')!= '') {{  trans(Session::get('lang_file').'.SUBSCRIBE') }} @else {{ trans($OUR_LANGUAGE.'.SUBSCRIBE') }} @endif</button></a>
               <div class="mail-loader"> <img src="<?php echo url('')?>/images/loader.gif"></div>
            </div>
         </div>
         <!-- Customers Box -->
         <div class="col-sm-6 col-xs-12 testimonials">
            <div class="page-header">
               <h2>@if (Lang::has(Session::get('lang_file').'.CUS_COMMENT')!= '') {{ trans(Session::get('lang_file').'.CUS_COMMENT') }}  @else {{ trans($OUR_LANGUAGE.'.CUS_COMMENT') }} @endif</h2>
               <div class="title-divider"><span></span></div>
            </div>
            <div class="slider-items-products">
               <div id="testimonials-slider" class="product-flexslider hidden-buttons home-testimonials">
                  <div class="owl-carousel owl-theme testimonial-slider">
                     @if(count($review_details)> 0)
                     @foreach($review_details as $review)
                     {{-- Store name--}}
                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                     @php  $stor_name = 'stor_name'; 
                     $ci_name = 'ci_name';
                     @endphp
                     @else 
                     @php  
                     $stor_name = 'stor_name_'.Session::get('lang_code'); 
                     $ci_name = 'ci_name_'.Session::get('lang_code'); 
                     @endphp 
                     @endif
                     <?php $store_details = DB::table('nm_store')->select($stor_name,'nm_city.'.$ci_name)->leftJoin('nm_city','nm_store.stor_city','=','nm_city.ci_id')->where('stor_id','=',$review->store_id)->first();
                        ?>
                     <div class="holder">
                        <blockquote>{{ strip_tags(substr($review->comments,0,25)) }}
                           {{ strip_tags(strlen($review->comments))>25?'..':'' }}
                        </blockquote>
                        <div class="thumb">
                           {{--Customer picture--}}

                         <?php  if($review->cus_pic !="") {
   
                                    if(file_exists('public/assets/profileimage/'.$review->cus_pic)){
                  
                                       $imgpath = url('').'/public/assets/profileimage/'.$review->cus_pic;   
                  
                                    } else {
                  
                                       $imgpath =  url('')."/themes/images/products/man.png";
                  
                                    }
                  
                                 } else {
                  
                                    $imgpath =  url('')."/themes/images/products/man.png";
                  
                                 }  ?>
                          
                           <img src="{{ $imgpath}}" alt="testimonials img"> 
                        </div>
                        <div class="holder-info"> <strong class="name">{{ $review->cus_name }}</strong> <strong class="designation">
                           @if($store_details)
                           {{ substr($store_details->$stor_name,0,25) }}
                           store,{{$store_details->$ci_name }}
                           @endif
                        </strong>
                        </div>
                     </div>
                     @endforeach
                     @else
                     <p>@if (Lang::has(Session::get('lang_file').'.NO_REVIEWS_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_REVIEWS_FOUND') }} @else {{ trans($OUR_LANGUAGE.'.NO_REVIEWS_FOUND') }} @endif
                     </p>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- MicFooter -->
{!! $footer !!}
<!--quick view best rated--> 
@if(count($product_details) != 0)
@foreach($product_details as $product_det) 
{{-- product name  --}}
<?php 
   $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
   $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
   $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
   $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
   // product id  
   $res = base64_encode($product_det->pro_id);
   //product image  
   $product_image = explode('/**/',$product_det->pro_Img);
   //product price  
   $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
   // product dicount percentage  
   $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
@php   $product_img= explode('/**/',trim($product_det->pro_Img,"/**/")); 
$img_count = count($product_img); @endphp
{{-- product title  --}} 
@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
@php
$title = 'pro_title';
@endphp
@else 
@php  
$title = 'pro_title_langCode'; 
@endphp 
@endif
{{-- product image --}}  
@php 
$prod_path = url('').'/public/assets/default_image/No_image_product.png';
$img_data = "public/assets/product/".$product_image[0];
@endphp
@if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
@php 
$prod_path = url('').'/public/assets/product/' .$product_image[0]; 
@endphp       
@else  
@if(isset($DynamicNoImage['productImg']))
@php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
@php
$prod_path = url('')."/".$dyanamicNoImg_path;
@endphp 
@endif
@endif
@endif
@php $discount_percent = $alt_text = ''; @endphp
{{-- Alt text--}}
@php 
$alt_text   = substr($product_det->$title,0,25);
$alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp
{{-- Discount percentage--}}
@if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
@php
$discount_percent = round($product_discount_percentage);
@endphp       
@endif
@php $count = $product_det->pro_qty - $product_det->pro_no_of_purchase; @endphp
<input type="hidden" id="pro_qty_hidden_{{ $product_det->pro_id }}" name="pro_qty_hidden" value="<?php echo  $product_det->pro_qty; ?>" />
<input type="hidden" id="pro_purchase_hidden_{{ $product_det->pro_id }}" name="pro_purchase_hidden" value="<?php echo  $product_det->pro_no_of_purchase; ?>" />
<div style="display: none;"  class="quick_view_popup-wrap hh" id="quick_view_popup-wrap{{$product_det->pro_id}}">
   <div id="quick_view_popup-overlay"></div>
   <div id="quick_view_popup-outer">
      <div id="quick_view_popup-content">
         <div style="width:auto;height:auto;overflow: auto;position:relative;">
            <div class="product-view-area">
               <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                  <div class="large-image"> 
                     <a href="{{$prod_path}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="{{$prod_path}}"> </a>
                  </div>
                  <div class="flexslider flexslider-thumb">
                     <ul class="previews-list slides">
                        @for($i=0;$i <$img_count;$i++)
                        @php  $product_image     = $product_img[$i];
                        $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_image; @endphp
                        @if(file_exists($img_data) && $product_image !='') <!-- //product image is not null and exists in folder -->
                        @php $prod_path = url('').'/public/assets/product/' .$product_image;  @endphp                 
                        @else  
                        @if(isset($DynamicNoImage['productImg']))
                        @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp
                        @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)) <!-- //no image for product is not null and exists in folder -->
                        @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp
                        @endif
                        @endif
                        @endif 
                        <li style="width: 100px; float: left; display: block;"><a href='{{$prod_path}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$prod_path}}' "><img src="{{$prod_path}}" alt = "Thumbnail 2"/></a></li>
                        @endfor
                     </ul>
                  </div>
                  <!-- end: more-images --> 
               </div>
               <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
                  <div class="product-details-area">
                     <div class="product-name">
                        <h1>{{ substr($product_det->$title,0,25) }}
                           {{  strlen($product_det->$title)>25?'..':'' }}
                        </h1>
                     </div>
                     <div class="price-box">
                        <p class="special-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $product_det->pro_disprice }} </span> </p>
                        <p class="old-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $product_det->pro_price }} </span> </p>
                     </div>
                     <div class="ratings">
                        <div class="rating"> 
                           @php           
                           $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();
                           $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();
                           $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();
                           $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();
                           $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();
                           $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                           $multiple_countone = $one_count *1;
                           $multiple_counttwo = $two_count *2;
                           $multiple_countthree = $three_count *3;
                           $multiple_countfour = $four_count *4;
                           $multiple_countfive = $five_count *5;
                           $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp
                           {{-- product rating--}}
                           @php
                           $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                           $multiple_countone   = $one_count *1;
                           $multiple_counttwo   = $two_count *2;
                           $multiple_countthree = $three_count *3;
                           $multiple_countfour  = $four_count *4;
                           $multiple_countfive  = $five_count *5;
                           $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp
                           @if($product_count)
                           @php  $product_divide_count = $product_total_count / $product_count;
                           $product_divide_count = round($product_divide_count); @endphp
                           @if($product_divide_count <= '1')
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           @elseif($product_divide_count >= '1') 
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           @elseif($product_divide_count >= '2')
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  
                           @elseif($product_divide_count >= '3') 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 
                           @elseif($product_divide_count >= '4') 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                           @elseif($product_divide_count >= '5') 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                           @else
                           @endif
                           @else
                           <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           @endif 
                        </div>
                        <p class="availability in-stock pull-right"> @if (Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= '') {{ trans(Session::get('lang_file').'.AVAILABLE_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE_STOCK') }} @endif: <span>{{ $product_det->pro_qty-$product_det->pro_no_of_purchase }}  @if (Lang::has(Session::get('lang_file').'.IN_STOCK')!= '') {{  trans(Session::get('lang_file').'.IN_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.IN_STOCK') }} @endif</span></p>
                     </div>
                     <div class="short-description">
                        <h2>@if (Lang::has(Session::get('lang_file').'.OVERVIEW')!= '') {{ trans(Session::get('lang_file').'.OVERVIEW') }} @else {{ trans($OUR_LANGUAGE.'.OVERVIEW') }} @endif</h2>
                        {{-- Product description --}}
                        @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                        @php  $pro_desc = 'pro_desc'; @endphp
                        @else
                        @php  
                        $pro_desc = 'pro_desc_langCode'; @endphp 
                        @endif
                        <div class="micheal"><?php //print_r($product_det); ?></div>
                        <!-- <?php $convertString = $product_det->$pro_desc; 
                              $new_str //= str_replace("&nbsp;", '', $convertString);
                               //echo $new_str;
                        ?> -->
                        <p> <!-- {!!  html_entity_decode(substr($product_det->$pro_desc,0,200)) !!} --> {!! $product_det->$pro_desc !!} </p>
                     </div>
                     @php  $product_color_detail = DB::table('nm_procolor')->where('pc_pro_id', '=',     $product_det->pro_id)->LeftJoin('nm_color', 'nm_color.co_id', '=', 'nm_procolor.pc_co_id')->get();
                     $product_size_detail = DB::table('nm_prosize')->where('ps_pro_id', '=', $product_det->pro_id)->LeftJoin('nm_size', 'nm_size.si_id', '=', 'nm_prosize.ps_si_id')->get(); @endphp
                     <div class="product-color-size-area">
                        @if(count($product_color_detail)>0)
                        <div class="color-area">
                           <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.COLOR')!= '') {{ trans(Session::get('lang_file').'.COLOR') }} @else {{ trans($OUR_LANGUAGE.'.COLOR') }} @endif </h2>
                           <div class="color">
                              <select name="addtocart_color" id="addtocart_color_{{ $product_det->pro_id }}" required>
                                 <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') {{ trans(Session::get('lang_file').'.SELECT_COLOR') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_COLOR') }} @endif--</option>
                                 @foreach($product_color_detail as $product_color_det) 
                                 <option value="{{ $product_color_det->co_id }}">
                                    {{ $product_color_det->co_name }}
                                 </option>
                                 @endforeach 
                              </select>
                           </div>
                        </div>
                        @endif
                        @if(count($product_size_detail) > 0)
                        @php  $size_name = $product_size_detail[0]->si_name;
                        $return  = strcmp($size_name,'no size');  @endphp
                        @if($return!=0) 
                        <div class="size-area">
                           <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.SIZE')!= '') {{ trans(Session::get('lang_file').'.SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SIZE') }} @endif</h2>
                           <div class="size">
                              <select name="addtocart_size" id="addtocart_size_{{ $product_det->pro_id }}" required>
                                 <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') {{ trans(Session::get('lang_file').'.SELECT_SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_SIZE') }} @endif--</option>
                                 @foreach($product_size_detail as $product_size_det) 
                                 <option value="{{ $product_size_det->ps_si_id }}">
                                    {{ $product_size_det->si_name }}
                                 </option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        @else 
                        <input type="hidden" name="addtocart_size" value="{{ $product_size_detail[0]->ps_si_id }}">
                        @endif
                        @endif
                     </div>
                     <div class="product-variation">
                        {!! Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data','id'=>'submit_form')) !!}
                        <form action="#" method="post">
                        <div class="cart-plus-minus">
                           <label for="qty">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.QUANTITY') }} @endif :</label>
                           <div class="numbers-row">
                              <div onClick="remove_quantity(<?php echo $product_det->pro_id; ?>)" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                              <input type="number" class="qty" min="1" value="1" max="{{  ($product_det->pro_qty - $product_det->pro_no_of_purchase) }}" id="addtocart_qty_{{ $product_det->pro_id }}" name="addtocart_qty" readonly required >
                              <div onClick="add_quantity(<?php echo $product_det->pro_id; ?>)" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                           </div>
                        </div>
                        {{-- Add to cart--}}
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        {{ Form::hidden('addtocart_type','product')}}
                        {{ Form::hidden('addtocart_pro_id',$product_det->pro_id)}}
                        @if(Session::has('customerid')) 
                        @if($count > 0)
                        <button onclick="addtocart_validate('<?php echo $product_det->pro_id; ?>');"  class="button pro-add-to-cart" title="Add to Cart" type="button" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button>
                        @else 
                        <button type="button" class="btn btn-danger">
                        @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif
                        </button> 
                        @endif 
                        @else 
                        @if($count > 0)
                        <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                        <button type="button" class=" button pro-add-to-cart">
                        <span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                        @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span>
                        </button> 
                        </a>
                        @else
                        <button type="button" class="btn btn-danger">
                        @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif
                        </button> 
                        @endif 
                        @endif
                        {{ Form::close() }}
                     </div>
                     <div class="product-cart-option">
                        <ul>
                           <li>
                              {{-- Add to wishlist--}}
                              @if(Session::has('customerid'))
                              @php  
                              $cus_id = Session::get('customerid');
                              $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',$cus_id)->first(); @endphp
                              @else
                              @php  $prodInWishlist = array(); @endphp
                              @endif
                              @if($count > 0)  
                              @if(Session::has('customerid'))
                              @if(count($prodInWishlist)==0)
                              <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                              {{ Form::hidden('pro_id','$product_det->pro_id') }}        
                              <!-- <input type="hidden" name="pro_id" value="<?php echo $product_det->pro_id; ?>"> -->
                              <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                              <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})">
                              <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                              <i class="fa fa-heart-o" aria-hidden="true"></i>@if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif 
                              </a>
                              @else
                              <?php /* remove wishlist */?>   
                              <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}">
                              <i class="fa fa-heart" aria-hidden="true"></i>    @if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST') }} @endif
                              </a> 
                              <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                              @endif  
                              @else 
                              <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                              <i class="fa fa-heart-o" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif
                              </a>
                              @endif
                              @endif
                           </li>
                           {{-- 
                           <li><a href="#"><i class="fa fa-link"></i><span>Add to Compare</span></a></li>
                           <li><a href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
                           --}}
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--product-view--> 
         </div>
      </div>
      <a style="display: inline;" id="quick_view_popup-close" href="{{ url('')}}/index"><i class="icon pe-7s-close"></i></a> 
   </div>
</div>
@endforeach
@endif
<!--end qiuck view best rated-->
@if(count($product_details) != 0)
@foreach($product_details as $product_det) 
{{-- product name  --}}
<?php 
   $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
   $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
   $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
   $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
   // product id  
   $res = base64_encode($product_det->pro_id);
   //product image  
   $product_image = explode('/**/',$product_det->pro_Img);
   //product price  
   $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
   // product dicount percentage  
   $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
@php  $product_img= explode('/**/',trim($product_det->pro_Img,"/**/")); 
$img_count = count($product_img); @endphp
@if($product_discount_percentage<= 50)
@if($product_det->pro_no_of_purchase < $product_det->pro_qty) 
{{-- product title  --}} 
@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')
@php
$title = 'pro_title';
@endphp
@else 
@php  
$title = 'pro_title_langCode'; 
@endphp 
@endif
{{-- product image --}}  
@php 
$prod_path = url('').'/public/assets/default_image/No_image_product.png';
$img_data = "public/assets/product/".$product_image[0];
@endphp
@if(file_exists($img_data) && $product_image[0] !='' ) {{-- Image exists---}}
@php 
$prod_path = url('').'/public/assets/product/' .$product_image[0]; 
@endphp       
@else  
@if(isset($DynamicNoImage['productImg']))
@php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
@php
$prod_path = url('')."/".$dyanamicNoImg_path;
@endphp 
@endif
@endif
@endif
@php $discount_percent = $alt_text = ''; @endphp
{{-- Alt text--}}
@php 
$alt_text   = substr($product_det->$title,0,25);
$alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp
{{-- Discount percentage--}}
@if($product_discount_percentage!='' && round($product_discount_percentage)!=0)
@php
$discount_percent = round($product_discount_percentage);
@endphp       
@endif
@php $count = $product_det->pro_qty - $product_det->pro_no_of_purchase; @endphp
<input type="hidden" id="pro_qty_hidden_offer_{{ $product_det->pro_id }}" name="pro_qty_hidden_offer" value="<?php echo  $product_det->pro_qty; ?>" />
<input type="hidden" id="pro_purchase_hidden_offer_{{ $product_det->pro_id }}" name="pro_purchase_hidden_offer" value="<?php echo  $product_det->pro_no_of_purchase; ?>" />
<div style="display:none;"  class="quick_view_popup-wrap" id="quick_view_popup-wrap_offer{{ $product_det->pro_id }}">
   <div id="quick_view_popup-overlay"></div>
   <div id="quick_view_popup-outer">
      <div id="quick_view_popup-content">
         <div style="width:auto;height:auto;overflow: auto;position:relative;">
            <div class="product-view-area">
               <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                  <div class="large-image"> 
                     <a href="{{$prod_path}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="{{$prod_path}}"> </a> 
                  </div>
                  <div class="flexslider flexslider-thumb">
                     <ul class="previews-list slides">
                        @for($i=0;$i <$img_count;$i++)
                        @php  $product_image     = $product_img[$i];
                        $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_image; @endphp
                        @if(file_exists($img_data) && $product_image !='') <!-- //product image is not null and exists in folder -->
                        @php $prod_path = url('').'/public/assets/product/' .$product_image;  @endphp                 
                        @else  
                        @if(isset($DynamicNoImage['productImg']))
                        @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp
                        @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)) <!-- //no image for product is not null and exists in folder -->
                        @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp
                        @endif
                        @endif
                        @endif
                        <li style="width: 100px; float: left; display: block;"><a href='{{$prod_path}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$prod_path}}' "><img src="{{$prod_path}}" alt = "Thumbnail 2"/></a></li>
                        @endfor
                     </ul>
                  </div>
                  <!-- end: more-images --> 
               </div>
               <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
                  <div class="product-details-area">
                     <div class="product-name">
                        <h1>{{ substr($product_det->$title,0,25) }}
                           {{  strlen($product_det->$title)>25?'..':'' }}
                        </h1>
                     </div>
                     <div class="price-box">
                        <p class="special-price"> <span class="price-label"></span> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }} </span> </p>
                        <p class="old-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $product_det->pro_price }} </span> </p>
                     </div>
                     <div class="ratings">
                        @php            
                        $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();
                        $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();
                        $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();
                        $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();
                        $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();
                        $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                        $multiple_countone = $one_count *1;
                        $multiple_counttwo = $two_count *2;
                        $multiple_countthree = $three_count *3;
                        $multiple_countfour = $four_count *4;
                        $multiple_countfive = $five_count *5;
                        $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp
                        <div class="rating"> 
                           {{-- product rating--}}
                           @php
                           $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                           $multiple_countone   = $one_count *1;
                           $multiple_counttwo   = $two_count *2;
                           $multiple_countthree = $three_count *3;
                           $multiple_countfour  = $four_count *4;
                           $multiple_countfive  = $five_count *5;
                           $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp
                           @if($product_count)
                           @php  $product_divide_count = $product_total_count / $product_count;
                           $product_divide_count = round($product_divide_count); @endphp
                           @if($product_divide_count <= '1')
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           @elseif($product_divide_count >= '1') 
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           @elseif($product_divide_count >= '2')
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  
                           @elseif($product_divide_count >= '3') 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 
                           @elseif($product_divide_count >= '4') 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                           @elseif($product_divide_count >= '5') 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                           @else
                           @endif
                           @else
                           <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           @endif 
                        </div>
                        <p class="availability in-stock pull-right"> @if (Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= '') {{ trans(Session::get('lang_file').'.AVAILABLE_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE_STOCK') }} @endif: <span>{{ $product_det->pro_qty-$product_det->pro_no_of_purchase }}  @if (Lang::has(Session::get('lang_file').'.IN_STOCK')!= '') {{  trans(Session::get('lang_file').'.IN_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.IN_STOCK') }} @endif</span></p>
                     </div>
                     <div class="short-description">
                        <h2>@if (Lang::has(Session::get('lang_file').'.OVERVIEW')!= '') {{ trans(Session::get('lang_file').'.OVERVIEW') }} @else {{ trans($OUR_LANGUAGE.'.OVERVIEW') }} @endif</h2>
                        {{-- Product description --}}
                        @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                        @php  $pro_desc = 'pro_desc'; @endphp
                        @else
                        @php  
                        $pro_desc = 'pro_desc_langCode'; @endphp 
                        @endif
                        <p>{!! html_entity_decode(substr($product_det->$pro_desc,0,200)) !!}</p>
                     </div>
                     @php  $product_color_detail = DB::table('nm_procolor')->where('pc_pro_id', '=',     $product_det->pro_id)->LeftJoin('nm_color', 'nm_color.co_id', '=', 'nm_procolor.pc_co_id')->get();
                     $product_size_detail = DB::table('nm_prosize')->where('ps_pro_id', '=', $product_det->pro_id)->LeftJoin('nm_size', 'nm_size.si_id', '=', 'nm_prosize.ps_si_id')->get(); @endphp
                     <div class="product-color-size-area">
                        @if(count($product_color_detail)>0)
                        <div class="color-area">
                           <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.COLOR')!= '') {{ trans(Session::get('lang_file').'.COLOR') }} @else {{ trans($OUR_LANGUAGE.'.COLOR') }} @endif </h2>
                           <div class="color">
                              <select name="addtocart_color" id="addtocart_color_offer_{{$product_det->pro_id}}" required>
                                 <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') {{ trans(Session::get('lang_file').'.SELECT_COLOR') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_COLOR') }} @endif--</option>
                                 @foreach($product_color_detail as $product_color_det) 
                                 <option value="{{ $product_color_det->co_id }}">
                                    {{ $product_color_det->co_name }}
                                 </option>
                                 @endforeach 
                              </select>
                           </div>
                        </div>
                        @endif
                        @if(count($product_size_detail) > 0)
                        @php  $size_name = $product_size_detail[0]->si_name;
                        $return  = strcmp($size_name,'no size');  @endphp
                        @if($return!=0) 
                        <div class="size-area">
                           <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.SIZE')!= '') {{ trans(Session::get('lang_file').'.SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SIZE') }} @endif</h2>
                           <div class="size">
                              <select name="addtocart_size" id="addtocart_size_offer_{{$product_det->pro_id}}" required>
                                 <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') {{ trans(Session::get('lang_file').'.SELECT_SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_SIZE') }} @endif--</option>
                                 @foreach($product_size_detail as $product_size_det) 
                                 <option value="{{ $product_size_det->ps_si_id }}">
                                    {{ $product_size_det->si_name }}
                                 </option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        @else 
                        <input type="hidden" name="addtocart_size" value="{{ $product_size_detail[0]->ps_si_id }}">
                        @endif
                        @endif
                     </div>
                     <div class="product-variation">
                        {!! Form :: open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data','id'=>'submit_form_offer')) !!}
                        <form action="#" method="post">
                        <div class="cart-plus-minus">
                           <label for="qty">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.QUANTITY') }} @endif :</label>
                           <div class="numbers-row">
                              <div onClick="remove_quantity_offer(<?php echo $product_det->pro_id; ?>)" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                              <input type="number" class="qty" min="1" value="1" max="{{  ($product_det->pro_qty - $product_det->pro_no_of_purchase) }}" id="addtocart_qty_offer_{{ $product_det->pro_id }}" name="addtocart_qty" readonly required >
                              <div onClick="add_quantity_offer(<?php echo $product_det->pro_id; ?>)" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                           </div>
                        </div>
                        {{-- Add to cart--}}
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        {{ Form::hidden('addtocart_type','product')}}
                        {{ Form::hidden('addtocart_pro_id',$product_det->pro_id)}}
                        @if(Session::has('customerid')) 
                        @if($count > 0)
                        <button onclick="addtocart_validate_offer('<?php echo $product_det->pro_id; ?>');"  class="button pro-add-to-cart" title="Add to Cart" type="button" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button>
                        @else 
                        <button type="button" class="btn btn-danger">
                        @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif
                        </button> 
                        @endif 
                        @else 
                        @if($count > 0)
                        <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                        <button type="button" class=" button pro-add-to-cart">
                        <span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                        @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span>
                        </button> 
                        </a>
                        @else
                        <button type="button" class="btn btn-danger">
                        @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif
                        </button> 
                        @endif 
                        @endif
                        {{ Form::close() }}
                     </div>
                     <div class="product-cart-option">
                        <ul>
                           <li>
                              {{-- Add to wishlist--}}
                              @if(Session::has('customerid'))
                              @php  
                              $cus_id = Session::get('customerid');
                              $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',$cus_id)->first(); @endphp
                              @else
                              @php  $prodInWishlist = array(); @endphp
                              @endif
                              @if($count > 0)  
                              @if(Session::has('customerid'))
                              @if(count($prodInWishlist)==0)
                              <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                              {{ Form::hidden('pro_id','$product_det->pro_id') }}        
                              <!-- <input type="hidden" name="pro_id" value="<?php echo $product_det->pro_id; ?>"> -->
                              <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                              <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})">
                              <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                              <i class="fa fa-heart-o" aria-hidden="true"></i>@if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif 
                              </a>
                              @else
                              <?php /* remove wishlist */?>   
                              <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}">
                              <i class="fa fa-heart" aria-hidden="true"></i>    @if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST') }} @endif
                              </a> 
                              <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                              @endif  
                              @else 
                              <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                              <i class="fa fa-heart-o" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif
                              </a>
                              @endif
                              @endif
                           </li>
                           {{-- 
                           <li><a href="#"><i class="fa fa-link"></i><span>Add to Compare</span></a></li>
                           <li><a href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
                           --}}
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--product-view--> 
         </div>
      </div>
      <a style="display: inline;" id="quick_view_popup-close" href="{{url('')}}/index"><i class="icon pe-7s-close"></i></a> 
   </div>
</div>
@endif  
@endif
@endforeach
@endif
<!--quick view best rated 50% offer--> 
<!--end quick view best rated 50% offer--> 

<a href="" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> 
<!-- End Footer --> 
</div>
<!-- JS --> 
<script type='text/javascript'>
   jQuery(document).ready(function(){
       jQuery('#rev_slider_4').show().revolution({
           dottedOverlay: 'none',
           delay: 5000,
           startwidth: 865,
        startheight: 450,
   
           hideThumbs: 200,
           thumbWidth: 200,
           thumbHeight: 50,
           thumbAmount: 2,
   
           navigationType: 'thumb',
           navigationArrows: 'solo',
           navigationStyle: 'round',
   
           touchenabled: 'on',
           onHoverStop: 'on',
           
           swipe_velocity: 0.7,
           swipe_min_touches: 1,
           swipe_max_touches: 1,
           drag_block_vertical: false,
       
           spinner: 'spinner0',
           keyboardNavigation: 'off',
   
           navigationHAlign: 'center',
           navigationVAlign: 'bottom',
           navigationHOffset: 0,
           navigationVOffset: 20,
   
           soloArrowLeftHalign: 'left',
           soloArrowLeftValign: 'center',
           soloArrowLeftHOffset: 20,
           soloArrowLeftVOffset: 0,
   
           soloArrowRightHalign: 'right',
           soloArrowRightValign: 'center',
           soloArrowRightHOffset: 20,
           soloArrowRightVOffset: 0,
   
           shadow: 0,
           fullWidth: 'on',
           fullScreen: 'off',
   
           stopLoop: 'off',
           stopAfterLoops: -1,
           stopAtSlide: -1,
   
           shuffle: 'off',
   
           autoHeight: 'off',
           forceFullWidth: 'on',
           fullScreenAlignForce: 'off',
           minFullScreenHeight: 0,
           hideNavDelayOnMobile: 1500,
       
           hideThumbsOnMobile: 'off',
           hideBulletsOnMobile: 'off',
           hideArrowsOnMobile: 'off',
           hideThumbsUnderResolution: 0,
   
   
           hideSliderAtLimit: 0,
           hideCaptionAtLimit: 0,
           hideAllCaptionAtLilmit: 0,
           startWithSlide: 0,
           fullScreenOffsetContainer: ''
       });
   });
</script>
<script type="text/javascript">
   $('#subscribe_submit').click(function(){  
     var email=$('#sub_email').val(); 
     if(email=='') {
       alert("{{ (Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL') : trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL') }}");
       return false;
     }
   $('.mail-loader').css('display','block');
      $.ajax( { 
               type: 'get',
               data: {email},
               url: '<?php echo url('subscription_submit'); ?>',
               success: function(responseText){  
                
                if(responseText=='0')
                { 
                        alert("{{ (Lang::has(Session::get('lang_file').'.EMAIL_ALREADY_SUBSCRIBED')!= '') ? trans(Session::get('lang_file').'.EMAIL_ALREADY_SUBSCRIBED') : trans($OUR_LANGUAGE.'.EMAIL_ALREADY_SUBSCRIBED') }}");
                           $('.mail-loader').css('display','none');
                } else{
                 // Need loader
               $('.mail-loader').css('display','none');
                 alert("{{ (Lang::has(Session::get('lang_file').'.YOUR_EMAIL_SUBSCRIBED_SUCCESSFULLY')!= '') ? trans(Session::get('lang_file').'.YOUR_EMAIL_SUBSCRIBED_SUCCESSFULLY') : trans($OUR_LANGUAGE.'.YOUR_EMAIL_SUBSCRIBED_SUCCESSFULLY') }}");
                }
             }       
         });  
   
   
   });
   
</script>
<script type="text/javascript">
   function addtowish(pro_id,cus_id){
   
     var wishlisturl = document.getElementById('wishlisturl').value;
   
     $.ajax({
           type: "get",   
           url:"<?php echo url('addtowish'); ?>",
           data:{'pro_id':pro_id,'cus_id':cus_id},
           success:function(response){
            // alert(response);
             if(response==0){
              
             <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>
                         $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');
               //window.location=wishlisturl;
                             window.location.reload();
                             
             }else{
               alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');
               //window.location=wishlisturl;
             }
             
             
           }
         });
   }
</script>
<!--<script type="text/javascript">
   jQuery(document).ready(function($) {
     var Body = $('body');
     Body.addClass('preloader-site');
   });
   $(window).load(function(){
     $('.preloader-wrapper').fadeOut();
     $('body').removeClass('preloader-site');
   });
</script> -->
<script>
   function add_quantity(id)
   {
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
     var quantity=$("#addtocart_qty_"+id).val(); 
     var pro_qty_hidden=$("#pro_qty_hidden_"+id).val();
     var pro_purchase_hidden=$("#pro_purchase_hidden_"+id).val();
     var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);
    
     
     if(quantity<remaining_product)
     { 
       var new_quantity=parseInt(quantity)+1;
       $("#addtocart_qty_"+id).val(new_quantity);
     }
     //alert();
   }
   
   function remove_quantity(id)
   {
     //alert();
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/
   
     var quantity=$("#addtocart_qty_"+id).val();
     var quantity=parseInt(quantity);
     if(quantity>1)
     {
       var new_quantity=quantity-1;
       $("#addtocart_qty_"+id).val(new_quantity);
     }
     //alert();
   }
   
   function add_quantity_offer(id)
   {
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
     var quantity=$("#addtocart_qty_offer_"+id).val(); 
     var pro_qty_hidden=$("#pro_qty_hidden_offer_"+id).val();
     var pro_purchase_hidden=$("#pro_purchase_hidden_offer_"+id).val();
     var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);
    
     
     if(quantity<remaining_product)
     { 
       var new_quantity=parseInt(quantity)+1;
       $("#addtocart_qty_offer_"+id).val(new_quantity);
     }
     //alert();
   }
   
   function remove_quantity_offer(id)
   {
     //alert();
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/
   
     var quantity=$("#addtocart_qty_offer_"+id).val();
     var quantity=parseInt(quantity);
     if(quantity>1)
     {
       var new_quantity=quantity-1;
       $("#addtocart_qty_offer_"+id).val(new_quantity);
     }
     //alert();
   }
</script>
<script type="text/javascript">
   function addtowish(pro_id,cus_id){
     //alert();
     var wishlisturl = document.getElementById('wishlisturl').value;
   
     $.ajax({
           type: "get",   
           url:"<?php echo url('addtowish'); ?>",
           data:{'pro_id':pro_id,'cus_id':cus_id},
             success:function(response){
             //alert(response); return false;
             if(response==0){
             <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>
                         $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');
               //window.location=wishlisturl;
                             window.location.reload();
                             
             }else{
               alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');
               //window.location=wishlisturl;
             }
             
             
           }
         });
   }
</script>
<script type="text/javascript">
   function addtocart_validate_offer(id){
    
   var pro_qty=$("#pro_qty_hidden_offer_"+id).val();
   var pro_purchase1=$("#pro_purchase_hidden_offer_"+id).val();
   var pro_purchase = parseInt($('#addtocart_qty_offer_').val()) + parseInt(pro_purchase1);
   var error = 0;
   if(pro_purchase > parseInt(pro_qty))
   {
     $('#addtocart_qty_offer_'+id).focus();
     $('#addtocart_qty_offer_'+id).css('border-color', 'red');
     $('#addtocart_qty_error_offer_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');
   error++;
     return false;
   }
   else
   {
     $('#addtocart_qty_offer_'+id).css('border-color', '');
     $('#addtocart_qty_error_offer_'+id).html('');
   }
   if($('#addtocart_color_offer_'+id).val() ==0) 
   {
     $('#addtocart_color_offer_'+id).focus();
     $('#addtocart_color_offer_'+id).css('border-color', 'red');
     $('#size_color_error_offer_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');
     error++;
   return false;
   }
   else
   {
     $('#addtocart_color_offer_'+id).css('border-color', '');
     $('#size_color_error_offer_'+id).html('');
   }
   if($('#addtocart_size_offer_'+id).val() ==0)
   {
     $('#addtocart_size_offer_'+id).focus();
     $('#addtocart_size_offer_'+id).css('border-color', 'red');
     $('#size_color_error_offer_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');
     error++;
   return false;
   }
   else
   {
     $('#addtocart_size_offer_'+id).css('border-color', '');
     $('#size_color_error_offer_'+id).html('');
   }
   
   if(error <= 0){
    $("#submit_form_offer").submit(); 
   }
   
   }  
</script>   
<script type="text/javascript">
   function addtocart_validate(id){
    
   var pro_qty=$("#pro_qty_hidden_"+id).val();
   var pro_purchase1=$("#pro_purchase_hidden_"+id).val();
   var pro_purchase = parseInt($('#addtocart_qty_offer_').val()) + parseInt(pro_purchase1);
   var error1 = 0;
    if(pro_purchase > parseInt(pro_qty))
   {
     $('#addtocart_qty_'+id).focus();
     $('#addtocart_qty_'+id).css('border-color', 'red');
     $('#addtocart_qty_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');
     return false;
   }
   else
   {
     $('#addtocart_qty_'+id).css('border-color', '');
     $('#addtocart_qty_error_'+id).html('');
   }
   if($('#addtocart_color_'+id).val() ==0) 
   {
     $('#addtocart_color_'+id).focus();
     $('#addtocart_color_'+id).css('border-color', 'red');
     $('#size_color_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');
     return false;
   }
   else
   {
     $('#addtocart_color_'+id).css('border-color', '');
     $('#size_color_error_'+id).html('');
   }
   if($('#addtocart_size_'+id).val() ==0)
   {
     $('#addtocart_size_'+id).focus();
     $('#addtocart_size_'+id).css('border-color', 'red');
     $('#size_color_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');
     return false;
   }
   else
   {
     $('#addtocart_size_'+id).css('border-color', '');
     $('#size_color_error_'+id).html('');
   }
   
   if(error1 <= 0){
    $("#submit_form").submit(); 
   }
   
   }  
</script>



<script>
   $(document).ready(function() {
              
         $('.computer-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: false,
                dots: false,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>
                            
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 3
                  },
                  992: {
                    items: 4
                  },
                  1200: {
                    items: 4
                  }
                }
            })


            $('.best-sale-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: true,
                dots: false,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>               
                autoplay:true,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 3
                  },
                  992: {
                    items: 4
                  },
                  1200: {
                    items: 4
                  }
                }
            })

            $('.hot-pr-img-slider').owlCarousel({                
                margin: 10,
                nav: false,
                dots: false,
                loop: true,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>             
                autoplay:true,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 2
                  },
                  992: {
                    items: 1
                  },
                  1200: {
                    items: 1
                  }
                }
            })

            $('.toprate-pr-slider').owlCarousel({                
                margin: 10,
                nav: false,                
                loop: true,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>              
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 1
                  },
                  768: {
                    items: 2
                  },
                  992: {
                    items: 1
                  },
                  1200: {
                    items: 1
                  }
                }
            })

            $('.new-pr-slider').owlCarousel({                
                margin: 10,
                nav: false,                
                loop: true,
                 <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>              
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 1
                  },
                  768: {
                    items: 2
                  },
                  992: {
                    items: 1
                  },
                  1200: {
                    items: 1
                  }                  
                }
            })

            $('.ltst-nws-slider').owlCarousel({                
                margin: 10,
                nav: false,                
                loop: false,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>              
                autoplay:true,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 2
                  },
                  992: {
                    items: 3
                  },
                  1200: {
                    items: 4
                  }
                }
            })

            $('.testimonial-slider').owlCarousel({                
                margin: 10,
                nav: false, 
                dots: false,               
                loop: true,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>             
                autoplay:true,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 1
                  },
                  768: {
                    items: 1
                  },
                  992: {
                    items: 1
                  },
                  1200: {
                    items: 1
                  }
                }
            })


            })


</script>




</body>
</html>