{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

<!-- Header End====================================================================== -->

<head>

<?php

	

	if($get_meta_details){

	 foreach($get_meta_details as $meta_details) { } 

	 

	$mtitle= $meta_details->gs_metatitle;

	 $mdetails=$meta_details->gs_metadesc;

	 $mkey=$meta_details->gs_metakeywords;

	}

	else

	{

		$mtitle="";

	 $mdetails="";

	 $mkey="";

		

	}

	 ?>

     <title><?php echo $mtitle; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="<?php echo $mdetails; ?>">

    <meta name="keywords" content="<?php echo $mkey;  ?>">

<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

<!-- Basic page needs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{ url('') }}">@if (Lang::has(Session::get('lang_file').'.HOME')!= '') {{  trans(Session::get('lang_file').'.HOME') }} @else {{ trans($OUR_LANGUAGE.'.HOME') }} @endif</a><span>&raquo;</span></li>

            <li><strong>@if (Lang::has(Session::get('lang_file').'.PAYMENT_RESULT')!= '') {{  trans(Session::get('lang_file').'.PAYMENT_RESULT') }} @else {{ trans($OUR_LANGUAGE.'.PAYMENT_RESULT') }} @endif</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <div class="category-description std">

            <div class="slider-items-products">

             

            </div>

          </div>

		  

		  

          <div class="shop-inner">

		 @if (Session::has('success'))

		 <div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{!! Session::get('success') !!}</div>

		 @endif

            <div class="page-title"> 

			  <div class="about-page"> 

			

                  <h2><span class="text_color">@if (Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED')!= '') {{ trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED') }} @else {{ trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED') }} @endif .. @if (Lang::has(Session::get('lang_file').'.PLEASE_NOTE_THE_TRANSACTION_DETAILS')!= '') {{ trans(Session::get('lang_file').'.PLEASE_NOTE_THE_TRANSACTION_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.PLEASE_NOTE_THE_TRANSACTION_DETAILS') }} @endif </span></h2>

		      

			

			   @if (Session::has('fail'))

                  <h2><span class="text_color">@if (Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED')!= '') {{ trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED') }} @else {{ trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_FAILED') }} @endif</span></h2>

		       @endif

			   

			    @if (Session::has('error'))

                  <h2><span class="text_color">@if (Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')!= '') {{ trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR') }} @else {{ trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR') }} @endif</span></h2>

		       @endif

			   

             </div>

		   </div>

		   

            <div class="product-grid-area">

			    <h5>@if (Lang::has(Session::get('lang_file').'.TRANSACTION_DETAILS')!= '') {{  trans(Session::get('lang_file').'.TRANSACTION_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.TRANSACTION_DETAILS') }} @endif</h5>

				<h6>@if (Lang::has(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH')!= '') {{  trans(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH') }} @else {{ trans($OUR_LANGUAGE.'.THANK_YOU_FOR_SHOPPING_WITH') }} @endif {{ $SITENAME }}.</h6>

				

			 <table class="table table-bordered">

              <thead>

                <tr>

                <th>@if (Lang::has(Session::get('lang_file').'.PAYER_NAME')!= '') {{  trans(Session::get('lang_file').'.PAYER_NAME') }} @else {{ trans($OUR_LANGUAGE.'.PAYER_NAME') }} @endif</th>

                <th>@if (Lang::has(Session::get('lang_file').'.TRANSACTIONID')!= '') {{  trans(Session::get('lang_file').'.TRANSACTIONID') }} @else {{ trans($OUR_LANGUAGE.'.TRANSACTIONID') }} @endif</th>

                <th>@if (Lang::has(Session::get('lang_file').'.TOKENID')!= '') {{  trans(Session::get('lang_file').'.TOKENID') }} @else {{ trans($OUR_LANGUAGE.'.TOKENID') }} @endif</th>

                <th>@if (Lang::has(Session::get('lang_file').'.PAYER_EMAIL')!= '') {{  trans(Session::get('lang_file').'.PAYER_EMAIL') }} @else {{ trans($OUR_LANGUAGE.'.PAYER_EMAIL') }} @endif</th>

                <th>@if (Lang::has(Session::get('lang_file').'.PAYER_ID')!= '') {{  trans(Session::get('lang_file').'.PAYER_ID') }} @else {{ trans($OUR_LANGUAGE.'.PAYER_ID') }} @endif</th>

                <th>@if (Lang::has(Session::get('lang_file').'.ACKNOWLEDGEMENT')!= '') {{  trans(Session::get('lang_file').'.ACKNOWLEDGEMENT') }} @else {{ trans($OUR_LANGUAGE.'.ACKNOWLEDGEMENT') }} @endif</th>

                <th>@if (Lang::has(Session::get('lang_file').'.PAYERSTATUS')!= '') {{  trans(Session::get('lang_file').'.PAYERSTATUS') }} @else {{ trans($OUR_LANGUAGE.'.PAYERSTATUS') }} @endif</th>

				</tr>

              </thead>

              <tbody>

           

			 <?php  $coupon = 0;

                    $shipping_amt = 0;

                    $get_subtotal  =0;

                    $get_tax = 0;

			  if($orderdetails){ 

				foreach($orderdetails as $orderdet) {

				   $coupon+= $orderdet->coupon_amount;

                   $shipping_amt+= $orderdet->order_shipping_amt;    //shipping amount getting from order table

                   $get_subtotal+= $orderdet->order_amt;

                   $get_tax+=$orderdet->order_tax;

             ?>

             <tr>

                  <td><?php echo $orderdet->payer_name;?></td>

                  <td><?php echo $orderdet->transaction_id;?> </td>

                  <td><?php echo $orderdet->token_id;?></td>

                  <td><?php echo $orderdet->payer_email;?></td>

                  

                  <td><?php echo $orderdet->payer_id;?></td>

                  <td><?php echo $orderdet->payment_ack; ?></td>

                  <td><?php echo $orderdet->payer_status; ?></td>

             </tr>

		  <?php  } }?>

				</tbody>

            </table>

			

			

			<h5>@if (Lang::has(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION') }} @else {{ trans($OUR_LANGUAGE.'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION') }} @endif </h5>

		   <table class="table table-bordered">

              <thead>

                <tr>

                <th><?php if (Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_NAME')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_DEAL_NAME');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_DEAL_NAME');} ?></th>

                  <th><?php if (Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_DEAL_QUANTITY');} ?></th>

                  <th><?php if (Lang::has(Session::get('lang_file').'.AMOUNT')!= '') { echo  trans(Session::get('lang_file').'.AMOUNT');}  else { echo trans($OUR_LANGUAGE.'.AMOUNT');} ?></th>

                

				</tr>

              </thead>

              <tbody>



				<?php  

                $taxamount =0;

                $wallet = 0;

                

                if($orderdetails){ 

					foreach($orderdetails as $orderdet) 

					{

						$trans_id = $orderdet->transaction_id;

					
						$getcolor = DB::table('nm_color')->select('co_name')->where('co_id','=',$orderdet->order_pro_color)->first();
						$getsize = DB::table('nm_size')->select('si_name')->where('si_id','=',$orderdet->order_pro_size)->first();
						/*if($orderdet->order_type == 1 || $orderdet->order_type == 2){

						

						$taxamount+=($orderdet->order_amt*$orderdet->order_tax)/100;

						$taxamount_au = ($orderdet->order_amt*$orderdet->order_tax)/100;

					}else{

						

						$taxamount_au=0;

					} */





            $taxamount    += $orderdet->order_taxAmt;

            $taxamount_au = $orderdet->order_taxAmt;



          ?>

				

				 <tr>

               	  <td><?php 

				  if($orderdet->order_type == 1){ 

					if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

						$pro_title = 'pro_title';

					}else {  $pro_title = 'pro_title_'.Session::get('lang_code'); }

					echo $orderdet->$pro_title; ?>
					
					@if( $orderdet->order_pro_color!='' && $orderdet->order_pro_color!=0)
					<br><b><?php if (Lang::has(Session::get('lang_file').'.COLOR')!= '') { echo  trans(Session::get('lang_file').'.COLOR');}  else { echo trans($OUR_LANGUAGE.'.COLOR');}?> : </b>{{ $getcolor->co_name }}
					@endif
					
					@if( $orderdet->order_pro_size!='' && $orderdet->order_pro_size!=0)
					<br><b><?php if (Lang::has(Session::get('lang_file').'.SIZE')!= '') { echo  trans(Session::get('lang_file').'.SIZE');}  else { echo trans($OUR_LANGUAGE.'.SIZE');}?> : </b>{{ $getsize->si_name }}
					@endif

				 <?php } else { 

					if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

						$deal_title = 'deal_title';

					}else {  $deal_title = 'deal_title_'.Session::get('lang_code'); }

					echo $orderdet->$deal_title;}?></td>

                  <td><?php echo $orderdet->order_qty;?> </td>

                  <td>{{ Helper::cur_sym() }} <?php echo $orderdet->order_amt + $taxamount_au;?><?php  $orderdet->order_amt + $taxamount;?> (<?php if (Lang::has(Session::get('lang_file').'.INCLUDING')!= '') { echo  trans(Session::get('lang_file').'.INCLUDING');}  else { echo trans($OUR_LANGUAGE.'.INCLUDING');} ?>

                   <?php  $orderdet->order_amt;?> <?php echo $orderdet->order_tax;?> % 

                   <?php  $orderdet->order_amt + $taxamount;?> 

                   (<?php if (Lang::has(Session::get('lang_file').'.TAXES')!= '') { echo  trans(Session::get('lang_file').'.TAXES');}  else { echo trans($OUR_LANGUAGE.'.TAXES');} ?>))</td>

      			 </tr>

				<?php }

				} ?>

                <?php if($coupon!=0){?>

                    <tr>

                        <td>&nbsp;</td> 

                        <td>{{ (Lang::has(Session::get('lang_file').'.COUPON_VALUE')!= '') ? trans(Session::get('lang_file').'.COUPON_VALUE') : trans($OUR_LANGUAGE.'.COUPON_VALUE') }} </td>

                        <td> - {{ Helper::cur_sym() }} <?php echo $coupon; ?></td>

                    </tr>    

                <?php } ?> 



                <tr>                  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.SUB-TOTAL')!= '') { echo  trans(Session::get('lang_file').'.SUB-TOTAL');}  else { echo trans($OUR_LANGUAGE.'.SUB-TOTAL');} ?></td>                  

                        <td style="font-weight:bold;"> <?php $subtotal = ($get_subtotal+$taxamount)-$coupon;  ?> {{ Helper::cur_sym() }} {{ round($subtotal,2) }} </td>             

                    </tr>

                <tr>                  

                    <td>&nbsp;</td>                  

                    <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.SHIPPING_TOTAL')!= '') { echo  trans(Session::get('lang_file').'.SHIPPING_TOTAL');}  else { echo trans($OUR_LANGUAGE.'.SHIPPING_TOTAL');} ?></td>                  

                    <td style="font-weight:bold;">{{ Helper::cur_sym() }} <?php  echo $shipping_amt; ?></td>             

                </tr>      



                <?php 

                    $trans_wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$trans_id)->value('wallet_used');

                    $wallet = $trans_wallet;

                    if(count($wallet)!=0){

                        $get_subtotal = $get_subtotal - $wallet;

                ?>

                    <tr>

                        <td>&nbsp;</td> 

                        <td>{{ (Lang::has(Session::get('lang_file').'.WALLET_USED')!= '') ? trans(Session::get('lang_file').'.WALLET_USED') : trans($OUR_LANGUAGE.'.WALLET_USED') }} </td>

                        <td> - {{ Helper::cur_sym() }} <?php echo $wallet; ?></td>

                    </tr>

                    <?php }else $wallet = 0; ?>          	 

                    

                    <?php /*

                    <tr>               	  

                        <td>&nbsp;</td> 

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.TAX')!= '') { echo  trans(Session::get('lang_file').'.TAX');}  else { echo trans($OUR_LANGUAGE.'.TAX');} ?></td>                  

                        <td style="font-weight:bold;"> <?php  echo $get_tax;?> %</td>      			

                    </tr>*/?>

                    

                    

                    <tr>               	  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.TOTAL')!= '') { echo  trans(Session::get('lang_file').'.TOTAL');}  else { echo trans($OUR_LANGUAGE.'.TOTAL');} ?></td>                  

                        <td style="font-weight:bold;">{{ Helper::cur_sym() }} <?php $total = ($subtotal + $shipping_amt) -$wallet;  echo round($total,2);//number_format((float)$total, 2, '.', '');  ?></td>      			

                    </tr>

                	</tbody>

            </table>

		

				 

				 <div class="special-product">

				 <h4><a class="link-all pull-right me_btn res-cont1" href="{{ url('index') }}">@if (Lang::has(Session::get('lang_file').'.CONTINUE_SHOPPING')!= '') {{  trans(Session::get('lang_file').'.CONTINUE_SHOPPING') }} @else {{ trans($OUR_LANGUAGE.'.CONTINUE_SHOPPING') }} @endif</a></h4>

				 </div>

				 

				 

				 <div class="clearfix"></div>

				

            </div>

           

          </div>

		  

		  

        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

          

          <div class="block shop-by-side">

            <div class="sidebar-bar-title">

              <h3>{{ (Lang::has(Session::get('lang_file').'.SHOP_BY')!= '') ? trans(Session::get('lang_file').'.SHOP_BY') : trans($OUR_LANGUAGE.'.SHOP_BY') }}</h3>

            </div>

            <div class="block-content">

              <p class="block-subtitle">{{ (Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES') }}</p>

              <div class="layered-Category">

                <div class="layered-content">

		

				<div id="divMenu">

					<ul>

					   @foreach($main_category as $fetch_main_cat) 

                       @php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; @endphp						

						<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>">

							 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

							  @php $mc_name = 'mc_name'; @endphp

							  @else @php  $mc_name = 'mc_name_code'; @endphp @endif

							  {{ $fetch_main_cat->$mc_name }}</a> 

							  @if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0)  

						   <ul>

							 @foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)  

                               @php  $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp

                               @if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0) 

								   

								<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"> 

								 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

									@php	$smc_name = 'smc_name'; @endphp

									@else @php  $smc_name = 'smc_name_code'; @endphp @endif

									{{ $fetch_sub_main_cat->$smc_name }} </a>

										

								

									 <ul>

									   @foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)  

                                       @php  $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp

                                        @if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0)

										<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"> 

										@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

										  @php $sb_name = 'sb_name'; @endphp

										  @else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

										  {{ $fetch_sub_cat->$sb_name }}</a> 

											<ul>	

												@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  

                                                @php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp 

												<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)}}">

												@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

												@php $ssb_name = 'ssb_name'; @endphp

												@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

												{{ $fetch_secsub_cat->$ssb_name }}</a>

												</li>

												 

													@endforeach 

											</ul>

											 @endif

										</li>

										  @endforeach

									</ul>

									 @endif

								</li>

								 @endforeach

						   </ul>

						   @endif

						</li>

						 @endforeach

					</ul>

					

				</div>

                </div>

              </div>

             

            </div>

          </div>

		  

		   <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3>@if (Lang::has(Session::get('lang_file').'.MOST_VISITED_PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.MOST_VISITED_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.MOST_VISITED_PRODUCTS') }} @endif</h3>

            </div>

            <div class="block-content">

              <ul>

			 <?php foreach($most_visited_product as $fetch_most_visit_pro) {

			 $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

			 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

			 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);

			 ?>

			  

                <li class="item">

                  <div class="products-block-left"> 

				  <?php  $product_image     = $mostproduct_img[0];

                /* Image Path */

                $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                $img_data   = "public/assets/product/".$product_image;

                if($mostproduct_img !=''){

                if(file_exists($img_data) && $product_image !='')   //product image is not null and exists in folder

					{

                

                $prod_path = url('').'/public/assets/product/' .$product_image;                  

                }else{  

                    if(isset($DynamicNoImage['productImg'])){

                        $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg'];

                        if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))    //no image for product is not null and exists in folder

                            $prod_path = url('').'/'.$dyanamicNoImg_path;

                    }

                } 

				}

				else{  

                    if(isset($DynamicNoImage['productImg'])){

                        $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg'];

                        if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))    //no image for product is not null and exists in folder

                            $prod_path = url('').'/'.$dyanamicNoImg_path;

                    }

                } ?>

			     <a title="Sample Product" class="product-image"><img  src="{{$prod_path}}" alt="{{$fetch_most_visit_pro->pro_title}}"></a>

				  

				  </div>

                  <div class="products-block-right">

                    <p class="product-name"> <a href="#product">{{ substr($fetch_most_visit_pro->pro_title,0,50) }}...</a> </p>

                    <span class="price">{{ Helper::cur_sym() }}{{ $fetch_most_visit_pro->pro_disprice }}</span>

                   

				   @php           

                $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 1)->count();

                $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 2)->count();

                $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 3)->count();

                $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 4)->count();

                $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 5)->count();

                

                

                $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

                $multiple_countone = $one_count *1;

                $multiple_counttwo = $two_count *2;

                $multiple_countthree = $three_count *3;

                $multiple_countfour = $four_count *4;

                $multiple_countfive = $five_count *5;

                $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp



                         <div class="rating">

             @if($product_count)

             @php   $product_divide_count = $product_total_count / $product_count; @endphp

             @if($product_divide_count <= '1') 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '1') 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '2') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

             @elseif($product_divide_count >= '3') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '4') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             @elseif($product_divide_count >= '5') 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

             @else

              

            @endif

          @else

             <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

          @endif  

           </div>

					

				<?php if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty) { ?>

					<a class="link-all">@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{  trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</a>

				<?php } else { ?>	

				<?php $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

					 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

					 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

					 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

					 $res = base64_encode($fetch_most_visit_pro->pro_id);

			   ?>	

					@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					 @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				    @endif

				   

				   @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')  

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				   @endif

					

				<?php } ?>

				

				

                  </div>

                </li>

			 <?php } ?>

              </ul>

		   </div>

          </div>

         

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  

  

  

  {!! $footer !!}

</html>

