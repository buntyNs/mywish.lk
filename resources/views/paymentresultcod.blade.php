{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

<!-- Header End====================================================================== -->


<head>

<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

<!-- Basic page needs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{ url('') }}">@if (Lang::has(Session::get('lang_file').'.HOME')!= '') {{  trans(Session::get('lang_file').'.HOME') }} @else {{ trans($OUR_LANGUAGE.'.HOME') }} @endif</a><span>&raquo;</span></li>

            <li><strong>@if (Lang::has(Session::get('lang_file').'.PAYMENT_RESULT')!= '') {{  trans(Session::get('lang_file').'.PAYMENT_RESULT') }} @else {{ trans($OUR_LANGUAGE.'.PAYMENT_RESULT') }} @endif</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <div class="category-description std">

            <div class="slider-items-products">

             

            </div>

          </div>

		  

		  

          <div class="shop-inner paymntcod">

            <div class="page-title"> <div class="about-page"> 

			

              <h2><span class="text_color">@if (Lang::has(Session::get('lang_file').'.YOUR_ORDER_SUCCESSFULLY_PLACED')!= '') {{ trans(Session::get('lang_file').'.YOUR_ORDER_SUCCESSFULLY_PLACED') }} @else {{ trans($OUR_LANGUAGE.'.YOUR_ORDER_SUCCESSFULLY_PLACED') }} @endif.</span></h2>

		   



			@if (Session::has('fail'))

              <h2><span class="text_color">@if (Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED')!= '') {{ trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED') }} @else {{ trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_FAILED') }} @endif.</span></h2>

		    @endif

			

			@if (Session::has('error'))

              <h2><span class="text_color">@if (Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')!= '') {{ trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR') }} @else {{ trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR') }} @endif.</span></h2>

		    @endif

            </div></div>

            <div class="product-grid-area">

			

			<div class="clearfix"></div>	<hr class="soft"/>			 

				<h4>@if (Lang::has(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH')!= '') {{  trans(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH') }}  @else {{ trans($OUR_LANGUAGE.'.THANK_YOU_FOR_SHOPPING_WITH') }} @endif {{ $SITENAME }} . </h4>				

				<h5>@if (Lang::has(Session::get('lang_file').'.TRANSACTION_DETAILS')!= '') {{  trans(Session::get('lang_file').'.TRANSACTION_DETAILS') }}  @else {{ trans($OUR_LANGUAGE.'.TRANSACTION_DETAILS') }} @endif</h5>

        <div class="table-responsive">	

				<table class="table table-bordered">              

					<thead><tr>                	

					<th>@if (Lang::has(Session::get('lang_file').'.CUSTOMER_NAME')!= '') {{ trans(Session::get('lang_file').'.CUSTOMER_NAME') }} @else {{ trans($OUR_LANGUAGE.'.CUSTOMER_NAME') }} @endif</th>                   

					<th>@if (Lang::has(Session::get('lang_file').'.TRANSACTION_ID')!= '') {{  trans(Session::get('lang_file').'.TRANSACTION_ID') }} @else {{ trans($OUR_LANGUAGE.'.TRANSACTION_ID') }} @endif</th>					

					<th>@if (Lang::has(Session::get('lang_file').'.STATUS')!= '') {{  trans(Session::get('lang_file').'.STATUS') }} @else {{ trans($OUR_LANGUAGE.'.STATUS') }} @endif</th>				

					</tr>              

                    </thead>              

                    <tbody>           			 

                    @php  $coupon = 0;

                           $shipping_amt = 0; @endphp

                    @if($orderdetails) 				

                        @foreach($orderdetails as $orderdet)  

						

                        @php    $coupon+= $orderdet->coupon_amount;

                            $shipping_amt+= $orderdet->cod_shipping_amt; @endphp    <!-- //shipping amount getting from order table -->

                        @endforeach @php $coupon_amt = $orderdet->coupon_amount; @endphp

                     <tr>                

                        <td><?php echo Session::get('user_name');?></td> 

                        <td><?php echo $orderdet->cod_transaction_id;?> </td>

                        <td>{{(Lang::has(Session::get('lang_file').'.HOLD')!= '') ? trans(Session::get('lang_file').'.HOLD') : trans($OUR_LANGUAGE.'.HOLD') }}</td>                                                     

                    </tr>		  

                    @endif			

                    </tbody>            

                </table></div>

				<h5> @if (Lang::has(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION') }} @else {{ trans($OUR_LANGUAGE.'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION') }} @endif </h5>	

          <div class="table-responsive">  

				<table class="table table-bordered">              

                    <thead>                

                    <tr>                

                        <th>@if (Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_NAME')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_DEAL_NAME') }} @else {{ trans($OUR_LANGUAGE.'.PRODUCT_DEAL_NAME') }} @endif</th>                  <th>@if (Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.PRODUCT_DEAL_QUANTITY') }} @endif</th>                  <th>@if (Lang::has(Session::get('lang_file').'.AMOUNT')!= '') {{  trans(Session::get('lang_file').'.AMOUNT') }} @else {{ trans($OUR_LANGUAGE.'.AMOUNT') }} @endif

                        </th>                				

                    </tr>              

                    </thead>              

                    <tbody>										

                    @php  $taxamount =0;   $wallet=0; $trans_id =0; $taxamount_total = 0; @endphp

                    @if($orderdetails) 

							

                        @foreach($orderdetails as $orderdet) 

							
							
						 @php $getcolor = DB::table('nm_color')->select('co_name')->where('co_id','=',$orderdet->cod_pro_color)->first();
						 $getsize = DB::table('nm_size')->select('si_name')->where('si_id','=',$orderdet->cod_pro_size)->first();
						 

						 $trans_id = $orderdet->cod_transaction_id;


                            $taxamount = (($orderdet->cod_amt*$orderdet->cod_tax)/100);



							$taxamount_total += $taxamount; @endphp

															  

							<tr>               	  

								<td> 

								@if($orderdet->cod_order_type == 1) 

									@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

									@php	$pro_title = 'pro_title'; @endphp

									@else @php  $pro_title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

									{{ $orderdet->$pro_title }}
									
									
									@if( $orderdet->cod_pro_color!='' && $orderdet->cod_pro_color!=0)
									<br><b>@php if (Lang::has(Session::get('lang_file').'.COLOR')!= '') { echo  trans(Session::get('lang_file').'.COLOR');}  else { echo trans($OUR_LANGUAGE.'.COLOR');} @endphp: </b>{{ $getcolor->co_name }}
									@endif
									
									@if( $orderdet->cod_pro_size!='' && $orderdet->cod_pro_size!=0)
									<br><b>@php if (Lang::has(Session::get('lang_file').'.SIZE')!= '') { echo  trans(Session::get('lang_file').'.SIZE');}  else { echo trans($OUR_LANGUAGE.'.SIZE');} @endphp : </b>{{ $getsize->si_name }}
									@endif
									

								@else  

									@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

									@php	$deal_title = 'deal_title'; @endphp

									@else @php  $deal_title = 'deal_title_'.Session::get('lang_code'); @endphp @endif

									{{ $orderdet->$deal_title }}

								@endif</td>                  

								<td>{{ $orderdet->cod_qty }}</td>                  

								<td>{{ Helper::cur_sym() }} <?php echo round(($orderdet->cod_amt + $taxamount),2);?> (@if (Lang::has(Session::get('lang_file').'.INCLUDING')!= '') {{  trans(Session::get('lang_file').'.INCLUDING') }} @else {{ trans($OUR_LANGUAGE.'.INCLUDING') }} @endif {{ $orderdet->cod_tax }} % {{ round(($orderdet->cod_amt + $taxamount),2) }} (@if (Lang::has(Session::get('lang_file').'.TAXES')!= '') {{  trans(Session::get('lang_file').'.TAXES') }} @else {{ trans($OUR_LANGUAGE.'.TAXES') }} @endif)</td>      			 

							</tr>  

                 @endforeach	

				 

                    @endif

					

                    @if(($coupon)!=0) 

                        <tr>

                        <td>&nbsp;</td> 

                        <td> {{ (Lang::has(Session::get('lang_file').'.COUPON_VALUE')!= '') ? trans(Session::get('lang_file').'.COUPON_VALUE') : trans($OUR_LANGUAGE.'.COUPON_VALUE') }}</td>

                        <td>{{ Helper::cur_sym() }} {{ round($coupon,2) }}</td>

                        </tr>

                    @endif



					

					<tr>                     

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;">@if (Lang::has(Session::get('lang_file').'.SUB-TOTAL')!= '') {{  trans(Session::get('lang_file').'.SUB-TOTAL') }} @else {{ trans($OUR_LANGUAGE.'.SUB-TOTAL') }} @endif</td>                  

                        <td style="font-weight:bold;">{{ Helper::cur_sym() }} <?php $subtotal = ($get_subtotal+$taxamount_total)-$coupon; echo round($subtotal,2);?> </td>                   

                    </tr>

					

                    

                

                    <?php /*

                    <tr>               	  

                        <td>&nbsp;</td> 

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.TAX')!= '') { echo  trans(Session::get('lang_file').'.TAX');}  else { echo trans($OUR_LANGUAGE.'.TAX');} ?></td>                  

                        <td style="font-weight:bold;"> <?php  echo $get_tax;?> %</td>      			

                    </tr>*/?>

                    

                    <tr>               	  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;">@if (Lang::has(Session::get('lang_file').'.SHIPPING_TOTAL')!= '') {{ trans(Session::get('lang_file').'.SHIPPING_TOTAL') }} @else {{ trans($OUR_LANGUAGE.'.SHIPPING_TOTAL') }} @endif</td>                  

                        <td style="font-weight:bold;">{{ Helper::cur_sym() }} {{ $shipping_amt }}</td>      			 

                    </tr>



                    

                  @php  $trans_wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$trans_id)->value('wallet_used');

                    $wallet = $trans_wallet; @endphp

                   @if(count($wallet)!=0)

                        

                    <tr>

                        <td>&nbsp;</td> 

                        <td>{{ (Lang::has(Session::get('lang_file').'.WALLET_USED')!= '') ? trans(Session::get('lang_file').'.WALLET_USED') : trans($OUR_LANGUAGE.'.WALLET_USED') }} </td>

                        <td> - {{ Helper::cur_sym() }} round($wallet,2); ?></td>

                    </tr>

                    @else @php $wallet = 0; @endphp @endif

                    

                    <tr>               	  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;">@if (Lang::has(Session::get('lang_file').'.TOTAL')!= '') {{  trans(Session::get('lang_file').'.TOTAL') }} @else {{ trans($OUR_LANGUAGE.'.TOTAL') }} @endif</td>                  

                        <td style="font-weight:bold;">{{ Helper::cur_sym() }} <?php $total = ($subtotal + $shipping_amt) - $wallet;  echo round($total,2);//number_format((float)$total, 2, '.', '');  ?></td>      			

                    </tr>



                  </tbody>            

                 </table> 

				          </div>

				 <div class="special-product">

				 <h4><a class="link-all pull-right me_btn res-cont1" href="{{ url('index') }}">@if (Lang::has(Session::get('lang_file').'.CONTINUE_SHOPPING')!= '') {{  trans(Session::get('lang_file').'.CONTINUE_SHOPPING') }} @else {{ trans($OUR_LANGUAGE.'.CONTINUE_SHOPPING') }} @endif</a></h4>

				 </div>

				 <div class="clearfix"></div>

				

            </div>

           

          </div>

		  

		  

        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

          

          <div class="block shop-by-side">

            <!--<div class="sidebar-bar-title">

              <h3>Shop By</h3>

            </div>-->

            <div class="block-content">

              <p class="block-subtitle">{{ (Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES') }}</p>

              <div class="layered-Category">

                <div class="layered-content">

		

				<div id="divMenu">

					<ul>

					   @foreach($main_category as $fetch_main_cat) 

                       @php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; @endphp						

						<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>">

							 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

							  @php $mc_name = 'mc_name'; @endphp

							  @else @php  $mc_name = 'mc_name_code'; @endphp @endif

							  {{ $fetch_main_cat->$mc_name }}</a> 

							  @if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0)  

						   <ul>

							 @foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)  

                               @php  $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp

                               @if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0) 

								   

								<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"> 

								 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

									@php	$smc_name = 'smc_name'; @endphp

									@else @php  $smc_name = 'smc_name_code'; @endphp @endif

									{{ $fetch_sub_main_cat->$smc_name }} </a>
									 <ul>

									   @foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)  

                                       @php  $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp

                                        @if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0)

										<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"> 

										@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

										  @php $sb_name = 'sb_name'; @endphp

										  @else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

										  {{ $fetch_sub_cat->$sb_name }}</a> 

											<ul>	

												@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  

                                                @php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp 

												<li><a href="{{ url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)}}">

												@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

												@php $ssb_name = 'ssb_name'; @endphp

												@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

												{{ $fetch_secsub_cat->$ssb_name }}</a>

												</li>

												 

													@endforeach 

											</ul>

											 @endif

										</li>

										  @endforeach

									</ul>

									 @endif

								</li>

								 @endforeach

						   </ul>

						   @endif

						</li>

						 @endforeach

					</ul>

					

				</div>

                </div>

              </div>

             

            </div>

          </div>

		  

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  

  

  

  {!! $footer !!}

</html>

