<!DOCTYPE html>

<html lang="en">



{!! $navbar !!}

{!! $header !!}



<body class="orders_list_page">



<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

  

  <!-- Header -->

  

  <!-- end header -->

   

  

  <!-- Breadcrumbs -->

  

  

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <section class="main-container col2-right-layout">

    <div class="main container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12">

          <div class="my-account">

            <div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.ORDERS_LIST')!= '') {{  trans(Session::get('lang_file').'.ORDERS_LIST')}}  @else {{ trans($OUR_LANGUAGE.'.ORDERS_LIST')}} @endif</h2>

            </div>

             



            @foreach($customerdetails as $customer_info)

   @endforeach

          <div class="row">

            <div class="col-md-12 text-right">

               <div class="btn btn-large btn-primary me_btn cart-res">@if (Lang::has(Session::get('lang_file').'.TOTAL_WALLET_BALANCE_AMOUNT')!= '') {{  trans(Session::get('lang_file').'.TOTAL_WALLET_BALANCE_AMOUNT')}}  @else {{ trans($OUR_LANGUAGE.'.TOTAL_WALLET_BALANCE_AMOUNT')}} @endif : {{ Helper::cur_sym() }}{{ (($customer_info->wallet!="")?number_format($customer_info->wallet,2):'0.00') }}</div>

             </div>

         </div>



            <div class="orders-list table-responsive"> 

              

              <!--orders list table-->

              <table class="table table-bordered cart_summary table-striped">

                <thead>

                  <tr> 

                    <!--titles for td-->

                    <th>@if (Lang::has(Session::get('lang_file').'.ORDER_NUMBER')!= '') {{  trans(Session::get('lang_file').'.ORDER_NUMBER')}}  @else {{ trans($OUR_LANGUAGE.'.ORDER_NUMBER')}} @endif</th>

                    <th>@if (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '') {{  trans(Session::get('lang_file').'.ORDER_DATE')}}  @else {{ trans($OUR_LANGUAGE.'.ORDER_DATE')}} @endif</th>

                    <th>@if (Lang::has(Session::get('lang_file').'.TOTAL')!= '') {{  trans(Session::get('lang_file').'.TOTAL')}}  @else {{ trans($OUR_LANGUAGE.'.TOTAL')}} @endif</th>

                    <th>@if (Lang::has(Session::get('lang_file').'.USED_WALLET')!= '') {{  trans(Session::get('lang_file').'.USED_WALLET')}}  @else {{ trans($OUR_LANGUAGE.'.USED_WALLET')}} @endif</th>

                    <th>@if (Lang::has(Session::get('lang_file').'.ACTION')!= '') {{  trans(Session::get('lang_file').'.ACTION')}}  @else {{ trans($OUR_LANGUAGE.'.ACTION')}} @endif</th>

                  </tr>

                </thead>

                <tbody>

                  <?php 

   if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

       $pro_title   = 'pro_title';

      

   }else {  

       $pro_title   = 'pro_title_'.Session::get('lang_code');

      

   } 

   ?>

                    @if(count($getproductorderpayumoney_orderwise)<1) 

                              <tr>

                                 <td colspan="6" class="text-center">@if (Lang::has(Session::get('lang_file').'.NO_ORDER_PAYUMONEY')!= '') {{  trans(Session::get('lang_file').'.NO_ORDER_PAYUMONEY')}}  @else {{ trans($OUR_LANGUAGE.'.NO_ORDER_PAYUMONEY') }} @endif</td>

                              </tr>

                              @endif

                   <?php $j=1;

                   $item_tax = 0;

                                 // foreach($getproductordersdetails as $orderdet) {

                                    ?>

                              @foreach($getproductorderpayumoney_orderwise as $orderdet) 

                              <?php

                                 $walletusedamt_final1=DB::table('nm_ordercod_wallet')->where('nm_ordercod_wallet.cod_transaction_id','=', $orderdet->transaction_id)->get();

                                 

                                 $item_amt = $orderdet->order_amt + (($orderdet->order_amt * $orderdet->order_tax)/100);

                                       

                                 

                                        $ship_amt = $orderdet->order_shipping_amt;

                                       

                                        //$item_tax = $codorderdet->cod_tax;

                                       

                                         $grand_total =  ($item_amt + $ship_amt + $item_tax);

                                 

                                 ?>

                  <tr> 

                    <!--order number-->

                    <td data-title="Order Number">{{ $orderdet->transaction_id }}</td>

                    <!--order date-->

                    <td data-title="Order Date">{{  $orderdet->order_date }}</td>

                    <!--order status-->

                    <td data-title="Total">

                       <?php

                                       $customerid   = Session::get('customerid');

                                         $product_titles=DB::table('nm_order_payu')

                                         ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')

                                         ->leftjoin('nm_color', 'nm_order_payu.order_pro_color', '=', 'nm_color.co_id')

                                         ->leftjoin('nm_size', 'nm_order_payu.order_pro_size', '=', 'nm_size.si_id')

                                         ->where('transaction_id', '=', $orderdet->transaction_id)

                                         ->where('nm_order_payu.order_cus_id', '=', $customerid)

                                         ->where('nm_order_payu.order_type','=',1)

                                         ->get(); 

                                         $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0;

                                         $wallet_amt =    $wallet     = 0; 

                                       

                                         

                                          ?>

                                    @foreach($product_titles as $prd_tit) 

                                    <?php 

                                       $subtotal=$prd_tit->order_amt; 

                                                   $tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);

                                                

                                                   $total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 

                                                   $total_ship_amt+= $prd_tit->order_shipping_amt;

                                                   $total_item_amt+=$prd_tit->order_amt;

                                                   $coupon_amount+= $prd_tit->coupon_amount;

                                                   $prodct_id = $prd_tit->order_pro_id;

                                                   

                                             $item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);

                                                

                                             

                                                 $ship_amt = $prd_tit->order_shipping_amt;

                                                

                                             

                                       

                                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);

                                                

                                                $wallet_amt +=  $prd_tit->wallet_amount;                                                                                       $wallet     += $prd_tit->wallet_amount; ?>

                                    @endforeach



                      <span class="order-total">{{Helper::cur_sym()}} {{ $grand_total-$wallet_amt }}</span></td>

                      <!-- wallet amount -->

                      <td data-title="Total">  {{Helper::cur_sym()}} <?php 

                                    // print_r($walletusedamt_final);

                                     echo ((count($walletusedamt_final1)>0)?" ".number_format($walletusedamt_final1[0]->wallet_used,2):'0');

                                     

                                     ?></td>

                    <!--quanity-->

                    <td data-title="Total"> <a  class="btn btn-success" href="{{ url('product_payu_inv').'/'.$orderdet->transaction_id }}" target="new">@if (Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '') {{  trans(Session::get('lang_file').'.VIEW_DETAILS')}}  @else {{ trans($OUR_LANGUAGE.'.VIEW_DETAILS')}} @endif</a>



                                      

                                        <?php       

                                       //cancel ,return and replacement process starts

                                                $paypal_cancel_valid = 0;

                                                $paypal_return_replace_valid = 0;

                                                $cod_cancel_valid = 0;

                                                $cod_return_replace_valid= 0;

                                       

                                                /* cancel starts */

                                              

                                                $cancel_valid =  DB::table('nm_order_payu')

                                       ->join('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')            

                                       ->orderBy('nm_order_payu.order_id', 'desc')

                                       ->where('nm_order_payu.order_type', '=', 1)

                                       ->where('nm_order_payu.transaction_id', '=', $orderdet->transaction_id)

                                       ->where('delivery_status','=',1)->get();

                                                $paypal_cancel_valid =  count($cancel_valid);

                                                $return_replace =  DB::table('nm_order_payu')

                                       ->join('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')

                                       ->orderBy('nm_order_payu.order_id', 'desc')

                                       ->where('nm_order_payu.order_type', '=', 1)

                                       ->where('nm_order_payu.transaction_id', '=',$orderdet->transaction_id)

                                       ->where('delivery_status','=',4)->get();

                                                $paypal_return_replace_valid =  count($return_replace);

                                      

                                                //cancel ,return and replacement process ends

                                                ?>

                                    @if($paypal_cancel_valid>0)

                                    <?php 

                                       $cancel_allow = 0;

                                       $cancel_allowed_itm  = array();

                                       //check Cancel date

                                       ?>

                                    @foreach ($cancel_valid as $cancelItm)  <?php 

                                       $order_date = $cancelItm->order_date;

                                       $now = time(); //current date

                                       $format_date = strtotime($order_date);                   

                                       $datediff = abs($now - $format_date);

                                       

                                       $numberDays = $datediff/86400;  // 86400 seconds in one day

                                       

                                       // and you might want to convert to integer

                                       $Ord_datecount = intval($numberDays); ?>

                                    @if($cancelItm->allow_cancel==1)

                                    @if($Ord_datecount<$cancelItm->cancel_days)

                                    <?php 

                                       $delStatusInfo = DB::table('nm_order_delivery_status')->where('order_id','=',$cancelItm->order_id)->get(); 

                                       //print_r($delStatusInfo); 

                                       ?>

                                    @if(count($delStatusInfo)==0)

                                    <?php

                                       $cancel_allowed_itm[]  = array('prod_id' => $cancelItm->order_id,'prod_title' => $cancelItm->$pro_title );

                                       $cancel_allow++;

                                       ?>                

                                    @endif

                                    @endif

                                    @endif

                                    @endforeach

                                    @if($cancel_allow>0)

                                    <a class="btn btn-warning" data-target="<?php echo '#cancelModalpp'.$j;?>" data-toggle="modal">@if (Lang::has(Session::get('lang_file').'.CANCELLATION')!= '') {{  trans(Session::get('lang_file').'.CANCELLATION')}}  @else {{ trans($OUR_LANGUAGE.'.CANCELLATION')}} @endif</a>

                                    <!-- Modal -->

                                    <div id="<?php echo 'cancelModalpp'.$j;?>" class="modal fade" role="dialog">

                                       <div class="modal-dialog">

                                          <!-- Modal content-->

                                          <div class="modal-content">

                                             <div class="modal-header">

                                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                <h4 class="modal-title">@if (Lang::has(Session::get('lang_file').'.CANCELLATION')!= '') {{  trans(Session::get('lang_file').'.CANCELLATION')}}  @else {{ trans($OUR_LANGUAGE.'.CANCELLATION')}}      @endif</h4>

                                             </div>

                                             <div class="modal-body">

                                                {!! Form::open(array('url'=>'product_payu_cancel_order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}

                                                @if($paypal_cancel_valid>0)

                                                <input type="hidden" name="customer_mail" value="{{ $orderdet->ship_email }}">

                                                <input type="hidden" name="order_payment_type" value="1"><input type="hidden" name="order_type" value="1"><input type="hidden" name="mer_id" value="{{ $orderdet->order_merchant_id }}">

                                                <div class="form-group">

                                                   <label for="cancel_item_id">

                                                   @if (Lang::has(Session::get('lang_file').'.SELECT_ITEM_TO_CANCEL')!= '') {{  trans(Session::get('lang_file').'.SELECT_ITEM_TO_CANCEL')}}   @else {{ trans($OUR_LANGUAGE.'.SELECT_ITEM_TO_CANCEL')}} @endif

                                                   </label> 

                                                   <select id = "cancel_item_id" name="cancel_item_id">

                                                      @if(count($cancel_allowed_itm)>0)

                                                      @foreach($cancel_allowed_itm as $row)

                                                      <option value="{{ $row['prod_id'] }}"> {{ $row['prod_title'] }}</option>

                                                      @endforeach

                                                      @else

                                                      <option value=""> {{ (Lang::has(Session::get('lang_file').'.NO_ITEM')!= '') ? trans(Session::get('lang_file').'.NO_ITEM') : trans($OUR_LANGUAGE.'.NO_ITEM') }}</option>

                                                      @endif   

                                                   </select>

                                                </div>

                                                <div class="form-group">

                                                   <label for="cancel_notes">

                                                   @if (Lang::has(Session::get('lang_file').'.REASON_FOR_CANCEL')!= '') {{  trans(Session::get('lang_file').'.REASON_FOR_CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.REASON_FOR_CANCEL')}} @endif

                                                   </label>

                                                   <textarea id="<?php echo '#cancel_notes'.$j;?>" class="cancel_notes_<?php echo $j; ?>" name="cancel_notes" maxlength="300"  placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_CANCEL')}} @endif"></textarea>

                                                </div>

                                                @endif

                                             </div>

                                             <div class="modal-footer">

                                                <button  type="submit" onclick="return cancel_validate('<?php echo $j; ?>');" class="btn btn-danger conform_cancel"  id="<?php echo '#conform_cancel'.$j;?>" >@if (Lang::has(Session::get('lang_file').'.CONFIRM_CANCEL')!= '') {{  trans(Session::get('lang_file').'.CONFIRM_CANCEL')}}  @else {{ trans($OUR_LANGUAGE.'.CONFIRM_CANCEL')}}    @endif</button>

                                                <button type="button" class="btn btn-danger" data-dismiss="modal">@if (Lang::has(Session::get('lang_file').'.CLOSE')!= '') {{  trans(Session::get('lang_file').'.CLOSE')}}  @else {{ trans($OUR_LANGUAGE.'.CLOSE')}} @endif</button>

                                             </div>

                                             {!! Form::close() !!}

                                          </div>

                                       </div>

                                    </div>

                                    @endif   

                                    @endif

                                    <?php /* cancel end */ ?>

                                    @if($paypal_return_replace_valid>0)

                                    <?php 

                                       $return_allow = $replace_allow = 0;

                                       $return_allowed_itm  = array();

                                       $replace_allowed_itm  = array();

                                       //check Cancel date 

                                       ?>

                                    @foreach ($return_replace as $Itm) 

                                    <?php

                                       $order_date = $Itm->order_date;

                                       $now = time(); //current date

                                       $format_date = strtotime($order_date);                   

                                       $datediff = abs($now - $format_date);

                                       

                                       $numberDays = $datediff/86400;  // 86400 seconds in one day

                                       

                                       // and you might want to convert to integer

                                       $Ord_datecount = intval($numberDays);

                                       ?>

                                    @if($Itm->allow_return==1)

                                    @if($Ord_datecount<$Itm->return_days)

                                    <?php 

                                       $delStatusInfo = DB::table('nm_order_delivery_status')->where('order_id','=',$Itm->order_id)->get(); 

                                       //print_r($delStatusInfo);

                                       ?>

                                    @if(count($delStatusInfo)==0)

                                    <?php

                                       $return_allowed_itm[]  = array('prod_id' => $Itm->order_id,'prod_title' => $Itm->$pro_title );

                                       $return_allow++; ?> 

                                    @endif

                                    @endif

                                    @endif 

                                    @if($Itm->allow_replace==1)

                                    @if($Ord_datecount<$Itm->replace_days)

                                    <?php 

                                       $delStatusInfo = DB::table('nm_order_delivery_status')->where('order_id','=',$Itm->order_id)->get(); 

                                       //print_r($delStatusInfo);

                                       ?>

                                    @if(count($delStatusInfo)==0)

                                    <?php 

                                       $replace_allowed_itm[]  = array('prod_id' => $Itm->order_id,'prod_title' => $Itm->$pro_title );

                                       $replace_allow++; ?> 

                                    @endif

                                    @endif

                                    @endif

                                    @endforeach

                                    @if($return_allow>0)

                                    <a class="btn btn-blue"  data-target="<?php echo '#returnModalpp'.$j;?>" data-toggle="modal">@if (Lang::has(Session::get('lang_file').'.RETURN')!= '') {{  trans(Session::get('lang_file').'.RETURN')}}  @else {{ trans($OUR_LANGUAGE.'.RETURN')}} @endif</a>

                                    <!-- REturn Modal -->

                                    <div id="<?php echo 'returnModalpp'.$j;?>" class="modal fade" role="dialog">

                                       <div class="modal-dialog">

                                          <!-- Modal content-->

                                          <div class="modal-content">

                                             <div class="modal-header">

                                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                <h4 class="modal-title">@if (Lang::has(Session::get('lang_file').'.ORDER_RETURN')!= '') {{  trans(Session::get('lang_file').'.ORDER_RETURN')}}  @else {{ trans($OUR_LANGUAGE.'.ORDER_RETURN')}}      @endif</h4>

                                             </div>

                                             <div class="modal-body">

                                                {!! Form::open(array('url'=>'product_payu_return_order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}

                                                @if($return_allow>0)

                                                <input type="hidden" name="customer_mail" value="{{ $codorderdet->ship_email }}">

                                                <input type="hidden" name="order_payment_type" value="1"><input type="hidden" name="order_type" value="1">

                                                <div class="form-group">

                                                   <label for="return_item_id">

                                                   @if (Lang::has(Session::get('lang_file').'.SELECT_ITEM_TO_RETURN')!= '') {{  trans(Session::get('lang_file').'.SELECT_ITEM_TO_RETURN')}}   @else {{ trans($OUR_LANGUAGE.'.SELECT_ITEM_TO_RETURN')}}   @endif

                                                   </label> 

                                                   <select id = "return_item_id" name="return_item_id">

                                                      @if(count($return_allowed_itm)>0)

                                                      @foreach($return_allowed_itm as $row)

                                                      <option value="{{ $row['prod_id'] }}"> {{ $row['prod_title'] }}</option>

                                                      @endforeach

                                                      @else

                                                      <option value=""> {{ (Lang::has(Session::get('lang_file').'.NO_ITEM')!= '') ? trans(Session::get('lang_file').'.NO_ITEM') : trans($OUR_LANGUAGE.'.NO_ITEM') }}</option>

                                                      @endif   

                                                   </select>

                                                </div>

                                                <div class="form-group">

                                                   <label for="return_notes">

                                                   @if (Lang::has(Session::get('lang_file').'.REASON_FOR_RETURN')!= '') {{  trans(Session::get('lang_file').'.REASON_FOR_RETURN')}}  @else {{ trans($OUR_LANGUAGE.'.REASON_FOR_RETURN')}} @endif 

                                                   </label>

                                                   <textarea id="return_notes" class="return_notes_<?php echo $j; ?>" maxlength="300" name="return_notes" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_RETURN')}} @endif"></textarea>

                                                </div>

                                                @endif

                                             </div>

                                             <div class="modal-footer">

                                                <button onclick="return return_validate('<?php echo $j; ?>');" type="submit" class="btn btn-danger return_conform" >@if (Lang::has(Session::get('lang_file').'.CONFIRM_RETURN')!= '') {{  trans(Session::get('lang_file').'.CONFIRM_RETURN')}}  @else {{ trans($OUR_LANGUAGE.'.CONFIRM_RETURN')}} @endif</button>

                                                <button type="button" class="btn btn-danger" data-dismiss="modal">@if (Lang::has(Session::get('lang_file').'.CLOSE')!= '') {{  trans(Session::get('lang_file').'.CLOSE')}}  @else {{ trans($OUR_LANGUAGE.'.CLOSE')}} @endif</button>

                                             </div>

                                             {!! Form::close() !!}

                                          </div>

                                       </div>

                                    </div>

                                    @endif

                                    @if($replace_allow>0)

                                    <a class="btn btn-info"  data-target="<?php echo '#replaceModal_pp'.$j;?>" data-toggle="modal">@if (Lang::has(Session::get('lang_file').'.REPLACE')!= '') {{  trans(Session::get('lang_file').'.REPLACE')}}  @else {{ trans($OUR_LANGUAGE.'.REPLACE')}} @endif</a>

                                    

                                    <!-- REturn Modal -->

                                    <div id="<?php echo 'replaceModal_pp'.$j;?>" class="modal fade" role="dialog">

                                       <div class="modal-dialog">

                                          <!-- Modal content-->

                                          <div class="modal-content">

                                             <div class="modal-header">

                                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                <h4 class="modal-title">@if (Lang::has(Session::get('lang_file').'.ORDER_REPLACE')!= '') {{  trans(Session::get('lang_file').'.ORDER_REPLACE')}}  @else {{ trans($OUR_LANGUAGE.'.ORDER_REPLACE')}} @endif</h4>

                                             </div>

                                             <div class="modal-body">

                                                {!! Form::open(array('url'=>'product_payu_replace_order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}

                                                @if($return_allow>0)

                                                <input type="hidden" name="customer_mail" value="{{ $codorderdet->ship_email }}">

                                                <input type="hidden" name="order_payment_type" value="1"><input type="hidden" name="order_type" value="1">

                                                <div class="form-group">

                                                   <label for="replace_item_id">

                                                   @if (Lang::has(Session::get('lang_file').'.SELECT_ITEM_TO_REPLACE')!= '') {{  trans(Session::get('lang_file').'.SELECT_ITEM_TO_REPLACE')}}  @else {{ trans($OUR_LANGUAGE.'.SELECT_ITEM_TO_REPLACE')}} @endif

                                                   </label> 

                                                   <select id = "replace_item_id" name="replace_item_id">

                                                      @if(count($replace_allowed_itm)>0)

                                                      @foreach($replace_allowed_itm as $row)

                                                      <option value="{{ $row['prod_id'] }}"> {{ $row['prod_title'] }}</option>

                                                      @endforeach

                                                      @else

                                                      <option value=""> {{ (Lang::has(Session::get('lang_file').'.NO_ITEM')!= '') ? trans(Session::get('lang_file').'.NO_ITEM') : trans($OUR_LANGUAGE.'.NO_ITEM') }}</option>

                                                      @endif   

                                                   </select>

                                                </div>

                                                <div class="form-group">

                                                   <label for="replace_notes">

                                                   @if (Lang::has(Session::get('lang_file').'.REASON_FOR_REPLACE')!= '') {{  trans(Session::get('lang_file').'.REASON_FOR_REPLACE')}}  @else {{ trans($OUR_LANGUAGE.'.REASON_FOR_REPLACE')}} @endif

                                                   </label>

                                                   <textarea id="replace_notes"  class="replace_notes_<?php echo $j; ?>" name="replace_notes" maxlength="300" placeholder="@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE')}}  @else {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_REPLACE')}} @endif"></textarea>

                                                </div>

                                                @endif

                                             </div>

                                             <div class="modal-footer">

                                                <button onclick="return replace_validate('<?php echo $j; ?>');" type="submit" class="btn btn-danger replace_conform" >@if (Lang::has(Session::get('lang_file').'.CONFIRM_REPLACE')!= '') {{  trans(Session::get('lang_file').'.CONFIRM_REPLACE')}}  @else {{ trans($OUR_LANGUAGE.'.CONFIRM_REPLACE')}} @endif</button>

                                                <button type="button" class="btn btn-danger" data-dismiss="modal">@if (Lang::has(Session::get('lang_file').'.CLOSE')!= '') {{  trans(Session::get('lang_file').'.CLOSE')}}  @else {{ trans($OUR_LANGUAGE.'.CLOSE')}} @endif</button>

                                             </div>

                                             {!! Form::close() !!}

                                          </div>

                                       </div>

                                    </div>

                                    @endif

                                    @endif

                                    



                    </td>

                   

                  </tr>

                  

                  

                  <?php $j=$j+1;  ?>   

                              @endforeach 

                  

                  

                  

                  

                  

                  

                  

                  

                </tbody>

              </table>

            </div>

              <div class="pagination-area">

      

              {!! $getproductorderpayumoney_orderwise->render() !!}

              

              

          </div>

          </div>



        </div>

         @include('dashboard_sidebar')

      </div>

    </div>

  </section>

  <!-- service section -->

   @include('service_section')

  

  <!-- Footer -->

  {!! $footer !!}

  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<!-- JS --> 



<script type="text/javascript">

 function cancel_validate(id){

  var cancel_id = $(".cancel_notes_"+id).val();

  if(cancel_id == ''){

    alert("{{ (Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL') : trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_CANCEL') }}");

    return false;

    }

  }

  

  //for return validate

  function return_validate(id){

  var cancel_id = $(".return_notes_"+id).val();

  if(cancel_id == ''){

    alert("{{ (Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN') : trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_RETURN') }}");

    return false;

    }

  }

  

  //for replace validate

  function replace_validate(id){

  var cancel_id = $(".replace_notes_"+id).val();

  if(cancel_id == ''){

    alert("{{ (Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE') : trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_REPLACE') }}");

    return false;

    }

  }

</script>

</body>

</html>