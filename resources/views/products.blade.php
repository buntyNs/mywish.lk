{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}


<!-- Header End====================================================================== -->

<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

<?php $prod_path_loader = url('').'/public/assets/noimage/product_loading.gif';

if(!isset($_SESSION['compare_product'])&&(!isset($_SESSION['sub_cat_id']))){

$_SESSION['compare_product']=array();

$_SESSION['sub_cat_id']      = array();

}

?>



<div class="loading_prnt">

	<div class="loadingGiff"></div>

</div>



  <div class="breadcrumbs">

    <div class="container">

	@if(Session::has('wish'))

	<p class="alert {!! Session::get('alert-class', 'alert-success') !!}">{!! Session::get('wish') !!}</p>

	@endif

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li>

            <li><strong>{{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '') ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT') }}</strong> </li>

          </ul>

        </div>

      </div>

    </div>

  </div>



  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout" alt="Products Page">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

           <?php /* <div class="category-description std">

            <div class="slider-items-products">

              <div id="category-desc-slider" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 

                  

                  <!-- Item -->

                 <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img1.jpg"></a>

                    <div class="inner-info">

                      <div class="cat-img-title"> <span>Best Product 2017</span>

                        <h2 class="cat-heading">Best Selling Brand</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

                        <a class="info" href="#">Shop Now</a> </div>

                    </div>

                  </div>

                  <!-- End Item --> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img2.jpg"></a> </div> 

                  

                  <!-- End Item --> 

                  

                </div>

              </div>

            </div>

          </div> */ ?>

          <div class="shop-inner" id="prdt_ajax_display">

            <div class="page-title">

              <h2>{{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '') ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT') }}</h2>





              <span   style="float:right; margin-right: 90px;margin-top: -20px;">

      <a href="{!! url('compare_products') !!}" target="_blank">@if (Lang::has(Session::get('lang_file').'.COMPARE')!= '') {{  trans(Session::get('lang_file').'.COMPARE') }} @else {{ trans($OUR_LANGUAGE.'.COMPARE') }} @endif

      

    <?php echo ' <span>'.$count = count($_SESSION['compare_product']); ?> </a>

              </span>



              @if($count > 0)



              <div class="or"></div>

             

              <span   style="float:right; margin-top: -19px;" ><a href="{!! url('clear_compare') !!}">@if (Lang::has(Session::get('lang_file').'.CLEAR_LIST')!= '') {{  trans(Session::get('lang_file').'.CLEAR_LIST') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_LIST') }} @endif</a></span>



                @endif



          

              

            </div>

           

            @if($maincategory_id=='')

            <div class="toolbar">              

              <div class="sorter">

                <div class="short-by">

        				@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

        					@php  $title = 'pro_title'; @endphp

        				@else 

        					@php  $title = 'pro_title_'.Session::get('lang_code'); @endphp 

        				@endif

                  <label>@if (Lang::has(Session::get('lang_file').'.SORT_BY')!= '') {{  trans(Session::get('lang_file').'.SORT_BY') }} @else {{ trans($OUR_LANGUAGE.'.SORT_BY') }} @endif:</label>

                  <select name="filtertypes" onchange="displayproductrecords('<?php echo $page_limit; ?>','<?php echo $pagenum; ?>',this.options[this.selectedIndex].value);">

                    <option value="">{{ (Lang::has(Session::get('lang_file').'.SORT_BY')!= '') ? trans(Session::get('lang_file').'.SORT_BY') : trans($OUR_LANGUAGE.'.SORT_BY') }}</option>

                    <option value="1" >@if (Lang::has(Session::get('lang_file').'.PRICE_LOW')!= '') {{  trans(Session::get('lang_file').'.PRICE_LOW') }} @else {{ trans($OUR_LANGUAGE.'.PRICE_LOW') }} @endif - @if (Lang::has(Session::get('lang_file').'.HIGH')!= '') {{  trans(Session::get('lang_file').'.HIGH') }} @else {{ trans($OUR_LANGUAGE.'.HIGH') }} @endif</option>

                    <option value="2" >@if (Lang::has(Session::get('lang_file').'.PRICE_HIGH')!= '') {{  trans(Session::get('lang_file').'.PRICE_HIGH') }} @else {{ trans($OUR_LANGUAGE.'.PRICE_HIGH') }} @endif -@if (Lang::has(Session::get('lang_file').'.LOW')!= '') {{  trans(Session::get('lang_file').'.LOW') }} @else {{ trans($OUR_LANGUAGE.'.LOW') }} @endif</option>

                    <option  value="3" >@if (Lang::has(Session::get('lang_file').'.TITLE')!= '') {{  trans(Session::get('lang_file').'.TITLE') }} @else {{ trans($OUR_LANGUAGE.'.TITLE') }} @endif @if (Lang::has(Session::get('lang_file').'.A')!= '') {{ trans(Session::get('lang_file').'.A') }}  @else {{ trans($OUR_LANGUAGE.'.A') }} @endif-@if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif</option>

                    <option value="4">@if (Lang::has(Session::get('lang_file').'.TITLE')!= '') {{  trans(Session::get('lang_file').'.TITLE') }} @else {{ trans($OUR_LANGUAGE.'.TITLE') }} @endif @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif-@if (Lang::has(Session::get('lang_file').'.A')!= '') {{  trans(Session::get('lang_file').'.A') }} @else {{ trans($OUR_LANGUAGE.'.A') }} @endif</option>

                    <option value="5">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{  trans(Session::get('lang_file').'.DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif @if (Lang::has(Session::get('lang_file').'.A')!= '') {{ trans(Session::get('lang_file').'.A') }}  @else {{ trans($OUR_LANGUAGE.'.A') }} @endif- @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{ trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif</option>

                    <option  value="6">@if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{ trans(Session::get('lang_file').'.DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif @if (Lang::has(Session::get('lang_file').'.Z')!= '') {{  trans(Session::get('lang_file').'.Z') }} @else {{ trans($OUR_LANGUAGE.'.Z') }} @endif- @if (Lang::has(Session::get('lang_file').'.A')!= '') {{  trans(Session::get('lang_file').'.A') }} @endif</option>

                  </select>

                </div>

                <div class="short-by page">

                  <label>@if (Lang::has(Session::get('lang_file').'.SHOW')!= '') {{  trans(Session::get('lang_file').'.SHOW') }} @else {{ trans($OUR_LANGUAGE.'.SHOW') }} @endif @if (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') {{  trans(Session::get('lang_file').'.PER_PAGE') }} @else {{ trans($OUR_LANGUAGE.'.PER_PAGE') }} @endif:</label>

                  <select name="perpagenumber" onchange="displayproductrecords(this.options[this.selectedIndex].value,'<?php echo $pagenum; ?>','<?php echo $filter; ?>')" >

          					<option value="9">9 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="18">18 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="36">36 {{ (Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE') }}</option>

          					<option value="all">@if (Lang::has(Session::get('lang_file').'.ALL')!= '') {{  trans(Session::get('lang_file').'.ALL') }} @else {{ trans($OUR_LANGUAGE.'.ALL') }} @endif </option>

                  </select>

                </div>                   

              </div>

            </div>



            @endif            

           

           <div class="product-grid-area">

             <ul class="products-grid">

			  @if(count($product_details) != 0) 

				@foreach($product_details as $product_det) 

				

				@php $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

				$smcat 	= strtolower(str_replace(' ','-',$product_det->smc_name));

				$sbcat 	= strtolower(str_replace(' ','-',$product_det->sb_name));

				$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 

				$res = base64_encode($product_det->pro_id);

				$product_image 	= explode('/**/',$product_det->pro_Img);

				$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

				$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); @endphp

				@if($product_det->pro_no_of_purchase < $product_det->pro_qty)   

					

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $title = 'pro_title'; @endphp

					@else @php  $title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

					

					@php   $product_img= explode('/**/',trim($product_det->pro_Img,"/**/")); 

					$img_count = count($product_img); @endphp



					@php $product_image 	= $product_image[0];

					 

					$prod_path 	= url('').'/public/assets/default_image/No_image_product.png';

					$img_data 	= "public/assets/product/".$product_image; @endphp

					

					@if(file_exists($img_data) && $product_image !='')	

					

					

					@php $prod_path = url('').'/public/assets/product/'.$product_image;	@endphp				

					@else	

			

						@if(isset($DynamicNoImage['productImg']))  

						  

						@php $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

							@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

							@php $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

						@endif

					@endif	

					

				 @php $alt_text 	= substr($product_det->$title,0,25);

					$alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp

					

				  @php $count = $product_det->pro_qty - $product_det->pro_no_of_purchase; @endphp

				  

			

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					  	@if($product_det->pro_discount_percentage!='' && round($product_det->pro_discount_percentage)!=0)		

                        <div class="icon-sale-label sale-left">{{ substr($product_det->pro_discount_percentage,0,2) }}%</div>@endif

                       

					   <div class="pr-img-area">

					   @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

					      <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> <img class="product__image" src="{{ $prod_path }}" data-src="{{ $prod_path }}" alt="{{ $alt_text }}" title=""/> 

						  <!--<img class="hover-img" src="<?php echo url(''); ?>/public/themes/images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

					  @endif

					  

					  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

					      <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" >

                          <figure> <img class="product__image" src="{{ $prod_path }}" alt="" data-src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="<?php echo url(''); ?>/public/themes/images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

					  @endif

					  

					 @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

					      <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" >

                          <figure> <img class="product__image" src="{{ $prod_path }}" alt="{{ $alt_text }}" data-src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="<?php echo url(''); ?>/public/themes/images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

					  @endif

					  

					   @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 

					      <a href="{!! url('productview').'/'.$mcat.'/'.$res!!}" >

                          <figure> <img class="product__image" alt="{{ $alt_text }}" src="{{ $prod_path }}" data-src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="<?php echo url(''); ?>/public/themes/images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

					  @endif

					 

					   </div>

						  

						  

                        <div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> 

                            	{{-- Add to wishlist--}}

								

								@if(Session::has('customerid'))

								@php  

								$cus_id = Session::get('customerid');

								$prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',$cus_id)->first(); @endphp

							    @else

								@php  $prodInWishlist = array(); @endphp

								@endif

					   

								@if($count > 0)  

						        @if(Session::has('customerid'))

								@if(count($prodInWishlist)==0)

								  <input type="hidden" name="_token" value="{!! csrf_token() !!}" />

										{{ Form::hidden('pro_id','$product_det->pro_id') }}        

								  <!-- <input type="hidden" name="pro_id" value="<?php echo $product_det->pro_id; ?>"> -->

								  <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">

								  <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})" title=" <?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '')  { echo  trans(Session::get('lang_file').'.ADD_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">

								  <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">

									<i class="fa fa-heart-o" aria-hidden="true"></i>

								  </a>

											@else

											<?php /* remove wishlist */?>   

												

											<a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}" title=" <?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '')  { echo  trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">

											<i class="fa fa-heart" aria-hidden="true"></i>   

											</a> 

											<?php /*remove link:remove_wish_product/wishlist table_id*/ ?>

												

								@endif  

						   @else 

							  <a href="" role="button" data-toggle="modal" data-target="#loginpop" title=" <?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '')  { echo  trans(Session::get('lang_file').'.ADD_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">

							  

								<i class="fa fa-heart-o" aria-hidden="true"></i>

							  

							  </a>

						  @endif

							@endif

                             </div>



                            @if(($compare==0)&&($maincategory_id!='')) 

                            @if(in_array($product_det->pro_id, $_SESSION['compare_product'])) 

                            <div class="mt-button add_to_compare" onclick="comparefunc(<?php echo $product_det->pro_id.','.'0'.','.$maincategory_id; ?>);" value="0" name="compare" id="compare"> <a href="" title="Remove from compare"> <i class="fa fa-check" ></i> </a> </div>

                            @else 

                             <div class="mt-button add_to_compare" onclick="comparefunc(<?php echo $product_det->pro_id.','.'1'.','.$maincategory_id; ?>);" value="1" name="compare" id="compare"> <a href="" title="Add to compare"> <i class="fa fa-signal" ></i> </a> </div>

                             @endif

                             @endif  

                            <div class="mt-button quick-view"> <a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap{{ $product_det->pro_id }}">

							<i class="fa fa-search" data-tooltip="Add to Compare"></i> </a> </div>

                          </div>

                        </div>

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="">

						  {{ substr($product_det->$title,0,25) }}

						  {{  strlen($product_det->$title)>25?'..':'' }}

						  </a> </div>

                          <div class="item-content">

						  

						     @php					  

							  $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();

							  $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();

							  $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();

							  $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();

							  $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();

							  

							  

							  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

							  $multiple_countone = $one_count *1;

							  $multiple_counttwo = $two_count *2;

							  $multiple_countthree = $three_count *3;

							  $multiple_countfour = $four_count *4;

							  $multiple_countfive = $five_count *5;

							  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp



                         <div class="rating">

						 @if($product_count)

						 @php   $product_divide_count = $product_total_count / $product_count; @endphp

						 @if($product_divide_count <= '1') 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 @elseif($product_divide_count >= '1') 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 @elseif($product_divide_count >= '2') 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

						 @elseif($product_divide_count >= '3') 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 @elseif($product_divide_count >= '4') 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 @elseif($product_divide_count >= '5') 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

						 @else

							

						@endif

					@else

						 <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					@endif	

				   </div>

							

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>

                            </div>

							

							

                            <div class="pro-action">

                             @if($product_det->pro_no_of_purchase >= $product_det->pro_qty) 

                              <button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span> </button>

						     @else

								 

							@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  

                              <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)}}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							  </span></button></a>

							@endif

							 

							@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

                             <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							  </span></button></a>

							@endif

							

							@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

                             <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							  </span></button></a>

							@endif

							

							@if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 

                             <a href="{{ url('productview/'.$mcat.'/'.$res) }}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif 

							  </span></button></a>

							@endif

							 

							@endif 	

                            </div>

							

							

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

				

				@endif

			

				@endforeach
             </ul>

           
				

				@elseif(count($product_details) == 0)

				

				<div class="jplist-no-results text-shadow align-center">

				<p style="color: rgb(54, 160, 222); float: left; clear: both; margin-left: 30%; margin-top: 10%;

				margin-bottom: 10%; font-size: 18px;">@if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') }} @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE') }} @endif



			</p></div>

				

				@endif

				
      </div>
              



            @if($maincategory_id=='')

            <div class="pagination-area">			

              <?php

              if ( ($pagenum-1) > 0) 

              {

                $prpre =$pagenum-1;

              ?>

              <div class="" >

                <button style="vertical-align: top; margin-top: 0px;" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo 1; ?>','<?php echo $filter;  ?>');" data-type="first">«</button>

                <button style="vertical-align: top; margin-top: 0px;" data-number="0" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $pagenum-1; ?>','<?php echo $filter;  ?>');">‹</button> 

              </div>

              <?php

              } 

              ?>

              <span  class="">

              <?php

              $links=$pagenum+4; 

              for($i=$pagenum; $i<=$links; $i++)

              {

                if($i<=$last)

                {

                  if ($i == $pagenum ) 

                  {

                  ?>

                    <button style="vertical-align: top; margin-top: 0px;" type="button" class="active" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $i; ?>','<?php echo $filter;  ?>');" ><?php echo $i; ?></button> 

                  <?php

                  }

                  else

                  {

                  ?>

                    <button style="vertical-align: top; margin-top: 0px;" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $i; ?>','<?php echo $filter;  ?>');" ><?php echo $i; ?></button>  

                  <?php

                  }

                }

              }

              ?>  

              </span>

              

              <span class="pagina-nav" >

              <?php 

              if ( ($pagenum+1) <= $last)

              {

              ?>

                <button style="vertical-align: top;" data-number="1" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $pagenum+1; ?>','<?php echo $filter;  ?>');" >›</button>

                <button style="vertical-align: top;" data-number="3" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $last; ?>','<?php echo $filter;  ?>');" >»</button>

              <?php

              }

              ?>

              </span>

            </div>

            @endif

          </div>

        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

		

	

	<!-- Sidebar ================================================== -->



 <div class="block shop-by-side">

	<div class="sidebar-bar-title">

         <h3>{{ (Lang::has(Session::get('lang_file').'.SHOP_BY')!= '') ?  trans(Session::get('lang_file').'.SHOP_BY'): trans($OUR_LANGUAGE.'.SHOP_BY') }}</h3>

     </div>

	 <div class="block-content">

	 <p class="block-subtitle">{{ (Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES') }}</p>

	<div id="divMenu">

	<ul>

	@if(count($product_details) != 0)

	@if($maincategory_id=='')

	@php $i=1;  @endphp

	@if(count($main_category)>0)



	@foreach($main_category as $fetch_main_cat)

	 

	@php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; @endphp

		@if($i<=20)

	@if(count($sub_main_category[$fetch_main_cat->mc_id])> 0)



			<li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1) }}">

			@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				@php  $mc_name = 'mc_name'; @endphp

				@else @php  $mc_name = 'mc_name_code';
 
				@endphp @endif
 
				{{ $fetch_main_cat->$mc_name }}</a>
     


				<ul>

					@foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)

					@php $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp

					

					<li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2) }}">

						@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

							@php  $smc_name = 'smc_name'; @endphp

							@else @php  $smc_name = 'smc_name_code'; @endphp @endif

							{{  $fetch_sub_main_cat->$smc_name }} </a> 

							

						@if(count($second_main_category[$fetch_sub_main_cat->smc_id])> 0)

						<ul>

					

						@foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)

						

						@php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp



						<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>">

							@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

							@php $sb_name = 'sb_name'; @endphp

							@else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

							{{ $fetch_sub_cat->$sb_name }} </a>

							

							@if(count($second_sub_main_category[$fetch_sub_cat->sb_id])> 0)

								<ul>

								@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  

								@php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp

									

									<li><a href="{{ url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4) }}"> 

										@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

										@php $ssb_name = 'ssb_name'; @endphp

										@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

										{{ $fetch_secsub_cat->$ssb_name }}</a> </li>                                    

								

										@endforeach

									

								</ul>

							@endif

							</li>

							@endforeach 

						</ul>

					@endif

					</li>

					

				@endforeach 

				</ul>

@endif @endif

	@endforeach @php $i++; @endphp

@endif

	

		



@else	

 @php $get_listby_id   = explode(",", $category_id); @endphp <!-- //category id -->

		@if ($get_listby_id[0] == 1)

         @php   $mc_id =  DB::table('nm_maincategory')->where('mc_id', '=', $get_listby_id[1])->value('mc_id');

            

			$i=1; @endphp

	@if(count($mc_id)>0)

	

		@php $pass_cat_id1 = "1,".$mc_id; @endphp <!-- //topcategory -->

		@if($i<=20)

		



			@if(count($sub_main_category[$mc_id])> 0)

			

			

				@foreach($sub_main_category[$mc_id] as $fetch_sub_main_cat)

				

				@php $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; @endphp	<!-- //maincategory -->

				

					<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"> 

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php $smc_name = 'smc_name'; @endphp

					@else @php  $smc_name = 'smc_name_code'; @endphp @endif

					{{ $fetch_sub_main_cat->$smc_name }}</a>

				

				@if(count($second_main_category[$fetch_sub_main_cat->smc_id])> 0)

				

				<ul>

							 

							@foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat)

							

							@php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp

							

					<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>">

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $sb_name = 'sb_name'; @endphp

					@else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

							{{ $fetch_sub_cat->$sb_name }} </a>

							

							@if(count($second_sub_main_category[$fetch_sub_cat->sb_id])> 0)

								

								<ul>

								

								@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  

							@php	$pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp

								                       

									<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>"> 

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $ssb_name = 'ssb_name'; @endphp

					@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

									{{ $fetch_secsub_cat->$ssb_name }}</a></li>                                        

								

								@endforeach 

							

							</ul>

							

							@endif

						

							</li>

							

							@endforeach

						</ul>

						

						@endif

					@endforeach

				

			

			 

		@endif

	@endif @php $i++; @endphp



@endif	

							

							

	<li><a href="{{ url('products') }}"><b> << {{ (Lang::has(Session::get('lang_file').'.BACK')!= '') ? trans(Session::get('lang_file').'.BACK') : trans($OUR_LANGUAGE.'.BACK') }} </b></a>	



  @elseif ($get_listby_id[0] == 2)

        @php   $mc_id =   DB::table('nm_secmaincategory')->select('smc_id','smc_mc_id')->where('smc_id', '=', $get_listby_id[1])->get(); @endphp

	

@php $i=1; @endphp 

	@if(count($mc_id)>0)

@php	$smc_id = $mc_id[0]->smc_id;	

	  $mc_id = $mc_id[0]->smc_mc_id;	



		$pass_cat_id1 = "1,".$mc_id; @endphp 

		@if($i<=20)



	

				@php $pass_cat_id2 = "2,".$smc_id; @endphp	<!-- //maincategory -->

					

					

					

					

						@if(count($second_main_category[$smc_id])> 0)

						

						

							

							@foreach($second_main_category[$smc_id] as $fetch_sub_cat)

							

							@php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; @endphp

							

							<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>">

				@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $sb_name = 'sb_name'; @endphp

					@else @php  $sb_name = 'sb_name_langCode'; @endphp @endif

							{{ $fetch_sub_cat->$sb_name }} </a>

						

							@if(count($second_sub_main_category[$fetch_sub_cat->sb_id])> 0)

							

							

							<ul>

							

							@foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat)  

							@php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp

								                        

									<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>"> 

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $ssb_name = 'ssb_name'; @endphp

					@else @php  $ssb_name = 'ssb_name_langCode'; @endphp @endif

									{{ $fetch_secsub_cat->$ssb_name }}</a></li>                                        

								

								@endforeach 

								

							</ul>

							

							@endif

							

							</li>

							

							@endforeach 

							

							@endif

				

	@endif @php $i++; @endphp



@endif

			

<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>"><b> << {{ (Lang::has(Session::get('lang_file').'.BACK')!= '') ? trans(Session::get('lang_file').'.BACK') : trans($OUR_LANGUAGE.'.BACK') }} </b></a> 

	@elseif ($get_listby_id[0] == 3)

   @php   $sb_smc_id = DB::table('nm_subcategory')->select('sb_id','sb_smc_id','mc_id')->where('sb_id', '=', $get_listby_id[1])->get();



	$i=1; @endphp

	@if(count($sb_smc_id)!=0)

		

	@php  $smc_id = $sb_smc_id[0]->sb_smc_id;  

	  $mc_id = $sb_smc_id[0]->mc_id;	

	  $sb_id = $sb_smc_id[0]->sb_id; @endphp

		

		@if($i<=20)

		

 				@php	$pass_cat_id2 = "2,".$smc_id;	

						  	 $pass_cat_id3 = "3,".$sb_id; @endphp

				

					@if(count($second_sub_main_category[$sb_id])> 0)

						@foreach($second_sub_main_category[$sb_id] as $fetch_secsub_cat)  

							@php	$pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id;  @endphp

								<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>">

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

						@php  $ssb_name = 'ssb_name';@endphp

					@else @php  $ssb_name = 'ssb_name_'.Session::get('lang_code'); @endphp @endif

								{{ $fetch_secsub_cat->$ssb_name }}</a></li>                                        

						

						@endforeach 

					@endif

	@endif @php $i++; @endphp



@endif

<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"><b> << {{ (Lang::has(Session::get('lang_file').'.BACK')!= '') ? trans(Session::get('lang_file').'.BACK') : trans($OUR_LANGUAGE.'.BACK') }}</b></a> 

        

@elseif ($get_listby_id[0] == 4) 

      @php    $sb_id =  DB::table('nm_secsubcategory')->select('ssb_sb_id','mc_id','ssb_smc_id')->where('ssb_id', '=', $get_listby_id[1])->get();

			

			

	$i=1; @endphp

	@if(count($sb_id)!=0)

		@php $ssb_sb_id = $sb_id[0]->ssb_sb_id;	

		 $ssb_smc_id = $sb_id[0]->ssb_smc_id;	

	  	 $mc_id = $sb_id[0]->mc_id; @endphp

		  @if($i<=20)

		 

						@php  	 $pass_cat_id3 = "3,".$ssb_sb_id; @endphp

				

					@if(count($second_sub_main_category[$ssb_sb_id])> 0)

						@foreach($second_sub_main_category[$ssb_sb_id] as $fetch_secsub_cat)  

							@php	$pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; @endphp

								<li class="subfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>"> 

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

						@php  $ssb_name = 'ssb_name'; @endphp

					@else @php  $ssb_name = 'ssb_name_'.Session::get('lang_code'); @endphp @endif

								{{ $fetch_secsub_cat->ssb_name }}</a></li>                                        

						

						@endforeach 

					@endif

				

		@endif

	@endif 

	@php $i++; @endphp

<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"><b> << Back </b></a> 

@endif @endif<!-- //else of main cat -->

@else

 <li><a href=""><?php if (Lang::has(Session::get('lang_file').'.NO_CATEGORY_FOUND')!= '') { echo  trans(Session::get('lang_file').'.NO_CATEGORY_FOUND');}  else { echo trans($OUR_LANGUAGE.'.NO_CATEGORY_FOUND');} ?></a></li>  @endif

</ul>

 </div>

 

<br>



	

	 

	 

	  @php

		$discount_filter=array();

		$filters_color=array();

		$size_filter=array(); @endphp

		@if(isset($_GET["filter_color"]))	

		@php $filter_color=base64_decode($_GET["filter_color"]);

			$filters_color=explode(",",$filter_color); @endphp

		@endif

		

		@if(isset($_GET["filter_discount"]))

		@php $filter_discount=base64_decode($_GET["filter_discount"]);

			$discount_filter=explode(",",$filter_discount); @endphp

		@endif

		

		@if(isset($_GET["filters_size"]))

		@php $filters_size=base64_decode($_GET["filters_size"]);

			$size_filter=explode(",",$filters_size); @endphp

		@endif

		

		@if(count($specification)>0 AND count($specification_values)>0)

			

			@if(isset($_GET["filter"]))

			 @php $filtered_item=base64_decode($_GET["filter"]);

					$filters=explode(",",$filtered_item); @endphp

			 @else

				@php $filters=array(); @endphp

			@endif

			

	  <div class="block product-price-range">

	  <div class="sidebar-bar-title"><h3>{{ (Lang::has(Session::get('lang_file').'.FILTER')!= '') ?  trans(Session::get('lang_file').'.FILTER') : trans($OUR_LANGUAGE.'.FILTER') }}</h3></div>

	  <form name="filter_form">

		@foreach($specification as $specification_det)

			<div class="amount-range-price"><h5>{{ $specification_det->spg_name }}</h5></div>

			 <ul class="check-box-list filter-height">

			

			@foreach($specification_values as $specification_val)

				

				@if($specification_val->sp_spg_id==$specification_det->spg_id)

					 <li><input type="checkbox" id="checkbox-sep{{ $specification_val->sp_id }}" name="filter_by" value="{{ $specification_val->sp_id }}" <?php if(in_array($specification_val->sp_id,$filters)){echo "checked";} ?> onclick="javascript:make_filter()"><label for="checkbox-sep<?php echo $specification_val->sp_id; ?>"> <span class="button"></span> {{ $specification_val->sp_name }}</label> </li>

				@endif



			@endforeach

			</ul>

			<hr>

		@endforeach

	 </form>

		@if(isset($_GET["filter"]) AND $_GET["filter"]!="")

			<button type="button" onclick="javascript:clear_filter('filter_for_values')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

		@endif 

	</div>

	 @endif

	 

	@if(Request::segment(1)=="catproducts") 

	

	 @php $filtered_item="";

				$price_from="";

				$price_to="";

				$filter_discount="";

				$filter_size=""; @endphp

				@if(isset($_GET["filter"]))

					

	@php $filtered_item=$_GET["filter"];

					$price_from=$_GET["price_from"];

					$price_to=$_GET["price_to"];

					$filter_color=$_GET["filter_color"];

					$filter_size=$_GET["filters_size"];

					$filter_discount=$_GET["filter_discount"]; @endphp

				@endif

				

		@if(count($color_filter_values)>0)

			

		<div class="color-area " >

		 <div class="sidebar-bar-title"><h3>{{ (Lang::has(Session::get('lang_file').'.COLOR_FILTER')!= '') ?  trans(Session::get('lang_file').'.COLOR_FILTER') : trans($OUR_LANGUAGE.'.COLOR_FILTER') }}</h3></div>

		 <br>

		  <div class="color filter-height">

		   <ul>

			@foreach($color_filter_values as $colrs)

				<div class="">

          <input type="checkbox" id="checkbox{{ $colrs->co_id }}" onclick="javascript:make_filter()" name="color" value="<?php echo $colrs->co_id; ?>" <?php if(in_array($colrs->co_id,$filters_color)){echo "checked";} ?>> 

          <label for="checkbox{{ $colrs->co_id }}"></label>

          <span class="color-box" style="background:{{ $colrs->co_code }}; margin-left: 15px; margin-top: 3px;"></span> <span style="margin-left: 10px; vertical-align: top;">{{ $colrs->co_name }}</span>

        </div>						

			@endforeach

		  </ul>

		</div>

		@if(isset($_GET["filter_color"]) AND $_GET["filter_color"]!="")

			<button type="button" onclick="javascript:clear_filter('filter_items_by_color_values')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

		@endif

		@endif

		</div>

		

		@if(count($size_filter_values)>0) 

		 <div class="size-area ">

			<div class="sidebar-bar-title"><h3>{{ (Lang::has(Session::get('lang_file').'.SIZE_FILTER')!= '') ?  trans(Session::get('lang_file').'.SIZE_FILTER') : trans($OUR_LANGUAGE.'.SIZE_FILTER') }}</h3></div><br>

			 <div class="size filter-height">

			  <ul>

				@foreach($size_filter_values as $sizes)

        <div class="">

          <input type="checkbox" id="checkbox-size{{ $sizes->si_id }}" onclick="javascript:make_filter()" name="size" value="<?php echo $sizes->si_id; ?>" <?php if(in_array($sizes->si_id,$size_filter)){echo "checked";} ?>>

          <span style="margin-left: 10px; vertical-align: top;">{{ $sizes->si_name }}</span>

				</div>				

				@endforeach

			</ul>

			</div>

			@if(isset($_GET["filters_size"]) AND $_GET["filters_size"]!="")

				<button type="button" onclick="javascript:clear_filter('filter_items_by_size_values')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

			@endif

		</div>

		@endif		

		<br>

	

				

		<div class="block product-price-range">

		  <div class="sidebar-bar-title">

              <h3>{{ (Lang::has(Session::get('lang_file').'.DISCOUNT')!= '') ?  trans(Session::get('lang_file').'.DISCOUNT') : trans($OUR_LANGUAGE.'.DISCOUNT') }}</h3>

          </div><br>

			@php $label=""; @endphp

				@for($i=1;$i<=6;$i++)

					@if($i==1)

					@php $label=((Lang::has(Session::get('lang_file').'.UPTO')!= '') ? trans(Session::get('lang_file').'.UPTO') : trans($OUR_LANGUAGE.'.UPTO')). " 10%"; @endphp @endif

					@if($i==2)

					@php $label="10% - 20%"; @endphp @endif

					@if($i==3)

					@php $label="20% - 30%"; @endphp @endif

					@if($i==4)

					@php $label="30% - 40%"; @endphp @endif

					@if($i==5)

					@php $label="40% - 50%"; @endphp @endif

					@if($i==6)


					@php 
					$label= "50% ".((Lang::has(Session::get('lang_file').'.AND_ABOVE')!= '') ? trans(Session::get('lang_file').'.AND_ABOVE') : trans($OUR_LANGUAGE.'.AND_ABOVE'));  @endphp @endif

					 <ul class=""> 

						<li>			

							<input type="checkbox" id="discount_price" name="discount_filter" value="{{ $i }}" <?php if(in_array($i,$discount_filter)){echo "checked";} ?> onclick="javascript:make_filter()"/> 

							<label for="discount"> <span class="button"></span>{{ $label }}</label>	

						</li>

					  </ul>

				@endfor

				

			@if(isset($_GET["filter_discount"]) AND $_GET["filter_discount"]!="")

				<button type="button" onclick="javascript:clear_filter('filter_for_values_discount')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

			@endif

		</div>

	

</div>	



		

		 <div class="block product-price-range ">

		   <div class="sidebar-bar-title">

              <h3>{{ (Lang::has(Session::get('lang_file').'.PRICE_FILTER')!= '') ?  trans(Session::get('lang_file').'.PRICE_FILTER') : trans($OUR_LANGUAGE.'.PRICE_FILTER') }}</h3>

           </div>

			 <div class="block-content">

			   <div class="slider-range">

				<form name="filter_form_to_generate" id="filter_form" method="get">

					{{ Form::hidden('filter',$filtered_item,array('id'=>'filter_for_values'))}}

					{{ Form::hidden('filter_discount',$filter_discount,array('id'=>'filter_for_values_discount'))}}

					<!-- <input type="hidden" name="filter" value="{{ $filtered_item }}" id="filter_for_values"> -->

					<!-- <input type="hidden" name="filter_discount" value="<?php echo $filter_discount; ?>" id="filter_for_values_discount"> -->

					<input type="hidden" name="filter_color" value="<?php if(isset($_GET["filter_color"])){echo $_GET["filter_color"];} ?>" id="filter_items_by_color_values">

					<input type="hidden" name="filters_size" value="<?php if(isset($_GET["filters_size"])){echo $_GET["filters_size"];} ?>" id="filter_items_by_size_values">

					

					<ul>

					 <li class="pri-filter-input">

             <div class="pri-filter-sec">

              <span style="color:#e83f33;">{{ (Lang::has(Session::get('lang_file').'.MIN')!= '') ? trans(Session::get('lang_file').'.MIN') : trans($OUR_LANGUAGE.'.MIN') }}</span><input type="text"  placeholder="100" name="price_from" id="price_from" value="<?php echo $price_from; ?>" style="width:75px"/>

            </div>

             <div class="pri-filter-sec">

					<span style="color:#e83f33;">{{ (Lang::has(Session::get('lang_file').'.MAX')!= '') ? trans(Session::get('lang_file').'.MAX') : trans($OUR_LANGUAGE.'.MAX') }}</span><input type="text" placeholder="10000" name="price_to" id="price_to" value="{{ $price_to }}" style="width:75px"/> </div>

           <div class="pri-filter-sec">

					<button type="button" onclick="javacript:make_filter();" class="button button-compare"> <span>{{ (Lang::has(Session::get('lang_file').'.GO')!= '') ? trans(Session::get('lang_file').'.GO') : trans($OUR_LANGUAGE.'.GO') }}</span></button></div>

					 </li>

					</ul>

					<br>

					@if($price_from !="" && $price_to !="") <!-- //Clear all for discount price -->

					    <button type="button" onclick="javascript:clear_discount_filter('price_from','price_to')" class="button button-clear"> <span>@if (Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= '') {{  trans(Session::get('lang_file').'.CLEAR_ALL') }} @else {{ trans($OUR_LANGUAGE.'.CLEAR_ALL') }} @endif</span></button>

					@endif

					<!--<input type="hidden" name="from" value="<?php // echo Request::segment(3); ?>">-->

				</form>

			  </div>

			</div>

		</div>

		@endif	

	



	<!--end side bar-->

<br><br>

		   @if(count($most_visited_product)>0)  

          <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3>@if (Lang::has(Session::get('lang_file').'.MOST_VISITED_PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.MOST_VISITED_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.MOST_VISITED_PRODUCTS') }} @endif</h3>

            </div>

            <div class="block-content">

              <ul>

			   @foreach($most_visited_product as $fetch_most_visit_pro) 

				@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $title = 'pro_title'; @endphp

					@else @php  $title = 'pro_title_langCode'; @endphp @endif



					 @php $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

					 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

					 $mostproduct_img 	= explode('/**/', $fetch_most_visit_pro->pro_Img);



					 $product_image 	= $mostproduct_img[0];

					 

					$prod_path 	= url('').'/public/assets/default_image/No_image_product.png';

					$img_data 	= "public/assets/product/".$product_image; @endphp

					

					@if(file_exists($img_data) && $product_image!='' )	

					 

					

					@php $prod_path = url('').'/public/assets/product/' .$product_image;	@endphp				

					@else	

						@if(isset($DynamicNoImage['productImg'])) 

						

					@php $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

							@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)) 

							@php	$prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

						@endif

					@endif	

					

				  @php $alt_text = substr($fetch_most_visit_pro->$title,0,25);

					$alt_text  .= strlen($fetch_most_visit_pro->$title)>25?'..':''; @endphp





			  @if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty) 

                <li class="item">

                  <div class="products-block-left">

				  <img  src="{{ $prod_path }}" alt="{{ $alt_text }}"/ style=""></div>

                  <div class="products-block-right">

                    <p class="product-name">{{substr($fetch_most_visit_pro->$title,0,25) }}

					{{  strlen($fetch_most_visit_pro->$title)>25?'..':'' }}</p>

                    <span class="price">{{ Helper::cur_sym() }} {{ $fetch_most_visit_pro->pro_disprice }}</span>

                   



				@php					  

				  $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 5)->count();

				  

				  

				  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

				  $multiple_countone = $one_count *1;

				  $multiple_counttwo = $two_count *2;

				  $multiple_countthree = $three_count *3;

				  $multiple_countfour = $four_count *4;

				  $multiple_countfive = $five_count *5;

				  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp



				   <div class="rating">

					@if($product_count)

					 @php   $product_divide_count = $product_total_count / $product_count; @endphp

					 @if($product_divide_count <= '1') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '1') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '2') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

					 @elseif($product_divide_count >= '3') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '4') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 @elseif($product_divide_count >= '5') 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

					 @else

						

					@endif

				@else

				     <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

				@endif	

			  </div>

				<br>

					@if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty) 

                      <h4 style="text-align:center;"><a  class="btn btn-danger">@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{  trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</a> 

                       @else   

						@php  $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name)); 

						 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

						 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

						 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

						 $res = base64_encode($fetch_most_visit_pro->pro_id); @endphp

					 </h4>

					 

					@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

					@endif

					

					 @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				    @endif

				   

				   @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')  

					 <a class="link-all" href="{{ url('productview/'.$mcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

				   @endif

					

				  @endif

                  </div>

				  

                </li>

			@endif @endforeach  

              </ul>

          </div>

        @endif

        </aside>

      </div>

	  

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   @include('service_section')

  

  

  

  <!--popup model quick view-->

 

			@if(count($product_details_quickview) != 0)  

				@foreach($product_details_quickview as $product_det) 

				

				@php $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

				$smcat 	= strtolower(str_replace(' ','-',$product_det->smc_name));

				$sbcat 	= strtolower(str_replace(' ','-',$product_det->sb_name));

				$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 

				$res = base64_encode($product_det->pro_id);

				$product_image 	= explode('/**/',$product_det->pro_Img);

				$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

				$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); @endphp

				@if($product_det->pro_no_of_purchase < $product_det->pro_qty)   

					

					@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

					@php  $title = 'pro_title'; @endphp

					@else @php  $title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

					

					@php   $product_img= explode('/**/',trim($product_det->pro_Img,"/**/")); 

					$img_count = count($product_img); @endphp



					@php $product_image 	= $product_image[0];

					 

					$prod_path 	= url('').'/public/assets/default_image/No_image_product.png';

					$img_data 	= "public/assets/product/".$product_image; @endphp

					

					@if(file_exists($img_data) && $product_image !='')	

					

					

					@php $prod_path = url('').'/public/assets/product/'.$product_image;	@endphp				

					@else	

			

						@if(isset($DynamicNoImage['productImg']))  

						  

						@php $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

							@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

							@php $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

						@endif

					@endif	

					

				 @php $alt_text 	= substr($product_det->$title,0,25);

					$alt_text  .= strlen($product_det->$title)>25?'..':''; @endphp

					

				  @php $count = $product_det->pro_qty - $product_det->pro_no_of_purchase; @endphp

				  

				 <input type="hidden" id="pro_qty_hidden_{{ $product_det->pro_id }}" name="pro_qty_hidden" value="<?php echo  $product_det->pro_qty; ?>" />

				 <input type="hidden" id="pro_purchase_hidden_{{ $product_det->pro_id }}" name="pro_purchase_hidden" value="<?php echo  $product_det->pro_no_of_purchase; ?>" />

				 

			<!--popup quick view-->

			<div style="display:none;" class="quick_view_popup-wrap" id="quick_view_popup-wrap{{ $product_det->pro_id }}">

				<div id="quick_view_popup-overlay"></div>

				  <div id="quick_view_popup-outer">

					<div id="quick_view_popup-content">

					  <div style="width:auto;height:auto;overflow: auto;position:relative;">

						<div class="product-view-area">

						  <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">

							<div class="large-image">

							<a href="{{$prod_path}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="{{$prod_path}}"> </a>

							</div>

							

						

							<div class="flexslider flexslider-thumb">

							  <ul class="previews-list slides">

							  

							@for($i=0;$i <$img_count;$i++)

							@php  $product_image     = $product_img[$i];

						

							$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

							$img_data   = "public/assets/product/".$product_image; @endphp

						

						@if(file_exists($img_data) && $product_image !='') <!-- //product image is not null and exists in folder -->



						   @php $prod_path = url('').'/public/assets/product/' .$product_image;  @endphp                 

							@else  

								@if(isset($DynamicNoImage['productImg']))

								 @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

									@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)) <!-- //no image for product is not null and exists in folder -->

														

									  @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp

									@endif

								@endif

							@endif 

		

								<li style="width: 95px; float: left; display: block;"><a href='{{$prod_path}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$prod_path}}' "><img src="{{$prod_path}}" alt = "Thumbnail 2"/></a></li>

						

						 @endfor

							  </ul>

							</div>

							

							<!-- end: more-images --> 

							

						  </div>

						  <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">

							<div class="product-details-area">

							  <div class="product-name">

								<h1>{{ substr($product_det->$title,0,25) }}

								{{  strlen($product_det->$title)>25?'..':'' }}</h1>

							  </div>

							  <div class="price-box">

								<p class="special-price"> <span class="price-label"></span> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }} </span> </p>

								<p class="old-price"> <span class="price-label"></span> <span class="price"> {{ Helper::cur_sym() }} {{ $product_det->pro_price }} </span> </p>

							  </div>

							  <div class="ratings">

								<div class="rating"> 

								

								 {{-- product rating--}}

								  @php

								  					  

				  $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();



								  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

										

								  $multiple_countone   = $one_count *1;

								  $multiple_counttwo   = $two_count *2;

								  $multiple_countthree = $three_count *3;

								  $multiple_countfour  = $four_count *4;

								  $multiple_countfive  = $five_count *5;

								  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

								  

								  @if($product_count)

									@php  $product_divide_count = $product_total_count / $product_count;

									$product_divide_count = round($product_divide_count); @endphp

									

								 @if($product_divide_count <= '1')

		  

								<i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

								@elseif($product_divide_count >= '1') 



								<i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

								

								@elseif($product_divide_count >= '2')

								 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

								 

								@elseif($product_divide_count >= '3') 

								 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 

								 

								@elseif($product_divide_count >= '4') 

							 

								 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>

							    @elseif($product_divide_count >= '5') 

								 

								 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>

							   @else

									

								@endif

							 @else

							   <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 @endif	

						

								</div>

								

								<p class="availability in-stock pull-right"> @if (Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= '') {{ trans(Session::get('lang_file').'.AVAILABLE_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE_STOCK') }} @endif: <span>{{ $product_det->pro_qty-$product_det->pro_no_of_purchase }}  @if (Lang::has(Session::get('lang_file').'.IN_STOCK')!= '') {{  trans(Session::get('lang_file').'.IN_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.IN_STOCK') }} @endif</span></p>

							  </div>

							  

							  <div class="short-description">

								<h2>@if (Lang::has(Session::get('lang_file').'.OVERVIEW')!= '') {{ trans(Session::get('lang_file').'.OVERVIEW') }} @else {{ trans($OUR_LANGUAGE.'.OVERVIEW') }} @endif</h2>

								

								 {{-- Product description --}}

								@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

								  @php  $pro_desc = 'pro_desc'; @endphp

								@else

								  @php  

								  $pro_desc = 'pro_desc_'.Session::get('lang_code'); @endphp 

								@endif

														

								<p>@if($product_det->$pro_desc != '')

								  {!! html_entity_decode(substr($product_det->$pro_desc,0,200)) !!}@endif </p>

							  </div>

							  

							@php  $product_color_detail = DB::table('nm_procolor')->where('pc_pro_id', '=',     $product_det->pro_id)->LeftJoin('nm_color', 'nm_color.co_id', '=', 'nm_procolor.pc_co_id')->get();

							  

							 $product_size_detail = DB::table('nm_prosize')->where('ps_pro_id', '=', $product_det->pro_id)->LeftJoin('nm_size', 'nm_size.si_id', '=', 'nm_prosize.ps_si_id')->get(); @endphp

							  

					  <div class="product-color-size-area">

							@if(count($product_color_detail)>0)

							<div class="color-area">

							  <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.COLOR')!= '') {{ trans(Session::get('lang_file').'.COLOR') }} @else {{ trans($OUR_LANGUAGE.'.COLOR') }} @endif </h2>

							   

							  <div class="color">

								<select name="addtocart_color" id="addtocart_color_{{ $product_det->pro_id }}" required>

							  <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') {{ trans(Session::get('lang_file').'.SELECT_COLOR') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_COLOR') }} @endif--</option>

						  @foreach($product_color_detail as $product_color_det) 

							  <option value="{{ $product_color_det->co_id }}">

							  {{ $product_color_det->co_name }}

							  </option>

						  @endforeach 

							</select>

							  </div>

							  

							</div>

							@endif



							 @if(count($product_size_detail) > 0)

							 @php  $size_name = $product_size_detail[0]->si_name;

						  $return  = strcmp($size_name,'no size');  @endphp

						@if($return!=0) 

							<div class="size-area">

							  <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.SIZE')!= '') {{ trans(Session::get('lang_file').'.SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SIZE') }} @endif</h2>

							  <div class="size">

							   <select name="addtocart_size" id="addtocart_size_{{ $product_det->pro_id }}" required>

								<option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') {{ trans(Session::get('lang_file').'.SELECT_SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_SIZE') }} @endif--</option>

							   

							  @foreach($product_size_detail as $product_size_det) 

							  <option value="{{ $product_size_det->ps_si_id }}">

							  {{ $product_size_det->si_name }}

							  </option>

						  @endforeach

							 </select>

							</div>

                  

          

						</div>

						 @else 

						 <input type="hidden" name="addtocart_size" value="{{ $product_size_detail[0]->ps_si_id }}">

						  @endif

						@endif

					</div>

			  

			  

							  <div class="product-variation">

							  {!! Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data','id'=>'submit_form')) !!}

								<form action="#" method="post">

								  <div class="cart-plus-minus">

									<label for="qty">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.QUANTITY') }} @endif :</label>

									<div class="numbers-row">

									  <div onClick="remove_quantity(<?php echo $product_det->pro_id; ?>)" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>

									  <input type="number" class="qty" min="1" value="1" max="{{  ($product_det->pro_qty - $product_det->pro_no_of_purchase) }}" id="addtocart_qty_{{ $product_det->pro_id }}" name="addtocart_qty" readonly required >

									  

									  <div onClick="add_quantity(<?php echo $product_det->pro_id; ?>)" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>

									</div>

								  </div>

								  

								   {{-- Add to cart--}}

								  

								   <input type="hidden" name="_token" value="{!! csrf_token() !!}" />

									 {{ Form::hidden('addtocart_type','product')}}

									 {{ Form::hidden('addtocart_pro_id',$product_det->pro_id)}}

									 

									  <input type="hidden" name="return_url" value="<?php echo $product_det->mc_name.'/'.base64_encode(base64_encode(base64_encode($product_det->pro_mc_id))); ?>" />

									  

								@if(Session::has('customerid')) 

									@if($count > 0)

									  <button onclick="addtocart_validate('<?php echo $product_det->pro_id; ?>');" class="button pro-add-to-cart" title="Add to Cart" type="button" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button>

									  @else 

								  <button type="button" class="btn btn-danger">

									@if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif

								  </button> 

								   @endif 

						     @else 

								  @if($count > 0)

								  <a href="" role="button" data-toggle="modal" data-target="#loginpop">

									  <button type="button" class=" button pro-add-to-cart">

										<span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 

										@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span>

									  </button> 

								  </a>

								  @else

									<button type="button" class="btn btn-danger">

									   @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif

									</button> 

								@endif 

                         @endif

                      {{ Form::close() }}

									  

								

							  </div>

							  

							  

							 <div class="product-cart-option">

								<ul>

								  <li>

								{{-- Add to wishlist--}}

								

								@if(Session::has('customerid'))

								@php  

								$cus_id = Session::get('customerid');

								$prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',$cus_id)->first(); @endphp

							    @else

								@php  $prodInWishlist = array(); @endphp

								@endif

					   

								@if($count > 0)  

						        @if(Session::has('customerid'))

								@if(count($prodInWishlist)==0)

								  <input type="hidden" name="_token" value="{!! csrf_token() !!}" />

										{{ Form::hidden('pro_id','$product_det->pro_id') }}        

								  <!-- <input type="hidden" name="pro_id" value="<?php echo $product_det->pro_id; ?>"> -->

								  <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">

								  <a href="" onclick="addtowish({{ $product_det->pro_id }},{{ Session::get('customerid') }})">

								  <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">

									<i class="fa fa-heart-o" aria-hidden="true"></i>@if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif 

								  </a>

											@else

											<?php /* remove wishlist */?>   

												

											<a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}">

											<i class="fa fa-heart" aria-hidden="true"></i>    @if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST') }} @endif

											</a> 

											<?php /*remove link:remove_wish_product/wishlist table_id*/ ?>

												

								@endif  

						   @else 

							  <a href="" role="button" data-toggle="modal" data-target="#loginpop">

							  

								<i class="fa fa-heart-o" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif

							  

							  </a>

						  @endif

							@endif

									</li>

								  {{-- <li><a href="#"><i class="fa fa-link"></i><span>EMAIL_TO_FRIEND</span></a></li>

								  <li><a href="#"><i class="fa fa-envelope"></i><span>{{ (Lang::has(Session::get('lang_file').'.EMAIL_TO_FRIEND')!= '') ? trans(Session::get('lang_file').'.EMAIL_TO_FRIEND') : trans($OUR_LANGUAGE.'.EMAIL_TO_FRIEND') }}</span></a></li> --}}

								</ul>

						</div>

							  

							</div>

						  </div>

						</div>

						<!--product-view--> 

						

					  </div>

					</div>

					<a style="display: inline;" id="quick_view_popup-close" href="{{ url('')}}/products"><i class="icon pe-7s-close"></i></a> </div>

			</div>

				

			@endif

			

				@endforeach


				@endif
			</div>

  {!! $footer !!}

  <!--End popup model quick view-->

 

  



<!-- <script type="text/javascript" src="js/jquery.flexslider.js"></script> --> 

<!--  <script type="text/javascript" src="public/themes/js/jquery.min.js"></script>   -->





<script type="text/javascript">



function add_to_cart_session(id){ alert("sds");

  /* var pro_purchase1 = $("#pro_purchase_hidden_"+id).val();

  var pro_purchase = parseInt($('#addtocart_qty_'+id).val()) + parseInt(pro_purchase1);

  var pro_qty = $("#addtocart_qty_"+id).val();

  if(pro_purchase > parseInt(pro_qty))

  {

    $('#addtocart_qty').focus();

    $('#addtocart_qty').css('border-color', 'red');

    $('#addtocart_qty_error').html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');

    return false;

  }

  else

  {

    $('#addtocart_qty').css('border-color', '');

    $('#addtocart_qty_error').html('');

  }

  if($('#addtocart_color').val() ==0) 

  {

    $('#addtocart_color').focus();

    $('#addtocart_color').css('border-color', 'red');

    $('#size_color_error').html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');

    return false;

  }

  else

  {

    $('#addtocart_color').css('border-color', '');

    $('#size_color_error').html('');

  }

  if($('#addtocart_size').val() ==0)

  {

    $('#addtocart_size').focus();

    $('#addtocart_size').css('border-color', 'red');

    $('#size_color_error').html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');

    return false;

  }

  else

  {

    $('#addtocart_size').css('border-color', '');

    $('#size_color_error').html('');

  } */

 

}



</script>





<script>

function filterpage(){

var pagelimit = $('#pagelimit').val();



		  $.ajax( {

			      type: 'get',

				  data: '&pagelimit=' +pagelimit,	//by mistake variable name is subcategory,but actully compare is based on main categroy

				  url: "<?php echo url('product_filter_ajax'); ?>",

				  success: function(responseText){  

				   if(responseText)

				   {  

					  alert(responseText);

					  location.reload();

					 				   

				   }

				}		

			});

 

}

</script>



<!-- Image loads after page loads -->

<script>

	 $(function(){

    $.each(document.images, function(){

$(this).attr("src", $(this).data("src"));

    	   });

  });

    

</script>



<script>

function comparefunc(pid,value,maincategory_id){



	var pid = pid;

		  $.ajax( {

			      type: 'get',

				  data: 'pid='+pid + '&value=' +value+'&subcategory_id='+maincategory_id,	//by mistake variable name is subcategory,but actully compare is based on main categroy

				  url: "<?php echo url('product_compare_ajax'); ?>",

				  success: function(responseText){  

				   if(responseText)

				   {  
				   	if(responseText == 1)
				   	{
                         alert("{{ (Lang::has(Session::get('lang_file').'.BACK_THIS_PRODUCT_ALREADY_EXISTS_IN_COMPARE')!= '') ? trans(Session::get('lang_file').'.BACK_THIS_PRODUCT_ALREADY_EXISTS_IN_COMPARE') : trans($OUR_LANGUAGE.'.BACK_THIS_PRODUCT_ALREADY_EXISTS_IN_COMPARE') }}");
                          location.reload();
				   	}
				   	else if(responseText ==2)
				   	{
                         alert("{{ (Lang::has(Session::get('lang_file').'.BACK_PRODUCT_ADDED_TO_COMPARE')!= '') ? trans(Session::get('lang_file').'.BACK_PRODUCT_ADDED_TO_COMPARE') : trans($OUR_LANGUAGE.'.BACK_PRODUCT_ADDED_TO_COMPARE') }}");
                          location.reload();
				   	}
				   	else if(responseText == 3)
				   	{
                          alert("{{ (Lang::has(Session::get('lang_file').'.BACK_YOU_CAN_ONLY_COMPARE_SIMILAR_PRODUCTS_SO_CLEAR_LIST_TO_COMPARE')!= '') ? trans(Session::get('lang_file').'.BACK_YOU_CAN_ONLY_COMPARE_SIMILAR_PRODUCTS_SO_CLEAR_LIST_TO_COMPARE') : trans($OUR_LANGUAGE.'.BACK_YOU_CAN_ONLY_COMPARE_SIMILAR_PRODUCTS_SO_CLEAR_LIST_TO_COMPARE') }}");
                           location.reload();
				   	}

					else
					{

					}

					 

					 				   

				   }

				}		

			});

 

}

</script>



<script type="text/javascript">

	$(document).ready(function(){



		$(document).on("click", ".customCategories .topfirst b", function(){

			$(this).next("ul").css("position", "relative");

			

			$(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

			 $(this).next("ul").toggle();

		});



		$(document).on("click", ".morePage", function(){

			$(".nextPage").slideToggle(200);

		});

		

		$(document).on("click", "#smallScreen", function(){

			$(this).toggleClass("customMenu");

		});

		$('#comp_myprod').show();

		/*$(window).scroll(function () {

			if ($(this).scrollTop() > 10) {

				

			}

			else{

				$('#comp_myprod').hide();

			} 

		});*/

});

	</script>

	<script type="text/javascript">

		var price_from = $('#price_from');

		var price_to = $('#price_to');

		

		 $('#price_from').keypress(function (e){

		 	

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            alert("Numbers only allowed");

			return false;

        }

      });



	  $('#price_to').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            alert("Numbers only allowed");

			return false;

        }

      });

	  

	

</script>



<script type="text/javascript">

function make_filter()

{ //alert();

	var filter_by_items=[];

	var filter_items_by_color=[];

	var filter_items_by_discount=[];

	var filter_items_by_size=[];

	var price_from=$("#price_from").val();

	var price_to=$("#price_to").val();

	var price_from=parseInt(price_from);

	var price_to=parseInt(price_to);

	$("input:checkbox[name=filter_by]:checked").each(function(){

		filter_by_items.push($(this).val());

	});

	var price_radio=$("input[name='radio_price_filter']:checked").val();

	

	if(price_radio)

	{

		var min_max_price=price_radio.split('-');

		$("#price_from").val(min_max_price[0]);

		$("#price_to").val(min_max_price[1]);

	}

	$("input:checkbox[name=discount_filter]:checked").each(function(){  

		filter_items_by_discount.push($(this).val());

	});

	

	$("input:checkbox[name=color]:checked").each(function(){

		filter_items_by_color.push($(this).val());

	});

	$("input:checkbox[name=size]:checked").each(function(){ 

		filter_items_by_size.push($(this).val());

	});

	var enc_filter_by_items = window.btoa(filter_by_items);

	var enc_filter_by_discount = window.btoa(filter_items_by_discount); 

	var enc_filter_items_by_color = window.btoa(filter_items_by_color); 

	var enc_filter_items_by_size = window.btoa(filter_items_by_size);

	$("#filter_for_values").val(enc_filter_by_items);

	$("#filter_for_values_discount").val(enc_filter_by_discount);

	$("#filter_items_by_color_values").val(enc_filter_items_by_color);

	$("#filter_items_by_size_values").val(enc_filter_items_by_size);

	if(price_from > price_to)

	{

		alert("Maximum price should less then Minimum Price!");

		$("#price_to").focus();

		return false;

	}

	

   document.filter_form_to_generate.submit();             // Submit the page

    

}

function clear_filter(id_to_clear)

{ //alert();

	$("#"+id_to_clear).val("");

	document.filter_form_to_generate.submit();

}

/*clear filter for discount price */

function clear_discount_filter(from_price,to_price)

{

	$("#"+from_price).val("");

	$("#"+to_price).val("");

	document.filter_form_to_generate.submit();

}

</script>



<script type="text/javascript"> 

     function displayproductrecords(numRecords,pageNum,filter)

     {  

        var path = '<?php echo url('product_ajax_pagination'); ?>';

        $.ajax({

                type: "GET",

                url: path,

                data: "show=" + numRecords + "&pagenum=" + pageNum + "&filter=" + filter,

                cache: false,

                datatype: "html",

                success: function(result) {

                    $("#prdt_ajax_display").html(result);

                }

        });

       

    }

</script>





<script>

function add_quantity(id)

{

  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/

  var quantity=$("#addtocart_qty_"+id).val(); 

  var pro_qty_hidden=$("#pro_qty_hidden_"+id).val();

  var pro_purchase_hidden=$("#pro_purchase_hidden_"+id).val();

  var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);

 

  

  if(quantity<remaining_product)

  { 

    var new_quantity=parseInt(quantity)+1;

    $("#addtocart_qty_"+id).val(new_quantity);

  }

  //alert();

}



function remove_quantity(id)

{

  //alert();

  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/



  var quantity=$("#addtocart_qty_"+id).val();

  var quantity=parseInt(quantity);

  if(quantity>1)

  {

    var new_quantity=quantity-1;

    $("#addtocart_qty_"+id).val(new_quantity);

  }

  //alert();

}

</script>



<script type="text/javascript">

  function addtowish(pro_id,cus_id){

    //alert();

    var wishlisturl = document.getElementById('wishlisturl').value;



    $.ajax({

          type: "get",   

          url:"<?php echo url('addtowish'); ?>",

          data:{'pro_id':pro_id,'cus_id':cus_id},

            success:function(response){

            //alert(response); return false;

            if(response==0){

             alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');

                       /* $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');*/

              //window.location=wishlisturl;

                            window.location.reload();

                            

            }else{

              alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');

              //window.location=wishlisturl;

            }

            

            

          }

        });

  }

</script>



<script type="text/javascript">

  function addtocart_validate(id){

	  

  var pro_qty=$("#pro_qty_hidden_"+id).val();

  var pro_purchase1=$("#pro_purchase_hidden_"+id).val();

  var pro_purchase = parseInt($('#addtocart_qty_offer_').val()) + parseInt(pro_purchase1);

  var error1 = 0;

   if(pro_purchase > parseInt(pro_qty))

  {

    $('#addtocart_qty_'+id).focus();

    $('#addtocart_qty_'+id).css('border-color', 'red');

    $('#addtocart_qty_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');

    return false;

  }

  else

  {

    $('#addtocart_qty_'+id).css('border-color', '');

    $('#addtocart_qty_error_'+id).html('');

  }

  if($('#addtocart_color_'+id).val() ==0) 

  {

    $('#addtocart_color_'+id).focus();

    $('#addtocart_color_'+id).css('border-color', 'red');

    $('#size_color_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');

    return false;

  }

  else

  {

    $('#addtocart_color_'+id).css('border-color', '');

    $('#size_color_error_'+id).html('');

  }

  if($('#addtocart_size_'+id).val() ==0)

  {

    $('#addtocart_size_'+id).focus();

    $('#addtocart_size_'+id).css('border-color', 'red');

    $('#size_color_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');

    return false;

  }

  else

  {

    $('#addtocart_size_'+id).css('border-color', '');

    $('#size_color_error_'+id).html('');

  }

 

  if(error1 <= 0){

	  $("#submit_form").submit(); 

  }

  

}  

</script>







</body>

</html>



	