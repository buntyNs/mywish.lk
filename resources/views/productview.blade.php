{!! $navbar !!}
<!-- Navbar ================================================== -->
{!! $header !!}
<!-- Header -->
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]--> 
<div id="page">
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <ul>
               <li class="home"> <a title="" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li>
               <li class=""> <a title="" href="{{ url('products')}}">{{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '') ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT') }}</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<!-- Breadcrumbs End --> 
<!-- Main Container -->
<center>
   @if (Session::has('success1'))
   <div class="alert alert-warning alert-dismissable">{!! Session::get('success1') !!}
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   </div>
   @endif
</center>
<div class="main-container col1-layout">
   <div class="container">
      <div class="row">
         <div class="col-main">
            <div class="product-view-area">
               @if(count($product_details_by_id) > 0)
               @foreach($product_details_by_id as $pro_details_by_id) @endforeach 
               {{-- Product title--}}
               @if((Session::get('lang_code'))=='' || (Session::get('lang_code'))== 'en')  
               @php    $pro_title = 'pro_title';
               $pro_mdesc = 'pro_mdesc';
               $pro_mkeywords = 'pro_mkeywords'; @endphp
               @else   
               @php   $pro_title = 'pro_title_'.Session::get('lang_code');
               $pro_mdesc = 'pro_mdesc_'.Session::get('lang_code');
               $pro_mkeywords = 'pro_mkeywords_'.Session::get('lang_code'); @endphp
               @endif 
               {{-- Product image--}}
               @php   $product_img= explode('/**/',trim($pro_details_by_id->pro_Img,"/**/")); 
               $img_count = count($product_img); @endphp
               @php   $product_image     = $product_img[0];
               $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
               $img_data   = "public/assets/product/".$product_image; @endphp
               @if($product_img !='' && $img_count !='')
               @if(file_exists($img_data) && $product_image !='')   <!-- //product image is not null and exists in folder -->
               @php  $prod_path = url('').'/public/assets/product/' .$product_image;   @endphp               
               @else  
               @if(isset($DynamicNoImage['productImg']))
               @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp
               @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))    <!-- //no image for product is not null and exists in folder -->
               @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
               @endif
               @endif
               @else  
               @if(isset($DynamicNoImage['productImg']))
               @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp
               @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))    <!-- //no image for product is not null and exists in folder -->
               @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
               @endif
               @endif 
               {{-- Alt text--}}
               @php    $alt_text   = substr($pro_details_by_id->$pro_title,0,25);
               $alt_text  .= strlen($pro_details_by_id->$pro_title)>25?'..':''; @endphp
               {{-- Product description --}}
               @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
               @php  $pro_desc = 'pro_desc'; @endphp
               @else
               @php  
               $pro_desc = 'pro_desc_'.Session::get('lang_code'); @endphp 
               @endif
               {{-- product rating--}}
               @php
               $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
               $multiple_countone   = $one_count *1;
               $multiple_counttwo   = $two_count *2;
               $multiple_countthree = $three_count *3;
               $multiple_countfour  = $four_count *4;
               $multiple_countfive  = $five_count *5;
               $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp
               {{-- delivery date--}}
               <?php $delivery_date = '+'.$pro_details_by_id->pro_delivery.'days'; ?>
               <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                  {{-- 
                  <div class="icon-sale-label sale-left">Sale</div>
                  --}}
                  <div class="large-image"> 
                     <a href="{{$prod_path}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> 
                     <img class="zoom-img" src="{{$prod_path}}" alt="products"> 
                     </a>
                  </div>
                  <div class="flexslider flexslider-thumb">
                     <ul class="previews-list slides">
                        @for($i=0;$i <$img_count;$i++)
                        @php  $product_image     = $product_img[$i];
                        $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_image; @endphp
                        @if(file_exists($img_data) && $product_image !='') <!-- //product image is not null and exists in folder -->
                        @php $prod_path = url('').'/public/assets/product/' .$product_image;  @endphp                 
                        @else  
                        @if(isset($DynamicNoImage['productImg']))
                        @php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp
                        @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)) <!-- //no image for product is not null and exists in folder -->
                        @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp
                        @endif
                        @endif
                        @endif 
                        <li style="width: 100px; float: left; display: block;">
                           <a href='{{$prod_path}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$prod_path}}' "><img src="{{$prod_path}}" alt = "Thumbnail 2"/></a>    
                        </li>
                        @endfor
                     </ul>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
                  <div class="product-name">
                     <h1>{{ $pro_details_by_id->$pro_title }}</h1>
                  </div>
                  <div class="price-box">
                     <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> {{ Helper::cur_sym() }} {{ $pro_details_by_id->pro_disprice }}</span> </p>
                     <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> {{ Helper::cur_sym() }} {{ $pro_details_by_id->pro_price }}</span> 
                        @if($pro_details_by_id->pro_discount_percentage!='') 
                        <span class="special-price">(<?php echo round($pro_details_by_id->pro_discount_percentage);?>% off)</span>
                        @endif
                     </p>
                  </div>
                  <div class="ratings">
                     <div class="rating"> 
                        @if($product_count)
                        @php  $product_divide_count = $product_total_count / $product_count;
                        $product_divide_count = round($product_divide_count); @endphp
                        @if($product_divide_count <= '1') 
                        <img src='{{ url("./images/stars-1.png") }}' style='margin-bottom:10px;'>
                        @elseif($product_divide_count >= '1') 
                        <img src='{{ url("./images/stars-1.png") }}' style='margin-bottom:10px;'> 
                        @elseif($product_divide_count >= '2')
                        <img src='{{ url("./images/stars-2.png") }}' style='margin-bottom:10px;'>  
                        @elseif($product_divide_count >= '3')
                        <img src='{{ url("./images/stars-3.png")}}' style='margin-bottom:10px;'> 
                        @elseif($product_divide_count >= '4') 
                        <img src='<?php echo url('./images/stars-4.png'); ?>' style='margin-bottom:10px;'> 
                        @elseif($product_divide_count >= '5')
                        <img src='<?php echo url('./images/stars-5.png'); ?>' style='margin-bottom:10px;'>
                        @else
                        @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT') }} @endif
                        @endif      
                        @elseif($product_count)
                        @php  $product_divide_count = $product_total_count / $product_count; @endphp
                        @else
                        @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT') }} @endif
                        @endif  
                     </div>
                     <p class="rating-links"> {{ $count_review_rating }} @if (Lang::has(Session::get('lang_file').'.REVIEWS_AND_RATINGS')!= '') {{  trans(Session::get('lang_file').'.REVIEWS_AND_RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.REVIEWS_AND_RATINGS') }} @endif  </p>
                     <p class="availability in-stock pull-right"> 
                        @if (Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= '') {{ trans(Session::get('lang_file').'.AVAILABLE_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE_STOCK') }} @endif: <span>
                        {{ $pro_details_by_id->pro_qty-$pro_details_by_id->pro_no_of_purchase }}
                        @if (Lang::has(Session::get('lang_file').'.IN_STOCK')!= '') {{  trans(Session::get('lang_file').'.IN_STOCK') }} @else {{ trans($OUR_LANGUAGE.'.IN_STOCK') }} @endif
                        </span>
                     </p>
                  </div>
                  <div class="short-description">
                     <h2>@if (Lang::has(Session::get('lang_file').'.OVERVIEW')!= '') {{ trans(Session::get('lang_file').'.OVERVIEW') }} @else {{ trans($OUR_LANGUAGE.'.OVERVIEW') }} @endif</h2>
                     <p>@if($pro_details_by_id->$pro_desc != '')
                        {!! html_entity_decode(substr($pro_details_by_id->$pro_desc,0,200)) !!}
                        @endif 
                     </p>
                  </div>
                  @if($GENERAL_SETTING->gs_store_status != 'Store')
                  <div class="store-name">
                     <h5>@if (Lang::has(Session::get('lang_file').'.STORE_NAME')!= '') {{ trans(Session::get('lang_file').'.STORE_NAME') }} @else {{ trans($OUR_LANGUAGE.'.STORE_NAME') }} @endif</h5>
                     <p>
                        @foreach($get_store as $storerow)
                        @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                        @php  $stor_name = 'stor_name';  @endphp
                        @else   
                        @php  $stor_name = 'stor_name_'.Session::get('lang_code'); @endphp 
                        @endif
                        {{ $storerow->$stor_name }}
                        @endforeach
                     </p>
                  </div>
                  @else
                  @endif
                  <div>
                     @foreach ($product_details_by_id as $row) @endforeach 
                     @if($row->cash_pack != 0) 
                     <b>@if (Lang::has(Session::get('lang_file').'.CASHBACK')!= '') {{ trans(Session::get('lang_file').'.CASHBACK') }} @else {{ trans($OUR_LANGUAGE.'.CASHBACK') }} @endif :</b>  
                     {{ Helper::cur_sym() }} {{ $row->cash_pack }} 
                     @elseif($row->cash_pack == "") 
                     @elseif($row->cash_pack == 0) @endif
                     @php $count = $pro_details_by_id->pro_qty - $pro_details_by_id->pro_no_of_purchase; @endphp
                     <p class="left-label">
                        @if(isset($coupon_details))
                        @php  $current_date = date('Y-m-d H:i');
                        $start_date = $coupon_details->start_date;
                        $end_date = $coupon_details->end_date;
                        $new_start_date = date("Y-m-d H:i", strtotime($start_date));
                        $new_end_date = date("Y-m-d H:i", strtotime($end_date)); @endphp
                        @if(($new_start_date <= $current_date) && ($new_end_date >= $current_date))
                        @php $total_of_product_count = $productview_check_coupon_purchase_count_cod + $productview_check_coupon_purchase_count_paypal; @endphp
                        @if(($productview_check_coupon_purchase_count_cod >= $coupon_details->coupon_per_product) || ($productview_check_coupon_purchase_count_paypal >= $coupon_details->coupon_per_product) || ($total_of_product_count >= $coupon_details->coupon_per_product)) 
                        @else
                        @if(Session::has('customerid'))
                        @php $total_coupon_of_user = $productview_check_coupon_purchase_count_cod_user + $productview_check_coupon_purchase_count_paypal_user; @endphp
                        @if(($productview_check_coupon_purchase_count_cod_user >= $coupon_details->coupon_per_user) || ($productview_check_coupon_purchase_count_paypal_user >= $coupon_details->coupon_per_user) || ($total_coupon_of_user >= $coupon_details->coupon_per_user) )
                        {{"Your Coupon Limit Exit" }}
                        @else 
                     <div style="padding-right:0;margin-top:5px;font-size:18px; color:#FF5722; font-weight: bold;">  
                        {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '') ? trans(Session::get('lang_file').'.COUPON_CODE') : trans($OUR_LANGUAGE.'.COUPON_CODE') }} :
                        &nbsp
                        {{ $coupon_details->coupon_code }}
                     </div>
                     @endif 
                     @else 
                     <a href="" role="button" data-toggle="modal" data-target="#loginpop" style="padding-right:0; margin-top:5px;"><button class="wish-but" id="login_a_click"  value="{{ (Lang::has(Session::get('lang_file').'.LOGIN')!= '') ? trans(Session::get('lang_file').'.LOGIN') : trans($OUR_LANGUAGE.'.LOGIN') }}"> {{ (Lang::has(Session::get('lang_file').'.CLICK_TO_VIEW_COUPON_CODE')!= '') ?  trans(Session::get('lang_file').'.CLICK_TO_VIEW_COUPON_CODE') : trans($OUR_LANGUAGE.'.CLICK_TO_VIEW_COUPON_CODE') }} </button></a>
                     @endif 
                     @endif <!-- //else -->
                     @endif
                     @endif      
                  </div>
                  {!! Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}
                  @if($count > 0)
                  <div class="product-color-size-area">
                     @if(count($product_color_details)>0)
                     <div class="color-area">
                        <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.COLOR')!= '') {{ trans(Session::get('lang_file').'.COLOR') }} @else {{ trans($OUR_LANGUAGE.'.COLOR') }} @endif </h2>
                        <div class="color">
                           <select name="addtocart_color" id="addtocart_color" required>
                              <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') {{ trans(Session::get('lang_file').'.SELECT_COLOR') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_COLOR') }} @endif--</option>
                              @foreach($product_color_details as $product_color_det) 
                              <option value="{{ $product_color_det->co_id }}">
                                 {{ $product_color_det->co_name }}
                              </option>
                              @endforeach 
                           </select>
                        </div>
                     </div>
                     @endif
                     @if(count($product_size_details) > 0)
                     @php  $size_name = $product_size_details[0]->si_name;
                     $return  = strcmp($size_name,'no size');  @endphp
                     @if($return!=0) 
                     <div class="size-area">
                        <h2 class="saider-bar-title">@if (Lang::has(Session::get('lang_file').'.SIZE')!= '') {{ trans(Session::get('lang_file').'.SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SIZE') }} @endif</h2>
                        <div class="size">
                           <select name="addtocart_size" id="addtocart_size" required>
                              <option value="">--@if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') {{ trans(Session::get('lang_file').'.SELECT_SIZE') }} @else {{ trans($OUR_LANGUAGE.'.SELECT_SIZE') }} @endif--</option>
                              @foreach($product_size_details as $product_size_det) 
                              <option value="{{ $product_size_det->ps_si_id }}">
                                 {{ $product_size_det->si_name }}
                              </option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     @else 
                     <input type="hidden" name="addtocart_size" value="{{ $product_size_details[0]->ps_si_id }}">
                     @endif
                     @endif
                  </div>
                  @endif
                  <div class="product-variation">
                     {!! Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}
                     @if($count > 0)
                     <div class="cart-plus-minus">
                        <label for="qty">@if (Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY') }} @else {{ trans($OUR_LANGUAGE.'.QUANTITY') }} @endif :</label>
                        <div class="numbers-row">
                           <div onClick="remove_quantity()" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                           <input type="number" class="qty" min="1" value="1" max="{{  ($pro_details_by_id->pro_qty - $pro_details_by_id->pro_no_of_purchase) }}" id="addtocart_qty" name="addtocart_qty" readonly required >
                           <div onClick="add_quantity()" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                        </div>
                     </div>
                     @endif
                     {{-- Add to cart--}}
                     <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                     {{ Form::hidden('addtocart_type','product')}}
                     {{ Form::hidden('addtocart_pro_id',$pro_details_by_id->pro_id)}}
                     @if(count($product_size_details) > 0)
                     <!-- <input type="hidden" name="addtocart_size" value="{{ $product_size_details[0]->ps_si_id }}">-->
                     @endif
                     @if(count($product_color_details) > 0)
                     <!-- <input type="hidden" name="addtocart_color" value="{{ $product_color_details[0]->co_id }}">-->
                     @endif
                     @if(Session::has('customerid')) 
                     @if($count > 0)
                     <button class="button pro-add-to-cart" title="Add to Cart" type="submit" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span></button>
                     @else 
                     <button type="button" class="btn btn-danger">
                      
                     @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif
                     </button>  
                    
                     @endif 

                     @else

                     @if($count > 0)
                     <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                     <button type="button" class=" button pro-add-to-cart">
                     <span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                     @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span>
                     </button> 
                     </a>
                     @else
                     <button type="button" class="btn btn-danger">
                        
                     @if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif
                     </button> 
                     

                     @endif 

                     @endif

                     {{ Form::close() }}
                  </div>
                  <div class="product-cart-option">
                     <ul>
                        <li>
                           {{-- Add to wishlist--}}
                           @if($count > 0)  
                           @if(Session::has('customerid'))
                           @if(count($prodInWishlist)==0)
                           <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                           {{ Form::hidden('pro_id','$pro_details_by_id->pro_id') }}        
                           <!-- <input type="hidden" name="pro_id" value="<?php echo $pro_details_by_id->pro_id; ?>"> -->
                           <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                           <a href="" onclick="addtowish({{ $pro_details_by_id->pro_id }},{{ Session::get('customerid') }})">
                           <input type="hidden" id="wishlisturl" value="{{ url('user_profile?id=4') }}">
                           <i class="fa fa-heart-o" aria-hidden="true"></i>@if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{ trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif 
                           </a>
                           @else
                           <?php /* remove wishlist */?>   
                           <a href="{!! url('remove_wish_product').'/'.$prodInWishlist->ws_id!!}">
                           <i class="fa fa-heart" aria-hidden="true"></i>    @if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST') }} @endif
                           </a> 
                           <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                           @endif  
                           @else 
                           <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                           <i class="fa fa-heart-o" aria-hidden="true"></i> @if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_WISHLIST') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST') }} @endif
                           </a>
                           @endif
                           @endif
                        </li>
                        {{-- 
                        <li><a href="#"><i class="fa fa-link"></i><span>Add to Compare</span></a></li>
                        <li><a href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
                        --}}
                     </ul>
                  </div>
                  <div class="pro-tags">
                     <div class="pro-tags-inner">
                        <div class="pro-tags-title">@if (Lang::has(Session::get('lang_file').'.DELIVERY_WITH_IN')!= '') {{ trans(Session::get('lang_file').'.DELIVERY_WITH_IN') }} @else {{ trans($OUR_LANGUAGE.'.DELIVERY_WITH_IN') }} @endif:</div>
                        {{ date('D d, M Y', strtotime($delivery_date)) }}
                     </div>
                  </div>
				
				
				   <div class="pro-tags">
                     <div class="pro-tags-inner">
                        <div class="pro-tags-title">@if (Lang::has(Session::get('lang_file').'.SHARE_THIS')!= '') {{ trans(Session::get('lang_file').'.SHARE_THIS') }} @else {{ trans($OUR_LANGUAGE.'.SHARE_THIS') }} @endif:</div>
						
						 @foreach($product_details_by_id as $product_det) 
                     @php $product_image = explode('/**/',$product_det->pro_Img);
                       $product_img    = $product_image[0]; @endphp
                     @if($product_img != '')
                       @php $prod_path = url('').'/public/assets/product/'.$product_img; @endphp                    
                     @else
                       @php $prod_path  = url('').'/public/assets/default_image/No_image_product.png'; @endphp

                     @endif
                     @endforeach
                       <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$prod_path}}">
							<img width="32px" height="30px" src="{{url('')}}/public/themes/image/Socialmedia/facebook.png" title="share in facebook">
						</a>
						
						<a target="_blank" href="https://twitter.com/home?status={{Request::url()}}')">
							<img width="32px" height="30px" src="{{url('')}}/public/themes/image/Socialmedia/twitter.png" title="share in twitter">
						</a>
						
						  <!-- Share in Google + -->
						<a target="_blank" href="https://plus.google.com/share?url={{$prod_path}}">
                         <img src="{{url('')}}/public/themes/image/Socialmedia/google.png" width="30" title="share in G+">
                      </a>
                      
                      

                  <!-- Share in Linked In -->
                  <!--<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{Request::url()}}">-->
                      <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{$prod_path}}">
                       <img src="{{url('')}}/public/themes/image/Socialmedia/linkdin.png" width="30" title="share in LinkedIn">
                  </a>
						
						
						
                     </div>
                  </div>
				  
				
				  
                  {{-- Policy details--}}
                  @if($pro_details_by_id->allow_cancel=='1' || $pro_details_by_id->allow_return=='1' || $pro_details_by_id->allow_replace=='1')
                  <div class="share-box">
                     <div class="title">@if (Lang::has(Session::get('lang_file').'.POLICY_DETAILS')!= '') {{  trans(Session::get('lang_file').'.POLICY_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.POLICY_DETAILS') }} @endif :</div>
                     <div class="socials-box">
                        @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                        @php   $cancel_policy  = 'cancel_policy';
                        $return_policy  = 'return_policy';
                        $replace_policy = 'replace_policy'; @endphp
                        @else   
                        @php    $cancel_policy  = 'cancel_policy_'.Session::get('lang_code'); 
                        $return_policy  = 'return_policy_'.Session::get('lang_code'); 
                        $replace_policy = 'replace_policy_'.Session::get('lang_code');  @endphp
                        @endif 
                        <div class="cancel-policy">
                           @if($pro_details_by_id->allow_cancel=='1')
                           <p class="policy-title"> @if($pro_details_by_id->cancel_days>0) 
                              {{ $pro_details_by_id->cancel_days }}
                              @if($pro_details_by_id->cancel_days>1) 
                              {{ (Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS') }}
                              @else 
                              {{ (Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY') }}
                              @endif
                              @if (Lang::has(Session::get('lang_file').'.CANCELLATION_ONLY')!= '') {{  trans(Session::get('lang_file').'.CANCELLATION_ONLY') }}  @else {{ trans($OUR_LANGUAGE.'.CANCELLATION_ONLY') }} @endif
                              <span class="policy-quest" id="policyclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                              <br>
                              @if($pro_details_by_id->$cancel_policy!='')
                           <div class="policy-container dev_cancel" style="display: none;">
                              <p>{!! $pro_details_by_id->$cancel_policy !!}   </p>
                           </div>
                           @endif
                           @endif
                           </p>
                           @endif
                           @if($pro_details_by_id->allow_return=='1')
                           <p class="policy-title"> @if($pro_details_by_id->return_days>0) 
                              {{ $pro_details_by_id->return_days }}
                              @if($pro_details_by_id->return_days>1) 
                              {{ (Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS') }}
                              @else 
                              {{ (Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY') }}
                              @endif
                              @if (Lang::has(Session::get('lang_file').'.RETURN_ONLY')!= '') {{ trans(Session::get('lang_file').'.RETURN_ONLY') }} @else {{ trans($OUR_LANGUAGE.'.RETURN_ONLY') }} @endif
                              <span class="policy-quest" id="returnclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                              <br>
                              @if($pro_details_by_id->$return_policy!='')
                           <div class="policy-container dev_return" style="display: none;"> 
                              {!! $pro_details_by_id->$return_policy !!}    
                           </div>
                           @endif    
                           @endif
                           </p>
                           @endif 
                           @if($pro_details_by_id->allow_replace=='1')
                           <p class="policy-title"> @if($pro_details_by_id->replace_days>0) 
                              {{ $pro_details_by_id->replace_days }}
                              @if($pro_details_by_id->replace_days>1) 
                              {{ (Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS') }}
                              @else 
                              {{ (Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY') }}
                              @endif
                              @if (Lang::has(Session::get('lang_file').'.REPLACEMENT_ONLY')!= '') {{ trans(Session::get('lang_file').'.REPLACEMENT_ONLY') }}  @else {{ trans($OUR_LANGUAGE.'.REPLACEMENT_ONLY') }} @endif
                              <span class="policy-quest" id="replaceclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                              <br>
                              @if($pro_details_by_id->$replace_policy!='')
                           <div class="policy-container dev_replace" style="display: none;"> 
                              {!! $pro_details_by_id->$replace_policy !!}    
                           </div>
                           @endif    
                           @endif
                           </p>                        
                           @endif    
                        </div>
                     </div>
                  </div>
                  @endif
               </div>
            </div>
         </div>
         {{-- end before description--}}
         @endif 
         <div class="product-overview-tab">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="product-tab-inner">
                        <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                           <li class="active"> <a href="#description" data-toggle="tab"> @if (Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') {{ trans(Session::get('lang_file').'.DESCRIPTION') }}  @else {{ trans($OUR_LANGUAGE.'.DESCRIPTION') }} @endif</a> </li>
                           <li> <a href="#specification" data-toggle="tab">@if (Lang::has(Session::get('lang_file').'.SPECIFICATION')!= '') {{ trans(Session::get('lang_file').'.SPECIFICATION') }}  @else {{ trans($OUR_LANGUAGE.'.SPECIFICATION') }} @endif</a> </li>
                           <li><a href="#reviews" data-toggle="tab">@if (Lang::has(Session::get('lang_file').'.REVIEWS')!= '') {{ trans(Session::get('lang_file').'.REVIEWS') }}  @else {{ trans($OUR_LANGUAGE.'.REVIEWS') }} @endif</a></li>
                           {{-- 
                           <li> <a href="#custom_tabs" data-toggle="tab">Custom Tab</a> </li>
                           --}}
                        </ul>
                        <div id="productTabContent" class="tab-content">
                           <div class="tab-pane fade in active" id="description" style="max-height: 300px;overflow-y: auto;">
                              <div class="std">
                                 <p> {!! $pro_details_by_id->$pro_desc !!} </p>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="specification">
                              <div >
                                 <div >
                                    <form  action="#" method="get">
                                       <div >
                                          @if(count($product_specGroupList) > 0)
                                          @foreach($product_specGroupList as $specgp)
                                          @if(count($product_spec_details[$specgp->spg_id])>0)
                                          @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
                                          @php    $spg_name = 'spg_name';
                                          $sp_name = 'sp_name';
                                          $spc_value = 'spc_value'; @endphp
                                          @else   
                                          @php    $spg_name = 'spg_name_'.Session::get('lang_code'); 
                                          $sp_name = 'sp_name_'.Session::get('lang_code');
                                          $spc_value = 'spc_value_langCode'; @endphp
                                          @endif
                                          <ul>
                                             <li>
                                                {{ $specgp->$spg_name }}
                                                <br> 
                                                @foreach($product_spec_details[$specgp->spg_id] as $prodSpec)
                                                @if($prodSpecAr[$prodSpec->sp_id]) 
                                                <div class="spec-top">
                                                   {{ $prodSpec->$sp_name }}: 
                                                   @php $i=1; @endphp
                                                   @foreach($prodSpecAr[$prodSpec->sp_id] as $spcVal)
                                                   <span class=""> {{ $spcVal->$spc_value }} 
                                                   @if($i!= count($prodSpecAr[$prodSpec->sp_id]))
                                                   @php $i++; @endphp
                                                   @endif </span>
                                                   @endforeach
                                                </div>
                                                @endif    
                                                @endforeach    
                                             </li>
                                          </ul>
                                          @endif
                                          @endforeach
                                          @else
                                          <p> @if (Lang::has(Session::get('lang_file').'.NO_SPECIFICATION_AVAILABLE')!= '') {{ trans(Session::get('lang_file').'.NO_SPECIFICATION_AVAILABLE') }}  @else {{ trans($OUR_LANGUAGE.'.NO_SPECIFICATION_AVAILABLE') }} @endif
                                          </p>
                                          @endif
                                       </div>
                                    </form>
                                 </div>
                                 <!--tags-->
                              </div>
                           </div>
                           <div id="reviews" class="tab-pane fade">
                              <div class="col-sm-5 col-lg-5 col-md-5">
                                 <div class="reviews-content-left">
                                    <h2>@if (Lang::has(Session::get('lang_file').'.CUST_REVIEW')!= '') {{ trans(Session::get('lang_file').'.CUST_REVIEW') }}  @else {{ trans($OUR_LANGUAGE.'.CUST_REVIEW') }} @endif</h2>
                                    @if(count($review_details)!=0)                   
                                    @php $r=0; 
                                    @endphp
                                    @foreach($review_details as $col)
                                    @php  $customer_name = $col->cus_name;
                                    $customer_mail = $col->cus_email;
                                    $customer_img = $col->cus_pic;
                                    $review_title = $col->title;
                                    $customer_comments = $col->comments;
                                    $customer_date = $col->review_date;
                                    $customer_product = $col->product_id;
                                    $final_date = date('M j, Y', strtotime($col->review_date) );
                                    $customer_title = $col->title;
                                    $customer_name_arr = str_split($customer_name);
                                    $start_letter = strtolower($customer_name_arr[0]);
                                    $customer_ratings = $col->ratings;
                                    $r++; @endphp 
                                    <div class="review-ratting">
                                       <p style="font-weight: 600;"> {{ $review_title }} by <span style="color:#e83f33;">{{ $customer_name }}</span></p>
                                       <table>
                                          <tbody>
                                             <tr>
                                                <th>@if (Lang::has(Session::get('lang_file').'.RATING')!= '') {{ trans(Session::get('lang_file').'.RATING') }}  @else {{ trans($OUR_LANGUAGE.'.RATING') }} @endif</th>
                                                <td>
                                                   <div class="rating"> 
                                                      @if($customer_ratings=='1')
                                                      <img src='{{ url('./images/stars-1.png') }}' style=''> 
                                                      @elseif($customer_ratings=='2')
                                                      <img src='{{ url('./images/stars-2.png') }}' style=''> 
                                                      @elseif($customer_ratings=='3')
                                                      <img src='{{ url('./images/stars-3.png') }}' style=''> 
                                                      @elseif($customer_ratings=='4')
                                                      <img src='{{ url('./images/stars-4.png') }}' style=''> 
                                                      @elseif($customer_ratings=='5')
                                                      <img src='{{ url('./images/stars-5.png') }}' style=''>
                                                      @endif
                                                   </div>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       <p class=""> {{ $customer_comments }}.<small> (Posted on {{ $final_date }})</small> </p>
                                    </div>
                                    @endforeach
                                    @else
                                    @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT') }} @endif @endif
                                 </div>
                              </div>
                              <div class="col-sm-7 col-lg-7 col-md-7">
                                 <div class="reviews-content-right">
                                    <h2>@if(Lang::has(Session::get('lang_file').'.WRITE_A_REVIEW')!= '') {{  trans(Session::get('lang_file').'.WRITE_A_REVIEW') }} @else {{ trans($OUR_LANGUAGE.'.WRITE_A_REVIEW') }} @endif</h2>
                                    {!! Form::open(array('url'=>'productcomments','class'=>'form-horizontal loginFrm')) !!}
                                    {{-- 
                                    <h3>You're reviewing: <span>Donec Ac Tempus</span></h3>
                                    --}}
                                    <h4>@if(Lang::has(Session::get('lang_file').'.RATE_FOR_PRODUCT')!= '') {{  trans(Session::get('lang_file').'.RATE_FOR_PRODUCT') }} @else {{ trans($OUR_LANGUAGE.'.RATE_FOR_PRODUCT') }} @endif<em>*</em></h4>
                                    {{-- Customer ratings --}}
                                    <div class="table-responsive reviews-table">
                                       <fieldset class="rating disableRating addReviews">
                                          <input id="review_ratings_5" name="ratings" type="radio" value="5" > 
                                          <label for="review_ratings_5" title="Awesome - 5 stars"> </label>
                                          <input id="review_ratings_4" name="ratings" type="radio" value="4" > 
                                          <label for="review_ratings_4" title="Pretty good - 4 stars"> </label>
                                          <input id="review_ratings_3" name="ratings" type="radio" value="3" > 
                                          <label for="review_ratings_3" title="Not bad - 3 stars"> </label>
                                          <input id="review_ratings_2" name="ratings" type="radio" value="2" > 
                                          <label for="review_ratings_2" title="Bad - 2 stars"> </label>
                                          <input id="review_ratings_1" name="ratings" type="radio" value="1" > 
                                          <label for="review_ratings_1" title="Very Bad - 1 stars"> </label>
                                       </fieldset >
                                    </div>
                                    <div class="form-area">
                                       <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                       <input type="hidden" name="customer_id" value="<?php echo Session::get('customerid');?>">
                                       <input type="hidden" name="product_id" value="{!!$pro_details_by_id->pro_id!!}">
                                       <div class="form-element">
                                          <label>@if (Lang::has(Session::get('lang_file').'.REVIEW_SUMMARY')!= '') {{  trans(Session::get('lang_file').'.REVIEW_SUMMARY') }} @else {{ trans($OUR_LANGUAGE.'.REVIEW_SUMMARY') }} @endif<em>*</em></label>
                                          <input type="text" name="title" value="" placeholder="@if (Lang::has(Session::get('lang_file').'.REVIEW_TITLE')!= '') {{  trans(Session::get('lang_file').'.REVIEW_TITLE') }} @else {{ trans($OUR_LANGUAGE.'.REVIEW_TITLE') }} @endif" required>
                                       </div>
                                       <div class="form-element">
                                          <label>{{ (Lang::has(Session::get('lang_file').'.REVIEWS')!= '') ? trans(Session::get('lang_file').'.REVIEWS') : trans($OUR_LANGUAGE.'.REVIEWS') }} <em>*</em></label>
                                          <textarea name="comments" placeholder="@if (Lang::has(Session::get('lang_file').'.REVIEW_DESCRIPTION')!= '') {{  trans(Session::get('lang_file').'.REVIEW_DESCRIPTION') }} @else {{ trans($OUR_LANGUAGE.'.REVIEW_DESCRIPTION') }} @endif" required></textarea>
                                       </div>
                                       <div class="buttons-set">
                                          @if(Session::has('customerid'))  
                                          <button class="button submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;@if (Lang::has(Session::get('lang_file').'.SUBMIT_REVIEW')!= '') {{ trans(Session::get('lang_file').'.SUBMIT_REVIEW') }} @else {{ trans($OUR_LANGUAGE.'.SUBMIT_REVIEW') }} @endif</span></button>
                                          @else 
                                          <a href="" role="button" data-toggle="modal" data-target="#loginpop"><button class="button submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;@if (Lang::has(Session::get('lang_file').'.SUBMIT_REVIEW')!= '') {{ trans(Session::get('lang_file').'.SUBMIT_REVIEW') }} @else {{ trans($OUR_LANGUAGE.'.SUBMIT_REVIEW') }} @endif</span></button></a>
                                          @endif
                                       </div>
                                    </div>
                                    {!! Form::close() !!}
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Main Container End --> 
<!-- Related Product Slider -->
<div class="container pdt-view-page">
   <div class="row">
      <div class="col-xs-12">
         <div class="related-product-area">
            <div class="page-header">
               <h2>@if (Lang::has(Session::get('lang_file').'.RELATED_PRODUCTS')!= '') {{ trans(Session::get('lang_file').'.RELATED_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.RELATED_PRODUCTS') }} @endif</h2>
            </div>
            @if(count($get_related_product)!=0)  
            @php  $i=0; @endphp 
            <div class="related-products-pro">
               <div class="slider-items-products">
                  <div id="related-product-slider" class="product-flexslider hidden-buttons">
                     <div class="re-pr-slider owl-carousel owl-theme fadeInUp">
                        @foreach($get_related_product as $product_det) 
                        @php  $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                        $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                        $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                        $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
                        $res = base64_encode($product_det->pro_id);
                        $product_image = explode('/**/',$product_det->pro_Img);
                        $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                        $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); @endphp
                        @if((Session::get('lang_code'))=='' || (Session::get('lang_code'))== 'en')  
                        @php    $pro_title = 'pro_title'; @endphp
                        @else @php  $pro_title = 'pro_title_'.Session::get('lang_code') @endphp @endif
                        @php  $product_img    = $product_image[0]; @endphp
                        <!-- /* Image Path */ -->
                        @php    $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_img; @endphp
                        @if(file_exists($img_data) && $product_img !='') 
                        @php   $prod_path = url('').'/public/assets/product/' .$product_img; @endphp                  
                        @else  
                        @if(isset($DynamicNoImage['productImg']))
                        @php    $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp
                        @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))
                        @php    $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif
                        @endif
                        @endif   
                        <!--  /* Image Path ends */   
                           //Alt text -->
                        @php   $alt_text   = substr($product_det->$pro_title,0,25); @endphp
                        @if($product_det->pro_no_of_purchase < $product_det->pro_qty) 
                        @php    $i++; @endphp
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="pr-img-area">
                                    @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  
                                    <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}">
                                       <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}"> {{-- <img class="hover-img" src="images/products/product-6.jpg" alt="HTML template"> --}}</figure>
                                    </a>
                                    @endif
                                    @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')  
                                    <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">
                                       <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}"> {{-- <img class="hover-img" src="images/products/product-6.jpg" alt="HTML template"> --}}</figure>
                                    </a>
                                    @endif
                                    @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                                    <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}">
                                       <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}"> {{-- <img class="hover-img" src="images/products/product-6.jpg" alt="HTML template"> --}}</figure>
                                    </a>
                                    @endif
                                 </div>
                                 {{--  
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                                 --}}
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 
                                       <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}">
                                       @endif 
                                       @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')  
                                       <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">
                                       @endif
                                       @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')  
                                       <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}">
                                       @endif
                                       {{ substr ($product_det->$pro_title,0,20) }}
                                       </a> 
                                    </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $product_det->pro_disprice }}</span> </span> </div>
                                       </div>
                                       {{-- 
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span> {{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                       --}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endif
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
            @else
            @if (Lang::has(Session::get('lang_file').'.NO_PRODUCTS')!= '') {{ trans(Session::get('lang_file').'.NO_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.NO_PRODUCTS') }} @endif
            @endif
         </div>
      </div>
   </div>
   <!-- Related Product Slider End --> 
   <!-- Upsell Product Slider -->
   {{-- 
   <section class="upsell-product-area">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <div class="page-header">
                  <h2>{{ (Lang::has(Session::get('lang_file').'.UP_SELL_PRODUCT')!= '') ? trans(Session::get('lang_file').'.UP_SELL_PRODUCT') : trans($OUR_LANGUAGE.'.UP_SELL_PRODUCT') }}</h2>
               </div>
               <div class="slider-items-products">
                  <div id="upsell-product-slider" class="product-flexslider hidden-buttons">
                     <div class="slider-items slider-width-col4">
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="icon-sale-label sale-left">{{ (Lang::has(Session::get('lang_file').'.SALE')!= '') ? trans(Session::get('lang_file').'.SALE') : trans($OUR_LANGUAGE.'.SALE') }}</div>
                                 <div class="icon-new-label new-right">{{ (Lang::has(Session::get('lang_file').'.NEW')!= '') ? trans(Session::get('lang_file').'.NEW') : trans($OUR_LANGUAGE.'.NEW') }}</div>
                                 <div class="pr-img-area">
                                    <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">
                                       <figure> <img class="first-img" src="images/products/product-3.jpg" alt="HTML template"> <img class="hover-img" src="images/products/product-4.jpg" alt="HTML template"></figure>
                                    </a>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }} </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span> {{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="icon-sale-label sale-left">{{ (Lang::has(Session::get('lang_file').'.SALE')!= '') ? trans(Session::get('lang_file').'.SALE') : trans($OUR_LANGUAGE.'.SALE') }}</div>
                                 <div class="pr-img-area">
                                    <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">
                                       <figure> <img class="first-img" src="images/products/product-12.jpg" alt="HTML template"> <img class="hover-img" src="images/products/product-9.jpg" alt="HTML template"></figure>
                                    </a>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }} </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box">
                                             <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                                             <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                                          </div>
                                       </div>
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span>{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="icon-sale-label sale-left">Sale</div>
                                 <div class="icon-new-label new-right">New</div>
                                 <div class="pr-img-area">
                                    <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">
                                       <figure> <img class="first-img" src="images/products/product-10.jpg" alt="HTML template"> <img class="hover-img" src="images/products/product-16.jpg" alt="HTML template"></figure>
                                    </a>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }} </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span> {{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="pr-img-area">
                                    <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">
                                       <figure> <img class="first-img" src="images/products/product-11.jpg" alt="HTML template"> <img class="hover-img" src="images/products/product-4.jpg" alt="HTML template"></figure>
                                    </a>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }} </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span>{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="pr-img-area">
                                    <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">
                                       <figure> <img class="first-img" src="images/products/product-2.jpg" alt="HTML template"> <img class="hover-img" src="images/products/product-3.jpg" alt="HTML template"></figure>
                                    </a>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }} </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box">
                                             <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                                             <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                                          </div>
                                       </div>
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span>{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="pr-img-area">
                                    <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">
                                       <figure> <img class="first-img" src="images/products/product-5.jpg" alt="HTML template"> <img class="hover-img" src="images/products/product-6.jpg" alt="HTML template"></figure>
                                    </a>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }}" href="single_product.html">{{ (Lang::has(Session::get('lang_file').'.PRODUCT_TITLE_HERE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_TITLE_HERE') : trans($OUR_LANGUAGE.'.PRODUCT_TITLE_HERE') }} </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                          <button type="button" class="add-to-cart"><span> {{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') ? trans(Session::get('lang_file').'.ADD_TO_CART') : trans($OUR_LANGUAGE.'.ADD_TO_CART') }}</span> </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   --}}
   <!-- Upsell Product Slider End --> 
   <!-- Store section -->
   @php $store_lat =''; $store_lan =''; @endphp
   @if($GENERAL_SETTING->gs_store_status == 'Store')
   <div class="store-details">
      <h5 class="page-header" style="font-size: 18px;font-weight: 600;">@if (Lang::has(Session::get('lang_file').'.STORE_DETAILS')!= '') {{  trans(Session::get('lang_file').'.STORE_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.STORE_DETAILS') }} @endif</h5>
      @php $store_lat =''; $store_lan =''; @endphp
      @foreach($get_store as $storerow)
      @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
      @php  $stor_name = 'stor_name';
      $stor_address1 = 'stor_address1';
      $stor_address2 = 'stor_address2'; @endphp
      @else   
      @php  $stor_name = 'stor_name_'.Session::get('lang_code'); 
      $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 
      $stor_address2 = 'stor_address2_'.Session::get('lang_code'); @endphp 
      @endif
      @php  $store_name = $storerow->$stor_name;
      $store_address = $storerow->$stor_address1;
      $store_address2 = $storerow->$stor_address2;
      $store_zip = $storerow->stor_zipcode;
      $store_phone = $storerow->stor_phone;
      $store_web = $storerow->stor_website;
      $store_lat = $storerow->stor_latitude;
      $store_lan = $storerow->stor_longitude; 
      $store_image = $storerow->stor_img; @endphp
      @php $prod_path  = url('').'/public/assets/default_image/No_image_store.png';
      $img_data   = "public/assets/storeimage/".$store_image; @endphp
      @if(file_exists($img_data) && $store_image !='')  
      @php
      $prod_path = url('').'/public/assets/storeimage/' .$store_image; @endphp                 
      @else  
      @if(isset($DynamicNoImage['store']))
      @php    $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; @endphp
      @if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))  
      @php  $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp
      @endif
      @endif
      @endif   
      @php  $alt_text   = substr($storerow->$stor_name,0,25);
      $alt_text  .= strlen($storerow->$stor_name)>25?'..':''; @endphp
      <div class="main container">
         <div class="row">
            <div class="col-xs-12 col-sm-6">
               <h1 style="font-weight: 600;font-size: 22px;"> <span style="color: #e83f33;">{{ $store_name }}</span></h1>
               <p> <a title="View Store" target="_blank" href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($storerow->stor_id)))); ?>">
                  <img src="{{ $prod_path }}" alt="{{ $alt_text }}" style="width:70px; height:70px;">
                  </a> 
               </p>
               <ul style="color: #999;  font-size: 15px; list-style-type:none;display: block;  margin: 1.2em 0 0;">
                  <li>{{ $store_address }},</li>
                  <li>{{ $store_address2 }},</li>
                  <li>{{ $store_zip }}</li>
                  <li>@if (Lang::has(Session::get('lang_file').'.MOBILE')!= '') {{  trans(Session::get('lang_file').'.MOBILE') }} @else {{ trans($OUR_LANGUAGE.'.MOBILE') }} @endif : {{ $store_phone }}</li>
                  <li>@if (Lang::has(Session::get('lang_file').'.WEBSITE')!= '') {{ trans(Session::get('lang_file').'.WEBSITE') }} @else {{ trans($OUR_LANGUAGE.'.WEBSITE') }} @endif : {{ $store_web }}</li>
               </ul>
            </div>
            <div class="col-xs-12 col-sm-6">
               <div class="">
                  <div >
                     <div >
                        <div id="us3" style="width: 100% !important; height: 240px;margin-bottom:10px;background-size: cover;  text-align: center;  padding: 80px 0 100px;"> </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endforeach
   </div>
   @else
   @endif
   <!-- Store section -->
   <!-- service section -->
   @include('service_section')
</div>
<!-- Footer -->
<a href="" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
<!-- End Footer --> 
<?php 
   if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
   
   $map_lang = 'en';
   
   }else {  
   
   $map_lang = 'fr';
   
   }
   
   ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>&language=<?php echo $map_lang; ?>'></script>
<script src="<?php echo url(''); ?>/public/assets/js/locationpicker.jquery.js"></script>
<script>
   $('#us3').locationpicker({
   
         
   
           location: {latitude: <?php echo $store_lat; ?>, longitude: <?php echo $store_lan; ?>},
   
         
   
           radius: 200,
   
           inputBinding: {
   
               latitudeInput: $('#us3-lat'),
   
               longitudeInput: $('#us3-lon'),
   
               radiusInput: $('#us3-radius'),
   
               locationNameInput: $('#us3-address')
   
           },
   
           enableAutocomplete: true,
   
           onchanged: function (currentLocation, radius, isMarkerDropped) {
   
               // Uncomment line below to show alert on each Location Changed event
   
               //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
   
           }
   
       });
   
</script>
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
   $('#add_to_cart_session').click(function(){
   
     var pro_purchase1 = '<?php echo $pro_details_by_id->pro_no_of_purchase; ?>' ;
   
     var pro_purchase = parseInt($('#addtocart_qty').val()) + parseInt(pro_purchase1);
   
     var pro_qty = '<?php echo $pro_details_by_id->pro_qty; ?>';
   
     if(pro_purchase > parseInt(pro_qty))
   
     {
   
       $('#addtocart_qty').focus();
   
       $('#addtocart_qty').css('border-color', 'red');
   
       $('#addtocart_qty_error').html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');
   
       return false;
   
     }
   
     else
   
     {
   
       $('#addtocart_qty').css('border-color', '');
   
       $('#addtocart_qty_error').html('');
   
     }
   
     if($('#addtocart_color').val() ==0) 
   
     {
   
       $('#addtocart_color').focus();
   
       $('#addtocart_color').css('border-color', 'red');
   
       $('#size_color_error').html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');
   
       return false;
   
     }
   
     else
   
     {
   
       $('#addtocart_color').css('border-color', '');
   
       $('#size_color_error').html('');
   
     }
   
     if($('#addtocart_size').val() ==0)
   
     {
   
       $('#addtocart_size').focus();
   
       $('#addtocart_size').css('border-color', 'red');
   
       $('#size_color_error').html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');
   
       return false;
   
     }
   
     else
   
     {
   
       $('#addtocart_size').css('border-color', '');
   
       $('#size_color_error').html('');
   
     }
   
   });
   
   $("#searchbox").keyup(function(e) 
   
   {
   
   
   
   var searchbox = $(this).val();
   
   var dataString = 'searchword='+ searchbox;
   
   if(searchbox=='')
   
   {
   
   $("#display").html("").hide();  
   
   }
   
   else
   
   {
   
     var passData = 'searchword='+ searchbox;
   
      $.ajax( {
   
               type: 'GET',
   
             data: passData,
   
             url: '<?php echo url('autosearch'); ?>',
   
             success: function(responseText){  
   
             $("#display").html(responseText).show();  
   
   }
   
   });
   
   }
   
   return false;    
   
   
   
   });
   
   });
   
   
   
</script>
<script type="text/javascript">
   $(document).ready(function()
   
   
   
   { 
   
   $('#policyclick').click(function(event)
   
   
   
   {  
   
   $('.dev_cancel').slideToggle("fast");
   
   event.stopPropagation();
   
   // $(".dev_cancel").css({"background":"#ffffff", "position":"absolute"});
   
   });
   
   $('#returnclick').click(function(event)
   
   
   
   {
   
   $('.dev_return').slideToggle("fast");
   
   event.stopPropagation();
   
   // $(".dev_return").css({"background":"#ffffff", "position":"absolute"});
   
   });
   
   $('#replaceclick').click(function(event)
   
   
   
   {
   
   $('.dev_replace').slideToggle("fast");
   
   event.stopPropagation();
   
   });
   
   
   
   
   
   $(".policy-container").on("click", function (event) {
   
   event.stopPropagation();
   
   });
   
   
   
   });
   
   
   
   
   
   $(document).on("click", function () {
   
   $(".policy-container").hide();
   
   });
   
   
   
</script>
<script>
   function add_quantity()
   
   {
   
     //alert();
   
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
   
     var quantity=$("#addtocart_qty").val(); 
   
     var remaining_product=parseInt(<?php echo  $pro_details_by_id->pro_qty-$pro_details_by_id->pro_no_of_purchase;?>);
   
    
   
     if(quantity<remaining_product)
   
     {
   
       var new_quantity=parseInt(quantity)+1;
   
       $("#addtocart_qty").val(new_quantity);
   
     }
   
     //alert();
   
   }
   
   
   
   function remove_quantity()
   
   {
   
     //alert();
   
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/
   
   
   
     var quantity=$("#addtocart_qty").val();
   
     var quantity=parseInt(quantity);
   
     if(quantity>1)
   
     {
   
       var new_quantity=quantity-1;
   
       $("#addtocart_qty").val(new_quantity);
   
     }
   
     //alert();
   
   }
   
</script>
<script type="text/javascript">
   function addtowish(pro_id,cus_id){
   
     //alert();
   
     var wishlisturl = document.getElementById('wishlisturl').value;
   
   
   
     $.ajax({
   
           type: "get",   
   
           url:"<?php echo url('addtowish'); ?>",
   
           data:{'pro_id':pro_id,'cus_id':cus_id},
   
             success:function(response){
   
             //alert(response); return false;
   
             if(response==0){
   
             <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>
   
                         $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');
   
               //window.location=wishlisturl;
   
                             window.location.reload();
   
                             
   
             }else{
   
               alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');
   
               //window.location=wishlisturl;
   
             }
   
             
   
             
   
           }
   
         });
   
   }
   
</script>
<script src="{{ url('') }}/themes/js/handleCounter.js"></script>
<script>
   $(function ($) {
   
       var options = {
   
           minimum: 1,
   
           maximize: 10,
   
           onChange: valChanged,
   
           onMinimum: function(e) {
   
               console.log('reached minimum: '+e)
   
           },
   
           onMaximize: function(e) {
   
               console.log('reached maximize'+e)
   
           }
   
       }
   
       $('#handleCounter').handleCounter(options)
   
       $('#handleCounter2').handleCounter(options)
   
   $('#handleCounter3').handleCounter({maximize: 100})
   
   })
   
   function valChanged(d) {
   
   //            console.log(d)
   
   }
   
</script>
<!-- For Responsive menu-->
<script type="text/javascript">
   $(document).ready(function() {
   
       $(document).on("click", ".customCategories .topfirst b", function() {
   
           $(this).next("ul").css("position", "relative");
   
   
   
           $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");
   
           $(this).next("ul").toggle();
   
       });
   
   
   
       $(document).on("click", ".morePage", function() {
   
           $(".nextPage").slideToggle(200);
   
       });
   
   
   
       $(document).on("click", "#smallScreen", function() {
   
           $(this).toggleClass("customMenu");
   
       });
   
   
   
       $(window).scroll(function() {
   
           if ($(this).scrollTop() > 250) {
   
               $('#comp_myprod').show();
   
           } else {
   
               $('#comp_myprod').hide();
   
           }
   
       });
   
   
   
   });
   
</script>



{!! $footer !!}


<script>
  $('.re-pr-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: false,
                dots: false,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>                  
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 3
                  },
                  992: {
                    items: 4
                  },
                  1200: {
                    items: 4
                  }
                }
     })
</script>

</body>
</html>