<div class="jtv-service-area">
    <div class="container">
      <div class="row">
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper ship">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-paper-plane"></i></div>
              <div class="service-wrapper">
                <h3>@if (Lang::has(Session::get('lang_file').'.WORLD_SHIP')!= '') {{  trans(Session::get('lang_file').'.WORLD_SHIP') }}  @else {{ trans($OUR_LANGUAGE.'.WORLD_SHIP') }} @endif</h3>
                <p>@if (Lang::has(Session::get('lang_file').'.GET_ANYWH')!= '') {{  trans(Session::get('lang_file').'.GET_ANYWH') }}  @else {{ trans($OUR_LANGUAGE.'.GET_ANYWH') }} @endif</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12 ">
          <div class="block-wrapper return">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-rotate-right"></i></div>
              <div class="service-wrapper">
                <h3>@if (Lang::has(Session::get('lang_file').'.SECURE')!= '') {{  trans(Session::get('lang_file').'.SECURE') }}  @else {{ trans($OUR_LANGUAGE.'.SECURE') }} @endif</h3>
                <p>@if (Lang::has(Session::get('lang_file').'.BANKNG')!= '') {{  trans(Session::get('lang_file').'.BANKNG') }}  @else {{ trans($OUR_LANGUAGE.'.BANKNG') }} @endif</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper support">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-umbrella"></i></div>
              <div class="service-wrapper">
                <h3>@if (Lang::has(Session::get('lang_file').'.SUPPORT')!= '') {{  trans(Session::get('lang_file').'.SUPPORT') }}  @else {{ trans($OUR_LANGUAGE.'.SUPPORT') }} @endif</h3>
               @php $ad_phone = DB::table('nm_emailsetting')->select('es_phone1')->get();  @endphp
               @foreach($ad_phone as $ph) @php $phone = $ph->es_phone1; @endphp @endforeach
               
                <p>@if (Lang::has(Session::get('lang_file').'.CALL_US')!= '') {{  trans(Session::get('lang_file').'.CALL_US') }}  @else {{ trans($OUR_LANGUAGE.'.CALL_US') }} @endif : {{ $phone }}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper user">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-tags"></i></div>
              <div class="service-wrapper">
                <h3>@if (Lang::has(Session::get('lang_file').'.MEM_DISCOUNT')!= '') {{  trans(Session::get('lang_file').'.MEM_DISCOUNT') }}  @else {{ trans($OUR_LANGUAGE.'.MEM_DISCOUNT') }} @endif</h3>
                <p>@if (Lang::has(Session::get('lang_file').'.USE_COUPON')!= '') {{  trans(Session::get('lang_file').'.USE_COUPON') }}  @else {{ trans($OUR_LANGUAGE.'.USE_COUPON') }} @endif</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>