<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | @if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT') }} @endif</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="{{ url('') }}/public/assets/css/plan.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp
     @if(count($favi)>0)  
     @foreach($favi as $fav) @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
@endif
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >@if (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_HOME') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME') }} @endif</a></li>
                                <li class="active"><a>@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT') }} @endif</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT') }} @endif</h5>
            
        </header>
        @if ($errors->any())
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message')) !!}
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
		</ul>	
		@endif
         @if (Session::has('mail_exist'))
		<div class="alert alert-warning alert-dismissable">{!! Session::get('mail_exist') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif
        <div id="error_msg"></div>
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    {!! Form::open(array('url'=>'add_merchant_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                         @if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_ACCOUNT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_ACCOUNT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_ACCOUNT') }} @endif
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_FIRST_NAME')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_FIRST_NAME') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_FIRST_NAME') }} @endif <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('first_name',Input::old('first_name'),array('id'=>'first_name','class'=>'form-control','maxlength'=>'50','placeholder'=>'Enter Merchant First Name')) }}
                        <!-- <input type="text" class="form-control" maxlength="50" placeholder="Enter Merchant First Name" id="first_name" name="first_name" value="{!! Input::old('first_name') !!}" > -->
                        <div id="first_name_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_NAME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LAST_NAME') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_NAME') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('last_name',Input::old('last_name'),array('id'=>'last_name','class'=>'form-control','maxlength'=>'50','placeholder'=>'Enter Merchant Last Name')) }}
                       <!--  <input type="text" maxlength="50" class="form-control" placeholder="Enter Merchant Last Name" id="last_name"  name="last_name" value="{!! Input::old('last_name') !!}"  > -->
                        <div id="last_name_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_EMAIL') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       {{ Form::text('email_id',Input::old('email_id'),array('id'=>'email_id','class'=>'form-control','placeholder'=>'Enter Email Id','onchange'=>'email_exixts();')) }}
                       <!--  <input type="email" class="form-control" onchange="email_exixts();" placeholder="Enter Email Id" id="email_id" name="email_id" value="{!! Input::old('email_id') !!}" > -->
                        <div id="email_id_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4"> 
                       <select class="form-control" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" >
                        <option value="0">-- @if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT') }} @endif --</option>
                          @foreach($country_details as $country_fetch)
          				 <option value="{{ $country_fetch->co_id }}" <?php if(Input::old('select_mer_country')==$country_fetch->co_id){echo 'selected';} ?>>{{ $country_fetch->co_name }}</option>
           				   @endforeach
       					 </select>
                            
                    </div>
                    <div id="country_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_CITY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SELECT_CITY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_CITY') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					
                       <select class="form-control" name="select_mer_city" id="select_mer_city" >
           				<option value="0">-- @if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SELECT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT') }} @endif --</option>
						
							@if(Input::old('select_mer_country'))
							
								@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
								@php	$ci_name = 'ci_name'; @endphp
								@else  @php $ci_name = 'ci_name_'.Session::get('lang_code'); @endphp @endif
								@foreach($city_details as $cities)
								
									@if($cities->ci_con_id==Input::old('select_mer_country'))
									
										@if(Input::old('select_mer_city')==$cities->ci_id)
										@php	$selected="selected"; @endphp
										@else
										@php	$selected=""; @endphp
										 {{ "<option value='" . $cities->ci_id . "'".$selected."> " . $cities->$ci_name. " </option>" }} @endif

									@endif
								@endforeach
							@endif
							
							
							
							
						</select>
                    </div>
                    <div id="city_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_PHONE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       {{ Form::text('phone_no',Input::old('phone_no'),array('id'=>'phone_no','class'=>'form-control','maxlength'=>'16','placeholder'=>'Enter Phone Number')) }}
                        <!-- <input type="text" maxlength="16" class="form-control" placeholder="Enter Phone Name" id="phone_no" name="phone_no" value="{!! Input::old('phone_no') !!}"  > -->
                        <div id="phone_no_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_ADDRESS1') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('addreess_one',Input::old('addreess_one'),array('id'=>'addreess_one','class'=>'form-control','maxlength'=>'149','placeholder'=>'Enter Address One')) }}
                        <!-- <input type="text" class="form-control" placeholder="Enter Address One" id="addreess_one" name="addreess_one" value="{!! Input::old('addreess_one') !!}"  maxlength="149"> -->
                        <div id="addreess_one_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADDRESS2') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('address_two',Input::old('address_two'),array('id'=>'address_two','class'=>'form-control','maxlength'=>'149','placeholder'=>'Enter Address Two')) }}
                      <!--   <input type="text" class="form-control" placeholder="Enter Address Two" id="address_two" name="address_two" value="{!! Input::old('address_two') !!}"  maxlength="149"> -->
                        <div id="addreess_two_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>

                          <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_COMMISSION')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_COMMISSION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMMISSION') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       {{ Form::number('commission',Input::old('commission'),array('id'=>'commission','class'=>'form-control','placeholder'=>'Enter Admin Commission','min'=>'1','max'=>'99')) }}
                     
                    </div>
                     <div class="col-lg-1 date-top">
                     <span>%</span>
                     </div>
                    <div id="commission_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>

						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_ACCOUNT_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_ACCOUNT_DETAILS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_ACCOUNT_DETAILS') }} @endif<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYPAL')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_PAYPAL') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYPAL') }} @endif @if (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID') }} @endif ( @if (Lang::has(Session::get('admin_lang_file').'.BACK_OR')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_OR') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_OR') }} @endif ) @if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_BANK_DETAILS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_PAYMENT_BANK_DETAILS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_BANK_DETAILS') }} @endif" id="payment_account" name="payment_account" value="{!!Input::old('payment_account') !!}"  >
                        
                    </div>
                </div>
                        </div>

                       <!--  <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY_ACCOUNT_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY_ACCOUNT_DETAILS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY_ACCOUNT_DETAILS') }} @endif<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder="@if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY') }} @endif @if (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID') }} @endif ( @if (Lang::has(Session::get('admin_lang_file').'.BACK_OR')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_OR') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_OR') }} @endif ) @if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY_DETAILS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY_DETAILS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY_DETAILS') }} @endif" id="payment_account" name="payment_account" value="{!!Input::old('payment_account') !!}"  >
                        
                    </div>
                </div>
                        </div>
 -->
                               <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.PAYMENT_EMAIL_KEY')!= '') {{  trans(Session::get('admin_lang_file').'.PAYMENT_EMAIL_KEY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.PAYMENT_EMAIL_KEY') }} @endif<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder=" @if (Lang::has(Session::get('admin_lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY')!= '') {{  trans(Session::get('admin_lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY') }} @endif " id="mer_payu_key" name="mer_payu_key" value="{!!Input::old('mer_payu_key') !!}"  >
                        
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.PAYMENT_EMAIL_SALT')!= '') {{  trans(Session::get('admin_lang_file').'.PAYMENT_EMAIL_SALT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.PAYMENT_EMAIL_SALT') }} @endif<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder=" @if (Lang::has(Session::get('admin_lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT')!= '') {{  trans(Session::get('admin_lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT') }} @endif " id="mer_payu_salt" name="mer_payu_salt" value="{!!Input::old('mer_payu_salt') !!}"  >
                        
                    </div>
                </div>
                        </div>
                        
                    </div>
					
					 <div class="panel panel-default">
                        <div class="panel-heading">
                      @if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORE_DETAILS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_DETAILS') }} @endif 
                        </div>
                    <div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_NAME')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_STORE_NAME') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_NAME') }} @endif<span class="text-sub">*</span></label>

							<div class="col-lg-4">

								<input type="text" class="form-control" placeholder="Enter Store Name {{ $default_lang }}" id="store_name" name="store_name" value="{!! Input::old('store_name') !!}"  >
								<div id="store_name_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
					
				@if(!empty($get_active_lang))  
				@foreach($get_active_lang as $get_lang) 
				@php $get_lang_name = $get_lang->lang_name; @endphp
				
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Store Name({{ $get_lang_name }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Name in {{ $get_lang_name }}" id="store_name_{{ $get_lang_name }}" name="store_name_{{ $get_lang_name }}" value="{!! Input::old('store_name_'.$get_lang_name) !!}"  maxlength="100">
								<div id="store_name_fr_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
					 @endforeach @endif
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_PHONE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE') }} @endif <span class="text-sub">*</span></label>

							<div class="col-lg-4">
                 {{ Form::text('store_pho',Input::old('store_pho'),array('id'=>'store_pho','class'=>'form-control','maxlength'=>'16','placeholder'=>'Enter Store Phone Number')) }}
								<!-- <input type="text" class="form-control" maxlength="16" placeholder="Enter Store Phone Number" id="store_pho" name="store_pho" value="{!! Input::old('store_pho') !!}"  > -->
								<div id="store_pho_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
						
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADDRESS1') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1') }} @endif<span class="text-sub">*</span></label>

							<div class="col-lg-4">
                {{ Form::text('store_address_one',Input::old('store_address_one'),array('id'=>'store_add_one','class'=>'form-control','placeholder'=>'Enter Store Address one')) }}
								<!-- <input type="text" class="form-control" placeholder="Enter Store Address one {{ $default_lang }}" id="store_add_one" name="store_address_one" value="{!! Input::old('store_address_one') !!}"  > -->
								<div id="store_addr_one_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
					
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address one({{ $get_lang_name }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address one In {{ $get_lang_name }}" id="store_add_one_{{ $get_lang_name }}" name="store_address_one_{{ $get_lang_name }}" value="{!! Input::old('store_address_one_'.$get_lang_name) !!}"  >
								<div id="store_addr_one_fr_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
					@endforeach @endif
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADDRESS2') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2') }} @endif<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address One {{ $default_lang }}" id="store_add_two" name="store_address_two" value="{!! Input::old('store_address_two') !!}"   >
								<div id="store_addr_two_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
					
					@if(!empty($get_active_lang)) 
					@foreach($get_active_lang as $get_lang) 
					@php $get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address two({{ $get_lang_name  }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address Two In {{ $get_lang_name  }}" id="store_add_two_{{ $get_lang_name }}" name="store_address_two_{{ $get_lang_name }}" value="{!! Input::old('store_address_two_'.$get_lang_name) !!}"   >
								<div id="store_addr_two_fr_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
                     @endforeach @endif
					 
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY') }} @endif<span class="text-sub">*</span></label>

							<div class="col-lg-4"> 
								<select class="form-control" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
									<option value="0">-- @if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT') }} @endif --</option>
									@foreach($country_details as $country_fetch) 
									<option <?php if(Input::old('select_country')==$country_fetch->co_id){echo 'selected';} ?> value="{{ $country_fetch->co_id }}">{{ $country_fetch->co_name }}</option>
									@endforeach
								</select>
								<div id="store_country_error_msg" style="color:#F00;font-weight:800"></div>
							</div>
						</div>
                    </div>
					
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_CITY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SELECT_CITY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_CITY') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       <select class="form-control" name="select_city" id="select_city" >
           				<option value="">-- @if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT') }} @endif --</option>
						
							@if(Input::old('select_country'))
							
								@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  
								@php	$ci_name = 'ci_name'; @endphp
								@else @php $ci_name = 'ci_name_'.Session::get('lang_code'); @endphp @endif
								@foreach($city_details as $cities)
								
									@if($cities->ci_con_id==Input::old('select_country'))
									
										@if(Input::old('select_city')==$cities->ci_id)
										@php	$selected="selected"; @endphp
										@else
										@php	$selected=""; @endphp
										{{ "<option value='" . $cities->ci_id . "'".$selected."> " . $cities->$ci_name. " </option>" }} @endif
									@endif
								@endforeach
							@endif
							
							
                  </select>
                  <div id="store_city_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                    
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ZIPCODE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ZIPCODE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ZIPCODE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('zip_code',Input::old('zip_code'),array('id'=>'zip_code','class'=>'form-control','maxlength'=>'12','placeholder'=>'Enter Store Zipcode')) }}
                       <!--  <input type="text" class="form-control" placeholder="Enter Store Zipcode" maxlength="12" id="zip_code" name="zip_code" value="{!! Input::old('zip_code') !!}"> -->
                        <div id="zip_code_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_META_KEYWORDS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_META_KEYWORDS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_META_KEYWORDS') }} @endif<span class="text-sub"></span></label>

							<div class="col-lg-4">
								<textarea  class="form-control" name="meta_keyword" id="meta_keyword" placeholder="Enter Meta Keywords {{ $default_lang }}">{!! Input::old('meta_keyword') !!}</textarea>
							</div>
							<div id="meta_key_error_msg" style="color:#F00;font-weight:800"></div>
						</div>
                    </div>
					
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Meta Keywords({{ $get_lang_name }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<textarea  class="form-control" name="meta_keyword_{{ $get_lang_name }}" placeholder="Enter Meta Keywords In {{ $get_lang_name }}" id="meta_keyword_{{ $get_lang_name }}" >{!! Input::old('meta_keyword_'.$get_lang_name) !!}</textarea>
							</div>
							<div id="meta_key_fr_error_msg" style="color:#F00;font-weight:800"></div>
						</div>
                    </div>
					@endforeach @endif
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_META_DESCRIPTION') }} @endif<span class="text-sub"></span></label>

							<div class="col-lg-4">
								<textarea id="meta_description"  placeholder="Enter Meta Description {{ $default_lang }}" name="meta_description" class="form-control">{!! Input::old('meta_description') !!}</textarea>
							</div>
							<div id="meta_desc_error_msg" style="color:#F00;font-weight:800"></div>
						</div>
                    </div>
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Meta Description({{ $get_lang_name }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<textarea id="meta_description_{{ $get_lang_name }}" placeholder="Enter Meta Description In {{ $get_lang_name }}" name="meta_description_{{ $get_lang_name }}" class="form-control">{!! Input::old('meta_description_'.$get_lang_name) !!}</textarea>
							</div>
							<div id="meta_desc_fr_error_msg" style="color:#F00;font-weight:800"></div>
						</div>
                    </div>
					 @endforeach @endif
					
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_WEBSITE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE') }} @endif<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input type="url" class="form-control"  id="website" name="website" value="{!! Input::old('website') !!}"  placeholder="https://www.example.com" >
                        <div id="website_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input id="pac-input" class="form-control" type="text" placeholder="@if (Lang::has(Session::get('admin_lang_file').'.BACK_TYPE_YOUR_LOCATION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TYPE_YOUR_LOCATION') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TYPE_YOUR_LOCATION') }} @endif ( @if (Lang::has(Session::get('admin_lang_file').'.BACK_AUTO_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_AUTO_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_AUTO_COMPLETE') }} @endif )">
                        <div id="location_error_msg" style="color:#F00;font-weight:800"></div>
                    </div>
                    
					<div class="col-lg-4"></div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH_LOCATION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SEARCH_LOCATION') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH_LOCATION') }} @endif<span class="text-sub">*</span><br><span  style="color:#999">( @if (Lang::has(Session::get('admin_lang_file').'.BACK_DRAG_MARKER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DRAG_MARKER') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DRAG_MARKER') }} @endif & @if (Lang::has(Session::get('admin_lang_file').'.BACK_LONGITUDE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LONGITUDE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LONGITUDE') }} @endif )</span></label>

                    <div class="col-lg-4">
                        <div id="map_canvas" style="width:300px;height:250px;" ></div>
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LATITUDE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LATITUDE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LATITUDE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder="" id="latitude" name="latitude" readonly  value="{!! Input::old('latitude') !!}"  >
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LONGITUDE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LONGITUDE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LONGITUDE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder="" id="longitude" name="longitude" value="{!! Input::old('longitude') !!}"  readonly >
                    </div>
                </div>
                        </div>
						
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_STORES_IMAGE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORES_IMAGE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORES_IMAGE') }} @endif<span class="text-sub">*</span></label>
			
                    <div class="col-lg-4">
                          <span class="errortext red logo-size" style="color:red"><em>@if (Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE_SIZE_MUST_BE') }} @endif {{ $STORE_WIDTH }} x {{ $STORE_HEIGHT }} @if (Lang::has(Session::get('admin_lang_file').'.BACK_PIXELS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_PIXELS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PIXELS') }} @endif</em></span>
                       <input type="file" id="file" name="file" placeholder="Fruit ball">
                    </div>
                    <div id="image_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>
                        
                    </div>
					
                    	
				
					<div class="form-group">
                    <label class="control-label col-lg-3" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" >@if (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SUBMIT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }} @endif</a></button>
                     <button class="btn btn-danger btn-sm btn-grad" type="reset" ><a style="color:#ffffff">@if (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_RESET') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }} @endif</a></button>
                   
                    </div>
					  
                </div>
                
                </form>
                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
<script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
<script>
function email_exixts(){
var email_id = $('#email_id');
var passdata = 'email_id='+email_id.val();
        $.ajax({
			      type: 'get',
				  data: passdata,
				  url: 'check_mer_email_exist',
				  success: function(responseText){ 
				    if(responseText){  
					  $('#email_id_error_msg').html(responseText);		
                      email_id.focus();
                      $('#email_id').val("");	
			          return false;			   
				    }else{
               $('#email_id_error_msg').html('');
            }
				  }		
		});	
}
</script>
<script>
$( document ).ready(function() {

    var first_name 	        = $('#first_name');
    var last_name 	        = $('#last_name');
    var email_id 	        = $('#email_id');
    var select_mer_country 	= $('#select_mer_country');
    var select_mer_city 	= $('#select_mer_city');
    var phone_no 	        = $('#phone_no');
    var addreess_one 	    = $('#addreess_one');
    var address_two 	    = $('#address_two');
    var payment_account     = $('#payment_account');

    var store_name          = $('#store_name');
	//var store_name_fr       = $('#store_name_french');
    var store_pho           = $('#store_pho');
    var store_add_one       = $('#store_add_one');
	//var store_add_one_fr    = $('#store_add_one_fr');
    var store_add_two       = $('#store_add_two');
	//var store_add_two_fr    = $('#store_add_two_fr');
    var select_country      = $('#select_country');
    var select_city         = $('#select_city');
    var zip_code            = $('#zip_code');
    var meta_keyword        = $('#meta_keyword');
//	var meta_keyword_fr     = $('#meta_keyword_french');
	var meta_description    = $('#meta_description');
//	var meta_description_fr = $('#meta_description_french');
    var website             = $('#website');
    var latitude            = $('#latitude');
    var location            = $('#pac-input');
    var commission          = $('#commission');
    

    /*Validation*/

    /*Merchant Phone number*/
    $('#phone_no').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    phone_no.css('border', '1px solid red'); 
			$('#phone_no_error_msg').html('Numbers Only Allowed');
			phone_no.focus();
			return false;
		}else{			
            phone_no.css('border', ''); 
			$('#phone_no_error_msg').html('');	        
		}
    });
    /*Store Phone Number*/
    $('#store_pho').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    store_pho.css('border', '1px solid red'); 
			$('#store_pho_error_msg').html('Numbers Only Allowed');
			store_pho.focus();
			return false;
		}else{			
            store_pho.css('border', ''); 
			$('#store_pho_error_msg').html('');	        
		}
    });
   
    /*Store Zipcode*/
    $('#zip_code').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    zip_code.css('border', '1px solid red'); 
			$('#zip_code_error_msg').html('Numbers Only Allowed');
			zip_code.focus();
			return false;
		}else{			
            zip_code.css('border', ''); 
			$('#zip_code_error_msg').html('');	        
		}
    });

    /*Admin commission*/
    $('#commission').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    commission.css('border', '1px solid red'); 
			$('#commission_error_msg').html('Numbers Only Allowed');
			commission.focus();
			return false;
		}else{			
            commission.css('border', ''); 
			$('#commission_error_msg').html('');	        
		}
    });


	$('#submit').click(function() {

        /*Merchant First Name*/
		if($.trim(first_name.val()) == ""){
			first_name.css('border', '1px solid red'); 
			$('#first_name_error_msg').html('Please Enter First Name');
			first_name.focus();
			return false;
		}else{
			first_name.css('border', ''); 
			$('#first_name_error_msg').html('');
		}

        /*Merchant Last Name*/
		if($.trim(last_name.val()) == ""){
			last_name.css('border', '1px solid red'); 
			$('#last_name_error_msg').html('Please Enter Last Name');
			last_name.focus();
			return false;
		}else{
			last_name.css('border', ''); 
			$('#last_name_error_msg').html('');
		}

        /*Merchant Email Id*/
        //var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
		if($.trim(email_id.val()) == ""){
			email_id.css('border', '1px solid red'); 
			$('#email_id_error_msg').html('Please Enter Email id');
			email_id.focus();
			return false;
		}else{
			email_id.css('border', ''); 
			$('#email_id_error_msg').html('');
		}

      
        /*Country*/	
		if(select_mer_country.val() == 0){
			select_mer_country.css('border', '1px solid red'); 
			$('#country_error_msg').html('Please Select Merchant Country');
			select_mer_country.focus();
			return false;
		}else{
			select_mer_country.css('border', ''); 
			$('#country_error_msg').html('');
		}

        /*City*/	
		if(select_mer_city.val() == 0){
			select_mer_city.css('border', '1px solid red'); 
			$('#city_error_msg').html('Please Select Merchant City');
			select_mer_city.focus();
			return false;
		}else{
			select_mer_city.css('border', ''); 
			$('#city_error_msg').html('');
		}

        /*Phone number*/	
		if($.trim(phone_no.val()) == ""){
			phone_no.css('border', '1px solid red'); 
			$('#phone_no_error_msg').html('Please Select Merchant Phone number');
			phone_no.focus();
			return false;
		}else{
			phone_no.css('border', ''); 
			$('#phone_no_error_msg').html('');
		}

        /*Merchant Address one*/	
		if($.trim(addreess_one.val()) == ""){
			addreess_one.css('border', '1px solid red'); 
			$('#addreess_one_error_msg').html('Please Select Merchant Address one');
			addreess_one.focus();
			return false;
		}else{
			addreess_one.css('border', ''); 
			$('#addreess_one_error_msg').html('');
		}

        /*Merchant Address two*/	
		if($.trim(address_two.val()) == ""){
			address_two.css('border', '1px solid red'); 
			$('#addreess_two_error_msg').html('Please Select Merchant Address two');
			address_two.focus();
			return false;
		}else{
			address_two.css('border', ''); 
			$('#addreess_two_error_msg').html('');
		}

        

        /*Store Name*/	
		if($.trim(store_name.val()) == ""){
			store_name.css('border', '1px solid red'); 
			$('#store_name_error_msg').html('Please Enter Store Name In English');
			store_name.focus();
			return false;
		}else{
			store_name.css('border', ''); 
			$('#store_name_error_msg').html('');
		}
		/*Store Name in french*/	
		<?php 
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_name = $get_lang->lang_name;
		?>
		var store_name_dyn = $('#store_name_<?php echo $get_lang_name; ?>');
		if($.trim(store_name_dyn.val()) == ""){
			store_name_dyn.css('border', '1px solid red'); 
			$('#store_name_fr_error_msg').html('Please Enter Store Name In <?php echo $get_lang_name; ?>');
			store_name_dyn.focus();
			return false;
		}else{
			store_name_dyn.css('border', ''); 
			$('#store_name_fr_error_msg').html('');
		}
	<?php } } ?>
        /*Store Phone Number*/	
		if($.trim(store_pho.val()) == ""){
			store_pho.css('border', '1px solid red'); 
			$('#store_pho_error_msg').html('Please Enter Store Phone Number');
			store_pho.focus();
			return false;
		}else{
			store_pho.css('border', ''); 
			$('#store_pho_error_msg').html('');
		}

        /*Store Address one*/	
		if($.trim(store_add_one.val()) == ""){
			store_add_one.css('border', '1px solid red'); 
			$('#store_addr_one_error_msg').html('Please Enter Store Address one In English');
			store_add_one.focus();
			return false;
		}else{
			store_add_one.css('border', ''); 
			$('#store_addr_one_error_msg').html('');
		}
		/*Store Address one french*/	
		<?php 
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_name = $get_lang->lang_name;
		?>
		var store_add_one_dyn = $('#store_add_one_<?php echo $get_lang_name; ?>');
		if($.trim(store_add_one_dyn.val()) == ""){
			store_add_one_dyn.css('border', '1px solid red'); 
			$('#store_addr_one_fr_error_msg').html('Please Enter Store Address one In <?php echo $get_lang_name; ?>');
			store_add_one_dyn.focus();
			return false;
		}else{
			store_add_one_dyn.css('border', ''); 
			$('#store_addr_one_fr_error_msg').html('');
		}
		<?php } } ?>
        /*Store Address two*/	
		if($.trim(store_add_two.val()) == ""){
			store_add_two.css('border', '1px solid red'); 
			$('#store_addr_two_error_msg').html('Please Enter Store Address two In English');
			store_add_two.focus();
			return false;
		}else{
			store_add_two.css('border', ''); 
			$('#store_addr_two_error_msg').html('');
		}
		/*Store Address two french */	
		<?php 
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_name = $get_lang->lang_name;
		?>
		var store_add_two_dyn = $('#store_add_two_<?php echo $get_lang_name; ?>');
		if($.trim(store_add_two_dyn.val()) == ""){
			store_add_two_dyn.css('border', '1px solid red'); 
			$('#store_addr_two_fr_error_msg').html('Please Enter Store Address two In <?php echo $get_lang_name; ?>');
			store_add_two_dyn.focus();
			return false;
		}else{
			store_add_two_dyn.css('border', ''); 
			$('#store_addr_two_fr_error_msg').html('');
		}
		<?php } } ?>
        /*Store country*/	
		if((select_country.val()) == 0){
			select_country.css('border', '1px solid red'); 
			$('#store_country_error_msg').html('Please Enter Store Country');
			select_country.focus();
			return false;
		}else{
			select_country.css('border', ''); 
			$('#store_country_error_msg').html('');
		}

        /*Store city*/	
		if((select_city.val()) == 0){
			select_city.css('border', '1px solid red'); 
			$('#store_city_error_msg').html('Please Enter Store City');
			select_city.focus();
			return false;
		}else{
			select_city.css('border', ''); 
			$('#store_city_error_msg').html('');
		}

        /*Store Zipcode*/	
		if($.trim(zip_code.val()) == ""){
			zip_code.css('border', '1px solid red'); 
			$('#zip_code_error_msg').html('Please Enter Store Zipcode');
			zip_code.focus();
			return false;
		}else{
			zip_code.css('border', ''); 
			$('#zip_code_error_msg').html('');
		}

        /*Store meta_keyword	
		if($.trim(meta_keyword.val()) == ""){
			meta_keyword.css('border', '1px solid red'); 
			$('#meta_key_error_msg').html('Please Enter Store Meta Keywords In English');
			meta_keyword.focus();
			return false;
		}else{
			meta_keyword.css('border', ''); 
			$('#meta_key_error_msg').html('');
		}
		/*Store meta_keyword french */	
		<?php /**
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_name = $get_lang->lang_name;
		?>
		var meta_keyword_dyn = $('#meta_keyword_<?php echo $get_lang_name; ?>');
		if($.trim(meta_keyword_dyn.val()) == ""){
			meta_keyword_dyn.css('border', '1px solid red'); 
			$('#meta_key_fr_error_msg').html('Please Enter Store Meta Keywords In <?php echo $get_lang_name; ?>');
			meta_keyword_dyn.focus();
			return false;
		}else{
			meta_keyword_dyn.css('border', ''); 
			$('#meta_key_error_msg').html('');
		}
		<?php } } ?>
        /*Store meta_description	
		if($.trim(meta_description.val()) == ""){
			meta_description.css('border', '1px solid red'); 
			$('#meta_desc_error_msg').html('Please Enter Store Meta Description In English');
			meta_description.focus();
			return false;
		}else{
			meta_description.css('border', ''); 
			$('#meta_desc_error_msg').html('');
		}
		/*Store meta_description In french 
		<?php 
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_name = $get_lang->lang_name;
		?>
		var meta_description_dyn = $('#meta_description_<?php echo $get_lang_name; ?>');
		if($.trim(meta_description_dyn.val()) == ""){
			meta_description_dyn.css('border', '1px solid red'); 
			$('#meta_desc_fr_error_msg').html('Please Enter Store Meta Description In <?php echo $get_lang_name; ?>');
			meta_description_dyn.focus();
			return false;
		}else{
			meta_description_dyn.css('border', ''); 
			$('#meta_desc_fr_error_msg').html('');
		}
	<?php } } **/?>
        //Store website	
		if($.trim(website.val()) == ""){
			website.css('border', '1px solid red'); 
			$('#website_error_msg').html('Please Enter Store Website');
			website.focus();
			return false;
		}else{
			website.css('border', ''); 
			$('#website_error_msg').html('');
		}

        /*Store Location*/	
		if((latitude.val()) == ""){
			location.css('border', '1px solid red'); 
			$('#location_error_msg').html('Please Enter Store Location');
			location.focus();
			return false;
		}else{
			location.css('border', ''); 
			$('#location_error_msg').html('');
		}

        /*Store Commission*/	
		if($.trim(commission.val()) == ""){
			commission.css('border', '1px solid red'); 
			$('#commission_error_msg').html('Please Enter Commission');
			commission.focus();
			return false;
		}else{
			commission.css('border', ''); 
			$('#commission_error_msg').html('');
		}

    var file		 	 = $('#file');
	var fileExtension = ['jpeg', 'jpg', 'png'];
      	if(file.val() == ""){
 		    file.focus();
		    file.css('border', '1px solid red'); 	
            $('#image_error_msg').html('Please Choose Image');	
		    return false;
		}else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
		    file.focus();
		    file.css('border', '1px solid red'); 		
		    return false;
            $('#image_error_msg').html('Please Choose Valid Format');
		}else{
		    file.css('border', ''); 
            $('#image_error_msg').html('');				
		}
	});
});
</script>
     <script>
	/* window.onload = function() {
		select_city_ajax(<?php //echo Input::old('select_country'); ?>);
		select_mer_city_ajax(<?php //echo Input::old('select_mer_country'); ?>);
	}; */
	function select_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_city').html(responseText);					   
				   }
				}		
			});		
	}
	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}
	</script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
    <?php $city_det = DB::table('nm_emailsetting')->first();?>
    <script type="text/javascript">
    var map;

    function initialize() {


 		var myLatlng = new google.maps.LatLng(<?php echo $city_det->es_latitude; ?>,<?php echo $city_det->es_longitude; ?>);
        var mapOptions = {

           zoom: 10,
                center: myLatlng,
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	 		 var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				draggable:true,    
            });	
			 
		google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
			});
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
 
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
 
            var place = autocomplete.getPlace();
	        var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            $('#latitude').val(latitude);
			$('#longitude').val(longitude);
                
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
				var myLatlng = place.geometry.location;	
				var latlng = new google.maps.LatLng(latitude, longitude);
                marker.setPosition(latlng);

			google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
			});
            } else {
                map.setCenter(place.geometry.location);	
				
                map.setZoom(17);
            }
        });



    }


    google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- END GLOBAL SCRIPTS -->   
     <!---Right Click Block Code---->
<script language="javascript">
document.onmousedown=disableclick;
status="Cannot Access for this mode";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;    
   }
}
</script>

	<script type="text/javascript">
		   $.ajaxSetup({
			   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		   });
	</script>	


<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});

$('#last_name,#store_name,#first_name').bind('keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }
);


</script>
</body>
     <!-- END BODY -->
</html>
