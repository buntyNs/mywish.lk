<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | @if (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_ACCOUNTS') }} @endif        </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
	
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     @php
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp
      @if(count($favi)>0)  
      @foreach($favi as $fav) @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
@endif	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
 <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
	<link href="{{ url('')}}/public/assets/css/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

   <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >@if (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_HOME') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME') }} @endif</a></li>
                                <li class="active"><a > @if (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_ACCOUNTS') }} @endif           </a></li>
                            </ul>
                    </div>
                </div>
				  <center><div class="cal-search-filter">
		 <form  action="{!!action('MerchantController@manage_merchant')!!}" method="POST">
							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
							 <div class="row">
							 <br>
							 
							 
							   <div class="col-sm-4 col-md-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">@if (Lang::has(Session::get('admin_lang_file').'.BACK_FROM_DATE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_FROM_DATE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_FROM_DATE') }} @endif</div>
							<div class="col-sm-6 place-size">
 <span class="icon-calendar cale-icon"></span>
 {{ Form::text('from_date',$from_date,array('id'=>'datepicker-8','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly')) }}
							 <!-- <input type="text" name="from_date" placeholder="DD/MM/YYYY"   class="form-control" id="datepicker-8" value="{{$from_date}}" required readonly> -->
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-4 col-md-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">@if (Lang::has(Session::get('admin_lang_file').'.BACK_TO_DATE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TO_DATE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TO_DATE') }} @endif</div>
							<div class="col-sm-6 place-size">
 <span class="icon-calendar cale-icon"></span>
{{ Form::text('to_date',$to_date,array('id'=>'datepicker-9','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly')) }}
							 <!-- <input type="text" name="to_date" placeholder="DD/MM/YYYY"  id="datepicker-9" class="form-control" value="{{$to_date}}" required readonly> -->
							 
							  </div>
							  </div>
							   </div>
							   
							   <div class="form-group">
							   <div class="col-sm-2">
							    <input type="submit" name="submit" class="btn btn-block btn-success" value="@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SEARCH') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH') }} @endif">
							   </div>
                               <div class="col-sm-2">
								<a href="{{ url('').'/manage_merchant' }}"><button type="button" name="reset" class="btn btn-block btn-info">@if (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_RESET') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }} @endif</button></a>
							 </div>
							</div>
							
							 </form></div>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_ACCOUNTS') }} @endif           </h5>
            
        </header>
         @if (Session::has('result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('result') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif
		<div style="display: none;" class="la-alert rec-select alert-success alert-dismissable">Select atleast one Merchant!  
         <button type="button" class="close closeAlert"  aria-hidden="true">×</button></div>
         <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
         <button type="button" class="close closeAlert"  aria-hidden="true">×</button></div>
		 <div style="display: none;" class="la-alert rec-update alert-success alert-dismissable">Record Updated Successfully!
         <button type="button" class="close closeAlert"  aria-hidden="true">×</button></div>

    <div class="manage-filter"><span class="squaredFour">
    	{{ Form::checkbox('chk[]','',null,array('id'=>'check_all','onchange'=>'checkAll(this)'))}}
     <!--  <input  type="checkbox" name="chk[]" onchange="checkAll(this)" id="check_all"/> -->
     {{ Form::label('check_all','Check all')}}
      <!-- <label for="check_all">Check all</label> -->
    </span> &nbsp;
       {{ Form::button('Block',array('id'=>'Block_value','class'=>'btn btn-primary'))}}
     <!-- <input class="btn btn-primary" type="button" id="Block_value"  value="Block" /> -->
       {{ Form::button('Un Block',array('id'=>'UNBlock_value','class'=>'btn btn-warning'))}}
   <!--  <input class="btn btn-warning" type="button" id="UNBlock_value"  value="Un Block" /> -->
	
        <div id="div-1" class="accordion-body collapse in body" style="clear: both;">
   
        		<div class="table-responsive panel_marg_clr ppd">
		   <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
										 <th aria-label="S.No: activate to sort column ascending" style="width: 61px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending"></th>
										<th aria-label="S.No: activate to sort column ascending" style="width: 61px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SNO') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO') }} @endif</th>
										<th aria-label="Product Name: activate to sort column ascending" style="width: 69px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_NAME') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME') }} @endif & @if (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID') }} @endif</th>
										
										<th aria-label="Store Name: activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_NAME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORE_NAME') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_NAME') }} @endif</th>
										<th aria-label="Original Price($): activate to sort column ascending" style="width: 75px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_CITY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY') }} @endif</th>
										<th aria-label=" Product Image : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_BRANCH')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_BRANCH') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_BRANCH') }} @endif</th>
										<th aria-label="Send Mail: activate to sort column ascending" style="width: 64px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_BRANCH')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MANAGE_BRANCH') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_BRANCH') }} @endif</th>
										<th aria-label="Send Mail: activate to sort column ascending" style="width: 64px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_CREATED_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_CREATED_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_CREATED_MERCHANT') }} @endif</th>
										<th aria-label="Actions: activate to sort column ascending" style="width: 73px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_EDIT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT') }} @endif</th>
										<th aria-label="Hot deals: activate to sort column ascending" style="width: 65px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_BLOCK') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK') }} @endif / @if (Lang::has(Session::get('admin_lang_file').'.BACK_UNBLOCK_MERCHANTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_UNBLOCK_MERCHANTS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_UNBLOCK_MERCHANTS') }} @endif </th>
	                                   <th aria-label="Hot deals: activate to sort column ascending" style="width: 65px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"> @if (Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_BLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK') }} @endif / @if (Lang::has(Session::get('admin_lang_file').'.BACK_UNBLOCK_PRODUCTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_UNBLOCK_PRODUCTS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_UNBLOCK_PRODUCTS') }} @endif </th>
									  <th aria-label="Hot deals: activate to sort column ascending" style="width: 65px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LOGIN_TYPE')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_LOGIN_TYPE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LOGIN_TYPE') }} @endif</th>
									   
										</tr>
                                    </thead>
                                    <tbody>
                                  @php $i = 1; @endphp
								  
				  @if(isset($_POST['submit']))
					
						@if(count($merchantrep)>0)
						@foreach($merchantrep as $merchant_details)
				
				
				@php $logintype=""; @endphp
				 @if($merchant_details->mer_logintype==1)
				

				@php $logintype = ((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_MERCHANT')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ADMIN_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_MERCHANT'); @endphp
				
				
				@elseif($merchant_details->mer_logintype==2)
				

			@php	$logintype = ((Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_MERCHANT')!= ''))? trans(Session::get('admin_lang_file').'.BACK_WEBSITE_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_MERCHANT'); @endphp
				
				
				@elseif($merchant_details->mer_logintype==3)
				
			@php	$logintype = ((Lang::has(Session::get('admin_lang_file').'.BACK_FACEBOOK_MERCHANT')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FACEBOOK_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FACEBOOK_MERCHANT'); @endphp
				
				@endif 


				

										<tr class="gradeA odd">
										
                                            <td class="text-center">{{ $i }}</td>
                                            <td class="text-center">{{ $merchant_details->mer_fname }}<br>{{ $merchant_details->mer_email }}</td>
                                            
                                            <td class="text-center">{{ $merchant_details->stor_name }}</td>
                                            <td class="text-center">{{ $merchant_details->ci_name }}</td>
											<td class="text-center"><a href="{{ url('add_store/'.$merchant_details->mer_id) }}" data-tooltip="Add Branches"><i class="icon-plus-sign icon-2x"></i></a></td>
											<td class="text-center"><a href="{{ url('manage_store/'.$merchant_details->mer_id) }}" data-tooltip="Branches"><i class="icon-shopping-cart icon-2x"></i><span style="color:#2574c4;padding-left:5px;">(<?php echo $store_count[$merchant_details->mer_id]; ?>) @if (Lang::has(Session::get('admin_lang_file').'.BACK_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORES') }} @endif </span></a></td>
											<td>{{ $merchant_details->created_date }}</td>
                                            <td class="text-center"><a href="{{ url('edit_merchant/'.$merchant_details->mer_id) }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_EDIT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT') }} @endif"><i class="icon icon-edit icon-2x"></i></a></td>                                            
                                            <td class="text-center ">
                                          <?php  //if($merchant_is_or_not_in_deals[$merchant_details->mer_id] == 0 && $merchant_is_or_not_in_product[$merchant_details->mer_id] == 0 && $merchant_is_or_not_in_auction[$merchant_details->mer_id] == 0 )
											//{ 
											?> 
                                            @if($merchant_details->mer_staus == 1) 
                                            <a href="{{ url('block_merchant/'.$merchant_details->mer_id."/0") }}"><i class="icon icon-ok icon-2x "></i></a>  
                                            @elseif($merchant_details->mer_staus == 0) 
                                            <a href="{{ url('block_merchant/'.$merchant_details->mer_id."/1") }}"><i class="icon icon-ban-circle icon-2x icon-me"></i></a> @endif 
                                            <?php  //} else { echo 'Merchant In Use'; } ?>
                                            </td>
											<td>
											@if($merchant_details->mer_pro_status == 1)  
                                            <a href="<?php echo url('block_merchant_product/'.$merchant_details->mer_id."/0"); ?>" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_UNBLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_UNBLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_UNBLOCK') }} @endif"><i class="icon icon-ok icon-2x "></i></a> @elseif($merchant_details->mer_pro_status == 0)  
                                            <a href="<?php echo url('block_merchant_product/'.$merchant_details->mer_id."/1"); ?>" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_BLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK') }} @endif"><i class="icon icon-ban-circle icon-2x icon-me"></i></a> 
                                            @endif
                                            </td>

                                            <td>
                                               
                                            @if($merchant_details->mer_pro_status == 1)  
                                            <a href="<?php echo url('block_merchant_product/'.$merchant_details->mer_id."/0"); ?>"><i class="icon icon-ok icon-2x "></i></a> @elseif($merchant_details->mer_pro_status == 0) 
                                            <a href="<?php echo url('block_merchant_product/'.$merchant_details->mer_id."/1"); ?>"><i class="icon icon-ban-circle icon-2x icon-me"></i></a> 
                                            @endif
                                            </td>
											
											<td class="center">{{ $logintype }}</td>
                                     
                                        </tr>
										@php $i++; @endphp @endforeach @endif
			
@else
  @if(count($merchant_return)>0)
	@foreach($merchant_return as $merchant_details)
	
				 
				@php $logintype=""; @endphp
				@if($merchant_details->mer_logintype==1)
				

			@php	$logintype = ((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_MERCHANT')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ADMIN_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_MERCHANT'); @endphp
				
				
				@elseif($merchant_details->mer_logintype==2)
				

			@php	$logintype = ((Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_MERCHANT')!= ''))? trans(Session::get('admin_lang_file').'.BACK_WEBSITE_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_MERCHANT'); @endphp
				
				
				@elseif($merchant_details->mer_logintype==3)
				
			@php	$logintype = ((Lang::has(Session::get('admin_lang_file').'.BACK_FACEBOOK_MERCHANT')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FACEBOOK_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FACEBOOK_MERCHANT'); @endphp
				
				@endif


				
										<tr class="gradeA odd">
										  <td  class="text-center">
                   <input type="checkbox" class="table_id" value="{{ $merchant_details->mer_id }}" name="chk[]">
                </td>
                                            <td class="text-center">{{ $i }}</td>
                                            <td class="text-center">{{ $merchant_details->mer_fname }}<br>{{ $merchant_details->mer_email }}</td>
                                            <td class="text-center">{{ $merchant_details->stor_name }}</td>
                                            <td class="text-center">{{ $merchant_details->ci_name }}</td>
                                            <td class="text-center"><a href="{{ url('add_store/'.$merchant_details->mer_id) }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_BRANCHES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_BRANCHES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_BRANCHES') }} @endif"><i class="icon-plus-sign icon-2x"></i></a></td>
                                            <td class="text-center"><a href="{{ url('manage_store/'.$merchant_details->mer_id) }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_BRANCHES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_BRANCHES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_BRANCHES') }} @endif"><i class="icon-shopping-cart icon-2x"></i><span style="color:#2574c4;padding-left:5px;">({{ $store_count[$merchant_details->mer_id] }}) @if (Lang::has(Session::get('admin_lang_file').'.BACK_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORES') }} @endif </span></a></td>
											<td>{{ $merchant_details->created_date }}</td>
                                            <td class="text-center"><a href="{{ url('edit_merchant/'.$merchant_details->mer_id) }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_EDIT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT') }} @endif "><i class="icon icon-edit icon-2x"></i></a></td>                                            
                                            <td class="text-center ">
                                          <?php  //if($merchant_is_or_not_in_deals[$merchant_details->mer_id] == 0 && $merchant_is_or_not_in_product[$merchant_details->mer_id] == 0 && $merchant_is_or_not_in_auction[$merchant_details->mer_id] == 0 )
											//{ 
											?> 
                                            @if($merchant_details->mer_staus == 1) 
                                            <a href="{{ url('block_merchant/'.$merchant_details->mer_id."/0") }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_BLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK') }} @endif"><i class="icon icon-ok icon-2x "></i></a> 
                                           
                                            @elseif($merchant_details->mer_staus == 0)  
                                            <a href="{{ url('block_merchant/'.$merchant_details->mer_id."/1") }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_UNBLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_UNBLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_UNBLOCK') }} @endif"><i class="icon icon-ban-circle icon-2x icon-me"></i></a> 
                                            @endif
                                            <?php  //} else { echo 'Merchant In Use'; } ?>
                                            </td>
                                            <td>
                                               
                                            @if($merchant_details->mer_pro_status == 1) 
                                            <a href="{{ url('block_merchant_product/'.$merchant_details->mer_id."/0") }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_BLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK') }} @endif"><i class="icon icon-ok icon-2x "></i></a>  @elseif($merchant_details->mer_pro_status == 0) 
                                            <a href="{{ url('block_merchant_product/'.$merchant_details->mer_id."/1") }}" data-tooltip="@if (Lang::has(Session::get('admin_lang_file').'.BACK_UNBLOCK')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_UNBLOCK') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_UNBLOCK') }} @endif"><i class="icon icon-ban-circle icon-2x icon-me"></i></a> 
                                           @endif
                                            </td>
											
											<td class="center">{{ $logintype }}</td>
											
                                        </tr>
										@php $i++; @endphp @endforeach @endif
									  @endif 			
										
										</tbody>
                                </table></div>

                                
        </div>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
	
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
	
	<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

    <!-- END GLOBAL SCRIPTS -->   
   <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<!-- <script src="{{url('')}}/public/assets/js/jquery-ui.js"></script> -->
		
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
         /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/



      </script>
	  <script type="text/javascript">
	  //Check all checked box
		 function checkAll(ele) {
  
  
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
 
  //To block multiple checked
  $(function(){
    
      $('#Block_value').click(function(){
         $(".rec-select").css({"display" : "none"});
        var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });  console.log(val);


         if(val=='')
         {

         $(".rec-select").css({"display" : "block"});
     
         return;
         }


        $.ajax({

          type:'GET',
          url :"<?php echo url("block_status_merchant_submit"); ?>",
          data:{val:val},

          success:function(data,success){
			 
             
            
            if(data==0){
              $(".rec-update").css("display", "block");
                window.setTimeout(function(){location.reload()},1000)
            
                       }
            else if(data==1){
               $(".rec-update").css("display", "block");
                  window.setTimeout(function(){location.reload()},1000)
             
                           }
          }
        }); });

    });
	
//To unblock multiple checked

   $(function(){

   
    
      $('#UNBlock_value').click(function(){
          $(".rec-select").css("display", "none");
        var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });  console.log(val);

         if(val=='')
         {
          //location.reload();
        $(".rec-select").css("display", "block");
          return;
         }


        $.ajax({


          type:'GET',
          url :"<?php echo url("unblock_status_merchant_submit"); ?>",
          data:{val:val},

          success:function(data,success){
            if(data==0){
            $(".rec-update").css("display", "block");
                window.setTimeout(function(){location.reload()},1000)
                       }
            else if(data==1){
              $(".rec-update").css("display", "block");
      
              //location.reload();
               window.setTimeout(function(){location.reload()},1000)
                           }
          }
        }); });	
		
		 });
	 $(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
		
 </script>
</body>
     <!-- END BODY -->
</html>
