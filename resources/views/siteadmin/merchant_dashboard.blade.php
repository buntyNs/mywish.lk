<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | @if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_DASHBOARD')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_DASHBOARD') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_DASHBOARD') }} @endif </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
	<link rel="stylesheet" href="{{ url('') }}/public/assets/css/plan.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
     @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp 
     @if(count($favi)>0)  @foreach($favi as $fav) @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif	
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="{{ url('') }}/public/assets/css/datepicker.css" rel="stylesheet">	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/timeline/timeline.css" />

 <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->


		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >@if (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_HOME') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME') }} @endif</a></li>
                                <li class="active"><a>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANTS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANTS') }} @endif</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-dashboard"></i></div>
            <h5>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_DASHBOARD')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_DASHBOARD') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_DASHBOARD') }} @endif</h5>
            
        </header>
<br>
            	<div class="row" style="padding-left:15px;">
                    <div class="col-lg-12 mer-das-button">
                 <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" href="{{ url('') }}"  target="_blank" >@if (Lang::has(Session::get('admin_lang_file').'.BACK_GO_TO_LIVE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_GO_TO_LIVE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_GO_TO_LIVE') }} @endif</a></button>
                <?php /* <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="<?php echo url('product_all_orders'); ?>"><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_TRANSACTION')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_TRANSACTION');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_TRANSACTION');} ?></a></button>
*/ ?>
                 <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="{{ url('product_all_orders') }}">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_PRODUCT_TRANSACTION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_PRODUCT_TRANSACTION') }}  @else {{
                  trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_PRODUCT_TRANSACTION') }} @endif</a></button>

                 <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="{{ url('cod_all_orders') }}">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_PRODUCT_COD_TRANSACTION')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_PRODUCT_COD_TRANSACTION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_PRODUCT_COD_TRANSACTION') }} @endif</a></button>
                  
                   <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="{{ url('deals_all_orders') }}">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_DEAL_TRANSACTION')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_DEAL_TRANSACTION') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_DEAL_TRANSACTION') }} @endif</a></button>

                     <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="{{ url('dealscod_all_orders') }}">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_DEAL_COD_TRANSACTION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_DEAL_COD_TRANSACTION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_DEAL_COD_TRANSACTION') }} @endif</a></button>

                     <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="{{ url('product_payu_all_orders') }}">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_PRODUCT_PAYU_TRANSACTION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_PRODUCT_PAYU_TRANSACTION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_PRODUCT_PAYU_TRANSACTION') }} @endif</a></button>

                     <button class="btn btn-success btn-sm btn-grad"><a style="color:#fff" target="_blank" href="{{ url('deals_payu_all_orders') }}">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_DEAL_PAYU_TRANSACTION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_DEAL_PAYU_TRANSACTION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_DEAL_PAYU_TRANSACTION') }} @endif</a></button>

                </div>
                </div>
                
            
        
        
      			  <div class="row">
                <div class="col-lg-6" style="padding:30px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>@if (Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_MERCHANTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TOTAL_MERCHANTS')}} @else {{
                             trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_MERCHANTS') }} @endif</strong> 
                            <a href="#"><i class="icon icon-align-justify pull-right"></i></a>
                        </div>
                        <div class="panel-body">
                            @if(($website_merchant+$admin_merchant)>0)
                           <div id="chart6" style="margin-top:20px; margin-left:20px; width:450px; height:450px;"></div>
                          <table width="70%" border="0">
                              <tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active">@if (Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_WEBSITE_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_MERCHANT') }} @endif</label>
                                    <span class="label label-danger">{{ $website_merchant }}</span>
                                </td>
                                <td style="background:#eaa228">
                                    <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADMIN_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_MERCHANT') }} @endif</label>
                                    <span class="label label-danger">{{ $admin_merchant }}</span>
                                </td>
                                                              
                              </tr>
                            </table>
                            @else
                                No Details Found.
                            @endif
                        </div>
                       
                        	

                        
                    </div>
                </div>
                <div class="col-lg-5 " style="padding-top:30px"> 
                    <div class="panel panel-default">
                        <div class="panel-heading">	
                            <strong>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_DETAILS') }}  @else {{
                             trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_DETAILS') }} @endif</strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive panel_marg_clr ppd">
                                <table class="" width="100%">
                                    
                                    <tbody>
                                        <tr>
 
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_MERCHANTS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TOTAL_MERCHANTS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_MERCHANTS') }} @endif </td>
                                            <td>{{  $merchant_count }}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$merchant_count.'%"'; ?>;> 
		  <span class="sr-only">60% @if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE') }} @endif</span>
		</div>
	      </div></td>
                                            
                                            
                                        </tr>
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ACTIVE_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE_MERCHANT') }} @endif</td>
                                            <td>{{ $active_merchant }}</td>
                                            
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$active_merchant.'%"'; ?>;> 
		  <span class="sr-only">60% @if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE') }} @endif</span>
		</div>
	      </div></td>
										 </tr>
                                        
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_INACTIVE_MERCHANT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_INACTIVE_MERCHANT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_INACTIVE_MERCHANT') }} @endif</td>
                                            <td>{{ $inactive_merchant }}</td>
                                            
                                        </tr>
                                         <tr>
                                             <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"  <?php echo 'style="width:'.$inactive_merchant.'%"'; ?>;>
		  <span class="sr-only">40% @if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE') }} @endif (@if (Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SUCCESS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS') }} @endif )</span>
		</div>
	      </div></td>  
 
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-5"  style="padding-left:12px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_REG_DETAILS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_MERCHANT_REG_DETAILS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_REG_DETAILS') }} @endif</strong>
                        </div>
                        <div class="panel-body">
                         <div class="table-responsive panel_marg_clr ppd">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_REG_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_REG_DETAILS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_REG_DETAILS') }} @endif</th>
                                            <th>@if (Lang::has(Session::get('admin_lang_file').'.BACK_COUNT')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_COUNT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNT') }} @endif</th>
					</tr>
                                    </thead>
                                    <tbody>
										<tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_TODAY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_TODAY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TODAY') }} @endif</td>
                                            <td>{{ $merchanttoday[0]->count }}</td>
                                           
                                            
                                        </tr>
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LAST') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST') }} @endif 7 @if (Lang::has(Session::get('admin_lang_file').'.BACK_DAYS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DAYS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS') }} @endif</td>
                                            <td>{{ ($merchant7days[0]->count) }}</td>
                                           
                                            
                                        </tr>
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_LAST') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST') }} @endif 30 @if (Lang::has(Session::get('admin_lang_file').'.BACK_DAYS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DAYS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS') }} @endif</td>
                                            <td>{{ ($merchant30days[0]->count) }}</td>
                                           
                                            
                                        </tr>
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_LAST') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST') }} @endif 12 @if (Lang::has(Session::get('admin_lang_file').'.BACK_MONTHS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_MONTHS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MONTHS') }} @endif </td>
                                            <td>{{ ($merchant12mnth[0]->count) }}</td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    </div>
</div>
   
    </div>
                    
					
					<!--store-->
					
					 <div class="row">
<div class="col-lg-12">
    <div class="box dark">
      
        
      			  <div class="row">
                <div class="col-lg-6" style="padding:30px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>@if (Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TOTAL_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_STORES') }} @endif</strong> 
                            <a href="#"><i class="icon icon-align-justify pull-right"></i></a>
                        </div>
                        <div class="panel-body">
                            @if(($admin_stores_cnt+$merchant_stores_cnt)>0)
                           <div id="chart7" style="margin-top:20px; width:450px; height:450px;"></div>
                          <table width="70%" border="0">
                              <tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_ADD_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADMIN_ADD_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_ADD_STORES') }} @endif</label>
                                    <span class="label label-danger">{{ $admin_stores_cnt }}</span>
                                </td>
                                <td style="background:#eaa228">
                                    <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_ADD_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_ADD_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_ADD_STORES') }} @endif</label>
                                    <span class="label label-danger">{{ $merchant_stores_cnt }}</span>
                                </td>
                                                              
                              </tr>
                            </table>
                            @else
                                No Details Found.
                            @endif
                        </div>
                       
                        	

                        
                    </div>
                </div>
                <div class="col-lg-5 " style="padding-top:30px"> 
                    <div class="panel panel-default">
                        <div class="panel-heading">	
                            <strong>@if (Lang::has(Session::get('admin_lang_file').'.BACK_STORES_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORES_DETAILS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORES_DETAILS') }} @endif</strong>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive panel_marg_clr ppd">
                                <table class="" width="100%">
                                    
                                    <tbody>
                                       
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TOTAL_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_STORES') }} @endif</td>
                                            <td>{{ $store_cnt }}</td>
                                            
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$store_cnt.'%"'; ?>;> 
		  <span class="sr-only">60% @if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE') }} @endif</span>
		</div>
	      </div></td>
										 </tr>
                                        
                                        <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_ADD_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADMIN_ADD_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_ADD_STORES') }} @endif</td>
                                            <td>{{ $admin_stores_cnt }}</td>
                                            
                                        </tr>
                                         <tr>
                                             <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"  <?php echo 'style="width:'.$admin_stores_cnt.'%"'; ?>;>
		  <span class="sr-only">40% @if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE') }} @endif ( @if (Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SUCCESS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS') }} @endif )</span>
		</div>
	      </div></td>  
 <tr>
                                            <td>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_ADD_STORES')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_ADD_STORES') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_ADD_STORES') }} @endif</td>
                                            <td>{{ $merchant_stores_cnt }}</td>
                                         
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$merchant_stores_cnt.'%"'; ?>;> 
		  <span class="sr-only">60% @if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_COMPLETE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE') }} @endif</span>
		</div>
	      </div></td>
                                            
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
    </div>
</div>
   
    </div>
	
	<!--store end -->
                    
                    <div class="row">
                    	<div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <strong>@if (Lang::has(Session::get('admin_lang_file').'.BACK_STATISTICS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_STATISTICS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STATISTICS') }} @endif</strong> 
                            </div>
                             
                            <div class="panel-body">
                              <ul class="nav nav-pills">
                                <li class="active"><a  data-toggle="tab">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_ONE_YEAR_TRANS') }} @endif</a>
                                </li>
                              </ul>
                            <br>
                            <div class="tab-content">
                                 @if($get_mer_register_count!='0,0,0,0,0,0,0,0,0,0,0,0')
                              <div id="chart1" style="margin-top:20px; margin-left:20px; width:950px; height:470px;"></div>
                                 @else
                                    No Transaction Found.
                                  @endif
                                <!--<div class="tab-pane fade" id="profile-pills">
                                    <h4>Profile Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>-->
                                <!--<div class="tab-pane fade" id="messages-pills">
                                    <h4>Messages Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>-->
                                
                            </div>
                            
                            
			
			
		</div>
		</div>
                             
		
                    </div>
                    </div>
                    </div>
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
   
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   {!! $adminfooter !!}
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
  
    <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->  
    @if(($website_merchant+ $admin_merchant)>0) 
    <script>
	$(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[{{ $website_merchant }},{{ $admin_merchant }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } });
		});
	</script>
	@endif
    @if(($admin_stores_cnt+ $merchant_stores_cnt)>0)
	 <script>
	$(document).ready(function(){
		
	plot6 = $.jqplot('chart7', [[{{ $admin_stores_cnt }},{{ $merchant_stores_cnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } });
		});
	</script>
	@endif
    @if($get_mer_register_count!='0,0,0,0,0,0,0,0,0,0,0,0')
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$get_mer_register_count. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_JANUARY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_FEBRUARY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_MARCH');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_APRIL');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_MAY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY');} ?>','<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_JUNE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_JULY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_AUGUST');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_OCTOBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_NOVEMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_DECEMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER');} ?>'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
	
	@endif
		<script type="text/javascript">
		   $.ajaxSetup({
			   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		   });
	</script>	
    

    <!--- Chart Plugins -->
      <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    
    <!-- -->
    


<script src="<?php echo url(''); ?>/public/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="<?php echo url(''); ?>/public/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="<?php echo url(''); ?>/public/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>

        
    
	<script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="<?php echo url(''); ?>/public/assets/js/bar_chart.js"></script>
        
     
</body>
     <!-- END BODY -->
</html>
