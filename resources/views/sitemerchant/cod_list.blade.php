<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')}} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY'): trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/success.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{ $fav->imgs_name }} ">
 @endif
     <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


           <!-- HEADER SECTION -->
         {!! $adminheader!!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
         {!! $adminleftmenus!!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="{{ url('sitemerchant_dashboard')}}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ? trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')  : trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ? trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')  : trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
           {{ Form::open(['class' => 'form-horizontal'])}}

              
<div class="panel_marg_clr ppd">
       <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                       <tr role="row">
                                        <th aria-sort="ascending" aria-label="S.No: activate to sort column ascending" style="width: 99px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>
                                        <th aria-label="Customers: activate to sort column ascending" style="width: 111px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_PRODUCTS_NAME') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS_NAME') }}</th>
                                        <th aria-label="Product Title: activate to sort column ascending" style="width: 219px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NAME') : trans($MER_OUR_LANGUAGE.'.MER_NAME') }}</th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ? trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }}</th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') ? trans(Session::get('mer_lang_file').'.MER_DATE') : trans($MER_OUR_LANGUAGE.'.MER_DATE') }}</th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }}</th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DETAILS')!= '') ? trans(Session::get('mer_lang_file').'.MER_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_DETAILS') }}</th>
                                       <!-- <th aria-label="Transaction Date: activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php //if (Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS');} ?></th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php  $i=1; ?>
								   	@foreach($coddetail as $cod)
									
                  @php 
									 $merid  = Session::get('merchantid');
									@endphp
										
								<tr class="gradeA odd">
                                            <td class="sorting_1">{{ $i }}</td>
											<td class="       "><ol>
											<?php $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $cod->cod_transaction_id)
									  ->where('nm_ordercod.cod_merchant_id', $merid)
										->where('nm_ordercod.cod_order_type', '=',1)->get();
                        ?>
												@foreach($product_titles as $product_datavalues)
												
											<?php
												echo '<li style="list-style-type: upper-roman; text-align:left;">'. $product_datavalues->pro_title.'.</li>';  ?>
																							
											


											
										@endforeach
                                           </ol> </td>
                                            <td class="       ">{{ $cod->cus_name }}</td>
                                            <td class="center">{{ $cod->cus_email}}</td>
                                            <td class="center">{{ $cod->cod_date }}</td>
                                            <td class="center">{{ $cod->ship_address1}}</td>
											   
                                            <td class="center"><a class="btn btn-success" data-target="<?php echo '#uiModal'.$i;?>" data-toggle="modal">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS')}}</a></td>
                                              
									  </tr>
                                      
                                     <?php   $i=$i+1; ?>
                                     @endforeach
                                        
                                        </tbody>
                                </table></div>

                                
        </div>
           
         {{ Form::close() }}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    <div class="modal fade in" id="formModal"  style="display:none;">
     <div class="modal-dialog">
                                    <div class="modal-content">
                                        
                                        <div class="modal-body">
                                          {{ Form::open(['role' => 'form'])}}
                                           <form role="form">
                                        <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ? trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }}</label>
                                            <input class="form-control">
                                            <p class="help-block">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE') : trans($MER_OUR_LANGUAGE.'.MER_EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE') }}.</p>
                                        </div>
										 <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ENTER_COUPEN_CODE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_COUPEN_CODE') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_COUPEN_CODE') }}</label>
                                            <input class="form-control">
                                           
                                        </div>
                                        <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }}&nbsp;:&nbsp;</label>
                                          asdsa,,California,Canadian
                                        </div>
                                        <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MESSAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MESSAGE'): trans($MER_OUR_LANGUAGE.'.MER_MESSAGE')}}</label>
                                          <textarea id="text4" class="form-control"></textarea>
                                        </div>
                                        
                                       
                                    {{ Form::close() }}
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLOSE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLOSE') : trans($MER_OUR_LANGUAGE.'.MER_CLOSE') }}</button>
                                            <button class="btn btn-success" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEND')!= '') ? trans(Session::get('mer_lang_file').'.MER_SEND') : trans($MER_OUR_LANGUAGE.'.MER_SEND') }}</button>
                                        </div>
                                    </div>
                                </div>  </div>
     <!--END MAIN WRAPPER -->
  <?php  $i=1; ?>
@foreach($coddetail as $coddetails)

<?php 
$status=$coddetails->cod_status; ?>
@if($coddetails->cod_status==1)
@php
$orderstatus="success";
@endphp
@elseif($coddetails->cod_status==2) 
@php
$orderstatus="completed";
@endphp
@elseif($coddetails->cod_status==3) 
@php
$orderstatus="Hold";
@endphp
@elseif($coddetails->cod_status==4) 
@php
$orderstatus="failed";
@endphp
@endif

@php 
$all_tax_amt  =0 ;
$all_shipping_amt =0;
$item_total=0;  $coupon_amount=0;
$subtotal =0;
$wallet_amt=0;



    $tax_amt = (($coddetails->cod_amt * $coddetails->cod_tax)/100);
    $all_tax_amt+=   (($coddetails->cod_amt * $coddetails->cod_tax)/100);

    $cod_shipping_amt = $coddetails->cod_shipping_amt;
    $all_shipping_amt+= $coddetails->cod_shipping_amt;
   
   
@endphp
    @if($coddetails->coupon_amount !=0)
        @php $coupon_value = $coddetails->coupon_amount; @endphp
        
    @else
         @php $coupon_value = 0; @endphp
    @endif

   @php

    $wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$coddetails->cod_transaction_id)->get();
   @endphp

    @if(count($wallet)!=0)
        @php $wallet_amt = $wallet[0]->wallet_used; @endphp
    @else
         @php $wallet_amt = 0;  @endphp
    @endif
@php    
    $item_total = $coddetails->cod_amt;
    $sub_total = ($item_total+$tax_amt+$cod_shipping_amt);
    $sub_total = ($sub_total - $coupon_value);
    $grand_total = ($sub_total - $wallet_amt);
@endphp




<div  id="{{ 'uiModal'.$i }}" class="modal fade in invoice-top" style="display:none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">
                                                <div class="col-lg-4"><img src="{{ $SITE_LOGO }}" alt="{{ (Lang::has(Session::get('mer_lang_file').'.LOGO')!= '') ?  trans(Session::get('mer_lang_file').'.LOGO') : trans($MER_OUR_LANGUAGE.'.LOGO') }}"></div>
                                                 <div class="col-lg-4" style="padding-top: 10px; text-align: center;"><strong>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX_INVOICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TAX_INVOICE') : trans($MER_OUR_LANGUAGE.'.MER_TAX_INVOICE') }} </strong></div>
                                                  <div class="col-lg-4" style="text-align: right;"><a href="" class="" style="color:#d9534f" data-dismiss="modal" CLASS="pull-right"><i class="icon-remove-sign icon-2x"></i></a></div>
                                        </div>
                                      <hr style="margin-top: 0px;">
                                      <div class="row">
                                      <div class="col-lg-12">
                                      <div class="col-lg-6">
                                     <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY1')!= '') ? trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY1') : trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY1') }}</h4>
                                       @if($coddetails->cod_status == 1) 
                    <b>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_PAID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT_PAID') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_PAID') }}
                    @else
                                      <b>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_TO_PAY')!= '') ? trans(Session::get('mer_lang_file').'.MER_AMOUNT_TO_PAY') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_TO_PAY') }} 
                    @endif 
                    
                    :{{ Helper::cur_sym() }} </b>
                                      <?php
                    $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $coddetails->cod_transaction_id)
                    ->where('nm_ordercod.cod_merchant_id', $merid)
                    ->where('nm_ordercod.cod_order_type', '=',1)->get();
                    $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
                     @foreach($product_titles as $prd_tit) 
                        
                          @php      $status=$prd_tit->cod_status;  @endphp
                      @if($prd_tit->delivery_status==1)
                      @php
                      $orderstatus="Order Placed";
                      @endphp
                      @elseif($prd_tit->delivery_status==2) 
                      @php
                      $orderstatus="Order Packed";
                      @endphp
                      @elseif($prd_tit->delivery_status==3) 
                      @php
                      $orderstatus="Dispatched";
                      @endphp
                      @elseif($prd_tit->delivery_status==4) 
                      @php
                      $orderstatus="Delivered";
                      @endphp
                      @elseif($prd_tit->delivery_status==5)
                      @php
                        $orderstatus="cancel pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==6) 
                      @php
                      $orderstatus="cancelled";
                      @endphp
                      @elseif($prd_tit->delivery_status==7) 
                      @php
                      $orderstatus="return pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==8) 
                      @php
                      $orderstatus="Returned";
                      @endphp
            @elseif($prd_tit->delivery_status==9) 
                      @php
                      $orderstatus="Replace Pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==10) 
                     @php
                      $orderstatus="Replaced";
                      @endphp
            @else
            @php $orderstatus = '';  @endphp
              @endif
                      @php
                      $subtotal=$prd_tit->cod_amt; 
                                $tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
                              
                                $total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
                                $total_ship_amt+= $prd_tit->cod_shipping_amt;
                                $total_item_amt+=$prd_tit->cod_amt;
                                $coupon_amount+= $prd_tit->coupon_amount;
                                $prodct_id = $prd_tit->cod_pro_id;
                                
                            $item_amt = $prd_tit->cod_amt + (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
                              
                            
                               $ship_amt = $prd_tit->cod_shipping_amt;
                              
                            
                               //$item_tax = $codorderdet->cod_tax;
                              /*if($prd_tit->coupon_amount != 0)
                              {
                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                              }
                              else
                              {
                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
                              }*/   

                              $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                              
                              //echo $grand_total;  @endphp
                    @endforeach
                     {{ $grand_total }}</b>
                                      <br>
                    
                                      <span>({{ (Lang::has(Session::get('mer_lang_file').'.MER_INCLUSIVE_OF_ALL_CHARGES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INCLUSIVE_OF_ALL_CHARGES') : trans($MER_OUR_LANGUAGE.'.MER_INCLUSIVE_OF_ALL_CHARGES') }}) </span>
                                     <br>
                                     <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDERID')!= '') ? trans(Session::get('mer_lang_file').'.MER_ORDERID') : trans($MER_OUR_LANGUAGE.'.MER_ORDERID') }}: {{ $coddetails->cod_transaction_id }}</span>
                                       <br>
                    <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') : trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }}: {{ $coddetails->cod_date }}</span>
                                      </div>
                                      <div class="col-lg-6" style="border-left:1px solid #eeeeee;">
                                          <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_DETAILS') }}</h4>
                                          
                                          <?php $explode = explode(',',$coddetails->cod_ship_addr); ?>
                                          @foreach($explode as $addr)
                                             {{-- echo $addr.'<br>'; --}}
                                          @endforeach
                       {{ (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NAME') : trans($MER_OUR_LANGUAGE.'.MER_NAME') }} :{{ $coddetails->ship_name }}<br>
                       
                        {{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ? trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} : {{ $coddetails->ship_email }}<br>
                       
                        @if($coddetails->ship_address1!='') 
                          {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1') }} : {{ $coddetails->ship_address1 }}<br>
                            @endif
              
              @if($coddetails->ship_address2!='') 
                          {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2') }} : {{ $coddetails->ship_address2 }}<br>
                            @endif
              
                          @if($coddetails->ship_ci_id!='')    
                            {{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY') : trans($MER_OUR_LANGUAGE.'.MER_CITY') }} : {{ $coddetails->ship_ci_id }} <br>
                          @endif   
              
                          @if($coddetails->ship_state!='')    
                       {{ (Lang::has(Session::get('mer_lang_file').'.MER_STATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STATE') : trans($MER_OUR_LANGUAGE.'.MER_STATE') }} : {{ $coddetails->ship_state }}<br>
                        @endif

                        @if($coddetails->ship_country!='')    
                       {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_COUNTRY') }} : {{ $coddetails->ship_country }}<br>
                        @endif
                       @if($coddetails->ship_postalcode!='') 
                       {{ (Lang::has(Session::get('mer_lang_file').'.MER_ZIPCODE')!= '') ? trans(Session::get('mer_lang_file').'.MER_ZIPCODE') : trans($MER_OUR_LANGUAGE.'.MER_ZIPCODE') }} : {{ $coddetails->ship_postalcode }}<br>
                        @endif                  
                                          {{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ? trans(Session::get('mer_lang_file').'.MER_PHONE')  : trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} : {{ $coddetails->ship_phone }}
                                          </div>
                                            
                                             
                                      </div>
                                      </div>
                                      <hr>
                                      <div class="row">
                                      <div class="span12 text-center">
                                      <h4 class="text-center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_INVOICE_DETAILS') }}</h4>
                                      <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS') : trans($MER_OUR_LANGUAGE.'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS') }} </span>
                                      </div>
                                      </div>
                                    <table class="inv-table" style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">
                                    <tr style="border-bottom:1px solid #e6e6e6; background:#f5f5f5;">
                  <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_TITLE')}}</th>
                                    <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_COLOR')!= '') ? trans(Session::get('mer_lang_file').'.MER_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_COLOR') }}</th>
                                      <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_SIZE') }}</th>
                                        <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_QUANTITY') : trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }}</th>
                                         <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_PRICE') }}</th>
                                            <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SUB_TOTAL')!= '') ? trans(Session::get('mer_lang_file').'.MER_SUB_TOTAL') :  trans($MER_OUR_LANGUAGE.'.MER_SUB_TOTAL') }}</th>
                                              <th  width="" align="center">{{ (Lang::has(Session::get('lang_file').'.COUPON_AMOUNT')!= '') ?  trans(Session::get('lang_file').'.COUPON_AMOUNT') : trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT') }}</th>
                            
                          <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS') : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_STATUS') }}</th>
                          
                                             <th  width="" align="center">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS') : trans($MER_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS') }}</th>
                                          <?php $subtotal=$prd_tit->cod_qty*$prd_tit->cod_amt;  ?>  
                                    </tr>
                                    <?php
                    $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $coddetails->cod_transaction_id)
                    ->where('nm_ordercod.cod_merchant_id', $merid)
                    ->where('nm_ordercod.cod_order_type', '=',1)->get();
                    $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
                   @foreach($product_titles as $prd_tit) 
                        @php 
                                $status=$prd_tit->cod_status;
                       @endphp
                      @if($prd_tit->delivery_status==1)
                      @php
                      $orderstatus="Order Placed";
                      @endphp
                      @elseif($prd_tit->delivery_status==2) 
                      @php
                      $orderstatus="Order Packed";
                      @endphp
                      @elseif($prd_tit->delivery_status==3) 
                      @php
                      $orderstatus="Dispatched";
                      @endphp
                      @elseif($prd_tit->delivery_status==4) 
                      @php
                      $orderstatus="Delivered";
                      @endphp
                      @elseif($prd_tit->delivery_status==5)
                      @php
                        $orderstatus="cancel pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==6) 
                      @php
                      $orderstatus="cancelled";
                      @endphp
                      @elseif($prd_tit->delivery_status==7) 
                      @php
                      $orderstatus="return pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==8) 
                      @php
                      $orderstatus="Returned";
                      @endphp
            @elseif($prd_tit->delivery_status==9) 
                      @php
                      $orderstatus="Replace Pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==10) 
                     @php
                      $orderstatus="Replaced";
                      @endphp
            @else
            @php $orderstatus = '';  @endphp
              @endif
                      
                      @if($prd_tit->cod_status==1)
                      @php
                      $payment_status="Success";
                      @endphp
                      @elseif($prd_tit->cod_status==2) 
                      @php
                      $payment_status="Order Packed";
                      @endphp
                      @elseif($prd_tit->cod_status==3) 
                      @php
                      $payment_status="Hold";
                      @endphp
                      @elseif($prd_tit->cod_status==4) 
                      @php
                      $payment_status="Faild";
                      @endphp
                      @endif
                      @php
                      $subtotal=$prd_tit->cod_amt; 
                                $tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
                              
                                $total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
                                $total_ship_amt+= $prd_tit->cod_shipping_amt;
                                $total_item_amt+=$prd_tit->cod_amt;
                                $coupon_amount+= $prd_tit->coupon_amount;
                                $prodct_id = $prd_tit->cod_pro_id;
                                
                            $item_amt = $prd_tit->cod_amt + (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
                              
                            
                               $ship_amt = $prd_tit->cod_shipping_amt;
                              
                            
                               //$item_tax = $codorderdet->cod_tax;
                              /*if($prd_tit->coupon_amount != 0)
                              {
                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                              }
                              else
                              {
                                $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
                              }   */
                              $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                    @endphp
                                     <tr>
                  <td  width="" align="center">{{ $prd_tit->pro_title}}</td>&nbsp;                   
                                   <td  width="" align="center">{{ $prd_tit->co_name }}</td>&nbsp;
                                      <td  width="" align="center">{{ $prd_tit->si_name }}</td>
                                        <td  width="" align="center">{{ $prd_tit->cod_qty }}</td>
                                          <td  width="" align="center">{{ Helper::cur_sym() }}  {{ $prd_tit->cod_prod_unitPrice }} <?php //echo $prd_tit->pro_disprice;?> </td>
                                            <td  width="" align="center">{{ Helper::cur_sym() }} {{ $subtotal - $prd_tit->coupon_amount}} </td>
                      @if($prd_tit->coupon_amount != 0)
                              <td  width="" align="center">{{ Helper::cur_sym() }} {{ $prd_tit->coupon_amount  }} </td>
                              @else  <td  width="" align="center">{{ Helper::cur_sym() }} {{ $prd_tit->coupon_amount }} 
                            </td>@endif
                              <td  width="" align="center">{{ $payment_status }}</td>
                                             <td  width="" align="center">{{ $orderstatus }}</td>                 
                                              
                                    </tr>
                  @endforeach
                                    </table>
                                    <br>
                                    <hr>
                                    <div class="">
                                    <div class="col-lg-6"></div>
                                     <div class="col-lg-6">
                               <div>{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPMENT_VALUE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPMENT_VALUE') : trans($MER_OUR_LANGUAGE.'.MER_SHIPMENT_VALUE') }}<p class="pull-right" style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $total_ship_amt }} </p></div><br>
                                        <div>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX')!= '') ? trans(Session::get('mer_lang_file').'.MER_TAX') : trans($MER_OUR_LANGUAGE.'.MER_TAX')}}<p class="pull-right"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $total_tax_amt }}</p></div>
                                        <hr>


                                     @if(count($wallet)!=0)
                                        <span>wallet : <p class="pull-right"style="margin-right:15px;">- {{ Helper::cur_sym() }} {{ $wallet_amt }}</p></span><br>
                                    @endif

                                     <?php   /*if($coddetails->coupon_amount !=0){ ?>
                                         <span>coupon Code : <b class="pull-right"style="margin-right:15px;">- {{ Helper::cur_sym() }}<?php echo $coupon_value;?></b></span><br>
<?php                                }*/
                                     ?>
                                       <strong><span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }}<p class="pull-right"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $grand_total }}</p></span></strong>
                                     </div>
                                    </div>
                        
                                        
                                        <div class="modal-footer" style="background: #f5f5f5;">
                                            <button data-dismiss="modal" class="btn btn-danger" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLOSE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLOSE') : trans($MER_OUR_LANGUAGE.'.MER_CLOSE') }}</button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>


   <?php $i=$i+1; ?> 
    @endforeach
    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; <?php echo $SITENAME; ?> &nbsp;<?php echo date("Y"); ?>&nbsp;</p>
    </div>
    <!--END FOOTER -->
  <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
       <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
	<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
