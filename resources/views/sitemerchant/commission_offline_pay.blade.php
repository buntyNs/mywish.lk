<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_COMMSSION_OFFLINE_PAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMSSION_OFFLINE_PAY') : trans($MER_OUR_LANGUAGE.'.MER_COMMSSION_OFFLINE_PAY') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
	<link rel="stylesheet" href="{{ url('') }}/public/assets/css/plan.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="{{ url('') }}/public/assets/css/datepicker.css" rel="stylesheet">	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/timeline/timeline.css" />

	

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
          {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        {!! $adminleftmenus !!}
        
        
        <!--END MENU SECTION -->

		<div></div>


         <!--PAGE CONTENT -->
        <div id="content">
           
                
    <div class="inner"> 
      <div class="row"> 
        <div class="col-lg-12"> 
          <ul class="breadcrumb">
            <li class=""><a href="{{ url('sitemerchant_dashboard')}}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
            <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_COMMSSION_OFFLINE_PAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMSSION_OFFLINE_PAY') : trans($MER_OUR_LANGUAGE.'.MER_COMMSSION_OFFLINE_PAY')}}</a></li>
          </ul>

        </div>
      </div>
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="box dark"> <header> 
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_COMMSSION_OFFLINE_PAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMSSION_OFFLINE_PAY') : trans($MER_OUR_LANGUAGE.'.MER_COMMSSION_OFFLINE_PAY')}}</h5>
            </header> 
             @if (Session::has('success'))

		          <div class="alert alert-success alert-dismissable">{!! Session::get('success') !!}
				{{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
              </div>
             @endif

            <div class="row"> 
          
              <div class="col-lg-12 col-sx-12"> 
                <div class="form-group"> 
                 {!! Form::open(array('url'=>'commission_offline_pay_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
                  <label for="text1" class="control-label col-lg-3 col-xs-12"> 
				  {{ (Lang::has(Session::get('mer_lang_file').'.MER_TOTAL_COMMISSION_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TOTAL_COMMISSION_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_TOTAL_COMMISSION_AMOUNT')}} 
                         
                </label>
                  <label for="text1" class="control-label col-lg-2">   : {{ Helper::cur_sym() }} {{ $dataAr['amt'] }}</label>
                  <div class="col-lg-7"> 
                   
					{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-4', 'for' => 'text1'])) !!}
                   
                    <div class="col-lg-5"> 
					
						{{ Form::hidden('mr_id',$dataAr['mr_id']) }}
						{{ Form::hidden('name',$dataAr['name']) }}
						{{ Form::hidden('mr_id',$dataAr['mr_id']) }}
						{{ Form::hidden('payTrans',$payTrans) }}
                     
                    </div>
                  </div>
                </div>
              </div>
</div>
<div class="row">
              <div class="col-lg-12">
                <div class="form-group form-horizontal">  
                  <label for="text1" class="control-label col-lg-3"> 
				  {{ (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION_ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TRANSACTION_ID'): trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION_ID')}} 
                
                  </label>
                  <div class="col-lg-8"> 
                    <div class=""> 
					{{ Form::text('txn_id','',array('required')) }}
                      
                    </div>
					{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-4', 'for' => 'text1'])) !!}
                    
                   
                  </div>
               
              </div></div>
</div><div class="row form-horizontal">
              <div class="col-lg-12">
                  <div class="form-group form-horizontal">
                    <div class="col-lg-3">
                    
                   
                    </div>
   <div class="col-lg-4"><button class="btn btn-warning btn-sm btn-grad" type="submit" id="total_to_pay_submit" style="color:#fff">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SUBMIT') : trans($MER_OUR_LANGUAGE.'.MER_SUBMIT')}}</button>
                     <button class="btn btn-danger btn-sm btn-grad"><a style="color:#ffffff;" href="{{ URL::previous() }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CANCEL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CANCEL'): trans($MER_OUR_LANGUAGE.'.MER_CANCEL')}}</a></button></div>
                    </div>
					{{ Form::close() }}
                </div>
            </div>
          </div>
        </div>
        
        
      </div>
      
    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $adminfooter !!}
    <!--END FOOTER -->

	


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/jquery-2.0.3.min.js"></script>
    <script> 
	
	</script>
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
    
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.resize.js"></script>
	<script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="{{ url('')}}/public/assets/js/pie_chart.js"></script>
    
    


<script src="{{ url('')}}/public/assets/js/jquery-ui.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="{{ url('')}}/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="{{ url('')}}/public/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="{{ url('')}}/public/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="{{ url('')}}/public/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="{{ url('')}}/public/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>

        
    
	<script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="{{ url('')}}/public/assets/js/bar_chart.js"></script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
