<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME}} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_EDIT_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_EDIT_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_EDIT_PRODUCTS')}}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css"/>
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-wysihtml5-hack.css" />
     <style>
                        ul.wysihtml5-toolbar > li {
                            position: relative;
                        }
                    </style>
    <style type="text/css">
    .multiselect-container {
      height: 200px !important;
      overflow-y: scroll !important;
    }  </style>                
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

     <?php /* Edit Image Starts */ ?>

    <link href="{{ url('') }}/public/assets/cropImage/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('') }}/public/assets/cropImage/editImage/css/style.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/cropImage/editImage/css/canvasCrop.css" />
     <?php /* Edit Image ends */ ?>
     <?php /* Edit Image Starts */ ?>
<style type="text/css">
  .imageBox {
  position: relative;
  /*height: 800px;
  width: 800px;*/
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor: move;
  box-shadow: 4px 4px 12px #B0B0B0;
}

.imageBox .thumbBox {
  position: absolute;
     /*width: 800px;
    height: 800px;*/
    /*left: 173px;
    top: 173px;*/
    top: 100px;
    left: 100px;
  box-sizing: border-box;
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}
.modal-content
{
  background-color: rgb(185, 185, 185);
}
</style>
 <?php /* Edit Image ends */ ?>

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >
    <?php /* Edit Image Starts */ ?>
      <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog"  style="width:64%;">
          
            <!-- Modal content-->
            <div class="modal-content dev_appendEditData">
              <?php ?>  
              <script type="text/javascript">
                    function calSubmit(){

                      $("#dev_upImg_form").submit();
                    }
                  </script>
           

              <!-- Modal content-->
              <div>
              <div class="modal-header">
                {{ Form::button('&times;',['class' => 'close' , 'data-dismiss' => 'modal']) }}
                  
                  <h4 class="modal-title">Edit Image</h4>
              </div>
              <div class="modal-body" id='model_body'>

              
                <div class="imageBox" style="width:{{ $PRODUCT_WIDTH }}px; height: {{ $PRODUCT_HEIGHT }}px;">
                  <!--<div id="img" ></div>-->
                  <!--<img class="cropImg" id="img" style="display: none;" src="images/avatar.jpg" />-->
                  <div class="mask"></div>
                  <div class="thumbBox" style="width: {{ $PRODUCT_WIDTH }}px; height: {{ $PRODUCT_HEIGHT }}px;"></div>
                  <div class="spinner" style="display: none">Loading...</div>
                </div>
                <div class="tools clearfix" style='display: block; left:5px;top:250px;width:600px; margin-top:15px;'>
                  <span id="rotateLeft" >rotateLeft</span>
                  <span id="rotateRight" >rotateRight</span>
                  <span id="zoomIn" >zoomIn</span>
                  <span id="zoomOut" >zoomOut</span>
                  <span id="crop" >Crop</span>
                  
                  <!--<span id="alertInfo" >alert</span> -->
                  <div class="upload-wapper">
                             Select An Image
                      <input type="file" id="upload-file" value="Upload" />
                  </div>
                  <div class="crop-edit-up"><button class="btn btn-success" type="button " id='dev_uploadBtn' onclick="calSubmit();" style='display: none'>Upload</button></div>
                </div>
                {!! Form::open(array('url'=>'mer_CropNdUpload_product','id' =>'dev_upImg_form','method' => 'post')) !!}
                
                    
                    <input name="_token" hidden value="{!! csrf_token() !!}" />
                    {{ Form::hidden('product_id','',array('id'=>'product_id')) }}
                    {{ Form::hidden('img_id','',array('id'=>'img_id')) }}
                    {{ Form::hidden('imgfileName','',array('id'=>'imgfileName')) }}
                    {{ Form::hidden('base64_imgData','',array('id'=>'croppedImg_base64')) }}
                    
                    <input type="submit" value="submit" style="display: none" />
                {{ Form::close() }} 
                <div id='showCroppedImg'></div>
                <!-- Edit image starts -->
                <script type="text/javascript">
                    $(function(){
                       
                    }) 
                </script>
                <!-- Edit image ends -->

              </div>
              <div class="modal-footer">
                {{ Form::button('Close',['class' => 'btn btn-default' , 'data-dismiss' => 'modal']) }}
                
              </div>
              </div>

   
            </div>
            
          </div>
        </div>

      <!--Modal Ends-->
    <?php /* Edit Image ends */ ?>
    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

       
    <div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                          <ul class="breadcrumb">
                              <li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_PRODUCTS') }}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_PRODUCTS') }}</h5>
            
        </header>
         @if (Session::has('block_message'))
          <div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
            {{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
            </div>
        @endif

    @if ($errors->any())  
         <br>
     <ul style="color:red;">
    <div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>')) !!}
      {{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
         
        </div>
    </ul> 
    @endif
        @if (Session::has('message'))
    <p style="background-color:green;color:#fff; padding: 10px;">{!! Session::get('message') !!}</p>
    @endif
 
 @foreach($product_list as $products)
    @endforeach
    <?php
     $title      = $products->pro_title;
     $title_fr     = $products->pro_title_fr;
         $category_get   = $products->pro_mc_id;
       $maincategory   = $products->pro_smc_id;
     $subcategory    = $products->pro_sb_id;
     $secondsubcategory= $products->pro_ssb_id;
     $originalprice  = $products->pro_price;
     $discountprice  = $products->pro_disprice;
     $inctax=$products->pro_inctax;
     $shippingamt=$products->pro_shippamt;
     $description    = $products->pro_desc;
     $description_fr = $products->pro_desc_fr;
     $deliverydays   = $products->pro_delivery;
     $merchant     = $products->pro_mr_id;
     $shop       = $products->pro_sh_id;
     $metakeyword  = $products->pro_mkeywords;
     $metakeyword_fr = $products->pro_mkeywords_fr;
     $metadescription= $products->pro_mdesc;
     $metadescription_fr= $products->pro_mdesc_fr;
     $file_get     = $products->pro_Img;
     $img_count    = $products->pro_image_count;
     $productspec    = $products->pro_isspec;
     $pqty           = $products->pro_qty;
     $sku_no          = $products->pro_sku_number;
     $no_of_purchase = $products->pro_no_of_purchase;
     $pro_is_siz     = $products->pro_is_size;
     $pro_is_color   = $products->pro_is_color;
     $cash_pack      = $products->cash_pack;
?>

        <div id="div-1" class="accordion-body collapse in body">
               {!! Form::open(array('url'=>'mer_edit_product_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
               {{ Form::hidden('product_edit_id',$products->pro_id,array('id'=>'product_edit_id')) }}
    
    <div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_TITLE') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Title" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_ENTER_YOUR_PRODUCT_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_YOUR_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_YOUR_PRODUCT_TITLE') }}" name="Product_Title" class="form-control" type="text" value="{{ $title }}">
            <div id="title_error_msg"  style="color:#F00;font-weight:800"  > </div>
                    </div>
                </div>
        
        
        @if(!empty($get_active_lang)) 
        @foreach($get_active_lang as $get_lang) 
        <?php 
        $get_lang_name = $get_lang->lang_code;
        $get_lang_code = $get_lang->lang_name;
        $Product_Title_dynamic = 'pro_title_'.$get_lang->lang_code;
        ?>
        <div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">Product Title({{ $get_lang_code }}) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Title_{{ $get_lang_name }}"  name="Product_Title_{{ $get_lang_name }}" placeholder="Enter Product Title In  {{ $get_lang_code }}" class="form-control" type="text" onChange="check();" value="{{ $products->$Product_Title_dynamic}}"><div id="title_{{ $get_lang_name }}_error_msg"  style="color:#F00;font-weight:800"  >
            @if ($errors->has('Product_Title_'.$get_lang_name.'')) {{ $errors->first('Product_Title_'.$get_lang_name.'') }}@endif</div>
          </div>
                </div>
        @endforeach
        @endif
        <!--
        <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Title({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Title_fr" placeholder="Enter Product Name in {{ Helper::lang_name() }}" name="Product_Title_fr" class="form-control" type="text" value="<?php echo $title_fr; ?>">
            <div id="title_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
           </div>
                </div>-->
                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_TOP_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TOP_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_TOP_CATEGORY') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
    
          <select class="form-control" name="category" id="category" onChange="select_main_cat(this.value);get_specification_details();show_alert();" >
                        <option value="0">--- {{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')}} ---</option>
                      @foreach($category as $cat_list)  
                    <option value="{{ $cat_list->mc_id }}" <?php if($cat_list->mc_id ==  $category_get) { ?> selected <?php } ?> >{{ $cat_list->mc_name }}<!--(<?php // echo $cat_list->mc_name_fr; ?>)--></option>
                        @endforeach
          </select>     
        <div id="category_error_msg"  style="color:#F00;font-weight:800"  > </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_MAIN_CATEGORY') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <select class="form-control" name="maincategory" id="maincategory" onChange="select_sub_cat(this.value);get_specification_details();show_alert();" >
                  <option value="0">---{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_MAIN_CATEGORY') }}---</option>          
                </select>
            <div id="main_cat_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SUB_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SUB_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SUB_CATEGORY') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                         <select class="form-control" name="subcategory" id="subcategory" onChange="select_second_sub_cat(this.value)" >
                    <option value="0">---{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_SUB_CATEGORY') }}---</option>         
                </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SECOND_SUB_CATEGORY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SECOND_SUB_CATEGORY') :  trans($MER_OUR_LANGUAGE.'.MER_SECOND_SUB_CATEGORY') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                    <select class="form-control" name="secondsubcategory" id="secondsubcategory"  >
                   <option value="0">---{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_SELECT_SECOND_SUB_CATEGORY')}}---</option>      
                 </select>
                    </div>
                </div>
        <?php /*Brand
        <div class="form-group">
                  <label for="pass1" class="control-label col-lg-2"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_BRAND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_BRAND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_BRAND');} ?><span class="text-sub">*</span></label>
          <div class="col-lg-8">
          <select class="form-control" name="product_brand" id="product_brand" >
                        <option value="0">--- <?php if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_BRAND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SELECT_BRAND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SELECT_BRAND');} ?>---</option>
                       <?php foreach($brand_details as $brand_data)  { ?>
                    <option value="<?php echo $brand_data->brand_id; ?>" <?php if($brand_data->brand_id ==  $products->product_brand_id) { ?> selected <?php } ?> ><?php echo $brand_data->brand_name; ?></option>
                        <?php } ?>
          </select>     
          <div id="brand_error_msg"  style="color:#F00;font-weight:800"  > </div>
                  </div>
               </div>
         Brand*/ ?>
        <!--  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SKU_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SKU_NUMBER') : trans($MER_OUR_LANGUAGE.'.MER_SKU_NUMBER') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                     
                        <input   placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_SKU_NUMBER') }}" class="form-control" type="text" id="sku_no" name="sku_no" maxlength="20" value="{{ $sku_no }}" onkeyup="check_sku_no()">
            <div id="skuno_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div> -->
        <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_QUANTITY') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_QUANTITY') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                      {{ Form::hidden('no_of_purchase',$no_of_purchase,array('id'=>'no_of_purchase')) }}
          
                        <input   placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_ENTER_QUANTITY_OF_PRODUCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_QUANTITY_OF_PRODUCT') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_QUANTITY_OF_PRODUCT') }}" class="form-control" type="text" id="Quantity_Product" name="Quantity_Product" maxlength="5" value="{{ $pqty }}" >
            <div id="qty_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ? trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') :  trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE') }} ({{ Helper::cur_sym() }})<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY') }}" class="form-control" type="text" maxlength="10" id="Original_Price" value="{{ $originalprice }}" name="Original_Price">
            <div id="org_price_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
          <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_DISCOUNTED_PRICE')}} ({{ Helper::cur_sym() }})<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY') }}" class="form-control" type="text" maxlength="10" id="Discounted_Price" name="Discounted_Price" value="<?php echo $discountprice; ?>">
            <div id="dis_price_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
         <div class="form-group">
          {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])) !!}
                    

                    <div class="col-lg-8">
                                <input type="checkbox" id="inctax_check" <?php if($inctax > 0) { ?> checked="checked" <?php } ?> onclick="if(this.checked){$('#inctax').show();}else{$('#inctax').hide();$('#inctax').val(0)}"> ( {{ (Lang::has(Session::get('mer_lang_file').'.MER_INCLUDING_TAX_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INCLUDING_TAX_AMOUNT') :  trans($MER_OUR_LANGUAGE.'.MER_INCLUDING_TAX_AMOUNT') }} ) 

                                <input placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY') }}" <?php if($inctax == 0) { ?> style="display:none;" <?php } ?>  class="form-control"  max="99" type="number" id="inctax" name="inctax" value="{{ $inctax }}">%
                <div id="tax_error_msg"  style="color:#F00;font-weight:800"> </div> 
          </div>
                </div>
    <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ? trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT') }}<span class="text-sub">*</span></label>

                   <div class="col-lg-8">
<input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'none');" value="1" <?php if($shippingamt<=0){?>checked<?php } ?> > <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_FREE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FREE') : trans($MER_OUR_LANGUAGE.'.MER_FREE') }}</label>
<input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'block');" value="2" <?php if($shippingamt>0){?>checked<?php } ?>  >{{(Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT') :  trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')}}</label>

          
            <label class="sample"></label>
                    </div>
                </div>
 

@if($shippingamt<=0) 
<?php $showship="display:none"; ?>
@else
<?php
  $showship="display:block";
?>
@endif
          <div class="form-group" id="showship" style="{{ $showship }}" >
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT') }} ({{ Helper::cur_sym() }})<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input  placeholder="" class="form-control" type="text"  maxlength="10" id="Shipping_Amount" name="Shipping_Amount" value="{{ $shippingamt }}">
            <div id="ship_amt_error_msg"  style="color:#F00;font-weight:800"> </div>
          </div>
                </div>
          
         
         <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION') :  trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                      {{ Form::textarea('Description',$description,['id' => 'wysihtml5','class' =>'wysihtml5 form-control', 'rows' => '10']) }}
                
            <div id="desc_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
         
        @if(!empty($get_active_lang)) 
        @foreach($get_active_lang as $get_lang) 
        <?php
        $get_lang_name = $get_lang->lang_code;
        $get_lang_code = $get_lang->lang_name;
        $Description_dynamic = 'pro_desc_'.$get_lang->lang_code;
        ?>
        <div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION') :  trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION') }}({{ $get_lang_code }}) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
            <textarea id="wysihtml5" placeholder="Enter Description in {{ $get_lang_code }}" name="Description_{{ $get_lang_name}}" class="wysihtml5 form-control" rows="10" >{{ $products->$Description_dynamic}}</textarea><div id="Description_<{{ $get_lang_name}}_error_msg"  style="color:#F00;font-weight:800"  >
            @if ($errors->has('Description_'.$get_lang_name.'')) {{ $errors->first('Description_'.$get_lang_name.'') }}@endif</div>
          </div>
                </div>
        @endforeach
                @endif
        <!--
    <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Description ({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
         <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10" id="Description_fr" name="Description_fr" placeholder="Enter Your Description in French"><?php echo $description_fr; ?></textarea>
     <div id="desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
        </div>-->
        
      <div class="form-group">
                    <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_WANT_TO_ADD_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_WANT_TO_ADD_SPECIFICATION'): trans($MER_OUR_LANGUAGE.'.MER_WANT_TO_ADD_SPECIFICATION') }}<span class="text-sub">*</span></label>

                   <div class="col-lg-8" id="specification_list_avail">
<input type="radio"  name="specification"  onClick="setVisibility('sub4', 'block');get_specification_details();" value="1" <?php if($productspec==1) {?> checked <?php }?>> <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES') }}</label>
    <input type="radio" name="specification"  onClick="setVisibility('sub4', 'none');" value="2" <?php if($productspec==2) {?>checked <?php } ?>> <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO') }}</label>
          </div>
                     <div class="col-lg-8" id="specification_list_notavail" style="display:none;"> No specification List avaliable for this particular Category  
                     </div>
                </div>


<?php  
$resultspec="";
$resultspectext="";
  ?>
  
   
  @foreach ($existingspecification as $existingspecification1) 
  <?php 
  $resultspec=$existingspecification1->spc_sp_id.",".$resultspec;
  $resultspectext=$existingspecification1->spc_value.",".$resultspectext;
  ?>
  @endforeach

 
@if(strlen($resultspec)>0)
<?php

$trimmedspec=trim($resultspec,",");
$specarray=explode(",",$trimmedspec);
$speccount=count($specarray)-1;
$trimmedtext=trim($resultspectext,",");
$textarray=explode(",",$trimmedtext);
$specidvalue=$speccount+1;
?>
@else
<?php
 $speccount=0;
?>
@endif

  <?php if($productspec==1){  ?>
  
                
         <div class="form-group" id="sub4" <?php if($productspec==1){?>style="display:block"<?php }else {?>style="display:none;"<?php }?> >
                    <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SPECIFICATION') : trans($MER_OUR_LANGUAGE.'.MER_SPECIFICATION') }}</label>

                    <div class="col-lg-8">
                    <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TEXT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TEXT'): trans($MER_OUR_LANGUAGE.'.MER_TEXT')}} ( {{ (Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SPECIFICATION'): trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_SPECIFICATION')}} <a href="{{ url('') }}/manage_specification">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD'): trans($MER_OUR_LANGUAGE.'.MER_ADD')}}</a> )</label>
          <label>Specification In English</label>
          <label>Specification In French</label>
      </div>

    <?php $i=0; $speccount = count($existingspecification);
    //for($i=0;$i<=$speccount;$i++){ ?>
    @foreach ($existingspecification as $existingspecification1) 
    <div class="col-lg-12 mer-ed-spc" style="display: block; overflow: hidden;">
        <div class="form-group" id="spec{{ $i }}">
            <div class="col-lg-2"></div>
      <div class="col-lg-2">
                <select class="form-control" name="{{ 'spec_grp'.$i }}" id="{{ 'spec_grp'.$i}}" onChange='spcfunction({{ $i}},this.value);'>
                        <option  value="0">-- select Specification Group --</option>
                  @foreach ($spec_group as $pro_spec_group) 
                  <option  value="{{ $pro_spec_group->spg_id}}" <?php if($pro_spec_group->spg_id==$existingspecification1->spc_spg_id){?>selected<?php } ?> >{!!$pro_spec_group->spg_name!!}</option>
                  @endforeach
            </select>
            </div>
    
      <div class="col-lg-2">
         <select class="form-control" id="{{ 'pro_spec'.$i}}" name="{{ 'spec'.$i}}">
        
                        <option  value="0">--{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SPECIFICATION') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_SPECIFICATION')}}--</option>
                @foreach ($productspecification as $specification) 
               <option  value="{{ $specification->sp_id }}" <?php if($specification->sp_id==$existingspecification1->spc_sp_id){?>selected<?php } ?> >{!!$specification->sp_name!!}</option>
               @endforeach
            </select>
                    </div>


                   
                    <div class="col-lg-2">
                       <input type="text" class="form-control" name="{{ 'spectext'.$i}}" value="{{ $existingspecification1->spc_value}}">
                    </div>
                    @if(!empty($get_active_lang)) 
        @foreach($get_active_lang as $get_lang)
        <?php
        
        $get_lang_name = $get_lang->lang_code;
        $get_lang_code = $get_lang->lang_name;
        $spc_value_dynamic = 'spc_value_'.$get_lang->lang_code;
        ?>
                    <div class="col-lg-2">
                        <input id="{{ $get_lang_code}}_spectext0"  name="{{ $get_lang_name}}_spectext{{ $i}}" placeholder="Enter Specification in In {{ $get_lang_code}}" class="form-control" value="{{$existingspecification1->$spc_value_dynamic}}" type="text" >
          </div>
        @endforeach
      @endif
                       <div class="col-lg-2">
                        <p id="addmore" <?php if($productspec==1){?>style="display:block"<?php }else {?>style="display:none;"<?php }?>>
                          <a class="pro-des-add" onClick="addspecificationFormField();"  style="cursor:pointer;color:#ffffff; ">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_MORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_MORE'): trans($MER_OUR_LANGUAGE.'.MER_ADD_MORE') }}</a> </p>
                          </div>

                           @if($i>0)
            <div class='col-lg-2'><a href='#' class='pro-des-rem' onClick='removespecFormField("#spec{{$i}}"); return false;' style='color:#ffffff;' >Remove</a></div>
            @endif


        <?php  
        /* print_r($get_active_lang); */ ?>
        
      </div></div>
        <?php  /*
      <div class="col-lg-3">
                <input type="text" class="form-control" name="<?php // echo 'fr_spectext'.$i;?>" value="<?php// echo $existingspecification1->spc_value_fr;?>">
            </div> */  ?>
    <?php $i++; ?>
    @endforeach
    <?php // } ?> 

                  {{ Form::hidden('',$i++,array('id'=>'specificationid1')) }}
                    
        
        {{ Form::hidden('specificationcount',$speccount,array('id'=>'specificationcount')) }}

                    
                </div>


    <div  id="divspecificationTxt">

    </div>  

  

<?php } else { ?>
  <div class="form-group" id="sub4" style="display:none;"  >
                    <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SPECIFICATION') : trans($MER_OUR_LANGUAGE.'.MER_SPECIFICATION') }}</label>

                    <div class="col-lg-8">
                    <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TEXT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TEXT'): trans($MER_OUR_LANGUAGE.'.MER_TEXT')}} ( {{ (Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SPECIFICATION'): trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_SPECIFICATION')}} <a href="{{ url('') }}/manage_specification">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD'): trans($MER_OUR_LANGUAGE.'.MER_ADD')}}</a> )</label>
      </div>

         <div class="col-lg-12 mer-ed-spc" style="display: block; overflow: hidden;">
           <div class="form-group">
            <div class="col-lg-2"></div>

            <div class="col-lg-2">
                <select class="form-control" name="spec_grp0" id="spec_grp0" onChange='spcfunction(0,this.value);'>
                     <option  value="0" selected></option>  
                  @foreach ($spec_group as $pro_spec_group) 
                  <option  value="{{ $pro_spec_group->spg_id }}">{!!$pro_spec_group->spg_name!!}</option>
                  @endforeach
            </select>
            </div>

     <div class="col-lg-2">
        <select class="form-control" id="pro_spec0" name="spec0">
                        <option  value="0">--select specification--</option>
                @foreach ($productspecification as $specification) 
               <option  value="{{ $specification->sp_id }}"  >{!!$specification->sp_name!!}</option>
               @endforeach
            </select>
                    </div>
    
      <?php /* <div class="col-lg-2">
        <select class="form-control" id="spec0" name="spec0">
                        <option  value="0">--<?php if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SPECIFICATION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SELECT_SPECIFICATION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SELECT_SPECIFICATION');} ?>--</option>
                @foreach ($productspecification as $specification) 
               <option  value="<?php echo $specification->sp_id;?>"  >{!!$specification->sp_name!!}</option>
               @endforeach
            </select>
                    </div>
 */ ?>

                   
                    <div class="col-lg-2">
                       <input type="text" class="form-control" id="spectext0" name="spectext0" value="">
                    </div>
  <div class="col-lg-2">
<p id="addmore" style="display:none;"><a class="pro-des-add" onClick="addspecificationFormField();"  style="cursor:pointer;color:#ffffff; display: inline-block; text-align: center;width:100%;">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_MORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_MORE'): trans($MER_OUR_LANGUAGE.'.MER_ADD_MORE') }}</a> </p></div>
 <?php /*Another specification */ ?>
</div>
</div>


          
        /* print_r($get_active_lang); */ ?>
        @if(!empty($get_active_lang)) 
        @foreach($get_active_lang as $get_lang) {
       <?php 
        $get_lang_name = $get_lang->lang_code;
        ?>
                    <div class="col-lg-2">
                        <input id="{{ $get_lang_code }}_spectext0"  name="{{ $get_lang_code }}_spectext0" placeholder="Enter Specification in In {{ Helper::lang_name() }}" class="form-control" type="text" >
          </div>
        @endforeach
        @endif
        <!--
          <div class="col-lg-3">
                       <input type="text" class="form-control" id="fr_spectext0" name="fr_spectext0" placeholder="Specification in French" value="">
          </div>
-->
                  {{ Form::hidden('','1',array('id'=>'specificationid1')) }}
                    {{ Form::hidden('specificationcount','0',array('id'=>'specificationcount')) }}
        
                    <div id="spec_error_msg"  style="color:#F00;font-weight:800"> </div>
                </div>


    <div  id="divspecificationTxt">

    </div>  

     <div class="form-group"  >

     <div class="col-lg-3">
                    
                    </div>
                   
    
    </div>  



<?php }?>
<input type="hidden" name="is_pro_siz" value="{{ $pro_is_siz }}">
<div class="form-group">
      <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCT_SIZE') }}<span class="text-sub"></span></label>

      <div class="col-lg-8">
      <input type="radio" id="pro_siz" name="pro_siz"  onClick="product_siz('pro_size', 'inline');" value="0" <?php if($pro_is_siz==0){ echo "checked";}?>> <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES') }}</label>
      <input type="radio" name="pro_siz"  id="pro_siz" onClick="product_siz('pro_size', 'none');" value="1" <?php if($pro_is_siz==1){ echo "checked";}?>> <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO') }}</label>
      <label class="sample"></label>
       </div>
</div>

<div class="form-group" id="pro_size" <?php if($pro_is_siz==0){?>style="display:block"<?php }else {?>style="display:none;"<?php }?>>
   <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_SIZE') }}<span class="text-sub"></span></label>

                    <div class="col-lg-3">
                       <select class="form-control" onchange="getsizename(this.value)"  name="Product_Size" id="Product_Size" >
                       
      @foreach ($productsize as $size) 
              @if($size->si_name!='no size')
               <option  value="{{ $size->si_id }}">{!!$size->si_name!!}</option>
          @endif
               @endforeach
         
        </select>
          <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SIZES')!= '') ? trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SIZES') : trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_SIZES') }} <a href="{{ url('') }}/manage_size"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD') : trans($MER_OUR_LANGUAGE.'.MER_ADD') }}</a></label>
                <div id="size_error_msg"  style="color:#F00;font-weight:800"> </div>
    </div>
                </div>

<?php  
$resultsize="";
$sizename="";
 ?>
  @foreach ($existingsize as $existingsize1) 
  <?php 
    $resultsize=$existingsize1->ps_si_id.",".$resultsize;
    $sizename=$existingsize1->si_name.",".$sizename;
  ?>
  @endforeach


@if(strlen($resultsize)>0)
<?php 

$trimmedsizes=trim($resultsize,",");
$sizearray=explode(",",$trimmedsizes);
$sizecount=count($sizearray);
$trimsizename=explode(",",$sizename);
?>
@else
<?php 
$resultsize="0,";
$sizecount=0;
?>
@endif

<div class="form-group" id="sizediv" <?php if($pro_is_siz==0){?>style="display:block"<?php }else {?>style="display:none;"<?php }?>>

{{ Form::hidden('productsizecount',$sizecount,array('id'=>'productsizecount')) }}
 
     <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YOUR_SELECT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YOUR_SELECT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_YOUR_SELECT_SIZE') }}<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <div id="showsize" >

    @for($i=0;$i<$sizecount;$i++) 
      @if($trimsizename[$i] !='no size')
    <div class="size-view">
      <span style="padding-right:5px;">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SIZE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SIZE'): trans($MER_OUR_LANGUAGE.'.MER_SELECT_SIZE')}}:</span>
      <span>{{ $trimsizename[$i] }}</span>
      <span style="color:#ff0099;"><input type="checkbox"  name="{{ 'sizecheckbox'.$sizearray[$i] }}" style="padding-left:30px;" checked="checked" value="1" ></span></div>
     @endif
     @endfor
    </div>
    
                       {{ Form::hidden('si',$resultsize,array('id'=>'si')) }}
                       
                    </div>
</div>
<div class="form-group" id="quantitydiv" <?php if($sizecount>0){ ?> style="display:none;" <?php }else { ?> style="display:none;" <?php } ?>>
       <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_QUANTITY') : trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }}<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showquantity" >

    @for($i=0;$i<$sizecount;$i++) 
   <input type="text" name="{{ 'quantity'.$sizearray[$i]}}" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input>  @endfor  
</p>
          {{ Form::hidden('','0',array('id'=>'sq')) }}
                       
                    </div>
</div>
        
<?php  
$resultcolor="";
$colorname="";
$colorcode="";
 ?>
  @foreach ($existingcolor as $existingcolor1) 
  <?php 
    $resultcolor=$existingcolor1->pc_co_id.",".$resultcolor;
    $colorname=$existingcolor1->co_name.",".$colorname;
    $colorcode=$existingcolor1->co_code.",".$colorcode;
  ?>
  @endforeach

 

 
@if(strlen($resultcolor)>0)
<?php

$trimmedcolor=trim($resultcolor,",");
$colorarray=explode(",",$trimmedcolor);
$colorcount=count($colorarray);
$colornamearray=explode(",",$colorname);
$colorcodearray=explode(",",$colorcode);
?>
@else
<?php
$colorcount=0;
$resultcolor="0,";
?>
@endif

{{ Form::hidden('is_pro_color',$pro_is_color) }}

<div class="form-group" >
   <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_COLOR_FIELD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_COLOR_FIELD') : trans($MER_OUR_LANGUAGE.'.MER_ADD_COLOR_FIELD') }}<span class="text-sub"></span></label>
   <div class="col-lg-8">  <input type="radio" name="productcolor" id="productcolor" onClick="setVisibility1('sub3', 'inline');" value="0" <?php if($pro_is_color==0){ echo "checked";}?>> <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES') }}</label>
          <input type="radio" name="productcolor" id="productcolor" onClick="setVisibility1('sub3', 'none');" value="1" <?php if($pro_is_color==1){ echo "checked";}?>><label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO') }} </label>
            <label class="sample"></label>
                    </div>
                </div>
                
                <div class="form-group" id="sub3"   <?php if($pro_is_color==0){ ?> style="display:block;" <?php }else { ?> style="display:none;" <?php } ?>>
                    <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_COLOR') }}</label>

                   <div class="col-lg-3">
           <select class="form-control" name="selectprocolor"  id="selectprocolor" onchange="getcolorname(this.value)">
            <option value="0" selected >--{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_PRODUCT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_PRODUCT_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_PRODUCT_COLOR') }}--</option>
       @foreach ($productcolor as $color) 
               <option style="background:{{ $color->co_code }}" value="{{ $color->co_id}}">{!!$color->co_name!!}</option>
               @endforeach
                </select>
        
        {{ (Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_COLORS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_COLORS') : trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_COLORS') }} <a href="{{ url('') }}/manage_color">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD'): trans($MER_OUR_LANGUAGE.'.MER_ADD')}}</a>
    <div id="color_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>

      <div class="form-group" id="colordiv" <?php if($pro_is_color==0){ ?> style="display:block;" <?php }else { ?> style="display:none;" <?php } ?>>
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YOUR_SELECT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YOUR_SELECT_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_YOUR_SELECT_COLOR') }}<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showcolor" >
      @for($i=0;$i<$colorcount;$i++)
      
<?php $bordercolor="border:4px solid".$colorcodearray[$i];?>
      <span style="width:195px;padding:5px;display:inline-block;{{ $bordercolor }};margin:5px 0px;">{{ $colornamearray[$i] }}<input type="checkbox"  name="{{ 'colorcheckbox'.$colorarray[$i]}}"" checked="checked" value="1" ></span>

      @endfor


      </p>
      {{ Form::hidden('co',$resultcolor,array('id'=>'co')) }}
                       
                    </div>
                </div>
    <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_DAYS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DELIVERY_DAYS') : trans($MER_OUR_LANGUAGE.'.MER_DELIVERY_DAYS') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <input type="text" maxlength="3" class="form-control" placeholder="" id="Delivery_Days" name="Delivery_Days" value="{{ $deliverydays }}">Days
                    </div>
                </div>
         <div class="form-group">
          {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text2'])) !!}
                    

                    <div class="col-lg-8">
                      <?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_EG')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_EG');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_EG');} */?>   
                    </div>
          <div id="delivery_error_msg"  style="color:#F00;font-weight:800"> </div>
                </div>
                
                {{ Form::hidden('Select_Merchant',Session::get('merchantid'),array('id'=>'Select_Merchant')) }}
        
          
                
          <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_STORE') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_STORE') }}<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                         <select class="form-control" name="Select_Shop" id="Select_Shop" >
            </select>
            <div id="store_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
          <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS') : trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS') }}<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                      {{ Form::textarea('Meta_Keywords',$metakeyword,['class' => 'form-control', 'id' => 'Meta_Keywords'])}}
                       
             <div id="meta_key_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
         
        @if(!empty($get_active_lang)) 
        @foreach($get_active_lang as $get_lang)
        <?php
        $get_lang_name = $get_lang->lang_code;
        $get_lang_code = $get_lang->lang_name;
        $Meta_Keywords_dynamic = 'pro_mkeywords_'.$get_lang->lang_code;
        ?>
        <div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS') : trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS') }} ({{ $get_lang_code }})</label>

                    <div class="col-lg-8">
            <textarea id="Meta_Keywords_{{ $get_lang_name}}" placeholder="Enter Keywords in {{ $get_lang_code }}" name="Meta_Keywords_{{ $get_lang_name}}" class="form-control" >{{ $products->$Meta_Keywords_dynamic}}</textarea>
            @if ($errors->has('metadescription'))<div id="meta_key_error_msg"  style="color:#F00;font-weight:800"  > {{ $errors->first('metadescription') }}</div>@endif
          </div>
                </div>
        @endforeach
        @endif
        <?php /* <!--
        <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Meta Keywords ({{ Helper::lang_name() }}) <span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Keywords_fr" name="Meta_Keywords_fr" placeholder="Enter Meta Keywords in French"><?php echo $metakeyword_fr;?></textarea>
             <div id="meta_key_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
        --> */ ?>
         <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION') }}<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                      {{ Form::Textarea('Meta_Description',$metadescription,['class' => 'form-control', 'id'=>'Meta_Description'])}}
                       
             <div id="meta_desc_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
         
        @if(!empty($get_active_lang)) 
        @foreach($get_active_lang as $get_lang)
        <?php
        $get_lang_name = $get_lang->lang_code;
        $get_lang_code = $get_lang->lang_name;
        $Meta_Description_dynamic = 'pro_mdesc_'.$get_lang->lang_code;
        ?>
        <div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION') }}( {{ $get_lang_code }}) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
            <textarea id="Meta_Description_{{ $get_lang_name}}" placeholder="Enter Meta Description in {{ $get_lang_code}}" name="Meta_Description_{{ $get_lang_name}}" class="form-control" > {{ $products->$Meta_Description_dynamic}}</textarea>
            @if ($errors->has('metadescription_'.$get_lang_name.''))<div id="meta_desc_{{ $get_lang_name}}_error_msg"  style="color:#F00;font-weight:800"  > {{ $errors->first('metadescription_'.$get_lang_name.'') }}</div>@endif
          </div>
                </div>
        @endforeach
        @endif
        <?php /* <!--
        <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Meta Description ({{ Helper::lang_name() }})<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Description_fr" name="Meta_Description_fr"><?php echo $metadescription_fr ; ?></textarea>
             <div id="meta_desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
        -->*/ ?>
        
        <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Cash Back ({{ Helper::cur_sym() }})<span class="text-sub">*</span></label>
          <div class="col-lg-8">
            {{ Form::text('cash_price',$cash_pack,['class' => 'form-control','id' => 'cash_back', 'maxlength' => '5'])}}
                      
          </div>
                </div>
        
        <?php /*  Product Policy details */ ?>
                {{-- Cancel Policy starts --}}
                <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_APPLY_CANCEL')!= '') ? trans(Session::get('mer_lang_file').'.BACK_APPLY_CANCEL') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_CANCEL') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="radio" id="allow_cancel" name="allow_cancel"  value="1"  @if($products->allow_cancel=='1') checked @endif  onClick="setPolicyDisplay('cancel_tab', 'block')"> <label class="sample"  >{{ (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES') }}</label>
                        <input type="radio" id="notallow_cancel" name="allow_cancel"  value="0" @if($products->allow_cancel=='0') checked @endif    onclick="setPolicyDisplay('cancel_tab', 'none')"><label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO') }}</label>


                        <label class="sample"></label>
                    </div>
                </div>


                <div id="cancel_tab" style="display:none;">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION') }} <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5"  class="wysihtml5 form-control" rows="10"  name="cancellation_policy" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION')!= '') ? trans(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_CANCEL_DESCRIPTION') }}">{{ $products->cancel_policy }}</textarea>
                           <div id="cancellation_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                      
                 @if(!empty($get_active_lang)) 
                 @foreach($get_active_lang as $get_lang) 
                  <?php
                  $get_lang_name = $get_lang->lang_name;
                  $get_lang_code = $get_lang->lang_code;
                  $cancel_policy_dynamic = 'cancel_policy_'.$get_lang_code;
                 ?>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION') }} ({{ $get_lang_name }}) <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="cancellation_policy_{{$get_lang_name}}" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION')!= '') ? trans(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_CANCEL_DESCRIPTION') }}">{{ $products->$cancel_policy_dynamic }}</textarea>
                           <div id="cancellation_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <div class="form-group">
                      {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])) !!}
                        

                        <div class="col-lg-8">
                                <label for="text1" class="control-label ">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION') }}<span class="text-sub"></span></label>     
                                <input placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION') }}"  class="form-control" type="text" maxlength="3" id="cancellation_days" required name="cancellation_days" value='{{ $products->cancel_days }}'>
                                    <div id="cancellation_error_msg"  style="color:#F00;font-weight:800"> </div>         
                        </div>
                    </div>

                </div> 

                {{-- Cancel Policy ends --}}
                {{-- REturn Policy starts --}}
                <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_APPLY_RETURN')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_APPLY_RETURN') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_RETURN') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="radio" id="allow_return" name="allow_return"  value="1" @if($products->allow_return=='1') checked @endif   onclick="setPolicyDisplay('return_tab', 'block')" > <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES') }}</label>
                        <input type="radio" id="notallow_return" name="allow_return"  value="0" @if($products->allow_return=='0') checked @endif    onclick="setPolicyDisplay('return_tab', 'none')" ><label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO') }}</label>


                        {{ Form::label('','',['class'=>'sample']) }}
                    </div>
                </div>


                <div id="return_tab" style="display:none;">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION') }}  <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="return_policy" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_RETURN_DESCRIPTION') }}">{{ $products->return_policy }}</textarea>
                           <div id="return_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                     
                     @if(!empty($get_active_lang)) 
                     @foreach($get_active_lang as $get_lang)
                      <?php
                      $get_lang_name = $get_lang->lang_name;
                      $get_lang_code = $get_lang->lang_code;
                      $return_policy_dynamic = 'return_policy_'.$get_lang_code;
                     ?>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION') }} ({{ $get_lang_name }}) <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="return_policy_{{$get_lang_name}}" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_RETURN_DESCRIPTION') }}">{{ $products->$return_policy_dynamic }}</textarea>
                           <div id="return_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                   @endforeach
                   @endif   

                    <div class="form-group">
                        {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])) !!}

                        <div class="col-lg-8">
                                    <label for="text1" class="control-label">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION') }}<span class="text-sub"></span></label>
                                    <input placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION') }}"  class="form-control" type="text" maxlength="3" id="return_days" name="return_days" value="{{ $products->return_days }}">
                                    <div id="replacement_error_msg"  style="color:#F00;font-weight:800"> </div>         
                        </div>
                    </div>

                </div> 


                {{-- REturn Policy ends --}}
                {{-- Replacement Policy starts --}}
                <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_APPLY_REPLACEMENT')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_APPLY_REPLACEMENT') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_REPLACEMENT') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="radio" id="allow_replace" name="allow_replace"  value="1" @if($products->allow_replace=='1') checked @endif    onclick="setPolicyDisplay('replace_tab', 'block')"  > <label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES') }}</label>
                        <input type="radio" id="notallow_replace" name="allow_replace"  value="0" @if($products->allow_replace=='0') checked @endif      onclick="setPolicyDisplay('replace_tab', 'none')" ><label class="sample">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO') }}</label>


                        <label class="sample"></label>
                    </div>
                </div>

                <div id="replace_tab" style="display:none;">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION') }} <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="replacement_policy" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_REPLACE_DESCRIPTION') }}">{{ $products->replace_policy }}</textarea>
                           <div id="replacement_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                     
                     @if(!empty($get_active_lang)) 
                     @foreach($get_active_lang as $get_lang)
                      <?php
                      $get_lang_name = $get_lang->lang_name;
                      $get_lang_code = $get_lang->lang_code;
                      $replace_policy_dynamic = 'replace_policy_'.$get_lang_code;
                     ?>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')}} ({{ $get_lang_name }})<span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="replacement_policy_{{$get_lang_name}}" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_REPLACE_DESCRIPTION') }}">{{ $products->$replace_policy_dynamic }}</textarea>
                           <div id="replacement_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                    @endforeach
                    @endif  

                    <div class="form-group">
                        {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])) !!}

                        <div class="col-lg-8">
                                     <label for="text1" class="control-label ">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION') }}<span class="text-sub"></span></label><br>
                                    <input placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION') }}"  class="form-control" type="text" maxlength="3" id="replace_days" name="replace_days"  value="{{ $products->replace_days }}">
                                    <div id="replacement_error_msg"  style="color:#F00;font-weight:800"> </div>         
                        </div>
                    </div>

                </div> 
                {{-- Replacement Policy ends --}}

                <?php /*  Product Policy details ends */ ?>         


        
<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_IMAGE') }} <span class="text-sub">*</span><br><span  style="color:#999">({{ (Lang::has(Session::get('mer_lang_file').'.MER_MAX_5')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAX_5') : trans($MER_OUR_LANGUAGE.'.MER_MAX_5') }})</span></label>
                    {{ Form::hidden('file_get',$file_get,array('id'=>'file_get')) }}
          
          <?php 
              $file_get_path =  explode("/**/",$file_get,-1); 
                        $img_count = count($file_get_path);
           ?>
           <input type="hidden" name="old_image_count" id="old_image_count" value="<?php echo $img_count ?>">
          <span class="errortext red" style="color:red"><em>{{ (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS') : trans($MER_OUR_LANGUAGE.'.MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS') }} {{ $PRODUCT_WIDTH }} x {{ $PRODUCT_HEIGHT }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PIXELS') : trans($MER_OUR_LANGUAGE.'.MER_PIXELS') }}</em></span>

                 <div class="col-lg-8" id="img_upload">
          <?php $i=1; ?>
          
          @if(count($file_get_path) > 0 && $img_count !='') {{-- check image is availble --}}
      
            @foreach($file_get_path as $image)
        @if($image != '') {{-- image is null --}}
            
             @php
              $pro_img = $image;
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
              $img_data = "public/assets/product/".$pro_img; @endphp
                @if(file_exists($img_data))  {{-- image not exists in folder  --}}
                          @php
                                 $prod_path = url('').'/public/assets/product/'.$pro_img;
                           @endphp
                @else  
                   @if(isset($DynamicNoImage['productImg']))
                   @php                    
                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp    
                                                    @if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
                                                     @php   
                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp  
                                                    @endif
                                                                        
                                                 @endif
                                                                     
                                                                     
                                                @endif
                     
      
               @else
               
                 
                @if(isset($DynamicNoImage['productImg']))
                   @php                    
                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp    
                                                    @if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
                                                     @php   
                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp  
                                                    @endif
                                                                        
                                                 @endif
                                                                     
                                                                     
                                                @endif        
            
                    <div style="float:left; max-width:219px; background: #f5f5f5;padding: 5px;margin: 5px;">
                      {{ Form::hidden('image_name',$image,array('id'=>'image_name')) }}
            
                      <input type="file" name="file[]" value="" onchange="imageval({{ $i }})" id="fileUpload1" />
            
            <span>
              {{ Form::hidden('image_old[]',$image,array('id'=>'')) }}
            
              <img style="margin-top:10px; margin-bottom: 10px;" src="{{ $prod_path }}" height="80" > <br>
              <?php 
              /* Edit Image start - Trigger the modal with a button */ ?>
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick=" calImgEdit( {{ $products->pro_id }},{{ $i }},'{{ $image }}' )" >Edit</button><?php /* */ ?>
            @if($img_count > 1)
            <a class="btn btn-danger" href="{{ url('mer_delete_product_img')."/".$products->pro_id."/".$image}}" style="cursor: pointer;">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_REMOVE')!= '')  ?  trans(Session::get('mer_lang_file').'.BACK_REMOVE') : trans($MER_OUR_LANGUAGE.'.BACK_REMOVE') }}</a>
            @endif
            </span>           
          </div>  
          <?php $i++; ?>
      @endforeach       
      @else
      @php 
      $prod_path = url('').'/public/assets/default_image/No_image_product.png'; @endphp
             @if(isset($DynamicNoImage['productImg']))
                   @php                    
                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp    
                                                    @if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
                                                     @php   
                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp  
                                                    @endif
                                                                        
                                                 @endif
    
    
        <img src="{{ $prod_path }}" height="100" width="100"><br>&nbsp; &nbsp;
    @endif                  
           @if($img_count < 5)
          <div  id="img_upload">
           <a style="display:inline-block; padding:4px 20px; margin-bottom: 5px;" href="javascript:void(0);"  title="Add field" class="chose-file-add" ><span id="add_button" style="cursor:pointer;width:84px;">{{ (Lang::has(Session::get('mer_lang_file').'.BACK_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ADD') : trans($MER_OUR_LANGUAGE.'.BACK_ADD')}}</span></a>
                  </div>
          @endif
          {{ Form::hidden('count','1',array('id'=>'count')) }}
                  
                 </div> 
    <div class="form-group" style="width:100%; float:left; margin:0px; padding:0px;">
      {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'pass1'])) !!}
                    

                    <div class="col-lg-8">
                   <button class="btn btn-warning btn-sm btn-grad" id="submit_product" ><a style="color:#fff"  >{{ (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_PRODUCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE_PRODUCT'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE_PRODUCT')}}</a></button>
                     <a href="{{ url('mer_manage_product')}}" class="btn btn-default btn-sm btn-grad" style="color:#000">{{ (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK'): trans($MER_OUR_LANGUAGE.'.MER_BACK')}}</a>
                   
                    </div>
            
                </div>

                
         {{ Form::close() }}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->
<input id="sh_alert_count" value="0" type="hidden" />
    <!-- FOOTER -->
     {!! $adminfooter !!}
    <!--END FOOTER -->

 
       <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 
<?php /* if($productspec==1){  hiding, becoz of changing category it's need to show, even product doesn't added specification already!  */?>
<script>

  function check_sku_no()
     {
        var sku_no    = $('#sku_no').val();
       // alert(sku_no);
       var sku_pro_id = $("#product_edit_id").val();
        $.ajax({
            type: "POST",
            url : "{{ url('check_sku_no_exist')}}",
            data : {'sku_no':sku_no,'sku_pro_id':sku_pro_id},
            success:function(response)
            {
                console.log(response);
              if(response == 2)
              {   
                $("#sku_no").css('border', '1px solid red').focus();                 
                 $("#skuno_error_msg").html('SKU Number Already Exist');
                 return false;
              }
              else
              {
                $("#sku_no").css('border','');                
                 $("#skuno_error_msg").html('');
                 return true;
              }
            }
        });
     }
function show_alert()
{
  var i=$("#sh_alert_count").val();
  if(i=="0")
   var option=confirm("This will change your product specification!");
   var count_id = document.getElementById("specificationcount").value;

  if(option==0){
   location.reload();
   return false;
   } 
   else {
   var count_id = document.getElementById("specificationcount").value;
   count_id=parseInt(count_id);
   for(i=1;i<=count_id;i++){
   jQuery('#spec'+i).remove();
                }
   document.getElementById("specificationcount").value=0; 
        }
   i++;
  i=$("#sh_alert_count").val(i);
}
$( document ).ready(function() {
  /* get_specification_details(); */
});
</script>


<?php /* }  */?>
 <script language="JavaScript">
 function get_specification_details()
{
  var main_cat=$("#category").val();
  var sec_main_cat=$("#maincategory").val();

  /*Top Category*/  
  if(main_cat == "0"){
    $("#Product_Category").css('border', '1px solid red'); 
    $('#category_error_msg').html('Please Select Category');
    $("#Product_Category").focus();
    return false;
  }else{
    $("#Product_Category").css('border', ''); 
    $('#category_error_msg').html('');
  }
  /*Main Category*/ 
  if(sec_main_cat == "0")
  {
    $("#Product_MainCategory").css('border', '1px solid red'); 
    $('#main_cat_error_msg').html('Please Select Main Category');
    $("#Product_MainCategory").focus();
    return false;
  }else{
    $("#Product_MainCategory").css('border', ''); 
    $('#main_cat_error_msg').html('');
  }
  if(sec_main_cat!="" && main_cat!=""&& sec_main_cat!="0" && main_cat!="0")
  {
    $.post("<?php echo url(''); ?>/get_spec_related_to_cat_mer",
    {
      main_cat: main_cat,
      sec_main_cat: sec_main_cat
    },
    function(data, status){
      
      var count_id = document.getElementById("specificationcount").value;
      var count_id=parseInt(count_id);
     
      if(data==1){
          $("input[name='specification'][value='2']").attr('checked',true);
          document.getElementById('specification_list_avail').style.display="none";
          document.getElementById('specification_list_notavail').style.display="inline";
          document.getElementById('sub4').style.display ="none";
          document.getElementById('divspecificationTxt').style.display ="none";
          document.getElementById('addmore').style.display ="none";

                 return false;   
                }
      
        $("#spec_grp0").html(data);
        $("input[name='spectext0']").val("");
        $("#pro_spec0").html("<option value='0'>-- Select --</option>");
        document.getElementById('specification_list_avail').style.display="block";
        document.getElementById('specification_list_notavail').style.display="none";
        for(var i=1;i<=count_id;i++)
        {
          $("#spec_grp"+i).html(data);
           $("input[name='spectext"+i+"']").val("");
          $("#pro_spec"+i).html("<option value='0'>-- Select --</option>");
        document.getElementById('specification_list_avail').style.display="block";
        document.getElementById('specification_list_notavail').style.display="none";
        } 
      
    });
  }
  else
  {
    $("input[name='specification'][value='2']").attr('checked', true);
    return false;
  }
}
 function spcfunction(count,spc_grop_id){

  
  var pass = 'spc_group_id='+spc_grop_id;

  $.ajax( {
            type: 'get',
          data: pass,
          url:  "<?php echo url('product_get_spec');?>",
          success: function(responseText){  
           
           if(responseText)
           {  
           
          $('#pro_spec'+count).html(responseText);             
           }
        }   
      });   
}





 /*add/remove Product image */
 $(document).ready(function(){
  var old_image_count = $('#old_image_count').val();
    var maxField = 6 - old_image_count; //Input fields increment limitation
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#img_upload'); //Input field wrapper //div
  
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
      var fieldHTML = '<div style="display:block; clear:both; width: 350px; margin-top:15px;"><input type="file" name="new_file[]" value="" id="fileUpload'+x+'" onchange="imageval('+x+')" required/><div id="remove_button"><a href="javascript:void(0);"  title="Remove field" style="color:#ffffff;">Remove</a></div></div>'; //New input field html 
            $(wrapper).append(fieldHTML); // Add field html
      document.getElementById('count').value = parseInt(x);
        }
    });
    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    document.getElementById('count').value = parseInt(x);
    });
});

function setVisibility(id, visibility) {


document.getElementById(id).style.display = visibility;
document.getElementById('addmore').style.display =visibility;
 
}
function setVisibility1(id, visibility) {


document.getElementById(id).style.display = visibility;
document.getElementById('colordiv').style.display =visibility;
 
}
function setshipVisibility(id, visibility) 
{
document.getElementById(id).style.display = visibility;
 
}
function product_siz(id, visibility)
 {
document.getElementById(id).style.display = visibility;
document.getElementById('pro_size').style.display =visibility;
document.getElementById('sizediv').style.display =visibility;
}
</script>
<script type='text/javascript'>


    
$(document).ready(function(){
 
    var counter = 2;
    $('#del_file').hide();
    $('img#add_file').click(function(){
        $('#file_tools').before('<br><div class="col-lg-8" id="f'+counter+'"><input name="file[]" type="file"></div>');
        $('#del_file').fadeIn(0);
    counter++;
    });
    $('img#del_file').click(function(){
        if(counter==3){
            $('#del_file').hide();
        }   
        counter--;
        $('#f'+counter).remove();
    });
});

   function addspecificationFormField()
  {
    var count_id = document.getElementById("specificationcount").value;
 
    var selectspec=$("#spec"+count_id+" option:selected").val();
    var spectext=$("#spectext"+count_id).val();
 
    if(selectspec!=0  && spectext!=""){

    var id       = document.getElementById("specificationid1").value;
    var count_id = document.getElementById("specificationcount").value;
    var nameid   = parseInt(count_id)+1;
    $("#spec_grp0").find('option:selected').removeAttr("selected");
    var result=$("#spec_grp0").html();
    lang_div ='';
     <?php if(!empty($get_active_lang)) { 
            foreach($get_active_lang as $get_lang) {
            $get_lang_name = $get_lang->lang_name;
            $get_lang_code = $get_lang->lang_code; 
            $spc_value_dynamic = 'spc_value_'.$get_lang_code; 
            ?>  
          lang_div += "<div class='col-lg-3'><input type='text' class='form-control' name='<?php echo $get_lang_code;?>_spectext"+ nameid  + "' placeholder='Specification in <?php echo $get_lang_name;?>' required/></div>";
          <?php } } ?>
    //if(count_id < 2){
      document.getElementById('specificationcount').value = parseInt(count_id)+1;
      jQuery("#divspecificationTxt").append(" <div class='form-group' id='spec"+ nameid + "'><div class='col-lg-2'></div><div class='col-lg-2'><select name='spec_grp"+ nameid  + "' id='spec_grp"+ nameid  + "' onChange='spcfunction("+nameid+",this.value);' class='form-control' required><option  value='0'>-- select specification Group--</option><?php foreach ($spec_group as $pro_spec_group) {?><option  value='<?php echo $pro_spec_group->spg_id;?>'><?php echo $pro_spec_group->spg_name;?></option><?php } ?></select></div><div class='col-lg-2'><select name='spec"+ nameid  + "' id='pro_spec"+nameid+"' class='form-control' required ><option  value='0'>-- select specification--</option></select></div><div class='col-lg-2'><input type='text' class='form-control' name='spectext"+ nameid  + "' required /></div>"+lang_div+"<div class='col-lg-2'><a class='pro-des-rem' href='#' onClick='removespecFormField(\"#spec" + nameid + "\"); return false;' style='width:100%;display:inline-block; text-align:center;' >Remove</a></div></div>");   

     id = (id - 1) + 1;
     document.getElementById("specificationid1").value = id;

      //}
    //else
    //{ 
    //alert("<?php //if (Lang::has(Session::get('mer_lang_file').'.MER_MAXIMUM_LIMIT_REACHED')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MAXIMUM_LIMIT_REACHED');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MAXIMUM_LIMIT_REACHED');} ?>");
    //return false;
    //}
    }
    else
    {
      alert("{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE') }}");
    }     

}
function removespecFormField(id) {
  
  var count_id = document.getElementById("specificationcount").value;
   count_id=count_id-1;

document.getElementById("specificationcount").value=count_id; 

        jQuery(id).remove();
    }

  
    function addimgFormField() {
    var id = document.getElementById("aid").value;
    var count_id = document.getElementById("count").value;    
    if(count_id < 4){
    document.getElementById('count').value = parseInt(count_id)+1;
      jQuery.noConflict()
      jQuery("#divTxt").append("<tr style='height:5px;' > <td> </td> </tr><tr id='row" + id + "' style='width:100%'><td width='20%'><input type='file' name='file_more"+count_id+"' /></td><td>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + id + "\"); return false;' style='color:#F60;' >Remove</a></td></tr>");     
         jQuery('#row' + id).highlightFade({    speed:1000 });
     id = (id - 1) + 2;
     document.getElementById("aid").value = id;
  } 
    }
       
      function removeFormField(id) {

        jQuery(id).remove();
    }

$(document).ready(function(){
      var counter = 2;
  $('#add_spec').click(function(){
        $('#file_tools').before('<br><div class="col-lg-8" id="f'+counter+'"><input name="file[]" type="file"></div>');
        $('#del_file').fadeIn(0);
    counter++;
    });
    $('img#del_file').click(function(){
        if(counter==3){
            $('#del_file').hide();
        }   
        counter--;
        $('#f'+counter).remove();
    });
});

function isNumberKey(evt){
         var charCode = (evt.which) ? evt.which : event.keyCode        
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
            
         return true;
          
      }
  function getshop_ajax(id)
  {
  
    var passmerchantid = 'id='+id;
       $.ajax( {
            type: 'get',
          data: passmerchantid,
          url: 'product_getmerchantshop_ajax',
          success: function(responseText){  

           if(responseText)
           {  //alert(responseText);
          $('#Select_Shop').html(responseText);            
           }
        }   
      });   
  }

  function getcolorname(id)
  {
  
     var passcolorid = 'id='+id+"&co_text_box="+$('#co').val();
  
       $.ajax( {
            type: 'get',
          data: passcolorid,
          url:'<?php echo url('product_getcolor'); ?>',
          success: function(responseText){  
           if(responseText)
           {   
         
          var get_result = responseText.split(',');
          if(get_result[3]=="success")
          {
            $('#colordiv').css('display', 'block'); 
            
          $('#showcolor').append('<span style="width:195px;padding:5px; display:inline-block;border:4px solid '+get_result[2]+';margin:5px 5px 5px 0px">'+get_result[0]+'<input type="checkbox"  name="colorcheckbox'+get_result[1]+'"style="" checked="checked" value="1" ></span>');      
          var co_name_js = $('#co').val();      
          var co_name_js = $('#co').val();  
          $('#co').val(get_result[1]+","+co_name_js);   
            
          }
          else if(get_result[3]=="failed")
          {
            alert("{{ (Lang::has(Session::get('mer_lang_file').'.MER_ALREADY_COLOR_IS_CHOOSED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ALREADY_COLOR_IS_CHOOSED')  : trans($MER_OUR_LANGUAGE.'.MER_ALREADY_COLOR_IS_CHOOSED') }}.");
          }
          else
          {
              alert("{{ (Lang::has(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')  : trans($MER_OUR_LANGUAGE.'.MER_SOMETHING_WENT_WRONG') }}.");
          }
          
           }
        }   
      });   
    
  }
function getsizename(id)
  {
     
     var passsizeid = 'id='+id+"&si_text_box="+$('#si').val();
   
       $.ajax( {
            type: 'get',
          data: passsizeid,
          url: '<?php echo url('product_getsize'); ?>',
          success: function(responseText){  
           if(responseText)
           {   
          
          var get_result = responseText.split(',');
          if(get_result[3]=="success")
          {
                                      var count=parseInt($('#productsizecount').val())+1;
            $("#productsizecount").val(count);
            $('#sizediv').css('display', 'block'); 
            //$('#quantitydiv').css('display', 'block'); 
            
          $('#showsize').append('<div class="size-view"><span style="padding-right:5px;">Select Size:</span><span style="color:#000000;">'+get_result[2]+'<input type="checkbox"  name="sizecheckbox'+get_result[1]+'"style="" checked="checked" value="1" ></span></div>');
          $('#showquantity').append('<input type="text" name="quantity'+get_result[1]+'" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input> ');
    
          var co_name_js = $('#si').val();  
          $('#si').val(get_result[1]+","+co_name_js);   
            
          
          }
          else if(get_result[3]=="failed")
          {
            alert("{{ (Lang::has(Session::get('mer_lang_file').'.MER_ALREADY_SIZE_IS_CHOOSED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ALREADY_SIZE_IS_CHOOSED') : trans($MER_OUR_LANGUAGE.'.MER_ALREADY_SIZE_IS_CHOOSED') }}.");
          }
          else
          {
              alert("{{ (Lang::has(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')  : trans($MER_OUR_LANGUAGE.'.MER_SOMETHING_WENT_WRONG') }} .");
          }
          
           }
        }   
      });   
    
  }
  
  function select_main_cat(id){
       var passData = 'id='+id;
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('product_getmaincategory'); ?>',
          success: function(responseText){  
           if(responseText)
           { 
              // alert(responseText);
          $('#maincategory').html(responseText);    
          $('#subcategory').html(0);  
          $('#secondsubcategory').html(0);          
           }
        }   
      });   
  }
  
  function select_sub_cat(id)
  {
    var passData = 'id='+id;
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('product_getsubcategory'); ?>',
          success: function(responseText){  
           if(responseText)
           { 
          $('#subcategory').html(responseText);            
           }
        }   
      });   
  }
  
  function select_second_sub_cat(id)
  {
    var passData = 'id='+id;
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('product_getsecondsubcategory'); ?>',
          success: function(responseText){  
           if(responseText)
           { 
          $('#secondsubcategory').html(responseText);            
           }
        }   
      });   
  }
  // Onload to get selected category
  $( document ).ready(function() {
    var passmerchantid = 'mer_id=<?php echo Session::get('merchantid'); ?>&store_id=<?php echo $shop;?>';
    $.ajax( {
            type: 'get',
          data: passmerchantid,
          url: '<?php echo url('product_mer_shop_selected_merchant'); ?>',
          success: function(responseText){  
           if(responseText)
           {  
          $('#Select_Shop').html(responseText);            
           }
        }   
      });   
 
  

  //var passData = 'edit_id=<?php //echo $maincategory; ?>';
var passData = 'edit_id=<?php echo $maincategory; ?>&edit_id_top=<?php echo $category_get;?>';
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('product_edit_getmaincategory'); ?>',
          success: function(responseText){  
           if(responseText)
           { 
              
          $('#maincategory').html(responseText);             
           }
        }   
      }); 
  //var passData = 'edit_sub_id=<?php //echo $subcategory; ?>';

   var passData = 'edit_sub_id=<?php echo $subcategory; ?>&edit_id_main=<?php echo $maincategory; ?>';
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('product_edit_getsubcategory'); ?>',
          success: function(responseText){ // alert(responseText);
           if(responseText)
           { 
            // alert(responseText);  
          $('#subcategory').html(responseText);            
           }
        }   
      }); 
        var passData = 'edit_second_sub_id=<?php echo $secondsubcategory; ?>&edit_sub_id=<?php echo $subcategory; ?>';
  
  //var passData = 'edit_second_sub_id=<?php echo $secondsubcategory; ?>';
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('product_edit_getsecondsubcategory'); ?>',
          success: function(responseText){  
           if(responseText)
           { 
 
            //alert(responseText);
          $('#secondsubcategory').html(responseText);            
           }
        }   
      }); 
  });
function imageval(i){ /*Image size validation*/
   
  var img_count = $("#count").val();
  //Get reference of FileUpload.

    var fileUpload = document.getElementById("fileUpload"+i);

  //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
   if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (width <= 500 || height <= 700) {
                        //alert("Image Height and Width should have 500 * 700 px.");
                        return false;
                    }
                   // alert("Uploaded image has valid Height and Width.");
                    return true;
                };
 
            }
        } else {
          //  alert("This browser does not support HTML5.");
          //  return false;
       }
   } else {
       // alert("Please select a valid Image file.");
       // return false;
   }
} 
    
    
  $( document ).ready(function() {

      $('#specificationdetails').hide();
      var title        = $('#Product_Title');
      var title_fr     = $('#Product_Title_fr');
      var category     = $('#Product_Category');
      var maincategory   = $('#Product_MainCategory');
      var subcategory    = $('#Product_SubCategory');
      var secondsubcategory= $('#Product_SecondSubCategory');
      var originalprice    = $('#Original_Price');
      var inctax       = $('#inctax');
      var discountprice    = $('#Discounted_Price');
      var shippingamt      = $('#Shipping_Amount');
      var description      = $('#Description');
      var description_fr   = $('#description_fr');
      var wysihtml5      = $('#wysihtml5');
      var shop       = $('#Select_Shop');
      var metakeyword    = $('#Meta_Keywords');
      var metakeyword_fr   = $('#Meta_Keywords_fr');
      var metadescription  = $('#Meta_Description');
      var metadescription_fr   = $('#Meta_Description_fr');
      var file       = $('#fileUpload');
      var pquantity    = $('#Quantity_Product');
      var sku_no    = $('#sku_no');
       var no_of_purchase = $('#no_of_purchase');
      var desc             = $('#desc');
      var cash_back    = $("#cash_back");

     
       //if(taxchecked==1)
    

  /*Product Quantity*/
  $('#Quantity_Product').keypress(function (e){
    if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
        pquantity.css('border', '1px solid red'); 
      $('#qty_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      pquantity.focus();
      return false;
    }else{      
            pquantity.css('border', ''); 
      $('#qty_error_msg').html('');         
    }
    });

  /*Product Original Price*/
  $('#Original_Price').keypress(function (e){
    if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
        originalprice.css('border', '1px solid red'); 
      $('#org_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      originalprice.focus();
      return false;
    }else{      
            originalprice.css('border', ''); 
      $('#org_price_error_msg').html('');         
    }
    });

  /*Product Discount Price*/
  $('#Discounted_Price').keypress(function (e){
    if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
        discountprice.css('border', '1px solid red'); 
      $('#dis_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      discountprice.focus();
      return false;
    }else{      
            discountprice.css('border', ''); 
      $('#dis_price_error_msg').html('');         
    }
    }); 

  /*Product Shipping Amount*/
  $('#Shipping_Amount').keypress(function (e){
    if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
        shippingamt.css('border', '1px solid red'); 
      $('#ship_amt_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      shippingamt.focus();
      return false;
    }else{      
            shippingamt.css('border', ''); 
      $('#ship_amt_error_msg').html('');          
    }
    });

  /*Delivery Days*/
  $('#Delivery_Days').keyup(function() { 
    if (this.value.match(/[^0-9-()\s]/g)){ 
    this.value = this.value.replace(/[^0-9-()\s]/g, ''); 
    } 
  });

  /*Cash back*/
  $('#cash_back').keyup(function() { 
    if (this.value.match(/[^0-9-()\s]/g)){ 
    this.value = this.value.replace(/[^0-9-()\s]/g, ''); 
    } 
  });
  
    /*including tax*/
  $('#inctax').keypress(function (e){
    if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
        inctax.css('border', '1px solid red'); 
      $('#tax_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      inctax.focus();
      return false;
    }else{      
            inctax.css('border', ''); 
      $('#tax_error_msg').html('');         
    }
    }); 
    
  
 $('#submit_product').click(function() {

   var checkedoptionsize  = $('input:radio[name=pro_siz]:checked').val();
   var sizeid       = $("#Product_Size option:selected").val();
   var checkedoptioncolor = $('input:radio[name=productcolor]:checked').val();
   var colorid      = $("#selectprocolor option:selected").val();
     var checkspec      = $('input:radio[name=specification]:checked').val();
   var sizecnt      = $("#productsizecount").val();
     var shipamtchecked   = $('input:radio[name=shipamt]:checked').val();
    
  /*Product Title*/
    if($.trim(title.val()) == ""){
      title.css('border', '1px solid red'); 
      $('#title_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_TITLE') }}');
      title.focus();
      return false;
    }else{
      title.css('border', ''); 
      $('#title_error_msg').html('');
    }
  <?php 
    /* print_r($get_active_lang); */
    if(!empty($get_active_lang)) { 
    foreach($get_active_lang as $get_lang) {
    $get_lang_code = $get_lang->lang_code;
    $get_lang_name = $get_lang->lang_name;
    ?>
    if($.trim(title_<?php echo $get_lang_code; ?>.val()) == ""){
      title_fr.css('border', '1px solid red'); 
      $('#title_<?php echo $get_lang_code; ?>_error_msg').html('Please Enter Title In <?php echo $get_lang_name; ?>');
      title_<?php echo $get_lang_code; ?>.focus();
      return false;
    }else{
      title_<?php echo $get_lang_code; ?>.css('border', ''); 
      $('#title_<?php echo $get_lang_code; ?>_error_msg').html('');
    }
    <?php } }?>
  /*Top Category*/  
    if(category.val() == 0){
      category.css('border', '1px solid red'); 
      $('#category_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') :  trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }}');
      category.focus();
      return false;
    }else{
      category.css('border', ''); 
      $('#category_error_msg').html('');
    }
  /*Main Category*/ 
    if(maincategory.val() == 0)
    {
      maincategory.css('border', '1px solid red'); 
      $('#main_cat_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_MAIN_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_MAIN_CATEGORY') }}');
      maincategory.focus();
      return false;
    }else{
      maincategory.css('border', ''); 
      $('#main_cat_error_msg').html('');
    }
    
    /*if(subcategory.val() == 0)
    {
      subcategory.css('border', '1px solid red'); 
      $('#error_msg').html('Please Select Sub Category');
      subcategory.focus();
      return false;
    }
    else
    {
    subcategory.css('border', ''); 
    $('#error_msg').html('');
    }
    if(secondsubcategory.val() == 0)
    {
      secondsubcategory.css('border', '1px solid red'); 
      $('#error_msg').html('Please Enter Select Sub Category');
      secondsubcategory.focus();
      return false;
    }
    else
    {
    secondsubcategory.css('border', ''); 
    $('#error_msg').html('');
    }
    */  
    /*brand*/ 
   /* if($("#product_brand").val() == 0){
      $("#product_brand").css('border', '1px solid red'); 
      $('#brand_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_BRAND')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_BRAND'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_BRAND')}}');
      $("#product_brand").focus();
      return false;
    }else{
      $("#product_brand").css('border', ''); 
      $('#brand_error_msg').html('');
    }*/
    /*sku number*/
    if(sku_no.val() == ''){
      sku_no.css('border', '1px solid red'); 
      $('#skuno_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER'): trans($MER_OUR_LANGUAGE.'.MER_ENTER_SKU_NUMBER')}}');
      sku_no.focus();
      return false;
    }else{
      sku_no.css('border', ''); 
      $('#skuno_error_msg').html('');
    }
    /*Product Qunatity*/
    if(pquantity.val() == 0){
      pquantity.css('border', '1px solid red'); 
      $('#qty_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_PRODUCT_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_PRODUCT_QUANTITY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_PRODUCT_QUANTITY')}}');
      pquantity.focus();
      return false;
    }
    else if(parseInt(pquantity.val()) <= parseInt(no_of_purchase.val()) )
      {  
        pquantity.css('border', '1px solid red'); 
        $('#qty_error_msg').html('Product quantity should be greater than number of purchase ' + $('#no_of_purchase').val());
        pquantity.focus();
        return false;
      }
    
    else{
      pquantity.css('border', ''); 
      $('#qty_error_msg').html('');
    }

    /*Product Original Price*/
    if(originalprice.val() == 0){
      originalprice.css('border', '1px solid red'); 
      $('#org_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_ORIGINAL_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_ORIGINAL_PRICE') }}');
      originalprice.focus();
      return false;
    }else if(isNaN(originalprice.val()) == true){
      originalprice.css('border', '1px solid red'); 
      $('#org_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      originalprice.focus();
      return false;
    }else{
      originalprice.css('border', ''); 
      $('#org_price_error_msg').html('');
    }

    /*Product Discount Price*/
    if(discountprice.val() == 0){
      discountprice.css('border', '1px solid red'); 
      $('#dis_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DISCOUNT_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DISCOUNT_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DISCOUNT_PRICE') }}');
      discountprice.focus();
      return false;
    }else if(isNaN(discountprice.val()) == true){
      discountprice.css('border', '1px solid red'); 
      $('#dis_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED') }}');
      discountprice.focus();
      return false;
    }else if(parseInt(discountprice.val()) > parseInt(originalprice.val()) ){
      discountprice.css('border', '1px solid red'); 
      $('#dis_price_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE')}}');
      discountprice.focus();
      return false;
    }else{
      discountprice.css('border', ''); 
      $('#dis_price_error_msg').html('');
    }
    if(parseInt($('#inctax').val()) > 100){
      inctax.css('border', '1px solid red'); 
      $('#tax_error_msg').html('Tax Should Less than 100%');
      inctax.focus();
      return false;
    }else{
      inctax.css('border', ''); 
      $('#tax_error_msg').html('');
    }
    if ($('#inctax_check').is(":checked"))
       //if(taxchecked==1)
    {
     
    if(parseInt($('#inctax').val()) ==0){
      inctax.css('border', '1px solid red'); 
      $('#tax_error_msg').html('Tax Should not be  Less than 1%');
      inctax.focus();
      return false;
    }else{
      inctax.css('border', ''); 
      $('#tax_error_msg').html('');
    }
  }
    /*Shipping Amount*/
    if(shipamtchecked==2){
      if(shippingamt.val()==""){
        shippingamt.css('border', '1px solid red'); 
        $('#ship_amt_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_SHIPPING_AMOUNT'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_SHIPPING_AMOUNT') }}');
        shippingamt.focus();
        return false;
      }else{
        shippingamt.css('border', ''); 
        $('#ship_amt_error_msg').html('');
      }
    }else if(shipamtchecked==1){
        shippingamt.css('border', ''); 
        $('#ship_amt_error_msg').html('');
    }

    /*Description*/
    if($.trim(wysihtml5.val()) == ''){
      wysihtml5.css('border', '1px solid red'); 
      $('#desc_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DESCRIPTION')}} {{ $default_lang }}');
      wysihtml5.focus();
      return false;
    }else{
      wysihtml5.css('border', ''); 
      $('#desc_error_msg').html('');
    }
    <?php 
    /* print_r($get_active_lang); */
    if(!empty($get_active_lang)) { 
    foreach($get_active_lang as $get_lang) {
    $get_lang_code = $get_lang->lang_code;
    $get_lang_name = $get_lang->lang_name;
    ?>
    if($.trim(wysihtml5.val()) == ''){
      
      wysihtml5.css('border', '1px solid red'); 
      $('#desc_<?php echo $get_lang_code; ?>_error_msg').html('Please Enter Description in <?php echo $get_lang_name; ?>');
      wysihtml5.focus();
      return false;
    }else{
      wysihtml5.css('border', ''); 
      $('#desc_<?php echo $get_lang_code; ?>_error_msg').html('');
    }
    <?php } }?>
    
    /*Specification*/
    if(checkspec==1){ 
      var cntspec=$('#specificationcount').val();
      var i=0;
      for(i=0;i<=cntspec;i++){
        var specid=$("#spec"+i+" option:selected").val();
        var spectxt=$("#spec"+i).val();
        if(specid!=0 && spectxt!==""){
        var success=1;  
        }else{
          var success=0;  
        }
       } //for
      /*if(success==0){
        i=i-1;
            $('#spec_error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT');} ?>');
            $('#spec'+i).css('border', '1px solid red');
            $('#spectext'+i).css('border', '1px solid red');  
            return false;
      }else{
        i=i-1;
        $('#spec_error_msg').html(' ');
            $('#spec'+i).css('border', '1px solid lightgray');
            $('#spectext'+i).css('border', '1px solid lightgray');  
      }*/
    }

/*Product size
  if($('input:radio[name=pro_siz]:checked').val()==0){
    if($("#Product_Size option:selected").val()<1){
        $('#size_error_msg').html('Please Select Product size');
      $("#Product_Size").css('border', '1px solid red'); 
      $("#Product_Size").focus();
      return false;
    }else{
       $('#size_error_msg').html('');
       $("#Product_Size").css('border', ''); 
    }
  }*/
    
/*Product color
    if($('input:radio[name=productcolor]:checked').val()==0){
      if($("#selectprocolor option:selected").val()<1){
        $('#color_error_msg').html('Please select color');
        $("#selectprocolor").css('border', '1px solid red'); 
        $("#selectprocolor").focus();
        return false;
      }else{ 
        $("#selectprocolor").css('border', ''); 
        $('#color_error_msg').html('');
      }
    }*/
  /*Meta Keyword
    if($.trim(metakeyword.val()) == ""){
      metakeyword.css('border', '1px solid red'); 
      $('#meta_key_error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METAKEYWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METAKEYWORD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_METAKEYWORD');} ?>');
      metakeyword.focus();
      return false;
    }else{
      metakeyword.css('border', ''); 
      $('#meta_key_error_msg').html('');
    }*/

   /*Meta description
    if($.trim(metadescription.val()) == ""){
      metadescription.css('border', '1px solid red'); 
      $('#meta_desc_error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_META_DESCRIPTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_META_DESCRIPTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_META_DESCRIPTION');} ?>');
      metadescription.focus();
      return false;
    }else{
      metadescription.css('border', ''); 
      $('#meta_desc_error_msg').html('');
    }*/

  /*Product Image*/
    /*var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if(file.val() == ""){
      return true;
    }else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {        
      file.focus();
      file.css('border', '1px solid red'); 
      $('#img_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_PRODUCT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_PRODUCT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_PRODUCT_SIZE') }}');
      return false;
    }else{
      file.css('border', ''); 
      $('#img_error_msg').html('');       
    }*/
  }); 
  
  });
  </script>
    
    <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

         <!-- PAGE LEVEL SCRIPTS -->
     <script src="{{ url('') }}/public/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="{{ url('') }}/public/assets/js/editorInit.js"></script>

    
<?php /* Edit Image starts */ ?>
  <?php //text editor hidded by this script ,so commanded
   /* <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script> */?>
    <script type="text/javascript" src="{{ url('') }}/public/assets/cropImage/editImage/js/jquery.canvasCrop.js" ></script>

    <script type="text/javascript">
      function calImgEdit(prodId,imgId,imgFileName){    
        $("#product_id").val(prodId);
        $("#img_id").val(prodId);
        $("#imgfileName").val(imgFileName);

        var rot = 0,ratio = 1;
        var CanvasCrop = $.CanvasCrop({
            cropBox : ".imageBox",
            imgSrc : "<?php echo url('');?>/public/assets/product/"+imgFileName,
            limitOver : 2
        });
        
        
        $('#upload-file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                CanvasCrop = $.CanvasCrop({
                    cropBox : ".imageBox",
                    imgSrc : e.target.result,
                    limitOver : 2
                });
                rot =0 ;
                ratio = 1;
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        });
        
        $("#rotateLeft").on("click",function(){
            rot -= 90;
            rot = rot<0?270:rot;
            CanvasCrop.rotate(rot);
        });
        $("#rotateRight").on("click",function(){
            rot += 90;
            rot = rot>360?90:rot;
            CanvasCrop.rotate(rot);
        });
        $("#zoomIn").on("click",function(){
            ratio =ratio*0.9;
            CanvasCrop.scale(ratio);
        });
        $("#zoomOut").on("click",function(){
            ratio =ratio*1.1;
            CanvasCrop.scale(ratio);
        });
        $("#alertInfo").on("click",function(){
            var canvas = document.getElementById("visbleCanvas");
            var context = canvas.getContext("2d");
            context.clearRect(0,0,canvas.width,canvas.height);
        });
        
        $("#crop").on("click",function(){
            
            //var src = CanvasCrop.getDataURL("jpeg");
            var src = CanvasCrop.getDataURL("png");
            //$("body").append("<div style='word-break: break-all;'>"+src+"</div>");  
            //$("#model_body").append("<img src='"+src+"' />");
            $("#showCroppedImg").html("<img src='"+src+"' />");
            $("#croppedImg_base64").val(src);
            if(src!='')
                $("#dev_uploadBtn").css('display','block');


        });
        
        console.log("ontouchend" in document);


        /* for load modal content from  */
        /*$.ajax({
          type: 'POST',   
          url:"<?php //echo url('');?>/ajaxEditImage",
          data:{prodId:prodId,imgId:imgId,imgFileName:imgFileName},
          success:function(response){
            //alert(response);
            $('.dev_appendEditData').html(response);
            return false;
          }
        });*/
      }
      </script>



<?php /* Edit Image ends */ ?>
   {{-- Policy details --}}
<script type="text/javascript">
  $(document).ready(function() {
    var allow_cancel = $('input[name=allow_cancel]:checked').val();
    var allow_return = $('input[name=allow_return]:checked').val();
    var allow_replace = $('input[name=allow_replace]:checked').val();
    if(allow_cancel==1)
      setPolicyDisplay('cancel_tab', 'block');
    else  
     setPolicyDisplay('cancel_tab', 'none');
    if(allow_return==1)
      setPolicyDisplay('return_tab', 'block');
    else  
     setPolicyDisplay('return_tab', 'none');
    if(allow_replace==1)
      setPolicyDisplay('replace_tab', 'block');
    else  
     setPolicyDisplay('replace_tab', 'none');
});

  function setPolicyDisplay(id, displayOption) 
  {
      $("#"+id).css('display',displayOption);
  } 
</script>
{{-- Policy details --}} 

  <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
  <script>
       // $(function () { formWysiwyg(); });
     $(document).ready(function () {
  $('.wysihtml5').wysihtml5();
});
        </script>
    
    
    <script>
  $('#replace_days').keydown(function (e) 
  {
     if (e.shiftKey || e.ctrlKey || e.altKey)
     {
      e.preventDefault();
     } 
     else 
     {
      var key = e.keyCode;
      if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
       {
        e.preventDefault();
       }
    }
  });
  
  $('#return_days').keydown(function (e) 
  {
     if (e.shiftKey || e.ctrlKey || e.altKey)
     {
      e.preventDefault();
     } 
     else 
     {
      var key = e.keyCode;
      if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
       {
        e.preventDefault();
       }
    }
  });
  
  $('#cancellation_days').keydown(function (e) 
  {
     if (e.shiftKey || e.ctrlKey || e.altKey)
     {
      e.preventDefault();
     } 
     else 
     {
      var key = e.keyCode;
      if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
       {
        e.preventDefault();
       }
    }
  });
  
  
  </script>
    
    

</body>
     <!-- END BODY -->
</html>
