<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo $SITENAME; ?> | Edit Services</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); if(count($favi)>0) { foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo url(''); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php } ?>	
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/bootstrap-wysihtml5-hack.css" />
     <style>
                        ul.wysihtml5-toolbar > li {
                            position: relative;
                        }
                    </style>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

       
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a>Home</a></li>
                                <li class="active"><a >Edit Services</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Edit Services</h5>
            
        </header>
		
		
@if ($errors->any())
         <br>
		 <ul style="color:red;">
		{!! implode('', $errors->all('<li>:message</li>')) !!}
		</ul>	
		@endif
        @if (Session::has('message'))
		<p style="background-color:green;color:#fff;">{!! Session::get('message') !!}</p>
		@endif
 
		@foreach($get_services as $services)
<?php 

$calendar = $services->calendar_option;

$store_id = $services->store_name;
?>
<div class="row">
        <div id="div-1" class="accordion-body collapse in body col-lg-11 panel_marg" style="padding-bottom:10px;">
               {!! Form::open(array('url'=>'mer_edit_services_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}


		<input type="hidden" name="service_id" value="<?php echo $services->service_id; ?>" />
		<div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
		
		 <div class="panel panel-default">

                <div class="panel-heading"> Edit Services</div>
				<div class="panel-body">
		        <div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Service Name<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <input id="service_name" placeholder="Enter Service Name" name="service_name" class="form-control" type="text" value="{{$services->service_name}}">
										 @if ($errors->has('service_name'))
                                <p class="error-block" style="color:red;">{{ $errors->first('service_name') }}</p> @endif
                                    </div>
                                </div>
								</div>
                               
				
								<div class="panel-body">

                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-3">Service Type</label>

                    <div class="col-lg-4">
		
			 <select class="form-control" name="service_type" id="service_type" >
                        <option value="0">Select Service Type</option>
                       <?php foreach($getservicetypes as $service_type)  { ?>
              			<option value="<?php echo $service_type->service_type_id; ?>" <?php if($service_type->service_type_id ==  $services->service_type) { ?> selected <?php } ?> ><?php echo $service_type->service_type_name; ?></option>
                        <?php } ?>
			 </select>			

                    </div>
                    <label for="text2">  More Custom Service Type  <a href="<?php echo url('')?>/mer_manage_service_type"> ADD</a></label>
                </div></div>
                 <input type="hidden" name="old_count" value=<?php echo count($get_service_duration);?>>
								
								<a href="javascript:void(0);"  title="Add field"  id="add_button" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-plus" ></span></a>
								<?php foreach($get_service_duration as $duration ) {   ?>
                               
								<div class="panel-body" id="">
								<input id="duration_id"  name="duration_id[]"  type="hidden" value="{{$duration->duration_id}}" required>
														<div class="form-group">
															<label for="text2" class="control-label col-lg-3">Service Timing<span class="text-sub">*</span></label>
															<div class="col-lg-4" >
															  <input id="service_timing" placeholder="Enter Service Timing" name="service_timing[]" class="form-control" type="number" value="{{$duration->service_duration}}" required>
															</div>
                                                            <div class="col-lg-3" >
																<select class="form-control" name="duration[]" id="duration">
                                                                    <option value="1" @if($duration->service_time_type== '1') selected @endif>Minutes</option>
                                                                    <option value="2" @if($duration->service_time_type== '2') selected @endif>Hours</option>
                                                                    <option value="3" @if($duration->service_time_type== '3') selected @endif>Day</option>
                                                                    <option value="4" @if($duration->service_time_type== '4') selected @endif>Week</option>
                                                                    <option value="5" @if($duration->service_time_type== '5') selected @endif>Month</option>
                                                                    <option value="6" @if($duration->service_time_type== '6') selected @endif>Year</option>
                                                                </select>
															</div>
														</div>
														
                                <div class="panel-body">
								<div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Original Price<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <input  type="number"  placeholder="Numbers Only" class="form-control"id="Original_Price" name="Original_Price[]" value="{{$duration->service_orginal_price}}" required >
										<p class="error-block" style="color:red;">
									@if ($errors->has('Original_Price')) {{ $errors->first('Original_Price') }} @endif </p>
                                    </div>
                                </div></div>
								
								<div class="panel-body">
                                <div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Discounted Price<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <input placeholder="Numbers Only" class="form-control" type="number" id="Discounted_Price" name="Discounted_Price[]" value="{{$duration->service_discount_price}}" required>
										<p class="error-block" style="color:red;">
									@if ($errors->has('Discounted_Price')) {{ $errors->first('Discounted_Price') }} @endif </p>
                                    </div>
                                </div></div>
								<a class="btn btn-xs btn-info" href="<?php echo url('mer_delete_duration')."/".$duration->duration_service_id ."/".$duration->duration_id ?>"><span class="glyphicon glyphicon-minus"></span></a>
								</div>
								<?php } ?>
								<div class="panel-body" id="img_upload">
								</div>
								<div class="panel-body">
								<div class="form-group">

                                    <label for="text2" class="control-label col-lg-3">Calendar Option<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
									<label class="sample checkbox-inline" id="chk_radio">
                                        <input type="radio" id="calendar_option" name="calendar_option"   value="1" @if($calendar== '1') checked @endif >
                                        <span>Yes</span></label>
										<label class="sample checkbox-inline" id="chk_radio">
                                        <input type="radio" name="calendar_option" id="calendar_option" value="2" @if($calendar== '2') checked @endif>
                                        <span>No</span></label>
                                        
                                    </div>
                                </div>
								</div>
								
								<div class="panel-body">
								<div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Select Store<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <select class="form-control" id="store_name" name="store_name" onChange="get_maincategory(this.value)">
                                            
                                            <?php foreach($getstore as $store)  { ?>
                                                <option value="<?php echo $store->stor_id; ?>" <?php if($store_id==$store->stor_id){ echo "selected";}?> >
                                                    <?php echo $store->stor_name; ?>
                                                </option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
								</div>
								<div class="panel-body">
								<div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Meta keywords<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <textarea class="form-control" id="Meta_Keywords" name="Meta_Keywords">{{$services->meta_keywords}} </textarea>
                                    </div>
                                </div>
								</div>
								<div class="panel-body">
								<div class="form-group">
                                    <label class="control-label col-lg-3" for="text1">Meta description<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <textarea class="form-control" id="Meta_Description" name="Meta_Description" >{{$services->meta_description}}</textarea>
                                    </div>
                                </div>
								</div>
								<div class="form-group">
                                    <label for="pass1" class="control-label col-lg-3"><span class="text-sub"></span></label>

                                    <div class="col-lg-4">
                                        <button type="submit" class="btn btn-success btn-sm btn-grad" id="submit_product"><a style="color:#fff">Update</a></button>
                                        <a href="url('')/manage_services"><button class="btn btn-default btn-sm btn-grad" style="color:#000">Cancel</button></a>
                                    </div>
                                </div>
                </div>
				@endforeach
</form>

</body>
     <!-- END BODY -->
     <script language="JavaScript">
        $(document).ready(function() {
			/*var id = showship2;
            var calenter_id = $('input[name="calendar_option"]:checked').val();

            if (calenter_id == 1) {
                document.getElementById(id).style.display = visibility;
                //document.getElementById(id1).style.display = visibility;
               // document.getElementById(id2).style.display = visibility;
            } else if (calenter_id == 2) {
                document.getElementById(id).style.display = 'none';
               // document.getElementById(id1).style.display = 'none';
               // document.getElementById(id2).style.display = 'none';
            } */
        }); 

      /*  $(document).ready(function() {
    
    
        var test = $("input[name='calendar_option']").val();
        alert(test);
        if(test==1){
            $("#showship2").show();
            
        } 
        if(test==2) 
        { 
            $("#showship2").hide(); 
        }else {  
            $("#showship2").show(); 
         }
    
});*/
</script>
<script src="<?php echo url('')?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
					    var maxField = 100; //Input fields increment limitation
					    var addButton = $('#add_button'); //Add button selector
					    var wrapper = $('#img_upload'); //Input field wrapper //div
					    var fieldHTML = '<div><input id="duration_id"  name="duration_id[]"  type="hidden" value="0" ><div class="panel-body" ><div class="form-group"><label for="text2" class="control-label col-lg-3">Service Timing<span class="text-sub">*</span></label><div class="col-lg-4" ><input id="service_timing" placeholder="Enter Service Timing" name="service_timing[]" class="form-control" type="number" value="" required></div><div class="col-lg-3"><select class="form-control" name="duration[]" id="duration"><option value="1">Minutes</option><option value="2">Hours</option><option value="3">Days</option><option value="4">Weeks</option><option value="5">Month</option><option value="6">Year</option></select></div></div><div class="panel-body"><div class="form-group"><label for="text1" class="control-label col-lg-3">Original Price<span class="text-sub">*</span></label><div class="col-lg-4"><input placeholder="Numbers Only" class="form-control" type="number" id="Original_Price" name="Original_Price[]" value="" required ><p class="error-block" style="color:red;">@if ($errors->has("Original_Price")) {{ $errors->first("Original_Price") }} @endif </p></div></div></div><div class="panel-body"><div class="form-group"><label for="text1" class="control-label col-lg-3">Discounted Price<span class="text-sub">*</span></label> <div class="col-lg-4"><input placeholder="Numbers Only" class="form-control" type="number" id="Discounted_Price" name="Discounted_Price[]" value="" required><p class="error-block" style="color:red;">@if ($errors->has("Discounted_Price")) {{ $errors->first("Discounted_Price") }} @endif </p></div></div></div><div id="remove_button"><a href="javascript:void(0);"  title="Remove field" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-minus"></span></a></div></div>'; //New input field html 
					    var x = 1; //Initial field counter is 1
					    $(addButton).click(function(){ //Once add button is clicked
					        if(x < maxField){ //Check maximum number of input fields
					            x++; //Increment field counter
					            $(wrapper).append(fieldHTML); 
					            
					        }
					    });
					    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
					        e.preventDefault();
					        $(this).parent('div').remove(); //Remove field html
					        x--; //Decrement field counter
					       // document.getElementById('count').value = parseInt(count_id)-1;
					    });
					});
					 
				</script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</html>
