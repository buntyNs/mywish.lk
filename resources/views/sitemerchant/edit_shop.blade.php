<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8">
  <![endif]-->
  <!--[if IE 9]> 
  <html lang="en" class="ie9">
    <![endif]-->
    <!--[if !IE]><!--> 
    <html lang="en">
      <!--<![endif]-->
      <!-- BEGIN HEAD -->
      <head>
        <meta charset="UTF-8" />
        <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT') }}  | {{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT')!= '') ? trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_ACCOUNT') }}</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
        <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
        <link rel="stylesheet" href="{{ url('') }}/public/assets/css/plan.css" />
        <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
        <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
        @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
        <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{ $fav->imgs_name }} ">
        @endif
        <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <?php /* Edit Image Starts */ ?>
        <link href="{{ url('') }}/public/assets/cropImage/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ url('') }}/public/assets/cropImage/editImage/css/style.css" type="text/css" />
        <link rel="stylesheet" href="{{ url('') }}/public/assets/cropImage/editImage/css/canvasCrop.css" />
        <style>
          .imageBox {
          position: relative;
          background: #fff;
          overflow: hidden;
          background-repeat: no-repeat;
          cursor: move;
          box-shadow: 4px 4px 12px #B0B0B0;
          }
          .imageBox .thumbBox {
          position: absolute;
          top: 100px;
          left: 100px;
          box-sizing: border-box;
          box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
          background: none repeat scroll 0% 0% transparent;
          }
          .modal-content
          {
          background-color: rgb(185, 185, 185);
          }
        </style>
        <?php /* Edit Image ends */ ?>
      </head>
      <!-- END HEAD -->
      <!-- BEGIN BODY -->
      <body class="padTop53 " >
        <?php /* Edit Image Starts */ ?>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog"  style="width:39%;">
            <!-- Modal content-->
            <div class="modal-content dev_appendEditData">
              <?php ?>  
              <script type="text/javascript">
                function calSubmit(){
                
                  $("#dev_upImg_form").submit();
                }
              </script>
              <!-- Modal content-->
              <div class="modal-header">
                {{ Form::button('&times;',['class' => 'close' , 'data-dismiss' => 'modal']) }}
                <h4 class="modal-title">Edit Image</h4>
              </div>
              <div class="modal-body" id='model_body'>
                <div class="imageBox" style="width: {{ $STORE_WIDTH }}px; height: {{ $STORE_HEIGHT}}px;">
                  <!--<div id="img" ></div>-->
                  <!--<img class="cropImg" id="img" style="display: none;" src="images/avatar.jpg" />-->
                  <div class="mask"></div>
                  <div class="thumbBox" style="width: {{ $STORE_WIDTH }}px; height: {{ $STORE_HEIGHT }}px;"></div>
                  <div class="spinner" style="display: none">Loading...</div>
                </div>
                <div class="tools clearfix" style='display: block; left:5px;top:250px;width:450px; margin-top: 15px;'>
                  <span id="rotateLeft" >rotateLeft</span>
                  <span id="rotateRight" >rotateRight</span>
                  <span id="zoomIn" >zoomIn</span>
                  <span id="zoomOut" >zoomOut</span>
                  <span id="crop" >Crop</span>
                  <!--<span id="alertInfo" >alert</span> -->
                  <div class="upload-wapper">
                    Select An Image
                    <input type="file" id="upload-file" value="Upload" />
                  </div>
                  <div class="crop-edit-up" style="margin-left: 5px; display: inline-block;">
                    <button type="button" class="btn btn-success" id='dev_uploadBtn' onclick="calSubmit();" style='display: none'>Upload</button>
                  </div>
                </div>
                {{ Form::open(['url' => 'mer_CropNdUpload_store','id' => 'dev_upImg_form','method' => 'post']) }}
                <input name="_token" hidden value="{!! csrf_token() !!}" />
                {{ Form::hidden('product_id','',array('id'=>'product_id')) }}
                {{ Form::hidden('img_id','',array('id'=>'img_id')) }}
                {{ Form::hidden('mer_id','',array('id'=>'mer_id')) }}
                {{ Form::hidden('imgfileName','',array('id'=>'imgfileName')) }}
                {{ Form::hidden('base64_imgData','',array('id'=>'croppedImg_base64')) }}
                <input type="submit" value="submit" style="display: none" />
                {{ Form::close() }}
                <div id='showCroppedImg' ></div>
                <!-- Edit image starts -->
                <script type="text/javascript">
                  $(function(){
                     
                  }) 
                </script>
                <!-- Edit image ends -->
              </div>
              <div class="modal-footer">
                {{ Form::button('Close',['class' => 'btn btn-default' , 'data-dismiss' => 'modal']) }}
              </div>
            </div>
          </div>
        </div>
        <!--Modal Ends-->
        <?php /* Edit Image ends */ ?>
        <!-- MAIN WRAPPER -->
        <div id="wrap">
          <!-- HEADER SECTION -->
          {!! $merchantheader !!}
          <!-- END HEADER SECTION -->
          <!-- MENU SECTION -->
          {!! $merchantleftmenus !!}
          <!--END MENU SECTION -->
          <div></div>
          <!--PAGE CONTENT -->
          <div id="content">
            <div class="inner">
              <div class="row">
                <div class="col-lg-12">
                  <ul class="breadcrumb">
                    <li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}
                      </a>
                    </li>
                    <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_STORE_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_STORE_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_STORE_ACCOUNT')}}</a></li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="box dark">
                    <header>
                      <div class="icons"><i class="icon-edit"></i></div>
                      <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_STORE_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_STORE_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_STORE_ACCOUNT')}}</h5>
                    </header>
                    @if (Session::has('block_message'))
                    <div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>
                    @endif
                    @if ($errors->any())
                    <br>
                    <ul style="color:red;">
                      <div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>')) !!}
                        {{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
                      </div>
                    </ul>
                    @endif
                    @if (Session::has('mail_exist'))
                    <div class="alert alert-warning alert-dismissable">{!! Session::get('mail_exist') !!}
                      {{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
                    </div>
                    @endif
                    <div class="row">
                      <div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                        {!! Form::open(array('url'=>'merchant_edit_shop_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
                        @foreach($store_return as $store_details )  @endforeach
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_STORE_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_STORE_DETAILS')  : trans($MER_OUR_LANGUAGE.'.MER_ADD_STORE_DETAILS') }}
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_NAME')  :  trans($MER_OUR_LANGUAGE.'.MER_STORE_NAME') }}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::hidden('mer_id',$mer_id,array('id'=>'store_merchant_id')) }} 
                                {{ Form::hidden('store_id',$id,array('id'=>'store_id')) }} 
                                {{ Form::text('store_name',$store_details->stor_name,array('id'=>'store_name','class' => 'form-control','maxlength' =>'100')) }} 
                                <div id="store_name_error_msg"  style="color:#F00;font-weight:800"  > </div>
                              </div>
                            </div>
                          </div>
                          <?php 
                            /* print_r($get_active_lang); */ ?>
                          @if(!empty($get_active_lang)) 
                          @foreach($get_active_lang as $get_lang) 
                          <?php 
                            $get_lang_code = $get_lang->lang_code;
                            $get_lang_name = $get_lang->lang_name;
                            $dynamic_stor_name="stor_name_".$get_lang_code;
                            ?>
                          <div class="panel-body">
                            <div class="form-group">
                              <label for="text1" class="control-label col-lg-2">Store Name({{ $get_lang_name }})<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                <input id="store_name_<?php echo strtolower($get_lang_name); ?>"  name="store_name_<?php echo strtolower($get_lang_name); ?>" placeholder="Enter Store Name In {{ $get_lang_name }}" value="{!! $store_details->$dynamic_stor_name !!}"  class="form-control" type="text">
                                <div id="store_name_{{ $get_lang_code}}_error_msg"  style="color:#F00;font-weight:800"  ></div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @endif
                          <!--
                            <div class="panel-body">
                                            <div class="form-group">
                                <label class="control-label col-lg-2" for="text1">Store Name({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
                                <div class="col-lg-4">
                                  <input type="text" class="form-control" placeholder="Enter Store Name in {{ Helper::lang_name() }}" id="store_name_french" name="store_name_french" value="{!! $store_details->stor_name_fr !!}"  >
                                  <div id="store_name_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
                                </div>
                              </div>
                                        </div>-->
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ? trans(Session::get('mer_lang_file').'.MER_PHONE')  : trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} <span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::text('store_pho',$store_details->stor_phone,array('id'=>'store_pho','class' => 'form-control','maxlength' =>'16')) }}
                                <div id="pho_no_error_msg"  style="color:#F00;font-weight:800"  > </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') :   trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1') }}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::text('store_add_one',$store_details->stor_address1,array('id'=>'store_add_one','class' => 'form-control','maxlength' =>'150')) }}
                                <div id="address1_error_msg"  style="color:#F00;font-weight:800"  > </div>
                              </div>
                            </div>
                          </div>
                          <?php 
                            /* print_r($get_active_lang); */ ?>
                          @if(!empty($get_active_lang)) 
                          @foreach($get_active_lang as $get_lang) 
                          <?php 
                            $get_lang_code = $get_lang->lang_code;
                            $get_lang_name = $get_lang->lang_name;
                            $stor_address1_dynamic = "stor_address1_".$get_lang_code;
                            ?>
                          <div class="panel-body">
                            <div class="form-group">
                              <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') :   trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1') }}({{ $get_lang_name }})<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                <input id="store_add_one_<?php echo strtolower($get_lang_name); ?>"  name="store_add_one_<?php echo strtolower($get_lang_name); ?>" placeholder="Enter Address One In {{ $get_lang_name }}" value="{!! $store_details->$stor_address1_dynamic !!}" class="form-control" type="text" >
                                <div id="address1_{{ $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  ></div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @endif
                          <!--
                            <div class="panel-body">
                                            <div class="form-group">
                                <label class="control-label col-lg-2" for="text1">Address One({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
                            
                                <div class="col-lg-4">
                                  <input type="text" class="form-control" placeholder="Enter Address One In {{ Helper::lang_name() }}" id="store_add_one_french" name="store_add_one_french" value="{!! $store_details->stor_address1_fr !!}"  >
                                  <div id="address1_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
                                </div>
                              </div>
                                        </div>-->
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2') :   trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2') }}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::text('store_add_two',$store_details->stor_address2,array('id'=>'store_add_two','class' => 'form-control','maxlength' =>'150')) }}
                                <div id="address2_error_msg"  style="color:#F00;font-weight:800"> </div>
                              </div>
                            </div>
                          </div>
                          <?php 
                            /* print_r($get_active_lang); */ ?>
                          @if(!empty($get_active_lang)) 
                          @foreach($get_active_lang as $get_lang) 
                          <?php 
                            $get_lang_code = $get_lang->lang_code;
                            $get_lang_name = $get_lang->lang_name;
                            $stor_address2_dynamic = "stor_address1_".$get_lang_code;
                            ?>
                          <div class="panel-body">
                            <div class="form-group">
                              <label for="text1" class="control-label col-lg-2">Address 2({{ $get_lang_name }})<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                <input id="store_add_two_<?php echo strtolower($get_lang_name); ?>"  name="store_add_two_<?php echo strtolower($get_lang_name); ?>" placeholder="Enter Address Two In {{ $get_lang_name }}" value="{!! $store_details->$stor_address2_dynamic !!}" class="form-control" type="text" >
                                <div id="address2_{{  $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  ></div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @endif
                          <!--
                            <div class="panel-body">
                                            <div class="form-group">
                                <label class="control-label col-lg-2" for="text1">Address two({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
                            
                                <div class="col-lg-4">
                                  <input type="text" class="form-control" placeholder="Enter Address Two In {{ Helper::lang_name() }}" id="store_add_two_french" name="store_add_two_french" value="{!! $store_details->stor_address2_fr !!}"   >
                                  <div id="address2_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                                </div>
                              </div>
                                        </div>-->
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_COUNTRY')}}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                <select class="form-control" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
                                  <option value="">-- {{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')}} --</option>
                                  @foreach($country_details as $country_fetch)
                                  <option value="{{ $country_fetch->co_id }}" <?php if($country_fetch->co_id == $store_details->stor_country){ echo 'selected'; } ?> ><?php echo $country_fetch->co_name; ?></option>
                                  @endforeach
                                </select>
                                <div id="country_error_msg"  style="color:#F00;font-weight:800"> </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') ? trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                <select class="form-control" name="select_city" id="select_city" >
                                  <option value="">--{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')}}--</option>
                                </select>
                                <div id="city_error_msg"  style="color:#F00;font-weight:800"> </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ZIPCODE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ZIPCODE'): trans($MER_OUR_LANGUAGE.'.MER_ZIPCODE')}}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::text('zip_code',$store_details->stor_zipcode,array('id'=>'zip_code','class' => 'form-control','maxlength' =>'16')) }}
                                <div id="zip_code_error_msg"  style="color:#F00;font-weight:800"> </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS'): trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS') }}<span class="text-sub"></span></label>
                              <div class="col-lg-4">
                                <textarea  class="form-control" name="meta_keyword" id="meta_keyword" >{!! $store_details->stor_metakeywords !!}</textarea>
                                <div id="meta_key_error_msg"  style="color:#F00;font-weight:800"> </div>
                              </div>
                            </div>
                          </div>
                          <?php 
                            /* print_r($get_active_lang); */ ?>
                          @if(!empty($get_active_lang)) 
                          @foreach($get_active_lang as $get_lang) 
                          <?php 
                            $get_lang_code = $get_lang->lang_code;
                            $get_lang_name = $get_lang->lang_name;
                            $meta_keyword_dynamic = "stor_address1_".$get_lang_code;
                            ?>
                          <div class="panel-body">
                            <div class="form-group">
                              <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS'): trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS') }}({{ $get_lang_name }})<span class="text-sub"></span></label>
                              <div class="col-lg-4">
                                <textarea  class="form-control" placeholder="Enter Meta Keywords In {{ $get_lang_name }}" name="meta_keyword_<?php echo strtolower($get_lang_name); ?>" id="meta_keyword_<?php echo strtolower($get_lang_name); ?>" >{{ $store_details->$meta_keyword_dynamic}}</textarea>
                                <div id="meta_key_{{  $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  ></div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @endif
                          <!--
                            <div class="panel-body">
                                            <div class="form-group">
                                <label class="control-label col-lg-2" for="text1">Meta Keywords({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
                            
                                <div class="col-lg-4">
                                  <textarea  class="form-control" name="meta_keyword_french" id="meta_keyword_french" >{!! $store_details->stor_metakeywords_fr !!}</textarea>
                                  <div id="meta_key_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                                </div>
                              </div>
                                        </div>-->
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')}}<span class="text-sub"></span></label>
                              <div class="col-lg-4">
                                {{ Form::textarea('meta_description',$store_details->stor_metadesc,['id' => 'meta_description','class' => 'form-control']) }}
                                <div id="meta_desc_error_msg"  style="color:#F00;font-weight:800"> </div>
                              </div>
                            </div>
                          </div>
                          <?php 
                            /* print_r($get_active_lang); */ ?>
                          @if(!empty($get_active_lang)) 
                          @foreach($get_active_lang as $get_lang) 
                          <?php 
                            $get_lang_code = $get_lang->lang_code;
                            $get_lang_name = $get_lang->lang_name;
                            $meta_description_dynamic = "stor_address1_".$get_lang_code;
                            ?>
                          <div class="panel-body">
                            <div class="form-group">
                              <label for="text1" class="control-label col-lg-2">Meta Description({{ $get_lang_name}})<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                <textarea  class="form-control" placeholder="Enter Meta Description In {{ $get_lang_name }}" name="meta_description_<?php echo strtolower($get_lang_name); ?>" id="meta_description_<?php echo strtolower($get_lang_name); ?>" >{{ $store_details->$meta_description_dynamic }}</textarea>
                                <div id="meta_desc_{{ $get_lang_code}}_error_msg"  style="color:#F00;font-weight:800"  ></div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @endif
                          <!--
                            <div class="panel-body">
                                            <div class="form-group">
                                <label class="control-label col-lg-2" for="text1">Meta Description({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
                            
                                <div class="col-lg-4">
                                   <textarea id="meta_description_french"  name="meta_description_french" class="form-control">{!! $store_details->stor_metadesc_fr !!}</textarea>
                                  <div id="meta_desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                                </div>
                              </div>
                                        </div>-->
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_WEBSITE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_WEBSITE') : trans($MER_OUR_LANGUAGE.'.MER_WEBSITE') }}<span class="text-sub"></span></label>
                              <div class="col-lg-4">
                                <input type="url" class="form-control" placeholder="https://www.google.co.in/" id="website" name="website" value="{!! $store_details->stor_website !!}"  >
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])) !!}
                              <div class="col-lg-4">
                                <input id="pac-input" onclick="check();" class="form-control" type="text" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_TYPE_YOUR_LOCATION_HERE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TYPE_YOUR_LOCATION_HERE')  : trans($MER_OUR_LANGUAGE.'.MER_TYPE_YOUR_LOCATION_HERE') }} ({{ (Lang::has(Session::get('mer_lang_file').'.MER_AUTO-COMPLETE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUTO-COMPLETE') :  trans($MER_OUR_LANGUAGE.'.MER_AUTO-COMPLETE') }})">
                              </div>
                              <div class="col-lg-4"></div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MAP_SEARCH_LOCATION')!= '') ?  trans(Session::get('mer_lang_file').'.MAP_SEARCH_LOCATION') : trans($MER_OUR_LANGUAGE.'.MAP_SEARCH_LOCATION')}}<span class="text-sub">*</span><br><span  style="color:#999">({{ (Lang::has(Session::get('mer_lang_file').'.MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE'): trans($MER_OUR_LANGUAGE.'.MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE')}} )</span></label>
                              <div class="col-lg-4">
                                <div id="map_canvas" style="width:300px;height:250px;" ></div>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_LATITUDE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LATITUDE') : trans($MER_OUR_LANGUAGE.'.MER_LATITUDE') }}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::text('latitude',$store_details->stor_latitude,array('id'=>'latitude','class' => 'form-control','readonly')) }}
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="form-group">
                              <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_LONGITUDE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LONGITUDE') : trans($MER_OUR_LANGUAGE.'.MER_LONGITUDE') }}<span class="text-sub">*</span></label>
                              <div class="col-lg-4">
                                {{ Form::text('longitude',$store_details->stor_longitude,array('id'=>'longitude','class' => 'form-control','readonly')) }}
                              </div>
                            </div>
                          </div>
                       
                        <div class="panel-body">
                          <div class="form-group">
                            <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.SLOGAN')!= '') ?  trans(Session::get('mer_lang_file').'.SLOGAN') : trans($MER_OUR_LANGUAGE.'.SLOGAN') }}<span class="text-sub">*</span></label>
                            <div class="col-lg-4">
                              {{ Form::text('Slogan',$store_details->stor_slogan,array('id'=>'Slogan','class' => 'form-control')) }}
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_IMAGE')   : trans($MER_OUR_LANGUAGE.'.MER_STORE_IMAGE')}}<span class="text-sub">*</span></label>
                            <div class="col-lg-4">
                              <span class="errortext red" style="color:red;"><em>{{ (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE') : trans($MER_OUR_LANGUAGE.'.MER_IMAGE_SIZE_MUST_BE') }}  {{ $STORE_WIDTH}} x {{ $STORE_HEIGHT }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PIXELS') : trans($MER_OUR_LANGUAGE.'.MER_PIXELS') }}</em></span>
                              <input type="file" id="file" name="file" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_FRUIT_BALL')!= '') ? trans(Session::get('mer_lang_file').'.MER_FRUIT_BALL') : trans($MER_OUR_LANGUAGE.'.MER_FRUIT_BALL') }}">
                              {{ Form::hidden('file_new',$store_details->stor_img) }}
                              <?php 
                                $pro_img = $store_details->stor_img;
                                  $prod_path = url(''). '/public/assets/default_image/No_image_store.png';
                                 ?>
                              @if($pro_img != '') {{--image is null --}}
                              <?php
                                $img_data = "public/assets/storeimage/".$pro_img; ?>
                              @if(file_exists($img_data))
                              {{--image not exists in folder --}}
                              <?php
                                $prod_path = url('').'/public/assets/storeimage/'.$pro_img;
                                ?>
                              @else  
                              @if(isset($DynamicNoImage['store']))
                              <?php         
                                $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['store']; ?>
                              @if($DynamicNoImage['store'] !='' && file_exists($dyanamicNoImg_path))
                              <?php  
                                $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['store']; ?>
                              @endif
                              @endif
                              @endif
                              @else
                              @if(isset($DynamicNoImage['store'])) {{-- check no_image_product is exist  --}}
                              <?php           
                                $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['store']; ?>
                              @if($DynamicNoImage['store'] !='' && file_exists($dyanamicNoImg_path))
                              <?php  
                                $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['store']; ?>
                              @endif
                              @endif
                              @endif      
                              <img src="{{ $prod_path }}" height="75px" >
                              @if($store_details->stor_img!='')
                              <?php /* Edit Image start - Trigger the modal with a button */ ?>
                              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick=" calImgEdit({{ $id }},{{ $mer_id }},1,'<?php echo $store_details->stor_img;?>')" >Edit</button></span><?php /* */ ?>
                              @endif
                              <div id="img_error_msg"  style="color:#F00;font-weight:800"> </div>
                            </div>
                          </div>
                        </div>
                    
                      <div class="form-group">
                       
                         <label class="control-label col-lg-2" for="text1"></label>
                        <div class="col-lg-4">
                          <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" >{{ (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE') : trans($MER_OUR_LANGUAGE.'.MER_UPDATE') }}</a></button>
                          <a href="{{ url('merchant_manage_shop').'/'.$mer_id }}" class="btn btn-default btn-sm btn-grad" style="color:#000">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CANCEL')!= '') ? trans(Session::get('mer_lang_file').'.MER_CANCEL') : trans($MER_OUR_LANGUAGE.'.MER_CANCEL') }}</a>
                        </div>
                      </div>
                      {{ Form::close() }}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--END PAGE CONTENT -->
        </div>
        <!--END MAIN WRAPPER -->
        <!-- FOOTER -->
        {!! $merchantfooter !!}
        <!--END FOOTER -->
        <!-- GLOBAL SCRIPTS -->
        <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
        <script>
          $( document ).ready(function() {
                var store_pho   = $('#store_pho');
                var zip_code    = $('#zip_code');
          
                 /*Phone number*/
                $('#store_pho').keypress(function (e){
                  if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                      store_pho.css('border', '1px solid red'); 
                $('#pho_no_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')}}');
                store_pho.focus();
                return false;
                  }else{      
                      store_pho.css('border', ''); 
                $('#pho_no_error_msg').html('');          
              }
                });
                /*zip code*/
                $('#zip_code').keypress(function (e){
                  if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                      zip_code.css('border', '1px solid red'); 
                $('#zip_code_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')}}');
                zip_code.focus();
                return false;
                  }else{      
                      zip_code.css('border', ''); 
                $('#zip_code_error_msg').html('');          
              }
                });
          
            $('#submit').click(function() {
                  var store_name     = $('#store_name');
                  var store_pho      = $('#store_pho');
                  var store_add_one  = $('#store_add_one');
                  var store_add_two  = $('#store_add_two');
                  var select_country = $('#select_country');
                  var select_city    = $('#select_city');
                  var zip_code       = $('#zip_code');
                  var meta_keyword   = $('#meta_keyword');
                  var meta_description = $('#meta_description');
                  var file       = $('#file');
          
                  /*store name*/
                  if(store_name.val() == ''){
                store_name.css('border', '1px solid red'); 
              $('#store_name_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_NAME'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_NAME')}}');
                store_name.focus();
                return false;
              }else{
                      store_name.css('border', ''); 
                      $('#store_name_error_msg').html('');
                  }
          
                  /*store Phone Number*/
                  if(store_pho.val() == ''){
                store_pho.css('border', '1px solid red'); 
                $('#pho_no_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_CONTACT_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_CONTACT_NUMBER'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_CONTACT_NUMBER')}}');
                store_pho.focus();
                return false;
              }else{
                      store_pho.css('border', ''); 
                      $('#pho_no_error_msg').html('');
                  }
          
                  /*store Address 1*/
                  if(store_add_one.val() == ''){
                store_add_one.css('border', '1px solid red'); 
                $('#address1_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_ADDRESS')}}');
                store_add_one.focus();
                return false;
              }else{
                      store_add_one.css('border', ''); 
                      $('#address1_error_msg').html('');
                  }
          
                  /*store Address 2*/
                  if(store_add_two.val() == ''){
                store_add_two.css('border', '1px solid red'); 
                $('#address2_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS_2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS_2'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_ADDRESS_2')}}');
                store_add_two.focus();
                return false;
              }else{
                      store_add_two.css('border', ''); 
                      $('#address2_error_msg').html('');
                  }
          
                  /*Country*/
                  if(select_country.val() == 0){
                select_country.css('border', '1px solid red'); 
                $('#country_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_COUNTRY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_COUNTRY')}}');
                select_country.focus();
                return false;
              }else{
                      select_country.css('border', ''); 
                      $('#country_error_msg').html('');
                  }
          
                  /*city*/
                  if(select_city.val() == 0){
                select_city.css('border', '1px solid red'); 
                $('#city_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CITY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CITY')}}');
                select_city.focus();
                return false;
              }else{
                      select_city.css('border', ''); 
                      $('#city_error_msg').html('');
                  } 
          
                  /*zip_code*/
                  if(zip_code.val() == 0){
                zip_code.css('border', '1px solid red'); 
                $('#zip_code_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_ZIP_CODE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_ZIP_CODE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_ZIP_CODE')}}');
                zip_code.focus();
                return false;
              }else{
                      zip_code.css('border', ''); 
                      $('#zip_code_error_msg').html('');
                  } 
          
                  
                  
              /*Image */ 
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                  if(file.val() == ""){
                  //file.focus();
                  //file.css('border', '1px solid red'); 
                      //$('#img_error_msg').html('Please Choose Image');    
                  return true;
              }else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {        
                  file.focus();
                  file.css('border', '1px solid red');  
                      $('#img_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_CHOOSE_VALID_IMAGE')}}');    
                  return false;
              }else{
                  file.css('border', ''); 
                      $('#img_error_msg').html('');       
              }
            });
          });
            
        </script>
        <script>
          $( document ).ready(function() {
           var passData = 'city_id_ajax=<?php echo $store_details->stor_city;  ?>&country_id_ajax=<?php echo $store_details->stor_country; ?>' ;
             //alert(passData);
               $.ajax( {
                    type: 'get',
                  data: passData,
                  url: '<?php echo url('ajax_select_city_edit_merchant'); ?>',
                  success: function(responseText){  
                  //alert(responseText);
                   if(responseText)
                   { 
                  $('#select_city').html(responseText);            
                   }
                }   
              });   
            
          
          });
          
              function check() { 
               
          
               var mer_id     = $('#store_merchant_id').val();
               //var longitude  = $('#longitude').val();
               var longitude  = document.getElementById("longitude").value;
               var latitude   = $('#latitude').val();
               var store_id   = $('#store_id').val();
               var datas      = "mer_id="+mer_id+"&latitude="+latitude+"&longitude="+longitude+"&store_id="+store_id;
              
                     $.ajax({
                     type: 'GET',
                           url: '<?php echo url('check_store_exists_edit'); ?>',
                   data: datas,
                   success: function(response){ 
                   //alert(response); 
                             
                     if(response==1){  //already exist
                     alert("This Store Already Exist with this same address");
                     $("#exist").val("1"); //already exist
                     $('#latitude').val('');
                     $('#longitude').val('');
                     $("#latitude").css('border', '1px solid red'); 
                     $('#longitude').css('border', '1px solid red');
                     $('#pac-input').css('border', '1px solid red');
                     $('#location_error_msg').html("Store Already Exist with this same address");  
                     $("#pac-input").focus();
                     return false;          
                    }else
                    {
                     $("#latitude").css('border', ''); 
                     $('#longitude').css('border', '');
                     $('#pac-input').css('border', '');
                     $('#location_error_msg').html(""); 
                    // $("#pac-input").blur(); 
                    }
                  }    
               });
             
          }
          
          
          function select_city_ajax(city_id)
          {
             var passData = 'city_id='+city_id;
             //alert(passData);
               $.ajax( {
                    type: 'get',
                  data: passData,
                  url: '<?php echo url('ajax_select_city_merchant'); ?>',
                  success: function(responseText){  
                 // alert(responseText);
                   if(responseText)
                   { 
                  $('#select_city').html(responseText);            
                   }
                }   
              });   
          }
          
          function select_mer_city_ajax(city_id)
          {
             var passData = 'city_id='+city_id;
            // alert(passData);
               $.ajax( {
                    type: 'get',
                  data: passData,
                  url: '<?php echo url('ajax_select_city'); ?>',
                  success: function(responseText){  
                 // alert(responseText);
                   if(responseText)
                   { 
                  $('#select_mer_city').html(responseText);            
                   }
                }   
              }); 
          }
        </script>
        <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <?php 
          //Commanded because Image Edit is effected by this script
          /*
          <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script> <?php */?>
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
        <script type="text/javascript">
          var map;
          
          function initialize() {
          
          
          var myLatlng = new google.maps.LatLng('<?php echo $store_details->stor_latitude; ?>','<?php echo $store_details->stor_longitude; ?>');
              var mapOptions = {
          
                 zoom: 10,
                      center: myLatlng,
                      disableDefaultUI: true,
                      panControl: true,
                      zoomControl: true,
                      mapTypeControl: true,
                      streetViewControl: true,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
          
              };
          
              map = new google.maps.Map(document.getElementById('map_canvas'),
                  mapOptions);
            var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
          draggable:true,    
                  }); 
          google.maps.event.addListener(marker, 'dragend', function(e) {
               
          var lat = this.getPosition().lat();
             var lng = this.getPosition().lng();
          $('#latitude').val(lat);
          $('#longitude').val(lng);
          });
              var input = document.getElementById('pac-input');
              var autocomplete = new google.maps.places.Autocomplete(input);
              autocomplete.bindTo('bounds', map);
          
              google.maps.event.addListener(autocomplete, 'place_changed', function () {
                 
          
                  var place = autocomplete.getPlace();
                  
                   var latitude = place.geometry.location.lat();
                      var longitude = place.geometry.location.lng();
                      
                      $('#latitude').val(latitude);
             $('#longitude').val(longitude);
                check();
          
                  if (place.geometry.viewport) {
                      map.fitBounds(place.geometry.viewport);
          var myLatlng = place.geometry.location; 
          var latlng = new google.maps.LatLng(latitude, longitude);
                      marker.setPosition(latlng);
          google.maps.event.addListener(marker, 'dragend', function(e) {
               
               
          var lat = this.getPosition().lat();
             var lng = this.getPosition().lng();
          $('#latitude').val(lat);
          $('#longitude').val(lng);
             check();
          });
                  } else {
                      map.setCenter(place.geometry.location); 
          
                      map.setZoom(17);
                  }
              });
          
          
          
          }
          
          
          google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        <!-- END GLOBAL SCRIPTS -->  
        <?php /* Edit Image starts */ ?>
        <?php //text editor hidded by this script ,so commanded
          /* <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script> */?>
        <script type="text/javascript" src="<?php echo url('')?>/public/assets/cropImage/editImage/js/jquery.canvasCrop.js" ></script>
        <script type="text/javascript">
          function calImgEdit(storeId,mer_id,imgId,imgFileName){    
            $("#product_id").val(storeId);
            $("#img_id").val(storeId);
            $("#mer_id").val(mer_id);
            $("#imgfileName").val(imgFileName);
           // alert(imgFileName);
          
            var rot = 0,ratio = 1;
            var CanvasCrop = $.CanvasCrop({
                cropBox : ".imageBox",
                imgSrc : "<?php echo url('');?>/public/assets/storeimage/"+imgFileName,
                limitOver : 2
            });
            
            
            $('#upload-file').on('change', function(){
                var reader = new FileReader();
                reader.onload = function(e) {
                    CanvasCrop = $.CanvasCrop({
                        cropBox : ".imageBox",
                        imgSrc : e.target.result,
                        limitOver : 2
                    });
                    rot =0 ;
                    ratio = 1;
                }
                reader.readAsDataURL(this.files[0]);
                this.files = [];
            });
            
            $("#rotateLeft").on("click",function(){
                rot -= 90;
                rot = rot<0?270:rot;
                CanvasCrop.rotate(rot);
            });
            $("#rotateRight").on("click",function(){
                rot += 90;
                rot = rot>360?90:rot;
                CanvasCrop.rotate(rot);
            });
            $("#zoomIn").on("click",function(){
                ratio =ratio*0.9;
                CanvasCrop.scale(ratio);
            });
            $("#zoomOut").on("click",function(){
                ratio =ratio*1.1;
                CanvasCrop.scale(ratio);
            });
            $("#alertInfo").on("click",function(){
                var canvas = document.getElementById("visbleCanvas");
                var context = canvas.getContext("2d");
                context.clearRect(0,0,canvas.width,canvas.height);
            });
            
            $("#crop").on("click",function(){
                
                //var src = CanvasCrop.getDataURL("jpeg");
                var src = CanvasCrop.getDataURL("png");
                //$("body").append("<div style='word-break: break-all;'>"+src+"</div>");  
                //$("#model_body").append("<img src='"+src+"' />");
                $("#showCroppedImg").html("<img src='"+src+"' />");
                $("#croppedImg_base64").val(src);
                if(src!='')
                    $("#dev_uploadBtn").css('display','block');
          
          
            });
            
            console.log("ontouchend" in document);
          
          }
        </script>
        <?php /* Edit Image ends */ ?>  
        <script type="text/javascript">
          $.ajaxSetup({
          headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
          });
          
          $('#store_name').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }
          );
          
          
          
        </script>     
      </body>
      <!-- END BODY -->
    </html>