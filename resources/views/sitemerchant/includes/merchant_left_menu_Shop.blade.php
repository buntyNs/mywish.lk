@php  $current_route = Route::getCurrentRoute()->uri(); @endphp
<?php

if (Session::has('merchantid'))
{
    $merchantid=Session::get('merchantid');
}

 ?>
 
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading">{{ (Lang::has(Session::get('mer_lang_file').'.PRODUCTS1')!= '') ?  trans(Session::get('mer_lang_file').'.PRODUCTS1') : trans($MER_OUR_LANGUAGE.'.PRODUCTS1') }}</h5>
                    
                </div>
                <br />
            </div>

             <ul id="menu" class="collapse">
                
      <li <?php if($current_route == 'merchant_manage_shop'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>> <a  href="{{ url('merchant_manage_shop/'.$merchantid) }}"> <i class="icon-dashboard"></i>&nbsp;{{ (Lang::has(Session::get('mer_lang_file').'.MANAGE_STORES')!= '') ?  trans(Session::get('mer_lang_file').'.MANAGE_STORES') : trans($MER_OUR_LANGUAGE.'.MANAGE_STORES') }}
        </a> </li>
                   
      <li <?php if($current_route == 'merchant_add_shop'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>> <a href="{{ url('merchant_add_shop/'.$merchantid) }}"> <i class=" icon-plus-sign"></i>&nbsp;{{ (Lang::has(Session::get('mer_lang_file').'.ADD_STORES')!= '') ?  trans(Session::get('mer_lang_file').'.ADD_STORES') : trans($MER_OUR_LANGUAGE.'.ADD_STORES') }}
       </a> </li>
    
            </ul>


        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
