   <?php  $current_route = Route::getCurrentRoute()->uri(); ?> 
  <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"><?php if (Lang::has(Session::get('mer_lang_file').'.AUCTION1')!= '') { echo  trans(Session::get('mer_lang_file').'.AUCTION1');}  else { echo trans($MER_OUR_LANGUAGE.'.AUCTION1');} ?></h5>
                    
                </div>
                <br />
            </div>

               <ul id="menu" class="collapse">
              <!--  <li <?php //if($current_route == 'fund_request'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>  
                    <a href="#">
                        <i class="icon-dashboard"></i>&nbsp;Auction Dashboard</a> -->                  
                </li>
                   <li <?php if($current_route == 'merchant_add_auction'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>  
                    <a href="<?php echo url('merchant_add_auction'); ?>" >
                        <i class=" icon-plus-sign"></i>&nbsp;<?php if (Lang::has(Session::get('mer_lang_file').'.ADD_AUCTION_PRODUCTS')!= '') { echo  trans(Session::get('mer_lang_file').'.ADD_AUCTION_PRODUCTS');}  else { echo trans($MER_OUR_LANGUAGE.'.ADD_AUCTION_PRODUCTS');} ?>
	                </a>                   
                </li>
                 <li <?php if($current_route == 'merchant_manage_auction'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>  
                    <a href="<?php echo url('merchant_manage_auction'); ?>" >
                        <i class=" icon-edit"></i>&nbsp;<?php if (Lang::has(Session::get('mer_lang_file').'.MANAGE_AUCTION_PRODUCTS')!= '') { echo  trans(Session::get('mer_lang_file').'.MANAGE_AUCTION_PRODUCTS');}  else { echo trans($MER_OUR_LANGUAGE.'.MANAGE_AUCTION_PRODUCTS');} ?>
                   </a>                   
                </li>
				 <li <?php if($current_route == 'merchant_expired_auction'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>  
                    <a href="<?php echo url('merchant_expired_auction'); ?>" >
                        <i class="icon-tag"></i>&nbsp;<?php if (Lang::has(Session::get('mer_lang_file').'.ARCHIVE_AUCTION_PRODUCTS')!= '') { echo  trans(Session::get('mer_lang_file').'.ARCHIVE_AUCTION_PRODUCTS');}  else { echo trans($MER_OUR_LANGUAGE.'.ARCHIVE_AUCTION_PRODUCTS');} ?>
                   </a>                   
                </li>
				 <li <?php if($current_route == 'merchant_auction_winners'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>  
                    <a href="<?php echo url('merchant_auction_winners'); ?>" >
                       <i class="icon-trophy"></i>&nbsp;<?php if (Lang::has(Session::get('mer_lang_file').'.AUCTION_WINNERS')!= '') { echo  trans(Session::get('mer_lang_file').'.AUCTION_WINNERS');}  else { echo trans($MER_OUR_LANGUAGE.'.AUCTION_WINNERS');} ?>
                   </a>                   
                </li>
			 
				<li <?php if($current_route == 'merchant_auction_cod'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>  
                    <a href="<?php echo url('merchant_auction_cod'); ?>" >
                        <i class="icon-money"></i>&nbsp;<?php if (Lang::has(Session::get('mer_lang_file').'.AUCTION_COD')!= '') { echo  trans(Session::get('mer_lang_file').'.AUCTION_COD');}  else { echo trans($MER_OUR_LANGUAGE.'.AUCTION_COD');} ?>
                   </a>                   
                </li>
            </ul>



        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>