 @php $current_route = Route::getCurrentRoute()->uri(); @endphp 
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?php echo url(''); ?>/public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_LISTING')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMISSION_LISTING') : trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_LISTING') }} </h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                
      <li <?php if($current_route == 'commission_listing'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>> <a href="{{ url('commission_listing') }}" > <i class="icon-dashboard"></i>&nbsp; {{(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_LISTING')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMISSION_LISTING') : trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_LISTING') }}
</a> </li>
                   
      <li <?php if($current_route == 'commission_paid'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>> <a href="{{ url('commission_paid') }}" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav"> 
        <i class="icon-resize-small"></i>&nbsp;{{ (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAID')!= '') ? trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAID') : trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAID') }} <span class="pull-right"> 
        <i class="icon-angle-right"></i> </span> </a> </li>
       

           
            </ul>

			 
        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>