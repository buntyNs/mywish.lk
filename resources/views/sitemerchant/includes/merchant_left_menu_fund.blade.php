 @php  $current_route = Route::getCurrentRoute()->uri(); @endphp
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?php echo url(''); ?>/public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"> @if (Lang::has(Session::get('mer_lang_file').'.FUND_REQUESTS')!= '') {{  trans(Session::get('mer_lang_file').'.FUND_REQUESTS') }} @else {{ trans($MER_OUR_LANGUAGE.'.FUND_REQUESTS') }} @endif </h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                
      <li <?php if($current_route == 'fund_request'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>> <a href="{{ url('fund_request') }}" > <i class="icon-dashboard"></i>&nbsp; {{ (Lang::has(Session::get('mer_lang_file').'.FUND_REQUEST_REPORT')!= '') ?  trans(Session::get('mer_lang_file').'.FUND_REQUEST_REPORT') : trans($MER_OUR_LANGUAGE.'.FUND_REQUEST_REPORT') }} 
</a> </li>
                   
      <li <?php if($current_route == 'with_fund_request'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>> <a href="{{ url('with_fund_request') }}" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav"> 
        <i class="icon-resize-small"></i>&nbsp; {{ (Lang::has(Session::get('mer_lang_file').'.WITHDRAW_FUND_REQUEST')!= '') ?  trans(Session::get('mer_lang_file').'.WITHDRAW_FUND_REQUEST') : trans($MER_OUR_LANGUAGE.'.WITHDRAW_FUND_REQUEST') }} <span class="pull-right"> 
        <i class="icon-angle-right"></i> </span> </a> </li>
       

           
            </ul>

			 
        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>