@php $current_route = Route::getCurrentRoute()->uri(); @endphp 
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading">{{ (Lang::has(Session::get('mer_lang_file').'.SETTINGS1')!= '') ?  trans(Session::get('mer_lang_file').'.SETTINGS1') : trans($MER_OUR_LANGUAGE.'.SETTINGS1') }}</h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                <li <?php if($current_route=="merchant_settings"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="{{ url('merchant_settings') }}">
                        <i class="icon-cog"></i>&nbsp; {{ (Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ?  trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS') }}</a>                   
                </li>
				
				<?php

			if (Session::has('merchantid'))
			{
				$merchantid=Session::get('merchantid');
			}

			 ?>
			 
                   <li <?php if($current_route=="edit_merchant_info/{id}"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>

				 <?php if (Session::has('merchantid')) {?>
                    <a href="<?php echo url('edit_merchant_info/'.$merchantid); ?>" >
                <?php }else { ?>  <a href="<?php echo url('sitemerchant'); ?>" ><?php } ?>
	
                        <i class="icon-envelope"></i>&nbsp;{{ (Lang::has(Session::get('mer_lang_file').'.EDIT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.EDIT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.EDIT_ACCOUNT') }}
	                </a>                   
                </li>
                 <li <?php if($current_route=="change_merchant_password/{id}"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>

<?php if (Session::has('merchantid')) {?>
                   <a href="<?php echo url('change_merchant_password/'.$merchantid); ?>" >
            <?php }else { ?>  <a href="<?php echo url('sitemerchant'); ?>" ><?php } ?>
  <i class="icon-mail-reply"></i>&nbsp;{{ (Lang::has(Session::get('mer_lang_file').'.CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.CHANGE_PASSWORD') }}
                   </a>                   
                </li>
				<li  <?php if( $current_route == "mer_add_size" || $current_route == "mer_manage_size" || $current_route == "mer_add_color" || $current_route == "mer_manage_color" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>  >
				<?php if (Session::has('merchantid')) {?>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pagesr-nav">
                        <i class="icon-anchor"></i> {{ (Lang::has(Session::get('mer_lang_file').'.ATTRIBUTES_MANAGEMENT')!= '') ?  trans(Session::get('mer_lang_file').'.ATTRIBUTES_MANAGEMENT') : trans($MER_OUR_LANGUAGE.'.ATTRIBUTES_MANAGEMENT') }}
	   
                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                         <?php }else { ?>  <a href="{{ url('sitemerchant') }}" ><?php } ?>
                    </a>
					
                    <ul 
					 <?php if (Session::has('merchantid')) {?>
					<?php if( $current_route == "mer_add_size" || $current_route == "mer_manage_size" || $current_route == "mer_add_color" || $current_route == "mer_manage_color" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="pagesr-nav">
                        <li <?php if( $current_route == "mer_add_color" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="{{ url('mer_add_color') }}"><i class="icon-angle-right"></i>@if (Lang::has(Session::get('mer_lang_file').'.ADD_COLOR')!= '') {{  trans(Session::get('mer_lang_file').'.ADD_COLOR') }} @else {{ trans($MER_OUR_LANGUAGE.'.ADD_COLOR') }} @endif </a></li>
                        <li <?php if( $current_route == "mer_manage_color" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="{{ url('mer_manage_color') }}"><i class="icon-angle-right"></i> {{ (Lang::has(Session::get('mer_lang_file').'.MANAGE_COLORS')!= '') ?  trans(Session::get('mer_lang_file').'.MANAGE_COLORS') : trans($MER_OUR_LANGUAGE.'.MANAGE_COLORS') }}  </a></li>
                        <li <?php if( $current_route == "mer_add_size" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="{{ url('mer_add_size') }}"><i class="icon-angle-right"></i> {{ (Lang::has(Session::get('mer_lang_file').'.ADD_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.ADD_SIZE') : trans($MER_OUR_LANGUAGE.'.ADD_SIZE') }} </a></li>
                        <li <?php if( $current_route == "mer_manage_size" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="{{ url('mer_manage_size') }}"><i class="icon-angle-right"></i> {{ (Lang::has(Session::get('mer_lang_file').'.MANAGE_SIZES')!= '') ?  trans(Session::get('mer_lang_file').'.MANAGE_SIZES') : trans($MER_OUR_LANGUAGE.'.MANAGE_SIZES') }} </a></li>
                         
                        <?php }else { ?>  <a href="<?php echo url('sitemerchant'); ?>" ><?php } ?>
                    </ul>
					
                </li>
                <?php /*
                 <li <?php if( $current_route == "mer_manage_service_type" || $current_route == "mer_add_service_type") { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >
                    <a  data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav10">
                         <i class="fa fa-cogs" aria-hidden="true"></i> Service Type Management
                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                    <ul class="collapse" id="chart-nav10">
                        <li><a href="<?php echo url('mer_add_service_type'); ?>"><i class="icon-angle-right"></i> Add Service Type</a></li>
                        <li><a href="<?php echo url('mer_manage_service_type');?>"><i class="icon-angle-right"></i> Manage Service Type</a></li>

                    </ul>
              </li>
				<li <?php if( $current_route == "mer_manage_service_time") { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >
                    <a  data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav11">
                        <i class="fa fa-hourglass-start" aria-hidden="true"></i> Service Time
                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                    <ul class="collapse" id="chart-nav11">
					<li><a href="<?php echo url('mer_add_service_time'); ?>"><i class="icon-angle-right"></i> Add Service Time</a></li>
                        <li><a href="<?php echo url('mer_manage_service_time'); ?>"><i class="icon-angle-right"></i> Manage Service Time</a></li>
                        

                    </ul>
              </li>*/?>
				 
				
            </ul>

        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>