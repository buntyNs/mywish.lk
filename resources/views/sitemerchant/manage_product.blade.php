﻿<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->



 <!-- BEGIN HEAD -->

<head>

    <meta charset="UTF-8" />

    <title>{{ $SITENAME}} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT'): trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')}} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_MANAGE_PRODUCTS')}}</title>

     <meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <![endif]-->

    <!-- GLOBAL STYLES -->

    <!-- GLOBAL STYLES -->

     <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />

<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />

	 <link href="public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <!--END GLOBAL STYLES -->

       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

</head>

     <!-- END HEAD -->

<style type="text/css">
    #dataTables-example_wrapper
    {
        overflow: auto;
    }
</style>

     <!-- BEGIN BODY -->

<body class="padTop53">



    <!-- MAIN WRAPPER -->

    <div id="wrap">

 <!-- HEADER SECTION -->

         {!! $adminheader !!}

        <!-- END HEADER SECTION -->

        <!-- MENU SECTION -->

       {!! $adminleftmenus !!}

        <!--END MENU SECTION -->



      

		<div></div>



         <!--PAGE CONTENT -->

        <div id="content">

           

                <div class="inner">

                    <div class="row">

                    <div class="col-lg-12">

                        	<ul class="breadcrumb">

                            	<li class=""><a href="{{ url('sitemerchant_dashboard')}}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>

                                <li class="active"><a href="#">  {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_PRODUCTS'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_PRODUCTS')}}                   </a></li>

                            </ul>

                    </div>

                </div>

				

			<center><div class="cal-search-filter">

					{{ Form::open(['action' => 'MerchantproductController@manage_product', 'method' => 'POST']) }}
		 

							<input type="hidden" name="_token"  value="{!! csrf_token() !!}">

							 <div class="row">

							   <div class="col-sm-4">

							    <div class="item form-group">

							<div class="col-sm-6 date-top">{{ (Lang::has(Session::get('mer_lang_file').'.MER_FROM_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FROM_DATE') : trans($MER_OUR_LANGUAGE.'.MER_FROM_DATE')}}</div>

						<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
                            {{ Form::text('from_date',$from_date,array('id'=>'datepicker-8','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly')) }}
							 

							 

							  </div>

							  </div>

							   </div>

							    <div class="col-sm-4">

							    <div class="item form-group">

							<div class="col-sm-6 date-top">{{ (Lang::has(Session::get('mer_lang_file').'.MER_TO_DATE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_TO_DATE')  : trans($MER_OUR_LANGUAGE.'.MER_TO_DATE')}}</div>

							<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
							 {{ Form::text('to_date',$to_date,array('id'=>'datepicker-9','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly')) }}

							 

							  </div>

							  </div>

							   </div>

							   

							   <div class="form-group">

							   <div class="col-sm-2">

							 <input type="submit" name="submit" class="btn btn-block btn-success" value="{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') ?   trans(Session::get('mer_lang_file').'.MER_SEARCH') : trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }}">

							 </div>
                             <div class="col-sm-2">

							<a href="{{ url('').'/mer_manage_product'}}"><button type="button" name="reset" class="btn btn-block btn-info">{{ (Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ? trans(Session::get('mer_lang_file').'.MER_RESET') : trans($MER_OUR_LANGUAGE.'.MER_RESET') }}</button></a>

							 </div>

							</div>

							
								{{ Form::close() }}
							 </div>

							 </center>

            <div class="row">

<div class="col-lg-12">

    <div class="box dark">

        <header>

            <div class="icons"><i class="icon-edit"></i></div>

            <h5> {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_PRODUCTS'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_PRODUCTS')}}                    </h5>

            

        </header>
 <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
 	{{ Form::button('×',['class' => 'close closeAlert' , 'aria-hidden' => 'true']) }}
         </div>
 @if (Session::has('block_message'))

		<div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}

			{{ Form::button('×',['class' => 'close' , 'aria-hidden' => 'true','data-dismiss' => 'alert']) }}

        </div>

		@endif

        <div id="div-1" class="accordion-body collapse in body">
<!-- 
           <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label>

		   

		   </label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter">

		   </div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div> -->
		    <div class="panel_marg_clr ppd">
		   <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">

                                    <thead>

                                        <tr role="row">

										<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_NAME') :  trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_NAME') }}</th>

										<!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SKU_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SKU_NUMBER') :  trans($MER_OUR_LANGUAGE.'.MER_SKU_NUMBER') }}</th> -->

										

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_NAME')  :  trans($MER_OUR_LANGUAGE.'.MER_STORE_NAME') }}</th>

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 79px;" aria-label="City: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY')  : trans($MER_OUR_LANGUAGE.'.MER_CITY') }}</th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Original Price($): activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ? trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') :  trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE') }} ({{ Helper::cur_sym() }})</th>

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Discounted Price($): activate to sort column ascending">{{(Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_DISCOUNTED_PRICE') }}({{ Helper::cur_sym() }})</th>

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending">  {{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE') :trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_IMAGE') }} </th>

									

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ACTIONS') : trans($MER_OUR_LANGUAGE.'.MER_ACTIONS') }}</th>

										

										<!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 74px;" aria-label="Preview: activate to sort column ascending">Preview</th> -->

										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 68px;" aria-label="Deal details: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_DETAILS')}}</th>

										</tr>

                                    </thead>

                                    <tbody>





@if (Session::has('merchantid'))

@php

    $merchantid=Session::get('merchantid');

@endphp
@endif



 @php $i = 1 ;   @endphp

 

  @if(isset($_POST['submit']))

			
				@if(count($allprod_reports)>0) 

			@foreach($allprod_reports as $product_list) 

						@if($product_list->pro_no_of_purchase < $product_list->pro_qty)

							

								

									 @php	  $product_get_img = explode("/**/",$product_list->pro_Img);  @endphp
									  @if($product_list->pro_status == 1)
									   @php 
										  $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
										  @endphp
								  elseif($product_list->pro_status == 0) 
								  	@php
										   $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
										   @endphp
									  @endif

                                    <tr class="gradeA odd">

                                            <td class="sorting_1">{{ $i }}</td>
                                            <td class="center">{{ substr($product_list->pro_title,0,45) }}</td>
                                        <!--      <td>{{$product_list->pro_sku_number }}</td> -->
                                            <td class="center  ">{{ $product_list->stor_name }}</td>
                                            <td class="center">{{ $product_list->ci_name }} </td>
                                            <td class="center  ">{{ Helper::cur_sym() }} {{ $product_list->pro_price }}</td>
                                            <td class="center  ">{{ Helper::cur_sym() }} {{ $product_list->pro_disprice }}</td>

                                            <td class="center  ">
											<?php  $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
											@if($product_get_img != '')  {{-- image is null--}}
									
			 								@php	
									  $pro_img = $product_get_img[0]; @endphp	
									  
									  $img_data = "public/assets/product/".$pro_img;
										  @if(file_exists($img_data) && $pro_img !='')  {{-- image not exists in folder  --}}
														@php 		
																			 $prod_path = url('').'/public/assets/product/'.$pro_img;
															@endphp	 
										  @else 
												 @if(isset($DynamicNoImage['productImg']))
												 	@php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp	
													@if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
													 @php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp	
													@endif
																		
												 @endif
																	 
																	 
												@endif
									   
						
						   @else
						   
							   
								 @if(isset($DynamicNoImage['productImg']))
												 	@php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp	
													@if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
													 @php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp	
													@endif
																		
												 @endif
																	 
																	 
												@endif											
											
											<a href="#"><img style="height:40px;" src="{{ $prod_path }}"></a></td>

                                        

                                            <td class="center  ">
                                              <a href="{{ url('mer_edit_product/'.$product_list->pro_id) }}" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT') }}"> <i class="icon icon-edit" style="margin-left:15px;"></i></a>
                                         	<?php  $process; ?>
                                         	
                                         	@if($product_list->pro_no_of_purchase==0) 
                                              
                                             	<a href="{{ url('mer_delete_product') }}/{{ $product_list->pro_id }}" > <i class="icon icon-trash icon-1x" style="margin-left:15px;"></i></a>

                                             @endif

                                            </td>
                                            <td class="center  ">
                                                <a href="{{ url('mer_product_details')."/".$product_list->pro_id }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS') }}</a>
                                            </td>

                                        </tr>

			<?php $i++; ?>
			@endif
			@endforeach
			@endif

			@else

			

 
				@if(count($product_details)>0) 
						@foreach($product_details as $product_list) 

						@if($product_list->pro_no_of_purchase < $product_list->pro_qty)

							
								

									  @php	  $product_get_img = explode("/**/",$product_list->pro_Img);  @endphp
									  @if($product_list->pro_status == 1)
									   @php 
										  $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
										  @endphp
								  @elseif($product_list->pro_status == 0) 
								  	@php
										   $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
										   @endphp
									  @endif

                                    <tr class="gradeA odd">

                                            <td class="sorting_1">{{ $i }}</td>
                                            <td class="center">{{ substr($product_list->pro_title,0,45) }}</td>
                                        <!--      <td>{{$product_list->pro_sku_number }}</td> -->
                                            <td class="center  ">{{ $product_list->stor_name }}</td>
                                            <td class="center">{{ $product_list->ci_name }} </td>
                                            <td class="center  ">{{ Helper::cur_sym() }} {{ $product_list->pro_price }}</td>
                                            <td class="center  ">{{ Helper::cur_sym() }} {{ $product_list->pro_disprice }}</td>

                                            <td class="center  ">
											<?php  $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
											@if($product_get_img != '')  {{-- image is null--}}
									
			 								@php	
									  $pro_img = $product_get_img[0]; 	
									  
									  $img_data = "public/assets/product/".$pro_img; @endphp
										  @if(file_exists($img_data) && $pro_img !='')  {{-- image not exists in folder  --}}
														@php 		
																			 $prod_path = url('').'/public/assets/product/'.$pro_img;
															@endphp	 
										  @else 
												 @if(isset($DynamicNoImage['productImg']))
												 	@php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp	
													@if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
													 @php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp	
													@endif
																		
												 @endif
																	 
																	 
												@endif
									   
						
						   @else
						   
							   
								 @if(isset($DynamicNoImage['productImg']))
												 	@php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; @endphp	
													@if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))
													 @php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  @endphp	
													@endif
																		
												 @endif
																	 
																	 
												@endif	
											
											<a href="#"><img style="height:40px;" src="{{ $prod_path }}"></a></td>

                                        

                                            <td class="center  "><a href="#">

                                           <a href=" {{ url('mer_edit_product/'.$product_list->pro_id

) }}" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') ? trans(Session::get('mer_lang_file').'.MER_EDIT') :  trans($MER_OUR_LANGUAGE.'.MER_EDIT') }}"> <i class="icon icon-edit" style="margin-left:15px;"></i></a>

                                         	<?php  $process; ?>
                                            <?php  /** if(count($delete_product) == "") {
                                             ?><a href="<?php echo url('delete_product'); ?>/<?php echo $product_list->pro_id; ?>" > <i class="icon icon-trash icon-1x" style="margin-left:15px;"></i></a> <?php } 

                                             else { 
                                             	foreach($delete_product as $dp) { } 

                                             		if($dp->order_pro_id != $product_list->pro_id) { ?> <a href="<?php echo url('delete_product'); ?>/<?php echo $product_list->pro_id; ?>" data-tooltip="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_DELETE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DELETE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DELETE');} ?>" > <i class="icon icon-trash icon-1x" style="margin-left:15px;"></i></a><?php } } **/?>


                                             	
                                            @if($product_list->pro_no_of_purchase==0) 
                                              
                                             	<a href="{{ url('mer_delete_product') }}/{{ $product_list->pro_id }}" > <i class="icon icon-trash icon-1x" style="margin-left:15px;"></i></a>

                                             @endif

                                            </td>

                                          

                             <td class="center  ">

                            

                             <a href="{{ url('mer_product_details')."/".$product_list->pro_id }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS') }}</a>

                             

                             </td>

                                        </tr>

			<?php $i++;  ?>
			@endif
			@endforeach
			@endif
			@endif

				 </tbody>

                                </table></div>

                               <!--  <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"><ul class="pagination"></ul></div></div></div></div><div class="row">

								

								<div class="col-sm-6">

								<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">

								<ul class="pagination">

								<li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">

								</li>

								</ul></div></div></div></div>
 -->
        </div>

    </div>

</div>

   

    </div>

                    

                    </div>

                    

                    

                    



                </div>

            <!--END PAGE CONTENT -->

 

        </div>

    

     <!--END MAIN WRAPPER -->



    <!-- FOOTER -->

      {!! $adminfooter !!}

    <!--END FOOTER -->





     <!-- GLOBAL SCRIPTS -->

     <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>

     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 

    <!-- END GLOBAL SCRIPTS -->

        <!-- PAGE LEVEL SCRIPTS -->

    <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>

    <!-- END GLOBAL SCRIPTS -->   

      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	   <script>

         $(function() {

            $( "#datepicker-8" ).datepicker({

               prevText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')}}",
               nextText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS') }}",

               showOtherMonths:true,

               selectOtherMonths: false

            });

            $( "#datepicker-9" ).datepicker({

               prevText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')}}",
               nextText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS') }}",
               showOtherMonths:true,

               selectOtherMonths: true

            });

         });
 /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/
$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
      </script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>

     <!-- END BODY -->

</html>

