﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD')}}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="{{ url('')}}/public/assets/css/plan.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
 {!! $merchantheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $merchantleftmenus !!}
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME') }}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')  : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD')}}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '')   ? trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD') }}</h5>
            
        </header>
	  @if (Session::has('pwd_error'))
		<p style="background-color:green;color:#fff; padding: 10px;">{!! Session::get('pwd_error') !!}</p>
		@endif
 @if (Session::has('success'))
		<p style="background-color:green;color:#fff; padding: 10px;">{!! Session::get('success') !!}</p>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
  {!! Form::open(array('url'=>'change_password_submit','class'=>'form-horizontal')) !!}   
	<div id="error_msg"  style="color:#F00;font-weight:800"  > </div>        


		<input type="hidden" value="{{ $mer_id }}" name="merchant_id" id="merchant_id">
 
                <div class="form-group">
                    <label class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_OLD_PASSWORD')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_OLD_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_OLD_PASSWORD') }}<span  class="text-sub">*</span></label>

                    <div class="col-lg-8">
					{{ Form::password('oldpwd', array('class' => 'form-control','id' => 'oldpwd')) }}
                       
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_NEW_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_NEW_PASSWORD') :  trans($MER_OUR_LANGUAGE.'.MER_NEW_PASSWORD') }}<span  class="text-sub">*</span></label>

                    <div class="col-lg-8">
					{{ Form::password('pwd', array('class' => 'form-control','id' => 'pwd')) }}
                      
                    </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')  : trans($MER_OUR_LANGUAGE.'.MER_CONFIRM_PASSWORD')}}<span  class="text-sub">*</span></label>

                    <div class="col-lg-8">
					{{ Form::password('confirmpwd', array('class' => 'form-control','id' => 'confirmpwd')) }}
                        
                    </div>
                </div>
				<div class="form-group">
				{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'pass1'])) !!}
                   

                    <div class="col-lg-8">
                     <button id="updatepwd"   class="btn btn-warning btn-sm btn-grad" style="color:#fff"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE')}}</button>
                     <a href="{{ url('merchant_settings')}}" class="btn btn-danger btn-sm btn-grad" style="color:#ffffff"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK'): trans($MER_OUR_LANGUAGE.'.MER_BACK')}}</a>
                   
                    </div>
					  
                </div>

                
				{{ Form::close() }}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; <?php echo $SITENAME;?>&nbsp;2017 &nbsp;</p>
    </div>
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>

<script type="text/javascript">
	$( document ).ready(function() {
	var pwd= $('#pwd');
	var confirmpwd= $('#confirmpwd');
var oldpwd=$('#oldpwd');
		
        $('#updatepwd').click(function() {

		if(oldpwd.val()=="")
		{
			oldpwd.css('border', '1px solid red');
			$('#error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_OLD_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_OLD_PASSWORD'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_OLD_PASSWORD')}} ');
			oldpwd.focus();
			return false;
		}
		 
		else if(pwd.val()=="")
		{
			pwd.css('border', '1px solid red');
			oldpwd.css('border', ''); 
			 confirmpwd.css('border', ''); 
			$('#error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_NEW_PASSWORD')}}');
			pwd.focus();
			return false;
		}
	 

		else if(confirmpwd.val()=="")
		{
			confirmpwd.css('border', '1px solid red');
			oldpwd.css('border', ''); 
			 pwd.css('border', ''); 
			$('#error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')}}');
			confirmpwd.focus();
			return false;
		}
		 
		 
		

});
});
</script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
  <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>   
</body>
     <!-- END BODY -->
</html>
