<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo $SITENAME; ?><?php if (Lang::has(Session::get('mer_lang_file').'.MER_ADMIN_TRANSACTION_DASHBOARD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_ADMIN_TRANSACTION_DASHBOARD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_ADMIN_TRANSACTION_DASHBOARD');} ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/theme.css" />
	<link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/MoneAdmin.css" />
<?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); if(count($favi)>0) { foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo url(''); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php } ?>
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="<?php echo url('')?>/public/assets/css/datepicker.css" rel="stylesheet">	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<?php echo url('')?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo url('')?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jquery.min.js"></script>
	

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
      {!!$merchantheader!!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!!$merchantleftmenus!!}
        
        
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo url('sitemerchant_dashboard'); ?>"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_HOME');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_HOME');} ?></a></li>
                                <li class="active"><a href="#"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TRANSACTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION');} ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-dashboard"></i></div>
            <h5><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION_DASHBOARD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TRANSACTION_DASHBOARD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION_DASHBOARD');} ?></h5>
            
        </header>
	    <div class="row">
                <div class="col-lg-6" style="padding:30px; min-height:750px">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><?php if (Lang::has(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TOTAL_TRANSACTIONS_COUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TOTAL_TRANSACTIONS_COUNT');} ?></strong> 
                            <a href="#"><i class="icon icon-align-justify pull-right"></i></a>
                        </div>
                        <div class="panel-body">
                           <div id="chart6" style="margin-top:20px; margin-left:20px; width:450px; height:450px;"></div>
                           <table width="30%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5"><label class="label label-active"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS');} ?></label></td>
                                <td style="background:#eaa228"><label class="label label-archive"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_DEALS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DEALS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DEALS');} ?></label></td>
                                 <td style="background:#eaa228"><label class="label label-archive"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_AUCTIONS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_AUCTIONS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_AUCTIONS');} ?></label></td>                         
                              </tr>
                            </tbody></table>
                        </div>
                       
                        	

                        
                    </div>
                </div>
                <br>
                <div class="col-lg-5 " style="padding-top:30px">                
                <strong><?php if (Lang::has(Session::get('mer_lang_file').'.MER_MARKETING_RESPONSE_RATE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MARKETING_RESPONSE_RATE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MARKETING_RESPONSE_RATE');} ?></strong> 
                </div>
                
                <div class="col-lg-5 " style="padding-top:10px">
                
                    <div class="panel panel-default">
                        
                        
                        <div class="panel-heading">
                            <strong><?php if (Lang::has(Session::get('mer_lang_file').'.MER_DEALS_TRANSACTION_DETAILS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DEALS_TRANSACTION_DETAILS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DEALS_TRANSACTION_DETAILS');} ?></strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                    <tr>
                                    	<th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTIONS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TRANSACTIONS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TRANSACTIONS');} ?></th>
                                        <th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_COUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_COUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_COUNT');} ?></th>
                                        <th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_AMOUNT');} ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TODAY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TODAY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TODAY');} ?></td>
                                            <td><?php echo $producttoday[0]->count;?></td>
                                            <td><?php if($producttoday[0]->amt){echo $producttoday[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_7_DAYS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_7_DAYS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_7_DAYS');} ?></td>
                                            <td><?php echo $produst7days[0]->count;?></td>
                                            <td><?php if($produst7days[0]->amt){echo $produst7days[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_30_DAYS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_30_DAYS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_30_DAYS');} ?></td>
                                            <td><?php echo $product30days[0]->count;?></td>
                                            <td><?php if($product30days[0]->amt){echo $product30days[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
							   </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-5 " style="padding-top:10px">
                
                    <div class="panel panel-default">
                        
                        
                        <div class="panel-heading">
                            <strong><?php if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS_TRANSACTION_DETAILS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PRODUCTS_TRANSACTION_DETAILS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS_TRANSACTION_DETAILS');} ?></strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                    <tr>
                                    	<th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTIONS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TRANSACTIONS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TRANSACTIONS');} ?></th>
                                        <th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_COUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_COUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_COUNT');} ?></th>
                                        <th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_AMOUNT');} ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TODAY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TODAY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TODAY');} ?></td>
                                            <td><?php echo $dealstoday[0]->count;?></td>
                                            <td><?php if($dealstoday[0]->amt){echo $dealstoday[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_7_DAYS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_7_DAYS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_7_DAYS');} ?></td>
                                            <td><?php echo $deals7days[0]->count;?></td>
                                            <td><?php if($deals7days[0]->amt){echo $deals7days[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_30_DAYS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_30_DAYS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_30_DAYS');} ?></td>
                                            <td><?php echo $deals30days[0]->count;?></td>
                                            <td><?php if($deals30days[0]->amt){echo $deals30days[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="col-lg-5 " style="padding-top:10px">
                
                    <div class="panel panel-default">
                        
                        
                        <div class="panel-heading">
                            <strong><?php if (Lang::has(Session::get('mer_lang_file').'.MER_AUCTION_TRANSACTION_DETAILS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_AUCTION_TRANSACTION_DETAILS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_AUCTION_TRANSACTION_DETAILS');} ?></strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                    <tr>
                                    	<th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTIONS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TRANSACTIONS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TRANSACTIONS');} ?></th>
                                        <th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_COUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_COUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_COUNT');} ?></th>
                                        <th><?php if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_AMOUNT');} ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
											<td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_TODAY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TODAY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TODAY');} ?></td>
                                            <td><?php echo $auctiontoday[0]->count;?></td>
                                            <td><?php if($auctiontoday[0]->amt){echo $auctiontoday[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_7_DAYS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_7_DAYS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_7_DAYS');} ?></td>
                                            <td><?php echo $auction7days[0]->count;?></td>
                                            <td><?php if($auction7days[0]->amt){echo $auction7days[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_30_DAYS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_30_DAYS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_30_DAYS');} ?></td>
                                            <td><?php echo $auction30days[0]->count;?></td>
                                            <td><?php if($auction30days[0]->amt){echo $auction30days[0]->amt;}else {echo "-";}?></td>
                                            
                                        </tr>
                                        
                                  
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    </div>
</div>
   
    </div>
                    
                    
                    <div class="row">
                    	<div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <strong><?php if (Lang::has(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_INVOICE_DETAILS');} ?></strong> 
                            </div>
                             
                            <div class="panel-body">
                              <ul class="nav nav-pills">
                                <li class="active"><a  style="cursor:pointer;" onclick="showchart1();" data-toggle="tab"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_ONE_YEAR_PRODUCT_TRANSACTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_ONE_YEAR_PRODUCT_TRANSACTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_ONE_YEAR_PRODUCT_TRANSACTION');} ?></a>  </li>
                                   <li class="active"><a  style="cursor:pointer;"  onclick="showchart2();" data-toggle="tab"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_ONE_YEAR_DEALS_TRANSACTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_ONE_YEAR_DEALS_TRANSACTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_ONE_YEAR_DEALS_TRANSACTION');} ?></a>  </li>
                                      <li class="active"><a  style="cursor:pointer;" onclick="showchart3();"  data-toggle="tab"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_ONE_YEAR_AUCTION_TRANSACTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LAST_ONE_YEAR_AUCTION_TRANSACTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LAST_ONE_YEAR_AUCTION_TRANSACTION');} ?></a>  </li>
                              
                              </ul>
                            
                            <div class="tab-content">
                              <div id="chart1" style="margin-top:20px; margin-left:20px; max-width:950px; height:470px;"></div>
                               <div id="chart2" style="margin-top:20px;margin-left:20px; max-width:950px; height:470px;"></div>
                                <div id="chart3" style="margin-top:20px;margin-left:20px; max-width:950px; height:470px;"></div>
                                <!--<div class="tab-pane fade" id="profile-pills">
                                    <h4>Profile Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>-->
                                <!--<div class="tab-pane fade" id="messages-pills">
                                    <h4>Messages Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>-->
                                
                            </div>
                            
                            
			
			
		</div>
		</div>
                             
		
                    </div>
                    </div>
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; Dip Multivendor&nbsp;2014 &nbsp;</p>
    </div>
    <!--END FOOTER -->
  <script src="<?php echo url(''); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript">

	function showchart1()
	{
		document.getElementById("chart1").style.display='block';
		document.getElementById("chart2").style.display='none';
		document.getElementById("chart3").style.display='none';
		/*$( "#chart1" ).show();
		$( "#chart2" ).hide();
		$( "#chart3" ).hide();*/
	}
	function showchart2()
	{ 
		document.getElementById("chart2").style.display='block';
		document.getElementById("chart1").style.display='none';
		document.getElementById("chart3").style.display='none';
	}
	function showchart3()
	{
		document.getElementById("chart3").style.display='block';
		document.getElementById("chart2").style.display='none';
		document.getElementById("chart1").style.display='none';
	}
	
	</script>
	<script>
	$(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[<?php echo $producttransactioncnt; ?>,<?php echo $dealtransactioncnt; ?>,<?php echo $auctiontransactioncnt; ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer!!});
		});
	</script>
   
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$productchartdetails. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
     <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$dealchartdetails. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        
        plot1 = $.jqplot('chart2', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart2').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
   
     <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$auctionchartdetails. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        
        plot1 = $.jqplot('chart3', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart3').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
   
   <!--- Chart Plugins -->
      <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo url(''); ?>/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    
    <!--- --->
    

 
<script src="<?php echo url(''); ?>/public/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="<?php echo url(''); ?>/public/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="<?php echo url(''); ?>/public/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>
	<script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="<?php echo url(''); ?>/public/assets/js/bar_chart.js"></script>
       <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script> 
        
     
</body>
     <!-- END BODY -->
</html>
