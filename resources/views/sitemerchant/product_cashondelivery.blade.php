<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo $SITENAME; ?> <?php if (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_CASH_ON_DELIVERY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MERCHANT_CASH_ON_DELIVERY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_CASH_ON_DELIVERY');} ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="public/assets/css/main.css" />
    <link rel="stylesheet" href="public/assets/css/theme.css" />
    <link rel="stylesheet" href="public/assets/css/MoneAdmin.css" />
<?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); if(count($favi)>0) { foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo url(''); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php } ?>
    <link rel="stylesheet" href="public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        
        {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
       
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo url('sitemerchant_dashboard'); ?>"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_HOME');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_HOME');} ?></a></li>
                                <li class="active"><a href="#"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY');} ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY');} ?></h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SEARCH');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SEARCH');} ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
					 <div class="col-lg-4">
					<input type="text" class="form-control" placeholder="" id="text1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_NAME_EMAIL_COUPON_CODE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_NAME_EMAIL_COUPON_CODE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_NAME_EMAIL_COUPON_CODE');} ?>
		</div>
					  <div class="col-lg-4"><button class="btn btn-warning btn-sm btn-grad"><a href="#" style="color:#fff"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SEARCH');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SEARCH');} ?></a></button></div>
                    </div>
                </div>
				 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
					<?php if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATA_FOUND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_NO_DATA_FOUND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_NO_DATA_FOUND');} ?>
                    </div>
                </div>
         </form>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
