{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

<!-- Header End====================================================================== -->

  <!-- Main Container -->

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="{{  url('index') }}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME') }}</a><span>&raquo;</span></li> 

            <li><strong>{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT') }}</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-12 col-xs-12">

         

          <div class="shop-inner">

            <div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.SOLD_PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.SOLD_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_PRODUCTS') }} @endif</h2>

            </div>

           

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			  @php $sold_product_error = "";

				 $sold_product_count=0; @endphp

				 @if($get_store_product_by_id)  

				 @foreach($get_store_product_by_id as $fetch_most_visit_pro) 

				 @if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty) 

				 @php  $sold_product_count++; 

				 $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

				 $res = base64_encode($fetch_most_visit_pro->pro_id);

				 $sold_product_error = 1; 

				 $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

				 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

				 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);@endphp

			  

                <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner sold-item">

                      <div class="product-thumbnail">

                        <div class="pr-img-area"> 

						 <span class="sold"></span>

					   @php $product_img   = $mostproduct_img[0];

						$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

						$img_data   = "public/assets/product/".$product_img; @endphp

						@if(file_exists($img_data) && $product_img !='')   

						@php  $prod_path = url('').'/public/assets/product/' .$product_img; @endphp                 

						@else  

						@if(!isset($DynamicNoImage['productImg']))

						@php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp

						@if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

						@php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

						@endif

						@endif

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

						 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						@endif 

						

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						@endif

						

						@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')

						 <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						@endif

						  

						  </div>

						<div class="sold-img"><img src="images/sold-out.png"></div>

                       

                      </div>

					  

					    @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

						@php  $title = 'pro_title'; @endphp

						@else  @php $title = 'pro_title_'.Session::get('lang_code'); @endphp  @endif

						

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> 

						  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

						  <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">{{substr($fetch_most_visit_pro->$title,0,25) }} {{  strlen($fetch_most_visit_pro->$title)>25?'..':'' }}</a> 

						  @endif

						  

						  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')

						  <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">{{substr($fetch_most_visit_pro->$title,0,25) }}    {{  strlen($fetch_most_visit_pro->$title)>25?'..':'' }}</a> 

						  @endif

						  

						  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						  <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">{{substr($fetch_most_visit_pro->$title,0,25) }}    {{  strlen($fetch_most_visit_pro->$title)>25?'..':'' }}</a> 

						  @endif

						  </div>

						 

                          <div class="item-content">

                            

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $fetch_most_visit_pro->pro_disprice }}</span> </span> </div>

                            </div>

						   <div class="pro-action">

                                <button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span> </button>

                           </div>

                          </div>

                        </div>

                      </div>

					  

                    </div>

                  </div>

                </li>

				 @endif @endforeach

				  <?php  if($sold_product_count==0)

					{

						echo "<center>No Products Found!</centre>";

					} ?>

			     @else  

					<h5 style="color:#933;" >@if(Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_PRODUCTS')!= '') {{ trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER_PRODUCTS') }} @endif.</h5>

				@endif

				

              </ul>

            </div>

           <!-- <div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

          </div>

		  

		  <!--for deals-->

		   <div class="shop-inner">

            <div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.SOLD_DEALS')!= '') {{ trans(Session::get('lang_file').'.SOLD_DEALS') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_DEALS') }} @endif</h2>

            </div>

           

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			  @php  $sold_deal_error = "";

				 $sold_deals_count=0; @endphp

				 @if($get_store_deal_by_id)  

				 @php $date = date('Y-m-d H:i:s'); @endphp

				 @foreach($get_store_deal_by_id as $store_deal_by_id) 

				 @if(($store_deal_by_id->deal_no_of_purchase >= $store_deal_by_id->deal_max_limit)) 

				 @php $sold_deals_count++; @endphp

			 

				 @php $sold_deal_error = 1; 

				 $dealdiscount_percentage = $store_deal_by_id->deal_discount_percentage;

				 $deal_img= explode('/**/', $store_deal_by_id->deal_image);

				 $mcat = strtolower(str_replace(' ','-',$store_deal_by_id->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$store_deal_by_id->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$store_deal_by_id->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$store_deal_by_id->ssb_name)); 

				 $res = base64_encode($store_deal_by_id->deal_id); @endphp

				 @php $product_image     = $deal_img[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/deals/".$product_image; @endphp

				 @if(file_exists($img_data) && $product_image !='')   

				 @php  $prod_path = url('').'/public/assets/deals/' .$product_image;  @endphp                

				 @else  

				 @if(isset($DynamicNoImage['dealImg']))

				 @php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg'];@endphp

				 @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

				 @php $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp

				 @endif

				 @endif

				 @endif 

			  

                <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner sold-item">

                      <div class="product-thumbnail">

                        <div class="pr-img-area"> 

					    <span class="sold"></span>

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

						 <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						 @elseif($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						 <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" >

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						@elseif($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						 <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						@elseif($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 

						 <a href="{!! url('dealview').'/'.$mcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a>

						@endif

						  

						  </div>

                       <div class="sold-img" ><img src="images/sold-out.png"></div>

                      </div>

					  

					    @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

						@php  $deal_title = 'deal_title'; @endphp

						@else @php  $deal_title = 'deal_title_'.Session::get('lang_code'); @endphp @endif

						

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> 

						  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

						  <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">{{substr($store_deal_by_id->$deal_title,0,25) }}    {{  strlen($store_deal_by_id->$deal_title)>25?'..':'' }}</a> 

						  @elseif($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						  <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">{{substr($store_deal_by_id->$deal_title,0,25) }}    {{  strlen($store_deal_by_id->$deal_title)>25?'..':'' }}</a> 

						  @elseif($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						  <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">{{substr($store_deal_by_id->$deal_title,0,25) }}    {{  strlen($store_deal_by_id->$deal_title)>25?'..':'' }}</a> 

						  @elseif($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '') 

						  <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">{{substr($store_deal_by_id->$deal_title,0,25) }}    {{  strlen($store_deal_by_id->$deal_title)>25?'..':'' }}</a>

						  @endif

						  </div>

						 

                          <div class="item-content">

                            

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $store_deal_by_id->deal_discount_price }}</span> </span> </div>

                            </div>

							<div class="pro-action">

                                <button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD')!= '') {{ trans(Session::get('lang_file').'.SOLD') }} @else {{ trans($OUR_LANGUAGE.'.SOLD') }} @endif</span> </button>

                           </div>

                          </div>

                        </div>

                      </div>

					  

                    </div>

                  </div>

                </li>

				 @endif @endforeach

				  <?php  if($sold_deals_count==0)

					{

					?><center> {{(Lang::has(Session::get('lang_file').'.NO_DEALS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_DEALS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_DEALS_AVAILABLE') }}</centre>;

					<?php } ?>

			     @else  

					<h5 style="color:#933;" >@if(Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_DEALS')!= '') {{ trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_DEALS') }} @else {{ trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER_DEALS') }} @endif.</h5>

				@endif

				

              </ul>

            </div>

           <!-- <div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

          </div>

		

		  

        </div>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   @include('service_section')

  {!! $footer !!}

  



<script type="text/javascript">

   $(document).ready(function() {

       $(document).on("click", ".customCategories .topfirst b", function() {

           $(this).next("ul").css("position", "relative");

   

           $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

           $(this).next("ul").toggle();

       });

   

       $(document).on("click", ".morePage", function() {

           $(".nextPage").slideToggle(200);

       });

   

       $(document).on("click", "#smallScreen", function() {

           $(this).toggleClass("customMenu");

       });

   

       $(window).scroll(function() {

           if ($(this).scrollTop() > 250) {

               $('#comp_myprod').show();

           } else {

               $('#comp_myprod').hide();

           }

       });

   

   });

</script>



<script language="JavaScript">

   $(document).ready(function() {

       $(".topnav").accordion({

           accordion:false,

           speed: 500,

           closedSign: '<span class="icon-chevron-right"></span>',

           openedSign: '<span class="icon-chevron-down"></span>'

       });

   });

   

</script>

<script type="text/javascript">

   $.ajaxSetup({

   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

   });

</script>