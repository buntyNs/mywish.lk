{!! $navbar !!}



<!-- Navbar ================================================== -->

{!! $header !!}



<div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="" href="{{ url('index') }}">@if (Lang::has(Session::get('lang_file').'.HOME')!= '') {{  trans(Session::get('lang_file').'.HOME') }} @else {{ trans($OUR_LANGUAGE.'.HOME') }} @endif</a><span>&raquo;</span></li>

            <li class=""><a title="Go to Home Page" href="{{ url('stores') }}"><strong> @if (Lang::has(Session::get('lang_file').'.STORES')!= '') {{ trans(Session::get('lang_file').'.STORES') }} @else {{ trans($OUR_LANGUAGE.'.STORES') }} @endif</strong></a></li>

      

          </ul>

        </div>

      </div>

    </div>

</div>



	<center> @if (Session::has('success_store'))

	<div class="alert alert-warning alert-dismissable">{!! Session::get('success_store') !!}

	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>

	@endif</center>

  

  <section class="blog_post">

    <div class="container"> 

      

      <!-- Center colunm-->

      <div class="blog-wrapper store-blg-wrpr">

        <div class="page-title">

          <h2>@if(Lang::has(Session::get('lang_file').'.STORES')!= '') {{ trans(Session::get('lang_file').'.STORES') }}  @else{{ trans($OUR_LANGUAGE.'.STORES') }} @endif</h2>

        </div>

		

		@php

		$get_store_count=DB::table('nm_store')

		->join('nm_merchant','nm_merchant.mer_id','=','nm_store.stor_merchant_id')

		->where('nm_merchant.mer_staus','=',1)

		->where('nm_store.stor_status', '=', 1)

		->groupby('stor_merchant_id')

		->orderby('nm_store.stor_id','desc')

		->get()

		->count();

	 

		$i=1;  @endphp

	  

	   @if(count($get_store_details)>0)

				   

        <ul class="blog-posts">

			 @foreach($get_store_details as $store) 

				@if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				@php  $stor_name = 'stor_name'; @endphp

				@else @php  $stor_name = 'stor_name_langCode'; @endphp @endif 



			    @php $product_image     = $store->stor_img;

			   

				$prod_path  = url('').'/public/assets/default_image/No_image_store.png';

				$img_data   = "public/assets/storeimage/".$product_image; @endphp

				

				@if(file_exists($img_data) && $product_image !='')   

			

			   @php $prod_path = url('').'/public/assets/storeimage/' .$product_image;  @endphp              

			   @else  

					

					@if(isset($DynamicNoImage['store']))

					  @php  $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; @endphp



						@if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))

						   @php $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

					@endif

				@endif

				

			   @php $alt_text   = $store->$stor_name;



			  $store_comment = DB::table('nm_review')->where('store_id', '=', $store->stor_id)->count();



			   @endphp

			   

          <li class="post-item col-md-3 col-sm-6">

           <div class="blog-box"> <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($store->stor_id)))); ?>"> <img class="primary-img" src="{{ $prod_path }}" alt='{{ $alt_text }}'></a>

                <div class="blog-btm-desc">

                  <div class="blog-top-desc">

                    <div class=""></div>

                   <center> <h4> {{substr($store->$stor_name,0,25) }}

					{{strlen($store->$stor_name)>25?'..':'' }}</h4>

                    <div class="jtv-entry-meta">

					<!--<i class="fa fa-user-o"></i> <strong>Admin</strong> <a>--><i class="fa fa-commenting-o"></i> 

					<strong>{{ $store_comment }} @if (Lang::has(Session::get('lang_file').'.COMMENT')!= '') {{ trans(Session::get('lang_file').'.COMMENT') }}  @else {{ trans($OUR_LANGUAGE.'.COMMENT') }} @endif</strong></a></div></center>

                  </div>

				  <center style="margin-bottom: 20px;"> <table border="0" class="table table-hover">

				  <tr>

					<td>@if (Lang::has(Session::get('lang_file').'.DEALS')!= '') {{ trans(Session::get('lang_file').'.DEALS') }} @else {{ trans($OUR_LANGUAGE.'.DEALS') }} @endif</td>

					 <td>:</td>

					  <td>@if($get_store_deal_count[$store->stor_id] != 0) 

				 @php  $sold_count=0;

				 $store->stor_id;

				 $date = date('Y-m-d H:i:s');

				$store_result = DB::table('nm_deals')->where('deal_status', '=', 1)->where('deal_shop_id','=',$store->stor_id)->where('deal_start_date','<',$date)->where('deal_end_date', '>', $date)->get(); @endphp

			  

				@foreach($store_result as $sold_pro)

				  @if($sold_pro->deal_no_of_purchase < $sold_pro->deal_max_limit) 



				  

					 @php $sold_count++; @endphp

				  @endif

				@endforeach

					@if($sold_count!=0){{ $sold_count }} 

				

				

				@else {{ 'N/A' }} @endif

				 

			   @else {{ 'N/A' }} @endif</td>

				  </tr>

				  <tr>

					<td>@if (Lang::has(Session::get('lang_file').'.PRODUCTS')!= '') {{ trans(Session::get('lang_file').'.PRODUCTS') }}  @else {{ trans($OUR_LANGUAGE.'.PRODUCTS') }} @endif</td>

					 <td>:</td>

					  <td>@if($get_store_product_count[$store->stor_id] != 0) 

			@php  $sold_count=0;

			$store->stor_id;

        

			$store_result = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_sh_id', '=', $store->stor_id)->get(); @endphp

        

			@foreach($store_result as $sold_pro)

			@if($sold_pro->pro_no_of_purchase < $sold_pro->pro_qty) 



          

             @php $sold_count++; @endphp

          @endif

        @endforeach

       <!--  //echo $sold_count;

			// echo $get_store_product_count[$store->stor_id]; 

     

		//$get_store_product_by_id      = HomeController::get_store_product_by_id($store->stor_id);-->

        @if($sold_count!=0){{ $sold_count }} 

        @else {{ 'N/A' }} @endif     

		@else {{ 'N/A' }} @endif</td>

				  </tr></table>

				  </center>

                  <a class="read-more str-det" href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($store->stor_id)))); ?>"> @if (Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '') {{ trans(Session::get('lang_file').'.VIEW_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.VIEW_DETAILS') }} @endif</a> </div>

              </div>

          </li>

		   @endforeach  

          

        </ul>@else

			<h5 style="color:#933;" >@if (Lang::has(Session::get('lang_file').'.NO_STORES_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_STORES_FOUND') }} @else {{ trans($OUR_LANGUAGE.'.NO_STORES_FOUND') }} @endif.</h5>

         @endif

		

        <div class="sortPagiBar">

          <div class="pagination-area" >

            <ul>

              {!! $get_store_details->render() !!}

            </ul>

          </div>

        </div>

      </div>

      <!-- ./ Center colunm --> 

      

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- service section -->

   @include('service_section')

  

  {!! $footer !!}

  

  <script type="text/javascript">



    $(document).ready(function() {

        $(document).on("click", ".customCategories .topfirst b", function() {

            $(this).next("ul").css("position", "relative");



            $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

            $(this).next("ul").toggle();

        });



        $(document).on("click", ".morePage", function() {

            $(".nextPage").slideToggle(200);

        });



        $(document).on("click", "#smallScreen", function() {

            $(this).toggleClass("customMenu");

        });



        $(window).scroll(function() {

            if ($(this).scrollTop() > 250) {

                $('#comp_myprod').show();

            } else {

                $('#comp_myprod').hide();

            }

        });



    });

</script>

  <!-- For Responsive menu-->

</body>

</html>



<script>

var pagenumber=$("#page_number").val();

var i=0;

var store_count='<?php echo $get_store_count; ?>';

var pagination=Math.round(store_count/4);

$(window).scroll(function(){

  if($(window).scrollTop()+$(window).height() >= $(document).height())  

  {

      $(".myLoader").show();

  pagenumber++; 

  $("#page_number").val(pagenumber);



if(pagination<=pagenumber)

{

   $(".myLoader").hide();

  return false;

}

  $.ajax({

    url:'<?php echo url("stores_ajax"); ?>?page='+pagenumber,

    type:"get",

    success:function(data,status){

      if(data.trim()=='' && i==0){ 

      $('img#thumb').removeAttr('id');

      i++;

      $(".myLoader").hide();

    } else {

        $(".myLoader").hide();

        $('#storpg').append(data);

      }

      }

    });

  }

});

</script>