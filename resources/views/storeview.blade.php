<!DOCTYPE html>

<html lang="en">


<head>

 <meta charset="utf-8">  
	  @if(count($get_store_merchant_by_id) >0) 

      @foreach($get_store_by_id as $get_store_details_id) @endforeach 

      @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

      @php    $stor_name            = 'stor_name';

      $stor_metakeywords    = 'stor_metakeywords';

      $stor_metadesc        = 'stor_metadesc'; @endphp

      @else   

      @php  $stor_name            = 'stor_name_'.Session::get('lang_code');

      $stor_metakeywords    = 'stor_metakeywords_'.Session::get('lang_code');

      $stor_metadesc        = 'stor_metadesc_'.Session::get('lang_code'); @endphp

      @endif

      @php  $metaname     = $get_store_details_id->$stor_name;

      $metakeywords = $get_store_details_id->$stor_metakeywords;

      $metadesc     = $get_store_details_id->$stor_metadesc;    @endphp  

      @else

      @if($metadetails)

      @foreach($metadetails as $metainfo) 

      @php  $metaname   = $metainfo->gs_metatitle;

      $metakeywords = $metainfo->gs_metakeywords;

      $metadesc     = $metainfo->gs_metadesc; @endphp

      @endforeach

      @else

      @php

      $metaname="";

      $metakeywords="";

      $metadesc=""; @endphp

      @endif

      @endif

 

	  <title>{{ $metaname }}</title>

      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <meta name="description" content="{{ $metadesc }}">

      <meta name="keywords" content="{{ $metakeywords }}">

      <meta name="author" content="{{ $metaname }}">

 

</head>



<body class="about_us_page">



{!! $navbar !!}

<!-- Navbar ================================================== -->

{!! $header !!}

		 

  <!-- Main Container -->

      @if($get_store_merchant_by_id)  

      @foreach($get_store_by_id as $get_store_details_id) @endforeach 

      @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

      @php  $stor_name = 'stor_name'; @endphp

      @else @php  $stor_name = 'stor_name_'.Session::get('lang_code'); @endphp @endif 

      @php $product_image     = $get_store_details_id->stor_img; 

      $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

      $img_data   = "public/assets/storeimage/".$product_image;@endphp

      @if(file_exists($img_data) && $product_image !='')    

      @php $prod_path = url('').'/public/assets/storeimage/' .$product_image;  @endphp                

      @else  

      @if(isset($DynamicNoImage['store']))

      @php  $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; @endphp

      @if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))  

      @php    $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

      @endif

      @endif 

      @php $alt_text   = $get_store_details_id->$stor_name; @endphp 

  <div class="main container">

  

			<center>

               @if (Session::has('success_store'))

               <div class="alert alert-warning alert-dismissable">{!! Session::get('success_store') !!}

                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

               </div>

               @endif

            </center>

 

     <div class="about-page">

	   <div class="row">

	   <div class="col-xs-12 col-sm-6">

          <div class="single-img-add sidebar-add-slider">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 

              <!-- Wrapper for slides -->

              <div class="carousel-inner" role="listbox">

                <div class="item active"> <img src="{{ $prod_path }}" alt="{{ $alt_text }}"> </div>  

              </div>

            </div>

          </div>

        </div>

		

        <div class="col-xs-12 col-sm-6"> 

		

			   @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') 

               @php $stor_name = 'stor_name';

               $stor_address1 = 'stor_address1';

               $stor_address2 = 'stor_address2'; @endphp

               @else   

               @php $stor_name = 'stor_name_'.Session::get('lang_code'); 

               $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 

               $stor_address2 = 'stor_address2_'.Session::get('lang_code');  @endphp

               @endif

			   

          <h1><span class="text_color">{{ $get_store_details_id->$stor_name }}</span></h1>

		  

			   @php

               $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

               $product_count;

               $multiple_countone = $one_count *1;

               $multiple_counttwo = $two_count *2;

               $multiple_countthree = $three_count *3;

               $multiple_countfour = $four_count *4;

               $multiple_countfive = $five_count *5;

               $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; @endphp

               @if($product_count)

               @php $product_divide_count = $product_total_count / $product_count; 

			   $product_divide_count = round($product_divide_count);@endphp

               @if(($product_divide_count != '0')&&($product_divide_count <= 1))

				   

		  <div class="rating"> 

			   @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif <img src='{{ url('images/stars-1.png') }}' style='margin-bottom:10px;'>

               @elseif($product_divide_count <= '2')

               @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif : <img src='{{ url('./images/stars-2.png') }}' style='margin-bottom:10px;'>

               @elseif($product_divide_count <= 3)

               @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif : <img src='{{ url('./images/stars-3.png') }}' style='margin-bottom:10px;'>

               @elseif($product_divide_count <= '4')

               @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{ trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif : <img src='{{ url('./images/stars-4.png') }}' style='margin-bottom:10px;'>

               @elseif($product_divide_count <= '5')

               @if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif : <img src='{{ url('./images/stars-5.png') }}' style='margin-bottom:10px;'>

               @else 

               @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE')!= '') {{ trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE') }} @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_STORE') }} @endif

               @endif

               @elseif($product_count)

               @php $product_divide_count = $product_total_count / $product_count; @endphp

               @else  

               @if (Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE')!= '') {{  trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE') }} @else {{ trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_STORE') }} @endif

		  

		  &nbsp; <span>( @if(isset($count_review_rating)) {{ $count_review_rating }} @endif @if (Lang::has(Session::get('lang_file').'.REVIEWS_AND_RATINGS')!= '') {{  trans(Session::get('lang_file').'.REVIEWS_AND_RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.REVIEWS_AND_RATINGS') }} @endif )</span>

		   @endif 

		  

          <ul>

            <li>@if (Lang::has(Session::get('lang_file').'.ADDRESS1')!= '') {{  trans(Session::get('lang_file').'.ADDRESS1') }} @else {{ trans($OUR_LANGUAGE.'.ADDRESS1') }} @endif</span> &nbsp;:&nbsp; <a>{{ $get_store_details_id->$stor_address1 }}.</a></li>

			

			 <li>@if (Lang::has(Session::get('lang_file').'.ADDRESS2')!= '') {{  trans(Session::get('lang_file').'.ADDRESS2') }} @else {{ trans($OUR_LANGUAGE.'.ADDRESS2') }} @endif</span> &nbsp;:&nbsp; <a>{{ $get_store_details_id->$stor_address2 }}.</a></li>

			 

			 <li>@if (Lang::has(Session::get('lang_file').'.ZIP_CODE')!= '') {{  trans(Session::get('lang_file').'.ZIP_CODE') }} @else {{ trans($OUR_LANGUAGE.'.ZIP_CODE') }} @endif</span> &nbsp;:&nbsp; <a>{{ $get_store_details_id->stor_zipcode }}.</a></li>

			  

			 <li>@if (Lang::has(Session::get('lang_file').'.MOBILE_NO')!= '') {{  trans(Session::get('lang_file').'.MOBILE_NO') }} @else {{ trans($OUR_LANGUAGE.'.MOBILE_NO') }} @endif</span> &nbsp;:&nbsp; <a>{{ $get_store_details_id->stor_phone }}.</a></li>

			  

			 <li>@if (Lang::has(Session::get('lang_file').'.WEBSITE')!= '') {{  trans(Session::get('lang_file').'.WEBSITE') }} @else {{ trans($OUR_LANGUAGE.'.WEBSITE') }} @endif</span> &nbsp;:&nbsp; <a>{{ $get_store_details_id->stor_website }}.</a></li>

			   

			 <li><div id="somecomponent" style="width: 100%; height: 300px;" class="store-map"></div></li>

			 

          </ul>

		  </div>

        </div>

       

      </div>

    </div>

  </div>

  

  

  <!---store products-->

  

  <div class="inner-box">

    <div class="container">

      <div class="row store_branch"> 

       

        <!-- Best Sale -->

        <div class="col-sm-12 col-md-12 jtv-best-sale special-pro">

          <div class="jtv-best-sale-list">

            <div class="wpb_wrapper">

              <div class="best-title text-left">

                 <h3><?php echo ($get_store_details_id->$stor_name);  ?> - <span class="text_color">@if (Lang::has(Session::get('lang_file').'.PRODUCTS')!= '') {{  trans(Session::get('lang_file').'.PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.PRODUCTS') }} @endif</span></h3>

              </div>

            </div>

           

            <div class="slider-items-products">

              <div id="jtv-best-sale-slider" class="product-flexslider">

                <div class="owl-carousel owl-theme jtv-best-sale-slider">

				

				 @if(count($get_store_product_by_id) > 0 )  

				 @foreach($get_store_product_by_id as $fetch_most_visit_pro)  

				 

				 

				 @php $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

				 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

				 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);

				 $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

				 $res = base64_encode($fetch_most_visit_pro->pro_id); @endphp

				 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				 @php $pro_title = 'pro_title'; @endphp

				 @else @php  $pro_title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

				 @php   $product_img    = $mostproduct_img[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/product/".$product_img; @endphp

				 @if(file_exists($img_data) && $product_img !='')   

				 @php  $prod_path = url('').'/public/assets/product/' .$product_img;  @endphp                 

				 @else  

				 @if(isset($DynamicNoImage['productImg']))

				 @php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; @endphp

				 @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

				 @php    $prod_path = $dyanamicNoImg_path; @endphp @endif

				 @endif

				 @endif   

				 @php   $alt_text   = substr($fetch_most_visit_pro->$pro_title,0,25); @endphp

		 

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">  

                        <div class="pr-img-area">

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

						<a  href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> 

						  <img class="first-img" alt="{{ $alt_text }}" src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif

						

						@if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						<a  href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">

                          <figure> 

						  <img class="first-img" alt="{{ $alt_text }}" src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif

						

						@if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						<a  href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}">

                          <figure> 

						  <img class="first-img" alt="{{ $alt_text }}" src="{{ $prod_path }}"> 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                        </a> 

						@endif

									 

						</div>

                        <!--<div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>

                            <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>

                            <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>

                          </div>

                        </div>-->

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="" href="">  {{ substr($fetch_most_visit_pro->$pro_title,0,20) }}... </a> 



                            </div>

                          <div class="item-content">

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $fetch_most_visit_pro->pro_disprice }}</span> </span> </div>

                            </div>

						

                            <div class="pro-action">

                              @if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty) 

							  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')  

                             <a  href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" ><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							  @endif

							  

							  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')

                              <a  href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" ><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							  @endif

							  

							  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')

                              <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							  @endif

							  @else

                <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else {{ trans($OUR_LANGUAGE.'.SOLD_OUT') }} @endif</span> </button></a>

                @endif



                            </div>

							

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                  

				@endforeach

			@else  

				<h5 class="no_record_fis" >@if (Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER')!= '') {{ trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER') }} @else {{ trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER') }} @endif <?php echo ($get_store_details_id->$stor_name);  ?> @if (Lang::has(Session::get('lang_file').'.PRODUCTS')!= '') {{ trans(Session::get('lang_file').'.PRODUCTS') }} @else {{ trans($OUR_LANGUAGE.'.PRODUCTS') }} @endif.</h5>

			@endif

                </div>

              </div>

            </div>

          </div>

        </div>

		

		<!--Deal-->

		 <!-- Best Sale -->

        <div class="col-sm-12 col-md-12 jtv-best-sale special-pro">

          <div class="jtv-best-sale-list">

            <div class="wpb_wrapper">

              <div class="best-title text-left">

                 <h2><?php echo ($get_store_details_id->$stor_name);  ?> - <span class="text_color">@if (Lang::has(Session::get('lang_file').'.DEALS')!= '') {{ trans(Session::get('lang_file').'.DEALS') }} @else {{ trans($OUR_LANGUAGE.'.DEALS') }} @endif</span></h2>

              </div>

            </div>

            <div class="slider-items-products">

              <div id="jtv-best-sale-slider" class="product-flexslider">

                <div class="jtv-best-sale-slider owl-carousel owl-theme">

				

				 @if(count($get_store_deal_by_id) > 0)  

				 @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

				 @php $deal_title = 'deal_title'; @endphp

				 @else @php  $deal_title = 'deal_title_'.Session::get('lang_code');  @endphp @endif



				 @foreach($get_store_deal_by_id as $store_deal_by_id)  

				  @if($store_deal_by_id->deal_no_of_purchase < $store_deal_by_id->deal_max_limit)

				 @php $dealdiscount_percentage = $store_deal_by_id->deal_discount_percentage;

				 $deal_img= explode('/**/', $store_deal_by_id->deal_image);

				 $mcat = strtolower(str_replace(' ','-',$store_deal_by_id->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$store_deal_by_id->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$store_deal_by_id->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$store_deal_by_id->ssb_name)); 

				 $res = base64_encode($store_deal_by_id->deal_id);

				 $product_img     = $deal_img[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/deals/".$product_img; @endphp

				 @if(file_exists($img_data) && $product_img !='')   

				 @php   $prod_path = url('').'/public/assets/deals/' .$product_img;  @endphp                 

				 @else  

				 @if(isset($DynamicNoImage['dealImg']))

				 @php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['dealImg']; @endphp

				 @if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path))

				 @php  $prod_path = $dyanamicNoImg_path; @endphp @endif

				 @endif

				 @endif   

				 @php   $alt_text   = substr($store_deal_by_id->$deal_title,0,25);

				 $alt_text  .= strlen($store_deal_by_id->$deal_title)>25?'..':''; @endphp

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

                      

                        <div class="pr-img-area"> 

						

						  @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

						  <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}" > 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                          </a> 

						  @endif

						  

						 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

						  <a  href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}">

                          <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}" > 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                          </a> 

						  @endif

						  

						  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

						  <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" >

                          <figure> <img class="first-img" src="{{ $prod_path }}" alt="{{ $alt_text }}" > 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                          </a> 

						   @endif

						   

						   

						</div>

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="" href="">{{ substr($store_deal_by_id->$deal_title,0,20) }}... </a> </div>

                          <div class="item-content">

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price">{{ Helper::cur_sym() }} {{ $store_deal_by_id->deal_discount_price }}</span> </span> </div>

                            </div>

                            <div class="pro-action">

							 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') 

                              <a  href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							 @endif

							 

							 @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

                              <a  href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							 @endif



							  @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') 

                              <a   href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res!!}"><button type="button" class="add-to-cart"><span>@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</span> </button></a>

							 @endif

							 

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

				   @else 

				  @endif

         @endforeach

	  @else  

         <h5 class="no_record_fis" >@if (Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER')!= '') {{ trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER') }} @else {{ trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER') }} @endif <?php echo ($get_store_details_id->$stor_name);  ?> @if (Lang::has(Session::get('lang_file').'.DEALS')!= '') {{ trans(Session::get('lang_file').'.DEALS') }} @else {{ trans($OUR_LANGUAGE.'.DEALS')}} @endif.</h5>

         @endif

                </div>

              </div>

            </div>

          </div>

        </div>

		

	<!--deal end-->

      </div>

    </div>

  </div>

  

  <!--end store products-->

  <!--store branched-->

   <section class="blog_post">

    <div class="container">

      <div class="row">

        <div class="col-xs-12 col-sm-12">

		

		<div class="single-box">

          <div class="best-title text-left">

            <h3>@if (Lang::has(Session::get('lang_file').'.BRANCHES')!= '') {{ trans(Session::get('lang_file').'.BRANCHES') }} @else {{ trans($OUR_LANGUAGE.'.BRANCHES') }} @endif</h3></div>

            <div class="slider-items-products">

              <div id="related-posts" class="product-flexslider hidden-buttons">

                <div class="owl-carousel owl-theme related-post-slider fadeInUp">

					 @php $store_count=count($get_storebranch);

                     $i=0; @endphp

                     @foreach($get_storebranch as $row_store)

                     @php  $i++; @endphp

                     @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                     @php $stor_name = 'stor_name';

                     $stor_address1 = 'stor_address1';

                     $stor_address2 = 'stor_address2'; @endphp

                     @else   

                     @php $stor_name = 'stor_name_'.Session::get('lang_code'); 

                     $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 

                     $stor_address2 = 'stor_address2_'.Session::get('lang_code');  @endphp

                     @endif

                     @php $store_img = $row_store->stor_img;

                     $store_name = $row_store->$stor_name; 

                     $product_image     = $row_store->stor_img;

                     $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                     $img_data   = "public/assets/storeimage/".$product_image; @endphp

                     @if(file_exists($img_data) && $product_image !='')  

                     @php  $prod_path = url('').'/public/assets/storeimage/' .$product_image;  @endphp                

                     @else  

                     @if(isset($DynamicNoImage['store']))

                     @php $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; @endphp

                     @if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))  

                     @php   $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp @endif

                     @endif

                     @endif 

                     @php  $alt_text   = $row_store->$stor_name; @endphp

				

                  <div class="product-item">

                    <article class="entry">

                      <div class="entry-thumb image-hover2">  <img src="{{ $prod_path }}" alt="{{ $alt_text }}">  </div>

                      <div class="entry-info">

                        <h3 class="entry-title"><a>{{ $store_name }}</a></h3>

                       

                        <div class="entry-more"> <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($row_store->stor_id)))); ?>">@if (Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '') {{  trans(Session::get('lang_file').'.VIEW_DETAILS') }} @else {{ trans($OUR_LANGUAGE.'.VIEW_DETAILS') }} @endif</a> </div>

						

                      </div>

                    </article>

                  </div>

                  @endforeach   

                </div>

              </div>

            </div>

          </div>

		  

		  <!---Leave a comment-->

		  @if(Session::has('customerid'))

		  <div class="single-box comment-box">

          <div class="best-title text-left">

            <h2>@if (Lang::has(Session::get('lang_file').'.WRITE_A_POST_COMMENTS')!= '') {{ trans(Session::get('lang_file').'.WRITE_A_POST_COMMENTS') }} @else {{ trans($OUR_LANGUAGE.'.WRITE_A_POST_COMMENTS') }} @endif</h2></div>

			 {!! Form::open(array('url'=>'storecomments','class'=>'form-horizontal loginFrm')) !!}

            <div class="coment-form">

             

              <div class="row">

                <input type="hidden" name="customer_id" value="{{ Session::get('customerid') }}">

                <input type="hidden" name="store_id" value="{!!$get_store_details_id->stor_id!!}">

				

                <div class="col-sm-12">

                  <label for="website">@if (Lang::has(Session::get('lang_file').'.ENTER_COMMENT_TITLE')!= '') {{ trans(Session::get('lang_file').'.ENTER_COMMENT_TITLE') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_COMMENT_TITLE') }} @endif</label>

                  <input id="title" name="title" type="text" class="form-control" value="{{ Input::old('title') }}" required>

                </div>

                <div class="col-sm-12">

                  <label for="message">@if (Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= '') {{ trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES') }} @else {{ trans($OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES') }} @endif</label>

                  <textarea name="comments" id="comments" rows="8" required class="form-control" value="{{ Input::old('comments') }}"></textarea>

                </div>

				 <div class="col-sm-12">

				  <fieldset>

                     <span class="star-cb-group">

                     <input type="radio" id="rating-5"  name="ratings" value="5"/><label for="rating-5"></label>

                     <input type="radio" id="rating-4" name="ratings" value="4"/><label for="rating-4"></label>

                     <input type="radio" id="rating-3"  name="ratings" value="3"/><label for="rating-3"></label>

                     <input type="radio" id="rating-2"  name="ratings" value="2"/><label for="rating-2"></label>

                     <input type="radio" id="rating-1"  name="ratings" value="1"/><label for="rating-1"></label>

                     </span>

                  </fieldset>

				 

				 </div>

              </div>

              <button type="submit" class="button"><span>@if (Lang::has(Session::get('lang_file').'.POST_COMMENTS')!= '') {{ trans(Session::get('lang_file').'.POST_COMMENTS') }} @else {{ trans($OUR_LANGUAGE.'.POST_COMMENTS') }} @endif</span></button>

            </div>

			</form>

          </div>

		  @else   

		<div class="special-product">

		  <a href="" class="link-all" value="@if (Lang::has(Session::get('lang_file').'.LOGIN')!= '') {{ trans(Session::get('lang_file').'.LOGIN') }} @else {{ trans($OUR_LANGUAGE.'.LOGIN') }} @endif" role="button" data-toggle="modal" data-target="#loginpop">@if (Lang::has(Session::get('lang_file').'.WRITE_A_REVIEW_POST')!= '') {{ trans(Session::get('lang_file').'.WRITE_A_REVIEW_POST') }} @else {{ trans($OUR_LANGUAGE.'.WRITE_A_REVIEW_POST') }} @endif</a></div>

        @endif

		   <!---Leave a comment end--->

		   <!-- Comment -->

		  <br>

          <div class="single-box ">

          <div class="best-title text-left">

            <h2>@if (Lang::has(Session::get('lang_file').'.REVIEWS')!= '') {{  trans(Session::get('lang_file').'.REVIEWS') }} @else {{ trans($OUR_LANGUAGE.'.REVIEWS') }} @endif</h2></div>

            <div class="comment-list str-review">

			  @if(count($review_details)!=0)

              <ul>

		     @foreach($review_details as $col)

			  @php  $customer_name = $col->cus_name;

			  $customer_mail = $col->cus_email;

			  $customer_img = $col->cus_pic;

			  $customer_comments = $col->comments;

			  $customer_date = $col->review_date;

			  $customer_product = $col->store_id;

			  $change_format = date('d/m/Y', strtotime($col->review_date) );

			  $review_date = date('F j, Y', strtotime($col->review_date) );

			  $customer_title = $col->title;

			  $customer_name_arr = str_split($customer_name);

			  $start_letter = strtolower($customer_name_arr[0]);

			  $customer_ratings = $col->ratings;

			  $date_exp=explode('/',$change_format); @endphp

                <li>

                  <div class="avartar"> 

				  

			@if($start_letter =='a')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='b')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='c')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='d')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='e')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='f')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='g')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='h')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='i')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='j')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='k')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='m')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='n')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='o')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='p')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='q')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='r')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='s')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='t')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='u')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			  @elseif($start_letter=='v')

			  {!! "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			@elseif($start_letter=='w')

				 {!! "

			  <div class='userimg'>

			 <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			@elseif($start_letter=='x')

					{!! "

			 <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			@elseif($start_letter=='y')

					   {!! "

			<div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			 @elseif($start_letter=='z')

						  {!! "

			<div class='userimg'>

			 <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>" !!}

			@endif

			</div>		

				

				  </div>

                  <div class="comment-body">

                    <div class="comment-meta"> <span class="author"><p>{{ $customer_name }}</p></span> <span class="date">{{ $review_date }}</span> </div>

					   @if($customer_ratings=='1')

                        <img src='{{ url('./images/stars-1.png') }}' style='margin-bottom:10px;'>@if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif

                        @elseif($customer_ratings=='2')

                        <img src='{{ url('./images/stars-2.png') }}' style='margin-bottom:10px;'>@if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif

                        @elseif($customer_ratings=='3')

                        <img src='{{ url('./images/stars-3.png') }}' style='margin-bottom:10px;'>@if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{ trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif

                        @elseif($customer_ratings=='4')

                        <img src='{{ url('./images/stars-4.png') }}' style='margin-bottom:10px;'>@if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{  trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif

                        @elseif($customer_ratings=='5')

                        <img src='{{ url('./images/stars-5.png') }}' style='margin-bottom:10px;'>@if (Lang::has(Session::get('lang_file').'.RATINGS')!= '') {{ trans(Session::get('lang_file').'.RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.RATINGS') }} @endif

                        @endif

                    <div class="comment-title">{{ $customer_title }} </div>

                    <div  class="comment">{{ $customer_comments }} </div>

                  </div>

                </li>

				@endforeach

              </ul>

			  @else @if (Lang::has(Session::get('lang_file').'.NO_REVIEW_RATINGS')!= '') {{ trans(Session::get('lang_file').'.NO_REVIEW_RATINGS') }} @else {{ trans($OUR_LANGUAGE.'.NO_REVIEW_RATINGS') }} @endif.<br>

            @endif

            </div>

          </div>

		  <!--end comment-->

		  

		</div>

       </div>

     </div>

  

  <!--end store branches-->

  

 

  

   <!-- service section -->

   @include('service_section')

  

  

   @else 

      <h4 style="color:#f00;">

         <center>Seems the store you are looking is not active, please contact us for more details.</center>

      </h4>

   @endif

  {!! $footer !!}

  

	<?php 

	 if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

	 $map_lang = 'en';

	 }else {  

	 $map_lang = 'fr';

	 }

	 ?>

  <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>&language=<?php echo $map_lang; ?>'></script>

      <script src="<?php echo url(''); ?>/public/assets/js/locationpicker.jquery.js"></script>

      <script>

         $('#somecomponent').locationpicker({

         location: {latitude: <?php echo  $get_store_details_id->stor_latitude;?>, longitude: <?php echo  $get_store_details_id->stor_longitude;?>},

         radius: 300,

         zoom: 12,

         });

  </script>

 <script type="text/javascript">

         var logID = 'log',

          log = $('<div id="'+logID+'"></div>');

         $('body').append(log);

          $('[type*="radio"]').change(function () {

            var me = $(this);

            log.html(me.attr('value'));

          });

 </script>


 <script>
   $('.jtv-best-sale-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: false,
                dots: false,
               <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>                 
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 3
                  },
                  992: {
                    items: 4
                  },
                  1200: {
                    items: 4
                  }
                }
     })

   $('.related-post-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: false,
                dots: false,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>                 
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 2
                  },
                  992: {
                    items: 3
                  },
                  1200: {
                    items: 3
                  }
                }
     })
   
 </script>