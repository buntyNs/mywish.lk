<!DOCTYPE html>

<html lang="en">


{!! $navbar !!}

{!! $header !!}



<body class="wishlist_page">



<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

 

  <!-- Main Container -->

  <section class="main-container col2-right-layout">

    <div class="main container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12">

          <div class="my-account">

            <div class="page-title">

              <h2>@if (Lang::has(Session::get('lang_file').'.MY_WISHLIST')!= '') {{  trans(Session::get('lang_file').'.MY_WISHLIST')}}  @else {{ trans($OUR_LANGUAGE.'.MY_WISHLIST')}} @endif</h2>

            </div>

            <div class="wishlist-item table-responsive">

              <table class="col-md-12">

                <thead>

                  <tr>

                    <th class="th-delate">@if (Lang::has(Session::get('lang_file').'.REMOVE')!= '') {{ trans(Session::get('lang_file').'.REMOVE') }}  @else {{ trans($OUR_LANGUAGE.'.REMOVE') }} @endif

</th>

                    <th class="th-product">@if (Lang::has(Session::get('lang_file').'.PRODUCT_IMAGE')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_IMAGE')}}  @else {{ trans($OUR_LANGUAGE.'.PRODUCT_IMAGE')}} @endif</th>

                    <th class="th-details">@if (Lang::has(Session::get('lang_file').'.PRODUCT_NAMES')!= '') {{  trans(Session::get('lang_file').'.PRODUCT_NAMES')}}  @else {{ trans($OUR_LANGUAGE.'.PRODUCT_NAMES')}} @endif</th>

                    <th class="th-price"> @if (Lang::has(Session::get('lang_file').'.UNIT_PRICE')!= '') {{  trans(Session::get('lang_file').'.UNIT_PRICE')}}  @else {{ trans($OUR_LANGUAGE.'.UNIT_PRICE')}} @endif</th>

                    <th class="th-total th-add-to-cart">@if (Lang::has(Session::get('lang_file').'.ACTION')!= '') {{  trans(Session::get('lang_file').'.ACTION')}}  @else {{ trans($OUR_LANGUAGE.'.ACTION')}} @endif</th>

                    <th class="th-total th-add-to-cart">@if (Lang::has(Session::get('lang_file').'.STOCK')!= '') {{  trans(Session::get('lang_file').'.STOCK')}}  @else {{ trans($OUR_LANGUAGE.'.STOCK')}} @endif</th>

                  </tr>

                </thead>

                <tbody>

                   @if(count($wishlistdetails)<1) 

                   <tr>

                     <td colspan="6" class="text-center">@if (Lang::has(Session::get('lang_file').'.NO_WHISLIST')!= '') {{  trans(Session::get('lang_file').'.NO_WHISLIST')}}  @else {{ trans($OUR_LANGUAGE.'.NO_WHISLIST')}} @endif   </td>

                        </tr>

                        @endif

                     @php $i=1;   @endphp

                        @if(count($wishlistdetails)!=0) 

                        @foreach($wishlistdetails as $orderdet)

                        @php  $product_img= explode('/**/',trim($orderdet->pro_Img,"/**/"));

                        $mcat = strtolower(str_replace(' ','-',$orderdet->mc_name));

                        $smcat = strtolower(str_replace(' ','-',$orderdet->smc_name));

                        $sbcat = strtolower(str_replace(' ','-',$orderdet->sb_name));

                        $ssbcat = strtolower(str_replace(' ','-',$orderdet->ssb_name)); 

                        $res = base64_encode($orderdet->pro_id); @endphp

                  <tr>

                    <td class="th-delate"><a href="{!! url('remove_wish_product').'/'.$orderdet->ws_id!!}"><i class="fa fa-trash"></i></a></td>

                    <td class="th-product">

                      @php  $product_image  = $product_img[0];

                              $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                              $img_data   = "public/assets/product/".$product_image; @endphp

                              @if($product_img !='')

                              @if(file_exists($img_data) && $product_image !='')  {{-- check product image is not null and exists in folder --}}

                              @php 

                              $prod_path = url('').'/public/assets/product/'.$product_image; @endphp          

                              @else 

                              @if(isset($DynamicNoImage['productImg']))  {{-- check no image for product is not null and exists in folder --}}

                              @php

                              $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

                              @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

                              @php 

                              $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp

                              @endif  

                              @endif

                              @endif  

                              @else 

                              @if(isset($DynamicNoImage['productImg']))  {{-- check no image for product is not null and exists in folder --}}

                              @php

                              $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; @endphp

                              @if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))

                              @php 

                              $prod_path = url('').'/'.$dyanamicNoImg_path; @endphp

                              @endif  

                              @endif

                              @endif    

                              <img src="{!! $prod_path !!}" alt="cart"></td>

                    <td class="th-details">

                      @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en')  

                              @php  $pro_title = 'pro_title'; @endphp

                              @else @php  $pro_title = 'pro_title_'.Session::get('lang_code'); @endphp @endif

                               {{ $orderdet->$pro_title }}</td>

                    <td class="th-price">{{ Helper::cur_sym() }} {{  $orderdet->pro_disprice }}</td>

                    <th class="td-add-to-cart">

                      @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')

                      <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res) }}"> @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

                      @endif

                              @if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') 

                              <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res) }}">

                                @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

                                @endif

                              @if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')

                              <a href="{{ url('productview/'.$mcat.'/'.$smcat.'/'.$res) }}">@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

                              @endif

                              @if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == '')  

                              <a href="{{ url('productview/'.$mcat.'/'.$res) }}">

                                @if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else {{ trans($OUR_LANGUAGE.'.ADD_TO_CART') }} @endif</a>

                              @endif

                    </th>

                    <th class="th-details"> @if($orderdet->pro_status =='1')   

                      @if (Lang::has(Session::get('lang_file').'.AVAILABLE')!= '') {{  trans(Session::get('lang_file').'.AVAILABLE') }} @else {{ trans($OUR_LANGUAGE.'.AVAILABLE') }} @endif    

                       @else  

                        @if (Lang::has(Session::get('lang_file').'.NOT_AVAILABLE')!= '') {{  trans(Session::get('lang_file').'.NOT_AVAILABLE') }} @else {{ trans($OUR_LANGUAGE.'.NOT_AVAILABLE') }} @endif  

                         @endif</th>

                  </tr>

                 

                 @endforeach

                 @endif

                 

                </tbody>

              </table>

               </div>

               <div class="pagination-area">

                {!! $wishlistdetails->render() !!}

               </div>

          </div>

        </div>



         @include('dashboard_sidebar')

      </div>

    </div>

  </section>

   <!-- service section -->

   @include('service_section')

  

  <!-- Footer -->

  {!! $footer !!}

  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<!-- JS --> 





 

</body>

</html>