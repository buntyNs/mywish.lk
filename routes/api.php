<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


 Route::post('test', 'MobileController@test');   
 Route::post('login_user', 'MobileController@login_user');



Route::group(['middleware' => ['chk_mob_lang']] ,function(){
      Route::post('user/register', 'MobileController@register');
	  Route::post('user_registration', 'MobileController@user_registration');
	 
});
