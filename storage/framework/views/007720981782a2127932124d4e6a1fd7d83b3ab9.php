﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |    <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_REVIEWS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_REVIEWS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_REVIEWS')); ?> <?php endif; ?>                  </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
     <?php if(count($favi)>0): ?>  
     <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php endif; ?>	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

      
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                                <li class="active"><a > <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_REVIEWS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_REVIEWS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_REVIEWS')); ?> <?php endif; ?>                  </a></li>
                            </ul>
                    </div>
                </div>

            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_REVIEWS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_REVIEWS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_REVIEWS')); ?> <?php endif; ?>                   </h5>
            
        </header>
 <?php if(Session::has('block_message')): ?>
		<div class="alert alert-success alert-dismissable"><?php echo Session::get('block_message'); ?>

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		<?php endif; ?>
        <div id="div-1" class="accordion-body collapse in body">
     

            <div class="table-responsive panel_marg_clr ppd">
           <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SNO')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> <?php endif; ?></th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REVIEW_TITLE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_TITLE')); ?> <?php endif; ?></th>
										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STORE_NAME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_NAME')); ?> <?php endif; ?></th>
										
										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_NAME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_NAME')); ?> <?php endif; ?></th>
										
									 
										<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ACTIONS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ACTIONS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIONS')); ?> <?php endif; ?></th>
										
										<!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 74px;" aria-label="Preview: activate to sort column ascending">Preview</th> -->
										
                                    </thead>
                                    <tbody>
                <?php $i = 1 ; ?>
                <?php if(count($get_review)>0): ?>
                   
				<?php $__currentLoopData = $get_review; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   
							

									  
                                    <tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
                                            <td class="  "><?php echo e($row->title); ?></td>
                                            <td class="  "><?php echo e(substr($row->stor_name,0,45)); ?></td>
                                            <td class="center  "><?php echo e($row->cus_email); ?></td>
                                            
                                            
                                          
                                            
                                             <td class="center  "><a href="#">
                                           <a href="<?php echo e(url('edit_store_review/'.$row->comment_id)); ?>" data-tooltip="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT')); ?> <?php endif; ?>"> <i class="icon icon-edit" style="margin-left:15px;"></i></a>
                                          <?php if($row->status == 0): ?>
                                            <a href="<?php echo e(url('block_store_review/'.$row->comment_id.'/1')); ?>" data-tooltip="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_BLOCK')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK')); ?> <?php endif; ?>"> <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>
                                            <?php elseif($row->status == 1): ?> 
                                             <a href="<?php echo e(url('block_store_review/'.$row->comment_id.'/0')); ?>" data-tooltip="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_UNBLOCK')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_UNBLOCK')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_UNBLOCK')); ?> <?php endif; ?>"> <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>
                                             <?php endif; ?>
                                             <a href="<?php echo e(url('delete_store_review')."/".$row->comment_id); ?>" data-tooltip="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELETE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELETE')); ?> <?php endif; ?>"><i class="icon icon-trash icon-1x" style="margin-left:14px;"></i></a> 
                                            
                                            </td>
                             
                                        </tr>
								<?php	 $i++; ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
								
				 </tbody>
                                </table></div>


        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      <?php echo $adminfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->   
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>
	  
	  <script type="text/javascript">
	   $.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	   });
	</script>
</body>
     <!-- END BODY -->
</html>
