 <div id="footer">
        <p>&copy; <?php echo e($SITENAME); ?>  - <?php echo e(date('Y')); ?>&nbsp;</p>
    </div>
 <script type="text/javascript">
var __lc = {};
__lc.license = 4302571;

(function() {
 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>

<script type="text/javascript">
  
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>