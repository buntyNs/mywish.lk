﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_SOLD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_SOLD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_SOLD_PRODUCTS')); ?>?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
   <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
 <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


 <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                                <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SOLD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SOLD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_SOLD_PRODUCTS')); ?></a></li>
                            </ul>
                    </div>
                </div>
			 <center><div class="cal-search-filter">
			 	<?php echo e(Form::open(['action' => 'MerchantproductController@sold_product', 'method' => 'POST'])); ?>

		 
							<input type="hidden" name="_token"  value="<?php echo e(csrf_token()); ?>">
							 <div class="row">
							 <br>
							 
							 
							   <div class="col-sm-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FROM_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FROM_DATE') : trans($MER_OUR_LANGUAGE.'.MER_FROM_DATE')); ?></div>
							<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
                            <?php echo e(Form::text('from_date',$from_date,array('id'=>'datepicker-8','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly'))); ?>

							 
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">
                                <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TO_DATE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_TO_DATE')  : trans($MER_OUR_LANGUAGE.'.MER_TO_DATE')); ?></div>
							 <div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
							 <?php echo e(Form::text('to_date',$to_date,array('id'=>'datepicker-9','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly'))); ?>

							 
							 
							  </div>
							  </div>
							   </div>
							   
							   <div class="form-group">
							   <div class="col-sm-2">
							 <input type="submit" name="submit" class="btn btn-block btn-success" value="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') ?   trans(Session::get('mer_lang_file').'.MER_SEARCH') : trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?>">
							 </div>
							</div>
							
							 </form></div>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SOLD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SOLD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_SOLD_PRODUCTS')); ?></h5>
            
        </header>
         <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
         	<?php echo e(Form::button('×',['class' => 'close closeAlert' , 'aria-hidden' => 'true'])); ?>

         </div>
        <div id="div-1" class="accordion-body collapse in body">
                  <div class="panel_marg_clr ppd">
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row"><th aria-label="S.No: activate to sort column ascending" style="width: 61px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending">S.No</th><th aria-label="Product Name: activate to sort column ascending" style="width: 69px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Product Name</th><th aria-label="City: activate to sort column ascending" style="width: 81px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">City</th><th aria-label="Store Name: activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Store Name</th><th aria-label="Original Price($): activate to sort column ascending" style="width: 75px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Original Price(<?php echo e(Helper::cur_sym()); ?>)</th><th aria-label="Discounted Price($): activate to sort column ascending" style="width: 90px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Discounted Price(<?php echo e(Helper::cur_sym()); ?>)</th><th aria-label=" Product Image : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"> Product Image </th>
                                      <?php /* <th aria-label="Preview: activate to sort column ascending" style="width: 76px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Actions</th>*/?>
                                        <th aria-label="Product details: activate to sort column ascending" style="width: 70px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Product details</th></tr>
                                    </thead>
                                    <tbody>
                   <?php $i = 1 ; ?>
				   
	<?php if(isset($_POST['submit'])): ?>
			
		<?php if(count($merchant_soldreports)>0): ?> 
	<?php $__currentLoopData = $merchant_soldreports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
						<?php if($product_list->pro_no_of_purchase	>=$product_list->pro_qty): ?>
						
								<?php	  $product_get_img = explode("/**/",$product_list->pro_Img);  ?>
									  <?php if($product_list->pro_status == 1): ?>
									   <?php 
										  $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
										  ?>
								  elseif($product_list->pro_status == 0) 
								  	<?php
										   $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
										   ?>
									  <?php endif; ?>
									  
                                    <tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
                                            <td class="center"><?php echo e(substr($product_list->pro_title,0,45)); ?></td>
                                            <td class="center"><?php echo e($product_list->ci_name); ?> </td>
                                            <td class="center  "><?php echo e($product_list->stor_name); ?></td>
                                            <td class="center  "><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_list->pro_price); ?></td>
                                            <td class="center  "><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_list->pro_disprice); ?></td>
                                            <td class="center  ">
											<?php  $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
											<?php if($product_get_img != ''): ?>  
									
			 								<?php	
									  $pro_img = $product_get_img[0]; ?>	
									  
									  $img_data = "public/assets/product/".$pro_img;
										  <?php if(file_exists($img_data) && $pro_img !=''): ?>  
														<?php 		
																			 $prod_path = url('').'/public/assets/product/'.$pro_img;
															?>	 
										  <?php else: ?> 
												 <?php if(isset($DynamicNoImage['productImg'])): ?>
												 	<?php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>	
													<?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
													 <?php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>	
													<?php endif; ?>
																		
												 <?php endif; ?>
																	 
																	 
												<?php endif; ?>
									   
						
						   <?php else: ?>
						   
							   
								 <?php if(isset($DynamicNoImage['productImg'])): ?>
												 	<?php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>	
													<?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
													 <?php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>	
													<?php endif; ?>
																		
												 <?php endif; ?>
																	 
																	 
												<?php endif; ?>		
											
											<a href="#"><img style="height:40px;" src="<?php echo $prod_path; ?>"></a></td>
                                           
                                            <?php /*
                                            <td class="center  "><a href="#">
                                           <a href="<?php echo url('mer_edit_product/'.$product_list->pro_id); ?>" > <i class="icon icon-edit" style="margin-left:15px;"></i></a>
                                         	<?php echo  $process; ?>
                                            </td>*/?>
                                           
                                     
                             <td class="center  "><a href="<?php echo e(url('mer_product_details')."/".$product_list->pro_id); ?>">View details</a></td>
                                        </tr>
			<?php $i++; ?> 			
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>

			<?php else: ?>
			 
				   <?php if(count($product_details)>0): ?> 
						<?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
						<?php if($product_list->pro_no_of_purchase	>=$product_list->pro_qty): ?>
									<?php
									  $product_get_img = explode("/**/",$product_list->pro_Img);  ?>
									  <?php if($product_list->pro_status == 1): ?>
											<?php
										  $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";  ?>
								  <?php elseif($product_list->pro_status == 0): ?> 
								  			<?php
										   $process = "<a href='".url('mer_block_product/'.$product_list->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
										   ?>
									  <?php endif; ?>
                                    <tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
                                            <td class="center"><?php echo e(substr($product_list->pro_title,0,45)); ?></td>
                                            <td class="center"><?php echo e($product_list->ci_name); ?></td>
                                            <td class="center  "><?php echo e($product_list->stor_name); ?></td>
                                            <td class="center  "><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_list->pro_price); ?></td>
                                            <td class="center  "><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_list->pro_disprice); ?></td>
                                            <td class="center  ">
										<?php 	$prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
											<?php if($product_get_img != ''): ?>  
							 
			 						<?php
								  $pro_img = $product_get_img[0];
								   
								  $img_data = "public/assets/product/".$pro_img;  ?>
									  <?php if(file_exists($img_data) && $pro_img !=''): ?>   
															<?php
																		 $prod_path = url('').'/public/assets/product/'.$pro_img;
															 ?>
									  <?php else: ?>  
											 <?php if(isset($DynamicNoImage['productImg'])): ?>
												 	<?php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>	
													<?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
													 <?php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>	
													<?php endif; ?>
																		
												 <?php endif; ?>
																	 
																	 
												<?php endif; ?>
									   
						
						   <?php else: ?>
						   
							   
								 <?php if(isset($DynamicNoImage['productImg'])): ?>
												 	<?php					
													$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>	
													<?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
													 <?php	
														$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>	
													<?php endif; ?>
																		
												 <?php endif; ?>
																	 
																	 
												<?php endif; ?>
											
											<a href="#"><img style="height:40px;" src="<?php echo e($prod_path); ?>"></a></td>
                                           <?php /*
                                            <td class="center  "><a href="#">
                                           <a href="<?php echo url('mer_edit_product/'.$product_list->pro_id); ?>" > <i class="icon icon-edit" style="margin-left:15px;"></i></a>
                                         	<?php echo  $process; ?>
                                            </td>*/?>
                                           
                                     
                             <td class="center  "><a href="<?php echo e(url('mer_product_details')."/".$product_list->pro_id); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW') : trans($MER_OUR_LANGUAGE.'.MER_VIEW')); ?></a>&nbsp;|&nbsp;<a href="<?php echo e(url('mer_edit_product')."/".$product_list->pro_id); ?>">Edit</a></td>
                                        </tr>
			<?php $i++;?>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>
			<?php endif; ?>
</tbody>
                                </table></div>

                    
        </div>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; <?php if (Lang::has(Session::get('mer_lang_file').'.MER_DIP_MULTIVENDOR_2014')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DIP_MULTIVENDOR_2014');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DIP_MULTIVENDOR_2014');} ?>&nbsp;</p>
    </div>
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
     <!-- GLOBAL SCRIPTS -->
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->   
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')); ?>",
               nextText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS')); ?>",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')); ?>",
               nextText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS')); ?>",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
   /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/
$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
</script>
</body>
     <!-- END BODY -->
</html>
