<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> | <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_COUPON')!= '')  ?   trans(Session::get('admin_lang_file').'.BACK_MANAGE_COUPON') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_COUPON')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
  <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link href="<?php echo url(''); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	 <?php  
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/ <?php echo e($fav->imgs_name); ?>">
<?php endif; ?>  
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        
       <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        <?php echo $adminleftmenus; ?>

       
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HOME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?></a></li>
                                <li class="active"><a><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_COUPON')!= '') ? trans(Session::get('admin_lang_file').'.BACK_MANAGE_COUPON')   : trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_COUPON')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_COUPON')!= '') ? trans(Session::get('admin_lang_file').'.BACK_MANAGE_COUPON')   : trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_COUPON')); ?></h5>
            
        </header>
   
   <?php if(Session::has('success')): ?>
		<div class="alert alert-success alert-dismissable">
	<?php echo e(Form::button('×',['class' => 'close','data-dismiss' =>'alert', 'aria-hidden' => 'true'])); ?>

	<?php echo Session::get('success'); ?></div>
		<?php endif; ?>
    <?php if(Session::has('delete_result')): ?>
    <div class="alert alert-success alert-dismissable"><?php echo Session::get('delete_result'); ?>

	<?php echo e(Form::button('×',['class' => 'close','data-dismiss' =>'alert', 'aria-hidden' => 'true'])); ?>

         </div>
    <?php endif; ?>
    <?php if(Session::has('cannot_delete_result')): ?>
    <div class="alert alert-warning alert-dismissable"><?php echo Session::get('cannot_delete_result'); ?></div>
<?php echo e(Form::button('×',['class' => 'close','data-dismiss' =>'alert', 'aria-hidden' => 'true'])); ?>

        
    <?php endif; ?>

            <div id="div-1" class="accordion-body collapse in body">
       
               <div class="table-responsive panel_marg_clr ppd">
       <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
										<th aria-label="S.No: activate to sort column ascending" style="width: 61px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="descending"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?></th>

										<th aria-label="Product Name: activate to sort column ascending" style="width: 69px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_COUPON_CODE')!= '') ? trans(Session::get('admin_lang_file').'.BACK_COUPON_CODE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COUPON_CODE')); ?></th>

										<th aria-label="City: activate to sort column ascending" style="width: 81px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_COUPON_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_COUPON_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_COUPON_NAME')); ?></th>

										<th aria-label="Store Name: activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_START_DATE')!= '') ? trans(Session::get('admin_lang_file').'.BACK_START_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_START_DATE')); ?></th>

										<th aria-label="Original Price($): activate to sort column ascending" style="width: 75px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_END_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_END_DATE'): trans($ADMIN_OUR_LANGUAGE.'.BACK_END_DATE')); ?></th>

										<th aria-label="Actions: activate to sort column ascending" style="width: 73px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_TYPE_OF_COUPON')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TYPE_OF_COUPON') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TYPE_OF_COUPON')); ?> </th>

										<th aria-label=" Product Image : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= '') ? trans(Session::get('admin_lang_file').'.BACK_EDIT')   : trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT')); ?></th>
										
										<th aria-label="Actions: activate to sort column ascending" style="width: 73px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DELETE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DELETE')   :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DELETE')); ?></th>
										
										
										</tr>
                                    </thead>
                                    <tbody>


				<?php $i = 1 ; ?>
			 <?php if(count($coupon_data)): ?> 
		      <?php $__currentLoopData = $coupon_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



 <tr class="gradeA odd">
            	<td class="sorting_1"><?php echo e($i); ?></td>
               	<td class="  "><?php echo e($row->coupon_code); ?></td>
    			<td class="  "><?php echo e($row->coupon_name); ?></td>
         		<td class="center  "><?php echo e($row->start_date); ?></td>
              	<td class="center  "><?php echo e($row->end_date); ?></td>
				<td class="text-center"><?php if($row->type_of_coupon == 1): ?>
                    <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_COUPON')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PRODUCT_COUPON')  : trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_COUPON')); ?>  
                  <?php elseif($row->type_of_coupon == 2): ?>
					  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_COUPON')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_COUPON') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_COUPON')); ?>  
                 <?php elseif($row->type_of_coupon == 3): ?>
				  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_COUPON')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_COUPON') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_COUPON')); ?>  
				  <?php endif; ?></td>	   
              <td class="text-center"><a href="<?php echo url('edit_coupon').'/'.$row->id; ?>" data-tooltip="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EDIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT')); ?>"><i class="icon icon-edit icon-2x"></i></a></td>
				 
				  
          <?php 
               $start_date = $row->start_date;
               $new_start_date = date("Y-m-d", strtotime($start_date));
               $end_date = $row->end_date;
               $new_end_date = date("Y-m-d", strtotime($end_date));
          $current_date = date('Y-m-d'); ?>
          <?php if($new_start_date <= $current_date && $new_end_date > $current_date): ?>  
          <td class="text-center">  <a href="<?php echo e(url('cannot_delete_coupon')); ?>" data-tooltip="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DELETE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DELETE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DELETE')); ?>"> <i class="icon icon-trash icon-2x" style="margin-left:15px;"></i></a></td>
          <?php else: ?> 
					<td class="text-center">  <a href="<?php echo e(url('delete_coupon/'.$row->id)); ?>" data-tooltip="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DELETE')!= '')  ?   trans(Session::get('admin_lang_file').'.BACK_DELETE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DELETE')); ?>"> <i class="icon icon-trash icon-2x" style="margin-left:15px;"></i></a></td>
          <?php endif; ?>

                                           
                                           
                                        </tr>
										
						<?php $i++; 	?>		 
			 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			 <?php endif; ?>
			
		
										
										</tbody>
                                </table></div>


        </div>
        </div>

    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <?php echo $adminfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url(''); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url(''); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
     <script src="<?php echo url(''); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable({
        "order": [[ 0, "desc" ]]
    });
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->   
      <script type="text/javascript">
       $.ajaxSetup({
           headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
       });
    </script>
</body>
     <!-- END BODY -->
</html>
