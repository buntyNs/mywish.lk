<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> | <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LOGO_SETTINGS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_LOGO_SETTINGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_LOGO_SETTINGS')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
      <?php  
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>		
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
      <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HOME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?></li>
                                <li class="active"><a ><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LOGO_SETTINGS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_LOGO_SETTINGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_LOGO_SETTINGS')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LOGO_SETTINGS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_LOGO_SETTINGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_LOGO_SETTINGS')); ?></h5>
            
        </header>
       		<?php if($errors->any()): ?>
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

		<?php echo e(Form::button('x',['class' => 'close' , 'data-dismiss' => 'alert' , 'aria-hidden' => 'true'])); ?>

         
        </div>
		</ul>	
		<?php endif; ?>
        <div id="div-1" class="accordion-body collapse in body">
             
             <?php if(Session::has('message')): ?>
		<p style="background-color:green;color:#fff;"><?php echo Session::get('message'); ?></p>
		<?php endif; ?>
        
 <?php echo Form::open(array('url'=>'add_logo_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>

          
         <?php $__currentLoopData = $logodetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $logo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php 
				$logo_img = $logo->imgs_name;
				$imgpath = "public/assets/logo/".$logo->imgs_name;
				$prod_path 	= url('').'/public/assets/default_image/No_image_logo.png';
				?>
				<?php if(file_exists($imgpath) && $logo_img !=''): ?>
				<?php 
					$prod_path = url('').'/public/assets/logo/'.$logo_img;
				?>
				<?php endif; ?>
				
				
		

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-3"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CURRENT_LOGO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CURRENT_LOGO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CURRENT_LOGO')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <img alt="Image Not Found" src="<?php echo e($prod_path); ?>">
                    </div>
                </div>
                
                <div class="form-group">
					<label class="control-label col-lg-3"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_UPLOAD_LOGO_IMAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_UPLOAD_LOGO_IMAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_UPLOAD_LOGO_IMAGE')); ?><span class="text-sub">*</span></label>
						<span class="errortext red logo-size" style="color:red"><em><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE_SIZE_MUST_BE')); ?> <?php echo e($LOGO_WIDTH); ?> x <?php echo e($LOGO_HEIGHT); ?> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PIXELS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_PIXELS'): trans($ADMIN_OUR_LANGUAGE.'.BACK_PIXELS')); ?></em></span>
                    <div class="col-lg-8">
					<?php echo e(Form::hidden('old_logo',$logo_img,['id' => 'old_logo'])); ?>

						<?php echo e(Form::file('logofile', array('id' => 'logofile'))); ?>

                     
                    </div>
                </div>
               

                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-4"><span  class="text-sub"></span></label>

                    <div class="col-lg-8">
                     <button type="submit" class="btn btn-success btn-sm btn-grad " style="color:#fff"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_UPDATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_UPDATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_UPDATE')); ?></button>
                     <button type="reset" class="btn btn-danger btn-sm btn-grad" style="color:#ffffff;"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
                   
                    </div>
					  
                </div>

                
				<?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <?php echo $adminfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
</body>
     <!-- END BODY -->
</html>
