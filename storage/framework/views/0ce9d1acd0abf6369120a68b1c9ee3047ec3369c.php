<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')); ?>  | <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE'): trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PROFILE')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="public/assets/css/main.css" />
    <link rel="stylesheet" href="public/assets/css/theme.css" />
	  <link rel="stylesheet" href="public/assets/css/plan.css" />
    <link rel="stylesheet" href="public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="public/assets/plugins/Font-Awesome/css/font-awesome.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         <?php echo $merchantheader; ?>

        <!-- END HEADER SECTION -->
      

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lan g_file').'.MER_HOME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_HOME'): trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                                <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')  : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PROFILE')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')  : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PROFILE')); ?></h5>
            
        </header>
        
        
        <div class="row">
        	<div class="col-lg-11 panel_marg">
                    
					<?php echo e(Form::open()); ?>

                    <?php $__currentLoopData = $merchant_setting_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merchant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                    <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')  : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PROFILE')); ?>

                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FIRST_NAME'): trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->mer_fname); ?>

                    </div>
                </div>
                        </div>
						  <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_LAST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LAST_NAME'): trans($MER_OUR_LANGUAGE.'.MER_LAST_NAME')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->mer_lname); ?>

                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PASSWORD'): trans($MER_OUR_LANGUAGE.'.MER_PASSWORD')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->mer_password); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EMAIL-ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL-ID'): trans($MER_OUR_LANGUAGE.'.MER_EMAIL-ID')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->mer_email); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PHONE_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PHONE_NUMBER'): trans($MER_OUR_LANGUAGE.'.MER_PHONE_NUMBER')); ?> <span class="text-sub">*</span></label>
                    <div class="col-lg-4"> 
					<?php echo e($merchant->mer_phone); ?>

                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS_ONE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS_ONE'):trans($MER_OUR_LANGUAGE.'.MER_ADDRESS_ONE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->mer_address1); ?>

                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS_TWO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS_TWO'): trans($MER_OUR_LANGUAGE.'.MER_ADDRESS_TWO')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
          			<?php echo e($merchant->mer_address2); ?>

                    </div>
                </div>
                        </div>
                         <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY'): trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->ci_name); ?>

                    </div>
                </div>
                        </div>
						
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_COUNTRY')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->co_name); ?>

                    </div>
                </div>
                        </div>
                       
                    
                    <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PAYMENT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_ACCOUNT')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($merchant->mer_payment); ?>

                    </div>
                </div>
                        </div>
                      </div> 
                    </div>
					
					<div class="form-group" style="padding-bottom:10px;">
					<?php /*{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-10', 'for' => 'pass1'])) !!}*/?>
                    

                    <div class="col-lg-2">
                     <a style="color:#fff; margin-left: 20px;" class="btn btn-warning btn-sm btn-grad" href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK') : trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?></a>
                    </div>
					  
                </div>
                
                <?php echo e(Form::close()); ?>

                </div>
        <br>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
  <?php echo $merchantfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
