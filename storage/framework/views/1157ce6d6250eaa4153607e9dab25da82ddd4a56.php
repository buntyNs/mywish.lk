<!DOCTYPE html>

<html lang="en">


<?php echo $navbar; ?>


<?php echo $header; ?>




<body class="wishlist_page">



<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

 

  <!-- Main Container -->

  <section class="main-container col2-right-layout">

    <div class="main container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12">

          <div class="my-account">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.MY_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_WISHLIST')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_WISHLIST')); ?> <?php endif; ?></h2>

            </div>

            <div class="wishlist-item table-responsive">

              <table class="col-md-12">

                <thead>

                  <tr>

                    <th class="th-delate"><?php if(Lang::has(Session::get('lang_file').'.REMOVE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REMOVE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REMOVE')); ?> <?php endif; ?>

</th>

                    <th class="th-product"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCT_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_IMAGE')); ?> <?php endif; ?></th>

                    <th class="th-details"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_NAMES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCT_NAMES')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_NAMES')); ?> <?php endif; ?></th>

                    <th class="th-price"> <?php if(Lang::has(Session::get('lang_file').'.UNIT_PRICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UNIT_PRICE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UNIT_PRICE')); ?> <?php endif; ?></th>

                    <th class="th-total th-add-to-cart"><?php if(Lang::has(Session::get('lang_file').'.ACTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ACTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ACTION')); ?> <?php endif; ?></th>

                    <th class="th-total th-add-to-cart"><?php if(Lang::has(Session::get('lang_file').'.STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STOCK')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STOCK')); ?> <?php endif; ?></th>

                  </tr>

                </thead>

                <tbody>

                   <?php if(count($wishlistdetails)<1): ?> 

                   <tr>

                     <td colspan="6" class="text-center"><?php if(Lang::has(Session::get('lang_file').'.NO_WHISLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_WHISLIST')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_WHISLIST')); ?> <?php endif; ?>   </td>

                        </tr>

                        <?php endif; ?>

                     <?php $i=1;   ?>

                        <?php if(count($wishlistdetails)!=0): ?> 

                        <?php $__currentLoopData = $wishlistdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderdet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php  $product_img= explode('/**/',trim($orderdet->pro_Img,"/**/"));

                        $mcat = strtolower(str_replace(' ','-',$orderdet->mc_name));

                        $smcat = strtolower(str_replace(' ','-',$orderdet->smc_name));

                        $sbcat = strtolower(str_replace(' ','-',$orderdet->sb_name));

                        $ssbcat = strtolower(str_replace(' ','-',$orderdet->ssb_name)); 

                        $res = base64_encode($orderdet->pro_id); ?>

                  <tr>

                    <td class="th-delate"><a href="<?php echo url('remove_wish_product').'/'.$orderdet->ws_id; ?>"><i class="fa fa-trash"></i></a></td>

                    <td class="th-product">

                      <?php  $product_image  = $product_img[0];

                              $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                              $img_data   = "public/assets/product/".$product_image; ?>

                              <?php if($product_img !=''): ?>

                              <?php if(file_exists($img_data) && $product_image !=''): ?>  

                              <?php 

                              $prod_path = url('').'/public/assets/product/'.$product_image; ?>          

                              <?php else: ?> 

                              <?php if(isset($DynamicNoImage['productImg'])): ?>  

                              <?php

                              $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>

                              <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

                              <?php 

                              $prod_path = url('').'/'.$dyanamicNoImg_path; ?>

                              <?php endif; ?>  

                              <?php endif; ?>

                              <?php endif; ?>  

                              <?php else: ?> 

                              <?php if(isset($DynamicNoImage['productImg'])): ?>  

                              <?php

                              $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>

                              <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

                              <?php 

                              $prod_path = url('').'/'.$dyanamicNoImg_path; ?>

                              <?php endif; ?>  

                              <?php endif; ?>

                              <?php endif; ?>    

                              <img src="<?php echo $prod_path; ?>" alt="cart"></td>

                    <td class="th-details">

                      <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                              <?php  $pro_title = 'pro_title'; ?>

                              <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

                               <?php echo e($orderdet->$pro_title); ?></td>

                    <td class="th-price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($orderdet->pro_disprice); ?></td>

                    <th class="td-add-to-cart">

                      <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>

                      <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>"> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

                      <?php endif; ?>

                              <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

                              <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>">

                                <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

                                <?php endif; ?>

                              <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>

                              <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

                              <?php endif; ?>

                              <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>  

                              <a href="<?php echo e(url('productview/'.$mcat.'/'.$res)); ?>">

                                <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

                              <?php endif; ?>

                    </th>

                    <th class="th-details"> <?php if($orderdet->pro_status =='1'): ?>   

                      <?php if(Lang::has(Session::get('lang_file').'.AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AVAILABLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AVAILABLE')); ?> <?php endif; ?>    

                       <?php else: ?>  

                        <?php if(Lang::has(Session::get('lang_file').'.NOT_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_AVAILABLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_AVAILABLE')); ?> <?php endif; ?>  

                         <?php endif; ?></th>

                  </tr>

                 

                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                 <?php endif; ?>

                 

                </tbody>

              </table>

               </div>

               <div class="pagination-area">

                <?php echo $wishlistdetails->render(); ?>


               </div>

          </div>

        </div>



         <?php echo $__env->make('dashboard_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

    </div>

  </section>

   <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <!-- Footer -->

  <?php echo $footer; ?>


  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<!-- JS --> 





 

</body>

</html>