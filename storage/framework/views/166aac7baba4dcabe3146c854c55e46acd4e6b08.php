<?php echo $navbar; ?>

<!-- Navbar ================================================== -->
<?php echo $header; ?>


<!-- Header End====================================================================== -->
<?php $table_data = DB::table('nm_social_media')->first(); ?>
 <section class="main-container col1-layout">
    <div class="main container">
	 <div class="col-sm-6 col-md-6 col-xs-12 accnt-lgn-left"> 
      <div class="page-content">
        <div class="account-login">
          <div class="box-authentication">
		   
		   <?php if(Session::has('loginblock_message')): ?>
            <div class="alert alert-warning alert-dismissable"><?php echo Session::get('loginblock_message'); ?>

               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
           <?php endif; ?>
		   
		   <?php if(Session::has('loginfail_message')): ?>
            <div class="alert alert-warning alert-dismissable"><?php echo Session::get('loginfail_message'); ?>

               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
            <?php endif; ?>
			
			
            <h4><?php if(Lang::has(Session::get('lang_file').'.LOGIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOGIN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOGIN')); ?> <?php endif; ?></h4>
			
			 <?php echo Form::open(array('url'=>'user_login_submit','class'=>'form-horizontal loginFrm')); ?>

			 
				<p class="before-login-text"><?php if(Lang::has(Session::get('lang_file').'.WELCOME_SIGN_IN_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WELCOME_SIGN_IN_ACCOUNT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WELCOME_SIGN_IN_ACCOUNT')); ?> <?php endif; ?></p>
				
					<label for="emmail_login"><?php if(Lang::has(Session::get('lang_file').'.EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL_ADDRESS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL_ADDRESS')); ?> <?php endif; ?><span class="required">*</span></label>
					<!--	<input id="loginemail" required name="loginemail" type="email" class="form-control" placeholder="<?php if(Lang::has(Session::get('lang_file').'.EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL_ADDRESS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL_ADDRESS')); ?> <?php endif; ?>">-->
						<input id="loginemail" required name="loginemail" value="" type="email" class="form-control" placeholder="<?php if(Lang::has(Session::get('lang_file').'.EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL_ADDRESS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL_ADDRESS')); ?> <?php endif; ?>">
						
						
					
					<label for="password_login"><?php if(Lang::has(Session::get('lang_file').'.PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PASSWORD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PASSWORD')); ?> <?php endif; ?><span class="required">*</span></label>
					
						<!--<input id="loginpassword" required name="loginpassword" placeholder="<?php if(Lang::has(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MIMIMUM_6_CHARACTERS')); ?> <?php endif; ?>" type="password" class="form-control">
						-->
				<input id="loginpassword" required value="" name="loginpassword" placeholder="<?php if(Lang::has(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MIMIMUM_6_CHARACTERS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MIMIMUM_6_CHARACTERS')); ?> <?php endif; ?>" type="password" class="form-control">
						
					
					<p class="forgot-pass"><a href="<?php echo e(url('')); ?>/forget_password"><?php if(Lang::has(Session::get('lang_file').'.LOST_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOST_PASSWORD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOST_PASSWORD')); ?> <?php endif; ?> ?</a></p>
					
					<button id="login_submit" class="button"><i class="icon-lock icons"></i>&nbsp; <span><?php if(Lang::has(Session::get('lang_file').'.LOGIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOGIN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOGIN')); ?> <?php endif; ?></span></button>
           
					<input type="hidden" id="return_url" value="<?php echo url('');?>" />
			 
				   <!--<label class="inline" for="rememberme">
					  <input type="checkbox" value="forever" id="rememberme" name="rememberme">
					  Remember me </label>-->
			  
          </div>
          <div class="box-authentication">
				<!--<p><?php if(Lang::has(Session::get('lang_file').'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')); ?> <?php endif; ?> <?php echo $SITENAME; ?>.</p>

				<button class="button"><i class="icon-user icons"></i>&nbsp; <span> <a  onclick="fb_login()" class="facebook_login" style="margin-top:5px; margin-bottom:5px" >&nbsp;</a><br><br></span></button>
				<div class="register-benefits">
					<h5><?php if(Lang::has(Session::get('lang_file').'.DONT_HAVE_AN_ACCOUNT_YET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DONT_HAVE_AN_ACCOUNT_YET')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DONT_HAVE_AN_ACCOUNT_YET')); ?> <?php endif; ?> ? <a href="<?php echo url("registers");?>"style="color:#ff8400;"><?php if(Lang::has(Session::get('lang_file').'.SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIGN_UP')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIGN_UP')); ?> <?php endif; ?>
				</div>-->
          </div>		
  
        </div>
      </div>
    </div>
	
	
	

	 <div class="col-sm-5 col-md-5 col-xs-12 accnt-lgn-right1"> 
	 <div class="page-content">
        <div class="account-login">
		<div class="box-authentication" style="margin-top: 10%;"> 
				<p><?php if(Lang::has(Session::get('lang_file').'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP')); ?> <?php endif; ?> <?php echo $SITENAME; ?>.</p>

			 <a  onclick="fb_login()" class="facebook_login" >&nbsp;</a><br><br>
				<div class="register-benefits">
					<h5><?php if(Lang::has(Session::get('lang_file').'.DONT_HAVE_AN_ACCOUNT_YET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DONT_HAVE_AN_ACCOUNT_YET')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DONT_HAVE_AN_ACCOUNT_YET')); ?> <?php endif; ?> ? <a href="<?php echo url("registers");?>"style="color:#ff8400;"><?php if(Lang::has(Session::get('lang_file').'.SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIGN_UP')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIGN_UP')); ?> <?php endif; ?>
				</div>
          </div>
	
		</div>
	</div>
	</div>
	 
	</div>
	
	</div>
  </section>
  
  <!-- service section -->
   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $footer; ?>


<script>
  $( document ).ready(function() {
	  
	  var loginemail    =$('#loginemail');
      var loginpassword =$('#loginpassword');
	  var return_url = $('#return_url');
 
     $('#login_submit').click(function() {
      
  
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(loginemail.val() == "")
    { 
      loginemail.css('border', '1px solid red'); 
       
      loginemail.focus();
      return false;
    }
    else
    {
      loginemail.css('border', ''); 
       
    }

     if(!emailReg.test(loginemail.val()))
        {
          loginemail.css('border', '1px solid red'); 
           
          loginemail.focus();
          return false;
          
        }

    else
      {
      loginemail.css('border', ''); 
       

       }
    if(loginpassword.val() == "")
    {
      loginpassword.css('border', '1px solid red'); 
       
      loginpassword.focus();
      return false;
    }
    

    });




     $('#register_submit').click(function() {
 
    if(cname.val() == "")
    {
      cname.css('border', '1px solid red'); 
      cname.focus();
      return false;
    }
    else
    {
      cname.css('border', ''); 
       
    }
    
    if(cemail.val() == "")
    {
      cemail.css('border', '1px solid red'); 
      cemail.focus();
      return false;
    }
    
    else
    {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
       var cemail     = $('#inputregisterEmail');

         if(!emailReg.test(cemail.val()))
        {
          cemail.css('border', '1px solid red'); 
           
          cemail.focus();
          return false;
          
        }


        else
        {
          cpwd.css('border', ''); 
          var email=cemail.val();
          var passemail = 'email='+email;
             $.ajax( {
               type: 'get',
            data: passemail,
            url: 'register_emailcheck_ajax',
            success: function(responseText){  
            if(responseText)
            {    
            if(responseText==1)
            {
            $('#error_msg').html('<?php if (Lang::has(Session::get('lang_file').'.ALREADY_EMAIL_EXISTS')!= '') { echo  trans(Session::get('lang_file').'.ALREADY_EMAIL_EXISTS');}  else { echo trans($OUR_LANGUAGE.'.ALREADY_EMAIL_EXISTS');} ?>');
            cemail.css('border', '1px solid red'); 
            return false;
            }
            else
            {
            cemail.css('border', ''); 
            $('#error_msg').html('');
            
      

            }
                     
            }
        }   
      });   

        

        }
      
    }
    if(cpwd.val() == "")
    {
      //cpwd.css('border', '1px solid red'); 
      cpwd.focus();
      return false;
    }
    else
    {

        cpwd.css('border', ' '); 
       
      cpwd.focus();
       
    }
    if(selectcity.val() == 0)
    {
      selectcity.css('border', '1px solid red'); 
       
      selectcity.focus();
      return false;
    }
    else
    {
      selectcity.css('border', ''); 
      $('#error_msg').html('');
    }
    if(selectcountry.val() == 0)
    {
      selectcountry.css('border', '1px solid red'); 
       
      selectcountry.focus();
      return false;
    }
    else
    {
      selectcountry.css('border', ''); 
      $('#error_msg').html('');
    }
       
});


$('#reset_password').click(function() {

    var emailpwd = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if($('#passwordemail').val() == "")
    {
      $('#passwordemail').css('border', '1px solid red'); 
       
      $('#passwordemail').focus();
      return false;
    }
    else
    {
      $('#passwordemail').css('border', ''); 
      $('#ui').html('');
    }

     if(!emailpwd.test($('#passwordemail').val()))
      {
          $('#passwordemail').css('border', '1px solid red'); 
           
          $('#passwordemail').focus();
          return false;
          
      }

      else
          {


      $('#passwordemail').css('border',''); 
          var pwdemail=$('#passwordemail').val();
          var passpwdemail = 'pwdemail='+pwdemail;
           //alert(passpwdemail);
           $.ajax({
           type: 'get',
            data: passpwdemail,
            url: '<?php echo url('user_forgot_submit');?>',
            beforeSend: function() {
            //$("#uimsg").html("Please wait.....");
            document.getElementById("loader").style.display = "block";
        },

            success: function(responseText){
             document.getElementById("loader").style.display = "none";
            // $('#login2').modal("hide");
             //$('#login2').css("display","none");
           //$("#loadalert").css("display", "block");
         
           
             
            if(responseText=="success")
            { 
				$('#login2').modal("hide");
			  document.getElementById("loadalert").style.display = "block";
               setTimeout(function () {
               location.reload();  
            },500);
               //alert("Please check your mail for further instruction");
              //$('.close').click();
               

            $('#uimsg').html('<?php if (Lang::has(Session::get('lang_file').'.PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS')!= '') { echo  trans(Session::get('lang_file').'.PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS');}  else { echo trans($OUR_LANGUAGE.'.PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS');} ?>');
			$('#passwordemail').val('');

      //$('#login2').toggle('slow');
            }
            else if(responseText=="fail")
            {
            $('#uimsg').html('<?php if (Lang::has(Session::get('lang_file').'.EMAIL_ID_DOES_NOT_EXIST')!= '') { echo  trans(Session::get('lang_file').'.EMAIL_ID_DOES_NOT_EXIST');}  else { echo trans($OUR_LANGUAGE.'.EMAIL_ID_DOES_NOT_EXIST');} ?>');
			
			       $('#passwordemail').val('');
            }
			
            
          }

        });
         
      } 
});


 $('#reset_new_password').click(function() {
    var emailpwd = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if($('#passwordnew').val() == "")
    {
      $('#passwordnew').css('border', '1px solid red'); 
       
      $('#passwordnew').focus();
      return false;
    }
    else
    {
      $('#passwordnew').css('border', ''); 
      $('#passmsg').html('');
    }
    if($('#passwordconfirmnew').val() == "")
    {
      $('#passwordconfirmnew').css('border', '1px solid red'); 
       
      $('#passwordconfirmnew').focus();
      return false;
    }
    else if($('#passwordconfirmnew').val()!= $('#passwordnew').val())
    {
      $('#passwordconfirmnew').css('border', '1px solid red'); 
       
      $('#passwordconfirmnew').focus();
      return false;
    }
    else
    {
      $('#passwordconfirmnew').css('border',''); 
          var newpwd=$('#passwordnew').val();
          var userid = $('#passsworduserid').val();
          var passnewpwd = 'newpwd='+newpwd+'&userid='+userid;
          
           $.ajax({
           type: 'get',
            data: passnewpwd,
            url: '<?php echo url('user_reset_password_submit');?>',
            success: function(responseText){  
           
            if(responseText=="success")
            {

            $('#passmsg').html('<?php if (Lang::has(Session::get('lang_file').'.PASSWORD_CHANGED_SUCCESS')!= '') { echo  trans(Session::get('lang_file').'.PASSWORD_CHANGED_SUCCESS');}  else { echo trans($OUR_LANGUAGE.'.PASSWORD_CHANGED_SUCCESS');} ?>');
            $('.close').click();
            $('#login_a_click').click();

            }
            else if(responseText=="fail")
            {
            $('#passmsg').html('<?php if (Lang::has(Session::get('lang_file').'.INVALID_USER')!= '') { echo  trans(Session::get('lang_file').'.INVALID_USER');}  else { echo trans($OUR_LANGUAGE.'.INVALID_USER');} ?>');

            }
            
          }

        });
    }
    

      
         
       
});

});

$('#login_close').click(function() 
  { 
  $('#loginemail').val('');
  $('#loginpassword').val('');
});

$('.close').click(function()
{
  $('#passwordemail').val('');
});

function check_email_ajax(email)
{
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

     if(!emailReg.test(cemail.val()))
      {
      cemail.css('border', '1px solid red'); 
       
      cemail.focus();
      return false;
      }

    else
      {
      var passemail = 'email='+email;
       $.ajax( {
            type: 'get',
          data: passemail,
          url: 'register_emailcheck',
          success: function(responseText){  
           if(responseText)
           {    
          if(responseText==1)
          {
          $('#error_msg').html('<?php if (Lang::has(Session::get('lang_file').'.ALREADY_EMAIL_EXISTS')!= '') { echo  trans(Session::get('lang_file').'.ALREADY_EMAIL_EXISTS');}  else { echo trans($OUR_LANGUAGE.'.ALREADY_EMAIL_EXISTS');} ?>');

          }
          else
          {
          cemail.css('border', ''); 
          $('#error_msg').html('');

          }
                     
           }
        }   
      });   




      }
       
}
function reset()
{
 $('#uimsg').html(''); 
}



function get_city_list(id)
  {
 
     var passcityid = 'id='+id;
       $.ajax( {
            type: 'get',
          data: passcityid,
          url: 'register_getcountry',
          success: function(responseText){  
     
           if(responseText)
           {   
          $('#selectcity').html(responseText);             
           }
        }   
      });   
  }
</script>

<script src="//connect.facebook.net/en_US/sdk.js" type="text/javascript"></script>
<script type="text/javascript">
window.fbAsyncInit = function() {
    FB.init({
        appId   : '<?php echo $table_data->sm_fb_app_id;?>',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true, // parse XFBML
        version    : 'v2.8' // use graph api version 2.8
    });

  };

function fb_login()
{ 
  console.log( 'fb_login function initiated' );
	  FB.login(function(response) {

      console.log( 'login response' );
      console.log( response );
      console.log( 'Response Status' + response.status );
		//top.location.href=http://demo.Sundaroecommerce.com/;
      if (response.authResponse) {

        console.log( 'Auth success' );

    		FB.api("/me",'GET',{'fields':'id,email,verified,name'}, function(me){

      		if (me.id) {


            //console.log( 'Retrived user details from FB.api', me );

             var id = me.id; 
		var email = me.email;
            	var name = me.name;
                var live ='';
				if (me.hometown!= null)
				{			
					var live = me.hometown.name;
				}        
            	
    var passData = 'fid='+ id + '&email='+ email + '&name='+ name + '&live='+ live ;
 //alert(passData);
            //console.log('data', passData);
          
            $.ajax({
             type: 'GET',
            data: passData,
             //data: $.param(passData),
             global: false,
             url: '<?php echo url('facebooklogin'); ?>',
             success: function(responseText){ 
              console.log( responseText ); 
            
             location.reload();
             },
             error: function(xhr,status,error){
               console.log(status, status.responseText);
             }
           }); 

        }else{

          console.log('There was a problem with FB.api', me);

        }
      });

    }else{
      console.log( 'Auth Failed' );
    }

  }, {scope: 'email'});
}
function logout() {

        try {
        if (FB.getAccessToken() != null) {
            FB.logout(function(response) {
                // user is now logged out from facebook do your post request or just redirect
                window.location = "<?php echo url('facebook_logout'); ?>";
            });
        } else {
            // user is not logged in with facebook, maybe with something else
            window.location = "<?php echo url('facebook_logout'); ?>";
        }
    } catch (err) {
        // any errors just logout
        window.location = "<?php echo url('facebook_logout'); ?>";
    }
           /*  FB.logout(function(response) {
				    
                FB.getAuthResponse(null, 'unknown');
                //FB.Auth.getAuthResponse(null, 'unknown');
                 window.location = "<?php //echo url('facebook_logout'); ?>";
              //FB.logout();
               				console.log(response);

            }); */
}
</script>