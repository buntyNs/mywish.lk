<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9">
      <![endif]-->
      <!--[if !IE]><!--> 
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <meta charset="UTF-8" />
            <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_MERCHANT')); ?> <?php endif; ?></title>
            <meta content="width=device-width, initial-scale=1.0" name="viewport" />
            <meta content="" name="description" />
            <meta content="" name="author" />
            <meta name="_token" content="<?php echo csrf_token(); ?>"/>
            <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
            <!-- GLOBAL STYLES -->
            <!-- GLOBAL STYLES -->
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
            <?php 
            $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
            <?php if(count($favi)>0): ?>  
            <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
            <?php endif; ?>  
            <!--END GLOBAL STYLES -->
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->
         </head>
         <!-- END HEAD -->
         <!-- BEGIN BODY -->
         <body class="padTop53 " >
            <!-- MAIN WRAPPER -->
            <div id="wrap">
               <!-- HEADER SECTION -->
               <?php echo $adminheader; ?>

               <!-- END HEADER SECTION -->
               <!-- MENU SECTION -->
               <?php echo $adminleftmenus; ?>

               <!--END MENU SECTION -->
               <div></div>
               <!--PAGE CONTENT -->
               <div id="content">
                  <div class="inner">
                     <div class="row">
                        <div class="col-lg-12">
                           <ul class="breadcrumb">
                              <li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                              <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_MERCHANT')); ?> <?php endif; ?></a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="box dark">
                              <header>
                                 <div class="icons"><i class="icon-edit"></i></div>
                                 <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_MERCHANT')); ?> <?php endif; ?></h5>
                              </header>
                              <?php if($errors->any()): ?>
                              <br>
                              <ul style="color:red;">
                                 <div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                 </div>
                              </ul>
                              <?php endif; ?>
                              <?php if(Session::has('mail_exist')): ?>
                              <div class="alert alert-warning alert-dismissable"><?php echo Session::get('mail_exist'); ?>

                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              </div>
                              <?php endif; ?>
                              <div class="row">
                                 <div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                                    <?php $__currentLoopData = $merchant_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_mer_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo Form::open(array('url'=>'edit_merchant_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

                                    <div class="panel panel-default">
                                       <div class="panel-heading">
                                          <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MERCHANT_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_ACCOUNT')); ?> <?php endif; ?>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FIRST_NAME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FIRST_NAME')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::hidden('mer_id',$fetch_mer_details->mer_id)); ?>

                                                <!-- <input type="hidden" name="mer_id" value="<?php echo e($fetch_mer_details->mer_id); ?>"> -->
                                                <?php echo e(Form::text('first_name',$fetch_mer_details->mer_fname,array('id'=>'first_name','class'=>'form-control','maxlength'=>'50'))); ?>

                                                <!--   <input type="text" maxlength="50" class="form-control" placeholder="" id="first_name" name="first_name" value="<?php echo $fetch_mer_details->mer_fname; ?>" > -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST_NAME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_NAME')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('last_name',$fetch_mer_details->mer_lname,array('id'=>'last_name','class'=>'form-control','maxlength'=>'50'))); ?>

                                                <!--  <input type="text" maxlength="50" class="form-control" placeholder="" id="last_name"  name="last_name" value="<?php echo $fetch_mer_details->mer_lname; ?>" > -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EMAIL')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('email_id',$fetch_mer_details->mer_email,array('id'=>'email_id','class'=>'form-control','onchange'=>'email_exixts();'))); ?>

                                                <!-- <input type="text" class="form-control" placeholder="" onchange="email_exixts();"  id="email_id" name="email_id" value="<?php echo $fetch_mer_details->mer_email; ?>" > -->
                                                <div id="email_id_error_msg" style="color:#F00;font-weight:800"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PASSWORD')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PASSWORD')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('',$fetch_mer_details->mer_password,array('id'=>'password','class'=>'form-control','readonly'=>'readonly'))); ?>

                                                <!-- <input type="text" class="form-control" placeholder="" id="email_id" name="" value="<?php echo $fetch_mer_details->mer_password; ?>" readonly> -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <select class="form-control" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" required>
                                                   <option value="">-- <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT')); ?> <?php endif; ?> --</option>
                                                   <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                   <option value="<?php echo e($country_fetch->co_id); ?>"  <?php if($fetch_mer_details->mer_co_id == $country_fetch->co_id){ echo 'selected'; } ?>><?php echo $country_fetch->co_name; ?></option>
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT_CITY')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_CITY')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <select class="form-control" name="select_mer_city" id="select_mer_city" required>
                                                   <option value="">-- <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT')); ?> <?php endif; ?> --</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PHONE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('phone_no',$fetch_mer_details->mer_phone,array('id'=>'phone_no','class'=>'form-control'))); ?>

                                                <!--  <input type="text" class="form-control" placeholder="" maxlength="16" id="phone_no" name="phone_no" value="<?php echo $fetch_mer_details->mer_phone; ?>" > -->
                                                <div id="phone_no_error_msg" style="color:#F00;font-weight:800"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS1')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('addreess_one',$fetch_mer_details->mer_address1,array('id'=>'addreess_one','class'=>'form-control','maxlength'=>'149'))); ?>

                                                <!-- <input type="text" class="form-control" placeholder="" id="addreess_one" name="addreess_one" value="<?php echo $fetch_mer_details->mer_address1; ?>" maxlength="149"> -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS2')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('address_two',$fetch_mer_details->mer_address2,array('id'=>'address_two','class'=>'form-control','maxlength'=>'149'))); ?>

                                                <!-- <input type="text" class="form-control" placeholder="" id="address_two" name="address_two" value="<?php echo $fetch_mer_details->mer_address2; ?>" maxlength="149"> -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYMENT_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_ACCOUNT')); ?> <?php endif; ?><span class="text-sub"></span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('payment_account',$fetch_mer_details->mer_payment,array('id'=>'payment_account','class'=>'form-control'))); ?>

                                                <!--  <input type="text" class="form-control" placeholder="" id="payment_account" name="payment_account" value="<?php echo $fetch_mer_details->mer_payment; ?>" > -->
                                             </div>
                                          </div>
                                       </div>

                                        <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY_KEY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY_KEY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY_KEY')); ?> <?php endif; ?><span class="text-sub"></span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('payumoney_key',$fetch_mer_details->mer_payu_key,array('id'=>'payment_account','class'=>'form-control'))); ?>

                                                
                                             </div>
                                          </div>
                                       </div>

                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY_SALT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY_SALT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY_SALT')); ?> <?php endif; ?><span class="text-sub"></span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::text('payumoney_salt',$fetch_mer_details->mer_payu_salt,array('id'=>'payment_account','class'=>'form-control'))); ?>

                                                
                                             </div>
                                          </div>
                                       </div>

                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMMISSION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMMISSION')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMMISSION')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Form::number('commission',$fetch_mer_details->mer_commission,array('id'=>'commission','class'=>'form-control','placeholder'=>'Enter Admin Commission','min'=>'1','max'=>'99'))); ?>

                                                <!--  <input type="number" class="form-control" placeholder="" id="commission" name="commission" value="<?php echo $fetch_mer_details->mer_commission; ?>" min="1" max="99" > --><span>%</span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="control-label col-lg-3" for="pass1"><span class="text-sub"></span></label>
                                          <div class="col-lg-8">
                                             <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_UPDATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_UPDATE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_UPDATE')); ?> <?php endif; ?></a></button>
                                             <a href="<?php echo e(url('manage_merchant')); ?>" class="btn btn-default btn-sm btn-grad" style="color:#000"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCEL')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL')); ?> <?php endif; ?></a>
                                          </div>
                                       </div>
                                    </div>
                                    <?php echo e(Form::close()); ?>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--END PAGE CONTENT -->
            </div>
            <!--END MAIN WRAPPER -->
            <!-- FOOTER -->
            <?php echo $adminfooter; ?>

            <!--END FOOTER -->
            <!-- GLOBAL SCRIPTS -->
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
            <script>
               $('#last_name,#store_name,#first_name').bind('keyup blur',function(){ 
               var node = $(this);
               node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }
               );
               
               
               
               $( document ).ready(function() {
                   var phone_no           = $('#phone_no');
                   var store_pho           = $('#store_pho');
                   var zip_code           = $('#zip_code');
                   var commission           = $('#commission');
                   var first_name            = $('#first_name');
                   var last_name           = $('#last_name');
               
                    /*Merchant Phone number*/
               $('#phone_no').keypress(function (e){
                   if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                       phone_no.css('border', '1px solid red'); 
                       $('#phone_no_error_msg').html('Numbers Only Allowed');
                       phone_no.focus();
                       return false;
                   }else{          
                       phone_no.css('border', ''); 
                       $('#phone_no_error_msg').html('');          
                   }
               });
               /*Store Phone Number*/
               $('#store_pho').keypress(function (e){
                   if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                       store_pho.css('border', '1px solid red'); 
                       $('#store_pho_error_msg').html('Numbers Only Allowed');
                       store_pho.focus();
                       return false;
                   }else{          
                       store_pho.css('border', ''); 
                       $('#store_pho_error_msg').html('');         
                   }
               });
               
               /*Store Zipcode*/
               $('#zip_code').keypress(function (e){
                   if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                       zip_code.css('border', '1px solid red'); 
                       $('#zip_code_error_msg').html('Numbers Only Allowed');
                       zip_code.focus();
                       return false;
                   }else{          
                       zip_code.css('border', ''); 
                       $('#zip_code_error_msg').html('');          
                   }
               });
               
               /*Admin commission*/
               $('#commission').keypress(function (e){
                   if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                       commission.css('border', '1px solid red'); 
                       $('#commission_error_msg').html('Numbers Only Allowed');
                       commission.focus();
                       return false;
                   }else{          
                       commission.css('border', ''); 
                       $('#commission_error_msg').html('');            
                   }
               });
               
               
                
               
               $('#submit').click(function() {
               /*Merchant First Name*/
                   if($.trim(first_name.val()) == ""){
                       first_name.css('border', '1px solid red'); 
                       $('#first_name_error_msg').html('Please Enter First Name');
                       first_name.focus();
                       return false;
                   }else{
                       first_name.css('border', ''); 
                       $('#first_name_error_msg').html('');
                   }
               
                   /*Merchant Last Name*/
                   if($.trim(last_name.val()) == ""){
                       last_name.css('border', '1px solid red'); 
                       $('#last_name_error_msg').html('Please Enter Last Name');
                       last_name.focus();
                       return false;
                   }else{
                       last_name.css('border', ''); 
                       $('#last_name_error_msg').html('');
                   }
               
                   /*Merchant Email Id*/
                   //var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
                   if($.trim(email_id.val()) == ""){
                       email_id.css('border', '1px solid red'); 
                       $('#email_id_error_msg').html('Please Enter Email id');
                       email_id.focus();
                       return false;
                   }else{
                       email_id.css('border', ''); 
                       $('#email_id_error_msg').html('');
                   }
               
                    /*Country*/ 
                   if(select_mer_country.val() == 0){
                       select_mer_country.css('border', '1px solid red'); 
                       $('#country_error_msg').html('Please Select Merchant Country');
                       select_mer_country.focus();
                       return false;
                   }else{
                       select_mer_country.css('border', ''); 
                       $('#country_error_msg').html('');
                   }
               
                   /*City*/    
                   if(select_mer_city.val() == 0){
                       select_mer_city.css('border', '1px solid red'); 
                       $('#city_error_msg').html('Please Select Merchant City');
                       select_mer_city.focus();
                       return false;
                   }else{
                       select_mer_city.css('border', ''); 
                       $('#city_error_msg').html('');
                   }
               
               
               
               var file          = $('#file');
               var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                    if(file.val() == "")
               {
               file.focus();
               file.css('border', '1px solid red');         
               return false;
               }            
               else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {              
               file.focus();
               file.css('border', '1px solid red');         
               return false;
               }            
               else
               {
               file.css('border', '');              
               }
               });
               
               var passData = 'city_id_ajax=<?php echo $fetch_mer_details->mer_ci_id; ?>';
               //alert(passData);
                $.ajax( {
                    type: 'get',
                 data: passData,
                 data: {'city_id_ajax':'<?php echo $fetch_mer_details->mer_ci_id; ?>','country_id_ajax':'<?php echo $fetch_mer_details->mer_co_id; ?>'},
                 url: '<?php echo url('ajax_select_city_edit'); ?>',
                 success: function(responseText){  
                // alert(responseText);
                  if(responseText)
                  { 
                $('#select_mer_city').html(responseText);                      
                  }
               }        
               });  
               
               });
            </script>
            <script>
               function select_mer_city_ajax(city_id)
               {
                 var passData = 'city_id='+city_id;
                // alert(passData);
                   $.ajax( {
                          type: 'get',
                          data: passData,
                          url: '<?php echo url('ajax_select_city'); ?>',
                          success: function(responseText){  
                         // alert(responseText);
                           if(responseText)
                           { 
                            $('#select_mer_city').html(responseText);                      
                           }
                        }       
                    }); 
               }
            </script>
            <script>
               function email_exixts(){
                   
               var email_id = $('#email_id');
               
               var passdata = 'email_id='+email_id.val();
               
                       $.ajax({
                                 type: 'get',
                                 data: passdata,
                                 url: '<?php echo url("check_mer_email_exist"); ?>',
                                 success: function(responseText){ 
                                  
                                   if(responseText){  
                                     $('#email_id_error_msg').html(responseText);      
                                     email_id.focus();
                                     $('#email_id').val("");   
                                     return false;            
                                   }else{
                              $('#email_id_error_msg').html('');
                           }
                                 }     
                       }); 
               }
            </script>
            <script type="text/javascript">
               $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
               });
            </script>   
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
         </body>
         <!-- END BODY -->
      </html>