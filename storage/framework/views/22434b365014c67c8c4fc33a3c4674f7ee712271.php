<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DETAILS')); ?> <?php endif; ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
      <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>		
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


       <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->
        
	<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?><?php endif; ?></a></li>
                                <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DETAILS')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DETAILS')); ?> <?php endif; ?></h5>
            
        </header>
         
        <?php $__currentLoopData = $deal_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php
		$title 		 = $deals->deal_title;
		
         $category_get	 = $deals->deal_category;
	     $maincategory 	 = $deals->deal_main_category;
		 $subcategory 	 = $deals->deal_sub_category;
		 $secondsubcategory = $deals->deal_second_sub_category;
		 $originalprice  = $deals->deal_original_price;
		 $discountprice  = $deals->deal_discount_price;
		 $startdate 	 = $deals->deal_start_date;
		 $enddate 		 = $deals->deal_end_date;
		 $expirydate	 =  $deals->deal_end_date;
		 $description 	 = $deals->deal_description;
		 //$description_fr = $deals->deal_description_fr;
		 $merchant		 = $deals->deal_merchant_id;
		 $shop			 = $deals->deal_shop_id;
		 $metakeyword	 = $deals->deal_meta_keyword;
     
		 $metadescription= $deals->deal_meta_description;
		
		 $minlimt		 = $deals->deal_min_limit;
		 $maxlimit		 = $deals->deal_max_limit;
		 $purchaselimit	 = $deals->deal_purchase_limit;
		 $file_get		     = $deals->deal_image;
		 $dealtax		     = $deals->deal_inctax;
		 $dealshipmt		 = $deals->deal_shippamt;
		
		 $file_get_path =  explode("/**/",$file_get,-1); 
		 $img_count =  count($file_get_path); 
		?>
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    <?php echo Form::open(); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DETAILS')); ?> <?php endif; ?>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_TITLE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_TITLE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($title); ?>

                    </div>
                </div>
                        </div>
                        <?php if(!empty($get_active_lang)): ?> 
                <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php    $get_lang_name = $get_lang->lang_name;
                $get_lang_code = $get_lang->lang_code;
                $deal_title_dynamic = 'deal_title_'.$get_lang_code; 
                ?>
                         <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_TITLE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_TITLE')); ?> <?php endif; ?>
                        (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo e($deals->$deal_title_dynamic); ?>

                    </div>
                </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
						  <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TOP_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TOP_CATEGORY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TOP_CATEGORY')); ?><?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($deals->mc_name); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MAIN_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MAIN_CATEGORY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MAIN_CATEGORY')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($deals->smc_name); ?>

                    </div>
                </div>
                        </div>
                
                 <div class="panel-body">
                    <div class="form-group">
                      <label class="control-label col-lg-2" for="text1">Sub Category</label>
                      <div class="col-lg-4">
                         
                          <?php if($deals->sb_name!=""): ?><?php echo e($deals->sb_name); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?>

                        
                      </div>
                    </div>
                 </div>

                 <div class="panel-body">
                    <div class="form-group">
                      <label class="control-label col-lg-2" for="text1">Second Sub Category</label>
                      <div class="col-lg-4">
                       <?php if($deals->ssb_name!=""): ?>
						<?php echo e($deals->ssb_name); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?>
                        
                      </div>
                    </div>
                 </div>
			
				
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEALS_PRICE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEALS_PRICE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS_PRICE')); ?><?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($originalprice); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DISCOUNT_PRICE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DISCOUNT_PRICE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DISCOUNT_PRICE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($discountprice); ?>

                    </div>
                </div>
                        </div>
				
                <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DISCOUNT_PERCENTAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DISCOUNT_PERCENTAGE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DISCOUNT_PERCENTAGE')); ?><?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo e($deals->deal_discount_percentage."%"); ?>

                    </div>
                </div>    
                </div>  
				
				<?php if($dealtax !=''): ?>
				<div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAX_PERCENTAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAX_PERCENTAGE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX_PERCENTAGE')); ?><?php endif; ?></label>
                    <div class="col-lg-4">
                    <?php echo e($dealtax."%"); ?>

                    </div>
                </div>    
                </div>
				<?php endif; ?>
				
				<?php if($dealshipmt !='0'): ?>
				<div class="panel-body">
                 <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIP_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIP_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIP_AMOUNT')); ?><?php endif; ?></label>
                    <div class="col-lg-4">
                    <?php echo e($dealshipmt); ?>

                    </div>
                </div>    
                </div>
               <?php endif; ?>

				<?php   /*		 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SAVINGS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SAVINGS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SAVINGS');} ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo $deals->deal_saving_price; ?>
                    </div>
                </div>
				</div>*/  ?>
				<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_START_DATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_START_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_START_DATE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e(date('M-d-Y h:m:s',strtotime($startdate))); ?>

                    </div>
                    </div>
               </div>

						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_END_DATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_END_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_END_DATE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e(date('M-d-Y h:m:s',strtotime($enddate))); ?>

                    </div>
                </div>
                        </div>
				<div class="panel-body">
                    <div class="form-group">
                     <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DESCRIPTION')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                      <div class="col-lg-4">
					 <?php echo $description; ?>
                       </div>
                    </div>
                </div>
				
				<?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
			<?php
				$get_lang_name = $get_lang->lang_name;
				$description_dynamic = 'deal_description_'.$get_lang->lang_code; 
				$descrip_dynamic = $deals->$description_dynamic; ?>
				 <?php if($descrip_dynamic !=''): ?> 
				<div class="panel-body">
                    <div class="form-group">
                     <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DESCRIPTION')); ?> <?php endif; ?>
                        (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                      <div class="col-lg-4">
					  
                       <?php echo $descrip_dynamic; ?>
                       </div>
                    </div>
                </div>	
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
				
				
                 <?php   /* 
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_MINIMUM_DEAL_LIMIT')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_MINIMUM_DEAL_LIMIT');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_MINIMUM_DEAL_LIMIT');} ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                   <?php echo  $minlimt; ?>
                    </div>
                </div>
				 </div>*/  ?>

				<div class="panel-body">
                           <div class="form-group">
						   <?php echo Html::decode(Form::label('','User Limit<span class="text-sub">*</span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>   
                    
                    <div class="col-lg-4">
					<?php echo e($maxlimit); ?>

                    </div>
                </div>
                        </div>
			<?php /*		
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_PURCHASE_COUNT')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_PURCHASE_COUNT');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_PURCHASE_COUNT');} ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                 <?php echo $deals->deal_no_of_purchase; ?>
                    </div>
                </div>
			</div>*/  ?>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MERCHANT_NAME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_NAME')); ?><?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo e($deals->mer_fname); ?>

                    </div>
                </div>
                        </div>
						
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHOP_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHOP_NAME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHOP_NAME')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-8">
					<?php echo e($deals->stor_name); ?> <?php /*if($deals->stor_name_fr !=''){ echo ' ('.$deals->stor_name_fr.')';}*/ ?>
                    </div>
                </div>
                        </div>
						
                <div class="panel-body">
                    <div class="form-group">
					 <?php echo Html::decode(Form::label('','Meta Keywords<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>  
						
						<div class="col-lg-8">
              <?php if($metakeyword!=""): ?><?php echo e($metakeyword); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?></div>
					</div>
                </div>
				
				 <?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php
			 	$get_lang_name = $get_lang->lang_name;
				$metakey_dynamic = 'deal_meta_keyword_'.$get_lang->lang_code; 
				$metakeyword_dynamic = $deals->$metakey_dynamic;  ?>
				<?php if($metakeyword_dynamic !=''): ?>
				<div class="panel-body">
                    <div class="form-group">
					 
						<label class="control-label col-lg-2" for="text1"> Meta Keywords (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
						<div class="col-lg-8">
                            <?php if($metakeyword!=""): ?><?php echo e($metakeyword_dynamic); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?></div>
					</div>
                </div>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
                <div class="panel-body">
                    <div class="form-group">
					 <?php echo Html::decode(Form::label('','Meta Description<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>  
					 
						<div class="col-lg-8">
            <?php if($metadescription!=""): ?><?php echo e($metadescription); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?></div>
					</div>
                </div>
				
				 <?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				
				<?php
				$get_lang_name = $get_lang->lang_name;
				$metades_dynamic = 'deal_meta_description_'.$get_lang->lang_code; 
				$metakeydesc_dynamic = $deals->$metades_dynamic;  ?>
				<?php if($metakeydesc_dynamic !=''): ?>
				<div class="panel-body">
                    <div class="form-group">
					 <label class="control-label col-lg-2" for="text1"> Meta Description(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>
						<div class="col-lg-8">
            <?php if($metadescription!=""): ?><?php echo e($metakeydesc_dynamic); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?></div>
					</div>
                </div>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>	
				
			   <div class="panel-body">
				  <div class="form-group">
					 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APPLY_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APPLY_CANCEL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APPLY_CANCEL')); ?> <?php endif; ?> <span class="text-sub">*</span></label>
					 <div class="col-lg-8">
					 <?php if($deals->allow_cancel == 1 ): ?><?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?> 
					 </div>
				  </div>
			   </div>
			   
			    <?php if($deals->allow_cancel == 1 ): ?> 
			   <div class="panel-body">
				  <div class="form-group">
					 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION')); ?><?php endif; ?><span class="text-sub">*</span></label>
					 <div class="col-lg-10">
						<?php echo $deals->cancel_policy ; ?>
					 </div>
				  </div>
			   </div>
			   <?php endif; ?>
			   
			    <?php if($deals->allow_cancel == 1 ): ?> <?php if(!empty($get_active_lang)): ?> 
				   <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				   <?php
					  $get_lang_name = $get_lang->lang_name;
					  $cancel_policy_dynamic = 'cancel_policy_'.$get_lang->lang_code; 
					  $descrip_cancel_policy = $deals->$cancel_policy_dynamic; ?>
				   <?php if($descrip_cancel_policy !=''): ?>
				   <div class="panel-body">
					  <div class="form-group">
						 <label class="control-label col-lg-2" for="text1"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION')); ?> <?php endif; ?> 
                            (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
						 <div class="col-lg-10">
							<?php echo $descrip_cancel_policy ; ?>
						 </div>
					  </div>
				   </div>
			   <?php endif; ?>
			   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			   <?php endif; ?>
			   <?php endif; ?>
				
			 <?php if($deals->allow_cancel == 1 ): ?>  
			<div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION')); ?> <?php endif; ?> <span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo e($deals->cancel_days); ?>

				 </div>
			  </div>
		   </div>
		   <?php endif; ?>
		   
		    <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APPLY_RETURN')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APPLY_RETURN')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APPLY_RETURN')); ?><?php endif; ?><span class="text-sub">*</span></label>
				 <div class="col-lg-8">
					<?php if($deals->allow_return == 1 ): ?> <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>  
				 </div>
			  </div>
		   </div>
		   
		    <?php if($deals->allow_return == 1 ): ?> 
		   <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION')); ?> <?php endif; ?> <span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo $deals->return_policy ; ?>
				 </div>
			  </div>
		   </div>
		   <?php endif; ?>
		   
		   <?php if($deals->allow_return == 1 ): ?>  <?php if(!empty($get_active_lang)): ?> 
		   <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
		   <?php 
			  $get_lang_name = $get_lang->lang_name;
			  $return_policy_dynamic = 'return_policy_'.$get_lang->lang_code; 
			  $return_policy = $deals->$return_policy_dynamic;  ?>
		   <?php if($return_policy !=''): ?> 
		   <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION')); ?>  <?php endif; ?>
                    (<?php echo e($get_lang_name); ?>)  <span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo $return_policy ; ?> 
				 </div>
			  </div>
		   </div>
		   <?php endif; ?>
		   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		   <?php endif; ?>
		   <?php endif; ?>
		   
		    <?php if($deals->allow_return == 1 ): ?> 
		   <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION')); ?>  <?php endif; ?><span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo e($deals->return_days); ?> 
				 </div>
			  </div>
		   </div>
		   <?php endif; ?>
		   
		    <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APPLY_REPLACEMENT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APPLY_REPLACEMENT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APPLY_REPLACEMENT')); ?>  <?php endif; ?><span class="text-sub">*</span></label>
				 <div class="col-lg-8">
					<?php if($deals->allow_replace == 1 ): ?> <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>  
				 </div>
			  </div>
		   </div>
		   <?php if($deals->allow_replace == 1 ): ?> 
		   <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')); ?>  <?php endif; ?> <span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo $deals->replace_policy; ?> 
				 </div>
			  </div>
		   </div>
		   <?php endif; ?>
		   
		    <?php if($deals->allow_replace == 1 ): ?>  <?php if(!empty($get_active_lang)): ?>  

		   <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
		   <?php 
			  $get_lang_name = $get_lang->lang_name;
			  $replace_policy_dynamic = 'replace_policy_'.$get_lang->lang_code; 
			  $replace_policy = $deals->$replace_policy_dynamic; ?>
		   <?php if($replace_policy !=''): ?>
		   <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')); ?>  <?php endif; ?>
                    (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo $replace_policy; ?>
				 </div>
			  </div>
		   </div>
		   <?php endif; ?>
		   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		   <?php endif; ?>
		   <?php endif; ?>
		   
		    <?php if($deals->allow_replace == 1 ): ?> 
		   <div class="panel-body">
			  <div class="form-group">
				 <label class="control-label col-lg-2" for="text1"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION')); ?>  <?php endif; ?><span class="text-sub">*</span></label>
				 <div class="col-lg-10">
					<?php echo e($deals->replace_days); ?> 
				 </div>
			  </div>
		   </div>
		   <?php endif; ?>

				
				
				
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEALS_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEALS_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS_IMAGE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					
					  <?php if(count($file_get_path) > 0 && $img_count !=''): ?> 
				   
					  
					   
						<?php $__currentLoopData = $file_get_path; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   
					    
						   <?php if( $image!=''): ?>   
						    <?php 
							   $pro_img = $image;
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';
							  $img_data = "public/assets/deals/".$pro_img; ?>
							    <?php if(file_exists($img_data)): ?>  
									 <?php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img; ?>
								     
								<?php else: ?> 
										 <?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 	<?php 					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];  ?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												 <?php 
													$prod_path = url().'/public/assets/noimage/'.$DynamicNoImage['dealImg']; ?>
												<?php endif; ?>
												<?php endif; ?>
										 
										 
                                    <?php endif; ?>
					       
					      
						   <?php else: ?>
						   
							    <?php
					  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; ?>
					   <?php if(isset($DynamicNoImage['dealImg'])): ?>
													
											<?php
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											
											?>
											  <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
											<?php
												
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											   
											   ?>
											<?php endif; ?>
											
									     <?php endif; ?>
										 
						   <?php endif; ?>
						    <img style="height:40px;" src="<?php echo e($prod_path); ?>">
					   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			      
				  <?php else: ?>
				  <?php
					  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; ?>
					   <?php if(isset($DynamicNoImage['dealImg'])): ?>
													
											<?php
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											
											?>
											  <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
											<?php
												
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											   
											   ?>
											<?php endif; ?>
											
									     <?php endif; ?>

									 
								
					  
					 <img style="height:40px;" src="<?php echo e($prod_path); ?>">
				 <?php endif; ?>
					
					
					
					
                           <!-- <img style="height:40px;" src=" echo url(''); ?>/public/assets/deals/ echo $file_get_path[0]; ?>">
                           
					 for($j=1 ; $j< $img_count; $j++)
					
                     <img style="height:40px;" src=" echo url(''); ?>/public/assets/deals/ echo $file_get_path[$j]; ?>"> -->
                     
                    </div>
                </div>
                        </div>
                    </div>
					
					<div class="form-group">
                    <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-2">
                    
                    <a style="color:#fff" href="<?php echo e(URL::previous()); ?>"   class="btn btn-warning btn-sm btn-grad"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_BACK')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_BACK')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_BACK')); ?> <?php endif; ?></a>
                    </div>
					  
                </div>
                
				<?php echo Form::close(); ?>

                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->  
  <!-- FOOTER -->
      <?php echo $adminfooter; ?>

    <!--END FOOTER --> 
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     
</body>
     <!-- END BODY -->
</html>
