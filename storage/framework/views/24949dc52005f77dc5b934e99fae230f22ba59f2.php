<!DOCTYPE html>
<html lang="en">
<?php echo $navbar; ?>

<?php echo $header; ?>


<body>
<?php  $i=1;
                           $grand_total =0;
                           $total_tax =0; ?>
                  
                        <?php $__currentLoopData = $getproductordercod_orderwise; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coddetails1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                           $status=$coddetails1->cod_status;
                           
                           
                           ?>

                        <div class="cod-invoice-table">
                        <div class="" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">
                           <div class="container">
                                    <div class="Front-inv-topleft"><?php 
                                       clearstatcache();
                                             //$logo = url().'/public/assets/default_image/Logo.png';
                                             //if(file_exists($SITE_LOGO))
                                               $logo = $SITE_LOGO;
                                             ?> 
                                       <img src="<?php echo e($logo); ?>" alt="" style="">
                                    </div>
                                    
                                    <div class="Front-inv-topright" style="">
                                       <strong><?php if(Lang::has(Session::get('lang_file').'.TAX_INVOICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TAX_INVOICE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TAX_INVOICE')); ?> <?php endif; ?> </strong>
                                    </div>
                                    </div>
                                 </div>
                           <div class="container">
                              <div class="">
                            
                                 <div class="" style=" clear: both;">
                                    <div class="col-lg-12">
                                       <div class="Front-inv-address-left" style="text-align: left;">
                                          <h4><?php if(Lang::has(Session::get('lang_file').'.CASH_ON_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CASH_ON_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CASH_ON_DELIVERY')); ?> <?php endif; ?></h4>
                                          <b><?php if(Lang::has(Session::get('lang_file').'.AMOUNT_PAID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AMOUNT_PAID')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AMOUNT_PAID')); ?> <?php endif; ?> :
                                          <?php echo e(Helper::cur_sym()); ?>

                                          <?php 
                                             $subtotal1=0;
                                             $customer_id = session::get('customerid');
                                             $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $coddetails1->cod_transaction_id)
                                             ->orderBy('nm_ordercod.cod_id', 'desc')
                                             ->where('nm_ordercod.cod_order_type', '=', 1)
                                             ->where('nm_ordercod.cod_cus_id', '=', $customer_id)
                                             ->get();
                                             
                                             $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = 0;
                                                foreach($product_titles as $prd_tit) {
                                             
                                                   
                                                $subtotal=$prd_tit->cod_amt; 
                                                $tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
                                             
                                                $total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
                                                $total_ship_amt+= $prd_tit->cod_shipping_amt;
                                                $total_item_amt+=$prd_tit->cod_amt;
                                                $coupon_amount+= $prd_tit->coupon_amount;
                                                $prodct_id = $prd_tit->cod_pro_id;
                                                $grand_total = ($total_item_amt + $total_tax_amt) + $total_ship_amt;
                                             $walletusedamt_final=DB::table('nm_ordercod_wallet')->where('nm_ordercod_wallet.cod_transaction_id','=', $coddetails1->cod_transaction_id)->get();
                                             ?>
                                          <?php if(count($walletusedamt_final)>0): ?> 
                                          <?php
                                          $walletamttot=$walletusedamt_final[0]->wallet_used;
                                          $totalpaid_amt=($grand_total-$walletusedamt_final[0]->wallet_used); 
                                          echo number_format($totalpaid_amt,2);
                                          ?>
                                          <?php else: ?> 
                                          <?php
                                          $totalpaid_amt =($total_item_amt + $total_ship_amt+ $total_tax_amt - $coupon_amount);
                                          $walletamttot=0;
                                          ?>
                                          <?php endif; ?>
                                          <?php } ?>
                                          <?php echo e($totalpaid_amt); ?></b>
                                          <br>
                                          <span>(<?php if(Lang::has(Session::get('lang_file').'.INCLUSIVE_OF_ALL_CHARGES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.INCLUSIVE_OF_ALL_CHARGES')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.INCLUSIVE_OF_ALL_CHARGES')); ?> <?php endif; ?>)</span>
                                          <br>
                                          <span><?php if(Lang::has(Session::get('lang_file').'.ORDERID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDERID')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDERID')); ?> <?php endif; ?>: <?php echo e($coddetails1->cod_transaction_id); ?></span><br>
                                          <span><?php if(Lang::has(Session::get('lang_file').'.ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_DATE')); ?> <?php endif; ?>: <?php echo e($coddetails1->cod_date); ?></span>
                                       </div>
                  <div class="Front-inv-address-right" style="border-left:1px solid #eeeeee;text-align:left;">
                                          <h4><?php if(Lang::has(Session::get('lang_file').'.SHIPPING_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHIPPING_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHIPPING_ADDRESS')); ?> <?php endif; ?></h4>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NAME')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_name); ?><br>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.PHONE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PHONE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PHONE')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_phone); ?><br>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_email); ?> <br>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS1')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_address1); ?> <br>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS2')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_address2); ?> <br>
                                          <?php if($coddetails1->ship_ci_id): ?>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_ci_id); ?> <br>
                                          <?php endif; ?>
                                          <?php if($coddetails1->ship_state): ?>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.STATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STATE')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_state); ?> <br>
                                          <?php endif; ?>
                                          <?php if($coddetails1->ship_country): ?>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUNTRY')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_country); ?> <br>
                                          <?php endif; ?>
                                          <strong><?php if(Lang::has(Session::get('lang_file').'.ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ZIPCODE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ZIPCODE')); ?> <?php endif; ?> : </strong><?php echo e($coddetails1->ship_postalcode); ?> <br>
                                       </div>
                                    </div>
                                 </div>
                                 <hr style="clear: both;">
                                 <div class="" style="padding: 15px; text-align: center;">
                                    <div class="span12 text-center">
                                       <h4 class="text-center"><?php if(Lang::has(Session::get('lang_file').'.INVOICE_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.INVOICE_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.INVOICE_DETAILS')); ?> <?php endif; ?></h4>
                                       <span><?php if(Lang::has(Session::get('lang_file').'.THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')); ?> <?php endif; ?> </span>
                                    </div>
                                 </div>
                                 <div style="clear: both;"></div>
                                 <br>
                                 <?php /*<!--<h4 class="text-center"> //if (Lang::has(Session::get('lang_file').'.PRODUCT_DETAILS')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_DETAILS');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_DETAILS');} </h4>--> */ ?>
                                 <div class="table-responsive">
                                    <table class="inv-table" style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">
                                       <tr style="border-bottom:1px solid #e6e6e6; background:#f5f5f5;">
                                          <th width=""><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCT_TITLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_TITLE')); ?> <?php endif; ?></th>
                                          <?php /*<!--td  width="13%" align="center">Color</td>&nbsp;
                                             <td  width="13%" align="center">Size</td-->  */?>
                                          <th  width="" align="center"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?></th>
                                          <th  width="" align="center"><?php if(Lang::has(Session::get('lang_file').'.PRICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRICE')); ?> <?php endif; ?></th>
                                          <th  width="" align="center"><?php if(Lang::has(Session::get('lang_file').'.COUPON_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUPON_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUPON_AMOUNT')); ?> <?php endif; ?></th>
                                          <th  width="" align="center"><?php if(Lang::has(Session::get('lang_file').'.SUB_TOTAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SUB_TOTAL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SUB_TOTAL')); ?> <?php endif; ?></th>
                                          <th  width="" align="center"><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_STATUS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_STATUS')); ?> <?php endif; ?></th>
                                          <th  width="" align="center"><?php if(Lang::has(Session::get('lang_file').'.DELIVERY_STATUS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DELIVERY_STATUS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DELIVERY_STATUS')); ?> <?php endif; ?></th>
                                       </tr>
                                       <?php 
                                          $subtotal1=0;
                                          $customer_id = session::get('customerid');
                                          $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $coddetails1->cod_transaction_id)
                                          ->orderBy('nm_ordercod.cod_id', 'desc')
                                          ->where('nm_ordercod.cod_order_type', '=', 1)
                                          ->where('nm_ordercod.cod_cus_id', '=', $customer_id)
                                          ->get();
                                          
                                          $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = 0; ?>
                                       <?php foreach($product_titles as $prd_tit) { ?>
                                    <?php if($prd_tit->delivery_status==1): ?>
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_PLACED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PLACED') : trans($OUR_LANGUAGE.'.ORDERS_PLACED');
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==2): ?> 
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_PACKED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PACKED') : trans($OUR_LANGUAGE.'.ORDERS_PACKED');
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==3): ?> 
                                    <?php
                                    $orderstatus= (Lang::has(Session::get('lang_file').'.ORDERS_DISPATCHED')!= '') ? trans(Session::get('lang_file').'.ORDERS_DISPATCHED') : trans($OUR_LANGUAGE.'.ORDERS_DISPATCHED') ;
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==4): ?> 
                                    <?php
                                    $orderstatus= (Lang::has(Session::get('lang_file').'.ORDERS_DELIVERED')!= '') ? trans(Session::get('lang_file').'.ORDERS_DELIVERED') : trans($OUR_LANGUAGE.'.ORDERS_DELIVERED');
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==5): ?>
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_CANCEL_PENDING')!= '') ? trans(Session::get('lang_file').'.ORDERS_CANCEL_PENDING') : trans($OUR_LANGUAGE.'.ORDERS_CANCEL_PENDING') ;
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==6): ?> 
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_CENCELED')!= '') ? trans(Session::get('lang_file').'.ORDERS_CENCELED') : trans($OUR_LANGUAGE.'.ORDERS_CENCELED') ;
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==7): ?> 
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_RETURN_PENDING')!= '') ? trans(Session::get('lang_file').'.ORDERS_RETURN_PENDING') : trans($OUR_LANGUAGE.'.ORDERS_RETURN_PENDING');
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==8): ?> 
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_RETURNED')!= '') ? trans(Session::get('lang_file').'.ORDERS_RETURNED') : trans($OUR_LANGUAGE.'.ORDERS_RETURNED');
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==9): ?> 
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_REPLACE_PENDING')!= '') ? trans(Session::get('lang_file').'.ORDERS_REPLACE_PENDING') : trans($OUR_LANGUAGE.'.ORDERS_REPLACE_PENDING');
                                    ?>
                                    <?php elseif($prd_tit->delivery_status==10): ?> 
                                    <?php
                                    $orderstatus=(Lang::has(Session::get('lang_file').'.ORDERS_REPLACED')!= '') ? trans(Session::get('lang_file').'.ORDERS_REPLACED') : trans($OUR_LANGUAGE.'.ORDERS_REPLACED') ;
                                    ?>
                                    <?php else: ?>
                                    <?php
                                    $orderstatus = '';
                                    ?>
                                    <?php endif; ?> 
                                       <?php if($prd_tit->cod_status==1): ?>
                                       <?php
                                       $payment_status=(Lang::has(Session::get('lang_file').'.SUCCESS')!= '') ? trans(Session::get('lang_file').'.SUCCESS') : trans($OUR_LANGUAGE.'.SUCCESS');
                                       ?>
                                       <?php elseif($prd_tit->cod_status==2): ?> 
                                       <?php
                                       $payment_status=(Lang::has(Session::get('lang_file').'.ORDERS_PACKED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PACKED') : trans($OUR_LANGUAGE.'.ORDERS_PACKED');
                                       ?>
                                       <?php elseif($prd_tit->cod_status==3): ?> 
                                       <?php
                                       $payment_status=(Lang::has(Session::get('lang_file').'.ORDERS_PACKED')!= '') ? trans(Session::get('lang_file').'.ORDERS_PACKED') : trans($OUR_LANGUAGE.'.ORDERS_PACKED');
                                       ?>
                                       <?php elseif($prd_tit->cod_status==4): ?> 
                                       <?php
                                       $payment_status=(Lang::has(Session::get('lang_file').'.FAILED')!= '') ? trans(Session::get('lang_file').'.FAILED') : trans($OUR_LANGUAGE.'.FAILED');
                                       ?>
                                       <?php endif; ?>
                                       <?php    
                                          $subtotal=$prd_tit->cod_amt; 
                                          $tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
                                          
                                          $total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
                                          $total_ship_amt+= $prd_tit->cod_shipping_amt;
                                          $total_item_amt+=$prd_tit->cod_amt;
                                          $coupon_amount+= $prd_tit->coupon_amount;
                                          $prodct_id = $prd_tit->cod_pro_id;
                                          ?> 
                                       <tr style="border-bottom:1px solid #666;">
                                          <td  width="" align="center"> 
                                             <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                                             <?php $pro_title = 'pro_title'; ?>
                                             <?php else: ?>  <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>
                                             <?php echo $prd_tit->$pro_title."<br/>";?>
                                             <?php if($prd_tit->si_name!="") echo "Size:".$prd_tit->si_name.", ";
                                                if($prd_tit->co_name!="") echo "Color:".$prd_tit->co_name."<br/>";?>
                                          </td>
                                          <!--td  width="13%" align="center"><?php //echo $coddetails1->co_name;?></td>&nbsp;
                                             <td  width="13%" align="center"><?php //echo $coddetails1->si_name;?></td-->
                                          <td  width="" align="center"><?php echo e($prd_tit->cod_qty); ?> </td>
                                          <td  width="13%" align="center"><?php echo e(Helper::cur_sym()); ?><?php echo e($prd_tit->pro_disprice); ?> </td>
                                          <?php if($prd_tit->coupon_amount != 0): ?> 
                                          <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?><?php echo e($prd_tit->coupon_amount); ?> </td>
                                          <?php else: ?>  
                                          <td  width="13%" align="center"><?php echo e(Helper::cur_sym()); ?><?php echo e($prd_tit->coupon_amount); ?> <?php endif; ?>
                                          <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?><?php echo e($subtotal - $prd_tit->coupon_amount); ?> </td>
                                          <td  width="" align="center"><?php echo e($payment_status); ?></td>
                                          <td  width="" align="center"><?php echo e($orderstatus); ?></td>
                                          </td>
                                       </tr>
                                       <?php }?>
                                       <?php $grand_total = ($total_item_amt + $total_tax_amt) + $total_ship_amt;
                                          $walletusedamt_final=DB::table('nm_ordercod_wallet')->where('nm_ordercod_wallet.cod_transaction_id','=', $coddetails1->cod_transaction_id)->get(); ?>
                                       <?php if(count($walletusedamt_final)>0): ?> 
                                       <?php 
                                       $walletamttot=$walletusedamt_final[0]->wallet_used;
                                       $totalpaid_amt=($grand_total-$walletusedamt_final[0]->wallet_used);
                                       echo number_format($totalpaid_amt,2); ?>
                                       <?php else: ?> 
                                       <?php
                                       $totalpaid_amt =($total_item_amt + $total_ship_amt+ $total_tax_amt - $coupon_amount);
                                       $walletamttot=0;
                                       ?>
                                       <?php endif; ?>
                                    </table>
                                 </div>
                                 <br>
                                 <hr>
                                 <div class="" style="padding: 15px; clear: both; overflow: hidden;">
                                    <div class="col-lg-6 Front-inv-price-left"></div>
                                    <div class="col-lg-6 Front-inv-price-right">
                                       <span><?php if(Lang::has(Session::get('lang_file').'.SHIPMENT_VALUE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHIPMENT_VALUE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHIPMENT_VALUE')); ?> <?php endif; ?><b class="pull-right" style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($total_ship_amt); ?> </b></span><br>
                                       <span><?php if(Lang::has(Session::get('lang_file').'.TAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TAX')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TAX')); ?> <?php endif; ?><b class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($total_tax_amt); ?></b></span><br>
                                       <?php if(count($walletusedamt_final)>0): ?> 
                                       <span><?php if(Lang::has(Session::get('lang_file').'.WALLET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WALLET')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WALLET')); ?> <?php endif; ?><b class="pull-right"style="margin-right:15px;">- <?php echo e(Helper::cur_sym()); ?><?php echo e($walletamttot); ?></b></span>
                                       <?php endif; ?>
                                       <hr>
                                       <span><?php if(Lang::has(Session::get('lang_file').'.AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AMOUNT')); ?> <?php endif; ?><b class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($totalpaid_amt); ?></b></span>
                                    </div>
                                 </div>
                                 
                              </div>
                           </div>
                           <div class="modal-footer" style="border-bottom:none; overflow: hidden;background: #f5f5f5;"><div class="container">
                                   
                                 </div>
                        </div>
                        <?php $i=$i+1;  ?>  
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php echo $footer; ?>

  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</body>
</html>