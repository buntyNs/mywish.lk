<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |Edit Merchant Store Account</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
     <?php if(count($favi)>0): ?>  
      <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php endif; ?>	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <?php /* Edit Image Starts */ ?>

    <link href="<?php echo e(url('')); ?>/public/assets/cropImage/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/cropImage/editImage/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/cropImage/editImage/css/canvasCrop.css" />
    <style>
         .imageBox {
  position: relative;

  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor: move;
  box-shadow: 4px 4px 12px #B0B0B0;
}

.imageBox .thumbBox {
  position: absolute;
    top: 100px;
    left: 100px;
  box-sizing: border-box;
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}
.modal-content
{
  background-color: rgb(185, 185, 185);
}
    </style>


    
     <?php /* Edit Image ends */ ?>

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >
    <?php /* Edit Image Starts */ ?>
      <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog"  style="width:39%;">
          
            <!-- Modal content-->
            <div class="modal-content dev_appendEditData">
              <?php ?>  
              <script type="text/javascript">
                    function calSubmit(){

                      $("#dev_upImg_form").submit();
                    }
                  </script>
            

              <!-- Modal content-->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Image</h4>
              </div>
              <div class="modal-body" id='model_body'>

              
                <div class="imageBox" style="width: <?php echo e($STORE_WIDTH); ?>px; height: <?php echo e($STORE_HEIGHT); ?>px;">
                  <!--<div id="img" ></div>-->
                  <!--<img class="cropImg" id="img" style="display: none;" src="images/avatar.jpg" />-->
                  <div class="mask"></div>
                  <div class="thumbBox" style="width: <?php echo e($STORE_WIDTH); ?>px; height: <?php echo e($STORE_HEIGHT); ?>px;"></div>
                  <div class="spinner" style="display: none">Loading...</div>
                </div>
                <div class="tools clearfix" style='display: block; left:5px;top:250px;width:450px; margin-top: 15px;'>
                  <span id="rotateLeft" >rotateLeft</span>
                  <span id="rotateRight" >rotateRight</span>
                  <span id="zoomIn" >zoomIn</span>
                  <span id="zoomOut" >zoomOut</span>
                  <span id="crop" >Crop</span>
                  <!--<span id="alertInfo" >alert</span> -->
                  <div class="upload-wapper">
                             Select An Image
                      <input type="file" id="upload-file" value="Upload" />
                  </div>
                    <div class="crop-edit-up" style="margin-left: 5px; display: inline-block;">
                  <button type="button" class="btn btn-success" id='dev_uploadBtn' onclick="calSubmit();" style='display: none'>Upload</button></div>
                </div>

                <form id='dev_upImg_form' action="<?php echo url(''); ?>/CropNdUpload_store" method='post' >
                    
                    <input name="_token" hidden value="<?php echo csrf_token(); ?>" />
                    <?php echo e(Form::hidden('product_id','',array('id'=>'product_id'))); ?>

                    <?php echo e(Form::hidden('img_id','',array('id'=>'img_id'))); ?>

                    <?php echo e(Form::hidden('mer_id','',array('id'=>'mer_id'))); ?>

                    <?php echo e(Form::hidden('imgfileName','',array('id'=>'imgfileName'))); ?>

                    <?php echo e(Form::hidden('base64_imgData','',array('id'=>'croppedImg_base64'))); ?>

                    <!-- <input type='hidden' id='product_id'  name='product_id' /> -->
                    <!-- <input type='hidden' id='img_id'  name='img_id'  /> -->
                    <!-- <input type='hidden' id='mer_id'  name='mer_id'  /> -->
                    <!-- <input type='hidden' id='imgfileName'  name='imgfileName'  /> -->
                    <!-- <input type='hidden' id='croppedImg_base64'  name='base64_imgData' /> -->
                    <input type="submit" value="submit" style="display: none" />
                </form> 
                <div id='showCroppedImg' ></div>
                <!-- Edit image starts -->
                <script type="text/javascript">
                    $(function(){
                       
                    }) 
                </script>
                <!-- Edit image ends -->

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>

  <?php ?>  
            </div>
            
          </div>
        </div>

      <!--Modal Ends-->
    <?php /* Edit Image ends */ ?>
    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                                <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT_STORE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT_STORE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_MERCHANT_STORE')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT_STORE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_MERCHANT_STORE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_MERCHANT_STORE')); ?> <?php endif; ?></h5>
            
        </header>
        <?php if(Session::has('block_message')): ?>
          <div class="alert alert-success alert-dismissable"><?php echo Session::get('block_message'); ?>

           <?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

		   </div>
        <?php endif; ?>

        <?php if($errors->any()): ?>
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

		<?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

        
        </div>
		</ul>	
		<?php endif; ?>
         <?php if(Session::has('mail_exist')): ?>
		<div class="alert alert-warning alert-dismissable"><?php echo Session::get('mail_exist'); ?>

	<?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

        </div>
		<?php endif; ?>
        
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    <?php echo Form::open(array('url'=>'edit_store_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

                   <?php $__currentLoopData = $store_return; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   <?php echo e(Form::hidden('mer_id',$mer_id,array('id'=>'mer_id'))); ?> 
                   <?php echo e(Form::hidden('store_id',$id,array('id'=>'store_id'))); ?> 
						<!-- <input type="hidden" name="mer_id" id="mer_id1" value="<?php echo $mer_id; ?>" > -->
                        <!-- <input type="hidden" id="store_id" name="store_id" value="<?php echo $id; ?>" > -->
						
					 <div class="panel panel-default">
                        <div class="panel-heading">
                      Store Details 
                        </div>
                    <div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Store Name<span class="text-sub">*</span></label>
							<div class="col-lg-4">
                <?php echo e(Form::text('store_name',$store_details->stor_name,array('id'=>'store_name','class'=>'form-control','maxlength'=>'150'))); ?>

								<!-- <input type="text" maxlength="150" class="form-control" placeholder="" id="store_name" name="store_name" value="<?php echo $store_details->stor_name; ?>"  > -->
							</div>
						</div>
                    </div>
					
					<?php if(!empty($get_active_lang)): ?>  
					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php	$get_lang_name = $get_lang->lang_name;
					$get_lang_code = $get_lang->lang_code;
					$stor_name_dynamic = 'stor_name_'.$get_lang_code;  ?>
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Store Name(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
							<div class="col-lg-4">
								<input type="text" maxlength="150" class="form-control" placeholder="Enter Store Name In <?php echo e($get_lang_name); ?>" id="store_name_<?php echo e($get_lang_name); ?>" name="store_name_<?php echo e($get_lang_name); ?>" value="<?php echo $store_details->$stor_name_dynamic; ?>"  >
							</div>
						</div>
                    </div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
					
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Phone <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       <?php echo e(Form::text('store_pho',$store_details->stor_phone,array('id'=>'store_pho','class'=>'form-control','maxlength'=>'16'))); ?>

                        <!-- <input type="text" maxlength="16" class="form-control" placeholder="" id="store_pho" name="store_pho" value="<?php echo $store_details->stor_phone; ?>"  > -->
                    </div>
                </div>
                        </div>
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address1<span class="text-sub">*</span></label>

							<div class="col-lg-4">
                <?php echo e(Form::text('store_add_one',$store_details->stor_address1,array('id'=>'store_add_one','class'=>'form-control'))); ?>

								<!-- <input type="text" class="form-control" placeholder="" id="store_add_one" name="store_add_one" value="<?php echo $store_details->stor_address1; ?>"  >
							</div> -->
						</div>
                    </div>
					
					<?php if(!empty($get_active_lang)): ?>  
					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php	$get_lang_name = $get_lang->lang_name;
					$get_lang_code = $get_lang->lang_code;
					$stor_address1_dynamic = 'stor_address1_'.$get_lang_code; ?>
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address1(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address One In <?php echo e($get_lang_name); ?>" id="store_add_one_<?php echo e($get_lang_name); ?>" name="store_add_one_<?php echo e($get_lang_name); ?>" value="<?php echo $store_details->$stor_address1_dynamic; ?>"  >
							</div>
						</div>
                    </div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address2<span class="text-sub">*</span></label>

							<div class="col-lg-4">
                <?php echo e(Form::text('store_add_two',$store_details->stor_address2,array('id'=>'store_add_two','class'=>'form-control'))); ?>

								<!-- <input type="text" class="form-control" placeholder="" id="store_add_two" name="store_add_two" value="<?php echo $store_details->stor_address2; ?>"   > -->
							</div>
						</div>
                    </div>
					
					<?php if(!empty($get_active_lang)): ?>  
					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php	$get_lang_name = $get_lang->lang_name;
					$get_lang_code = $get_lang->lang_code;
					$stor_address2_dynamic = 'stor_address2_'.$get_lang_code; ?>
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address2(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address In <?php echo e($get_lang_name); ?>" id="store_add_two_<?php echo e($get_lang_name); ?>" name="store_add_two_<?php echo e($get_lang_name); ?>" value="<?php echo $store_details->$stor_address2_dynamic; ?>"   >
							</div>
						</div>
                    </div>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
                     
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Select Country<span class="text-sub">*</span></label>

                    <div class="col-lg-4"> 
                       <select class="form-control" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
                        <option value="">-- Select --</option>
                          <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          				 <option value="<?php echo e($country_fetch->co_id); ?>" <?php if($country_fetch->co_id == $store_details->stor_country){ echo 'selected'; } ?> ><?php echo $country_fetch->co_name; ?></option>
           				   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       					 </select>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Select City<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       <select class="form-control" name="select_city" id="select_city" >
           				<option value="">--Select--</option>
                  </select>
                    </div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Zipcode<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      <?php echo e(Form::text('zip_code',$store_details->stor_zipcode,array('id'=>'zip_code','class'=>'form-control','maxlength'=>'16'))); ?>

                       <!--  <input type="text" maxlength="16" class="form-control" placeholder="" id="zip_code" name="zip_code" value="<?php echo $store_details->stor_zipcode; ?>"  > -->
                    </div>
                </div>
                        </div>
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Meta keywords<span class="text-sub"></span></label>

						<div class="col-lg-4">
							<textarea  class="form-control" name="meta_keyword" id="meta_keyword" ><?php echo $store_details->stor_metakeywords; ?></textarea>
						</div>
					</div>
                </div>
				
				<?php if(!empty($get_active_lang)): ?>  
					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
			<?php		$get_lang_name = $get_lang->lang_name;
					$get_lang_code = $get_lang->lang_code;
					$stor_metakeywords_dynamic = 'stor_metakeywords_'.$get_lang_code; ?>
					
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Meta keywords(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>

						<div class="col-lg-4">
							<textarea  class="form-control" name="meta_keyword_<?php echo e($get_lang_name); ?>" id="meta_keyword_<?php echo e($get_lang_name); ?>" ><?php echo $store_details->$stor_metakeywords_dynamic; ?></textarea>
						</div>
					</div>
                </div>
				
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Meta description<span class="text-sub"></span></label>

						<div class="col-lg-4">
							<textarea id="meta_description"  name="meta_description" class="form-control"><?php echo $store_details->stor_metadesc; ?></textarea>
						</div>
					</div>
                </div>
				
				<?php if(!empty($get_active_lang)): ?>  
					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php	$get_lang_name = $get_lang->lang_name;
					$get_lang_code = $get_lang->lang_code;
					$stor_metadesc_dynamic = 'stor_metadesc_'.$get_lang_code; ?>
					
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Meta description(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>

						<div class="col-lg-4">
							<textarea id="meta_description_<?php echo e($get_lang_name); ?>"  name="meta_description_<?php echo e($get_lang_name); ?>" class="form-control"><?php echo $store_details->$stor_metadesc_dynamic; ?></textarea>
						</div>
					</div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
				
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Website<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                      <?php echo e(Form::text('website',$store_details->stor_website,array('id'=>'website','class'=>'form-control','placeholder'=>'http://www.example.com'))); ?>

                      <!--   <input type="url" class="form-control"  id="website" name="website" value="<?php echo $store_details->stor_website; ?>"   placeholder="http://www.example.com"> -->
                    </div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input id="pac-input" onclick="check();" class="form-control getlocation" type="text" placeholder="Type your location here (Auto-complete)" value="">

                    </div>
					<div class="col-lg-4"></div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Map Search Location<span class="text-sub">*</span><br><span  style="color:#999">(Drag Marker to get latitude & longitude )</span></label>

                    <div class="col-lg-4">
                        <div id="map_canvas" style="width:300px;height:250px;" ></div>
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Latitude<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      <?php echo e(Form::text('latitude',$store_details->stor_latitude,array('id'=>'latitude','class'=>'form-control','readonly'=>'readonly'))); ?>

                       <!--  <input type="text" class="form-control" placeholder="" id="latitude" name="latitude" readonly  value="<?php echo $store_details->stor_latitude; ?>"  > -->
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Longitude<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      <?php echo e(Form::text('longitude',$store_details->stor_longitude,array('id'=>'longitude','class'=>'form-control','readonly'=>'readonly'))); ?>

                       <!--  <input type="text" class="form-control" placeholder="" id="longitude" name="longitude" value="<?php echo $store_details->stor_longitude; ?>"  readonly > -->
                    </div>
                </div>
                        </div>
                          <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SLOGAN')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SLOGAN')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SLOGAN')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      <?php echo e(Form::text('slogan',$store_details->stor_slogan,array('id'=>'slogan','class'=>'form-control'))); ?>

                       <!--  <input type="text" class="form-control" placeholder="" id="longitude" name="longitude" value="<?php echo $store_details->stor_longitude; ?>"  readonly > -->
                    </div>
                </div>
                        </div>
							
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Store Image<span class="text-sub">*</span></label>
				
                    <div class="col-lg-4 all-image-view">
                      <div class="errortext red" style="color:red"><em><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE_SIZE_MUST_BE')); ?> <?php endif; ?> <?php echo e($STORE_WIDTH); ?> x <?php echo e($STORE_HEIGHT); ?> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PIXELS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PIXELS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PIXELS')); ?> <?php endif; ?></em></div>
                       <input type="file" id="file" name="file" placeholder=""><span class="text-sub" required>
                        <?php echo e(Form::hidden('file_new',$store_details->stor_img)); ?>

                       <!-- <input type="hidden" name="file_new" value="<?php echo $store_details->stor_img; ?>" /> -->
					  <?php   $pro_img = $store_details->stor_img;
							   $prod_path = url('').'/public/assets/default_image/No_image_store.png';?>
							  
								
						  <?php if($pro_img !=''): ?>  <!-- //check image is null -->
						    
							  
							<?php   $img_data = "public/assets/storeimage/".$pro_img; ?>
							   <?php if(file_exists($img_data)): ?>  
									  
								 	<?php  $prod_path = url('').'/public/assets/storeimage/'.$pro_img; ?>
								     
								<?php else: ?>  
										 <?php if(isset($DynamicNoImage['store'])): ?> <!-- //check no_product_image is exist  -->
										 						
											<?php $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['store']; ?>
												<?php if($DynamicNoImage['store'] !='' && file_exists($dyanamicNoImg_path)): ?>
												 
												<?php	$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['store']; ?>
												<?php endif; ?>
									     <?php endif; ?>
										 
										 
                                    <?php endif; ?>
					       
					      
						   <?php else: ?>
						   
							<?php    $prod_path = url('').'/public/assets/default_image/No_image_store.png'; ?>
							<?php if(isset($DynamicNoImage['store'])): ?> <!-- //check no_product_image is set in database  -->
										 						
									<?php		$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['store']; ?>
												<?php if($DynamicNoImage['store'] !='' && file_exists($dyanamicNoImg_path)): ?> 
												
												<?php	$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['store']; ?>
												<?php endif; ?>
										
									     <?php endif; ?>
										 
						   <?php endif; ?> 
                       <img src="<?php echo e($prod_path); ?>" height="75px" >
                      
                       <br>
                      <?php if($store_details->stor_img!=''): ?>
                          <?php /* Edit Image start - Trigger the modal with a button */ ?>
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick=" calImgEdit(<?php echo $id; ?>,<?php echo e($mer_id); ?>,1,'<?php echo e($store_details->stor_img); ?>' )" >Edit</button></span><?php /* */ ?>
                      <?php endif; ?>
                    </div>
                </div>
                        </div>
                        
                    </div>
					
                    	
				
					<div class="form-group">
                    <label class="control-label col-lg-3" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" >Update</a></button>
                     <a href="<?php echo e(url('manage_store').'/'.$mer_id); ?>" class="btn btn-default btn-sm btn-grad" style="color:#000">Cancel</a>
                   
                    </div>
					  
                </div>
                
                </form>
                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      <?php echo $adminfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
    <script>
	$( document ).ready(function() {

	var passData = 'city_id_ajax='+'<?php echo $store_details->stor_city;  ?>';
		 
		   $.ajax( {
			      type: 'get',
				  data: {'city_id_ajax':'<?php echo $store_details->stor_city; ?>','country_id_ajax':'<?php echo $store_details->stor_country; ?>'},
				  url: '<?php echo url('ajax_select_city_edit'); ?>',
				  success: function(responseText){  
				  
				   if(responseText)
				   { 
            
					$('#select_city').html(responseText);					   
				   }
				}		
			});		
		
	
	});
	</script>
     <script>
	
	function select_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_city').html(responseText);					   
				   }
				}		
			});		
	}
	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}
	</script>
     <script src="<?php echo url('')?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <?php 
    //Commanded because Image Edit is effected by this script
    /*
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script> <?php */?>
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
   
<script>
$( document ).ready(function() {
var geocoder;
geocoder = new google.maps.Geocoder();
var latlng = new google.maps.LatLng('<?php echo $store_details->stor_latitude; ?>','<?php echo $store_details->stor_longitude; ?>');

geocoder.geocode(
    {'latLng': latlng}, 
    function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;
                    var  value=add.split(",");

                    count=value.length;
                    country=value[count-1];
                    state=value[count-2];
                    city=value[count-3];
                    loc=value[count-4];
					var array = [city, state, country]; 
                    //alert("city name is: " + city);
                    //alert("city name is: " + state);
                   // alert("city name is: " + country);
					$('.getlocation').val(array);
                }
                else  {
                   // alert("address not found");
                }
        }
         else {
            //alert("Geocoder failed due to: " + status);
        }
    }
);
});
</script>


   <script type="text/javascript">
    var map;

    function initialize() {


 		var myLatlng = new google.maps.LatLng('<?php echo $store_details->stor_latitude; ?>','<?php echo $store_details->stor_longitude; ?>');
        var mapOptions = {

           zoom: 10,
                center: myLatlng,
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	 		  var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				draggable:true,    
            });	
		google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
			});
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
 
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
 
            var place = autocomplete.getPlace();
            
	            var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                
                $('#latitude').val(latitude);
			    $('#longitude').val(longitude);
          check();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
				var myLatlng = place.geometry.location;	
				var latlng = new google.maps.LatLng(latitude, longitude);
                marker.setPosition(latlng);
                
			google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
        check();
			});
            } else {
                map.setCenter(place.geometry.location);	
				
                map.setZoom(17);
            }
        });



    }


    google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- END GLOBAL SCRIPTS -->   
  <?php /* Edit Image starts */ ?>
    <?php //text editor hidded by this script ,so commanded
    /* <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script> */?>
    <script type="text/javascript" src="<?php echo url('')?>/public/assets/cropImage/editImage/js/jquery.canvasCrop.js" ></script>

    <script type="text/javascript">

    function check() { 

      var mer_id     = $('#mer_id1').val();
      //var longitude  = $('#longitude').val();
      var longitude  = document.getElementById("longitude").value;
      var latitude   = $('#latitude').val();
      var store_id   = $('#store_id').val();
      var datas      = "mer_id="+mer_id+"&latitude="+latitude+"&longitude="+longitude+"&store_id="+store_id;
     
            $.ajax({
            type: 'GET',
                  url: '<?php echo url('check_store_exists_edit'); ?>',
          data: datas,
          success: function(response){ 
          //alert(response); 
                    
            if(response==1){  //already exist
            alert("This Store Already Exist with this same address");
            $("#exist").val("1"); //already exist
            $('#latitude').val('');
            $('#longitude').val('');
            $("#latitude").css('border', '1px solid red'); 
            $('#longitude').css('border', '1px solid red');
            $('#pac-input').css('border', '1px solid red');
            $('#location_error_msg').html("Store Already Exist with this same address");  
            $("#pac-input").focus();
            return false;          
           }else
           {
            $("#latitude").css('border', ''); 
            $('#longitude').css('border', '');
            $('#pac-input').css('border', '');
            $('#location_error_msg').html(""); 
           // $("#pac-input").blur(); 
           }
         }    
      });
    
}
      function calImgEdit(storeId,mer_id,imgId,imgFileName){    
        $("#product_id").val(storeId);
        $("#img_id").val(storeId);
        $("#mer_id").val(mer_id);
        $("#imgfileName").val(imgFileName);

        var rot = 0,ratio = 1;
        var CanvasCrop = $.CanvasCrop({
            cropBox : ".imageBox",
            imgSrc : "<?php echo url('');?>/public/assets/storeimage/"+imgFileName,
            limitOver : 2
        });
        
        
        $('#upload-file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                CanvasCrop = $.CanvasCrop({
                    cropBox : ".imageBox",
                    imgSrc : e.target.result,
                    limitOver : 2
                });
                rot =0 ;
                ratio = 1;
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        });
        
        $("#rotateLeft").on("click",function(){
            rot -= 90;
            rot = rot<0?270:rot;
            CanvasCrop.rotate(rot);
        });
        $("#rotateRight").on("click",function(){
            rot += 90;
            rot = rot>360?90:rot;
            CanvasCrop.rotate(rot);
        });
        $("#zoomIn").on("click",function(){
            ratio =ratio*0.9;
            CanvasCrop.scale(ratio);
        });
        $("#zoomOut").on("click",function(){
            ratio =ratio*1.1;
            CanvasCrop.scale(ratio);
        });
        $("#alertInfo").on("click",function(){
            var canvas = document.getElementById("visbleCanvas");
            var context = canvas.getContext("2d");
            context.clearRect(0,0,canvas.width,canvas.height);
        });
        
        $("#crop").on("click",function(){
            
            //var src = CanvasCrop.getDataURL("jpeg");
            var src = CanvasCrop.getDataURL("png");
            //$("body").append("<div style='word-break: break-all;'>"+src+"</div>");  
            //$("#model_body").append("<img src='"+src+"' />");
            $("#showCroppedImg").html("<img src='"+src+"' />");
            $("#croppedImg_base64").val(src);
            if(src!='')
                $("#dev_uploadBtn").css('display','block');


        });
        
        console.log("ontouchend" in document);

      }

  $( document ).ready(function() {


var phone_no         = $('#phone_no');
var store_pho        = $('#store_pho');
var zip_code         = $('#zip_code');
   $('#phone_no').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            phone_no.css('border', '1px solid red'); 
            $('#phone_no_error_msg').html('Numbers Only Allowed');
            phone_no.focus();
            return false;
        }else{          
            phone_no.css('border', ''); 
            $('#phone_no_error_msg').html('');          
        }
    });
    /*Store Phone Number*/
    $('#store_pho').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            store_pho.css('border', '1px solid red'); 
            $('#store_pho_error_msg').html('Numbers Only Allowed');
            store_pho.focus();
            return false;
        }else{          
            store_pho.css('border', ''); 
            $('#store_pho_error_msg').html('');         
        }
    });
   
    /*Store Zipcode*/
    $('#zip_code').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            zip_code.css('border', '1px solid red'); 
            $('#zip_code_error_msg').html('Numbers Only Allowed');
            zip_code.focus();
            return false;
        }else{          
            zip_code.css('border', ''); 
            $('#zip_code_error_msg').html('');          
        }
    });

        
});

      $('#store_name').bind('keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }
);
      </script>
    <?php /* Edit Image ends */ ?>  
     
</body>
     <!-- END BODY -->
</html>
