<!DOCTYPE html>

<html lang="en">

<?php echo $navbar; ?>


<?php echo $header; ?>




<body class="contact_us_page">

<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

  <!-- Header -->

  

  <!-- end header -->

  

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li> 

            <li><strong><?php echo e((Lang::has(Session::get('lang_file').'.CONTACT_US')!= '') ?  trans(Session::get('lang_file').'.CONTACT_US'): trans($OUR_LANGUAGE.'.CONTACT_US')); ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="row">

        <section class="col-main col-sm-12">

          <div id="contact" class="page-content page-contact">

            <div class="page-title">

              <h2><?php echo e((Lang::has(Session::get('lang_file').'.CONTACT')!= '') ?  trans(Session::get('lang_file').'.CONTACT'): trans($OUR_LANGUAGE.'.CONTACT')); ?></h2>

            </div>

            <div id="message-box-conact"></div>

            <div class="row">

              <div class="col-xs-12 col-sm-6" id="contact_form_map">

                <h3 class="page-subheading"></h3>

               

                <?php $__currentLoopData = $get_contact_det; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $enquiry_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <ul class="store_info">

                  <li><i class="fa fa-envelope"></i><?php echo e($enquiry_det->es_contactemail); ?></li>

                  <li><i class="fa fa-skype"></i><?php echo e($enquiry_det->es_skype_email_id); ?></li>

                  <li><i class="fa fa-phone"></i><span><?php echo e($enquiry_det->es_phone1); ?>, <?php echo e($enquiry_det->es_phone2); ?></span></li>

                  <li><a target="_blank" href="<?php echo e(url('')); ?>"><i class="fa fa-globe"></i><span><?php echo e(url('')); ?></span></a></li>

                 

                </ul>

              </div>

              <div class="col-sm-6">

                <h3 class="page-subheading"><?php if(Lang::has(Session::get('lang_file').'.EMAIL_US')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL_US')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL_US')); ?> <?php endif; ?></h3>

                <div id="error_name"  style="color:#F00;font-weight:400"  > </div>

            <?php if(Session::has('success')): ?>

            <div class="alert alert-warning alert-dismissable"><?php echo Session::get('success'); ?>


               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            </div>

            <?php endif; ?>

            <?php echo Form::open(array('url'=>'enquiry_submit','class'=>'form-horizontal')); ?>


                <div class="contact-form-box">

                  <div class="form-selector">

                    <label><?php if(Lang::has(Session::get('lang_file').'.NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NAME')); ?> <?php endif; ?></label>

                    <input type="text" class="form-control input-sm"  name="name" id="inquiry_name" maxlength ="100" required />

                  </div>

                  <div class="form-selector">

                    <label><?php if(Lang::has(Session::get('lang_file').'.EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL')); ?> <?php endif; ?></label>

                    <input type="email" class="form-control input-sm"  name="email" id="inquiry_email" maxlength="50" required />

                  </div>

                  <div class="form-selector">

                    <label><?php if(Lang::has(Session::get('lang_file').'.PHONE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PHONE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PHONE')); ?> <?php endif; ?></label>

                    <input type="text" class="form-control input-sm" name="phone" id="inquiry_phone" maxlength="16" required />

                  </div>

                  <div class="form-selector">

                    <label><?php if(Lang::has(Session::get('lang_file').'.MESSAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MESSAGE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MESSAGE')); ?> <?php endif; ?></label>

                    <textarea class="form-control input-sm" rows="10" name="message" id="inquiry_desc" required></textarea>

                  </div>

                  <div class="form-selector">

                    <button class="button"><i class="icon-paper-plane icons"></i>&nbsp; <span><?php if(Lang::has(Session::get('lang_file').'.SEND_MESSAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEND_MESSAGE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEND_MESSAGE')); ?> <?php endif; ?></span></button>

                    </div>

                </div>

                <?php echo Form::close(); ?>


              </div>

            </div>

          </div>

        </section>

      </div>

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <!-- Footer -->

  <?php echo $footer; ?>


  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<!-- JS --> 





</body>


</html>