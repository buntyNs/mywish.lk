<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


<!-- Header End====================================================================== -->

<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

	

	<?php if(Session::has('error')): ?>

		<div  ><?php echo Session::get('error'); ?>


        </div>

	<?php endif; ?>

    <?php if(Session::has('result_success')): ?>

		<div><?php echo Session::get('result_success'); ?>


        </div>

		<?php endif; ?>

          <?php if(Session::has('result_cancel')): ?>

		<div><?php echo Session::get('result_cancel'); ?>


        </div>

	<?php endif; ?>

		

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <?php /* <div class="category-description std">

            <div class="slider-items-products">

              <div id="category-desc-slider" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img1.jpg"></a>

                    <div class="inner-info">

                      <div class="cat-img-title"> <span>Best Product 2017</span>

                        <h2 class="cat-heading">Best Selling Brand</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

                        <a class="info" href="#">Shop Now</a> </div>

                    </div>

                  </div>

                  <!-- End Item --> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img2.jpg"></a> </div>

                  

                  <!-- End Item --> 

                  

                </div>

              </div>

            </div>

          </div> */ ?>

          

		  <?php if(count($searchTerms)!=0): ?>  

		  <div class="shop-inner">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_PRODUCTS')); ?> <?php endif; ?></h2>

            </div>

           

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			   <?php $i = 1; ?>

			   <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				 <?php $pro_title = 'pro_title'; ?>

				<?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

				<?php $__currentLoopData = $searchTerms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  



			

				  <?php

				  $product_saving_price = $fetch_product->pro_price - $fetch_product->pro_disprice;

				  $product_discount_percentage = round(($product_saving_price/ $fetch_product->pro_price)*100,2);



				  $product_img = explode('/**/', $fetch_product->pro_Img);

				  $mcat = strtolower(str_replace(' ','-',$fetch_product->mc_name));

				  $smcat = strtolower(str_replace(' ','-',$fetch_product->smc_name));

				  $sbcat = strtolower(str_replace(' ','-',$fetch_product->sb_name));

				  $ssbcat = strtolower(str_replace(' ','-',$fetch_product->ssb_name)); 

				  $res = base64_encode($fetch_product->pro_id); 



				  $product_image    = $product_img[0]; 

				  

				  $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				  $img_data   = "public/assets/product/".$product_image; ?>

				  

				  <?php if(file_exists($img_data) && $product_image !=''): ?>   

				  

				<?php  $prod_path = url('').'/public/assets/product/' .$product_image;                  ?>

				  <?php else: ?>  

					  <?php if(isset($DynamicNoImage['productImg'])): ?>

						<?php  $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; ?>

						  <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

						   <?php   $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

					  <?php endif; ?>

				  <?php endif; ?>   

				 

				<?php $alt_text   = substr($fetch_product->$pro_title,0,25); ?>

			  

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					  <?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>

                        <div class="icon-new-label new-left"><?php echo e(substr($product_discount_percentage,0,2)); ?> %</div><?php endif; ?>

                        <div class="pr-img-area"> 

						

						  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						  <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						  <?php endif; ?>

						  

						 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						  <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						  <?php endif; ?>

						  

						  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						  <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						  <?php endif; ?>

						  

						   <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

						  <a href="<?php echo e(url('productview/'.$mcat.'/'.$res)); ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						  <?php endif; ?>

						  

					  </div>

					  

                       

                      </div>

                      <div class="item-info">

                      	

                      	 <?php					  

							  $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_product->pro_id)->where('ratings', '=', 1)->count();

							  $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_product->pro_id)->where('ratings', '=', 2)->count();

							  $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_product->pro_id)->where('ratings', '=', 3)->count();

							  $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_product->pro_id)->where('ratings', '=', 4)->count();

							  $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_product->pro_id)->where('ratings', '=', 5)->count();

							  

							  

							  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

							  $multiple_countone = $one_count *1;

							  $multiple_counttwo = $two_count *2;

							  $multiple_countthree = $three_count *3;

							  $multiple_countfour = $four_count *4;

							  $multiple_countfive = $five_count *5;

							  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

                        <div class="info-inner">

                          <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html"><?php echo e(substr($fetch_product->$pro_title,0,25)); ?> </a> </div>

                          <div class="item-content">

                            <div class="rating"> 

                            	 <?php if($product_count): ?>

						 <?php   $product_divide_count = $product_total_count / $product_count; ?>

						 <?php if($product_divide_count <= '1'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '1'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '2'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

						 <?php elseif($product_divide_count >= '3'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '4'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '5'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

						 <?php else: ?>

							

						<?php endif; ?>

					<?php else: ?>

						 <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					<?php endif; ?>	

                            </div>

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_product->pro_disprice); ?></span> </span> </div>

                            </div>

							

							

                            <div class="pro-action">

                         

							   <?php if($fetch_product->pro_no_of_purchase < $fetch_product->pro_qty): ?>

								   

							    <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  

								<a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							    <?php endif; ?>

								

								<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

								<a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							   <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

								<a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							   <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

								<a href="<?php echo e(url('productview/'.$mcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							 <?php endif; ?> 



							<?php if($fetch_product->pro_no_of_purchase >= $fetch_product->pro_qty): ?>

							<button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span></button>

						   <?php endif; ?>

							   

                            </div>

							

							

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

              <?php if( $i % 3 == 0 ): ?> 

			  <?php endif; ?>

			   <?php $i++; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				

                </ul> 

			   </div>



			  

			  </div>

			   <?php else: ?>

				   <div class="page-title">

				    <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_PRODUCTS')); ?> <?php endif; ?></h2>

				   </div>

				  <center><h4><?php echo e((Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?></h4></center> 

				

			  <?php endif; ?>

			   

           <!-- <div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

         

		  

		  <!--Deal-->

		<?php if(count($searchTermss) > 0 ): ?>  

		  <div class="shop-inner">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_DEALS')); ?> <?php endif; ?></h2>

            </div>

           

            <div class="product-grid-area">

              <ul class="products-grid clearfix ">

			  <?php $i = 2; ?>

			<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

			 <?php   $deal_title = 'deal_title'; ?>

			<?php else: ?> <?php  $deal_title = 'deal_title_'.Session::get('lang_code'); ?> <?php endif; ?>
			<?php $__currentLoopData = $searchTermss; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

			 <?php $product_saving_price = $fetch_product->deal_original_price - $fetch_product->deal_original_price;



          $product_discount_percentage = round(($product_saving_price/ $fetch_product->deal_discount_price)*100,2);



          $product_img = explode('/**/', $fetch_product->deal_image);



          $mcat = strtolower(str_replace(' ','-',$fetch_product->mc_name));

          $smcat = strtolower(str_replace(' ','-',$fetch_product->smc_name));

          $sbcat = strtolower(str_replace(' ','-',$fetch_product->sb_name));

          $ssbcat = strtolower(str_replace(' ','-',$fetch_product->ssb_name)); 

          $res = base64_encode($fetch_product->deal_id);



          $product_image     = $product_img[0];

          

          $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

          $img_data   = "public/assets/deals/".$product_image; ?>

          

          <?php if(file_exists($img_data) && $product_image !=''): ?>   

          

       <?php   $prod_path = url('').'/public/assets/deals/' .$product_image; ?>

          <?php else: ?>  

              <?php if(isset($DynamicNoImage['dealImg'])): ?>

              <?php    $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

                  <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

                 <?php  $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

              <?php endif; ?>

          <?php endif; ?>   

          

        <?php  $alt_text   = substr($fetch_product->$deal_title,0,25);

          $alt_text  .= strlen($fetch_product->$deal_title)>25?'..':''; ?>

			  

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					   <?php if($fetch_product->deal_discount_percentage!='' && round($fetch_product->deal_discount_percentage)!=0): ?>

                        <div class="icon-new-label new-left">

						<?php echo e(substr($fetch_product->deal_discount_percentage,0,2)); ?>%

						</div><?php endif; ?>

                        <div class="pr-img-area"> 

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?> 

						

					   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						

					    <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						

						<?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						

						

						</div>

                        

                      </div>

                      <div class="item-info">

                      	 <?php

						  

						  $one_count = DB::table('nm_review')->where('deal_id', '=', $fetch_product->deal_id)->where('ratings', '=', 1)->count();

						  $two_count = DB::table('nm_review')->where('deal_id', '=', $fetch_product->deal_id)->where('ratings', '=', 2)->count();

						  $three_count = DB::table('nm_review')->where('deal_id', '=', $fetch_product->deal_id)->where('ratings', '=', 3)->count();

						  $four_count = DB::table('nm_review')->where('deal_id', '=', $fetch_product->deal_id)->where('ratings', '=', 4)->count();

						  $five_count = DB::table('nm_review')->where('deal_id', '=', $fetch_product->deal_id)->where('ratings', '=', 5)->count();

						  

						  

						  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

						  $multiple_countone = $one_count *1;

						  $multiple_counttwo = $two_count *2;

						  $multiple_countthree = $three_count *3;

						  $multiple_countfour = $four_count *4;

						  $multiple_countfive = $five_count *5;

						  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

                        <div class="info-inner">

                          <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html"><?php echo e(substr($fetch_product->$deal_title,0,25)); ?></a> </div>

                          <div class="item-content">

                            <div class="rating"> 

                            	 <?php if($product_count): ?>

						 <?php   $product_divide_count = $product_total_count / $product_count; ?>

						 <?php if($product_divide_count <= '1'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '1'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '2'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

						 <?php elseif($product_divide_count >= '3'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '4'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

						 <?php elseif($product_divide_count >= '5'): ?> 

						 

						 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

						 <?php else: ?>

							

						<?php endif; ?>

					<?php else: ?>

						 <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					<?php endif; ?>	

                             </div>

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_product->deal_discount_price); ?></span> </span> </div>

                            </div>

                            <div class="pro-action">

							  

							   <?php if($fetch_product->deal_no_of_purchase < $fetch_product->deal_max_limit): ?> 

								   

							   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

								<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

								<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							   <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

								<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							   <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

								<a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

						   <?php endif; ?>	   

						 

						 	<?php if($fetch_product->deal_no_of_purchase >= $fetch_product->deal_max_limit): ?> 

							<button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span></button> 

						    <?php endif; ?>

								

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

              <?php

 

   if( $i % 3 == 0 ){

   } ?>

    <?php  $i++; ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

     	

              </ul> </div> </div>

			  <?php else: ?>

				   <div class="sidebar-bar-title">

					  <h3><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_DEALS')); ?> <?php endif; ?></h3>

					</div>

					<?php echo "<center><h4>No Deals available with given search criteria</h4></center> "; ?>

					 <?php endif; ?>

			

            </div>

            

           <!-- <div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

         

		 

      

		

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

			<div class="block-content">

              <p class="block-subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES')); ?></p>

               <?php if(count($main_category)!=0): ?>

				<div id="divMenu">

					<ul>

					  <?php if(count($main_category)>0): ?>

						  <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

						<?php $check_main_product_exists = DB::table('nm_product')->where('pro_mc_id', '=', $fetch_main_cat->mc_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

							<?php if($check_main_product_exists): ?>

						<?php  $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>			

						<?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0): ?>  

						<li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1)); ?>">

							 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

								<?php $mc_name = 'mc_name'; ?>

								  <?php else: ?> <?php  $mc_name = 'mc_name_'.Session::get('lang_code'); ?> <?php endif; ?>

												

								 <?php echo e($fetch_main_cat->$mc_name); ?> </a>

						   <ul>

							<?php $__currentLoopData = $sub_main_category[$fetch_main_cat->mc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

							  <?php  $check_sub_product_exists = DB::table('nm_product')->where('pro_smc_id', '=', $fetch_sub_main_cat->smc_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

							  <?php if($check_sub_product_exists): ?>

							  <?php  $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>

								<li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2)); ?>"> 

								 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

									<?php $smc_name = 'smc_name'; ?>

									   <?php else: ?> <?php  $smc_name = 'smc_name_'.Session::get('lang_code'); ?> <?php endif; ?>

									   <?php echo e($fetch_sub_main_cat->$smc_name); ?></a>

										

									<?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0): ?>  

									 <ul>

									   <?php $__currentLoopData = $second_main_category[$fetch_sub_main_cat->smc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

										<?php $check_sec_main_product_exists = DB::table('nm_product')->where('pro_sb_id', '=', $fetch_sub_cat->sb_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

										<?php if($check_sec_main_product_exists): ?>

									   <?php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; ?>

										<li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3)); ?>"> 

										 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

											<?php $sb_name = 'sb_name'; ?>

										  <?php else: ?> <?php  $sb_name = 'sb_name_'.Session::get('lang_code'); ?> <?php endif; ?>

										  <?php echo e($fetch_sub_cat->$sb_name); ?> </a>

										   <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0): ?>  

											<ul>	

												<?php $__currentLoopData = $second_sub_main_category[$fetch_sub_cat->sb_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_secsub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

												<?php $check_sec_sub_main_product_exists = DB::table('nm_product')->where('pro_ssb_id', '=', $fetch_secsub_cat->ssb_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

												<?php if($check_sec_sub_main_product_exists): ?>

											  <?php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?> 

												<li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4)); ?>"> 

												<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

												<?php $ssb_name = 'ssb_name'; ?>

												  <?php else: ?> <?php  $ssb_name = 'ssb_name_'.Session::get('lang_code'); ?> <?php endif; ?>

												<?php echo e($fetch_secsub_cat->$ssb_name); ?></a>

												</li>

												 <?php endif; ?>

													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

											</ul>

											 <?php endif; ?>

										</li>

										<?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									</ul>

									 <?php endif; ?>

								</li>

								 <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						   </ul>

						   <?php endif; ?>

						</li>

						<?php endif; ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?> <?php endif; ?>

					</ul>

					

				</div>

            </div>

			<div class="clearfix"></div>

			<br/>

		  

		  

         

		<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?> 

        <?php $pro_title = 'pro_title'; ?>

        <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

		

		 <?php if(count($most_visited_product)>0): ?> 

          <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3><?php if(Lang::has(Session::get('lang_file').'.MOST_VISITED_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MOST_VISITED_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MOST_VISITED_PRODUCT')); ?> <?php endif; ?></h3>

            </div>

            <div class="block-content">

              <ul>

			    <?php $__currentLoopData = $most_visited_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                <?php $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

                $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

                $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);



                $product_img  = $mostproduct_img[0]; 

                

                $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                $img_data   = "public/assets/product/".$product_img; ?>

                

                <?php if(file_exists($img_data) && $product_img !=''): ?>   

                

              <?php $prod_path = url('').'/public/assets/product/' .$product_img;                  ?>

                <?php else: ?>  

                    <?php if(isset($DynamicNoImage['productImg'])): ?>

                      <?php  $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; ?>

                       <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

                        <?php  $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

                    <?php endif; ?>

                <?php endif; ?>   

                

              <?php  $alt_text  = substr($fetch_most_visit_pro->$pro_title,0,25); ?>





            <?php if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty): ?> 

				

                <li class="item">

                  <div class="products-block-left">			 

				  <a class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a>

				  </div>

				  

                  <div class="products-block-right">

                    <p class="product-name"> <a><?php echo e(substr($fetch_most_visit_pro->$pro_title,0,25)); ?>


                     <?php echo e(strlen($fetch_most_visit_pro->$pro_title)>25?'..':''); ?></a> </p>

                    <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_most_visit_pro->pro_disprice); ?></span>

                  



				  <?php					  

				  $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 5)->count();

				  

				  

				  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

				  $multiple_countone = $one_count *1;

				  $multiple_counttwo = $two_count *2;

				  $multiple_countthree = $three_count *3;

				  $multiple_countfour = $four_count *4;

				  $multiple_countfive = $five_count *5;

				  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>



				  <div class="rating">

				<?php if($product_count): ?>

					 <?php   $product_divide_count = $product_total_count / $product_count; ?>

					 <?php if($product_divide_count <= '1'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '1'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '2'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

					 <?php elseif($product_divide_count >= '3'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '4'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '5'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

					 <?php else: ?>

						

					<?php endif; ?>

				<?php else: ?>

				     <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

				<?php endif; ?>	

			  </div>

				  <br>

				 <?php if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty): ?> 

					 

				 <h4 style="text-align:center;"><a  class="btn btn-danger"><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></a> 

                       <?php else: ?>   

					<?php $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

					$smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

					$sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

					$ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

					$res = base64_encode($fetch_most_visit_pro->pro_id); ?>
					 

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

					<a class="link-all" href="<?php echo url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

					<a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>

					<a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					<?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

					<a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					</div>

                </li>

				 <?php endif; ?>

				<?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

              </ul>

              </div>

          </div>       

   

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <script type="text/javascript">

  $(document).ready(function(){

    $(document).on("click", ".customCategories .topfirst b", function(){

      $(this).next("ul").css("position", "relative");

      

      $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

       $(this).next("ul").toggle();

    });



    $(document).on("click", ".morePage", function(){

      $(".nextPage").slideToggle(200);

    });

    

    $(document).on("click", "#smallScreen", function(){

      $(this).toggleClass("customMenu");

    });



    $(window).scroll(function () {

      if ($(this).scrollTop() > 250) {

        $('#comp_myprod').show();

      }

      else{

        $('#comp_myprod').hide();

      }

    });





  });



  </script>

  

  <?php echo $footer; ?>