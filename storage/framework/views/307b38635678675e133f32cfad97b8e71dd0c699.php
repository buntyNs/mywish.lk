<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9">
      <![endif]-->
      <!--[if !IE]><!--> 
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <meta charset="UTF-8" />
            <title><?php echo e($SITENAME); ?> | Product details</title>
            <meta content="width=device-width, initial-scale=1.0" name="viewport" />
            <meta content="" name="description" />
            <meta content="" name="author" />
            <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
            <!-- GLOBAL STYLES -->
            <!-- GLOBAL STYLES -->
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
            <?php 
            $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
            <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
            <?php endif; ?>      
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
            <!--END GLOBAL STYLES -->
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->
         </head>
         <!-- END HEAD -->
         <!-- BEGIN BODY -->
         <body class="padTop53 " >
            <!-- MAIN WRAPPER -->
            <div id="wrap">
               <!-- HEADER SECTION -->
               <?php echo $adminheader; ?>

               <!-- END HEADER SECTION -->
               <!-- MENU SECTION -->
               <?php echo $adminleftmenus; ?>

               <!--END MENU SECTION -->
               <div></div>
               <!--PAGE CONTENT -->
               <div id="content">
                  <div class="inner">
                     <div class="row">
                        <div class="col-lg-12">
                           <ul class="breadcrumb">
                              <li class=""><a >Home</a></li>
                              <li class="active"><a >Product details</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="box dark">
                              <header>
                                 <div class="icons"><i class="icon-edit"></i></div>
                                 <h5>Product details</h5>
                              </header>
                              <?php $__currentLoopData = $product_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              <?php
                                 $title          = $products->pro_title;
                                 $title_fr       = $products->pro_title_fr;
                                              $category_get  = $products->pro_mc_id;
                                        $maincategory    = $products->pro_smc_id;
                                 $subcategory    = $products->pro_sb_id;
                                 $secondsubcategory= $products->pro_ssb_id;
                                 $originalprice  = $products->pro_price;
                                 $discountprice  = $products->pro_disprice;
                                 $inctax=$products->pro_inctax;
                                 $shippingamt=$products->pro_shippamt;
                                 $description    = $products->pro_desc;
                                 $description_fr     = $products->pro_desc_fr;
                                 $deliverydays=$products->pro_delivery;
                                 $metakeyword    = $products->pro_mkeywords;
                                 $metakeyword_fr     = $products->pro_mkeywords_fr;
                                 $metadescription= $products->pro_mdesc;
                                 $metadescription_fr= $products->pro_mdesc_fr;
                                    $file_get  = $products->pro_Img;
                                      $file_get_path =  explode("/**/",$file_get,-1); 
                                 $img_count      = $products->pro_image_count;
                                 
                                 
                                 
                                 
                                 ?>
                              <div class="row">
                                 <div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                                    <?php echo e(Form::open()); ?>

                                    <div class="panel panel-default">
                                       <div class="panel-heading">
                                          Product details
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Product Title<span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($title); ?> <!--(<?php echo $title_fr ; ?>)-->
                                             </div>
                                          </div>
                                       </div>
                                        <?php if(!empty($get_active_lang)): ?>  
                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                       <?php
                                          $get_lang_name = $get_lang->lang_name;
                                          $title = 'pro_title_'.$get_lang->lang_code; 
                                          $title_dynamic = $products->$title; ?>
                                       <?php if($title_dynamic !=''): ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Product Title <?php echo e($get_lang_name); ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($title_dynamic); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Top Category<span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->mc_name); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Main Category<span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->smc_name); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Sub Category<span class="text-sub"></span></label>
                                             <div class="col-lg-4">
                                                <?php if($products->sb_name!=""): ?>
                                                <?php echo e($products->sb_name); ?> <?php else: ?> <?php echo e("-"); ?><?php endif; ?>
                                                
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Second Sub Category<span class="text-sub"></span></label>
                                             <div class="col-lg-4">
                                                <?php if($products->ssb_name!=""): ?><?php echo e($products->ssb_name); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?>
                                                
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Product Quantity<span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e($products->pro_qty); ?> 
                                             </div>
                                          </div>
                                       </div>
                                     <!--   <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SKU_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SKU_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SKU_NUMBER')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e($products->pro_sku_number); ?> 
                                             </div>
                                          </div>
                                       </div> -->
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Original Price<span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Helper::cur_sym()); ?> <?php echo e($products->pro_price); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Discount Price<span class="text-sub">*</span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Helper::cur_sym()); ?> <?php echo e($products->pro_disprice); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Tax Percentage</label>
                                             <div class="col-lg-4">
                                                <?php echo e($products->pro_inctax); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Shipping Amount<span class="text-sub"></span></label>
                                             <div class="col-lg-4">
                                                <?php echo e(Helper::cur_sym()); ?> <?php echo e($products->pro_shippamt); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Description<span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo $description; ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <?php if(!empty($get_active_lang)): ?>  
                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                       <?php
                                          $get_lang_name = $get_lang->lang_name;
                                          $description_dynamic = 'pro_desc_'.$get_lang->lang_code; 
                                          $descrip_dynamic = $products->$description_dynamic; ?>
                                       <?php if($descrip_dynamic !=''): ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Description(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo $descrip_dynamic; ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Specification<span class="text-sub"></span></label>
                                             <div class="col-lg-10">
                                                <?php if(count($product_spec_details)!=0): ?>
                                                <table class="table table-bordered">
                                                   <tbody>
                                                      <tr>
                                                         <th colspan="2"><?php echo e($product_spec_details[0]->spg_name); ?> 
                                                            <?php if(!empty($get_active_lang)): ?>  
                                                            <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                            <?php
                                                               $get_lang_name = $get_lang->lang_name;
                                                               $specification_dynamic = 'spg_name_'.$get_lang->lang_code; 
                                                               $specific_dynamic = $product_spec_details[0]->$specification_dynamic; ?>
                                                            <?php if($specific_dynamic !=''): ?>
                                                            <?php echo e('('.$specific_dynamic.')'); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                         </th>
                                                      </tr>
                                                      <?php $__currentLoopData = $product_spec_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      <tr>
                                                         <td><?php echo e($spec->sp_name); ?> 
                                                            <?php if(!empty($get_active_lang)): ?>  
                                                            <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                            <?php       $get_lang_name = $get_lang->lang_name;
                                                               $specification_name = 'sp_name_'.$get_lang->lang_code; 
                                                               $specific_name = $spec->$specification_name;  ?>
                                                            <?php if($specific_name !=''): ?>
                                                            <?php echo e('('.$specific_name.')'); ?> 
                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                         </td>
                                                         <td><?php echo e($spec->spc_value); ?> 
                                                            <?php if(!empty($get_active_lang)): ?>  
                                                            <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                            <?php
                                                               $get_lang_name = $get_lang->lang_name;
                                                               $specification_value = 'spc_value_'.$get_lang->lang_code; 
                                                               $specific_value = $spec->$specification_value; ?>
                                                            <?php if($specific_value !=''): ?> 
                                                            <?php echo e('('.$specific_value.')'); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                         </td>
                                                      </tr>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                   </tbody>
                                                </table>
                                                <?php else: ?> <?php echo e('No specification List avaliable for this particular Category '); ?> 
                                                <?php endif; ?>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Product Color<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php if(count($product_color_details) > 0): ?> 
                                                <?php $__currentLoopData = $product_color_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                <?php echo e($product_color_det->co_name.","); ?>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php else: ?> <?php echo e('-'); ?> <?php endif; ?>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Product Size<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php if(count($product_size_details)!=0): ?>  
                                                <?php 
                                                   $size_name = $product_size_details[0]->si_name;
                                                   $return  = strcmp($size_name,'no size'); ?>
                                                <?php if($return!=0): ?>
                                                <?php $__currentLoopData = $product_size_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                <?php echo e($product_size_det->si_name.','); ?>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                <?php else: ?> <?php echo e('-'); ?> <?php endif; ?>
                                                <?php endif; ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Delivery Days<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php echo e($products->pro_delivery); ?> Days 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Merchant Name<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php echo e($products->mer_fname.' '.$products->mer_lname); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Store Name<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php echo e($products->stor_name); ?>

                                                <?php if(!empty($get_active_lang)): ?> 
                                                <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                <?php 
                                                   $store_dynamic = 'stor_name_'.$get_lang->lang_code; 
                                                   $stor_dynamic = $products->$store_dynamic; ?>
                                                <?php if($stor_dynamic !=''): ?> <?php echo e('('.$stor_dynamic.')'); ?> <?php endif; ?> 
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Meta Keywords</label>
                                             <div class="col-lg-8">
                                                <?php if($products->pro_mkeywords!=""): ?>
                                                <?php echo e($products->pro_mkeywords); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <?php if(!empty($get_active_lang)): ?>  
                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                       <?php
                                         $get_lang_name = $get_lang->lang_name;
                                          $meta_key_dynamic = 'pro_mkeywords_'.$get_lang->lang_code; 
                                          $meta_keyword_dyn = $products->$meta_key_dynamic; ?>
                                       <?php if($meta_keyword_dyn !=''): ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Meta Keywords(<?php echo e($get_lang_name); ?>)</label>
                                             <div class="col-lg-8">
                                                <?php if($products->$meta_key_dynamic!=""): ?><?php echo e($meta_keyword_dyn); ?> <?php else: ?> <?php echo e("-"); ?>   <?php endif; ?>
                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?> 
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Meta Description</label>
                                             <div class="col-lg-8">
                                                <?php if($products->pro_mdesc!=""): ?><?php echo e($products->pro_mdesc); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <?php if(!empty($get_active_lang)): ?>  
                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                       <?php
                                        $get_lang_name = $get_lang->lang_name;
                                          $meta_desc_dynamic = 'pro_mdesc_'.$get_lang->lang_code; 
                                          $meta_desc_dyn = $products->$meta_desc_dynamic; ?>
                                       <?php if($meta_desc_dyn !=''): ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Meta Description1 (<?php echo e($get_lang_name); ?>)</label>
                                             <div class="col-lg-8">
                                                <?php if($meta_desc_dyn!=""): ?><?php echo e($meta_desc_dyn); ?> <?php else: ?> <?php echo e("-"); ?> <?php endif; ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Cash Back<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php echo e(Helper::cur_sym()); ?> <?php echo e($products->cash_pack); ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Apply Cancellation Policy<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php if($products->allow_cancel == 1 ) { echo "Yes" ; } else { echo "No"; } ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <?php if($products->allow_cancel == 1 ): ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Cancellation Policy <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->cancel_policy); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php if($products->allow_cancel == 1 ): ?> <?php if(!empty($get_active_lang)): ?> 
                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <?php
                                          $get_lang_name = $get_lang->lang_name;
                                          $cancel_policy_dynamic = 'cancel_policy_'.$get_lang->lang_code; 
                                          $descrip_cancel_policy = $products->$cancel_policy_dynamic; ?>
                                       <?php if($descrip_cancel_policy !=''): ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> Cancellation Policy(<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($descrip_cancel_policy); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                       <?php endif; ?>
                                       <?php if($products->allow_cancel == 1 ): ?>  
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> No of days Cancellation Applicable <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->cancel_days); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Apply Return/Refund Policy<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php if($products->allow_return == 1 ): ?> <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <?php if($products->allow_return == 1 ): ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Return Policy <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->return_policy); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php if($products->allow_return == 1 ): ?>  <?php if(!empty($get_active_lang)): ?> 
                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                       <?php 
                                          $get_lang_name = $get_lang->lang_name;
                                          $return_policy_dynamic = 'return_policy_'.$get_lang->lang_code; 
                                          $return_policy = $products->$return_policy_dynamic;  ?>
                                       <?php if($return_policy !=''): ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Return Policy(<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($return_policy); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                       <?php endif; ?>
                                       <?php if($products->allow_return == 1 ): ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> No of days Return Applicable <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->return_days); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Apply Replacement Policy<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php if($products->allow_replace == 1 ): ?> <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>  
                                             </div>
                                          </div>
                                       </div>
                                       <?php if($products->allow_replace == 1 ): ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Replacement Policy <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->replace_policy); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>

                                       <?php if($products->allow_replace == 1 ): ?>  <?php if(!empty($get_active_lang)): ?>  

                                       <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                       <?php 
                                          $get_lang_name = $get_lang->lang_name;
                                          $replace_policy_dynamic = 'replace_policy_'.$get_lang->lang_code; 
                                          $replace_policy = $products->$replace_policy_dynamic; ?>
                                       <?php if($replace_policy !=''): ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Replacement Policy (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($replace_policy); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                       <?php endif; ?>
                                      
                                       <?php if($products->allow_replace == 1 ): ?> 
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1"> No of days Replacement Applicable<span class="text-sub">*</span></label>
                                             <div class="col-lg-10">
                                                <?php echo e($products->replace_days); ?> 
                                             </div>
                                          </div>
                                       </div>
                                       <?php endif; ?>
                                       <div class="panel-body">
                                          <div class="form-group">
                                             <label class="control-label col-lg-2" for="text1">Product Image<span class="text-sub">*</span></label>
                                             <div class="col-lg-8">
                                                <?php $img_count = count($file_get_path); ?>
                                                <!-- Image path starts -->
                                                <?php if(count($file_get_path) > 0  && $img_count != ''): ?> 
                                                <?php for($j=0 ; $j< $img_count; $j++): ?>
                                                <?php if($file_get_path[$j] !=''): ?> 
                                                <?php
                                                   $pro_img = $file_get_path[$j];
                                                   $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                                                   $img_data = "public/assets/product/".$pro_img; ?>
                                                <?php if(file_exists($img_data)): ?>  
                                                <?php 
                                                $prod_path = url('').'/public/assets/product/'.$pro_img; ?>
                                                <?php else: ?>  
                                                <?php if(isset($DynamicNoImage['productImg'])): ?>
                                                <?php $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['productImg']; ?>
                                                <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['productImg'] !=''): ?>
                                                <?php 
                                                $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg']; 
                                                ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php else: ?>
                                                <?php $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
                                                <?php if(isset($DynamicNoImage['productImg'])): ?>
                                                <?php $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['productImg']; ?>
                                                <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['productImg'] !=''): ?>
                                                <?php 
                                                $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg']; 
                                                ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <img style="height:70px; width:50px;" src="<?php echo e($prod_path); ?>">
                                                <?php endfor; ?>
                                                <?php else: ?>
                                                <?php  $prod_path = url('').'/public/assets/default_image/No_image_product.png';  ?>
                                                <?php if(isset($DynamicNoImage['productImg'])): ?>
                                                <?php $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['productImg']; ?>
                                                <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['productImg'] !=''): ?>
                                                <?php 
                                                $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg']; 
                                                ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <img style="height:70px; width:50px;" src="<?php echo e($prod_path); ?>">
                                                <?php endif; ?>
                                                <!-- Image path ends -->    
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>
                                       <div class="col-lg-2">
                                          <a style="color:#fff" href="<?php echo e(URL::previous()); ?>" class="btn btn-success btn-sm btn-grad">Back</a>
                                       </div>
                                    </div>
                                    <?php echo e(Form::close()); ?>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--END PAGE CONTENT -->
            </div>
            <!--END MAIN WRAPPER -->  
            <!-- FOOTER -->
            <?php echo $adminfooter; ?>

            <!--END FOOTER --> 
            <!-- GLOBAL SCRIPTS -->
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
            <!-- END GLOBAL SCRIPTS -->   
         </body>
         <!-- END BODY -->
      </html>