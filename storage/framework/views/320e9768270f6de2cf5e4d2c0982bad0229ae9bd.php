<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]--> 
<!-- mobile menu -->
<?php echo $navbar; ?>

<?php echo $header; ?> 
<div class="preloader-wrapper">
   <div class="preloader">
     <!--   <img src="http://prorabbi.website/nila/assets/images/preloader.gif" alt="NILA">-->
         </div>
   </div>
   
    <script>
 $(document).ready(function() {
 setTimeout(function(){
          $('.preloader-wrapper').fadeOut();
   }, 3);
  });
 </script>
<!-- Slideshow  -->

<div class="main-slider" id="home">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-sm-3 col-xs-12 banner-left hidden-xs">
            <br><br>
           
               <?php if(count($product_details) != 0): ?>
               <?php for($i = 0; $i < 1 ;$i++): ?> 
               <div class="jtv-banner3">
               
               <?php 
                  $mcat = strtolower(str_replace(' ','-',$product_details[0]->mc_name));
                  $smcat = strtolower(str_replace(' ','-',$product_details[0]->smc_name));
                  $sbcat = strtolower(str_replace(' ','-',$product_details[0]->sb_name));
                  $ssbcat = strtolower(str_replace(' ','-',$product_details[0]->ssb_name)); 
                  // product id  
                  $res = base64_encode($product_details[0]->pro_id);
                  //product image  
                  $product_image = explode('/**/',$product_details[0]->pro_Img);
                  //product price  
                  $product_saving_price = $product_details[0]->pro_price - $product_details[0]->pro_disprice;
                  // product dicount percentage  
                  $product_discount_percentage = round(($product_saving_price/ $product_details[0]->pro_price)*100,2); ?>
                
               <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
               <?php
               $title = 'pro_title';
               ?>
               <?php else: ?> 
               <?php  
               $title = 'pro_title_langCode'; 
               ?> 
               <?php endif; ?>
                 
               <?php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               $img_data = "public/assets/product/".$product_image[0];
               ?>
               <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
               <?php 
               $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
               ?>       
               <?php else: ?>  
               <?php if(isset($DynamicNoImage['productImg'])): ?>
               <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
               <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
               <?php
               $prod_path = url('')."/".$dyanamicNoImg_path;
               ?> 
               <?php endif; ?>
               <?php endif; ?>
               <?php endif; ?>
               <?php $discount_percent = $alt_text = ''; ?>
               
               <?php 
               $alt_text   = substr($product_details[0]->$title,0,25);
               $alt_text  .= strlen($product_details[0]->$title)>25?'..':''; ?>
               
               <?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
               <?php
               $discount_percent = round($product_discount_percentage);
               ?>       
               <?php endif; ?>
               <div class="jtv-banner3-inner"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>">
               </div>
            </div>
           
               <br><br>
               <div class="hover_content">
                  <div class="hover_data">
                     <div class="title"><?php if(Lang::has(Session::get('lang_file').'.BIG_SALE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BIG_SALE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BIG_SALE')); ?> <?php endif; ?>
                     </div>
                     <div class="desc-text"><?php if(Lang::has(Session::get('lang_file').'.UP_TO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UP_TO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UP_TO')); ?> <?php endif; ?>
                        <?php echo e($discount_percent); ?> <?php if(Lang::has(Session::get('lang_file').'.OFF')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OFF')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OFF')); ?> <?php endif; ?>
                     </div>
                     <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                     <p class="big-sale"> <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                        <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                        </a>
                     </p>
                     <?php endif; ?> 
                     <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                     <p class="big-sale">   <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                        <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                        </a> 
                     </p>
                     <?php endif; ?> <!-- //if -->
                     <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 
                     <p class="big-sale"> <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                        <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                        </a> 
                     </p>
                     <?php endif; ?> 
                     <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>  
                     <p class="big-sale">  <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>" class="shop-now">
                        <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                        </a> 
                     </p>
                     <?php endif; ?>
                  </div>
               </div>
              
               <?php endfor; ?>
               <?php else: ?>
               <?php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               ?>
               <?php if(isset($DynamicNoImage['productImg'])): ?>
               <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
               <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
               <?php
               $prod_path = url('')."/".$dyanamicNoImg_path;
               ?> 
               <?php endif; ?>
               <?php endif; ?>
               <div>
               <img src="<?php echo e($prod_path); ?>" alt=""> </div>
               <?php endif; ?>

            </div>
            <div class="col-sm-9 col-md-9 col-lg-9 col-xs-12 jtv-slideshow">
               <div id="jtv-slideshow">
                  <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
                     <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                        <?php if(count($bannerimagedetails) > 0 ): ?>
                        <ul>
                           <?php $__currentLoopData = $bannerimagedetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php 
                              $pro_img = $banner->bn_img;
                              $prod_path = url('').'/public/assets/default_image/No_image_banner.png'; ?>
                           <?php   $img_data = "public/assets/bannerimage/".$pro_img; ?>
                           <?php if(file_exists($img_data) && $pro_img != ''): ?>
                           <?php     $prod_path = url('').'/public/assets/bannerimage/'.$pro_img; ?>
                           <?php else: ?>
                           <?php if(isset($DynamicNoImage['banner'])): ?> 
                           <?php               
                              $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['banner']; ?>
                           <?php if($DynamicNoImage['banner'] !='' && file_exists($dyanamicNoImg_path)): ?>
                           <?php $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['banner']; ?>
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php endif; ?>
                           <li data-transition='fade' data-slotamount='7' data-masterspeed='1000' data-thumb=''>
                             <a href="<?php echo e($banner->bn_redirecturl); ?>" target="new"> <img src="<?php echo e($prod_path); ?>" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/></a>
                              <div class="caption-inner">
                                 <div class='tp-caption LargeTitle sft  tp-resizeme' data-x='250'  data-y='110'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'><?php if(Lang::has(Session::get('lang_file').'.ESHOP_BUSINESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ESHOP_BUSINESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ESHOP_BUSINESS')); ?> <?php endif; ?></div>
                                 <div class='tp-caption ExtraLargeTitle sft  tp-resizeme' data-x='200'  data-y='160'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap; color:#fff; text-shadow:none;'><?php if(Lang::has(Session::get('lang_file').'.EASY_BUY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EASY_BUY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EASY_BUY')); ?> <?php endif; ?></div>
                                 <div class='tp-caption' data-x='310'  data-y='230'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap; color:#f8f8f8;'> </div>
                                 <div class='tp-caption sfb  tp-resizeme ' data-x='370'  data-y='280'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='<?php echo url('products'); ?>' class="buy-btn"><?php if(Lang::has(Session::get('lang_file').'.GET_START')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_START')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_START')); ?> <?php endif; ?></a> </div>
                              </div>
                           </li>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                        <?php else: ?>
                        <img src="<?php echo e(url('')); ?>/public/assets/noimage/No_image_1509364387_870x350.png " data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                        <?php endif; ?>
                        <div class="tp-bannertimer"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- service section -->
   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <!-- All products-->
   <div class="container">
      <div class="home-tab">
         <div class="tab-title text-left">
            <h2><?php if(Lang::has(Session::get('lang_file').'.BEST_SELLING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BEST_SELLING')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BEST_SELLING')); ?> <?php endif; ?></h2>
            
         </div>
         <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="computer">
               <div class="featured-pro">
                  <div class="slider-items-products">                    
                     <div id="computer-slider" class="product-flexslider hidden-buttons">
                        <div class="slider-items slider-width-col4">
                           <?php if(count($product_details) != 0): ?>
                           <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           
                           <?php 
                              $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                              $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                              $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                              $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                              // product id  
                              $res = base64_encode($product_det->pro_id);
                              //product image  
                              $product_image = explode('/**/',$product_det->pro_Img);
                              //product price  
                              $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                              // product dicount percentage  
                              $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
                            
                           <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
                           <?php
                           $title = 'pro_title';
                           ?>
                           <?php else: ?> 
                           <?php  
                           $title = 'pro_title_langCode'; 
                           ?> 
                           <?php endif; ?>
                             
                           <?php 
                           $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                           $img_data = "public/assets/product/".$product_image[0];
                           ?>
                           <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
                           <?php 
                           $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                           ?>       
                           <?php else: ?>  
                           <?php if(isset($DynamicNoImage['productImg'])): ?>
                           <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                           <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                           <?php
                           $prod_path = url('')."/".$dyanamicNoImg_path;
                           ?> 
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php $discount_percent = $alt_text = ''; ?>
                           
                           <?php 
                           $alt_text   = substr($product_det->$title,0,25);
                           $alt_text  .= strlen($product_det->$title)>25?'..':''; ?>
                           
                           <?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
                           <?php
                           $discount_percent = round($product_discount_percentage);
                           ?>       
                           <?php endif; ?>
                           <div class="product-item">
                              <div class="item-inner">
                                 <div class="product-thumbnail">
                                    <div class="icon-new-label new-left"><?php echo e($discount_percent); ?>%</div>
                                    <div class="pr-img-area">
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"> </figure>
                                       </a>
                                       <?php endif; ?> <!-- /*//if*/ --> 
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"></figure>
                                       </a>
                                       <?php endif; ?> <!-- //if -->
                                       <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"></figure>
                                       </a>
                                       <?php endif; ?> 
                                       <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo $alt_text; ?>"></figure>
                                       </a>
                                       <?php endif; ?>
                                    </div>
                                    <div class="pr-info-area">
                                       <div class="pr-button">
                                          <div class="mt-button add_to_wishlist"> 
                                             
                                             <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>
                                             <?php if(Session::has('customerid')): ?>
                                             <?php if(count($prodInWishlist)==0): ?>
                                             <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                             <?php echo e(Form::hidden('pro_id','$product_det->pro_id')); ?>        
                                             <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                             <a href="" onclick="addtowish(<?php echo e($product_det->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                             <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             <?php else: ?>
                                             <?php /* remove wishlist */?>   
                                             <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                             <i class="fa fa-heart" aria-hidden="true"></i> 
                                             </a> 
                                             <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                             <?php endif; ?>  
                                             <?php else: ?> 
                                             <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             <?php endif; ?>
                                          </div>
                                          <div class="mt-button quick-view" ><a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap<?php echo e($product_det->pro_id); ?>"> <i class="fa fa-search"></i> </a> </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item-info">
                                    <div class="info-inner">
                                       <div class="item-title">
                                          <a title="" >
                                             <?php echo e(substr($product_det->$title,0,25)); ?>

                                             <?php echo e(strlen($product_det->$title)>25?'..':''); ?> 
                                       </div>
                                       <div class="item-content">
                                       
                                       <div class="item-price">
                                       <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?></span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                       <?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty): ?>
                                       <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span> </button>
                                       <?php else: ?>  
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button> </a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button> </a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button> </a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button>
                                       </a>
                                       <?php endif; ?>
                                       <?php endif; ?>
                                       </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php elseif(count($product_details) == 0): ?>
                           <p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">
                              <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?> <?php endif; ?>
                           </p>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="inner-box">
      <div class="container">
         <div class="row">
            <!-- Banner -->
            <div class="col-md-3 top-banner hidden-sm">
               <div class="jtv-banner3">
                  <?php if(count($product_details) != 0 && isset($product_details[1]) ): ?>
                  <?php for($i = 0; $i < 1 ;$i++): ?> 
                  
                  <?php 
                     $mcat = strtolower(str_replace(' ','-',$product_details[1]->mc_name));
                     $smcat = strtolower(str_replace(' ','-',$product_details[1]->smc_name));
                     $sbcat = strtolower(str_replace(' ','-',$product_details[1]->sb_name));
                     $ssbcat = strtolower(str_replace(' ','-',$product_details[1]->ssb_name)); 
                     // product id  
                     $res = base64_encode($product_details[1]->pro_id);
                     //product image  
                     $product_image = explode('/**/',$product_details[1]->pro_Img);
                     //product price  
                     $product_saving_price = $product_details[1]->pro_price - $product_details[1]->pro_disprice;
                     // product dicount percentage  
                     $product_discount_percentage = round(($product_saving_price/ $product_details[1]->pro_price)*100,2); ?>
                   
                  <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
                  <?php
                  $title = 'pro_title';
                  ?>
                  <?php else: ?> 
                  <?php  
                  $title = 'pro_title_langCode'; 
                  ?> 
                  <?php endif; ?>
                    
                  <?php 
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                  $img_data = "public/assets/product/".$product_image[0];
                  ?>
                  <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
                  <?php 
                  $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                  ?>       
                  <?php else: ?>  
                  <?php if(isset($DynamicNoImage['productImg'])): ?>
                  <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                  <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                  <?php
                  $prod_path = url('')."/".$dyanamicNoImg_path;
                  ?> 
                  <?php endif; ?>
                  <?php endif; ?>
                  <?php endif; ?>
                  <?php $discount_percent = $alt_text = ''; ?>
                  
                  <?php 
                  $alt_text   = substr($product_details[1]->$title,0,25);
                  $alt_text  .= strlen($product_details[1]->$title)>25?'..':''; ?>
                  
                  <?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
                  <?php
                  $discount_percent = round($product_discount_percentage);
                  ?>       
                  <?php endif; ?>
                  <div class="jtv-banner3-inner">
                     <img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>">
                     <div class="hover_content">
                        <div class="hover_data">
                           <div class="title"><?php if(Lang::has(Session::get('lang_file').'.BIG_SALE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BIG_SALE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BIG_SALE')); ?> <?php endif; ?>
                           </div>
                           <div class="desc-text"><?php if(Lang::has(Session::get('lang_file').'.UP_TO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UP_TO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UP_TO')); ?> <?php endif; ?>
                              <?php echo e($discount_percent); ?> <?php if(Lang::has(Session::get('lang_file').'.OFF')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OFF')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OFF')); ?> <?php endif; ?>
                           </div>
                           <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                           <p> <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a>
                           </p>
                           <?php endif; ?> <!-- /*//if*/ --> 
                           <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                           <p>   <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a> 
                           </p>
                           <?php endif; ?> <!-- //if -->
                           <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                           <p> <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a> 
                           </p>
                           <?php endif; ?> 
                           <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>
                           <p>  <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a> 
                           </p>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
                  <?php endfor; ?>
                  <?php else: ?>
                  <?php 
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                  ?>
                  <?php if(isset($DynamicNoImage['productImg'])): ?>
                  <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                  <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                  <?php
                  $prod_path = url('')."/".$dyanamicNoImg_path;
                  ?> 
                  <?php endif; ?>
                  <?php endif; ?>
                  <div class="jtv-banner3-inner"><img src="<?php echo e($prod_path); ?>" alt=""></div>
                  <?php endif; ?>
               </div>
            </div>
            <!-- Best Sale -->
            <div class="col-sm-12 col-md-9 jtv-best-sale special-pro">
               <div class="jtv-best-sale-list">
                  <div class="wpb_wrapper">
                     <div class="best-title text-left">
                        <h2><?php if(Lang::has(Session::get('lang_file').'.SPECIAL_OFFERS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SPECIAL_OFFERS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SPECIAL_OFFERS')); ?> <?php endif; ?></h2>
                     </div>
                  </div>
                  <div class="slider-items-products">
                     <div id="jtv-best-sale-slider" class="product-flexslider">
                        <div class="slider-items">
                           <?php if(count($product_details) != 0): ?>
                           <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           
                           <?php 
                              $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                              $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                              $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                              $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                              // product id  
                              $res = base64_encode($product_det->pro_id);
                              //product image  
                              $product_image = explode('/**/',$product_det->pro_Img);
                              //product price  
                              $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                              // product dicount percentage  
                              $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
                           <?php if($product_discount_percentage<= 50): ?>
                           <?php if($product_det->pro_no_of_purchase < $product_det->pro_qty): ?> 
                            
                           <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
                           <?php
                           $title = 'pro_title';
                           ?>
                           <?php else: ?> 
                           <?php  
                           $title = 'pro_title_langCode'; 
                           ?> 
                           <?php endif; ?>
                             
                           <?php 
                           $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                           $img_data = "public/assets/product/".$product_image[0];
                           ?>
                           <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
                           <?php 
                           $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                           ?>       
                           <?php else: ?>  
                           <?php if(isset($DynamicNoImage['productImg'])): ?>
                           <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                           <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                           <?php
                           $prod_path = url('')."/".$dyanamicNoImg_path;
                           ?> 
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php $discount_percent = $alt_text = ''; ?>
                           
                           <?php 
                           $alt_text   = substr($product_det->$title,0,25);
                           $alt_text  .= strlen($product_det->$title)>25?'..':''; ?>
                           
                           <?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
                           <?php
                           $discount_percent = round($product_discount_percentage);
                           ?>       
                           <?php endif; ?>
                           <div class="product-item">
                              <div class="item-inner">
                                 <div class="product-thumbnail">
                                    <div class="icon-new-label new-left"><?php echo e($discount_percent); ?>%</div>
                                    <div class="pr-img-area">
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo e($alt_text); ?>"></figure>
                                       </a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo e($alt_text); ?>"> </figure>
                                       </a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo e($alt_text); ?>"> </figure>
                                       </a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                          <figure> <img class="first-img" src="<?php echo $prod_path; ?>" alt="<?php echo e($alt_text); ?>"></figure>
                                       </a>
                                       <?php endif; ?>
                                    </div>
                                    <div class="pr-info-area">
                                       <div class="pr-button">
                                          <div class="mt-button add_to_wishlist"> 
                                             
                                             <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>
                                             <?php if(Session::has('customerid')): ?>
                                             <?php if(count($prodInWishlist)==0): ?>
                                             <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                             <?php echo e(Form::hidden('pro_id','$product_det->pro_id')); ?>        
                                             <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                             <a href="" onclick="addtowish(<?php echo e($product_det->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                             <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             <?php else: ?>
                                             <?php /* remove wishlist */?>   
                                             <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                             <i class="fa fa-heart" aria-hidden="true"></i> 
                                             </a> 
                                             <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                             <?php endif; ?>  
                                             <?php else: ?> 
                                             <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             <?php endif; ?>
                                          </div>
                                          <div class="mt-button quick-view"> <a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap_offer<?php echo e($product_det->pro_id); ?>"> <i class="fa fa-search"></i> </a> </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item-info">
                                    <div class="info-inner">
                                       <div class="item-title"> <a title="" ><?php echo e(substr($product_det->$title,0,25)); ?>

                                          <?php echo e(strlen($product_det->$title)>25?'..':''); ?></a> 
                                       </div>
                                       <div class="item-content">
                                          
                                          <div class="item-price">
                                             <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?></span> </span> </div>
                                          </div>
                                          <div class="pro-action">
                                             <?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty): ?>
                                             <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span> </button>
                                             <?php else: ?>  
                                             <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                             <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                                             <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button> </a>
                                             <?php endif; ?>
                                             <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                             <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">
                                             <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button> </a>
                                             <?php endif; ?>
                                             <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                                             <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">
                                             <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button> </a>
                                             <?php endif; ?>
                                             <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>
                                             <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>">
                                             <button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button>
                                             </a>
                                             <?php endif; ?>
                                             <?php endif; ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php else: ?>
                           <p> <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?> <?php endif; ?> </p>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="row">
         <div class="daily-deal-section">
            <!-- daily deal section-->
            <div class="col-md-7 daily-deal">
               <h3 class="deal-title"><?php if(Lang::has(Session::get('lang_file').'.DEALS_OF_THE_DAY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DEALS_OF_THE_DAY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DEALS_OF_THE_DAY')); ?> <?php endif; ?></h3>
               <div class="title-divider"><span></span></div>
               <p><?php if(Lang::has(Session::get('lang_file').'.GREAT_SAVING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GREAT_SAVING')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GREAT_SAVING')); ?> <?php endif; ?></p>
               <div class="hot-offer-text"><?php if(Lang::has(Session::get('lang_file').'.GREAT_SALE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GREAT_SALE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GREAT_SALE')); ?> <?php endif; ?><span><?php echo e(date('Y')); ?></span></div>
               <div class="box-timer">
                  <span class="des-hot-deal"><?php if(Lang::has(Session::get('lang_file').'.HURRY_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HURRY_UP')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HURRY_UP')); ?> <?php endif; ?></span>
                  
                  <?php if(count($dealsof_day_details) > 0): ?>
                  <?php $__currentLoopData = $dealsof_day_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php   $deal_timing = array($fetch_most_visit_pro->deal_end_date); ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <div class="time" data-countdown="countdown" data-date="<?php echo e(date('m-d-Y-h-i-s',strtotime(max($deal_timing)))); ?>"></div>
                  <a href="<?php echo e(url('')."/deals"); ?>" class="link"><?php if(Lang::has(Session::get('lang_file').'.SHOP_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHOP_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHOP_NOW')); ?> <?php endif; ?></a>
                  <?php else: ?>
                  <p> <?php if(Lang::has(Session::get('lang_file').'.NO_DEALS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_DEALS_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_DEALS_AVAILABLE')); ?> <?php endif; ?></p>
                  <?php endif; ?>
               </div>
            </div>
            <div class="col-md-5 hot-pr-img-area">
               <div id="daily-deal-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4 ">
                     
                     <?php if(count($dealsof_day_details) > 0): ?>
                     <?php $__currentLoopData = $dealsof_day_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <?php
                        $mostproduct_img = explode('/**/', $fetch_most_visit_pro->deal_image);
                        $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));
                            $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));
                            $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));
                            $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 
                            $res = base64_encode($fetch_most_visit_pro->deal_id);
                        //Deal image/
                                        $prod_path = url('').'/public/assets/default_image/No_image_deal.png';
                                        
                                        $img_data = "public/assets/deals/".$mostproduct_img[0]; ?>
                     <?php if(file_exists($img_data) && $mostproduct_img[0] ): ?>  
                     <?php $prod_path = url('').'/public/assets/deals/' .$mostproduct_img[0]; ?>
                     <?php else: ?>  
                     <?php if(isset($DynamicNoImage['dealImg'])): ?>
                     <?php $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg'];?>
                     <?php if($DynamicNoImage['dealImg']!='' && file_exists(trim($dyanamicNoImg_path))): ?>
                     <?php $prod_path = url('')."/".$dyanamicNoImg_path; ?> <?php endif; ?>
                     <?php endif; ?>
                     <?php endif; ?> 
                     
                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                     <?php  $deal_title = 'deal_title'; ?>
                     <?php else: ?>  <?php $deal_title = 'deal_title_langCode'; ?> <?php endif; ?>
                     
                     <?php $alt_txt  = substr($fetch_most_visit_pro->$deal_title,0,25);
                     $alt_txt   .= strlen($fetch_most_visit_pro->$deal_title)>25?'..':''; ?>
                     <div class="pr-img-area">
                        <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                        <a title="<?php echo e($alt_txt); ?>" href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                           <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_txt); ?>"></figure>
                        </a>
                        <?php endif; ?> <!-- //if  -->
                        <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                        <a title="<?php echo e($alt_txt); ?>" href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">
                           <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_txt); ?>"></figure>
                        </a>
                        <!-- //if --> <?php endif; ?>
                        <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 
                        <a title="<?php echo e($alt_txt); ?>" href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">
                           <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_txt); ?>"></figure>
                        </a>
                        <?php endif; ?>
                        <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>  
                        <a title="<?php echo e($alt_txt); ?>" href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>">
                           <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_txt); ?>"></figure>
                        </a>
                        <?php endif; ?>
                     </div>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>  
                     <?php if(isset($DynamicNoImage['dealImg'])): ?>
                     <?php $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg'];?>
                     <?php if($DynamicNoImage['dealImg']!='' && file_exists(trim($dyanamicNoImg_path))): ?>
                     <?php $prod_path = url('')."/".$dyanamicNoImg_path; ?> <?php endif; ?>
                     <?php endif; ?>
                     <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt=""></figure>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="banner-section">
      <div class="container">
         <div class="row">
            <?php if(count($addetails) >0): ?>
            <?php $__currentLoopData = $addetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
            
            <?php $noimgpath="public/assets/noimage/No_image_1509364387_380x215.png"; ?>
            <?php if(isset($DynamicNoImage['ads'])): ?>
            <?php $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['ads']; ?>
            <?php if($DynamicNoImage['ads']!='' && file_exists(trim($dyanamicNoImg_path))): ?>
            <?php  $noimgpath =url('')."/".$dyanamicNoImg_path; ?> <?php endif; ?>
            <?php endif; ?>
             
            <?php $imgpath="public/assets/adimage/".$adinfo->ad_img; ?>
            <?php $adurl=$adinfo->ad_redirecturl;  ?>
             
            <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
            <?php $ad_name = 'ad_name'; ?>
            <?php else: ?>  
            <?php 
            $ad_name = 'ad_name_'.Session::get('lang_code');  
            ?>
            <?php endif; ?>
            <div class="col-sm-4">
               <a href="<?php echo e($adurl); ?>" target="_blank" title="<?php echo e($adinfo->$ad_name); ?>">
                  <?php if(file_exists($imgpath)): ?>
                  <figure> <img src="<?php echo e(url('').'/'.$imgpath); ?>" alt="<?php echo e($ad_name); ?>"></figure>
               </a>
               <?php else: ?> 
               <figure> <img src="<?php echo e($noimgpath); ?>" alt="<?php echo e($ad_name); ?>"></figure>
               </a>
               <?php endif; ?>  
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
            <?php endif; ?>
            <?php if(count($addetails) == 1): ?>
            <div class="col-sm-4">
               <figure> <img src="<?php echo e($noimgpath); ?>" alt="ad_image"></figure>
            </div>
            <div class="col-sm-4">
               <figure> <img src="<?php echo e($noimgpath); ?>" alt="ad_image"></figure>
            </div>
            <?php elseif(count($addetails) == 2): ?>
            <div class="col-sm-4">
               <figure> <img src="<?php echo e($noimgpath); ?>" alt=" ad_image "></figure>
            </div>
            <?php endif; ?> 
         </div>
      </div>
   </div>
   
   <div class="featured-products">
      <div class="container">
         <div class="row">
            <!-- Best Sale -->
            <div class="col-sm-12 col-md-4 jtv-best-sale">
               <div class="jtv-best-sale-list">
                  <div class="wpb_wrapper">
                     <div class="best-title text-left">
                        <h2><?php if(Lang::has(Session::get('lang_file').'.TOP_RATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOP_RATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TOP_RATE')); ?> <?php endif; ?></h2>
                     </div>
                  </div>
                  <div class="slider-items-products">
                     <div id="toprate-products-slider" class="product-flexslider">
                        <div class="slider-items">
                           <!-- start -->
                           <?php if(count($most_popular_product) != 0): ?>
                           <?php $i=0;?>
                           <?php $__currentLoopData = $most_popular_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           <?php if($i % 2 == 0 ): ?>  <?php  $ul = '<ul  class="products-grid">'; echo $ul;  ?> <?php endif; ?>
                           <li class="item">
                              <div class="item-inner">
                                 
                                 <?php 
                                    $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                                    $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                                    $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                                    $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                                    // product id  
                                    $res = base64_encode($product_det->pro_id);
                                    //product image  
                                    $product_image = explode('/**/',$product_det->pro_Img);
                                    //product price  
                                    $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                                    // product dicount percentage  ?>
                                  
                                 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
                                 <?php
                                 $title = 'pro_title';
                                 ?>
                                 <?php else: ?> 
                                 <?php  
                                 $title = 'pro_title_langCode'; 
                                 ?> 
                                 <?php endif; ?>
                                   
                                 <?php 
                                 $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                                 $img_data = "public/assets/product/".$product_image[0];
                                 ?>
                                 <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
                                 <?php 
                                 $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                                 ?>       
                                 <?php else: ?>  
                                 <?php if(isset($DynamicNoImage['productImg'])): ?>
                                 <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                                 <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                                 <?php
                                 $prod_path = url('')."/".$dyanamicNoImg_path;
                                 ?> 
                                 <?php endif; ?>
                                 <?php endif; ?>
                                 <?php endif; ?>
                                 <?php $discount_percent = $alt_text = ''; ?>
                                 
                                 <?php 
                                 $alt_text   = substr($product_det->$title,0,25);
                                 $alt_text  .= strlen($product_det->$title)>25?'..':''; 
                                 ?>
                                 <div class="item-img">
                                    <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 
                                    <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>"> 
                                    <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>">
                                    </a> 
                                    <?php endif; ?> <!-- /*//if*/ --> 
                                    <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                    <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                    <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a> 
                                    <?php endif; ?> <!-- //if -->
                                    <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 
                                    <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                    <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a> 
                                    <?php endif; ?> 
                                    <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 
                                    <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" title="<?php echo e($alt_text); ?>">
                                    <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a>
                                    <?php endif; ?> 
                                 </div>
                                 <div class="item-info">
                                    <div class="info-inner">
                                       <div class="item-title"> 
                                          <?php echo e(substr($product_det->$title,0,25)); ?>

                                          <?php echo e(strlen($product_det->$title)>25?'..':''); ?> </a> 
                                       </div>
                                       
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?></span> </span> </div>
                                       </div>
                                       <div class="pro-action">
                                          <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                          <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          <?php endif; ?>
                                          <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                          <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" class="product-image">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          <?php endif; ?>
                                          <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>  
                                          <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" class="product-image">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          <?php endif; ?>
                                          <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 
                                          <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" class="product-image">
                                          <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                          <?php endif; ?>
                                       </div>
                                       <div class="pr-button-hover">
                                          <div class="mt-button ="> 
                                             
                                             <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first();  ?>
                                             <?php if(Session::has('customerid')): ?>
                                             <?php if(count($prodInWishlist)==0): ?>
                                             <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                             <?php echo e(Form::hidden('pro_id','$product_det->pro_id')); ?>        
                                             <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                             <a href="" onclick="addtowish(<?php echo e($product_det->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                             <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             <?php else: ?>
                                             <?php /* remove wishlist */?>   
                                             <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                             <i class="fa fa-heart" aria-hidden="true"></i> 
                                             </a> 
                                             <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                             <?php endif; ?>  
                                             <?php else: ?> 
                                             <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                             <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                             </a>
                                             <?php endif; ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <?php 
                              $i++;
                              if($i%2==0)
                              {
                                echo '</ul>';
                              }
                                          ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php else: ?>
                           <p> <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?> <?php endif; ?> </p>
                           <?php endif; ?>
                           <!-- end -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Banner -->
            <div class="col-sm-12 col-md-4 top-banner hidden-sm">
               <div class="jtv-banner3">
                  <?php if(count($product_details) != 0 && isset($product_details[2])): ?>
                  <?php for($i = 0; $i < 1 ;$i++): ?> 
                  
                  <?php 
                     $mcat = strtolower(str_replace(' ','-',$product_details[2]->mc_name));
                     $smcat = strtolower(str_replace(' ','-',$product_details[2]->smc_name));
                     $sbcat = strtolower(str_replace(' ','-',$product_details[2]->sb_name));
                     $ssbcat = strtolower(str_replace(' ','-',$product_details[2]->ssb_name)); 
                     // product id  
                     $res = base64_encode($product_details[2]->pro_id);
                     //product image  
                     $product_image = explode('/**/',$product_details[2]->pro_Img);
                     //product price  
                     $product_saving_price = $product_details[2]->pro_price - $product_details[2]->pro_disprice;
                     // product dicount percentage  
                     $product_discount_percentage = round(($product_saving_price/ $product_details[2]->pro_price)*100,2); ?>
                   
                  <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
                  <?php
                  $title = 'pro_title';
                  ?>
                  <?php else: ?> 
                  <?php  
                  $title = 'pro_title_langCode'; 
                  ?> 
                  <?php endif; ?>
                    
                  <?php 
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                  $img_data = "public/assets/product/".$product_image[0];
                  ?>
                  <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
                  <?php 
                  $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                  ?>       
                  <?php else: ?>  
                  <?php if(isset($DynamicNoImage['productImg'])): ?>
                  <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                  <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                  <?php
                  $prod_path = url('')."/".$dyanamicNoImg_path;
                  ?> 
                  <?php endif; ?>
                  <?php endif; ?>
                  <?php endif; ?>
                  <?php $discount_percent = $alt_text = ''; ?>
                  
                  <?php 
                  $alt_text   = substr($product_details[2]->$title,0,25);
                  $alt_text  .= strlen($product_details[2]->$title)>25?'..':''; ?>
                  
                  <?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
                  <?php
                  $discount_percent = round($product_discount_percentage);
                  ?>       
                  <?php endif; ?>
                  <div class="jtv-banner3-inner">
                     <img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>">
                     <div class="hover_content">
                        <div class="hover_data">
                           <div class="title"><?php if(Lang::has(Session::get('lang_file').'.BIG_SALE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BIG_SALE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BIG_SALE')); ?> <?php endif; ?>
                           </div>
                           <div class="desc-text"><?php if(Lang::has(Session::get('lang_file').'.UP_TO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UP_TO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UP_TO')); ?> <?php endif; ?>
                              <?php echo e($discount_percent); ?> <?php if(Lang::has(Session::get('lang_file').'.OFF')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OFF')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OFF')); ?> <?php endif; ?>
                           </div>
                           <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                           <p> <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a>
                           </p>
                           <?php endif; ?> <!-- /*//if*/ --> 
                           <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                           <p>   <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a> 
                           </p>
                           <?php endif; ?> <!-- //if -->
                           <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                           <p> <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a> 
                           </p>
                           <?php endif; ?> 
                           <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>
                           <p>  <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" title="<?php echo $alt_text; ?>" class="shop-now">
                              <?php if(Lang::has(Session::get('lang_file').'.GET_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_NOW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_NOW')); ?> <?php endif; ?>
                              </a> 
                           </p>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <?php endfor; ?>
               <?php else: ?>
               <?php 
               $prod_path = url('').'/public/assets/default_image/No_image_product.png';
               ?>
               <?php if(isset($DynamicNoImage['productImg'])): ?>
               <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
               <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
               <?php
               $prod_path = url('')."/".$dyanamicNoImg_path;
               ?> 
               <?php endif; ?>
               <?php endif; ?>
               <div class="jtv-banner3-inner"><img src="<?php echo e($prod_path); ?>" alt=""> </div>
            </div>
            <?php endif; ?>
         </div>
         <div class="col-sm-12 col-md-4 jtv-best-sale">
            <div class="jtv-best-sale-list">
               <div class="wpb_wrapper">
                  <div class="best-title text-left">
                     <h2><?php if(Lang::has(Session::get('lang_file').'.NEW_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NEW_PRODUCT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NEW_PRODUCT')); ?> <?php endif; ?></h2>
                  </div>
               </div>
               <div class="slider-items-products">
                  <div id="new-products-slider" class="product-flexslider">
                     <div class="slider-items">
                        <?php if(count($new_product) != 0): ?>
                        <?php $i = 0; ?>
                        <?php $__currentLoopData = $new_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php if($i % 2 == 0 ): ?>  <?php  $ul = '<ul  class="products-grid">'; echo $ul;  ?> <?php endif; ?>
                        <li class="item">
                           <div class="item-inner">
                              
                              <?php 
                                 $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                                 $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                                 $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                                 $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
                                 // product id  
                                 $res = base64_encode($product_det->pro_id);
                                 //product image  
                                 $product_image = explode('/**/',$product_det->pro_Img);
                                 //product price  
                                 $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                                 // product dicount percentage  ?>
                               
                              <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
                              <?php
                              $title = 'pro_title';
                              ?>
                              <?php else: ?> 
                              <?php  
                              $title = 'pro_title_langCode'; 
                              ?> 
                              <?php endif; ?>
                                
                              <?php 
                              $prod_path = url('').'/public/assets/default_image/No_image_product.png';
                              $img_data = "public/assets/product/".$product_image[0];
                              ?>
                              <?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
                              <?php 
                              $prod_path = url('').'/public/assets/product/' .$product_image[0]; 
                              ?>       
                              <?php else: ?>  
                              <?php if(isset($DynamicNoImage['productImg'])): ?>
                              <?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                              <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                              <?php
                              $prod_path = url('')."/".$dyanamicNoImg_path;
                              ?> 
                              <?php endif; ?>
                              <?php endif; ?>
                              <?php endif; ?>
                              <?php $discount_percent = $alt_text = ''; ?>
                              
                              <?php 
                              $alt_text   = substr($product_det->$title,0,25);
                              $alt_text  .= strlen($product_det->$title)>25?'..':''; ?>
                              <div class="item-img">
                                 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 
                                 <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                                 <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a> 
                                 <?php endif; ?> <!-- /*//if*/ --> 
                                 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                 <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" >
                                 <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a> 
                                 <?php endif; ?> <!-- //if -->
                                 <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 
                                 <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" >
                                 <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a> 
                                 <?php endif; ?> 
                                 <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 
                                 <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>" >
                                 <img alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> </a> 
                                 <?php endif; ?>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> 
                                     <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                                       <?php echo e(substr($product_det->$title,0,25)); ?>

                                       <?php echo e(strlen($product_det->$title)>25?'..':''); ?></a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">
                                       <?php echo e(substr($product_det->$title,0,25)); ?>

                                       <?php echo e(strlen($product_det->$title)>25?'..':''); ?></a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>  
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">
                                       <?php echo e(substr($product_det->$title,0,25)); ?>

                                       <?php echo e(strlen($product_det->$title)>25?'..':''); ?></a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>">
                                       <?php echo e(substr($product_det->$title,0,25)); ?>

                                       <?php echo e(strlen($product_det->$title)>25?'..':''); ?></a>
                                       <?php endif; ?>
                                     
                                    </div>
                                    
                                    <div class="item-price">
                                       <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?></span> </span> </div>
                                    </div>
                                    <div class="pro-action">
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>  
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 
                                       <a href="<?php echo url('productview').'/'.$mcat.'/'.$res; ?>">
                                       <button type="button" class="add-to-cart"><i class="fa fa-shopping-cart"></i></button></a>
                                       <?php endif; ?>
                                    </div>
                                    <div class="pr-button-hover">
                                       <div class="mt-button"> 
                                          
                                          <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>
                                          <?php if(Session::has('customerid')): ?>
                                          <?php if(count($prodInWishlist)==0): ?>
                                          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                          <?php echo e(Form::hidden('pro_id','$product_det->pro_id')); ?>        
                                          <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                                          <a href="" onclick="addtowish(<?php echo e($product_det->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >
                                          <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                                          <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                          </a>
                                          <?php else: ?>
                                          <?php /* remove wishlist */?>   
                                          <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">
                                          <i class="fa fa-heart" aria-hidden="true"></i> 
                                          </a> 
                                          <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                                          <?php endif; ?>  
                                          <?php else: ?> 
                                          <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">
                                          <i class="fa fa-heart-o" aria-hidden="true"></i> 
                                          </a>
                                          <?php endif; ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <?php 
                           $i++;
                           if($i%2==0)
                           {
                            echo '</ul>';
                           }
                                       ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                        <p> <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?> <?php endif; ?> </p>
                        <?php endif; ?>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- our clients Slider -->
<?php /* <div class="container">
   <div class="row">
     <div class="col-md-12 col-xs-12">
       <div class="our-clients">
         <div class="slider-items-products">
           <div id="our-clients-slider" class="product-flexslider hidden-buttons">
             <div class="slider-items slider-width-col6">
               <div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand1.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand2.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand3.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand4.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand5.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand6.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand7.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand8.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand9.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand10.png" alt="Image"></a> </div>
<div class="item"><a href="#"><img src="<?php echo url(''); ?>/public/themes/images/brand11.png" alt="Image"></a> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> */ ?>
<!-- BANNER-AREA-START -->
<section class="banner-area">
   <div class="container">
   <div class="best-title text-left">
      <h2><?php if(Lang::has(Session::get('lang_file').'.FEATURED_STORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.FEATURED_STORE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.FEATURED_STORE')); ?> <?php endif; ?></h2>
   </div>
      <?php if(count($get_store_details)>0): ?> 
      <div class="row">
         <?php $__currentLoopData = $get_store_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         
         <?php $stor_name = '';?>
         <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
         <?php  $stor_name = 'stor_name'; 
         $co_name = 'co_name';
         $ci_name = 'ci_name';
         ?>
         <?php else: ?> 
         <?php  
         $stor_name = 'stor_name_'.Session::get('lang_code'); 
         $co_name = 'co_name_'.Session::get('lang_code'); 
         $ci_name = 'ci_name_'.Session::get('lang_code'); 
         ?> 
         <?php endif; ?>
         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="banner-block">
               
               <?php $st_name = DB::table('nm_store')->select('*')->where('stor_id','=',$store_details->stor_id)->first(); ?>
               
               <?php
               $product_image     = $store_details->stor_img;
               $prod_path  = url('').'/public/assets/default_image/No_image_store.png';
               $img_data   = "public/assets/storeimage/".$product_image; ?>
               <?php if(file_exists($img_data) && $product_image !=''): ?>   
               <?php $prod_path = url('').'/public/assets/storeimage/' .$product_image;  ?>              
               <?php else: ?>  
               <?php if(isset($DynamicNoImage['store'])): ?>
               <?php  $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; ?>
               <?php if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path)): ?>
               <?php $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>
               <?php endif; ?>
               <?php endif; ?>
               <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($store_details->stor_id)))); ?>"> <img src="<?php echo e($prod_path); ?>" alt="banner sunglasses"> </a>
               <div class="text-des-container">
                  <div class="text-des">
                     <h2><?php echo e($st_name->$stor_name); ?></h2>
                     <p><?php echo e($st_name->stor_slogan); ?> </p>
                  </div>
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <?php else: ?>
      <p> <?php if(Lang::has(Session::get('lang_file').'.NO_STORES_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_STORES_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_STORES_FOUND')); ?> <?php endif; ?> </p>
      <?php endif; ?>
   </div>
</section>
<!-- BANNER-AREA-END -->
<!-- Blog -->
   <section class="blog-post-wrapper">
      <div class="container">
         <div class="best-title text-left">
            <h2><?php if(Lang::has(Session::get('lang_file').'.BLOG')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BLOG')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BLOG')); ?> <?php endif; ?></h2>
         </div>
         <div class="slider-items-products">
            <div id="latest-news-slider" class="product-flexslider hidden-buttons">
               <div class="slider-items slider-width-col6">
                  <?php if(count($get_blog_list) > 0): ?>  
                  <?php $__currentLoopData = $get_blog_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetchblog_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                   
                  <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                  <?php  $blog_title = 'blog_title'; ?>
                  <?php else: ?>
                  <?php  $blog_title = 'blog_title_'.Session::get('lang_code'); ?>
                  <?php endif; ?>
                   
                  <?php   $created_date =  $fetchblog_list->blog_created_date;
                  $explode_date = explode(" ",$created_date);
                  $date =  date(' jS M Y', strtotime($explode_date[0]));
                  ?>
                  
                  <?php  $product_image     = $fetchblog_list->blog_image;
                  $prod_path  = url('').'/public/assets/default_image/No_image_blog.png';
                  $img_data   = "public/assets/blogimage/".$product_image; ?>
                  <?php if(file_exists($img_data) && $product_image!='' ): ?>   
                  <?php   $prod_path = url('public/assets/blogimage/').'/'.$product_image; ?>                 
                  <?php else: ?>  
                  <?php if(isset($DynamicNoImage['blog'])): ?> 
                  <?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['blog']; ?>
                  <?php if($DynamicNoImage['blog']!='' && file_exists($dyanamicNoImg_path)): ?> 
                  <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>
                  <?php endif; ?>
                  <?php endif; ?>
                  
                  <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                  <?php  $blog_desc = 'blog_desc'; ?>
                  <?php else: ?>
                  <?php  $blog_desc = 'blog_desc_'.Session::get('lang_code'); ?>
                  <?php endif; ?>
                  <div class="item">
                     <div class="blog-box">
                        <a href="<?php echo e(url('blog_view/'.$fetchblog_list->blog_id)); ?>" title="<?php echo e($fetchblog_list->$blog_title); ?>"> <img class="primary-img" src="<?php echo e($prod_path); ?>" </a>
                        <div class="blog-btm-desc">
                           <div class="blog-top-desc">
                              <div class="blog-date"> <?php echo e($date); ?> </div>
                              <!--   <h5><a href="<?php echo e(url('blog_view/'.$fetchblog_list->blog_id)); ?>" <?php echo e($fetchblog_list->$blog_title); ?></a></h5> -->
                              <div class="jtv-entry-meta">
                                 <div class="hom-blog-title"><a href="<?php echo e(url('blog_view/'.$fetchblog_list->blog_id)); ?>">
                                    <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                                    <?php    $blog_title = 'blog_title'; ?>
                                    <?php else: ?> <?php  $blog_title = 'blog_title_'.Session::get('lang_code'); ?> <?php endif; ?>
                                    <?php echo e($fetchblog_list->$blog_title); ?>

                                    </a>
                                 </div>
                                 <i class="fa fa-user-o"></i>
                                 <strong><?php if(Lang::has(Session::get('lang_file').'.ADMIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADMIN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADMIN')); ?> <?php endif; ?></strong> 
                                 <a href="<?php echo e(url('blog_comment/'. $fetchblog_list->blog_id)); ?>"><i class="fa fa-commenting-o"></i> <strong>
                                 <?php echo e($get_blog_list_count[$fetchblog_list->blog_id]); ?> 
                                 <?php if(Lang::has(Session::get('lang_file').'.COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COMMENTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COMMENTS')); ?> <?php endif; ?>
                                 </strong></a>
                              </div>
                           </div>
                           <div style="display: block; width: 100%; clear: both;">
                              <p><?php echo e(substr($fetchblog_list->$blog_desc,0,100)); ?> <?php echo e((strlen($fetchblog_list->$blog_desc) > 100) ?'..':''); ?></p>
                           </div>
                           <a class="read-more" href="<?php echo e(url('blog_view/'.$fetchblog_list->blog_id)); ?>"> <?php if(Lang::has(Session::get('lang_file').'.CONTINUE_READING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONTINUE_READING')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONTINUE_READING')); ?> <?php endif; ?></a> 
                        </div>
                     </div>
                  </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php else: ?>
                  <p>
                     <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?> <?php endif; ?> 
                  </p>
                  <?php endif; ?>
               </div>
            </div>
         </div>
      </div>
   </section>
<div class="footer-newsletter">
   <div class="container">
      <div class="row">
         <!-- Newsletter -->
         <div class="col-md-6 col-sm-6">
            <h3><?php if(Lang::has(Session::get('lang_file').'.JOIN_NEWSLETTER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.JOIN_NEWSLETTER')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.JOIN_NEWSLETTER')); ?> <?php endif; ?></h3>
            <div class="title-divider"><span></span></div>
            <span class="sub-text"><?php if(Lang::has(Session::get('lang_file').'.ENTR_MAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTR_MAIL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTR_MAIL')); ?> <?php endif; ?>
            </span>
            <p class="sub-title text-center"><?php if(Lang::has(Session::get('lang_file').'.LATEST_NEWS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LATEST_NEWS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LATEST_NEWS')); ?> <?php endif; ?></p>
            <span class="sub-text1"><?php if(Lang::has(Session::get('lang_file').'.TO_INBOX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TO_INBOX')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TO_INBOX')); ?> <?php endif; ?>
            </span>
            <div class="newsletter-inner">
               <input class="newsletter-email" id="sub_email" type="email" required name='email' placeholder='<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION')); ?> <?php endif; ?>'/>
               <button class="button subscribe" id="subscribe_submit" title="Subscribe"><?php if(Lang::has(Session::get('lang_file').'.SUBSCRIBE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SUBSCRIBE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SUBSCRIBE')); ?> <?php endif; ?></button></a>
               <div class="mail-loader"> <img src="<?php echo url('')?>/images/loader.gif"></div>
            </div>
         </div>
         <!-- Customers Box -->
         <div class="col-sm-6 col-xs-12 testimonials">
            <div class="page-header">
               <h2><?php if(Lang::has(Session::get('lang_file').'.CUS_COMMENT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CUS_COMMENT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CUS_COMMENT')); ?> <?php endif; ?></h2>
               <div class="title-divider"><span></span></div>
            </div>
            <div class="slider-items-products">
               <div id="testimonials-slider" class="product-flexslider hidden-buttons home-testimonials">
                  <div class="slider-items slider-width-col4 ">
                     <?php if(count($review_details)> 0): ?>
                     <?php $__currentLoopData = $review_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     
                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                     <?php  $stor_name = 'stor_name'; 
                     $ci_name = 'ci_name';
                     ?>
                     <?php else: ?> 
                     <?php  
                     $stor_name = 'stor_name_'.Session::get('lang_code'); 
                     $ci_name = 'ci_name_'.Session::get('lang_code'); 
                     ?> 
                     <?php endif; ?>
                     <?php $store_details = DB::table('nm_store')->select($stor_name,'nm_city.'.$ci_name)->leftJoin('nm_city','nm_store.stor_city','=','nm_city.ci_id')->where('stor_id','=',$review->store_id)->first();
                        ?>
                     <div class="holder">
                        <blockquote><?php echo e(strip_tags(substr($review->comments,0,25))); ?>

                           <?php echo e(strip_tags(strlen($review->comments))>25?'..':''); ?>

                        </blockquote>
                        <div class="thumb">
                           
                           <?php if($review->cus_pic!='' && file_exists("url('')/public/assets/profileimagw".$review->cus_pic)): ?>
                           <?php  $imgpath="public/assets/profileimage/".$review->cus_pic;
                              ?>
                           <?php else: ?>
                           <?php 
                              $imgpath="themes/images/products/man.png";
                              ?>
                           <?php endif; ?>
                           <img src="<?php echo e($imgpath); ?>" alt="testimonials img"> 
                        </div>
                        <div class="holder-info"> <strong class="name"><?php echo e($review->cus_name); ?></strong> <strong class="designation">
                           <?php if($store_details): ?>
                           <?php echo e(substr($store_details->$stor_name,0,25)); ?>

                           store,<?php echo e($store_details->$ci_name); ?>

                           <?php endif; ?>
                        </strong>
                        </div>
                     </div>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <p><?php if(Lang::has(Session::get('lang_file').'.NO_REVIEWS_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_REVIEWS_FOUND')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_REVIEWS_FOUND')); ?> <?php endif; ?>
                     </p>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- MicFooter -->
<?php echo $footer; ?>

<!--quick view best rated--> 
<?php if(count($product_details) != 0): ?>
<?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

<?php 
   $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
   $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
   $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
   $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
   // product id  
   $res = base64_encode($product_det->pro_id);
   //product image  
   $product_image = explode('/**/',$product_det->pro_Img);
   //product price  
   $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
   // product dicount percentage  
   $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
<?php   $product_img= explode('/**/',trim($product_det->pro_Img,"/**/")); 
$img_count = count($product_img); ?>
 
<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
<?php
$title = 'pro_title';
?>
<?php else: ?> 
<?php  
$title = 'pro_title_langCode'; 
?> 
<?php endif; ?>
  
<?php 
$prod_path = url('').'/public/assets/default_image/No_image_product.png';
$img_data = "public/assets/product/".$product_image[0];
?>
<?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
<?php 
$prod_path = url('').'/public/assets/product/' .$product_image[0]; 
?>       
<?php else: ?>  
<?php if(isset($DynamicNoImage['productImg'])): ?>
<?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
<?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
<?php
$prod_path = url('')."/".$dyanamicNoImg_path;
?> 
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php $discount_percent = $alt_text = ''; ?>

<?php 
$alt_text   = substr($product_det->$title,0,25);
$alt_text  .= strlen($product_det->$title)>25?'..':''; ?>

<?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
<?php
$discount_percent = round($product_discount_percentage);
?>       
<?php endif; ?>
<?php $count = $product_det->pro_qty - $product_det->pro_no_of_purchase; ?>
<input type="hidden" id="pro_qty_hidden_<?php echo e($product_det->pro_id); ?>" name="pro_qty_hidden" value="<?php echo  $product_det->pro_qty; ?>" />
<input type="hidden" id="pro_purchase_hidden_<?php echo e($product_det->pro_id); ?>" name="pro_purchase_hidden" value="<?php echo  $product_det->pro_no_of_purchase; ?>" />
<div style="display: none;"  class="quick_view_popup-wrap hh" id="quick_view_popup-wrap<?php echo e($product_det->pro_id); ?>">
   <div id="quick_view_popup-overlay"></div>
   <div id="quick_view_popup-outer">
      <div id="quick_view_popup-content">
         <div style="width:auto;height:auto;overflow: auto;position:relative;">
            <div class="product-view-area">
               <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                  <div class="large-image"> 
                     <a href="<?php echo e($prod_path); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="<?php echo e($prod_path); ?>"> </a>
                  </div>
                  <div class="flexslider flexslider-thumb">
                     <ul class="previews-list slides">
                        <?php for($i=0;$i <$img_count;$i++): ?>
                        <?php  $product_image     = $product_img[$i];
                        $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_image; ?>
                        <?php if(file_exists($img_data) && $product_image !=''): ?> <!-- //product image is not null and exists in folder -->
                        <?php $prod_path = url('').'/public/assets/product/' .$product_image;  ?>                 
                        <?php else: ?>  
                        <?php if(isset($DynamicNoImage['productImg'])): ?>
                        <?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>
                        <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?> <!-- //no image for product is not null and exists in folder -->
                        <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?> 
                        <li style="width: 100px; float: left; display: block;"><a href='<?php echo e($prod_path); ?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo e($prod_path); ?>' "><img src="<?php echo e($prod_path); ?>" alt = "Thumbnail 2"/></a></li>
                        <?php endfor; ?>
                     </ul>
                  </div>
                  <!-- end: more-images --> 
               </div>
               <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
                  <div class="product-details-area">
                     <div class="product-name">
                        <h1><?php echo e(substr($product_det->$title,0,25)); ?>

                           <?php echo e(strlen($product_det->$title)>25?'..':''); ?>

                        </h1>
                     </div>
                     <div class="price-box">
                        <p class="special-price"> <span class="price-label"></span> <span class="price"> <?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?> </span> </p>
                        <p class="old-price"> <span class="price-label"></span> <span class="price"> <?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_price); ?> </span> </p>
                     </div>
                     <div class="ratings">
                        <div class="rating"> 
                           <?php           
                           $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();
                           $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();
                           $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();
                           $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();
                           $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();
                           $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                           $multiple_countone = $one_count *1;
                           $multiple_counttwo = $two_count *2;
                           $multiple_countthree = $three_count *3;
                           $multiple_countfour = $four_count *4;
                           $multiple_countfive = $five_count *5;
                           $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>
                           
                           <?php
                           $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                           $multiple_countone   = $one_count *1;
                           $multiple_counttwo   = $two_count *2;
                           $multiple_countthree = $three_count *3;
                           $multiple_countfour  = $four_count *4;
                           $multiple_countfive  = $five_count *5;
                           $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>
                           <?php if($product_count): ?>
                           <?php  $product_divide_count = $product_total_count / $product_count;
                           $product_divide_count = round($product_divide_count); ?>
                           <?php if($product_divide_count <= '1'): ?>
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           <?php elseif($product_divide_count >= '1'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           <?php elseif($product_divide_count >= '2'): ?>
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  
                           <?php elseif($product_divide_count >= '3'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 
                           <?php elseif($product_divide_count >= '4'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                           <?php elseif($product_divide_count >= '5'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                           <?php else: ?>
                           <?php endif; ?>
                           <?php else: ?>
                           <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           <?php endif; ?> 
                        </div>
                        <p class="availability in-stock pull-right"> <?php if(Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AVAILABLE_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AVAILABLE_STOCK')); ?> <?php endif; ?>: <span><?php echo e($product_det->pro_qty-$product_det->pro_no_of_purchase); ?>  <?php if(Lang::has(Session::get('lang_file').'.IN_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.IN_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.IN_STOCK')); ?> <?php endif; ?></span></p>
                     </div>
                     <div class="short-description">
                        <h2><?php if(Lang::has(Session::get('lang_file').'.OVERVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OVERVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OVERVIEW')); ?> <?php endif; ?></h2>
                        
                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                        <?php  $pro_desc = 'pro_desc'; ?>
                        <?php else: ?>
                        <?php  
                        $pro_desc = 'pro_desc_langCode'; ?> 
                        <?php endif; ?>
                        <div class="micheal"><?php //print_r($product_det); ?></div>
                        <!-- <?php $convertString = $product_det->$pro_desc; 
                              $new_str //= str_replace("&nbsp;", '', $convertString);
                               //echo $new_str;
                        ?> -->
                        <p> <!-- <?php echo html_entity_decode(substr($product_det->$pro_desc,0,200)); ?> --> <?php echo $product_det->$pro_desc; ?> </p>
                     </div>
                     <?php  $product_color_detail = DB::table('nm_procolor')->where('pc_pro_id', '=',     $product_det->pro_id)->LeftJoin('nm_color', 'nm_color.co_id', '=', 'nm_procolor.pc_co_id')->get();
                     $product_size_detail = DB::table('nm_prosize')->where('ps_pro_id', '=', $product_det->pro_id)->LeftJoin('nm_size', 'nm_size.si_id', '=', 'nm_prosize.ps_si_id')->get(); ?>
                     <div class="product-color-size-area">
                        <?php if(count($product_color_detail)>0): ?>
                        <div class="color-area">
                           <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COLOR')); ?> <?php endif; ?> </h2>
                           <div class="color">
                              <select name="addtocart_color" id="addtocart_color_<?php echo e($product_det->pro_id); ?>" required>
                                 <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COLOR')); ?> <?php endif; ?>--</option>
                                 <?php $__currentLoopData = $product_color_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <option value="<?php echo e($product_color_det->co_id); ?>">
                                    <?php echo e($product_color_det->co_name); ?>

                                 </option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                              </select>
                           </div>
                        </div>
                        <?php endif; ?>
                        <?php if(count($product_size_detail) > 0): ?>
                        <?php  $size_name = $product_size_detail[0]->si_name;
                        $return  = strcmp($size_name,'no size');  ?>
                        <?php if($return!=0): ?> 
                        <div class="size-area">
                           <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIZE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIZE')); ?> <?php endif; ?></h2>
                           <div class="size">
                              <select name="addtocart_size" id="addtocart_size_<?php echo e($product_det->pro_id); ?>" required>
                                 <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_SIZE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_SIZE')); ?> <?php endif; ?>--</option>
                                 <?php $__currentLoopData = $product_size_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <option value="<?php echo e($product_size_det->ps_si_id); ?>">
                                    <?php echo e($product_size_det->si_name); ?>

                                 </option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                           </div>
                        </div>
                        <?php else: ?> 
                        <input type="hidden" name="addtocart_size" value="<?php echo e($product_size_detail[0]->ps_si_id); ?>">
                        <?php endif; ?>
                        <?php endif; ?>
                     </div>
                     <div class="product-variation">
                        <?php echo Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data','id'=>'submit_form')); ?>

                        <form action="#" method="post">
                        <div class="cart-plus-minus">
                           <label for="qty"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?> :</label>
                           <div class="numbers-row">
                              <div onClick="remove_quantity(<?php echo $product_det->pro_id; ?>)" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                              <input type="number" class="qty" min="1" value="1" max="<?php echo e(($product_det->pro_qty - $product_det->pro_no_of_purchase)); ?>" id="addtocart_qty_<?php echo e($product_det->pro_id); ?>" name="addtocart_qty" readonly required >
                              <div onClick="add_quantity(<?php echo $product_det->pro_id; ?>)" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                           </div>
                        </div>
                        
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <?php echo e(Form::hidden('addtocart_type','product')); ?>

                        <?php echo e(Form::hidden('addtocart_pro_id',$product_det->pro_id)); ?>

                        <?php if(Session::has('customerid')): ?> 
                        <?php if($count > 0): ?>
                        <button onclick="addtocart_validate('<?php echo $product_det->pro_id; ?>');"  class="button pro-add-to-cart" title="Add to Cart" type="button" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span></button>
                        <?php else: ?> 
                        <button type="button" class="btn btn-danger">
                        <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>
                        </button> 
                        <?php endif; ?> 
                        <?php else: ?> 
                        <?php if($count > 0): ?>
                        <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                        <button type="button" class=" button pro-add-to-cart">
                        <span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                        <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span>
                        </button> 
                        </a>
                        <?php else: ?>
                        <button type="button" class="btn btn-danger">
                        <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>
                        </button> 
                        <?php endif; ?> 
                        <?php endif; ?>
                        <?php echo e(Form::close()); ?>

                     </div>
                     <div class="product-cart-option">
                        <ul>
                           <li>
                              
                              <?php if(Session::has('customerid')): ?>
                              <?php  
                              $cus_id = Session::get('customerid');
                              $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',$cus_id)->first(); ?>
                              <?php else: ?>
                              <?php  $prodInWishlist = array(); ?>
                              <?php endif; ?>
                              <?php if($count > 0): ?>  
                              <?php if(Session::has('customerid')): ?>
                              <?php if(count($prodInWishlist)==0): ?>
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                              <?php echo e(Form::hidden('pro_id','$product_det->pro_id')); ?>        
                              <!-- <input type="hidden" name="pro_id" value="<?php echo $product_det->pro_id; ?>"> -->
                              <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                              <a href="" onclick="addtowish(<?php echo e($product_det->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)">
                              <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                              <i class="fa fa-heart-o" aria-hidden="true"></i><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST')); ?> <?php endif; ?> 
                              </a>
                              <?php else: ?>
                              <?php /* remove wishlist */?>   
                              <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>">
                              <i class="fa fa-heart" aria-hidden="true"></i>    <?php if(Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST')); ?> <?php endif; ?>
                              </a> 
                              <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                              <?php endif; ?>  
                              <?php else: ?> 
                              <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                              <i class="fa fa-heart-o" aria-hidden="true"></i> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST')); ?> <?php endif; ?>
                              </a>
                              <?php endif; ?>
                              <?php endif; ?>
                           </li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--product-view--> 
         </div>
      </div>
      <a style="display: inline;" id="quick_view_popup-close" href="<?php echo e(url('')); ?>/index"><i class="icon pe-7s-close"></i></a> 
   </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<!--end qiuck view best rated-->
<?php if(count($product_details) != 0): ?>
<?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

<?php 
   $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
   $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
   $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
   $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 
   // product id  
   $res = base64_encode($product_det->pro_id);
   //product image  
   $product_image = explode('/**/',$product_det->pro_Img);
   //product price  
   $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
   // product dicount percentage  
   $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
<?php  $product_img= explode('/**/',trim($product_det->pro_Img,"/**/")); 
$img_count = count($product_img); ?>
<?php if($product_discount_percentage<= 50): ?>
<?php if($product_det->pro_no_of_purchase < $product_det->pro_qty): ?> 
 
<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>
<?php
$title = 'pro_title';
?>
<?php else: ?> 
<?php  
$title = 'pro_title_langCode'; 
?> 
<?php endif; ?>
  
<?php 
$prod_path = url('').'/public/assets/default_image/No_image_product.png';
$img_data = "public/assets/product/".$product_image[0];
?>
<?php if(file_exists($img_data) && $product_image[0] !='' ): ?> 
<?php 
$prod_path = url('').'/public/assets/product/' .$product_image[0]; 
?>       
<?php else: ?>  
<?php if(isset($DynamicNoImage['productImg'])): ?>
<?php $dyanamicNoImg_path ='public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
<?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
<?php
$prod_path = url('')."/".$dyanamicNoImg_path;
?> 
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php $discount_percent = $alt_text = ''; ?>

<?php 
$alt_text   = substr($product_det->$title,0,25);
$alt_text  .= strlen($product_det->$title)>25?'..':''; ?>

<?php if($product_discount_percentage!='' && round($product_discount_percentage)!=0): ?>
<?php
$discount_percent = round($product_discount_percentage);
?>       
<?php endif; ?>
<?php $count = $product_det->pro_qty - $product_det->pro_no_of_purchase; ?>
<input type="hidden" id="pro_qty_hidden_offer_<?php echo e($product_det->pro_id); ?>" name="pro_qty_hidden_offer" value="<?php echo  $product_det->pro_qty; ?>" />
<input type="hidden" id="pro_purchase_hidden_offer_<?php echo e($product_det->pro_id); ?>" name="pro_purchase_hidden_offer" value="<?php echo  $product_det->pro_no_of_purchase; ?>" />
<div style="display:none;"  class="quick_view_popup-wrap" id="quick_view_popup-wrap_offer<?php echo e($product_det->pro_id); ?>">
   <div id="quick_view_popup-overlay"></div>
   <div id="quick_view_popup-outer">
      <div id="quick_view_popup-content">
         <div style="width:auto;height:auto;overflow: auto;position:relative;">
            <div class="product-view-area">
               <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                  <div class="large-image"> 
                     <a href="<?php echo e($prod_path); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="<?php echo e($prod_path); ?>"> </a> 
                  </div>
                  <div class="flexslider flexslider-thumb">
                     <ul class="previews-list slides">
                        <?php for($i=0;$i <$img_count;$i++): ?>
                        <?php  $product_image     = $product_img[$i];
                        $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_image; ?>
                        <?php if(file_exists($img_data) && $product_image !=''): ?> <!-- //product image is not null and exists in folder -->
                        <?php $prod_path = url('').'/public/assets/product/' .$product_image;  ?>                 
                        <?php else: ?>  
                        <?php if(isset($DynamicNoImage['productImg'])): ?>
                        <?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>
                        <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?> <!-- //no image for product is not null and exists in folder -->
                        <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <li style="width: 100px; float: left; display: block;"><a href='<?php echo e($prod_path); ?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo e($prod_path); ?>' "><img src="<?php echo e($prod_path); ?>" alt = "Thumbnail 2"/></a></li>
                        <?php endfor; ?>
                     </ul>
                  </div>
                  <!-- end: more-images --> 
               </div>
               <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
                  <div class="product-details-area">
                     <div class="product-name">
                        <h1><?php echo e(substr($product_det->$title,0,25)); ?>

                           <?php echo e(strlen($product_det->$title)>25?'..':''); ?>

                        </h1>
                     </div>
                     <div class="price-box">
                        <p class="special-price"> <span class="price-label"></span> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?> </span> </p>
                        <p class="old-price"> <span class="price-label"></span> <span class="price"> <?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_price); ?> </span> </p>
                     </div>
                     <div class="ratings">
                        <?php            
                        $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();
                        $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();
                        $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();
                        $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();
                        $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();
                        $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                        $multiple_countone = $one_count *1;
                        $multiple_counttwo = $two_count *2;
                        $multiple_countthree = $three_count *3;
                        $multiple_countfour = $four_count *4;
                        $multiple_countfive = $five_count *5;
                        $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>
                        <div class="rating"> 
                           
                           <?php
                           $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
                           $multiple_countone   = $one_count *1;
                           $multiple_counttwo   = $two_count *2;
                           $multiple_countthree = $three_count *3;
                           $multiple_countfour  = $four_count *4;
                           $multiple_countfive  = $five_count *5;
                           $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>
                           <?php if($product_count): ?>
                           <?php  $product_divide_count = $product_total_count / $product_count;
                           $product_divide_count = round($product_divide_count); ?>
                           <?php if($product_divide_count <= '1'): ?>
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           <?php elseif($product_divide_count >= '1'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           <?php elseif($product_divide_count >= '2'): ?>
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  
                           <?php elseif($product_divide_count >= '3'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 
                           <?php elseif($product_divide_count >= '4'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                           <?php elseif($product_divide_count >= '5'): ?> 
                           <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                           <?php else: ?>
                           <?php endif; ?>
                           <?php else: ?>
                           <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                           <?php endif; ?> 
                        </div>
                        <p class="availability in-stock pull-right"> <?php if(Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AVAILABLE_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AVAILABLE_STOCK')); ?> <?php endif; ?>: <span><?php echo e($product_det->pro_qty-$product_det->pro_no_of_purchase); ?>  <?php if(Lang::has(Session::get('lang_file').'.IN_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.IN_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.IN_STOCK')); ?> <?php endif; ?></span></p>
                     </div>
                     <div class="short-description">
                        <h2><?php if(Lang::has(Session::get('lang_file').'.OVERVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OVERVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OVERVIEW')); ?> <?php endif; ?></h2>
                        
                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                        <?php  $pro_desc = 'pro_desc'; ?>
                        <?php else: ?>
                        <?php  
                        $pro_desc = 'pro_desc_langCode'; ?> 
                        <?php endif; ?>
                        <p><?php echo html_entity_decode(substr($product_det->$pro_desc,0,200)); ?></p>
                     </div>
                     <?php  $product_color_detail = DB::table('nm_procolor')->where('pc_pro_id', '=',     $product_det->pro_id)->LeftJoin('nm_color', 'nm_color.co_id', '=', 'nm_procolor.pc_co_id')->get();
                     $product_size_detail = DB::table('nm_prosize')->where('ps_pro_id', '=', $product_det->pro_id)->LeftJoin('nm_size', 'nm_size.si_id', '=', 'nm_prosize.ps_si_id')->get(); ?>
                     <div class="product-color-size-area">
                        <?php if(count($product_color_detail)>0): ?>
                        <div class="color-area">
                           <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COLOR')); ?> <?php endif; ?> </h2>
                           <div class="color">
                              <select name="addtocart_color" id="addtocart_color_offer_<?php echo e($product_det->pro_id); ?>" required>
                                 <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COLOR')); ?> <?php endif; ?>--</option>
                                 <?php $__currentLoopData = $product_color_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <option value="<?php echo e($product_color_det->co_id); ?>">
                                    <?php echo e($product_color_det->co_name); ?>

                                 </option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                              </select>
                           </div>
                        </div>
                        <?php endif; ?>
                        <?php if(count($product_size_detail) > 0): ?>
                        <?php  $size_name = $product_size_detail[0]->si_name;
                        $return  = strcmp($size_name,'no size');  ?>
                        <?php if($return!=0): ?> 
                        <div class="size-area">
                           <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIZE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIZE')); ?> <?php endif; ?></h2>
                           <div class="size">
                              <select name="addtocart_size" id="addtocart_size_offer_<?php echo e($product_det->pro_id); ?>" required>
                                 <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_SIZE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_SIZE')); ?> <?php endif; ?>--</option>
                                 <?php $__currentLoopData = $product_size_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <option value="<?php echo e($product_size_det->ps_si_id); ?>">
                                    <?php echo e($product_size_det->si_name); ?>

                                 </option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                           </div>
                        </div>
                        <?php else: ?> 
                        <input type="hidden" name="addtocart_size" value="<?php echo e($product_size_detail[0]->ps_si_id); ?>">
                        <?php endif; ?>
                        <?php endif; ?>
                     </div>
                     <div class="product-variation">
                        <?php echo Form :: open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data','id'=>'submit_form_offer')); ?>

                        <form action="#" method="post">
                        <div class="cart-plus-minus">
                           <label for="qty"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?> :</label>
                           <div class="numbers-row">
                              <div onClick="remove_quantity_offer(<?php echo $product_det->pro_id; ?>)" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                              <input type="number" class="qty" min="1" value="1" max="<?php echo e(($product_det->pro_qty - $product_det->pro_no_of_purchase)); ?>" id="addtocart_qty_offer_<?php echo e($product_det->pro_id); ?>" name="addtocart_qty" readonly required >
                              <div onClick="add_quantity_offer(<?php echo $product_det->pro_id; ?>)" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                           </div>
                        </div>
                        
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <?php echo e(Form::hidden('addtocart_type','product')); ?>

                        <?php echo e(Form::hidden('addtocart_pro_id',$product_det->pro_id)); ?>

                        <?php if(Session::has('customerid')): ?> 
                        <?php if($count > 0): ?>
                        <button onclick="addtocart_validate_offer('<?php echo $product_det->pro_id; ?>');"  class="button pro-add-to-cart" title="Add to Cart" type="button" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span></button>
                        <?php else: ?> 
                        <button type="button" class="btn btn-danger">
                        <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>
                        </button> 
                        <?php endif; ?> 
                        <?php else: ?> 
                        <?php if($count > 0): ?>
                        <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                        <button type="button" class=" button pro-add-to-cart">
                        <span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                        <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span>
                        </button> 
                        </a>
                        <?php else: ?>
                        <button type="button" class="btn btn-danger">
                        <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>
                        </button> 
                        <?php endif; ?> 
                        <?php endif; ?>
                        <?php echo e(Form::close()); ?>

                     </div>
                     <div class="product-cart-option">
                        <ul>
                           <li>
                              
                              <?php if(Session::has('customerid')): ?>
                              <?php  
                              $cus_id = Session::get('customerid');
                              $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->pro_id)->where('ws_cus_id','=',$cus_id)->first(); ?>
                              <?php else: ?>
                              <?php  $prodInWishlist = array(); ?>
                              <?php endif; ?>
                              <?php if($count > 0): ?>  
                              <?php if(Session::has('customerid')): ?>
                              <?php if(count($prodInWishlist)==0): ?>
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                              <?php echo e(Form::hidden('pro_id','$product_det->pro_id')); ?>        
                              <!-- <input type="hidden" name="pro_id" value="<?php echo $product_det->pro_id; ?>"> -->
                              <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                              <a href="" onclick="addtowish(<?php echo e($product_det->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)">
                              <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                              <i class="fa fa-heart-o" aria-hidden="true"></i><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST')); ?> <?php endif; ?> 
                              </a>
                              <?php else: ?>
                              <?php /* remove wishlist */?>   
                              <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>">
                              <i class="fa fa-heart" aria-hidden="true"></i>    <?php if(Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST')); ?> <?php endif; ?>
                              </a> 
                              <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                              <?php endif; ?>  
                              <?php else: ?> 
                              <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                              <i class="fa fa-heart-o" aria-hidden="true"></i> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST')); ?> <?php endif; ?>
                              </a>
                              <?php endif; ?>
                              <?php endif; ?>
                           </li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--product-view--> 
         </div>
      </div>
      <a style="display: inline;" id="quick_view_popup-close" href="<?php echo e(url('')); ?>/index"><i class="icon pe-7s-close"></i></a> 
   </div>
</div>
<?php endif; ?>  
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<!--quick view best rated 50% offer--> 
<!--end quick view best rated 50% offer--> 

<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> 
<!-- End Footer --> 
</div>
<!-- JS --> 
<script type='text/javascript'>
   jQuery(document).ready(function(){
       jQuery('#rev_slider_4').show().revolution({
           dottedOverlay: 'none',
           delay: 5000,
           startwidth: 865,
        startheight: 450,
   
           hideThumbs: 200,
           thumbWidth: 200,
           thumbHeight: 50,
           thumbAmount: 2,
   
           navigationType: 'thumb',
           navigationArrows: 'solo',
           navigationStyle: 'round',
   
           touchenabled: 'on',
           onHoverStop: 'on',
           
           swipe_velocity: 0.7,
           swipe_min_touches: 1,
           swipe_max_touches: 1,
           drag_block_vertical: false,
       
           spinner: 'spinner0',
           keyboardNavigation: 'off',
   
           navigationHAlign: 'center',
           navigationVAlign: 'bottom',
           navigationHOffset: 0,
           navigationVOffset: 20,
   
           soloArrowLeftHalign: 'left',
           soloArrowLeftValign: 'center',
           soloArrowLeftHOffset: 20,
           soloArrowLeftVOffset: 0,
   
           soloArrowRightHalign: 'right',
           soloArrowRightValign: 'center',
           soloArrowRightHOffset: 20,
           soloArrowRightVOffset: 0,
   
           shadow: 0,
           fullWidth: 'on',
           fullScreen: 'off',
   
           stopLoop: 'off',
           stopAfterLoops: -1,
           stopAtSlide: -1,
   
           shuffle: 'off',
   
           autoHeight: 'off',
           forceFullWidth: 'on',
           fullScreenAlignForce: 'off',
           minFullScreenHeight: 0,
           hideNavDelayOnMobile: 1500,
       
           hideThumbsOnMobile: 'off',
           hideBulletsOnMobile: 'off',
           hideArrowsOnMobile: 'off',
           hideThumbsUnderResolution: 0,
   
   
           hideSliderAtLimit: 0,
           hideCaptionAtLimit: 0,
           hideAllCaptionAtLilmit: 0,
           startWithSlide: 0,
           fullScreenOffsetContainer: ''
       });
   });
</script>
<script type="text/javascript">
   $('#subscribe_submit').click(function(){  
     var email=$('#sub_email').val(); 
     if(email=='') {
       alert('Enter an email id');
       return false;
     }
   $('.mail-loader').css('display','block');
      $.ajax( { 
               type: 'get',
               data: {email},
               url: '<?php echo url('subscription_submit'); ?>',
               success: function(responseText){  
                
                if(responseText=='0')
                { 
                        alert("Email already subscribed!");
                           $('.mail-loader').css('display','none');
                } else{
                 // Need loader
               $('.mail-loader').css('display','none');
                 alert("Email subscribed Successfully!");
                }
             }       
         });  
   
   
   });
   
</script>
<script type="text/javascript">
   function addtowish(pro_id,cus_id){
   
     var wishlisturl = document.getElementById('wishlisturl').value;
   
     $.ajax({
           type: "get",   
           url:"<?php echo url('addtowish'); ?>",
           data:{'pro_id':pro_id,'cus_id':cus_id},
           success:function(response){
            // alert(response);
             if(response==0){
              
             <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>
                         $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');
               //window.location=wishlisturl;
                             window.location.reload();
                             
             }else{
               alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');
               //window.location=wishlisturl;
             }
             
             
           }
         });
   }
</script>
<script type="text/javascript">
   jQuery(document).ready(function($) {
     var Body = $('body');
     Body.addClass('preloader-site');
   });
   $(window).load(function(){
     $('.preloader-wrapper').fadeOut();
     $('body').removeClass('preloader-site');
   });
</script>
<script>
   function add_quantity(id)
   {
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
     var quantity=$("#addtocart_qty_"+id).val(); 
     var pro_qty_hidden=$("#pro_qty_hidden_"+id).val();
     var pro_purchase_hidden=$("#pro_purchase_hidden_"+id).val();
     var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);
    
     
     if(quantity<remaining_product)
     { 
       var new_quantity=parseInt(quantity)+1;
       $("#addtocart_qty_"+id).val(new_quantity);
     }
     //alert();
   }
   
   function remove_quantity(id)
   {
     //alert();
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/
   
     var quantity=$("#addtocart_qty_"+id).val();
     var quantity=parseInt(quantity);
     if(quantity>1)
     {
       var new_quantity=quantity-1;
       $("#addtocart_qty_"+id).val(new_quantity);
     }
     //alert();
   }
   
   function add_quantity_offer(id)
   {
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
     var quantity=$("#addtocart_qty_offer_"+id).val(); 
     var pro_qty_hidden=$("#pro_qty_hidden_offer_"+id).val();
     var pro_purchase_hidden=$("#pro_purchase_hidden_offer_"+id).val();
     var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);
    
     
     if(quantity<remaining_product)
     { 
       var new_quantity=parseInt(quantity)+1;
       $("#addtocart_qty_offer_"+id).val(new_quantity);
     }
     //alert();
   }
   
   function remove_quantity_offer(id)
   {
     //alert();
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/
   
     var quantity=$("#addtocart_qty_offer_"+id).val();
     var quantity=parseInt(quantity);
     if(quantity>1)
     {
       var new_quantity=quantity-1;
       $("#addtocart_qty_offer_"+id).val(new_quantity);
     }
     //alert();
   }
</script>
<script type="text/javascript">
   function addtowish(pro_id,cus_id){
     //alert();
     var wishlisturl = document.getElementById('wishlisturl').value;
   
     $.ajax({
           type: "get",   
           url:"<?php echo url('addtowish'); ?>",
           data:{'pro_id':pro_id,'cus_id':cus_id},
             success:function(response){
             //alert(response); return false;
             if(response==0){
             <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>
                         $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');
               //window.location=wishlisturl;
                             window.location.reload();
                             
             }else{
               alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');
               //window.location=wishlisturl;
             }
             
             
           }
         });
   }
</script>
<script type="text/javascript">
   function addtocart_validate_offer(id){
    
   var pro_qty=$("#pro_qty_hidden_offer_"+id).val();
   var pro_purchase1=$("#pro_purchase_hidden_offer_"+id).val();
   var pro_purchase = parseInt($('#addtocart_qty_offer_').val()) + parseInt(pro_purchase1);
   var error = 0;
   if(pro_purchase > parseInt(pro_qty))
   {
     $('#addtocart_qty_offer_'+id).focus();
     $('#addtocart_qty_offer_'+id).css('border-color', 'red');
     $('#addtocart_qty_error_offer_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');
   error++;
     return false;
   }
   else
   {
     $('#addtocart_qty_offer_'+id).css('border-color', '');
     $('#addtocart_qty_error_offer_'+id).html('');
   }
   if($('#addtocart_color_offer_'+id).val() ==0) 
   {
     $('#addtocart_color_offer_'+id).focus();
     $('#addtocart_color_offer_'+id).css('border-color', 'red');
     $('#size_color_error_offer_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');
     error++;
   return false;
   }
   else
   {
     $('#addtocart_color_offer_'+id).css('border-color', '');
     $('#size_color_error_offer_'+id).html('');
   }
   if($('#addtocart_size_offer_'+id).val() ==0)
   {
     $('#addtocart_size_offer_'+id).focus();
     $('#addtocart_size_offer_'+id).css('border-color', 'red');
     $('#size_color_error_offer_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');
     error++;
   return false;
   }
   else
   {
     $('#addtocart_size_offer_'+id).css('border-color', '');
     $('#size_color_error_offer_'+id).html('');
   }
   
   if(error <= 0){
    $("#submit_form_offer").submit(); 
   }
   
   }  
</script>   
<script type="text/javascript">
   function addtocart_validate(id){
    
   var pro_qty=$("#pro_qty_hidden_"+id).val();
   var pro_purchase1=$("#pro_purchase_hidden_"+id).val();
   var pro_purchase = parseInt($('#addtocart_qty_offer_').val()) + parseInt(pro_purchase1);
   var error1 = 0;
    if(pro_purchase > parseInt(pro_qty))
   {
     $('#addtocart_qty_'+id).focus();
     $('#addtocart_qty_'+id).css('border-color', 'red');
     $('#addtocart_qty_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');
     return false;
   }
   else
   {
     $('#addtocart_qty_'+id).css('border-color', '');
     $('#addtocart_qty_error_'+id).html('');
   }
   if($('#addtocart_color_'+id).val() ==0) 
   {
     $('#addtocart_color_'+id).focus();
     $('#addtocart_color_'+id).css('border-color', 'red');
     $('#size_color_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');
     return false;
   }
   else
   {
     $('#addtocart_color_'+id).css('border-color', '');
     $('#size_color_error_'+id).html('');
   }
   if($('#addtocart_size_'+id).val() ==0)
   {
     $('#addtocart_size_'+id).focus();
     $('#addtocart_size_'+id).css('border-color', 'red');
     $('#size_color_error_'+id).html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');
     return false;
   }
   else
   {
     $('#addtocart_size_'+id).css('border-color', '');
     $('#size_color_error_'+id).html('');
   }
   
   if(error1 <= 0){
    $("#submit_form").submit(); 
   }
   
   }  
</script>

   

</body>
</html>