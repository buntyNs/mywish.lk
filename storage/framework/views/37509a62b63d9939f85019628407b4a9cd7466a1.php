<?php echo $navbar; ?>

<!-- <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script> -->

<!-- Navbar ================================================== -->
<?php echo $header; ?>

<?php $city_det = DB::table('nm_emailsetting')->first(); ?>
<!-- Header End====================================================================== -->
<div id="mainBody">
   <div class="breadcrumbs">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <ul>
                  <li><a href="index"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="divider">/</span></li>
                  <li class="active"><?php if(Lang::has(Session::get('lang_file').'.MERCHANT_SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MERCHANT_SIGN_UP')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MERCHANT_SIGN_UP')); ?> <?php endif; ?></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
<div class="main-container">
   <div class="container">
      <div class="row">
         <!-- Sidebar ================================================== -->
         <!-- Sidebar end=============================================== -->
         <div class="col-md-12 mrchnt-sgnup">
         <div class="span12">
            
            <?php if($errors->any()): ?>
            <br>
            <ul style="color:red;">
               <div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
               </div>
            </ul>
            <?php endif; ?>
            <?php if(Session::has('mail_exist')): ?>
            <div class="alert alert-warning alert-dismissable"><?php echo Session::get('mail_exist'); ?>

               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            </div>
            <?php endif; ?>
            <?php if(Session::has('result')): ?>
            <div class="alert alert-success alert-dismissable"><?php echo Session::get('result'); ?>

               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            </div>
            <?php endif; ?>
            <h3><?php if(Lang::has(Session::get('lang_file').'.WELCOME_TO_MERCHANT_SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WELCOME_TO_MERCHANT_SIGN_UP')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WELCOME_TO_MERCHANT_SIGN_UP')); ?> <?php endif; ?>!</h3>
            <p><?php if(Lang::has(Session::get('lang_file').'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')); ?> <?php endif; ?>!
            </p>
            <div class="content forms_new">
               <?php echo Form::open(array('url'=>'merchant_signup','class'=>'testform','id'=>'testform','enctype'=>'multipart/form-data','accept-charset' => 'UTF-8')); ?>    
       <div class="personal-data">
               <div class="col-md-6 mrchnt-sgnup-left">
                          <h4 style="padding:10px;background:#eee;"><?php if(Lang::has(Session::get('lang_file').'.CREATE_YOUR_STORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CREATE_YOUR_STORE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CREATE_YOUR_STORE')); ?> <?php endif; ?></h4>
                  <div class="">
                     <div class="span5">
                        <label for="text1"> <?php if(Lang::has(Session::get('lang_file').'.STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORE_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORE_NAME')); ?> <?php endif; ?>  
                           (English):<span class="text-sub">*</span></label>
                        <input type="text" id="store_name" maxlength="150" name="store_name" class="form-control span5" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STORE_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_STORE_NAME')); ?> <?php endif; ?>" value="<?php echo Input::old('store_name'); ?>" />
                        <?php if(!empty($get_active_lang)): ?>  
                        <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php
                        $get_lang_name = $get_lang->lang_name;
                        ?>
                        <label for="text1" ><?php if(Lang::has(Session::get('lang_file').'.STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORE_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORE_NAME')); ?> <?php endif; ?>:(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                        <input type="text" id="store_name_<?php echo $get_lang_name; ?>" name="store_name_<?php echo $get_lang_name; ?>" class="form-control span5" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STORE_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_STORE_NAME')); ?> <?php endif; ?>  <?php echo e($get_lang_name); ?>"  value="<?php echo Input::old('store_name_'.$get_lang_name); ?>" />
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <label for="text1" ><?php if(Lang::has(Session::get('lang_file').'.PHONE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PHONE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PHONE')); ?> <?php endif; ?> :<span class="text-sub">*</span></label>
                        <input type="text" id="store_pho" maxlength="16" required="required" name="store_pho" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_CONTACT_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_CONTACT_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_CONTACT_NUMBER')); ?> <?php endif; ?>" class="form-control span5" value="<?php echo Input::old('store_pho'); ?>" onkeypress="return isNumber(event)" 
                           data-minlength="15" data-maxlength="15" data-error="Less Number" >
                        <div id="store_pho_error_msg"></div>
                        <label for="text1"> <?php if(Lang::has(Session::get('lang_file').'.ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS1')); ?> <?php endif; ?>  
                           (English):<span class="text-sub">*</span></label>
                        <input type="text" id="store_add_one" name="store_add_one" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS1')); ?> <?php endif; ?> English" class="form-control span5" value="<?php echo Input::old('store_add_one'); ?>">
                        <?php if(!empty($get_active_lang)): ?>  
                        <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php 
                        $get_lang_name = $get_lang->lang_name;
                        ?>           
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS1')); ?> <?php endif; ?> 
                           (<?php echo e($get_lang_name); ?>):<span class="text-sub">*</span></label>
                        <input type="text" id="store_add_one_<?php echo $get_lang_name; ?>" name="store_add_one_<?php echo $get_lang_name; ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS1')); ?> <?php endif; ?> <?php echo e($get_lang_name); ?>" class="form-control span5" value="<?php echo Input::old('store_add_one_'.$get_lang_name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>   
                        <label for="text1" ><?php if(Lang::has(Session::get('lang_file').'.ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS2')); ?> <?php endif; ?> 
                           (English):<span class="text-sub">*</span></label>
                        <input type="text" id="store_add_two" name="store_add_two" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS2')); ?> <?php endif; ?> English" class="form-control span5" value="<?php echo Input::old('store_add_two'); ?>">
                        <?php if(!empty($get_active_lang)): ?>  
                        <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php 
                        $get_lang_name = $get_lang->lang_name;
                        ?>              
                        <label for="text1" ><?php if(Lang::has(Session::get('lang_file').'.ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS2')); ?> <?php endif; ?>
                           (<?php echo e($get_lang_name); ?>):<span class="text-sub">*</span></label>
                        <input type="text" id="store_add_two_<?php echo $get_lang_name; ?>" name="store_add_two_<?php echo $get_lang_name; ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS2')); ?> <?php endif; ?> <?php echo e($get_lang_name); ?>" class="form-control span5" value="<?php echo Input::old('store_add_two_'.$get_lang_name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>    
                        <label for="text1" ><?php if(Lang::has(Session::get('lang_file').'.COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUNTRY')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <select class="span5" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
                           <option value="">-- <?php if(Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COUNTRY')); ?> <?php endif; ?> --</option>
                           <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option value="<?php echo $country_fetch->co_id; ?>" <?php if(Input::old('select_country')==$country_fetch->co_id){ echo "selected";}?>><?php 
                              if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
                              $co_name = 'co_name';
                              }else {  $co_name = 'co_name_'.Session::get('lang_code'); }
                               echo $country_fetch->$co_name; ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <?php if(Input::old('select_city')): ?>
                        <select class="span5" name="select_city" id="select_city" >
                           <?php $__currentLoopData = $get_city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           <?php if(Input::old('select_country')==$city->ci_con_id): ?>
                           <?php  if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
                              $ci_name = 'ci_name';
                              }else {  $ci_name = 'ci_name_'.Session::get('lang_code'); }?>
                           <option value="<?php echo $city->ci_id;?>" <?php if(Input::old('select_city')==$city->ci_id){ echo "selected";} ?>>
                              <?php echo $city->$ci_name; ?>
                           </option>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php else: ?>
                        <select class="span5" name="select_city" id="select_city" >
                           <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?>--</option>
                        </select>
                        <?php endif; ?>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ZIPCODE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ZIPCODE')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="text" id="zip_code" name="zip_code" maxlength="6" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ZIPCODE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ZIPCODE')); ?> <?php endif; ?>" class="form-control span5" value="<?php echo Input::old('zip_code'); ?>" onkeypress="return isNumber(event)" 
                           data-minlength="6" data-maxlength="6" data-error="Less Number">
                        <div id="zip_error_msg"></div>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.META_KEYWORDS')); ?> <?php endif; ?>
                           (English):<span class="text-sub">*</span></label>
                        <input type="text" id="meta_keyword" name="meta_keyword" style="height:50px;" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_META_KEYWORDS')); ?> <?php endif; ?>"  class="form-control span5" value="<?php echo Input::old('meta_keyword'); ?>">
                        <?php if(!empty($get_active_lang)): ?>  
                        <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php
                        $get_lang_name = $get_lang->lang_name;
                        ?>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.META_KEYWORDS')); ?> <?php endif; ?>
                           (<?php echo e($get_lang_name); ?>):<span class="text-sub">*</span></label>
                        <input type="text" id="meta_keyword_<?php echo e($get_lang_name); ?>" name="meta_keyword_<?php echo e($get_lang_name); ?>" style="height:50px;" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_META_KEYWORDS')); ?> <?php endif; ?>  <?php echo e($get_lang_name); ?>"  class="form-control span5" value="<?php echo Input::old('meta_keyword_'.$get_lang_name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.META_DESCRIPTION')); ?> <?php endif; ?>
                           (English)<span class="text-sub">*</span></label>
                        <input type="text" id="meta_description"  name="meta_description" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_META_DESCRIPTION')); ?> <?php endif; ?>" style="height:50px;" class="form-control span5" value="<?php echo Input::old('meta_description'); ?>">
                        <?php if(!empty($get_active_lang)): ?> 
                        <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $get_lang_name = $get_lang->lang_name;
                        ?>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.META_DESCRIPTION')); ?> <?php endif; ?>
                           (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                        <input type="text" id="meta_description_<?php echo $get_lang_name; ?>"  name="meta_description_<?php echo $get_lang_name; ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_META_DESCRIPTION')); ?> <?php endif; ?>  <?php echo e($get_lang_name); ?>" style="height:50px;" class="form-control span5" value="<?php echo Input::old('meta_description_'.$get_lang_name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <label  for="text1"><?php if(Lang::has(Session::get('lang_file').'.WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WEBSITE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WEBSITE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                        <input type="url" class="form-control span5" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STORE_WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STORE_WEBSITE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_STORE_WEBSITE')); ?> <?php endif; ?>" id="website" name="website" value="<?php echo Input::old('website'); ?>"  placeholder="http://www.example.com">
                     </div>

                     <div class="span5">
                        <?php /*
                           <label for="text1">Commission<span>(%):</span><span class="text-sub">*</span></label>
                            <input type="text" class="form-control span5" placeholder="" id="commission" name="commission" value="{!! Input::old('commission') !!}"  > */ ?>
                        <label for="pass1"><span class="text-sub"></span></label>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.UPLOAD_IMAGES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UPLOAD_IMAGES')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UPLOAD_IMAGES')); ?> <?php endif; ?>:<span class="text-sub">* ( <?php echo e((Lang::has(Session::get('lang_file').'.IMAGE_SIZE_MUST_BE')!= '') ? trans(Session::get('lang_file').'.IMAGE_SIZE_MUST_BE') : trans($OUR_LANGUAGE.'.IMAGE_SIZE_MUST_BE')); ?> <?php echo e($STORE_WIDTH); ?> x <?php echo e($STORE_HEIGHT); ?>  <?php echo e((Lang::has(Session::get('lang_file').'.PIXELS')!= '') ? trans(Session::get('lang_file').'.PIXELS') : trans($OUR_LANGUAGE.'.PIXELS')); ?> )</span>*JPG,PNG</label>
                        <input type="file" placeholder="<?php echo e($STORE_WIDTH); ?> x <?php echo e($STORE_HEIGHT); ?>" class="form-control span5"  id="file" name="file" value="<?php echo Input::old('file'); ?>" required>
                        <span style="color:red"></span>
                        <div class="gllpLatlonPicker" >
                           <div class="form-group controls-row" style="margin-top:10px; margin-bottom:10px">
                              <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STORE_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STORE_LOCATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_STORE_LOCATION')); ?> <?php endif; ?> <small class="text-sub">*</small></label>
                              <div class="">
                                 <div class="span4">
                                    <input type="text" name="location" value="<?php echo Input::old('location'); ?>" class="gllpSearchField span5 form-control" placeholder="<?php if(Lang::has(Session::get('lang_file').'.TYPE_YOUR_LOCATION_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TYPE_YOUR_LOCATION_HERE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TYPE_YOUR_LOCATION_HERE')); ?> <?php endif; ?>" style=" margin-left: 0px;">
                                 </div>
                                 <div class="span4">
                                    
                                    <input type="button" class="gllpSearchButton form-control" value="<?php if(Lang::has(Session::get('lang_file').'.SEARCH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH')); ?> <?php endif; ?>">
                                 </div>
                              </div>
                              <div class="gllpMap"><?php if(Lang::has(Session::get('lang_file').'.GOOGLE_MAPS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GOOGLE_MAPS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GOOGLE_MAPS')); ?> <?php endif; ?></div>
                              <div class="form-group controls-row" style="margin-top:10px; margin-bottom:10px">
                                 <label  for="text1"><?php if(Lang::has(Session::get('lang_file').'.LATITUDE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LATITUDE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LATITUDE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                 <input type="text" name="latitude" class="gllpLatitude form-control span5" readonly  value="<?php echo $city_det->es_latitude; ?>"  id="latitude"/> 
                                 <!-- <?php echo Input::old('latitude'); ?> -->
                              </div>
                              <div class="form-group controls-row" style="margin-top:10px; margin-bottom:10px">
                                 <label  for="text1"><?php if(Lang::has(Session::get('lang_file').'.LONGTITUDE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LONGTITUDE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LONGTITUDE')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                                 <input type="text" name="longtitude" class="gllpLongitude form-control span5" value="<?php echo e($city_det->es_longitude); ?>"  readonly id="longtitude"/>
                                 <!-- <?php echo Input::old('longtitude'); ?> -->
                                 <input type="text" class="gllpZoom"  style="visibility:hidden">
                                 <input class="gllpUpdateButton" style="visibility:hidden">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> 

                <div class="col-md-6 mrchnt-sgnup-right">        

                  <h4 style="padding:10px;background:#eee;"><?php if(Lang::has(Session::get('lang_file').'.PERSONAL_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PERSONAL_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PERSONAL_DETAILS')); ?> <?php endif; ?></h4>
                  <div class="">
                     <div class="span5">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.FIRST_NAME')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="text" maxlength="100" id="first_name" name="first_name" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_FIRST_NAME')); ?> <?php endif; ?>" class="form-control span5" value="<?php echo Input::old('first_name'); ?>" tabindex="1" >
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LAST_NAME')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="text" id="last_name" maxlength="100" class="form-control span5" name="last_name" value="<?php echo Input::old('last_name'); ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_LAST_NAME')); ?> <?php endif; ?>" tabindex="2">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.E-MAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.E-MAIL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.E-MAIL')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="email" id="email_id" class="form-control span5" name="email_id" value="<?php echo Input::old('email_id'); ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_ID')); ?> <?php endif; ?>" tabindex="3" onchange="check_email();">
                        <div id="email_id_error_msg"  style="color:#F00;font-weight:800"> </div>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.CONTACT_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONTACT_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONTACT_NUMBER')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="text" id="phone_no" maxlength="16" class="form-control span5" name="phone_no" value="<?php echo Input::old('phone_no'); ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_CONTACT_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_CONTACT_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_CONTACT_NUMBER')); ?> <?php endif; ?>"  tabindex="6" onkeypress="return isNumber(event)"  data-minlength="15" data-maxlength="15" data-error="Less Number">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS1')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="text" id="addreess_one" class="form-control span5" name="addreess_one" value="<?php echo Input::old('addreess_one'); ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS1')); ?> <?php endif; ?>" tabindex="7">
                     </div>
                     <div class="span5">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS2')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <input type="text" id="address_two" name="address_two" class="form-control span5" value="<?php echo Input::old('address_two'); ?>" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS2')); ?> <?php endif; ?>"  tabindex="8">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUNTRY')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <select class="form-control span5" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" tabindex="4" >
                           <option value="">-- <?php if(Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COUNTRY')); ?> <?php endif; ?>--</option>
                           <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?> 
                           <?php	$co_name = 'co_name'; ?>
                           <?php else: ?>  <?php  $co_name = 'co_name_'.Session::get('lang_code');  ?>
                           <?php endif; ?>
                           <option value="<?php echo e($country_fetch->co_id); ?>" <?php if(Input::old('select_mer_country')==$country_fetch->co_id){ echo "selected"; }?>><?php echo $country_fetch->$co_name; ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?> <?php endif; ?>:<span class="text-sub">*</span></label>
                        <?php if(Input::old('select_mer_city')): ?>
                        <select class="form-control span5" name="select_mer_city" id="select_mer_city" tabindex="5" >
                           <?php $__currentLoopData = $get_city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php if(Input::old('select_mer_country')==$city->ci_con_id): ?>
                           <option value="<?php echo $city->ci_id;?>" <?php if(Input::old('select_mer_city')==$city->ci_id){ echo "selected";} ?>><?php echo $city->ci_name; ?></option>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php else: ?>
                        <select class="form-control span5" name="select_mer_city" id="select_mer_city" tabindex="5" >
                           <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?>--</option>
                        </select>
                        <?php endif; ?>
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_EMAIL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_EMAIL')); ?> <?php endif; ?>:<span class="text-sub"></span></label>
                        <input type="text" class="form-control span5"  id="payment_account" value="<?php echo Input::old('payment_account'); ?>" name="payment_account" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS')); ?> <?php endif; ?>"  tabindex="9">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_EMAIL_KEY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_EMAIL_KEY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_EMAIL_KEY')); ?> <?php endif; ?>:<span class="text-sub"></span></label>
                        <input type="text" class="form-control span5"  id="payment_account_payu_key" value="<?php echo Input::old('payment_account_payu_key'); ?>" name="payment_account_payu_key" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY')); ?> <?php endif; ?>"  tabindex="9">
                        <label for="text1"><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_EMAIL_SALT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_EMAIL_SALT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_EMAIL_SALT')); ?> <?php endif; ?>:<span class="text-sub"></span></label>
                        <input type="text" class="form-control span5"  id="payment_account" value="<?php echo Input::old('payment_account_payu_salt'); ?>" name="payment_account_payu_salt" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT')); ?> <?php endif; ?>"  tabindex="9">

                     </div>
                  </div>
                  </div>     
               </div>

            </div> 
            <?php echo Form::close(); ?>


         </div>
      </div>
      </div>
      <br>
   </div>
</div>
</div>
</div> 
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
<?php echo $footer; ?> 
<!-- Placed at the end of the document so the pages load faster ============================================= -->
<!--<script src="<?php //echo url('');?>/themes/js/jquery.steps.js"></script>-->
<script src="<?php echo e(url('')); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo e(url('')); ?>/themes/js/bootshop.js"></script>

<!--    <script src="<?php // echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>--> 
<script>
   $( document ).ready(function() {
   	$('#zip_code').keypress(function (q){
          if(q.which!=8 && q.which!=0 && q.which!=13 && (q.which<48 || q.which>57))
   	{
              originalprice.css('border', '1px solid red'); 
   		$('#zip_error_msg').html('<?php if (Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') { echo  trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED');}  else { echo trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED');} ?>');
   		originalprice.focus();
   		return false;
          }
   	else
   	{			
              originalprice.css('border', ''); 
   		$('#zip_error_msg').html('');	        
   	}
          });
   
   	
   	$('.close').click(function() {
   		$('.alert').hide();
   	});
   $('#submit').click(function() { 
      var file		 	 = $('#file');
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        	if(file.val() == "")
   		{
   		file.focus();
   	file.css('border', '1px solid red'); 		
   	return false;
   	}			
   	else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
   	file.focus();
   	file.css('border', '1px solid red'); 		
   	return false;
   	}			
   	else
   	{
   	file.css('border', ''); 				
   	}
   });
   });
</script>

<script>
   function select_city_ajax(city_id)
   {
   	 var passData = 'city_id='+city_id;
   	 //alert(passData);
   	   $.ajax( {
   		      type: 'get',
   			  data: passData,
   			  url: '<?php echo url('ajax_select_city_frond_end'); ?>',
   			  success: function(responseText){  
   			 // alert(responseText);
   			   if(responseText)
   			   { 
   				$('#select_city').html(responseText);					   
   			   }
   			}		
   		});		
   }
   
   function select_mer_city_ajax(city_id)
   {
   	 var passData = 'city_id='+city_id;
   	// alert(passData);
   	   $.ajax( {
   		      type: 'get',
   			  data: passData,
   			  url: '<?php echo url('ajax_select_city_frond_end'); ?>',
   			  success: function(responseText){  
   			 // alert(responseText);
   			   if(responseText)
   			   { 
   				$('#select_mer_city').html(responseText);					   
   			   }
   			}		
   		});	
   }
</script>

<script src="<?php echo e(url('')); ?>/themes/js/simpleform.min.js"></script>
<script type="text/javascript"> 
   $(".testform").simpleform({
   	speed : 500,
   	transition : 'fade',
   	progressBar : true,
   	showProgressText : true,
   	validate: true
   });
   
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
   	//alert(responseText);
   	$('#email_id_error_msg').html(responseText);	
   	if(responseText!=''){
   		$("#email_id").css('border', '1px solid red'); 
   		$("#email_id").focus();
   	}
   	else
   		$("#email_id").css('border', '1px solid #ccc'); 
   
   	
   	
   	
   }		
   });	
   }
   
   /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
   
   function validateForm(formID, Obj)
   {   
   	switch(formID){
   		case 'testform' :
   			Obj.validate({
   				rules: {
   					email: {
   						required: true,
   						email: true
   					},
   					store_name: {
   						required: true
   					},
   					store_name_french: {
   						required: true
   					},
   					store_pho: {
   						required: true
   					},
   					store_add_one: {
   						required: true
   					},
   					store_add_one_french: {
   						required: true
   					},
   					store_add_two: {
   						required: true
   					},
   					store_add_two_french: {
   						required: true
   					},
   					
   					zip_code: {
   						required: true
   					},
   					meta_keyword: {
   						required: true
   					},
   					meta_keyword_french: {
   						required: true
   					},
   					meta_description: {
   						required: true
   					},
   					meta_description_french: {
   						required: true
   					},
   					website: {
   						required: true
   					},
   					location: {
   						required: true
   					},
   					commission: {
   						required: true
   					},
   					file: {
   						required: true
   					},
   					select_country: {
   						required: true
   					},
   					select_city: {
   						required: true
   					},
   					email_id: {
   						required: true,
   						email_id: true
   					},
   					first_name: {
   						required: true
   					},
   					select_mer_city: {
   						required: true,
   						
   					},
   					addreess_one: {
   						required: true
   					},
   					payment_account: {
   						required: true
   					},
   					
   					last_name: {
   						required: true
   					},
   					select_mer_country: {
   						required: true
   					},
   					phone_no: {
   						required: true
   					},
   					address_two: {
   						required: true
   					}
   					
   					
   				},
   				messages: {
   					email: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?> <?php endif; ?>",
   						email: "<?php if(Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')); ?> <?php endif; ?>"
   					},
   					store_name: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_STORE_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_STORE_NAME')); ?> <?php endif; ?>"
   					},
   					store_name_french: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_STORE_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_STORE_NAME')); ?> <?php endif; ?> "
   					},
   					store_pho: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PHONE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PHONE_NO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PHONE_NO')); ?> <?php endif; ?>"
   					},
   					store_add_one: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')); ?> <?php endif; ?> "
   					},
   					store_add_one_french: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')); ?> <?php endif; ?> <?php echo e(Helper::lang_name()); ?>"
   					},
   					
   					store_add_two: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')); ?> <?php endif; ?> "
   					},
   					store_add_two_french: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')); ?> <?php endif; ?>  "
   					},
   					zip_code: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ZIPCODE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ZIPCODE')); ?> <?php endif; ?>"
   					},
   					meta_keyword: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_META_KEYWORDS')); ?> <?php endif; ?>   "
   					},
   					meta_keyword_french: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_META_KEYWORDS')); ?> <?php endif; ?>  "
   					},
   					meta_description: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_META_DESCRIPTION')); ?> <?php endif; ?>   "
   					},
   					meta_description_french: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_META_DESCRIPTION')); ?> <?php endif; ?>  "
   					},
   					website: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_WEBSITE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_WEBSITE')); ?> <?php endif; ?>"
   					},
   					location: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_LOCATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_LOCATION')); ?> <?php endif; ?>"
   					},
   					
   					commission: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_COMMISSION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_COMMISSION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_COMMISSION')); ?> <?php endif; ?>"
   					},
   					file: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')); ?> <?php endif; ?>"
   					},
   					select_country: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?> <?php endif; ?>"
   					},
   					select_city: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_CITY')); ?> <?php endif; ?>"
   					},
   					
   					email_id: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?> <?php endif; ?>",
   						email: "<?php if(Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')); ?> <?php endif; ?>"
   					},
   					first_name: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?> <?php endif; ?>"
   					},
   					select_mer_city: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_CITY')); ?> <?php endif; ?>",
   						
   					},
   					addreess_one: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')); ?> <?php endif; ?>"
   					},
   					payment_account: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PAYMENT_ACCOUNT')); ?> <?php endif; ?>"
   					},
   					
   					last_name: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_LAST_NAME')); ?> <?php endif; ?>"
   					},
   					select_mer_country: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?> <?php endif; ?>"
   					},
   					phone_no: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PHONE_NO')); ?> <?php endif; ?>"
   					},
   					address_two: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')); ?> <?php endif; ?>"
   					}
   					
   				}
   			});
   		return Obj.valid();
   		break;
   
   		case 'testform2' :
   			Obj.validate({
   				rules: {
   					email_id: {
   						required: true,
   						email_id: true
   					},
   					first_name: {
   						required: true
   					},
   					select_mer_city: {
   						required: true,
   						
   					},
   					addreess_one: {
   						required: true
   					},
   					payment_account: {
   						required: true
   					},
   					
   					last_name: {
   						required: true
   					},
   					select_mer_country: {
   						required: true
   					},
   					phone_no: {
   						required: true
   					},
   					address_two: {
   						required: true
   					}
   				},
   				messages: {
   					email_id: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?> <?php endif; ?>",
   						email: "<?php if(Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')); ?> <?php endif; ?>"
   					},
   					first_name: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?> <?php endif; ?>"
   					},
   					select_mer_city: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_CITY')); ?> <?php endif; ?>",
   						
   					},
   					addreess_one: {
   					 	required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')); ?> <?php endif; ?>"
   					},
   					payment_account: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PAYMENT_ACCOUNT')); ?> <?php endif; ?>"
   					},
   					
   					last_name: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_LAST_NAME')); ?> <?php endif; ?>"
   					},
   					select_mer_country: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?> <?php endif; ?>"
   					},
   					phone_no: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PHONE_NO')); ?> <?php endif; ?>"
   					},
   					address_two: {
   						required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')); ?> <?php endif; ?>"
   					}
   				}
   			});
   		return Obj.valid();
   		break;
   	}
   }
</script>

<script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   
</script>
<script>
   $.noConflict($);
</script>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
<script src="<?php echo url(''); ?>/themes/js/jquery-gmaps-latlon-picker.js"></script>

<!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript">
   var map;
   
   function initialize() {
   var myLatlng = new google.maps.LatLng(24.88892693527280,67.00149512695316); 
       var mapOptions = {
   
          zoom: 20,
               center: myLatlng,
               disableDefaultUI: true,
               panControl: true,
               zoomControl: true,
               mapTypeControl: true,
               streetViewControl: true,
               mapTypeId: google.maps.MapTypeId.ROADMAP
   
       };
   
       map = new google.maps.Map(document.getElementById('map_canvas'),
           mapOptions);
     var marker = new google.maps.Marker({
               position: myLatlng,
               map: map,
   visible: false,
   draggable:true,    
           });	
   google.maps.event.addListener(marker, 'dragend', function(e) {
   		 
   var lat = this.getPosition().lat();
   	 var lng = this.getPosition().lng();
   $('#latitude').val(lat);
   $('#longtitude').val(lng);
   });
       var input = document.getElementById('pac-input');
       var autocomplete = new google.maps.places.Autocomplete(input);
       autocomplete.bindTo('bounds', map);
   
       google.maps.event.addListener(autocomplete, 'place_changed', function () {
   
           var place = autocomplete.getPlace();
   
           if (place.geometry.viewport) {
               map.fitBounds(place.geometry.viewport);
   var myLatlng = place.geometry.location;	
   //alert(place.geometry.location);			
   var marker = new google.maps.Marker({
                position: myLatlng,
    visible: true,
               map: map,
   draggable:true,   
           });	
   google.maps.event.addListener(marker, 'dragend', function(e) {
   		 
   var lat = this.getPosition().lat();
   	 var lng = this.getPosition().lng();
   $('#latitude').val(lat);
   $('#longtitude').val(lng);
   });
           } else {
               map.setCenter(place.geometry.location);	
   
               map.setZoom(17);
           }
       });
   
   
   
   }
   
   
   google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
   $.ajaxSetup({
   	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<!-- Themes switcher section ============================================================================================= -->
</body>
</html>