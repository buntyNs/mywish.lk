<!DOCTYPE html>

<html lang="en">

<?php echo $navbar; ?>


<?php echo $header; ?>




<body class="orders_list_page">



<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

  

  <!-- Header -->

  

  <!-- end header -->

   

  

  <!-- Breadcrumbs -->

  

  

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <section class="main-container col2-right-layout">

    <div class="main container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12">

          <div class="my-account">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.ORDERS_LIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDERS_LIST')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDERS_LIST')); ?> <?php endif; ?></h2>

            </div>

             



            <?php $__currentLoopData = $customerdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <div class="row">

          <div class="col-md-12 text-right">

             <div class="btn btn-large btn-primary me_btn cart-res"><?php if(Lang::has(Session::get('lang_file').'.TOTAL_WALLET_BALANCE_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOTAL_WALLET_BALANCE_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TOTAL_WALLET_BALANCE_AMOUNT')); ?> <?php endif; ?> : <?php echo e(Helper::cur_sym()); ?><?php echo e((($customer_info->wallet!="")?number_format($customer_info->wallet,2):'0.00')); ?></div>

           </div>

         </div>



            <div class="orders-list table-responsive"> 

              

              <!--orders list table-->

              <table class="table table-bordered cart_summary table-striped">

                <thead>

                  <tr> 

                    <!--titles for td-->

                    <th><?php if(Lang::has(Session::get('lang_file').'.ORDER_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_NUMBER')); ?> <?php endif; ?></th>

                    <th><?php if(Lang::has(Session::get('lang_file').'.ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_DATE')); ?> <?php endif; ?></th>

                    <th><?php if(Lang::has(Session::get('lang_file').'.TOTAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOTAL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TOTAL')); ?> <?php endif; ?></th>

                    <th><?php if(Lang::has(Session::get('lang_file').'.USED_WALLET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.USED_WALLET')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.USED_WALLET')); ?> <?php endif; ?></th>

                    <th><?php if(Lang::has(Session::get('lang_file').'.ACTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ACTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ACTION')); ?> <?php endif; ?></th>

                  </tr>

                </thead>

                <tbody>

                  <?php 

   if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

      

       $deal_title    = 'deal_title';

   }else {  

      

       $deal_title    = 'deal_title_'.Session::get('lang_code');

   } 

   ?>

                     <?php if(count($get_deal_PayPal)<1): ?> 

                              <tr>

                                 <td colspan="6" class="text-center"><?php if(Lang::has(Session::get('lang_file').'.NO_ORDER_DEAL_PAYPAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_ORDER_DEAL_PAYPAL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_ORDER_DEAL_PAYPAL')); ?> <?php endif; ?></td>

                              </tr>

                              <?php endif; ?>

                 <?php $j=1; $item_tax = 0; ?>

                           <?php $__currentLoopData = $get_deal_PayPal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderdet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                           <?php

                              $walletusedamt_final1=DB::table('nm_ordercod_wallet')->where('nm_ordercod_wallet.cod_transaction_id','=', $orderdet->transaction_id)->get();

                              

                              $item_amt = $orderdet->order_amt + (($orderdet->order_amt * $orderdet->order_tax)/100);

                                  

                              

                                   $ship_amt = $orderdet->order_shipping_amt;

                                  

                                  

                                    $grand_total =  ($item_amt + $ship_amt + $item_tax);

                              

                              ?>

                  <tr> 

                    <!--order number-->

                    <td data-title="Order Number"><?php echo e($orderdet->transaction_id); ?></td>

                    <!--order date-->

                    <td data-title="Order Date"><?php echo e($orderdet->order_date); ?></td>

                    <!--order status-->

                    <td data-title="Total">

                       <?php

                                    $customerid   = Session::get('customerid');

                                      $product_titles=DB::table('nm_order')

                                      ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')

                                      ->leftjoin('nm_color', 'nm_order.order_pro_color', '=', 'nm_color.co_id')

                                      ->leftjoin('nm_size', 'nm_order.order_pro_size', '=', 'nm_size.si_id')

                                      ->where('transaction_id', '=', $orderdet->transaction_id)

                                      ->where('nm_order.order_cus_id', '=', $customerid)

                                      ->where('nm_order.order_type','=',2)

                                      ->get();

                                      $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0;

                                      $wallet_amt =  $wallet     = 0;

                                    

                                      /*$wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$orderdet->transaction_id)->get();

                                    

                                    if(count($wallet)!=0){

                                        $wallet_amt = $wallet[0]->wallet_used;

                                        }else{

                                        $wallet_amt = 0;

                                      }*/

                                      ?>

                                 <?php $__currentLoopData = $product_titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prd_tit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                 <?php

                                    $subtotal=$prd_tit->order_amt; 

                                            $tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);

                                          

                                            $total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 

                                            $total_ship_amt+= $prd_tit->order_shipping_amt;

                                            $total_item_amt+=$prd_tit->order_amt;

                                            $coupon_amount+= $prd_tit->coupon_amount;

                                            $prodct_id = $prd_tit->order_pro_id;

                                            

                                        $item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);

                                          

                                        

                                           $ship_amt = $prd_tit->order_shipping_amt;

                                          

                                        

                                           //$item_tax = $codorderdet->cod_tax;

                                          /*if($prd_tit->coupon_amount != 0)

                                          {

                                            $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);

                                          }

                                          else

                                          {

                                            $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);

                                          }*/   

                                          $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);

                                          $wallet_amt +=  $prd_tit->wallet_amount;

                                              $wallet     += $prd_tit->wallet_amount; ?>

                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                      <span class="order-total"><?php echo e(Helper::cur_sym()); ?> <?php echo e($grand_total-$wallet_amt); ?></span></td>

                      <!-- wallet amount -->

                      <td data-title="Total">  <?php echo e(Helper::cur_sym()); ?>                                  

                                 <?php echo e(((count($walletusedamt_final1)>0)?" ".number_format($walletusedamt_final1[0]->wallet_used,2):'0')); ?>


                                  

                                  </td>

                    <!--quanity-->

                    <td data-title="Total"> <a   class="btn btn-success" href="<?php echo e(url('deal_paypal_inv').'/'.$orderdet->transaction_id); ?>" target="new"><?php if(Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VIEW_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?> <?php endif; ?></a>



                                      

                                         <?php    

                                    //cancel ,return and replacement process starts

                                             $paypal_cancel_valid = 0;

                                             $paypal_return_replace_valid = 0;

                                             $cod_cancel_valid = 0;

                                             $cod_return_replace_valid= 0;

                                    

                                             /* cancel starts */

                                           

                                             $cancel_valid =  DB::table('nm_order')

                                    ->join('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')             

                                    ->orderBy('nm_order.order_id', 'desc')

                                    ->where('nm_order.order_type', '=', 2)

                                    ->where('nm_order.transaction_id', '=', $orderdet->transaction_id)

                                    ->where('delivery_status','=',1)->get();

                                             $paypal_cancel_valid =  count($cancel_valid);

                                             $return_replace =  DB::table('nm_order')

                                    ->join('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')  

                                    ->orderBy('nm_order.order_id', 'desc')

                                    ->where('nm_order.order_type', '=', 2)

                                    ->where('nm_order.transaction_id', '=',$orderdet->transaction_id)

                                    ->where('delivery_status','=',4)->get();

                                             $paypal_return_replace_valid =  count($return_replace);

                                    

                                             //cancel ,return and replacement process ends

                                             ?>

                                 <?php if($paypal_cancel_valid>0): ?>

                                 <?php 

                                    $cancel_allow = 0;

                                    $cancel_allowed_itm  = array();

                                    //check Cancel date

                                    ?>

                                 <?php $__currentLoopData = $cancel_valid; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cancelItm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                 <?php

                                    $order_date = $cancelItm->order_date;

                                    $now = time(); //current date

                                    $format_date = strtotime($order_date);              

                                    $datediff = abs($now - $format_date);

                                    

                                    $numberDays = $datediff/86400;  // 86400 seconds in one day

                                    

                                    // and you might want to convert to integer

                                    $Ord_datecount = intval($numberDays);

                                    ?>

                                 <?php if($cancelItm->allow_cancel==1): ?>

                                 <?php if($Ord_datecount<$cancelItm->cancel_days): ?>

                                 <?php

                                    $delStatusInfo = DB::table('nm_order_delivery_status')->where('order_id','=',$cancelItm->order_id)->get(); 

                                    //print_r($delStatusInfo);

                                    ?>

                                 <?php if(count($delStatusInfo)==0): ?>

                                 <?php

                                    $cancel_allowed_itm[]  = array('prod_id' => $cancelItm->order_id,'prod_title' => $cancelItm->$deal_title );

                                    $cancel_allow++; 

                                    ?>

                                 <?php endif; ?>

                                 <?php endif; ?>

                                 <?php endif; ?>

                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                 <?php if($cancel_allow>0): ?>

                                 <a class="btn btn-warning" data-target="<?php echo '#cancelModal_dp'.$j;?>" data-toggle="modal"><?php if (Lang::has(Session::get('lang_file').'.CANCELLATION')!= '') { echo  trans(Session::get('lang_file').'.CANCELLATION');}  else { echo trans($OUR_LANGUAGE.'.CANCELLATION');} ?></a>

                                 <!-- Modal -->

                                 <div id="<?php echo 'cancelModal_dp'.$j;?>" class="modal fade" role="dialog">

                                    <div class="modal-dialog">

                                       <!-- Modal content-->

                                       <div class="modal-content">

                                          <div class="modal-header">

                                             <button type="button" class="close" data-dismiss="modal">&times;</button>

                                             <h4 class="modal-title"><?php if(Lang::has(Session::get('lang_file').'.CANCELLATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CANCELLATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CANCELLATION')); ?> <?php endif; ?></h4>

                                          </div>

                                          <div class="modal-body">

                                             <?php echo Form::open(array('url'=>'deal_cancel_order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>


                                             <?php if($paypal_cancel_valid>0): ?>

                                             <input type="hidden" name="customer_mail" value="<?php echo e($orderdet->ship_email); ?>">

                                             <input type="hidden" name="order_payment_type" value="1"><input type="hidden" name="order_type" value="2"><input type="hidden" name="mer_id" value="<?php echo e($orderdet->order_merchant_id); ?>">

                                             <div class="form-group">

                                                <label for="cancel_item_id">

                                                <?php if(Lang::has(Session::get('lang_file').'.SELECT_ITEM_TO_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_ITEM_TO_CANCEL')); ?>   <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_ITEM_TO_CANCEL')); ?> <?php endif; ?>

                                                </label>  

                                                <select id = "cancel_item_id" name="cancel_item_id">

                                                   <?php if(count($cancel_allowed_itm)>0): ?>

                                                   <?php $__currentLoopData = $cancel_allowed_itm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                   <option value="<?php echo e($row['prod_id']); ?>"> <?php echo e($row['prod_title']); ?></option>

                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                   <?php else: ?>

                                                   <option value=""> No Item</option>

                                                   <?php endif; ?> 

                                                </select>

                                             </div>

                                             <div class="form-group">

                                                <label for="cancel_notes">

                                                <?php if(Lang::has(Session::get('lang_file').'.REASON_FOR_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REASON_FOR_CANCEL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REASON_FOR_CANCEL')); ?> <?php endif; ?>

                                                </label>

                                                <textarea id="<?php echo '#cancel_notes'.$j;?>" class="cancel_dealnotes_<?php echo $j; ?>" name="cancel_notes" maxlength="300"  placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_CANCEL')); ?> <?php endif; ?>"></textarea>

                                             </div>

                                             <?php endif; ?>

                                          </div>

                                          <div class="modal-footer">

                                             <button  type="submit" onclick="return cancel_dealvalidate('<?php echo $j; ?>');" class="btn btn-danger conform_cancel" id="<?php echo '#conform_cancel'.$j;?>" ><?php if(Lang::has(Session::get('lang_file').'.CONFIRM_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONFIRM_CANCEL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONFIRM_CANCEL')); ?>    <?php endif; ?></button>

                                             <button type="button" class="btn btn-danger" data-dismiss="modal"><?php if(Lang::has(Session::get('lang_file').'.CLOSE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CLOSE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CLOSE')); ?> <?php endif; ?></button>

                                          </div>

                                          <?php echo Form::close(); ?>


                                       </div>

                                    </div>

                                 </div>

                                 <?php endif; ?> 

                                 <?php endif; ?>

                                 <?php /* cancel end */ ?>

                                 <?php if($paypal_return_replace_valid>0): ?>

                                 <?php 

                                    $return_allow = $replace_allow = 0;

                                    $return_allowed_itm  = array();

                                    $replace_allowed_itm  = array();

                                    //check Cancel date 

                                    ?>

                                 <?php $__currentLoopData = $return_replace; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Itm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                 <?php

                                    $order_date = $Itm->order_date; 

                                              $now = time(); //current date

                                    $format_date = strtotime($order_date);              

                                    $datediff = abs($now - $format_date);

                                    

                                    $numberDays = $datediff/86400;  // 86400 seconds in one day

                                    

                                    // and you might want to convert to integer

                                    $Ord_datecount = intval($numberDays);

                                    ?>

                                 <?php if($Itm->allow_return==1): ?> 

                                 <?php if($Ord_datecount<$Itm->return_days): ?>  

                                 <?php

                                    $delStatusInfo = DB::table('nm_order_delivery_status')->where('order_id','=',$Itm->order_id)->get();  

                                    //print_r($delStatusInfo); 

                                    ?>

                                 <?php if(count($delStatusInfo)==0): ?>

                                 <?php

                                    $return_allowed_itm[]  = array('prod_id' => $Itm->order_id,'prod_title' => $Itm->$deal_title );

                                    $return_allow++; 

                                    ?>

                                 <?php endif; ?>

                                 <?php endif; ?>

                                 <?php endif; ?> 

                                 <?php if($Itm->allow_replace==1): ?>

                                 <?php if($Ord_datecount<$Itm->replace_days): ?>

                                 <?php

                                    $delStatusInfo = DB::table('nm_order_delivery_status')->where('order_id','=',$Itm->order_id)->get(); 

                                    //print_r($delStatusInfo);

                                    

                                    ?>

                                 <?php if(count($delStatusInfo)==0): ?>

                                 <?php

                                    $replace_allowed_itm[]  = array('prod_id' => $Itm->order_id,'prod_title' => $Itm->$deal_title );

                                    $replace_allow++;

                                    ?>              

                                 <?php endif; ?>

                                 <?php endif; ?>

                                 <?php endif; ?>

                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                 <?php if($return_allow>0): ?>

                                 <a class="btn btn-blue"  data-target="<?php echo '#returnModal_dp'.$j;?>" data-toggle="modal"><?php if(Lang::has(Session::get('lang_file').'.RETURN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RETURN')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RETURN')); ?> <?php endif; ?></a>

                                 <!-- REturn Modal -->

                                 <div id="<?php echo 'returnModal_dp'.$j;?>" class="modal fade" role="dialog">

                                    <div class="modal-dialog">

                                       <!-- Modal content-->

                                       <div class="modal-content">

                                          <div class="modal-header">

                                             <button type="button" class="close" data-dismiss="modal">&times;</button>

                                             <h4 class="modal-title"><?php if(Lang::has(Session::get('lang_file').'.ORDER_RETURN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_RETURN')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_RETURN')); ?>      <?php endif; ?></h4>

                                          </div>

                                          <div class="modal-body">

                                             <?php echo Form::open(array('url'=>'deal_return_order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>


                                             <?php if($return_allow>0): ?>

                                             <input type="hidden" name="customer_mail" value="<?php echo e($orderdet->ship_email); ?>">

                                             <input type="hidden" name="order_payment_type" value="1"><input type="hidden" name="order_type" value="2">

                                             <div class="form-group">

                                                <label for="return_item_id">

                                                <?php if(Lang::has(Session::get('lang_file').'.SELECT_ITEM_TO_RETURN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_ITEM_TO_RETURN')); ?>   <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_ITEM_TO_RETURN')); ?>   <?php endif; ?>

                                                </label>  

                                                <select id = "return_item_id" name="return_item_id">

                                                   <?php if(count($return_allowed_itm)>0): ?>

                                                   <?php $__currentLoopData = $return_allowed_itm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                   <option value="<?php echo e($row['prod_id']); ?>"> <?php echo e($row['prod_title']); ?></option>

                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                   <?php else: ?>

                                                   <option value=""> No Item</option>

                                                   <?php endif; ?> 

                                                </select>

                                             </div>

                                             <div class="form-group">

                                                <label for="return_notes">

                                                <?php if(Lang::has(Session::get('lang_file').'.REASON_FOR_RETURN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REASON_FOR_RETURN')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REASON_FOR_RETURN')); ?>       <?php endif; ?> 

                                                </label>

                                                <textarea id="return_notes" class="return_dealnotes_<?php echo $j; ?>" maxlength="300" name="return_notes" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_RETURN')); ?> <?php endif; ?>"></textarea>

                                             </div>

                                             <?php endif; ?>

                                          </div>

                                          <div class="modal-footer">

                                             <button  type="submit" onclick="return return_dealvalidate('<?php echo $j; ?>');" class="btn btn-danger return_conform" ><?php if(Lang::has(Session::get('lang_file').'.CONFIRM_RETURN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONFIRM_RETURN')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONFIRM_RETURN')); ?> <?php endif; ?></button>

                                             <button type="button" class="btn btn-danger" data-dismiss="modal"><?php if(Lang::has(Session::get('lang_file').'.CLOSE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CLOSE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CLOSE')); ?> <?php endif; ?></button>

                                          </div>

                                          <?php echo Form::close(); ?>


                                       </div>

                                    </div>

                                 </div>

                                 <?php endif; ?>

                                 <?php if($replace_allow>0): ?>

                                 <a class="btn btn-info"  data-target="<?php echo '#replaceModal_dp'.$j;?>" data-toggle="modal"><?php if(Lang::has(Session::get('lang_file').'.REPLACE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REPLACE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REPLACE')); ?> <?php endif; ?></a>

                                 <!-- REturn Modal -->

                                 <div id="<?php echo 'replaceModal_dp'.$j;?>" class="modal fade" role="dialog">

                                    <div class="modal-dialog">

                                       <!-- Modal content-->

                                       <div class="modal-content">

                                          <div class="modal-header">

                                             <button type="button" class="close" data-dismiss="modal">&times;</button>

                                             <h4 class="modal-title"><?php if(Lang::has(Session::get('lang_file').'.ORDER_REPLACE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_REPLACE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_REPLACE')); ?> <?php endif; ?></h4>

                                          </div>

                                          <div class="modal-body">

                                             <?php echo Form::open(array('url'=>'deal_replace_order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>


                                             <?php if($return_allow>0): ?>

                                             <input type="hidden" name="customer_mail" value="<?php echo e($orderdet->ship_email); ?>">

                                             <input type="hidden" name="order_payment_type" value="1"><input type="hidden" name="order_type" value="2">

                                             <div class="form-group">

                                                <label for="replace_item_id">

                                                <?php if(Lang::has(Session::get('lang_file').'.SELECT_ITEM_TO_REPLACE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_ITEM_TO_REPLACE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_ITEM_TO_REPLACE')); ?> <?php endif; ?>

                                                </label>  

                                                <select id = "replace_item_id" name="replace_item_id">

                                                   <?php if(count($replace_allowed_itm)>0): ?>

                                                   <?php $__currentLoopData = $replace_allowed_itm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                   <option value="<?php echo e($row['prod_id']); ?>"> <?php echo e($row['prod_title']); ?></option>

                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                   <?php else: ?>

                                                   <option value=""> No Item</option>

                                                   <?php endif; ?> 

                                                </select>

                                             </div>

                                             <div class="form-group">

                                                <label for="replace_notes">

                                                <?php if(Lang::has(Session::get('lang_file').'.REASON_FOR_REPLACE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REASON_FOR_REPLACE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REASON_FOR_REPLACE')); ?> <?php endif; ?>

                                                </label>

                                                <textarea id="replace_notes" class="replace_dealnotes_<?php echo $j; ?>" name="replace_notes" maxlength="300" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_REPLACE')); ?> <?php endif; ?>"></textarea>

                                             </div>

                                             <?php endif; ?>

                                          </div>

                                          <div class="modal-footer">

                                             <button onclick="return replace_dealvalidate('<?php echo $j; ?>');" type="submit" class="btn btn-danger replace_conform"  ><?php if(Lang::has(Session::get('lang_file').'.CONFIRM_REPLACE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONFIRM_REPLACE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONFIRM_REPLACE')); ?> <?php endif; ?></button>

                                             <button type="button" class="btn btn-danger" data-dismiss="modal"><?php if(Lang::has(Session::get('lang_file').'.CLOSE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CLOSE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CLOSE')); ?> <?php endif; ?></button>

                                          </div>

                                          <?php echo Form::close(); ?>


                                       </div>

                                    </div>

                                 </div>

                                 <?php endif; ?>

                                 <?php endif; ?>

                                    



                    </td>

                   

                  </tr>

                  

                  

                 <?php $j=$j+1;  ?>

                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  

                  

                  

                  

                  

                  

                  

                </tbody>

              </table>

            </div>

               <div class="pagination-area">

      

              <?php echo $get_deal_PayPal->render(); ?>


              

              

          </div>

          </div>



        </div>

         <?php echo $__env->make('dashboard_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

    </div>

  </section>

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <!-- Footer -->

  <?php echo $footer; ?>


  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<!-- JS --> 

<script type="text/javascript">

function cancel_dealvalidate(id){

  var cancel_id = $(".cancel_dealnotes_"+id).val();

  if(cancel_id == ''){

    alert("<?php echo e((Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_CANCEL') : trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_CANCEL')); ?>");

    return false;

    }

  }


  //for return validate

  function return_dealvalidate(id){

  var cancel_id = $(".return_dealnotes_"+id).val();

  if(cancel_id == ''){

    alert("<?php echo e((Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_RETURN') : trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_RETURN')); ?>");

    return false;

    }

  }

  

  //for replace validate

  function replace_dealvalidate(id){

  var cancel_id = $(".replace_dealnotes_"+id).val();

  if(cancel_id == ''){

    alert("<?php echo e((Lang::has(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE')!= '') ? trans(Session::get('lang_file').'.ENTER_YOUR_REASON_FOR_REPLACE') : trans($OUR_LANGUAGE.'.ENTER_YOUR_REASON_FOR_REPLACE')); ?>");

    return false;

    }

  }

  

</script>



</body>


</html>