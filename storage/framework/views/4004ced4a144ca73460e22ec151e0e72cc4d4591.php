   <?php $current_route = Route::getCurrentRoute()->uri(); ?>
    <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"><?php echo e((Lang::has(Session::get('mer_lang_file').'.PRODUCTS1')!= '') ? trans(Session::get('mer_lang_file').'.PRODUCTS1') : trans($MER_OUR_LANGUAGE.'.PRODUCTS1')); ?></h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
              <!--  <li class="panel">
                    <a href="#">
                        <i class="icon-dashboard"></i>&nbsp;Products Dashboard</a>                   
                </li>-->
 					<li <?php if($current_route == 'mer_add_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                  
                    <a href="<?php echo e(url('mer_add_product')); ?>" >
                        <i class=" icon-plus-sign"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.ADD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.ADD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.ADD_PRODUCTS')); ?>

	                </a>                   
                </li>
				<li <?php if($current_route == 'mer_product_bulk_upload' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo e(url('mer_product_bulk_upload')); ?>" >
                        <i class=" icon-upload"></i>&nbsp; <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_BULK_UPLOAD')!= '') ? trans(Session::get('mer_lang_file').'.MER_PRODUCT_BULK_UPLOAD') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_BULK_UPLOAD')); ?>

                   </a>                   
                </li>
                <li <?php if($current_route == 'mer_manage_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo e(url('mer_manage_product')); ?>" >
                        <i class=" icon-edit"></i>&nbsp; <?php echo e((Lang::has(Session::get('mer_lang_file').'.MANAGE_PRODUCTS')!= '') ? trans(Session::get('mer_lang_file').'.MANAGE_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MANAGE_PRODUCTS')); ?>

                   </a>                   
                </li>
	 <li <?php if($current_route == 'mer_sold_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo e(url('mer_sold_product')); ?>" >
                        <i class="icon-tag"></i>&nbsp; <?php echo e((Lang::has(Session::get('mer_lang_file').'.SOLD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.SOLD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.SOLD_PRODUCTS')); ?>

                   </a>                   
                </li>
				 <li <?php if( $current_route == "mer_manage_product_shipping_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo e(url('mer_manage_product_shipping_details')); ?>" >
                        <i class="icon-ambulance"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.SHIPPING_AND_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.SHIPPING_AND_DELIVERY') : trans($MER_OUR_LANGUAGE.'.SHIPPING_AND_DELIVERY')); ?> 
                   </a>                   
                </li>
                 <li <?php if( $current_route == "mer_product_payu_shipping_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo e(url('mer_product_payu_shipping_details')); ?>" >
                        <i class="icon-ambulance"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY') : trans($MER_OUR_LANGUAGE.'.PAYU_SHIPPING_DELIVERY')); ?> 
                   </a>                   
                </li>
			<?php $general=DB::table('nm_generalsetting')->get(); ?>
      <?php $__currentLoopData = $general; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
      <?php if($gs->gs_payment_status == 'COD'): ?>  	
      <li <?php if( $current_route == "mer_manage_cashondelivery_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo e(url('mer_manage_cashondelivery_details')); ?>" >
                        <i class="icon-money"></i>&nbsp; <?php echo e((Lang::has(Session::get('mer_lang_file').'.CASH_ON_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.CASH_ON_DELIVERY') : trans($MER_OUR_LANGUAGE.'.CASH_ON_DELIVERY')); ?>

                   </a>                   
                </li><?php endif; ?>
            </ul>

        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>