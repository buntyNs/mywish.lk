<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEALS_DASHBOARD')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEALS_DASHBOARD')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS_DASHBOARD')); ?> <?php endif; ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
	
	
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>	
    <link href="<?php echo e(url('')); ?>/public/assets/css/datepicker.css" rel="stylesheet">	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />

 <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->


		<div></div>
<?php 
$sold_cnt=0; ?>
 <?php $__currentLoopData = $sold_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $soldres): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($soldres->deal_no_of_purchase >=$soldres->deal_max_limit): ?>
		
		<?php  $sold_cnt++; ?> 
		<?php endif; ?>
        
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?><?php endif; ?></a></li>
                                <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS')); ?><?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-dashboard"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEALS_DASHBOARD')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEALS_DASHBOARD')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS_DASHBOARD')); ?> <?php endif; ?></h5>
            
        </header>
        <br>
        
        
        	
           
            	<div class="col-lg-12">
                    <div class="col-lg-12">
                 <a style="color:#fff" href="<?php echo url(''); ?>" class="btn btn-success btn-sm btn-grad" target="_blank" ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_GO_TO_LIVE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_GO_TO_LIVE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_GO_TO_LIVE')); ?><?php endif; ?></a>
                 
                  <a style="color:#fff" href="<?php echo e(url('')); ?>/deals_all_orders" class="btn btn-warning btn-sm btn-grad" target="_blank" ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEE_ALL_TRANSACTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEE_ALL_TRANSACTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEE_ALL_TRANSACTION')); ?><?php endif; ?></a>
  
                </div>
                </div>
                
            
        
        
        <div class="row">
                <div class="col-lg-6" style="padding:30px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_DEALS_COUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TOTAL_DEALS_COUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_DEALS_COUNT')); ?><?php endif; ?></strong> 
                            <a href="#"><i class="icon icon-align-justify pull-right"></i></a>
                        </div>
                        <div class="panel-body">
                            <?php if(($active_count+$archievd_count+$inactive_cnt)>0): ?>
                           <div id="chart6" style="margin-top:20px; margin-left:20px; width:450px; height:450px;"></div>
                            <table width="100%" border="0">
                              <tr>
                                <td style="background:#4bb2c5">								
                                    <label class="label label-active"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ACTIVE_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE_DEALS')); ?> <?php endif; ?></label>
                                     <span class="label label-danger"><?php echo e($active_count); ?></span>
                                </td>
                                <td style="background:#eaa228">
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EXPIRED_DEALS')); ?> <?php endif; ?></label>
                                     <span class="label label-danger"><?php echo e($archievd_count); ?></span>
                                </td>
                                <td style="background:#C5B47F">
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_INACTIVE_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_INACTIVE_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_INACTIVE_DEALS')); ?> <?php endif; ?></label>
                                     <span class="label label-danger"><?php echo e($inactive_cnt); ?></span>
                                </td>
                              </tr>
                            </table>
                            <?php else: ?>
                                No details found.
                            <?php endif; ?>    
                        </div>
                       
                        	

                        
                    </div>
                </div>
                <div class="col-lg-5 " style="padding-top:30px"> 
                    <div class="panel panel-default">
                        <div class="panel-heading">	
                            <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MARKET_RESPONSE_RATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MARKET_RESPONSE_RATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MARKET_RESPONSE_RATE')); ?> <?php endif; ?></strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="" width="100%">
                                    
                                    <tbody>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_DEALS_COUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TOTAL_DEALS_COUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_DEALS_COUNT')); ?> <?php endif; ?></td>
                                            <td><?php echo e($total_deals); ?> 
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$total_deals.'%"'; ?>;> 
		  <span class="sr-only">60% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?> <?php endif; ?></span>
		</div>
	      </div></td>
                                            
                                            
                                        </tr>
                                        <tr>
                                            <td>Active Deals Count</td>
                                            <td><?php echo e($active_count); ?></td>
                                            
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$active_count.'%"'; ?>;> 
                                              <span class="sr-only">60% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?><?php endif; ?></span>
                                            </div>
                                              </div></td>
                                                                                       
                                            
                                        </tr>
                                        <tr>
                                            <td>Inactive Deals Count</td>
                                            <td><?php echo e($inactive_cnt); ?></td>
                                            
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
											<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$inactive_cnt.'%"'; ?>;> 
											  <span class="sr-only">60% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?> <?php endif; ?></span>
											</div>
											  </div></td>
                                                                                       
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS_COUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS_COUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EXPIRED_DEALS_COUNT')); ?><?php endif; ?></td>
                                            <td><?php echo e($archievd_count); ?></td>
                                            
                                        </tr>
                                         <tr>
                                             <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"  <?php echo 'style="width:'.$archievd_count.'%"'; ?>;>
		  <span class="sr-only">40% <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_COMPLETE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE');} ?> ( <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SUCCESS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS');} ?> )</span>
		</div>
	      </div></td>
                                            
                                            
                                        </tr>
                                     <!--  <tr>
                                            <td>Sold Deals Count</td>
                                            <td><?php //echo $sold_cnt;?></td>
                                            
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php //echo 'style="width:'.$sold_cnt.'%"'; ?>;> 
		  <span class="sr-only">60% <?php //if (Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= '') { //echo  trans(Session::get('admin_lang_file').'.BACK_COMPLETE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE');} ?></span>
		</div>
	      </div></td>
                                                                                       
                                            
                                        </tr>-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-5"  style="padding-left:12px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEALS_TRANSACTION_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEALS_TRANSACTION_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS_TRANSACTION_DETAILS')); ?> <?php endif; ?></strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTIONS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TRANSACTIONS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTIONS')); ?> <?php endif; ?></th>
                                            <th><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNT')); ?> <?php endif; ?></th>
                                            <th><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TODAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TODAY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TODAY')); ?> <?php endif; ?></td>
                                            <td><?php echo e(($dealstdy[0]->count)+($dealstdy_payu[0]->count)+($ordercod_tdy[0]->count)); ?></td>
                                            <td><?php echo e(Helper::cur_sym()); ?> 
                                            <?php if(($dealstdy[0]->amt)||($ordercod_tdy[0]->amt) || ($dealstdy_payu[0]->amt)): ?>
											<?php echo e((($dealstdy[0]->amt+$dealstdy[0]->tax+$dealstdy[0]->ship)+($ordercod_tdy[0]->amt+$ordercod_tdy[0]->tax+$ordercod_tdy[0]->ship) + ($dealstdy_payu[0]->amt+$dealstdy_payu[0]->tax+$dealstdy_payu[0]->ship))); ?>

                                            <?php else: ?>
											<?php echo e("-"); ?> 
                                            <?php endif; ?></td>
                                            
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST')); ?> <?php endif; ?> 7 <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS')); ?> <?php endif; ?></td>
                                            <td><?php echo e(($deals7days[0]->count)+($deals7days_payu[0]->count)+($ordercod_7days[0]->count)); ?></td>
                                            <td><?php echo e(Helper::cur_sym()); ?> 
                                            <?php if(($deals7days[0]->amt)+($ordercod_7days[0]->amt) + ($deals7days_payu[0]->amt)): ?>
											<?php echo e((($deals7days[0]->amt+$deals7days[0]->tax+$deals7days[0]->ship)+($deals7days_payu[0]->amt+$deals7days_payu[0]->tax+$deals7days_payu[0]->ship)+($ordercod_7days[0]->amt+$ordercod_7days[0]->tax+$ordercod_7days[0]->ship))); ?>

                                            <?php else: ?> <?php echo e("-"); ?> <?php endif; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST')); ?> <?php endif; ?> 30 <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS')); ?> <?php endif; ?> </td>
                                            <td><?php echo e(($deals30days[0]->count)+($deals30days_payu[0]->count)+($ordercod_30days[0]->count)); ?></td>
                                            <td><?php echo e(Helper::cur_sym()); ?>

                                            <?php if(($deals30days[0]->amt)+($deals30days_payu[0]->amt)+($ordercod_30days[0]->amt)): ?>
											<?php echo e("".(($deals30days[0]->amt+$deals30days[0]->tax+$deals30days[0]->ship)+($deals30days_payu[0]->amt+$deals30days_payu[0]->tax+$deals30days_payu[0]->ship)+($ordercod_30days[0]->amt+$ordercod_30days[0]->tax+$ordercod_30days[0]->ship))); ?>

                                            <?php else: ?> <?php echo e("-"); ?>  <?php endif; ?></td>
                                            
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_ONE_YEAR')); ?> <?php endif; ?></td>
                                            <td><?php echo e(($deals12mnth[0]->count)+($deals12mnth_payu[0]->count)+($ordercod_12mnth[0]->count)); ?></td>
                                            <td><?php echo e(Helper::cur_sym()); ?>  
                                            <?php if(($deals12mnth[0]->amt)+($deals12mnth_payu[0]->amt)+($ordercod_12mnth[0]->amt)): ?>
											<?php echo e((($deals12mnth[0]->amt+$deals12mnth[0]->tax+$deals12mnth[0]->ship)+($deals12mnth_payu[0]->amt+$deals12mnth_payu[0]->tax+$deals12mnth_payu[0]->ship)+($ordercod_12mnth[0]->amt+$ordercod_12mnth[0]->tax+$ordercod_12mnth[0]->ship))); ?>

                                            <?php else: ?> <?php echo e("-"); ?> <?php endif; ?></td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    </div>
</div>
   
    </div>
                    
                    
                    <div class="row">
                    	<div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STATISTICS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STATISTICS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STATISTICS')); ?> <?php endif; ?></strong> 
                            </div>
                             
                            <div class="panel-body">
                              <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="tab"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')); ?> <?php endif; ?></a>
                                </li>
                              </ul>
                            
                            <div class="tab-content">
                                <?php if($deal_cnt!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
                              <div id="chart1" style="margin-top:20px; margin-left:0px; width:950px; height:470px;"></div>
                              <?php else: ?>
                                No Transaction Found.
                              <?php endif; ?>
                                <!--<div class="tab-pane fade" id="profile-pills">
                                    <h4>Profile Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>-->
                                <!--<div class="tab-pane fade" id="messages-pills">
                                    <h4>Messages Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>-->
                                
                            </div>
                            
                            
			
			
		</div>
		</div>
                             
		
                    </div>
                    </div>
                    </div>
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
   
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   <?php echo $adminfooter; ?>

    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
  
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
    <?php if(($active_count+$archievd_count+$inactive_cnt)>0): ?>
    <script>
	$(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[<?php echo e($active_count); ?>, <?php echo e($archievd_count); ?>,<?php echo e($inactive_cnt); ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } });
		});
	</script>
    <?php endif; ?>
    <?php if($deal_cnt!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>

    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php echo e($s1 = "[" .$deal_cnt. "]"); ?>

        var s1 = <?php echo e($s1); ?>

        var ticks = ['<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JANUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FEBRUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MARCH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APRIL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL')); ?><?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MAY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY')); ?> <?php endif; ?>','<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JUNE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JULY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AUGUST')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_OCTOBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')); ?>  <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DECEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')); ?> <?php endif; ?>'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    <?php endif; ?>

    <!--- Chart Plugins -->
      <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    
    <!-- -->
    


<script src="<?php echo e(url('')); ?>/public/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="<?php echo e(url('')); ?>/public/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="<?php echo e(url('')); ?>/public/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>

        
    
	<script src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="<?php echo e(url('')); ?>/public/assets/js/bar_chart.js"></script>
        
      <script type="text/javascript">
	   $.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	   });
	</script>  
	
</body>
    
</html>
