<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


<!-- Header End====================================================================== -->



<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <?php /* <div class="category-description std">

            <div class="slider-items-products">

              <div id="category-desc-slider" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img1.jpg"></a>

                    <div class="inner-info">

                      <div class="cat-img-title"> <span>Best Product 2017</span>

                        <h2 class="cat-heading">Best Selling Brand</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

                        <a class="info" href="#">Shop Now</a> </div>

                    </div>

                  </div>

                  <!-- End Item --> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img2.jpg"></a> </div>

                  

                  <!-- End Item --> 

                  

                </div>

              </div>

            </div>

          </div> */ ?>

		   <?php if(count($product_details) != 0): ?> 

          <div class="shop-inner">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_PRODUCTS')); ?> <?php endif; ?></h2>

            </div>

            <div class="product-grid-area">

            	<div class="toolbar">              

              <div class="sorter">

                <div class="short-by">

        				<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

        					<?php  $title = 'pro_title'; ?>

        				<?php else: ?> 

        					<?php  $title = 'pro_title_'.Session::get('lang_code'); ?> 

        				<?php endif; ?>

                  <label><?php if(Lang::has(Session::get('lang_file').'.SORT_BY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SORT_BY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SORT_BY')); ?> <?php endif; ?>:</label>

                  <select name="filtertypes" >

                    <option value=""><?php echo e((Lang::has(Session::get('lang_file').'.SORT_BY')!= '') ? trans(Session::get('lang_file').'.SORT_BY') : trans($OUR_LANGUAGE.'.SORT_BY')); ?></option>

                    <option value="1" ><?php if(Lang::has(Session::get('lang_file').'.PRICE_LOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE_LOW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRICE_LOW')); ?> <?php endif; ?> - <?php if(Lang::has(Session::get('lang_file').'.HIGH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HIGH')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HIGH')); ?> <?php endif; ?></option>

                    <option value="2" ><?php if(Lang::has(Session::get('lang_file').'.PRICE_HIGH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE_HIGH')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRICE_HIGH')); ?> <?php endif; ?> -<?php if(Lang::has(Session::get('lang_file').'.LOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOW')); ?> <?php endif; ?></option>

                    <option  value="3" ><?php if(Lang::has(Session::get('lang_file').'.TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TITLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TITLE')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?>-<?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?></option>

                    <option value="4"><?php if(Lang::has(Session::get('lang_file').'.TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TITLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TITLE')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?>-<?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?></option>

                    <option value="5"><?php if(Lang::has(Session::get('lang_file').'.DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DESCRIPTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DESCRIPTION')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?>- <?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?></option>

                    <option  value="6"><?php if(Lang::has(Session::get('lang_file').'.DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DESCRIPTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DESCRIPTION')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?>- <?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?> <?php endif; ?></option>

                  </select>

                </div>

                <div class="short-by page">

                  <label><?php if(Lang::has(Session::get('lang_file').'.SHOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHOW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHOW')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.PER_PAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PER_PAGE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PER_PAGE')); ?> <?php endif; ?>:</label>

                  <select name="perpagenumber"  >

          					<option value="9">9 <?php echo e((Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE')); ?></option>

          					<option value="18">18 <?php echo e((Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE')); ?></option>

          					<option value="36">36 <?php echo e((Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE')); ?></option>

          					<option value="all"><?php if(Lang::has(Session::get('lang_file').'.ALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ALL')); ?> <?php endif; ?> </option>

                  </select>

                </div>

                   

              </div>

            </div>

              <ul class="products-grid clearfix">

			  <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				 <?php $product_image = explode('/**/',$product_det->pro_Img);

				 $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

				 $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>

				 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				 <?php $pro_title = 'pro_title'; ?>

				 <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

				 <?php	$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));

				 $res = base64_encode($product_det->pro_id);

				 $prod_image    = $product_image[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/product/".$prod_image; ?>

				 <?php if(file_exists($img_data) && $prod_image !=''): ?>   

				 <?php $prod_path = url('').'/public/assets/product/' .$prod_image;  ?>                

				 <?php else: ?>  

				 <?php if(isset($DynamicNoImage['productImg'])): ?>

				 <?php $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; ?>

				 <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

				 <?php $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

				 <?php endif; ?>

				 <?php endif; ?>

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					   <?php if($product_det->pro_discount_percentage!='' && round($product_det->pro_discount_percentage)!=0): ?>

                        <div class="icon-new-label new-left"><?php echo e(substr($product_det->pro_discount_percentage,0,2)); ?>%</div><?php endif; ?>

                        <div class="pr-img-area">

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>

						<a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" alt="<?php echo e(substr($product_det->$pro_title,0,25)); ?> <?php echo e(strlen($product_det->$pro_title)>25?'..':''); ?>" src="<?php echo e($prod_path); ?>">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   <?php endif; ?>



					   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						<a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" alt="<?php echo e(substr($product_det->$pro_title,0,25)); ?><?php echo e(strlen($product_det->$pro_title)>25?'..':''); ?>" src="<?php echo e($prod_path); ?>">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   <?php endif; ?>

					   

					   <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						<a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" >

                          <figure> <img class="first-img" alt="<?php echo e(substr($product_det->$pro_title,0,25)); ?>	<?php echo e(strlen($product_det->$pro_title)>25?'..':''); ?>" src="<?php echo e($prod_path); ?>">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   <?php endif; ?>

					   

					   </div>

                        

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html"><?php echo e(substr($product_det->$pro_title,0,25)); ?>


						  <?php echo e(strlen($product_det->$pro_title)>25?'..':''); ?></a> </div>

                          <div class="item-content">

                           

                               <?php           

                $one_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 1)->count();

                $two_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 2)->count();

                $three_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 3)->count();

                $four_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 4)->count();

                $five_count = DB::table('nm_review')->where('product_id', '=', $product_det->pro_id)->where('ratings', '=', 5)->count();

                

                

                $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

                $multiple_countone = $one_count *1;

                $multiple_counttwo = $two_count *2;

                $multiple_countthree = $three_count *3;

                $multiple_countfour = $four_count *4;

                $multiple_countfive = $five_count *5;

                $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>



                         <div class="rating">

             <?php if($product_count): ?>

             <?php   $product_divide_count = $product_total_count / $product_count; ?>

             <?php if($product_divide_count <= '1'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '1'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '2'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

             <?php elseif($product_divide_count >= '3'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '4'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '5'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

             <?php else: ?>

              

            <?php endif; ?>

          <?php else: ?>

             <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

          <?php endif; ?>  

           </div>



                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?></span> </span> </div>

                            </div>

							

                            <div class="pro-action">

							<?php if($product_det->pro_no_of_purchase < $product_det->pro_qty): ?> 

								

								<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  

								<a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							    <?php endif; ?>

								

							   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

								<a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							   <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

								<a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

								<?php endif; ?>

							  <?php endif; ?>

								

								<?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty): ?> 

								<button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span></button> 

								<?php endif; ?>								

							  

							  

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

		  

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			    </ul>

			</div> 	

	    </div>

		  <?php else: ?>

			<div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_PRODUCTS')); ?> <?php endif; ?></h2>

              </div>

			<p><center><h4>
<?php echo e((Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?></h4></center> </p>

			

		   <?php endif; ?>

            

            

		 

		

		

			<!--For Deals-->

		 <?php if(count($deals_details) > 0): ?> 

		   <div class="shop-inner">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_DEALS')); ?> <?php endif; ?></h2>

            </div>

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			 <?php $__currentLoopData = $deals_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_deals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

			 <?php  $deal_img = explode('/**/', $fetch_deals->deal_image);

			 $mcat = strtolower(str_replace(' ','-',$fetch_deals->mc_name));

			 $smcat = strtolower(str_replace(' ','-',$fetch_deals->smc_name));

			 $sbcat = strtolower(str_replace(' ','-',$fetch_deals->sb_name));

			 $ssbcat = strtolower(str_replace(' ','-',$fetch_deals->ssb_name)); 

			 $res = base64_encode($fetch_deals->deal_id);

			 $product_image     = $deal_img[0];

			 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

			 $img_data   = "public/assets/deals/".$product_image; ?>

			 <?php if(file_exists($img_data) && $product_image !=''): ?>   

			 <?php  $prod_path = url('').'/public/assets/deals/' .$product_image;                  ?>

			 <?php else: ?>  

			 <?php if(isset($DynamicNoImage['dealImg'])): ?>

			 <?php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

			 <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

			 <?php    $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

			 <?php endif; ?>

			 <?php endif; ?>  

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					   <?php if($fetch_deals->deal_discount_percentage!='' && round($fetch_deals->deal_discount_percentage)!=0): ?> 

                        <div class="icon-new-label new-left"><?php echo e(substr($fetch_deals->deal_discount_percentage,0,2)); ?>%</div><?php endif; ?>

                        <div class="pr-img-area">

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  

						<a href="<?php echo e(url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="" class="product__image">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   <?php endif; ?>



					  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						<a href="<?php echo e(url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   <?php endif; ?>

					   

					  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						<a href="<?php echo e(url('dealview/'.$mcat.'/'.$smcat.'/'.$res)); ?>" >

                          <figure> <img class="first-img" alt="" src="<?php echo e($prod_path); ?>">

						 <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">-->

						  </figure>

                        </a> 

					   <?php endif; ?>

					   

					   </div>

                        

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html"><?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

						  <?php $deal_title = 'deal_title'; ?>

						  <?php else: ?> <?php  $deal_title = 'deal_title_'.Session::get('lang_code'); ?> <?php endif; ?>

						  <?php echo e(substr($fetch_deals->$deal_title,0,50)); ?>...</a> </div>

                          <div class="item-content">

                            <?php

              

              $one_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 1)->count();

              $two_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 2)->count();

              $three_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 3)->count();

              $four_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 4)->count();

              $five_count = DB::table('nm_review')->where('deal_id', '=', $fetch_deals->deal_id)->where('ratings', '=', 5)->count();

              

              

              $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

              $multiple_countone = $one_count *1;

              $multiple_counttwo = $two_count *2;

              $multiple_countthree = $three_count *3;

              $multiple_countfour = $four_count *4;

              $multiple_countfive = $five_count *5;

              $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

          

                          <div class="rating">

            <?php if($product_count): ?>

               <?php   $product_divide_count = $product_total_count / $product_count; ?>

               <?php if($product_divide_count <= '1'): ?> 

               

               <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

               <?php elseif($product_divide_count >= '1'): ?> 

               

               <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

               <?php elseif($product_divide_count >= '2'): ?> 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

               <?php elseif($product_divide_count >= '3'): ?> 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

               <?php elseif($product_divide_count >= '4'): ?> 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>

               <?php elseif($product_divide_count >= '5'): ?> 

               

               <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>

               <?php else: ?>

                

              <?php endif; ?>

            <?php else: ?>

               <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

              <?php endif; ?>  

            </div>

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_deals->deal_discount_price); ?></span> </span> </div>

                            </div>

							

                            <div class="pro-action">

							 <?php if($fetch_deals->deal_no_of_purchase < $fetch_deals->deal_max_limit): ?>  

								

								<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

								<a href="<?php echo e(url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							    <?php endif; ?>

								

							   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

								<a href="<?php echo e(url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

							   <?php endif; ?>

							   

							    <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

								<a href="<?php echo e(url('dealview/'.$mcat.'/'.$smcat.'/'.$res)); ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?> 

							    </span></button></a>

								<?php endif; ?>

							  <?php endif; ?>

								

								<?php if($fetch_deals->deal_no_of_purchase >= $fetch_deals->deal_max_limit): ?> 

								<button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span></button> 

								<?php endif; ?>								

							 

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

		

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			  </ul>

			 </div>

			</div>

			<?php elseif(count($deals_details) == 0): ?>

			

        <div class="page-title">

			 <h2><?php if(Lang::has(Session::get('lang_file').'.SEARCH_FOR_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SEARCH_FOR_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SEARCH_FOR_DEALS')); ?> <?php endif; ?></h2>

			 </div>

			<p><center><h4> <?php echo e((Lang::has(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_PRODUCTS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_PRODUCTS_AVAILABLE')); ?> </h4></center></p>

			 <?php endif; ?>

             

            </div>

			

			

            <!--<div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

			

	

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

			<div class="block-content">

              <p class="block-subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES')); ?></p>

               <?php if(count($main_category)!=0): ?>

				<div id="divMenu">

					<ul>

					  <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

					   <?php	$check_main_product_exists = DB::table('nm_product')->where('pro_mc_id', '=', $fetch_main_cat->mc_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

					   <?php if($check_main_product_exists): ?>

					   <?php	$pass_cat_id1 = "1,".$fetch_main_cat->mc_id;  ?>

					   <?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0): ?> 						

						<li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1)); ?>">

							 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

							  <?php $mc_name = 'mc_name'; ?>

							  <?php else: ?> <?php  $mc_name = 'mc_name_code'; ?> <?php endif; ?>

							  <?php echo e($fetch_main_cat->$mc_name); ?></a> 

						   <ul>

							<?php $__currentLoopData = $sub_main_category[$fetch_main_cat->mc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   

							 <?php 

               $check_sub_product_exists = DB::table('nm_product')->where('pro_smc_id', '=', $fetch_sub_main_cat->smc_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); 

               ?>

							 <?php if($check_sub_product_exists): ?>

							 <?php	$pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>

								<li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2)); ?>"> 

								 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

									<?php	$smc_name = 'smc_name'; ?>

									<?php else: ?> <?php  $smc_name = 'smc_name_code'; ?> <?php endif; ?>

									<?php echo e($fetch_sub_main_cat->$smc_name); ?> </a>

										

									<?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0): ?>  

									 <ul>

									   <?php $__currentLoopData = $second_main_category[$fetch_sub_main_cat->smc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

									   <?php	

                     $check_sec_main_product_exists = DB::table('nm_product')->where('pro_sb_id', '=', $fetch_sub_cat->sb_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

									   <?php if($check_sec_main_product_exists): ?>

									   <?php	$pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; ?>

									   <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0): ?>

										<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"> 

										<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

										  <?php $sb_name = 'sb_name'; ?>

										  <?php else: ?> <?php  $sb_name = 'sb_name_langCode'; ?> <?php endif; ?>

										  <?php echo e($fetch_sub_cat->$sb_name); ?></a> 

											<ul>	

												<?php $__currentLoopData = $second_sub_main_category[$fetch_sub_cat->sb_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_secsub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

												 <?php	$check_sec_sub_main_product_exists = DB::table('nm_product')->where('pro_ssb_id', '=', $fetch_secsub_cat->ssb_id)->where('pro_status','=',1)->whereRaw('pro_no_of_purchase < pro_qty')->get(); ?>

												 <?php if($check_sec_sub_main_product_exists): ?>

												 <?php	$pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>  

												<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)); ?>">

												<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

												<?php $ssb_name = 'ssb_name'; ?>

												<?php else: ?> <?php  $ssb_name = 'ssb_name_langCode'; ?> <?php endif; ?>

												<?php echo e($fetch_secsub_cat->$ssb_name); ?></a>

												</li>

												 <?php endif; ?>

													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

											</ul>

											 <?php endif; ?>

										</li>

										<?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									</ul>

									 <?php endif; ?>

								</li>

								 <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						   </ul>

						   <?php endif; ?>

						</li>

						<?php endif; ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					</ul>

					<?php else: ?> 

						<ul> <li><?php if(Lang::has(Session::get('lang_file').'.NO_CATEGORY_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_CATEGORY_FOUND')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_CATEGORY_FOUND')); ?>! <?php endif; ?> </li></ul> <?php endif; ?>

				</div>

            </div>

		<div class="clearfix"></div>

	 <br/>

         

        <?php if(count($most_visited_product)>0): ?> 

          <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3><?php if(Lang::has(Session::get('lang_file').'.MOST_VISITED_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MOST_VISITED_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MOST_VISITED_PRODUCT')); ?> <?php endif; ?></h3>

            </div>

            <div class="block-content">

              <ul>

			  <?php $__currentLoopData = $most_visited_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

			  <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				<?php $pro_title = 'pro_title'; ?>

				<?php else: ?> <?php  $pro_title = 'pro_title_langCode'; ?> <?php endif; ?>

				<?php	$mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				$smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				$sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				$ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name));

				$res = base64_encode($fetch_most_visit_pro->pro_id);

				$mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

				$mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

				$mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);

				$product_image    = $mostproduct_img[0];

				$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				$img_data   = "public/assets/product/".$product_image; ?>

				<?php if(file_exists($img_data) && $product_image !=''): ?>   

				<?php  $prod_path = url('').'/public/assets/product/' .$product_image;  ?>                 

				<?php else: ?>  

				<?php if(isset($DynamicNoImage['productImg'])): ?>

				<?php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; ?>

				<?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

				<?php   $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

				<?php endif; ?>

				<?php endif; ?>  

                <li class="item">

                  <div class="products-block-left">

				  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

				  <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e(substr($fetch_most_visit_pro->$pro_title,0,25)); ?><?php echo e(strlen($fetch_most_visit_pro->$pro_title)>25?'..':''); ?>"> </a>

				  <?php endif; ?>

				  

				  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

				  <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e(substr($fetch_most_visit_pro->$pro_title,0,25)); ?><?php echo e(strlen($fetch_most_visit_pro->$pro_title)>25?'..':''); ?>"></a>

				  <?php endif; ?>

				  

				  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

				  <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e(substr($fetch_most_visit_pro->$pro_title,0,25)); ?> <?php echo e(strlen($fetch_most_visit_pro->$pro_title)>25?'..':''); ?>"></a>

				  <?php endif; ?>

				  

				  </div>

                  <div class="products-block-right">

                    <p class="product-name"> <a><?php echo e(substr($fetch_most_visit_pro->$pro_title,0,25)); ?>


                     <?php echo e(strlen($fetch_most_visit_pro->$pro_title)>25?'..':''); ?> </a> </p>

                    <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_most_visit_pro->pro_disprice); ?></span>

                   

				   

				  <?php					  

				  $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 5)->count();

				  

				  

				  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

				  $multiple_countone = $one_count *1;

				  $multiple_counttwo = $two_count *2;

				  $multiple_countthree = $three_count *3;

				  $multiple_countfour = $four_count *4;

				  $multiple_countfive = $five_count *5;

				  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

				   

				   <div class="rating">

				<?php if($product_count): ?>

					 <?php   $product_divide_count = $product_total_count / $product_count; ?>

					 <?php if($product_divide_count <= '1'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '1'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '2'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

					 <?php elseif($product_divide_count >= '3'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '4'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '5'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

					 <?php else: ?>

						

					<?php endif; ?>

				<?php else: ?>

				     <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

				<?php endif; ?>	

			    </div>

				   

				   

				   

					<br>

					

					<?php if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty): ?>

						

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

					<a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>  

					<a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

					<a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					<?php endif; ?>

					

					<?php if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty): ?>  

					<a class="link-all"><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					

                  </div>

                </li>

			  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </ul>

            </div>

          </div>  

		  <?php endif; ?>

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $footer; ?>




