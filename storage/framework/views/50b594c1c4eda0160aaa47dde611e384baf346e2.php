<!DOCTYPE html>

<html lang="en">

<?php echo $navbar; ?>


<?php echo $header; ?>









<body class="checkout_page">

<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 





<div id="page"> 
  <!-- Breadcrumbs -->

  <div class="breadcrumbs">

    <div class="container"> 

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li>

            <li class="home"> <a title="" href=""><?php echo e((Lang::has(Session::get('lang_file').'.CHECKOUT')!= '') ?  trans(Session::get('lang_file').'.CHECKOUT'): trans($OUR_LANGUAGE.'.CHECKOUT')); ?></a></li>

           

          </ul>

        </div>

      </div>

    </div>

  </div>

 

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col2-right-layout">

    <div class="main container">

      <div class="row">

        <div class="col-main col-sm-12 col-xs-12">

<div class="page-title">

              <h2><?php echo e((Lang::has(Session::get('lang_file').'.CHECKOUT')!= '') ?  trans(Session::get('lang_file').'.CHECKOUT'): trans($OUR_LANGUAGE.'.CHECKOUT')); ?></h2>

            </div>

            <div class="row">

              <div class="col-sm-12">

          <div class="page-content checkout-page">

            <?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?> <?php  

               $item_count_header1 = count($_SESSION['cart']); ?> <?php else: ?> <?php $item_count_header1 = 0; ?> <?php endif; ?> 

                     <?php if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])): ?>    <?php $item_count_header2 = count($_SESSION['deal_cart']); ?> <?php else: ?> <?php $item_count_header2 = 0; ?> <?php endif; ?>

                     <?php $count = $item_count_header1 + $item_count_header2; ?>

                     <?php if($count !=0): ?> 

                      <?php echo Form::Open(array('url' => 'payment_checkout_process','id' => 'payment_')); ?> 

                      <?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?> <?php     $item_count_header1 = count($_SESSION['cart']); ?> <?php else: ?> <?php $item_count_header1 = 0; ?> <?php endif; ?> 

                              <?php if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])): ?><?php   $item_count_header2 = count($_SESSION['deal_cart']); ?> <?php else: ?> <?php $item_count_header2 = 0; ?> <?php endif; ?>

                              <?php $item_count_header = $item_count_header1 + $item_count_header2; ?>

            

            <h4 class="checkout-sep"> <?php if(Lang::has(Session::get('lang_file').'.SHIPPING_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHIPPING_ADDRESS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHIPPING_ADDRESS')); ?> <?php endif; ?> </h4>

            <div class="box-border">

              <div class="row">

                <div class="col-xs-12 col-md-5 col-sm-5">

               <div class="box-border" style="">

                  <?php if($shipping_addr_details): ?>

              <ul>

               <?php $__currentLoopData = $shipping_addr_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ship_addr_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <li class="row">

                  

                  <div class="col-sm-6"><?php print(Input::old('fname')); ?>

                    <label for="first_name_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NAME')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <input class="input form-control" type="text" name="fname" id="fname" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_NAME')); ?> <?php endif; ?>" maxlength ="50" value="<?php echo $ship_addr_det->ship_name; ?>" required>

                  </div>

                  <!--/ [col] -->

                  

                  <div class="col-sm-6">

                    <label for="last_name_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.EMAIL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.EMAIL')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <input class="input form-control" type="email" name="email" id="email_id" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_ID')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_ID')); ?> <?php endif; ?>" maxlength="50" value="<?php echo $ship_addr_det->ship_email; ?>" required>

                     <?php if($errors->has('email')): ?> 

                                    <p class="error-block" style="color:red;"><?php echo e($errors->first('email')); ?></p>

                                    <?php endif; ?>

                  </div>

                  <!--/ [col] --> 

                  

                </li>

                <!--/ .row -->

                

                <li class="row">

                  <div class="col-sm-6">

                    <label for="company_name_1"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS_LINE1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS_LINE1')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS_LINE1')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <input class="input form-control" type="text" name="addr_line" id="addr_line" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS_LINE1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS_LINE1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS_LINE1')); ?> <?php endif; ?>" maxlength="90" value="<?php echo $ship_addr_det->ship_address1; ?>" required>

                    <?php if($errors->has('addr_line')): ?> <p class="error-block" style="color:red;"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS_FIELD_IS_REQUIRED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS_FIELD_IS_REQUIRED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS_FIELD_IS_REQUIRED')); ?> <?php endif; ?></p> <?php endif; ?>

                  </div>

                  <!--/ [col] -->

                  

                  <div class="col-sm-6">

                    <label for="email_address_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS_LINE2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS_LINE2')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS_LINE2')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <input class="input form-control" type="text" name="addr1_line" id="addr1_line" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_ADDRESS_LINE2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_ADDRESS_LINE2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_ADDRESS_LINE2')); ?> <?php endif; ?>" maxlength="90" value="<?php echo $ship_addr_det->ship_address2; ?>" required>

                    <?php if($errors->has('addr1_line')): ?> 

                       <p class="error-block" style="color:red;"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS_FIELD_IS_REQUIRED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS_FIELD_IS_REQUIRED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS_FIELD_IS_REQUIRED')); ?> <?php endif; ?></p>

                     <?php endif; ?> 

                  </div>

                  <!--/ [col] --> 

                  

                </li>

                <!--/ .row -->

                

                <li class="row">

                  <div class="col-sm-6">

                    <label for="address_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.STATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STATE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STATE')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <input class="input form-control" type="text" name="state" id="state" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STATE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_YOUR_STATE')); ?> <?php endif; ?>" maxlength="50"  value="<?php echo $ship_addr_det->ship_state; ?>" required>

                  </div>

                  <div class="col-sm-6">

                    <label for="address_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUNTRY')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                     <div class="custom_select">



                      <select class="input form-control" name="country" id="country" onChange="select_city_ajax(this.value,'')"  >
                         <option value="0"><?php if(Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COUNTRY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COUNTRY')); ?> <?php endif; ?></option>

                         <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                         <option value="<?php echo e($country_fetch->co_name); ?>"  <?php  if($ship_addr_det->co_id==$country_fetch->co_id){ echo 'selected'; } ?>>

                            <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                            <?php  $co_name = 'co_name'; ?>

                            <?php else: ?> <?php  $co_name = 'co_name_'.Session::get('lang_code'); ?> <?php endif; ?>

                            <?php echo $country_fetch->$co_name; ?> 

                         </option>

                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      </select>

                    </div>

                  </div>

                  <!--/ [col] --> 

                  

                </li>

                <!--/ .row -->

                

                <li class="row">

                  <div class="col-sm-6">

                    <label for="city_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                   <div class="custom_select">

                     <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                     <?php $__currentLoopData = $shipping_addr_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ship_addr_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      <select class="input form-control" id="city" name="City">

                       

                         <option value="0"><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?> </option>

                    <?php $__currentLoopData = $city_shipping; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                      <option value="<?php echo e($city->ci_name); ?>"<?php if($city->ci_id==$ship_addr_det->ci_id){ ?>selected<?php } ?>>

                         <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                          <?php $ci_name = 'ci_name'; ?>

                         <?php else: ?> 

                         <?php  

                         $ci_name = 'ci_name_'.Session::get('lang_code');

                          ?>

                           <?php endif; ?>

                                   <?php echo $city->$ci_name; ?>


                         </option>

                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      </select>

                    </div>

                  </div>

                  <!--/ [col] -->

                  

                  <div class="col-sm-6">

                    <label class="required"><?php if(Lang::has(Session::get('lang_file').'.ZIP_CODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ZIP_CODE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ZIP_CODE')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                   <input class="input form-control" type="text" name="zipcode" id="zipcode" placeholder="<?php if(Lang::has(Session::get('lang_file').'.ENTER_ZIP_CODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_ZIP_CODE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_ZIP_CODE')); ?> <?php endif; ?>" onkeypress="return isNumber(event)" maxlength="7" value="<?php echo $ship_addr_det->ship_postalcode; ?>"  required>

                   <?php if($errors->has('zipcode')): ?> 

                    <p class="error-block" style="color:red;"><?php echo e($errors->first('zipcode')); ?></p>

                   <?php endif; ?>

                  </div>

                  <!--/ [col] --> 

                  

                </li>

                <!--/ .row -->

                

                <li class="row">

                  <div class="col-sm-6">

                    <label for="postal_code_1" class="required"><?php if(Lang::has(Session::get('lang_file').'.PHONE_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PHONE_NUMBER')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PHONE_NUMBER')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <input class="input form-control" type="text" name="phone1_line" id="phone1_line" placeholder="<?php if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_PHONENUMBER')!= '') { echo  trans(Session::get('lang_file').'.ENTER_YOUR_PHONENUMBER');}  else { echo trans($OUR_LANGUAGE.'.ENTER_YOUR_PHONENUMBER');} ?>" maxlength="16" onkeypress="return isNumber(event)" value="<?php echo $ship_addr_det->ship_phone; ?>" required>

                     <?php if($errors->has('phone1_line')): ?> 

                                       <p class="error-block" style="color:red;"><?php if(Lang::has(Session::get('lang_file').'.PHONE_NUMBER_FIELD_IS_REQUIRED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PHONE_NUMBER_FIELD_IS_REQUIRED')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PHONE_NUMBER_FIELD_IS_REQUIRED')); ?> <?php endif; ?></p>

                                       <?php endif; ?>

                  </div>

                  <!--/ [col] -->

                </li>

                <!--/ .row -->

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </ul>



              <?php endif; ?>

              

            </div>

            <br>

            <h4 class="checkout-sep"><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_METHOD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_METHOD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_METHOD')); ?> <?php endif; ?></h4>
            <div id="pay_type_error" style="color:red;font-size: 16px;"> </div>

            <div class="box-border" style="">

              <ul>

               <?php if(Session::get('coupon_total_amount') < 0): ?>

                        <?php else: ?>

                        <?php if(count($general)>0): ?>

                        <?php $__currentLoopData = $general; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php if($gs->gs_paypal_payment != '' || $gs->gs_payment_status != ''): ?> 

                        <?php if($gs->gs_paypal_payment == 'Paypal'): ?>

                            <li>

                              <label for="radio_button_5">

                                <input type="radio" value="1" id="paypal_radio" name="select_payment_type">

                                <?php if(Lang::has(Session::get('lang_file').'.PAYPAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYPAL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYPAL')); ?> <?php endif; ?></label>

                            </li>

                        <?php endif; ?>

                        <?php if($gs->gs_payumoney_status == 'PayUmoney'): ?> 

                        <li>

                              <label for="radio_button_5">

                                <input type="radio" value="2" id="payumoney_radio" name="select_payment_type">

                                <?php if(Lang::has(Session::get('lang_file').'.PAYUMONEY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYUMONEY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYUMONEY')); ?> 

                             <?php endif; ?></label>

                            </li>

                            <?php endif; ?>
				  
				  
				    <?php if($gs->gs_paycorp_status == 'PayCorp'): ?> 

                            <li>

                                  <label for="radio_button_5">

                                    <input type="radio" value="3" id="paycorp_radio" name="select_payment_type">

                                    <?php if(Lang::has(Session::get('lang_file').'.PAYCORP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYCORP')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYCORP')); ?> 

                                <?php endif; ?></label>

                                </li>

                                <?php endif; ?>
				  
				  
				  

                             <?php if($gs->gs_payment_status == 'COD'): ?> 

                             <li>

                              <label for="radio_button_5">

                                <input type="radio" value="0" id="cod_radio" name="select_payment_type" >

                                <?php if(Lang::has(Session::get('lang_file').'.CASH_ON_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CASH_ON_DELIVERY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CASH_ON_DELIVERY')); ?> <?php endif; ?></label>

                            </li>

                            <?php endif; ?>

                             <?php endif; ?>    

                        <?php endif; ?>        

                        <?php endif; ?>

              </ul>

              

            </div>

         </div>

         

            <div class="col-xs-12 col-md-7 col-sm-7">

               <div class="box-border" style="">

              <div class="table-responsive">

               

                <table class="table table-bordered cart_summary">

                  <thead>

                    <tr >

                      <th class="cart_product"><?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '') ? trans(Session::get('lang_file').'.PRODUCT') : trans($OUR_LANGUAGE.'.PRODUCT')); ?></th>

                      <th><?php echo e((Lang::has(Session::get('lang_file').'.DESCRIPTION')!= '') ? trans(Session::get('lang_file').'.DESCRIPTION') : trans($OUR_LANGUAGE.'.DESCRIPTION')); ?></th>

                      <th><?php echo e((Lang::has(Session::get('lang_file').'.DELIVERED_ON')!= '') ? trans(Session::get('lang_file').'.DELIVERED_ON') : trans($OUR_LANGUAGE.'.DELIVERED_ON')); ?></th>

                      <th><?php echo e((Lang::has(Session::get('lang_file').'.UNIT_PRICE')!= '') ? trans(Session::get('lang_file').'.UNIT_PRICE') : trans($OUR_LANGUAGE.'.UNIT_PRICE')); ?></th>

                      <th><?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '') ? trans(Session::get('lang_file').'.QTY') : trans($OUR_LANGUAGE.'.QTY')); ?></th>

                      <th><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL')!= '') ? trans(Session::get('lang_file').'.TOTAL') : trans($OUR_LANGUAGE.'.TOTAL')); ?></th>

                      <th class="action"><?php echo e((Lang::has(Session::get('lang_file').'.SHIP_AMOUNT')!= '') ? trans(Session::get('lang_file').'.SHIP_AMOUNT') : trans($OUR_LANGUAGE.'.SHIP_AMOUNT')); ?></th>

                    </tr>

                  </thead>

                  <?php   $appliedCouponAmt = 0;                      

                           $z = 1;

                           $overall_total_price=0;

                           $overall_shipping_price=0;

                           $tax = 0;

                           $overall_tax_price=0;

                           $overall_tax_amt=0;

                           $overall_coupon_value=0;

                           $shipping_price=0;

                           $per_product_shipping = 0; ?>

                           <?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>

                           <?php $max=count($_SESSION['cart']); ?>

                           <?php for($i=0;$i<$max;$i++): ?>

                           <?php $pid=$_SESSION['cart'][$i]['productid'];

                           $q=$_SESSION['cart'][$i]['qty'];

                           $size=$size_result[$_SESSION['cart'][$i]['size']];

                           $color=$color_result[$_SESSION['cart'][$i]['color']];

                           $pname="Have to get"; ?>

                           <?php $__currentLoopData = $result_cart[$pid]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session_cart_result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                           <?php $product_img=explode('/**/',$session_cart_result->pro_Img); 

                           $session_pro_id = $_SESSION['cart'][$i]['productid'];

                           $session_color_id = $_SESSION['cart'][$i]['color'];

                           $session_size_id = $_SESSION['cart'][$i]['size'];

                           $session_customer_id = Session::get('customerid');

                           $coupon_details ='';

                           $coupon_details =  DB::table('nm_coupon_purchage')->where('product_id','=',$session_pro_id)->where('sold_user','=',$session_customer_id)->where('color','=',$session_color_id)->where('size','=',$session_size_id)->where('type_of_coupon','=',1)->first(); 

                           $coupon_details_all =  DB::table('nm_coupon_purchage')->where('sold_user','=',$session_customer_id)->first();

                           $type_of_coupon_details =  DB::table('nm_coupon_purchage')->where('sold_user','=',$session_customer_id)->whereRaw('type_of_coupon=2 or type_of_coupon=3')->first(); ?>

                           <?php if(isset($coupon_details) && $coupon_details != ""): ?> 

                           <?php $tax= $session_cart_result->pro_inctax;

                           $overall_tax_price= ((($session_cart_result->pro_disprice * $q) * $tax)/100);

                           $item_total_price = ($session_cart_result->pro_disprice * $q)+ $overall_tax_price;  

                           $overall_total_price += ($item_total_price);  

                           $overall_shipping_price +=($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_shippamt);

                           $per_product_shipping = ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_shippamt);

                           $appliedCouponAmt += $coupon_details->value; ?>

                           <?php elseif($type_of_coupon_details != ""): ?>

                           <?php if($type_of_coupon_details->type !=  ''): ?> 

                           <?php $product_qty_price = ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_disprice);

                           $overall_coupon_value = $type_of_coupon_details->value;

                           $tax=               $session_cart_result->pro_inctax;

                           $overall_tax_price= ((($product_qty_price)*$tax)/100);

                           $appliedCouponAmt = $overall_coupon_value;

                           $flat = $type_of_coupon_details->value * $product_qty_price;

                           $flat_less = $flat / Session::get('user_total_amount');
                         
                           $rount_total_price = ($product_qty_price -  $flat_less);                                 

                           $item_total_price = ($product_qty_price) + $overall_tax_price; 

                           $overall_total_price += round($item_total_price,2);  

                           $overall_shipping_price +=($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_shippamt);

                           $per_product_shipping = ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_shippamt); ?>

                           <?php endif; ?>    

                           <?php else: ?>

                           <?php $product_qty_price = ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_disprice);

                           $overall_shipping_price += ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_shippamt);

                           $per_product_shipping = ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->pro_shippamt);

                           $tax=               $session_cart_result->pro_inctax;

                           $overall_tax_price =((($product_qty_price)*$tax)/100);

                           $item_total_price = ($product_qty_price + $overall_tax_price);

                           $overall_total_price += round($item_total_price,2);  ?>

                           <?php endif; ?>

                           <?php $delivery_date[$z] = '+'.$session_cart_result->pro_delivery.'days';

                           $overall_tax_amt+= round($overall_tax_price,2); ?>



                  <tbody id="product_select_div<?php echo $pid;?>">

                    <tr>

                      <td class="cart_product">

                            

                           <?php 

                    $pro_img = $product_img[0];

                      $prod_path = url('').'/public/assets/default_image/No_image_product.png';

                    if($product_img != '') // image is null

                        {  

                          $img_data = "public/assets/product/".$pro_img;

                              if(file_exists($img_data) && $pro_img !='')  //image not exists in folder 

                                                    {

                    $prod_path = url('').'/public/assets/product/'.$pro_img;

                                                     }

                              else{  

                                     if(isset($DynamicNoImage['productImg']))

                                     {                  

                                        $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];

                                        if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))

                                        { 

                                            $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];

                                        }

                                                            

                                     }

                                                         

                                                         

                                    } ?>

                 <?php  } else

                    {
                          if(isset($DynamicNoImage['productImg'])) // check no_image_product is exist 

                                   {                      

                                      $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];

                                      if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))

                                      { 

                                          $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];

                                      }

                                  

                                   }

                      

                                    } ?>       

                        <a href="#"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($session_cart_result->pro_title); ?>"></a></td>

                      <td class="cart_description rg">

                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                                 <?php $pro_title = 'pro_title'; ?>

                                 <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

                        <p class="product-name"><a href="#">



                        <?php echo e(substr($session_cart_result->$pro_title,0,25)); ?>


              <?php echo e(strlen($session_cart_result->$pro_title)>25?'..':''); ?></a></p>

                        <small><a href="#"><?php if($size != "-" ): ?><b>  <?php if(Lang::has(Session::get('lang_file').'.SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIZE')); ?><?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIZE')); ?> <?php endif; ?> : <?php echo $size;?></b> <?php endif; ?></a></small><br>

                        <small><a href="#"><?php if($color != "-" ): ?><b>  <?php if(Lang::has(Session::get('lang_file').'.COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COLOR')); ?> <?php endif; ?> : <?php echo $color;?> </b><?php endif; ?></a></small></td>

                      <td class="cart_avail"><span class="label label-success"><?php echo e(date('d, M Y', strtotime($delivery_date[$z]))); ?></span></td>

                      <td class="price"><span><?php echo e(Helper::cur_sym()); ?> <?php echo e($session_cart_result->pro_disprice); ?></span></td>

                      <td class="qty"><?php echo e($q); ?></td>

                      <td class="price"><span>

                        <?php echo e(Helper::cur_sym()); ?> 

                        <?php if($item_total_price < 0): ?>

                                 <?php echo e('0.00'); ?>


                                 <?php else: ?>

                                 <?php echo e(round($item_total_price,2)); ?>


                                 <?php endif; ?>

                                 <?php if($session_cart_result->pro_inctax !='' || $session_cart_result->pro_inctax!=0): ?> <br>

                                 (<?php if (Lang::has(Session::get('lang_file').'.INCLUDING')!= '') { echo  trans(Session::get('lang_file').'.INCLUDING');}  else { echo trans($OUR_LANGUAGE.'.INCLUDING');} ?> <?php echo $session_cart_result->pro_inctax.' % '; if (Lang::has(Session::get('lang_file').'.TAXES')!= '') { echo  trans(Session::get('lang_file').'.TAXES');}  else { echo trans($OUR_LANGUAGE.'.TAXES');} ?>)

                                 <?php endif; ?>

                                <br>

                                

                                <?php if($coupon_details != ""): ?> 

                                 <label><?php if (Lang::has(Session::get('lang_file').'.TYPE_OF_COUPON')!= '') { echo  trans(Session::get('lang_file').'.TYPE_OF_COUPON');}  else { echo trans($OUR_LANGUAGE.'.TYPE_OF_COUPON');} ?>: <?php if($coupon_details->type == 1){ echo "Flat";}elseif($coupon_details->type == 2){ echo "Percentage";}?>
                                 <br>
                                 <span class="">

                                  <?php if (Lang::has(Session::get('lang_file').'.VALUE')!= '') { echo  trans(Session::get('lang_file').'.VALUE');}  else { echo trans($OUR_LANGUAGE.'.VALUE');} ?>: <?php if($coupon_details->type == 1){ ?><?php echo e(Helper::cur_sym()); ?> 
                                   <?php echo $coupon_details->value;} elseif($coupon_details->type == 2){?><?php echo e(Helper::cur_sym()); ?> <?php echo $coupon_details->value;}?></span></label>

                                 

                                 <?php endif; ?>

                                 

                       </span></td>

                      <td class="action">

                        <?php echo e(Helper::cur_sym()); ?> <?php echo e($per_product_shipping); ?>


                        

                      </td>



                    </tr>

                    

                           <?php 

                              //$overall_pro_price = $overall_total_price;  ?>

                           <input type="hidden" name="item_name[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_title; ?>" />

                           <input type="hidden" name="item_type[<?php echo $z;?>]" value="1" />

                           <input type="hidden" name="item_code[<?php echo $z;?>]" value="<?php echo $pid; ?>" />

                           <input type="hidden" name="item_desc[<?php echo $z;?>]" value="<?php echo strip_tags($session_cart_result->pro_desc); ?>" />

                           <input type="hidden" name="item_qty[<?php echo $z;?>]" value="<?php echo $q; ?>" />

                           <input type="hidden" name="item_color[<?php echo $z;?>]" value="<?php echo $_SESSION['cart'][$i]['color']; ?>" />

                           <input type="hidden" name="item_size[<?php echo $z;?>]" value="<?php echo $_SESSION['cart'][$i]['size']; ?>" />

                           <input type="hidden" name="item_color_name[<?php echo $z;?>]" value="<?php echo $color; ?>" />

                           <input type="hidden" name="item_size_name[<?php echo $z;?>]" value="<?php echo $size; ?>" />

                           <input type="hidden" name="item_price[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_disprice; ?>" />

                           <?php //} ?>

                           <!-- For Wallet Cash pack fetch -->

                           <input type="hidden" name="item_cash_pack[<?php echo $z;?>]" value="<?php echo $session_cart_result->cash_pack; ?>" />

                           <input type="hidden" name="item_tax[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_inctax; ?>" />

                           <input type="hidden" name="item_shipping[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_shippamt; ?>" />

                           <input type="hidden" name="item_totprice[<?php echo $z;?>]" value="<?php echo $item_total_price; ?>" />

                           <input type="hidden" name="item_merchant[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_mr_id; ?>" />

                    <?php $no_item_found = 1;  $z++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endfor; ?>  <?php endif; ?>



                          

                    <?php 

                           $overall_deal_total_price=0;

                           $overall_deal_shipping_price=0;

                           $overall_deal_tax_price=0; ?>

                           <?php if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])): ?>                

                           <?php $max=count($_SESSION['deal_cart']); ?>

                           <?php for($i=0;$i<$max;$i++): ?>

                           <?php $pid=$_SESSION['deal_cart'][$i]['productid'];

                           $q=$_SESSION['deal_cart'][$i]['qty'];

                           $pname="Have to get"; ?>

                           <?php $__currentLoopData = $result_cart_deal[$pid]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session_deal_cart_result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                           <?php $product_img=explode('/**/',$session_deal_cart_result->deal_image); 

                           $deal_qty_price = ($_SESSION['deal_cart'][$i]['qty']) * ($session_deal_cart_result->deal_discount_price );

                           $tax =               $session_deal_cart_result->deal_inctax;

                           $overall_tax_price =((($deal_qty_price)*$tax)/100);

                           $overall_tax_amt += round($overall_tax_price,2);

                           $item_total_price = ($deal_qty_price + $overall_tax_price);

                           $session_customer_id = Session::get('customerid');

                           $coupon_details_all =  DB::table('nm_coupon_purchage')->where('sold_user','=',$session_customer_id)->first();

                           $overall_deal_total_price += round($item_total_price,2); 

                           $overall_deal_shipping_price +=($_SESSION['deal_cart'][$i]['qty']) * ($session_deal_cart_result->deal_shippamt);

                           $overall_deal_tax_price +=0; ?>  

                           <?php $delivery_date[$z] = '+'.$session_deal_cart_result->deal_delivery.'days';

                            $per_product_shipping = ($_SESSION['deal_cart'][$i]['qty']) * ($session_deal_cart_result->deal_shippamt); ?>

                            <tr id="product_select_div<?php echo $pid;?>">

                      <td class="cart_product">

                           

                           <?php 

                                    $pro_img = $product_img[0];

                                      $prod_path = url('').'/public/assets/default_image/No_image_product.png';

                                    

                                    if($product_img != '') // image is null

                                        {  


                                          $img_data = "public/assets/deals/".$pro_img;

                                              if(file_exists($img_data) && $pro_img !='')  //image not exists in folder 
                                              {
                                                 $prod_path = url('').'/public/assets/deals/'.$pro_img;    }

                                              else{  

                                                     if(isset($DynamicNoImage['productImg']))

                                                     {                  

                                                        $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];

                                                        if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))

                                                        { 

                                                            $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];

                                                        }

                                                                            

                                                     }

                                                                         

                                                                         

                                                    } ?>

                                 <?php  } else

                                    {

                                        

                                        if(isset($DynamicNoImage['productImg'])) // check no_image_product is exist 

                                                 {                      

                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];

                                                    if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path))

                                                    { 

                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];

                                                    }

                                                

                                                 }

                                    

                                    } ?>        

                        <a href="#"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($session_deal_cart_result->deal_title); ?>"></a></td>

                      <td class="cart_description">

                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                                 <?php   $deal_title = 'deal_title'; ?>

                                 <?php else: ?> <?php  $deal_title = 'deal_title_'.Session::get('lang_code'); ?> <?php endif; ?>

                        <p class="product-name"><a href="#"><?php echo e($session_deal_cart_result->$deal_title); ?></a></p>

                        </td>

                      <td class="cart_avail"><span class="label label-success"><?php echo e(date('d, M Y', strtotime($delivery_date[$z]))); ?></span></td>

                      <td class="price"><span><?php echo e(Helper::cur_sym()); ?> <?php echo e($session_deal_cart_result->deal_discount_price); ?></span></td>

                      <td class="qty"><?php echo e($q); ?></td>

                      <td class="price"><span>

                        <?php echo e(Helper::cur_sym()); ?> 

                        <?php echo e($item_total_price); ?>

                       </span></td>

                      <td class="action">

                        <?php echo e(Helper::cur_sym()); ?> <?php echo e($per_product_shipping); ?>

                      </td>

                    </tr> 

                    

                           <input type="hidden" name="item_name[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->deal_title; ?>" />

                           <input type="hidden" name="item_type[<?php echo $z;?>]" value="2" />

                           <input type="hidden" name="item_code[<?php echo $z;?>]" value="<?php echo $pid; ?>" />

                           <input type="hidden" name="item_desc[<?php echo $z;?>]" value="<?php echo strip_tags($session_deal_cart_result->deal_description); ?>" />

                           <input type="hidden" name="item_qty[<?php echo $z;?>]" value="<?php echo $q; ?>" />

                           <input type="hidden" name="item_color[<?php echo $z;?>]" value="" />

                           <input type="hidden" name="item_size[<?php echo $z;?>]" value="" />

                           <input type="hidden" name="item_color_name[<?php echo $z;?>]" value="" />

                           <input type="hidden" name="item_size_name[<?php echo $z;?>]" value="" />

                           <input type="hidden" name="item_price[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->deal_discount_price; ?>" />

                           <!-- For Wallet Cash pack fetch -->

                           <input type="hidden" name="item_cash_pack[<?php echo $z;?>]" value="0" />

                           <input type="hidden" name="item_tax[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->deal_inctax; ?>" />

                           <input type="hidden" name="item_shipping[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->deal_shippamt; ?>" />

                           <input type="hidden" name="item_totprice[<?php echo $z;?>]" value="<?php echo $item_total_price; ?>" />

                           <input type="hidden" name="item_merchant[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->deal_merchant_id; ?>" />

                           <?php $no_item_found = 1; $z++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endfor; ?>   <?php endif; ?>      

                  </tbody>

                 

                  <tfoot>

                    <tr>

                    

                      <td colspan="5" style="text-align: right;"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_SUBTOTAL_TAX_INCL')!= '') ? trans(Session::get('lang_file').'.ORDER_SUBTOTAL_TAX_INCL') : trans($OUR_LANGUAGE.'.ORDER_SUBTOTAL_TAX_INCL')); ?></td>

                      <td colspan="2" style="text-align: right;"><?php echo e(Helper::cur_sym()); ?> <?php

                                       //echo $overall_tax_amt; 

                                       if($overall_total_price < 0){

                                           echo '0.00';

                                       }

                                       else{

                                           echo round(($overall_total_price + $overall_deal_total_price),2);

                                       }

                                       ?>

                                       <input type="hidden" name="subtotal" id="subtotal" value="<?php echo ($overall_total_price + $overall_deal_total_price); ?>" />

                                    </td>

                    </tr>

                    <tr>

                      <td colspan="5" style="text-align: right;"><?php echo e((Lang::has(Session::get('lang_file').'.SHIPPING')!= '') ? trans(Session::get('lang_file').'.SHIPPING') : trans($OUR_LANGUAGE.'.SHIPPING')); ?></td>

                      <td colspan="2" style="text-align: right;"><?php echo e(Helper::cur_sym()); ?> <?php echo ($overall_shipping_price + $overall_deal_shipping_price).'.00';?>

                        <input type="hidden" name="shipping_price" value="<?php echo ($overall_shipping_price + $overall_deal_shipping_price); ?>" />

                      </td>

                    </tr>

                    <?php   $product_total  =  ($overall_total_price + $overall_deal_total_price + $overall_shipping_price + $overall_deal_shipping_price); 

                              $customer_wallet_amount = DB::table('nm_customer')->where('cus_id','=',Session::get('customerid'))->where('cus_status','=',0)->first(); ?>



                              <?php if(count($customer_wallet_amount)>0): ?>     

                              <?php if($customer_wallet_amount->wallet != 0): ?>

                    <tr id="wallet"  style="display: none;">

                            
                         <td colspan="5" style="text-align: right;" >

                          <input name="checkbox" onclick="wallet_usage(<?php echo $customer_wallet_amount->wallet;?>, $(this), <?php echo $product_total-$appliedCouponAmt;?>);" type="checkbox" id="checkbox" />

                             <?php if(Lang::has(Session::get('lang_file').'.USE_WALLET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.USE_WALLET')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.USE_WALLET')); ?> <?php endif; ?>
                          </td> 

                                 

          								<td colspan="2" style="text-align: right;">

          								   <input type="text" value=" <?php echo e(Helper::cur_sym()); ?> <?php echo $customer_wallet_amount->wallet;?>" readonly>

                            <input type="hidden" id="user_wallet_amount" name="user_wallet_amount" value="<?php echo $customer_wallet_amount->wallet;?>">

          								 </td>                            

                     
                          

                    </tr>

                    <?php endif; ?> <?php endif; ?>

                    <?php if($appliedCouponAmt>0): ?>

                    <tr>

                      <td colspan="5" style="text-align: right;"><?php if(Lang::has(Session::get('lang_file').'.COUPON_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUPON_AMOUNT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUPON_AMOUNT')); ?> <?php endif; ?></td>

                      <td colspan="2" style="text-align: right;"> <?php echo e(Helper::cur_sym()); ?> <?php echo e(($appliedCouponAmt)); ?>


                         <input type="hidden" name="coupon_amount" value="<?php echo ($appliedCouponAmt); ?>" />

                      </td>

                    </tr>

                     <?php endif; ?>

                     

                     <tr id="if_wallet"  style="text-align: right; display:none">

                      <td colspan="5" style="text-align: right;"><?php if(Lang::has(Session::get('lang_file').'.WALLET_USED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WALLET_USED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WALLET_USED')); ?> <?php endif; ?></td>

                      <td colspan="2" style="text-align: right;">- <?php echo e(Helper::cur_sym()); ?><span id="wallet_used"></span>

                          <input type="hidden" name="wallet_used_amount" id="wallet_used_amount" value="0">

                      </td>

                    </tr>

                    <tr id="if_wallet1" class="row hide" style="display:none">

                      <td colspan="3"><?php if(Lang::has(Session::get('lang_file').'.ORDER_TAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_TAX')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_TAX')); ?> <?php endif; ?><span class="text-sub">*</span></td>

                      <td colspan="2"> 

                        <?php 

                           if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){    

                               $avg = count($_SESSION['cart']); 

                           }else { 

                               $avg = 0; 

                           } 

                           

                           $overall_per_pro_tax = 0; 

                           $overall_per_deal_tax = 0; ?> 

                           <?php echo ($overall_tax_amt + $overall_deal_tax_price).' %';?>

                  <input type="hidden" name="tax_price" value="<?php echo round(($overall_tax_amt+ $overall_deal_tax_price),2); ?>" />

                      </td>

                    </tr>

                    <tr>


                      <td colspan="5" style="text-align: right;"><strong><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL')!= '') ? trans(Session::get('lang_file').'.TOTAL') : trans($OUR_LANGUAGE.'.TOTAL')); ?></strong></td>

                      <td colspan="2" style="text-align: right;">

                        <?php

                                    $overall_pro_price = $overall_total_price + ($overall_shipping_price); 

                                    $overall_deal_price = ($overall_deal_total_price+$overall_deal_shipping_price) + (($overall_deal_total_price+$overall_deal_shipping_price) *($overall_deal_tax_price/100));  ?> 

                        <strong><?php echo e(Helper::cur_sym()); ?>


                                          <span id="wallet_apply_final_total">

                                          <?php $total_amount = round(($overall_pro_price + $overall_deal_price - $appliedCouponAmt),2);

                                          echo $total_amount;

                                          $price = round(($overall_pro_price+$overall_deal_price - $appliedCouponAmt),2) ;          

                                          ?></span></strong></td>

                                          <input type="hidden" name="grand_total" id="grand_total" value="<?php echo e($price); ?>">

                                    <input type="hidden" name="old_grand_total" id="old_grand_total" value="<?php echo e($price); ?>">

                                    <?php /*not using this totalprice & total price1 & walletcall function, (saranya)*/ ?>

                                    <input type="hidden" name="total_price" id="total_price" value="<?php echo round(($overall_pro_price+$overall_deal_price),2); ?>" />

                                    <input type="hidden" name="total_price1" id="total_price1" value="<?php echo round(($overall_pro_price+$overall_deal_price),2); ?>" />

                    </tr>

                  </tfoot>

                </table>



              </div>

              <?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?> <?php $item_count_header1 = count($_SESSION['cart']); ?> <?php else: ?> <?php $item_count_header1 = 0; ?> <?php endif; ?> 

                                 <?php if(isset($_SESSION['deal_cart'])&& !empty($_SESSION['deal_cart'])): ?> <?php    $item_count_header2 = count($_SESSION['deal_cart']); ?> <?php else: ?> <?php $item_count_header2 = 0; ?> <?php endif; ?>

                                 <?php $count = $item_count_header1 + $item_count_header2; ?>

                                 <?php if($count != 0): ?> 

                                 <input type="hidden" name="count_session" id="count_session" value="<?php echo $count; ?>" />

                                 <?php if(count($general)>0): ?>

                                 <?php $__currentLoopData = $general; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                 <?php if($gs->gs_paypal_payment != '' || $gs->gs_payment_status != ''): ?> 

              <button class="button pull-right" id="place_order_submit">

               <span>

                  <?php if(Lang::has(Session::get('lang_file').'.PLACE_ORDER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLACE_ORDER')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLACE_ORDER')); ?> <?php endif; ?>

               </span></button>

               <?php else: ?>

                                 <p><?php if(Lang::has(Session::get('lang_file').'.NO_PAYMENT_METHOD_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PAYMENT_METHOD_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PAYMENT_METHOD_AVAILABLE')); ?> <?php endif; ?></p>

                                 <?php endif; ?> 

                                 <?php endif; ?> 

                                 <?php endif; ?>

               <?php echo e(Form::close()); ?>


               

               

                   <?php endif; ?>  

                   <?php if(empty($item_count_header)): ?>

                     <div class="span6">

                        

                        

                        <div class="row">

                           <div class="span3">

                              <div class="form-group">

                                 <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('lang_file').'.NO_ORDERS_PLACED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_ORDERS_PLACED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_ORDERS_PLACED')); ?> <?php endif; ?> </label>

                              </div>

                           </div>

                        </div>

                     </div>

                     <?php endif; ?>

            </div>

               </div>

               

              </div>

            </div>

            

      </div>

    </div>



     



    </div>

  </section>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <!-- Footer -->

 <?php echo $footer; ?>


  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> 



<!-- End Footer --> 





  <!-- jquery js --> 

<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.min.js"></script> 





 <script>





         function select_city_ajax(id,city_name)

            

         { 

             var passData = 'id='+id+'&city='+city_name;

             //alert(passData);

               $.ajax( {

                      type: 'get',

                      data: passData,

                      url: '<?php echo url('register_getcityname'); ?>',

                      success: function(responseText){  

                     // alert(responseText);

                       if(responseText)

                       { 

                        $('#city').html(responseText);                     

                       }

                    }       

                });     

         }



</script>

<script>

        



         $(document).ready(function(){ 

        

            $('#cod_div').show();

            $('#paypal_div').hide();

            

            $('#paypal_radio').click(function()

            {        

                $('#paypal_div').show();

                $('#cod_div').hide();   

                $('#wallet').show();    //by only using online payment user should use wallet

            });

            $('#cod_radio').click(function()

            {           

                $('#paypal_div').hide();

                $('#cod_div').show();

                    $('#wallet').hide();

            });

			

			$('#payumoney_radio').click(function()

            {           

                $('#wallet').hide();

            });

			

            $('#mulshipping_addr_1rad').click(function()

            {   

                $('.shipping_addr_class').css("display","block");

            });

            $('#mulshipping_addr_2rad').click(function()

            {

                $('.shipping_addr_class').css("display","none");

                $('#shipping_addr_div1').css("display","block");

            });

         

            $('#fname').bind('keyup blur',function(){ 

                var node = $(this);

                node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/,'') ); }

            );

            

            $('#addr_line').bind('keyup blur',function(){ 

                var node = $(this);

                node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/,'') ); }

            );

            

            $('#addr1_line').bind('keyup blur',function(){ 

                var node = $(this);

                node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/,'') ); }

            );

            

            $('#state').bind('keyup blur',function(){ 

                var node = $(this);

                node.val(node.val().replace(/[^a-z  A-Z_-]/,'') ); }

            );       

            

            

            

            var count = $('#count_pid').val();

         

            var zip_regex =  /[0-9-()+]{6,7}/;

         

            var phone_regex =  /[0-9-()+]{8,10}/;

         

            $('#place_order_submit').click(function(e){

        var pay_typ=$('input:radio[name=select_payment_type]:checked').val();

        if (!$('input[name="select_payment_type"]').is(':checked'))
         {

                $('#pay_type_error').html("<?php echo (Lang::has(Session::get('lang_file').'.SELECT_PAYMENT_METHOD')!= '')  ?  trans(Session::get('lang_file').'.SELECT_PAYMENT_METHOD') : trans($OUR_LANGUAGE.'.SELECT_PAYMENT_METHOD') ; ?>");
                e.preventDefault();
            } 

       /*payumoney_check_out*/

        if(parseInt(pay_typ)==2){

            $('#payment_').attr('action','payumoney_check_out_payumoney');

        } 
				
	if(parseInt(pay_typ)==3){

            $('#payment_').attr('action','test');

        } 
				
		

                //alert($('#city').val());

                //alert($('input:radio[name=mul_shipping_addr]:checked').val());

                if($('#country').val() == 0)

         

                    {

         

                        alert("<?php echo e((Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= '') ? trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY') : trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?>");

         

                        return false;

         

                    }

                if($('#city').val() == 0)

         

                    {

         

                        alert("<?php echo e((Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CITY')!= '') ? trans(Session::get('lang_file').'.PLEASE_SELECT_CITY') : trans($OUR_LANGUAGE.'.PLEASE_SELECT_CITY')); ?>");

         

                        return false;

         

                    }

                  for(var i=1;i<=count;i++)

         

                { 

         

                    if($('#fname'+i).val() == '')

         

                    {

         

                        $('#fname'+i).css("border","1px solid red");

         

                        $('#fname'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_FIRST_NAME')!= '') { echo  trans(Session::get('lang_file').'.ENTER_FIRST_NAME');}  else { echo trans($OUR_LANGUAGE.'.ENTER_FIRST_NAME');} ?>');

         

                        return false;

         

                    }

         

                    else

         

                    {

         

                        $('#fname'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }

         

                    if($('#lname'+i).val() == '')

         

                    {

         

                        $('#lname'+i).css("border","1px solid red");

         

                        $('#lname'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_LAST_NAME')!= '') { echo  trans(Session::get('lang_file').'.ENTER_LAST_NAME');}  else { echo trans($OUR_LANGUAGE.'.ENTER_LAST_NAME');} ?>');

         

                        return false;

         

                    }

         

                    else

         

                    {

         

                        $('#lname'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }

         

                    if($('#addr_line'+i).val() == '')

         

                    {

         

                        $('#addr_line'+i).css("border","1px solid red");

         

                        $('#addr_line'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_ADDRESS_LINE1')!= '') { echo  trans(Session::get('lang_file').'.ENTER_ADDRESS_LINE1');}  else { echo trans($OUR_LANGUAGE.'.ENTER_ADDRESS_LINE1');} ?>');

         

                        return false;

         

                    }

         

                    else

         

                    {

         

                        $('#addr_line'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }

         

                    if($('#addr1_line'+i).val() == '')

         

                    {

         

                        $('#addr1_line'+i).css("border","1px solid red");

         

                        $('#addr1_line'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_ADDRESS_LINE2')!= '') { echo  trans(Session::get('lang_file').'.ENTER_ADDRESS_LINE2');}  else { echo trans($OUR_LANGUAGE.'.ENTER_ADDRESS_LINE2');} ?>');

         

                        return false;

         

                    }

         

                    else

         

                    {

         

                        $('#addr1_line'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }           

         

                    if($('#state'+i).val() == '')

         

                    {

         

                        $('#state'+i).css("border","1px solid red");

         

                        $('#state'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STATE')!= '') { echo  trans(Session::get('lang_file').'.ENTER_YOUR_STATE');}  else { echo trans($OUR_LANGUAGE.'.ENTER_YOUR_STATE');} ?>');

         

                        return false;

         

                    }

         

                    else

         

                    {

         

                        $('#state'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }

         

                    if($('#phone1_line'+i).val() == '')

         

                    {

         

                        $('#phone1_line'+i).css("border","1px solid red");

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_PHONE_NO')!= '') { echo  trans(Session::get('lang_file').'.ENTER_YOUR_PHONE_NO');}  else { echo trans($OUR_LANGUAGE.'.ENTER_YOUR_PHONE_NO');} ?>');

         

                        $('#phone1_line'+i).focus();

         

                        return false;

         

                    }

         

                    else if(!phone_regex.test($('#phone1_line'+i).val()))

         

                    {

         

                        $('#phone1_line'+i).css("border","1px solid red");

         

                        $('#phone1_line'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_VALID_PHONE_NO')!= '') { echo  trans(Session::get('lang_file').'.ENTER_YOUR_VALID_PHONE_NO');}  else { echo trans($OUR_LANGUAGE.'.ENTER_YOUR_VALID_PHONE_NO');} ?>');

         

                        return false;

         

                    }
         

                    else

         

                    {

         

                        $('#phone1_line'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }

         

                    

                    if($('#zipcode'+i).val() == '')

         

                    {

         

                        $('#zipcode'+i).css("border","1px solid red");

         

                        $('#zipcode'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_ZIPCODE')!= '') { echo  trans(Session::get('lang_file').'.ENTER_YOUR_ZIPCODE');}  else { echo trans($OUR_LANGUAGE.'.ENTER_YOUR_ZIPCODE');} ?>');

         

                        return false;

         

                    }

         

                    else if(!zip_regex.test($('#zipcode'+i).val()))

                    {

         

                        $('#zipcode'+i).css("border","1px solid red");

         

                        $('#zipcode'+i).focus();

         

                        $('#error_div').html('<?php if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_VALID_ZIPCODE')!= '') { echo  trans(Session::get('lang_file').'.ENTER_YOUR_VALID_ZIPCODE');}  else { echo trans($OUR_LANGUAGE.'.ENTER_YOUR_VALID_ZIPCODE');} ?>');

         

                        return false;

         

                    }

         

                    else

         

                    {

         

                        $('#zipcode'+i).css("border","");

         

                        $('#error_div').html('');

         

                    }

         

                    

         

                }

         

                });

             });

         </script>



          <script>

         //paste this code under head tag or in a seperate js file.

         // Wait for window load

         $(window).load(function() {

            // Animate loader off screen

            $(".se-pre-con").fadeOut("slow");;

         });

      </script>

      <script type="text/javascript">

         function wallet_usage(user_wallet_amount, t, product_total){

        

             var wallet_amount = user_wallet_amount;//$('#user_wallet_amount').val();

             var product_total = product_total;//$('#product_total').val(); 

            //alert(wallet_amount);

             var grand_total   = $('#grand_total').val(); 

             var old_grand_total = $('#old_grand_total').val(); 

             var amount = 0;

             var used = 0;

             if (t.is(':checked')) {

         

                //alert(product_total);

               // alert(wallet_amount);

                if(product_total > wallet_amount){          // if product total is greater than wallet

                     amount = product_total - wallet_amount;  

                     used   = product_total - amount; 

         used_round = parseFloat(used).toFixed(2);  // you will get how much wallet amount we have used

                    //alert(1);

                 }else if(product_total < wallet_amount){     // if product total is less than wallet

                     amount = wallet_amount - product_total;  

                     used   = wallet_amount - amount;         // you will get how much wallet amount we have used

                     //alert(2); 

         used_round = parseFloat(used).toFixed(2); 

                 }

                

                 //alert(used);

                 $('#wallet_used').html(used_round);   //wallet used amount for span 

                 $('input[name=wallet_used_amount]').val(used_round);  // wallet used amount for input

                 $('#if_wallet').show();  //display wallet

                 $('#wallet_apply_final_total').html(parseFloat(grand_total-used_round).toFixed(2));  //displaying grand total span after applying wallet

                 $('input[name=grand_total]').val(parseFloat(grand_total-used_round).toFixed(2));     //grand total input after applying wallet

                

                     /*setting (only wallet amount used for this order) in session*/

                     $.ajax({

                         type: "POST",   

                         url:"<?php echo url('ajax_wallet_session_set'); ?>",

                         data:{'wallet_used_amount':used_round},

                         success:function(response){

         

                         }

                     });

         

             }else{

         

                 $('#wallet_used').html(0);

                 $('input[name=wallet_used_amount]').val(used_round);

                 $('#if_wallet').show();

                 $('#wallet_apply_final_total').html(old_grand_total);

                 $('input[name=grand_total]').val(old_grand_total);

               

                     /*destorying session which is stored the wallet used amount*/

                     $.ajax({

                         type: "POST",   

                         url:"<?php echo url('ajax_wallet_session_unset'); ?>",

                         data:{},

                         success:function(response){

                             

                         }

                     });

             }

             

         }

         

         

         

         function walletCal(user_wallet_amount, t, subtotal) {

         

             var wallet_amount = document.getElementById('user_wallet_amount').value;

             var subtotal      = document.getElementById('subtotal').value;

             var total_price   = document.getElementById('total_price').value;

             var total_price1  = document.getElementById('total_price1').value;

             

         if (t.is(':checked')) {

             var wallet_cal_amount = subtotal - wallet_amount;

             var wallet_total_price = total_price - wallet_amount;

           $('#wallet_apply_total').html('$'+wallet_cal_amount+'.00');

           $('#wallet_apply_final_total').html('$'+wallet_total_price+'.00');

           $('input[name=total_price]').val(wallet_total_price);

           

           if(wallet_total_price <= 0){

             //$('#paypal_cod').hide();

             $('#wallet_apply_total').html('$'+'0.00');

             $('#wallet_apply_final_total').html('$'+'0.00');

           }

           $.ajax({

                         type: "POST",   

                         url:"<?php echo url('ajax_wallet_session_set'); ?>",

                         data:{'wallet_amount':wallet_amount,'wallet_total_price':wallet_total_price},

                         success:function(response){

         

                         }

                     });

         } else {

           

           $('#wallet_apply_total').html('$'+subtotal+'.00');

           $('#wallet_apply_final_total').html('$'+total_price1+'.00');

           $('input[name=total_price]').val(total_price1);

           if(total_price1 > 0){

            // $('#paypal_cod').show();

           }

                 $.ajax({

                         type: "POST",   

                         url:"<?php echo url('ajax_wallet_session_unset'); ?>",

                         data:{},

                         success:function(response){

                             

                         }

                     });

         }

         }

      </script>      

      <script type="text/javascript">

         $.ajaxSetup({

             headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

         });

      </script> 

      <script>

         $(document).on("ready", function() {

             $("#orderstatus").wizard({

                 onfinish: function() {

                     console.log("Hola mundo");

                 }

             });

         });

         /* Mobile Number Validation */

         function isNumber(evt) {

         evt = (evt) ? evt : window.event;

         var charCode = (evt.which) ? evt.which : evt.keyCode;

         if (charCode > 31 && (charCode < 48 || charCode > 57)) {

         return false;

         }

         return true;

         }

      </script>



</body>

</html>