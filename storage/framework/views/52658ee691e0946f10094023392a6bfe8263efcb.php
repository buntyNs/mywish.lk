<aside class="right sidebar col-sm-3 col-xs-12">

                  <div class="sidebar-account block">

                    <div class="sidebar-bar-title">

                      <h3><?php if(Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?> <?php endif; ?></h3>

                    </div>

                    <div class="block-content">

                      <ul>

                      <ul>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'acc_dashboard') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('acc_dashboard')); ?>" ><?php if(Lang::has(Session::get('lang_file').'.ACC_DASHBOARD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ACC_DASHBOARD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ACC_DASHBOARD')); ?> <?php endif; ?></a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'product_paypal') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('product_paypal')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_PRODUCT_PAYPAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_PRODUCT_PAYPAL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_PRODUCT_PAYPAL')); ?> <?php endif; ?></a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'product_payumoney') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('product_payumoney')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_PRODUCT_PAYUMONEY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_PRODUCT_PAYUMONEY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_PRODUCT_PAYUMONEY')); ?> <?php endif; ?></a></li>
                        
                        <?php if($GENERAL_SETTING->gs_payment_status == 'COD'): ?>
                          <!-- check COD payment enable or disable to view COD details -->
                        <li <?php if(Route::getCurrentRoute()->uri() == 'product_cod') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('product_cod')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_PRODUCT_COD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_PRODUCT_COD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_PRODUCT_COD')); ?> <?php endif; ?></a>
                        </li>
                        <?php else: ?>
                        <?php endif; ?>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'deal_paypal') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('deal_paypal')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_DEAL_PAYPAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_DEAL_PAYPAL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_DEAL_PAYPAL')); ?> <?php endif; ?></a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'deal_payumoney') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('deal_payumoney')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_DEAL_PAYUMONEY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_DEAL_PAYUMONEY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_DEAL_PAYUMONEY')); ?> <?php endif; ?></a></li>

                         <?php if($GENERAL_SETTING->gs_payment_status == 'COD'): ?>
                         <!-- check COD payment enable or disable to view COD details -->
                        <li <?php if(Route::getCurrentRoute()->uri() == 'deal_cod') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('deal_cod')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_DEAL_COD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_DEAL_COD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_DEAL_COD')); ?> <?php endif; ?></a>
                        </li>
                        <?php else: ?>
                        <?php endif; ?>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'user_wishlist') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('user_wishlist')); ?>"><?php if(Lang::has(Session::get('lang_file').'.MY_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_WISHLIST')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_WISHLIST')); ?> <?php endif; ?></a></li>

                        <li <?php if(Route::getCurrentRoute()->uri() == 'shipping_address') { ?> class="current"  <?php } else {?> class="" <?php } ?>><a href="<?php echo e(url('shipping_address')); ?>"> <?php if(Lang::has(Session::get('lang_file').'.MY_SHIPPING_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_SHIPPING_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_SHIPPING_ADDRESS')); ?> <?php endif; ?></a></li>



                      </ul>

                    </div>

                  </div>



                </aside>