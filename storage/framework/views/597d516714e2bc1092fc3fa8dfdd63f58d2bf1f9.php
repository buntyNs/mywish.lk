<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9">
      <![endif]-->
      <!--[if !IE]><!--> 
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <meta charset="UTF-8" />
            <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT'): trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')); ?> | <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_DEALS'): trans($MER_OUR_LANGUAGE.'.MER_ADD_DEALS')); ?></title>
            <meta content="width=device-width, initial-scale=1.0" name="viewport" />
            <meta content="" name="description" />
            <meta content="" name="author" />
            <meta name="_token" content="<?php echo csrf_token(); ?>"/>
            <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
            <!-- GLOBAL STYLES -->
            <!-- GLOBAL STYLES -->
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
            <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
            <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
            <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
            <?php endif; ?>
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
            <link href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
            <!-- New Date picker-->
            <link href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
            <!-- New Date picker-->
            <!--END GLOBAL STYLES -->
            <!-- PAGE LEVEL STYLES -->
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/Markdown.Editor.hack.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/jquery.cleditor-hack.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-wysihtml5-hack.css" />
            <style>
               ul.wysihtml5-toolbar > li {
               position: relative;
               }
               #dtpickerdemo .bootstrap-datetimepicker-widget
               {
               padding-left: 40px;
               margin-left:15px;
               }
               #dtpickerdemo1 .bootstrap-datetimepicker-widget
               {
               padding-left: 40px;
               margin-left:15px;
               }
            </style>
            <!--END GLOBAL STYLES -->
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->
         </head>
         <!-- END HEAD -->
         <!-- BEGIN BODY -->
         <body class="padTop53 " >
            <!-- MAIN WRAPPER -->
            <div id="wrap">
               <!-- HEADER SECTION -->
               <?php echo $adminheader; ?>

               <!-- END HEADER SECTION -->
               <!-- MENU SECTION -->
               <?php echo $adminleftmenus; ?>

               <!--END MENU SECTION -->
               <div></div>
               <!--PAGE CONTENT -->
               <div id="content">
                  <div class="inner">
                     <div class="row">
                        <div class="col-lg-12">
                           <ul class="breadcrumb">
                              <li class=""><a href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                              <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_DEALS')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_ADD_DEALS') : trans($MER_OUR_LANGUAGE.'.MER_ADD_DEALS')); ?></a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="box dark">
                              <header>
                                 <div class="icons"><i class="icon-edit"></i></div>
                                 <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_DEALS')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_ADD_DEALS') : trans($MER_OUR_LANGUAGE.'.MER_ADD_DEALS')); ?></h5>
                              </header>
                              <?php if($errors->any()): ?>
                              <br>
                              <ul style="color:red;">
                                 <div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

                                    <?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

                                 </div>
                              </ul>
                              <?php endif; ?>
                              <div id="div-1" class="accordion-body collapse in body">
                                 <?php if(isset($action) && $action == 'save'): ?> <?php  $process = 'save'; $button = 'Add Deals'; $form_action = 'add_deals_submit';  ?>
                                 <?php elseif(isset($action) && $action == 'update'): ?> <?php $process = 'update'; $button = 'Update Deals';   ?> <?php endif; ?>
                                 <?php echo Form::open(array('url'=>'mer_add_deals_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

                                 <?php echo e(Form::hidden('action',$process)); ?>

                                 <?php /* print_r($errors->all()); */ ?>
                                 <div class="form-group">
                                    <?php echo e(Form::label('','',['for' => 'text1', 'class' => 'control-label col-lg-2'])); ?> 
                                    <div class="col-lg-8">
                                       <div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_TITLE'): trans($MER_OUR_LANGUAGE.'.MER_DEAL_TITLE')); ?><span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <input id="title" name="title" placeholder="Enter Deal Title <?php echo e($default_lang); ?>"  class="form-control" type="text">
                                       <input id="check_title_val" name="check_title_val" placeholder="" value="0" class="form-control" type="hidden" onChange="check();">
                                       <?php if($errors->has('title')): ?> 
                                       <div id="title_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('title')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php 
                                    /* print_r($get_active_lang); */ ?>
                                 <?php if(!empty($get_active_lang)): ?>  
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php 
                                    $get_lang_name = $get_lang->lang_name;
                                    ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2">Deal Title(<?php echo e($get_lang_name); ?>) :<span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <input id="title_<?php echo e($get_lang_name); ?>"  name="title_<?php echo e($get_lang_name); ?>" placeholder="Enter Deal Title In <?php echo e($get_lang_name); ?>" class="form-control" type="text" >
                                       <?php if($errors->has('title_'.$get_lang_name.'')): ?>
                                       <div id="title_<?php echo e($get_lang_name); ?>_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('title_'.$get_lang_name.'')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>
                                 <!--
                                    <div class="form-group">
                                                    <label for="text1" class="control-label col-lg-2">Deal Title(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                                    
                                                    <div class="col-lg-8">
                                                        <input id="title_fr" name="title_fr" placeholder="Enter Deal Title in <?php echo e(Helper::lang_name()); ?>" class="form-control" type="text">
                                                    
                                        <div id="title_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
                                                    </div>
                                                </div>-->
                                 <div class="form-group">
                                    <label for="pass1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TOP_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TOP_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_TOP_CATEGORY')); ?><span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <select class="form-control" name="category" id="category" onChange="select_main_cat(this.value)" >
                                          <option value="0">--- <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> ---</option>
                                          <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                                          <option value="<?php echo e($cat_list->mc_id); ?>">
                                             <?php echo e($cat_list->mc_name); ?><!--(<?php //echo $cat_list->mc_name_fr; ?>)-->
                                          </option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       </select>
                                       <?php if($errors->has('category')): ?> 
                                       <div id="category_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('category')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_SELECT_MAIN_CATEGORY')); ?><span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <select class="form-control" name="maincategory" id="maincategory" onChange="select_sub_cat(this.value)" >
                                          <option value="0">---<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?>---</option>
                                       </select>
                                       <?php if($errors->has('maincategory')): ?> 
                                       <div id="category_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('maincategory')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_SELECT_SUB_CATEGORY')); ?><span class="text-sub"></span></label>
                                    <div class="col-lg-8">
                                       <select class="form-control" name="subcategory" id="subcategory" onChange="select_second_sub_cat(this.value)" >
                                          <option value="0">---<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> ---</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY')   : trans($MER_OUR_LANGUAGE.'.MER_SELECT_SECOND_SUB_CATEGORY')); ?><span class="text-sub"></span></label>
                                    <div class="col-lg-8">
                                       <select class="form-control" name="secondsubcategory" id="secondsubcategory"  >
                                          <option value="0">---<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?>---</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE')); ?> (<?php echo e(Helper::cur_sym()); ?>) <span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <input id="originalprice" name="originalprice" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" class="form-control" type="text" maxlength="10">
                                       <?php if($errors->has('originalprice')): ?> 
                                       <div id="category_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('originalprice')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_DISCOUNTED_PRICE')); ?>(<?php echo e(Helper::cur_sym()); ?>) <span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <input id="discountprice" name="discountprice" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" class="form-control" type="text" maxlength="10">
                                       <?php if($errors->has('discountprice')): ?> 
                                       <div id="category_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('discountprice')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>

                                    <div class="col-lg-8">
                                       <input type="checkbox" onclick="if(this.checked){$('#inctax').show();}else{$('#inctax').hide();$('#inctax').val(0)}"> ( <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_INCLUDING_TAX_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INCLUDING_TAX_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_INCLUDING_TAX_AMOUNT')); ?> ) 
                                       <input placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" style="display:none;" class="form-control" type="number" id="inctax" min="1" max="99" name="inctax"> %
                                       <div id="tax_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <div class="form-group"  >
                                    <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT'): trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT')); ?><span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'none');" value="1" checked > <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FREE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FREE') : trans($MER_OUR_LANGUAGE.'.MER_FREE')); ?></label>
                                       <input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'block');" value="2"  ><label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '')   ? trans(Session::get('mer_lang_file').'.MER_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')); ?></label>
                                       <label class="sample"></label>
                                    </div>
                                 </div>
                                 <div class="form-group" id="showship" style="display:none;">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT'): trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT')); ?> (<?php echo e(Helper::cur_sym()); ?>)<span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <input id="Shipping_Amount" name="Shipping_Amount" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'. MER_NUMBERS_ONLY') != '') ? trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" class="form-control" type="text" maxlength="10">
                                       <!--<input  placeholder="" class="form-control" type="text" id="Shipping_Amount" name="Shipping_Amount" maxlength="10">-->
                                       <div id="ship_amt_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_START_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_START_DATE'): trans($MER_OUR_LANGUAGE.'.MER_START_DATE')); ?><span class="text-sub">*</span></label>
                                    <div class='col-sm-4 input-group date' id='dtpickerdemo1'>
                                       <?php echo e(Form::text('startdate','',array('id'=>'startdate','class' => 'form-control'))); ?>

                                       <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                       </span>
                                    </div>
                                    <div id="end_date_error_msg"  style="color:#F00;font-weight:800"> </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_END_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_END_DATE'):trans($MER_OUR_LANGUAGE.'.MER_END_DATE')); ?><span class="text-sub">*</span></label>
                                    <div class='col-sm-4 input-group date' id='dtpickerdemo'>
                                       <?php echo e(Form::text('enddate','',array('id'=>'enddate','class' => 'form-control'))); ?>

                                       <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                       </span>
                                    </div>
                                 </div>
                                 <div id="end_date_error_msg"  style="color:#F00;font-weight:800"> </div>
                                 <?php /*
                                    <div class="form-group">
                                                  <label for="text1" class="control-label col-lg-2">Deal Expiry Date<span class="text-sub">*</span></label>
                                    
                                                  <div class="col-lg-3" >
                                                      <div id="datetimepicker3" class=" date input-group">
                                                      <input data-format="yyyy-MM-dd hh:mm:ss"  type="text" id="expirydate" name="expirydate" class="form-control"></input>
                                                      <span class="add-on input-group-addon">
                                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                                      </span>
                                    </div>
                                      <div id="expiry_date_error_msg"  style="color:#F00;font-weight:800"> </div>
                                                  </div>
                                              </div>
                                    */?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION')); ?><span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <textarea id="wysihtml5" placeholder="Enter Description <?php echo e($default_lang); ?>" name="description" class="wysihtml5 form-control" rows="10" ></textarea>
                                       <?php if($errors->has('description')): ?> 
                                       <div id="category_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('description')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php if(!empty($get_active_lang)): ?> 
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php 
                                    $get_lang_name = $get_lang->lang_name;
                                    ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2">Description(<?php echo e($get_lang_name); ?>) :<span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <textarea id="wysihtml5" placeholder="Enter Description in <?php echo e($get_lang_name); ?>" name="description_<?php echo e($get_lang_name); ?>" class="wysihtml5 form-control" rows="10" ></textarea>
                                       <?php if($errors->has('description_'.$get_lang_name.'')): ?>
                                       <div id="title_<?php echo e($get_lang_name); ?>_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('description_'.$get_lang_name.'')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>
                                 <!--
                                    <div class="form-group">
                                                    <label for="text1" class="control-label col-lg-2">Description(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                                    
                                                    <div class="col-lg-8">
                                                       <textarea id="wysihtml5" placeholder="Enter Description in French" name="description_fr" class="wysihtml5 form-control" rows="10" ></textarea>
                                         <div id="desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                                      </div>
                                      
                                                </div>--> 
                                 <?php echo e(Form::hidden('merchant',Session::get('merchantid'),array('id'=>'merchant','class' => 'form-control'))); ?>

                                 <div class="form-group">
                                    <label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STORE')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_SELECT_STORE'): trans($MER_OUR_LANGUAGE.'.MER_SELECT_STORE')); ?><span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <select class="form-control" id="shop" name="shop" onChange="check();">
                                          <option value="">-- <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_STORE')  : trans($MER_OUR_LANGUAGE.'.MER_SELECT_STORE')); ?>--</option>
                                          <?php $__currentLoopData = $get_mer_store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($store->stor_id); ?>">
                                             <?php echo e($store->stor_name); ?><!--(<?php // echo $store->stor_name_fr; ?>)-->
                                          </option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       </select>
                                       <div id="address"></div>
                                       <div id="store_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php echo e(Form::hidden('exist','',array('id'=>'product_id'))); ?>

                                 <input type="hidden" name="exist" id="exist" value="">
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS') : trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS')); ?><span class="text-sub"></span></label>
                                    <div class="col-lg-8">
                                       <textarea id="metakeyword" placeholder="Enter Meta Keywords <?php echo e($default_lang); ?>" name="metakeyword" class="form-control" ></textarea>
                                       <?php if($errors->has('metakeyword')): ?>
                                       <div id="meta_key_<?php echo e($get_lang_name); ?>_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('metakeyword')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php if(!empty($get_active_lang)): ?> 
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php 
                                    $get_lang_name = $get_lang->lang_name;
                                    ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2">Meta Keywords(<?php echo e($get_lang_name); ?>) :<span class="text-sub"></span></label>
                                    <div class="col-lg-8">
                                       <textarea id="metakeyword_<?php echo e($get_lang_name); ?>" placeholder="Enter Keywords in <?php echo e($get_lang_name); ?>" name="metakeyword_<?php echo e($get_lang_name); ?>" class="form-control" ></textarea>
                                       <?php if($errors->has('metadescription')): ?>
                                       <div id="meta_key_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('metadescription')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>
                                 <!--
                                    <div class="form-group">
                                                    <label for="text1" class="control-label col-lg-2">Meta Keywords(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                                    
                                                    <div class="col-lg-8">
                                                       <textarea id="metakeyword_fr" name="metakeyword_fr" placeholder="Enter Keywords in <?php echo e(Helper::lang_name()); ?>" class="form-control" ></textarea>
                                         <div id="meta_key_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                                                    </div>
                                                </div>
                                    -->
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')); ?><span class="text-sub"></span></label>
                                    <div class="col-lg-8">
                                       <textarea id="metadescription"  placeholder="Enter Meta Description <?php echo e($default_lang); ?>" name="metadescription" class="form-control"></textarea>
                                       <?php if($errors->has('metadescription')): ?>
                                       <div id="meta_desc_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('metadescription')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php if(!empty($get_active_lang)): ?> 
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php  
                                    $get_lang_name = $get_lang->lang_name;
                                    ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2">Meta Description(<?php echo e($get_lang_name); ?>) :<span class="text-sub">*</span></label>
                                    <div class="col-lg-8">
                                       <textarea id="metadescription_<?php echo e($get_lang_name); ?>" placeholder="Enter Meta Description in <?php echo e($get_lang_name); ?>" name="metadescription_<?php echo e($get_lang_name); ?>" class="form-control" ></textarea>
                                       <?php if($errors->has('metadescription_'.$get_lang_name.'')): ?>
                                       <div id="meta_desc_<?php echo e($get_lang_name); ?>_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('metadescription_'.$get_lang_name.'')); ?></div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>
                                 <!--<div class="form-group">
                                    <label for="text1" class="control-label col-lg-2">Meta Description(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                                    
                                    <div class="col-lg-8">
                                       <textarea id="metadescription_fr"  placeholder="Enter Meta Description in <?php echo e(Helper::lang_name()); ?>" name="metadescription_fr" class="form-control"></textarea>
                                    <div id="meta_desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                    </div>-->
                                 <?php /*  <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_MINIMUM_DEAL_LIMIT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MINIMUM_DEAL_LIMIT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MINIMUM_DEAL_LIMIT');} ?><span class="text-sub">*</span></label>
                                 <div class="col-lg-8">
                                    <input id="minlimit" name="minlimt" placeholder="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY');} ?>" class="form-control" type="text">
                                    <div id="mini_deal_error_msg"  style="color:#F00;font-weight:800"> </div>
                                 </div>
                              </div>
                              */ ?>
                              <div class="form-group">
                                 <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAXIMUM_USER_LIMIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAXIMUM_USER_LIMIT') : trans($MER_OUR_LANGUAGE.'.MER_MAXIMUM_USER_LIMIT')); ?><span class="text-sub">*</span></label>
                                 <div class="col-lg-8">
                                    <input id="maxlimit" name="maxlimit" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')  : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" class="form-control" type="text" maxlength="5">
                                    <?php if($errors->has('maxlimit')): ?> 
                                    <div id="category_error_msg"  style="color:#F00;font-weight:800"  > <?php echo e($errors->first('maxlimit')); ?></div>
                                    <?php endif; ?>
                                 </div>
                              </div>
                              <?php /*?>  
                              <div class="form-group">
                                 <label for="text1" class="control-label col-lg-2">Deal Purchase Limit Per Customer<span class="text-sub">*</span></label>
                                 <div class="col-lg-8">
                                    <input id="purchaselimit" name="purchaselimit" placeholder="Numbers Only" class="form-control" type="text">
                                 </div>
                              </div>
                              <?php */?>
                              <?php /*  Product Policy details */ ?>
                              
                              <div class="form-group"  >
                                 <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_APPLY_CANCEL')!= '')   ?  trans(Session::get('admin_lang_file').'.BACK_APPLY_CANCEL') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_CANCEL')); ?><span class="text-sub">*</span></label>
                                 <div class="col-lg-8">
                                    <input type="radio" id="allow_cancel" name="allow_cancel"  value="1" onClick="setPolicyDisplay('cancel_tab', 'block')"> <label class="sample"  ><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_YES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_YES') : trans($MER_OUR_LANGUAGE.'.BACK_YES')); ?></label>
                                    <input type="radio" id="notallow_cancel" name="allow_cancel"  value="0" checked  onclick="setPolicyDisplay('cancel_tab', 'none')"><label class="sample"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO'): trans($MER_OUR_LANGUAGE.'.BACK_NO')); ?></label>
                                    <label class="sample"></label>
                                 </div>
                              </div>
                              <div id="cancel_tab" style="display:none;">
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')   : trans($MER_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION')); ?> <span class="text-sub">*</span></label>
                                    <div class="col-lg-8" id="description">
                                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="cancellation_policy" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION')!= '')? trans(Session::get('admin_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_ENTER_CANCEL_DESCRIPTION')); ?>"><?php echo e(old('cancellation_policy')); ?></textarea>
                                       <div id="cancellation_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php if(!empty($get_active_lang)): ?>  
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php 
                                    $get_lang_name = $get_lang->lang_name;
                                                 ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL_DESCRIPTION')   : trans($MER_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                                    <div class="col-lg-8" id="description">
                                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="cancellation_policy_<?php echo e($get_lang_name); ?>" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION'):trans($MER_OUR_LANGUAGE.'.BACK_ENTER_CANCEL_DESCRIPTION')); ?>"><?php echo e(old('cancellation_policy_'.$get_lang_name)); ?></textarea>
                                       <div id="cancellation_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>
                                 <div class="form-group">
                                    <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>

                                    <div class="col-lg-8">
                                       <label for="text1" class="control-label "><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION')); ?><span class="text-sub"></span></label>     
                                       <input placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION')); ?>"  class="form-control" type="text"  id="cancellation_days" name="cancellation_days" value="<?php echo e(old('cancellation_days')); ?>" maxlength="3">
                                       <div id="cancellation_days_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="form-group"  >
                                 <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_APPLY_RETURN')!= '')   ? trans(Session::get('admin_lang_file').'.BACK_APPLY_RETURN')  : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_RETURN')); ?><span class="text-sub">*</span></label>
                                 <div class="col-lg-8">
                                    <input type="radio" id="allow_return" name="allow_return"  value="1" onclick="setPolicyDisplay('return_tab', 'block')" > <label class="sample"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_YES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_YES') : trans($MER_OUR_LANGUAGE.'.BACK_YES')); ?></label>
                                    <input type="radio" id="notallow_return" name="allow_return"  value="0" checked onclick="setPolicyDisplay('return_tab', 'none')" ><label class="sample"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO'): trans($MER_OUR_LANGUAGE.'.BACK_NO')); ?></label>
                                    <label class="sample"></label>
                                 </div>
                              </div>
                              <div id="return_tab" style="display:none;">
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')   : trans($MER_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION')); ?>  <span class="text-sub">*</span></label>
                                    <div class="col-lg-8" id="description">
                                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="return_policy" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION')!= '') ? trans(Session::get('admin_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_ENTER_RETURN_DESCRIPTION')); ?>"><?php echo e(old('return_policy')); ?></textarea>
                                       <div id="return_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php if(!empty($get_active_lang)): ?>  
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php  
                                    $get_lang_name = $get_lang->lang_name;
                                                  ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RETURN_DESCRIPTION')   : trans($MER_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                                    <div class="col-lg-8" id="description">
                                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="return_policy_<?php echo e($get_lang_name); ?>" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_ENTER_RETURN_DESCRIPTION')); ?>"><?php echo e(old('return_policy_'.$get_lang_name)); ?></textarea>
                                       <div id="return_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>
                                    <div class="col-lg-8">
                                       <label for="text1" class="control-label"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION')); ?><span class="text-sub"></span></label>
                                       <input placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION')); ?>"  class="form-control" type="text"  id="return_days" name="return_days" value="<?php echo e(old('return_days')); ?>" maxlength="3">
                                       <div id="return_days_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="form-group"  >
                                 <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_APPLY_REPLACEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_APPLY_REPLACEMENT')  : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_REPLACEMENT')); ?> <span class="text-sub">*</span></label>
                                 <div class="col-lg-8">
                                    <input type="radio" id="allow_replace" name="allow_replace"  value="1" onclick="setPolicyDisplay('replace_tab', 'block')"  > <label class="sample"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_YES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_YES') : trans($MER_OUR_LANGUAGE.'.BACK_YES')); ?></label>
                                    <input type="radio" id="notallow_replace" name="allow_replace"  value="0" checked  onclick="setPolicyDisplay('replace_tab', 'none')" ><label class="sample"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO'): trans($MER_OUR_LANGUAGE.'.BACK_NO')); ?></label>
                                    <label class="sample"></label>
                                 </div>
                              </div>
                              <div id="replace_tab" style="display:none;">
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')   : trans($MER_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')); ?> <span class="text-sub">*</span></label>
                                    <div class="col-lg-8" id="description">
                                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="replacement_policy" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_ENTER_REPLACE_DESCRIPTION')); ?>"><?php echo e(old('replacement_policy')); ?></textarea>
                                       <div id="replacement_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php if(!empty($get_active_lang)): ?> 
                                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                 <?php
                                    $get_lang_name = $get_lang->lang_name;
                                                  ?>
                                 <div class="form-group">
                                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REPLACE_DESCRIPTION')   : trans($MER_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                                    <div class="col-lg-8" id="description">
                                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="replacement_policy_<?php echo e($get_lang_name); ?>" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_ENTER_REPLACE_DESCRIPTION')); ?>"><?php echo e(old('replacement_policy_'.$get_lang_name)); ?></textarea>
                                       <div id="replacement_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 <?php endif; ?>         
                                 <div class="form-group">
                                    <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>

                                    <div class="col-lg-8">
                                       <label for="text1" class="control-label "><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION')); ?><span class="text-sub"></span></label><br>
                                       <input placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION')); ?>"  class="form-control" type="text"  id="replace_days" name="replace_days" id="replace_days" value="<?php echo e(old('Replace_days')); ?>"  maxlength="3" max="5">
                                       <div id="replace_days_error_msg"  style="color:#F00;font-weight:800"> </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <?php /*  Product Policy details ends */ ?>
                              <div class="form-group">
                                 <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_DEAL_IMAGE')); ?><span class="text-sub">*</span><br><span  style="color:#999">(<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAX')!= '') ? trans(Session::get('mer_lang_file').'.MER_MAX'): trans($MER_OUR_LANGUAGE.'.MER_MAX')); ?> 5)</span><br></label>
                                 <span class="errortext red" style="color:red; margin-left: 20px;"><em><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE')  : trans($MER_OUR_LANGUAGE.'.MER_IMAGE_SIZE_MUST_BE')); ?>  <?php echo e($DEAL_WIDTH); ?> x <?php echo e($DEAL_HEIGHT); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PIXELS'): trans($MER_OUR_LANGUAGE.'.MER_PIXELS')); ?></em></span>
                                 <div class="col-lg-8" id="img_upload">
                                    <div style="display: block; overflow: hidden;">
                                       <input type="file" name="file[]" id="fileUpload1" value="" onchange="imageval(1)"  required />
                                       <a href="javascript:void(0);"  title="Add field" class="chose-file-add" ><span id="add_button" style="cursor:pointer;width:84px;"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_ADD'): trans($MER_OUR_LANGUAGE.'.MER_ADD')); ?></span></a>
                                    </div>
                                 </div>
                                 <?php echo e(Form::hidden('count','1',array('id'=>'count'))); ?>

                                 <div id="img_error_msg"  style="color:#F00;font-weight:800"></div>
                              </div>
                              <div class="form-group">
                                 <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'pass1'])); ?>

                                 <div class="col-lg-8">
                                    <button class="btn btn-warning btn-sm btn-grad" id="submit_deal" ><a style="color:#fff"  ><?php echo e($button); ?></a></button>
                                    <button class="btn btn-danger btn-sm btn-grad" type="reset" ><a style="color:#ffffff;"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET'): trans($MER_OUR_LANGUAGE.'.MER_RESET')); ?></a></button>
                                 </div>
                              </div>
                              <?php echo e(Form::close()); ?>

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--END PAGE CONTENT -->
            </div>
            <!--END MAIN WRAPPER -->
            <!-- FOOTER -->
            <?php echo $adminfooter; ?>

            <!--END FOOTER --> 
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
            
            <script type="text/javascript">
               function setPolicyDisplay(id, displayOption) 
               {
                   $("#"+id).css('display',displayOption);
               } 
            </script>
            
            <script type='text/javascript'>
               $(document).ready(function(){
                   var maxField = 5; //Input fields increment limitation
                   var addButton = $('#add_button'); //Add button selector
                   var wrapper = $('#img_upload'); //Input field wrapper //div
                
                   var x = 1; //Initial field counter is 1
                   $(addButton).click(function(){ //Once add button is clicked
                       if(x < maxField){ //Check maximum number of input fields
                      x++; //Increment field counter
                    var fieldHTML = '<div style="display:block; width: 350px; margin-top:15px;"><input type="file" name="file[]" id="fileUpload'+x+'" onchange="imageval('+x+')"  value="" required/><div id="remove_button"><a href="javascript:void(0);"  title="Remove field" style="color:#ffffff;">Remove</a></div></div>'; //New input field html 
                           
                           $(wrapper).append(fieldHTML); // Add field html
                    
                    document.getElementById('count').value = parseInt(x);
                       }
                   });
                   $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
                       e.preventDefault();
                       $(this).parent('div').remove(); //Remove field html
                       x--; //Decrement field counter
                  document.getElementById('count').value = parseInt(x);
                   });
               });
               
               $(document).ready(function(){
                   var counter = 2;
                   $('#del_file').hide();
                   $('img#add_file').click(function(){
                       $('#file_tools').before('<br><div class="col-lg-8" id="f'+counter+'"><input name="file[]" type="file"></div>');
                       $('#del_file').fadeIn(0);
                   counter++;
                   });
                   $('img#del_file').click(function(){
                       if(counter==3){
                           $('#del_file').hide();
                       }   
                       counter--;
                       $('#f'+counter).remove();
                   });
               });
            </script>
            <script>
               function select_main_cat(id)
               {
                 var passData = 'id='+id;
                   $.ajax( {
                        type: 'get',
                      data: passData,
                      url: '<?php echo url('mer_deals_select_main_cat'); ?>',
                      success: function(responseText){  
                       if(responseText)
                       { 
                      $('#maincategory').html(responseText);  
                      $('#subcategory').html(0);  
                      $('#secondsubcategory').html(0);            
                       }
                    }   
                  });   
               }
               
               function select_sub_cat(id)
               {
                var passData = 'id='+id;
                   $.ajax( {
                        type: 'get',
                      data: passData,
                      url: '<?php echo url('mer_deals_select_sub_cat'); ?>',
                      success: function(responseText){  
                       if(responseText)
                       { 
                      $('#subcategory').html(responseText);            
                       }
                    }   
                  });   
               }
               
               function select_second_sub_cat(id)
               {
                var passData = 'id='+id;
                   $.ajax( {
                        type: 'get',
                      data: passData,
                      url: '<?php echo url('mer_deals_select_second_sub_cat'); ?>',
                      success: function(responseText){  
                       if(responseText)
                       { 
                      $('#secondsubcategory').html(responseText);            
                       }
                    }   
                  });   
               }
               
               function check(){
               
                  var mer_id   = $('#merchant').val();
                  var store_id = $('#shop').val();
                  var title    = $('#title').val();
                  var pass = "mer_id="+mer_id+"&store_id="+store_id+"&title="+title;
                  //alert(pass);
                  //exit();
                  $.ajax({
                        type: 'get',
                      data: pass,
                    
                      url: '<?php echo url('check_title_exist');?>',
                      success: function(responseText){  
                       
                       if(responseText==1){  //already exist
                        alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_THIS_DEAL_TITLE_ALREADY_EXIST_IN_THIS_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_THIS_DEAL_TITLE_ALREADY_EXIST_IN_THIS_STORE') : trans($MER_OUR_LANGUAGE.'.MER_THIS_DEAL_TITLE_ALREADY_EXIST_IN_THIS_STORE')); ?>");
                        $("#exist").val("1"); //already exist
                        $("#title").css('border', '1px solid red'); 
                        $('#Shop').css('border', '1px solid red');
                        $('#title_error_msg').html("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST')!= '') ?  trans(Session::get('mer_lang_file').'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST'): trans($MER_OUR_LANGUAGE.'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST')); ?>");  
                        $("#title").focus();
                        return false;          
                       }else if(responseText==0){
                        $("#exist").val("0");
                        $("#title").css('border', ''); 
                        $('#Shop').css('border', '');
                        $('#title_error_msg').html('');
                        return true;
                       }
                    }   
                  }); 
               }
               /*displaying store address*/
               $("#shop").change(function(){
                    var shop_id  = $(this).find('option:selected').val();
                var passdata = "shop_id="+shop_id;
                $.ajax({
                        type: 'get',
                      data: passdata,
                      url: 'store_details',
                      success: function(responseText){ 
                      $('#address').html(responseText);
                      }   
                });   
               });
               function setshipVisibility(id, visibility) 
               {
               document.getElementById(id).style.display = visibility;
               
               }
               function imageval(i){  /*Image size validation*/
                
               //var img_count = $("#count").val();
                //Get reference of FileUpload.
                  // alert(i);
               // var i;
               //for (i = 0; i <=img_count; i++) {
                    var fileUpload = document.getElementById("fileUpload"+i);
               //}
               
               //Check whether the file is valid Image.
                  var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
                  if (regex.test(fileUpload.value.toLowerCase())) {
               
                      //Check whether HTML5 is supported.
                      if (typeof (fileUpload.files) != "undefined") {
                          //Initiate the FileReader object.
                          var reader = new FileReader();
                          //Read the contents of Image File.
                          reader.readAsDataURL(fileUpload.files[0]);
                          reader.onload = function (e) {
                              //Initiate the JavaScript Image object.
                              var image = new Image();
               
                              //Set the Base64 string return from FileReader as source.
                              image.src = e.target.result;
                                     
                              //Validate the File Height and Width.
                              image.onload = function () {
                                  var height = this.height;
                                  var width = this.width;
                                  if (width < 500 || height < 700) {
                                      //alert("Image Height and Width should have 500 * 700 px.");
                                      return false;
                                  }
                                  //alert("Uploaded image has valid Height and Width.");
                                  return true;
                              };
               
                          }
                      } else {
                          //alert("This browser does not support HTML5.");
                          //return false;
                      }
                  } else {
                      //alert("Please select a valid Image file.");
                     // return false;
                  }
               }
               
                $( document ).ready(function() {
                  
                      var title        = $('#title');
                
                      var category     = $('#category');
                   var maincategory    = $('#maincategory');
                var subcategory    = $('#subcategory');
                var secondsubcategory= $('#secondsubcategory');
                var originalprice    = $('#originalprice');
                var discountprice    = $('#discountprice');
                //var Shipping_Amount  = $('#Shipping_Amount');
                var startdate      = $('#startdate');
                var enddate      = $('#enddate');
                var expirydate     = $('#expirydate');
                var wysihtml5      = $('#wysihtml5');
                var merchant     = $('#merchant');
                var shop       = $('#shop');
                var metakeyword    = $('#metakeyword');
                var metadescription  = $('#metadescription');
                var minlimt      = $('#minlimit');
                var maxlimit     = $('#maxlimit');
                var purchaselimit  = $('#purchaselimit');
                var file       = $('#file');
                  var Shipping_Amount    = $('#Shipping_Amount');
                  
                  var inctax    = $('#inctax');
               
                    
                /*Deal Origianl Price*/
                    $('#originalprice').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                          originalprice.css('border', '1px solid red'); 
                  $('#org_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  originalprice.focus();
                  return false;
                      }else{      
                          originalprice.css('border', ''); 
                  $('#org_price_error_msg').html('');         
                }
                    });
               
                /*shipping amount*/
                    $('#Shipping_Amount').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                          Shipping_Amount.css('border', '1px solid red'); 
                    $('#Shipping_Amount_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                    Shipping_Amount.focus();
                    return false;
                      }else{      
                          Shipping_Amount.css('border', ''); 
                    $('#Shipping_Amount_error_msg').html('');         
                  }
                    });
                 /*tax percentage*/
                  $('#inctax').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                          inctax.css('border', '1px solid red'); 
                    $('#tax_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                    inctax.focus();
                    return false;
                      }else{      
                          inctax.css('border', ''); 
                    $('#tax_error_msg').html('');         
                  }
                    });
                
                /*Deal Discount Price*/
                 $('#discountprice').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                          discountprice.css('border', '1px solid red'); 
                  $('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  discountprice.focus();
                  return false;
                      }else{      
                          discountprice.css('border', ''); 
                  $('#dis_price_error_msg').html('');         
                }
                   });
               
                 /*minimum Limit*/  
                 $('#minlimit').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                          minlimt.css('border', '1px solid red'); 
                  $('#mini_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  minlimt.focus();
                  return false;
                      }else{      
                          minlimt.css('border', ''); 
                  $('#mini_deal_error_msg').html('');         
                }
                    });
               
               /*Maximum Limit*/
                  $('#maxlimit').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
                          maxlimit.css('border', '1px solid red'); 
                  $('#max_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  maxlimit.focus();
                  return false;
                      }else{      
                          maxlimit.css('border', ''); 
                  $('#max_deal_error_msg').html('');          
                }
                    });
               
                   
                
               <?php /*?> $('#purchaselimit').keypress(function (e){
                      if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57))
                {
                          purchaselimit.css('border', '1px solid red'); 
                  $('#error_msg').html('Numbers Only Allowed');
                  purchaselimit.focus();
                  return false;
                      }
                else
                {     
                          purchaselimit.css('border', ''); 
                  $('#error_msg').html('');         
                }
                      });<?php */?>
                      
                
               $('#submit_deal').click(function() {
               
               if(shop.val() == 0){
                  shop.css('border', '1px solid red'); 
                  $('#store_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SHOP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SHOP'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SHOP')); ?>');
                  shop.focus();
                  return false;
                }else{
                        shop.css('border', ''); 
                        $('#store_error_msg').html('');
                      }
               
               /*Deal Title Already exist */  
                   //var passData = 'title='+title.val();
                   var mer_id   = $('#merchant').val();
                  var store_id = $('#shop').val();
                  var title    = $('#title').val();
                  var pass = "mer_id="+mer_id+"&store_id="+store_id+"&title="+title;
                   $.ajax( {
                        type: 'get',
                      data: pass,
                      url: '<?php echo url('check_title_exist'); ?>',
                      success: function(responseText){  
                      if(responseText == 1){ 
                      title.css('border', '1px solid red'); 
                      $('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_TITLE')); ?>');
                      title.focus();
                      return false;
                      }            
                    }   
                  });   
               
               /*Deal Title*/ 
                if($.trim(title.val()) == ""){
                  title.css('border', '1px solid red'); 
                  $('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_TITLE')); ?>');
                  title.focus();
                  return false;
                }else{
                   title.css('border', ''); 
                         $('#title_error_msg').html('');
                }
               /*Deal Title in french
                if($.trim(title_fr.val()) == ""){
                  title_fr.css('border', '1px solid red'); 
                  $('#title_fr_error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_TITLE');} ?> in French');
                  title_fr.focus();
                  return false;
                }else{
                   title_fr.css('border', ''); 
                         $('#title_fr_error_msg').html('');
                } */    
               
               /*Top Category*/ 
                if(category.val() == 0){
                  category.css('border', '1px solid red'); 
                  $('#category_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY')); ?>');
                  category.focus();
                  return false;
                }else{
                        category.css('border', ''); 
                        $('#category_error_msg').html('');
                      }
               
               /*Main Category*/  
                if(maincategory.val() == 0){
                  maincategory.css('border', '1px solid red'); 
                  $('#main_cat_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_MAIN_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_MAIN_CATEGORY')); ?>');
                  maincategory.focus();
                  return false;
                }else{
                        maincategory.css('border', ''); 
                        $('#main_cat_error_msg').html('');
                      }
               
                /*
                {if(subcategory.val() == 0)
                
                  subcategory.css('border', '1px solid red'); 
                  $('#error_msg').html('Please Select Sub Category');
                  subcategory.focus();
                  return false;
                }
                      else
                      {
                      subcategory.css('border', ''); 
                      $('#error_msg').html('');
                      }
                      if(secondsubcategory.val() == 0)
                {
                  secondsubcategory.css('border', '1px solid red'); 
                  $('#error_msg').html('Please Enter Select Sub Category');
                  secondsubcategory.focus();
                  return false;
                }
                      else
                      {
                      secondsubcategory.css('border', ''); 
                      $('#error_msg').html('');
               }*/
               
                /*Original Price*/    
                if(originalprice.val() == 0){
                  originalprice.css('border', '1px solid red'); 
                  $('#org_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_ORIGINAL_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_ORIGINAL_PRICE')); ?>');
                  originalprice.focus();
                  return false;
                }else if(isNaN(originalprice.val()) == true){
                  originalprice.css('border', '1px solid red'); 
                  $('#org_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  originalprice.focus();
                  return false;
                }else{
                        originalprice.css('border', ''); 
                        $('#org_price_error_msg').html('');
                      }
               /*Discount Price*/   
                if(discountprice.val() == 0){
                  discountprice.css('border', '1px solid red'); 
                  $('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DISCOUNT_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DISCOUNT_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DISCOUNT_PRICE')); ?>');
                  discountprice.focus();
                  return false;
                }else if(isNaN(discountprice.val()) == true){
                  discountprice.css('border', '1px solid red'); 
                  $('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  discountprice.focus();
                  return false;
                }else if(parseInt(discountprice.val()) > parseInt(originalprice.val()) ){
                  discountprice.css('border', '1px solid red'); 
                  $('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE_SHOLUD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE_SHOLUD'): trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT_PRICE_SHOLUD')); ?>');
                  discountprice.focus();
                  return false;
                }else{
                        discountprice.css('border', ''); 
                        $('#dis_price_error_msg').html('');
                      }
               
               /*Shipping amount */
                if(Shipping_Amount.val() == 0){
                  Shipping_Amount.css('border', '1px solid red'); 
                  $('#ship_amt_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_SHIPPING_AMOUNT'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_SHIPPING_AMOUNT')); ?>');
                  Shipping_Amount.focus();
                  return false;
                }else if(isNaN(Shipping_Amount.val()) == true){
                  Shipping_Amount.css('border', '1px solid red'); 
                  $('#ship_amt_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  Shipping_Amount.focus();
                  return false;
                }else{
                        Shipping_Amount.css('border', ''); 
                        $('#ship_amt_error_msg').html('');
                      }
                
                
               /*Deal Start Date*/  
                if(startdate.val() == ''){
                  startdate.css('border', '1px solid red'); 
                  $('#start_date_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_DEAL_START_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_DEAL_START_DATE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_DEAL_START_DATE')); ?>');
                  startdate.focus();
                  return false;
                }else{
                        startdate.css('border', ''); 
                        $('#start_date_error_msg').html('');
                      }
               
               /*Deal End Date*/    
                if(enddate.val() == ''){
                  enddate.css('border', '1px solid red'); 
                  $('#end_date_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_DEAL_END_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_DEAL_END_DATE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_DEAL_END_DATE')); ?>');
                  enddate.focus();
                  return false;
                }else  if ((Date.parse(startdate.val()) >= Date.parse(enddate.val()))) {
               
                  enddate.css('border', '1px solid red'); 
                  $('#end_date_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_END_DATE_SHOLUD_BE_GREATER_THAN_START_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_END_DATE_SHOLUD_BE_GREATER_THAN_START_DATE') : trans($MER_OUR_LANGUAGE.'.MER_END_DATE_SHOLUD_BE_GREATER_THAN_START_DATE')); ?>');
                  enddate.focus();
                  return false;
                }else{
                        enddate.css('border', ''); 
                        $('#end_date_error_msg').html('');
                      }
               
               /*Deal Expiry Date*/ 
                if(expirydate.val() == ''){
                  expirydate.css('border', '1px solid red'); 
                  $('#expiry_date_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_EXPIRY_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_EXPIRY_DATE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_EXPIRY_DATE')); ?>');
                  expirydate.focus();
                  return false;
                }else if(expirydate.val() < enddate.val()){
                  expirydate.css('border', '1px solid red'); 
                  $('#expiry_date_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EXPIRY_DATE_SHOLUD_BE_GREATER_THAN_START_DATE')!= '') ? trans(Session::get('mer_lang_file').'.MER_EXPIRY_DATE_SHOLUD_BE_GREATER_THAN_START_DATE') : trans($MER_OUR_LANGUAGE.'.MER_EXPIRY_DATE_SHOLUD_BE_GREATER_THAN_START_DATE')); ?>');
                  expirydate.focus();
                  return false;
                }else{
                        expirydate.css('border', ''); 
                        $('#expiry_date_error_msg').html('');
                      }
                
               /*Description*/
                if($.trim(wysihtml5.val()) == ''){
                  wysihtml5.css('border', '1px solid red'); 
                  $('#desc_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DESCRIPTION')); ?>');
                  wysihtml5.focus();
                  return false;
                }else{
                        wysihtml5.css('border', ''); 
                        $('#desc_error_msg').html('');
                      }
                  
               /*Store*/
                if(shop.val() == 0){
                  shop.css('border', '1px solid red'); 
                  $('#store_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SHOP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SHOP'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SHOP')); ?>');
                  shop.focus();
                  return false;
                }else{
                        shop.css('border', ''); 
                        $('#store_error_msg').html('');
                      }
               /*Deal title check*/
                if(($('#exist').val())==""){
                  $('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE')); ?>');
                  title.css('border', '1px solid red'); 
                  title.focus();
                  return false;
                }else if(($('#exist').val())==1){
                  $('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE')); ?>');
                  title.css('border', '1px solid red'); 
                  title.focus();
                  return false;
                }else{
                  title.css('border', ''); 
                  $('#title_error_msg').html('');
                }
                
               /*Meta Keywords*/  
                if($.trim(metakeyword.val()) == ""){
                  metakeyword.css('border', '1px solid red'); 
                  $('#meta_key_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METAKEYWORD')!= '') ? trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METAKEYWORD'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_METAKEYWORD')); ?>');
                  metakeyword.focus();
                  return false;
                }else{
                        metakeyword.css('border', ''); 
                        $('#meta_key_error_msg').html('');
                      }
               
               /*Meta Description*/
                if($.trim(metadescription.val()) == ""){
                  metadescription.css('border', '1px solid red'); 
                  $('#meta_desc_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METADESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METADESCRIPTION'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_METADESCRIPTION')); ?>');
                  metadescription.focus();
                  return false;
                }else{
                        metadescription.css('border', ''); 
                        $('#meta_desc_error_msg').html('');
                      }
               
               /*Minimum Deal Limit*/ 
                if(minlimt.val() == 0){
                  minlimt.css('border', '1px solid red'); 
                  $('#mini_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_MINIMUM_LIMIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_MINIMUM_LIMIT'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_MINIMUM_LIMIT')); ?>');
                  minlimt.focus();
                  return false;
                }else if(isNaN(minlimt.val()) == true){
                  minlimt.css('border', '1px solid red'); 
                  $('#mini_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  minlimt.focus();
                  return false;
                }else{
                        minlimt.css('border', ''); 
                        $('#mini_deal_error_msg').html('');
                      }
                 /*Maximum Deal Limit*/
                if(maxlimit.val() == 0){
                  maxlimit.css('border', '1px solid red'); 
                  $('#max_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_MAXIMUM_LIMIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_MAXIMUM_LIMIT'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_MAXIMUM_LIMIT')); ?>');
                  maxlimit.focus();
                  return false;
                }else if(isNaN(maxlimit.val()) == true){
                  maxlimit.css('border', '1px solid red'); 
                  $('#max_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
                  maxlimit.focus();
                  return false;
                }else if(parseInt(maxlimit.val()) <= parseInt(minlimt.val())){
                  maxlimit.css('border', '1px solid red'); 
                  $('#max_deal_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAXIMUM_VALUE_SHOULD_GREATER_THAN_MINIMUM_VALUE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAXIMUM_VALUE_SHOULD_GREATER_THAN_MINIMUM_VALUE') : trans($MER_OUR_LANGUAGE.'.MER_MAXIMUM_VALUE_SHOULD_GREATER_THAN_MINIMUM_VALUE')); ?>');
                  maxlimit.focus();
                  return false;
                }else{
                        maxlimit.css('border', ''); 
                        $('#max_deal_error_msg').html('');
                      }
                
               <?php /* if(purchaselimit.val() == 0)
                  {
                    purchaselimit.css('border', '1px solid red'); 
                    $('#error_msg').html('Please Enter Purchase Limit');
                    purchaselimit.focus();
                    return false;
                  }
                  else if(isNaN(purchaselimit.val()) == true)
                  {
                    purchaselimit.css('border', '1px solid red'); 
                    $('#error_msg').html('Numbers Only Allowed');
                    purchaselimit.focus();
                    return false;
                  }
                        else
                        {
                        purchaselimit.css('border', ''); 
                        $('#error_msg').html('');
                  }*/?>
               
               /*Deal Image*/
                  var ext = $('#file').val().split('.').pop().toLowerCase();
                  var allow = new Array('png','jpg','jpeg','png');
                  var valid = false;
                  for(var x = 0; x < allow.length; x++) {
                        if(allow[x] == ext) {
                            valid = true;
                            break;
                        }
                  }
               
                var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                      if(file.val() == ""){
                    file.focus();
                  file.css('border', '1px solid red'); 
                  $('#img_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_CHOOSE_IMAGE')); ?>');
                  return false;
                }else if($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {         
                  file.focus();
                  file.css('border', '1px solid red'); 
                  $('#img_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_CHOOSE_VALID_IMAGE')); ?>');
                  return false;
                }else{
                  file.css('border', ''); 
                  $('#img_error_msg').html('');       
                }       
                    
               });
               
               /*
               var passData = 'id='+<?php echo Session::get('merchantid');?>;
                   $.ajax( {
                        type: 'get',
                      data: passData,
                      url: '<?php echo url('auction_select_store_name'); ?>',
                      success: function(responseText){  
                       if(responseText)
                       { 
                      // alert(responseText)
                      $('#shop').html(responseText);             
                       }
                    }   
                  });   
               */
               
                
                  });
            </script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
            <!-- END GLOBAL SCRIPTS -->
            <script src="<?php echo e(url('')); ?>/public/assets/js/bootstrap-datetimepicker.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/js/moment-with-locales.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/js/bootstrap-datetimepicker.js"></script>
            <!-- New Date picker-->
            <script type="text/javascript">
               $(function () {
                             $('#dtpickerdemo').datetimepicker({
                         minDate:new Date()
                     });
                         });
                 
                  $(function () {
                             $('#dtpickerdemo1').datetimepicker({
                         minDate:new Date()
                     });
                         });
               
               
                
                   
                 
            </script>
            <!-- PAGE LEVEL SCRIPTS -->
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Converter.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/Markdown.Editor-hack.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/js/editorInit.js"></script>
            <script>
               //$(function () { formWysiwyg(); });
               $(document).ready(function () {
               $('.wysihtml5').wysihtml5();
               
               $('.wysihtml5').wysihtml5({
               "events": { "focus": function() { 
               
               $('#datetimepicker1').hide(); 
               }, 
               "paste": function()}
               });
               });
            </script>
            <script type="text/javascript">
               function addFormField() {
                 var id = document.getElementById("aid").value;
               var count_id = document.getElementById("count").value;   
               if(count_id < 4){
               document.getElementById('count').value = parseInt(count_id)+1;
                 jQuery.noConflict()
                 jQuery("#divTxt").append("<tr style='height:5px;' > <td> </td> </tr><tr id='row" + id + "' style='width:100%'><td width='20%'><input type='file' name='file_more"+count_id+"' id='file"+count_id+"' onchange='file_up(this.id)' /></td><td>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + id + "\"); return false;' style='' >Remove</a></td></tr>");     
                    jQuery('#row' + id).highlightFade({    speed:1000 });
                id = (id - 1) + 2;
                document.getElementById("aid").value = id;   
               }  
               }
                   
                 function removeFormField(id) {
               var count_id = document.getElementById("count").value;
               document.getElementById('count').value = parseInt(count_id)-1;
                   jQuery(id).remove();
               }
            </script>
            <script type="text/javascript">
               $.ajaxSetup({
               headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
               });
            </script>
            <script>
               $('#replace_days').keydown(function (e) 
               {
                 if (e.shiftKey || e.ctrlKey || e.altKey)
                 {
                  e.preventDefault();
                 } 
                 else 
                 {
                  var key = e.keyCode;
                  if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
                   {
                    e.preventDefault();
                   }
                }
               });
               
               $('#return_days').keydown(function (e) 
               {
                 if (e.shiftKey || e.ctrlKey || e.altKey)
                 {
                  e.preventDefault();
                 } 
                 else 
                 {
                  var key = e.keyCode;
                  if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
                   {
                    e.preventDefault();
                   }
                }
               });
               
               $('#cancellation_days').keydown(function (e) 
               {
                 if (e.shiftKey || e.ctrlKey || e.altKey)
                 {
                  e.preventDefault();
                 } 
                 else 
                 {
                  var key = e.keyCode;
                  if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
                   {
                    e.preventDefault();
                   }
                }
               });
               
               
            </script>
         </body>
         <!-- END BODY -->
      </html>