<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


<!-- Header End====================================================================== -->



<head>

<?php

	

	if($get_meta_details){

	 foreach($get_meta_details as $meta_details) { } 

	 

	$mtitle= $meta_details->gs_metatitle;

	 $mdetails=$meta_details->gs_metadesc;

	 $mkey=$meta_details->gs_metakeywords;

	}

	else

	{

		$mtitle="";

	 $mdetails="";

	 $mkey="";

		

	}

	 ?>

     <title><?php echo $mtitle; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="<?php echo $mdetails; ?>">

    <meta name="keywords" content="<?php echo $mkey;  ?>">

<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

<!-- Basic page needs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('')); ?>"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a><span>&raquo;</span></li>

            <li><strong><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_RESULT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_RESULT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_RESULT')); ?> <?php endif; ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <div class="category-description std">

            <div class="slider-items-products">

             

            </div>

          </div>

		  

		  

          <div class="shop-inner paymnt-payu">

		 <?php if(Session::has('success')): ?>

		 <div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><?php echo Session::get('success'); ?></div>

		 <?php endif; ?>

            <div class="page-title"> 

			  <div class="about-page"> 


                  <h2><span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED')); ?> <?php endif; ?> .. <?php if(Lang::has(Session::get('lang_file').'.PLEASE_NOTE_THE_TRANSACTION_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_NOTE_THE_TRANSACTION_DETAILS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_NOTE_THE_TRANSACTION_DETAILS')); ?> <?php endif; ?> </span></h2>

		      

			

			   <?php if(Session::has('fail')): ?>

                  <h2><span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_FAILED')); ?> <?php endif; ?></span></h2>

		       <?php endif; ?>

			   

			    <?php if(Session::has('error')): ?>

                  <h2><span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')); ?> <?php endif; ?></span></h2>

		       <?php endif; ?>

			   

             </div>

		   </div>

		   

            <div class="product-grid-area">

			

			<h5><?php if(Lang::has(Session::get('lang_file').'.TRANSACTION_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TRANSACTION_DETAILS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TRANSACTION_DETAILS')); ?> <?php endif; ?></h5>

			<h6><?php if(Lang::has(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.THANK_YOU_FOR_SHOPPING_WITH')); ?> <?php endif; ?> <?php echo e($SITENAME); ?>.</h6>

				
      <div class="table-responsive">
			<table class="table table-bordered">

              <thead>

                 <tr>

                <th><?php if(Lang::has(Session::get('lang_file').'.PAYER_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYER_NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYER_NAME')); ?> <?php endif; ?></th>

                <th><?php if(Lang::has(Session::get('lang_file').'.TRANSACTIONID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TRANSACTIONID')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TRANSACTIONID')); ?> <?php endif; ?></th>

                <th><?php if(Lang::has(Session::get('lang_file').'.TOKENID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOKENID')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TOKENID')); ?> <?php endif; ?></th>

                <th><?php if(Lang::has(Session::get('lang_file').'.PAYER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYER_EMAIL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYER_EMAIL')); ?> <?php endif; ?></th>

                <th><?php if(Lang::has(Session::get('lang_file').'.PAYER_ID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYER_ID')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYER_ID')); ?> <?php endif; ?></th>

                <th><?php if(Lang::has(Session::get('lang_file').'.ACKNOWLEDGEMENT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ACKNOWLEDGEMENT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ACKNOWLEDGEMENT')); ?> <?php endif; ?></th>

                <th><?php if(Lang::has(Session::get('lang_file').'.PAYERSTATUS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYERSTATUS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYERSTATUS')); ?> <?php endif; ?></th>

				</tr>

              </thead>

              <tbody>

           

			 <?php  $coupon = 0;

                    $shipping_amt = 0;

                    $get_subtotal  =0;

                    $get_tax = 0;

			  if($orderdetails){ 

				foreach($orderdetails as $orderdet) {

				   $coupon+= $orderdet->coupon_amount;

                   $shipping_amt+= $orderdet->order_shipping_amt;    //shipping amount getting from order table

                   $get_subtotal+= $orderdet->order_amt;

                   $get_tax+=$orderdet->order_tax;

           }}  ?>

             <tr>

                  <td><?php echo $orderdet->payer_name;?></td>

                  <td><?php echo $orderdet->transaction_id;?> </td>

                  <td><?php echo $orderdet->token_id;?></td>

                  <td><?php echo $orderdet->payer_email;?></td>

                  <td><?php echo $orderdet->payer_id;?></td>

                  <td><?php echo $orderdet->payment_ack; ?></td>

                  <td><?php echo $orderdet->payer_status; ?></td>

             </tr>

		

				</tbody>

            </table></div>

	    	<h5> <?php if (Lang::has(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION');} ?> </h5>

			
        <div class="table-responsive">
			 <table class="table table-bordered">

              <thead>

                <tr>

                <th><?php if (Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_NAME')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_DEAL_NAME');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_DEAL_NAME');} ?></th>

                  <th><?php if (Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_DEAL_QUANTITY');} ?></th>

                  <th><?php if (Lang::has(Session::get('lang_file').'.AMOUNT')!= '') { echo  trans(Session::get('lang_file').'.AMOUNT');}  else { echo trans($OUR_LANGUAGE.'.AMOUNT');} ?></th>

                

				</tr>

              </thead>

              <tbody>



				<?php   

                $taxamount =0;

                $wallet = 0;

                $trans_id='';

                if($orderdetails){ 

					foreach($orderdetails as $orderdet) 

					{

						$trans_id = $orderdet->transaction_id;

					

						/*if($orderdet->order_type == 1 || $orderdet->order_type == 2){

						

						$taxamount+=($orderdet->order_amt*$orderdet->order_tax)/100;

						$taxamount_au = ($orderdet->order_amt*$orderdet->order_tax)/100;

					}else{

						

						$taxamount_au=0;

					} */

			$getcolor = DB::table('nm_color')->select('co_name')->where('co_id','=',$orderdet->order_pro_color)->first();
			$getsize = DB::table('nm_size')->select('si_name')->where('si_id','=',$orderdet->order_pro_size)->first();



            $taxamount    += $orderdet->order_taxAmt;

            $taxamount_au = $orderdet->order_taxAmt;



          ?>

				

				 <tr>

               	  <td><?php 

				  if($orderdet->order_type == 1){ 

					if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

						$pro_title = 'pro_title';

					}else {  $pro_title = 'pro_title_'.Session::get('lang_code'); }

					echo $orderdet->$pro_title; ?>
					
					<?php if( $orderdet->order_pro_color!='' && $orderdet->order_pro_color!=0): ?>
					<br><b><?php if (Lang::has(Session::get('lang_file').'.COLOR')!= '') { echo  trans(Session::get('lang_file').'.COLOR');}  else { echo trans($OUR_LANGUAGE.'.COLOR');}?> : </b><?php echo e($getcolor->co_name); ?>

					<?php endif; ?>
					
					<?php if( $orderdet->order_pro_size!='' && $orderdet->order_pro_size!=0): ?>
					<br><b><?php if (Lang::has(Session::get('lang_file').'.SIZE')!= '') { echo  trans(Session::get('lang_file').'.SIZE');}  else { echo trans($OUR_LANGUAGE.'.SIZE');}?> : </b><?php echo e($getsize->si_name); ?>

					<?php endif; ?>
					

				   <?php  } else { 

					if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

						$deal_title = 'deal_title';

					}else {  $deal_title = 'deal_title_'.Session::get('lang_code'); }

					echo $orderdet->$deal_title;}?>
					
					
					</td>

                  <td><?php echo $orderdet->order_qty;?> </td>

                  <td><?php echo e(Helper::cur_sym()); ?> <?php echo $orderdet->order_amt + $taxamount_au;?><?php  $orderdet->order_amt + $taxamount;?> (<?php if (Lang::has(Session::get('lang_file').'.INCLUDING')!= '') { echo  trans(Session::get('lang_file').'.INCLUDING');}  else { echo trans($OUR_LANGUAGE.'.INCLUDING');} ?>

                   <?php  $orderdet->order_amt;?> <?php echo $orderdet->order_tax;?> % 

                   <?php  $orderdet->order_amt + $taxamount;?> 

                   (<?php if (Lang::has(Session::get('lang_file').'.TAXES')!= '') { echo  trans(Session::get('lang_file').'.TAXES');}  else { echo trans($OUR_LANGUAGE.'.TAXES');} ?>))</td>

      			 </tr>

				<?php }

				} ?>

                <?php if($coupon!=0){?>

                    <tr>

                        <td>&nbsp;</td> 

                        <td><?php echo e((Lang::has(Session::get('lang_file').'.COUPON_VALUE')!= '') ? trans(Session::get('lang_file').'.COUPON_VALUE') : trans($OUR_LANGUAGE.'.COUPON_VALUE')); ?> </td>

                        <td> - <?php echo e(Helper::cur_sym()); ?> <?php echo $coupon; ?></td>

                    </tr>    

                <?php } ?> 



                <tr>                  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.SUB-TOTAL')!= '') { echo  trans(Session::get('lang_file').'.SUB-TOTAL');}  else { echo trans($OUR_LANGUAGE.'.SUB-TOTAL');} ?></td>                  

                        <td style="font-weight:bold;"> <?php $subtotal = ($get_subtotal+$taxamount)-$coupon;  ?> <?php echo e(Helper::cur_sym()); ?> <?php echo e(round($subtotal,2)); ?> </td>             

                    </tr>

                <tr>                  

                    <td>&nbsp;</td>                  

                    <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.SHIPPING_TOTAL')!= '') { echo  trans(Session::get('lang_file').'.SHIPPING_TOTAL');}  else { echo trans($OUR_LANGUAGE.'.SHIPPING_TOTAL');} ?></td>                  

                    <td style="font-weight:bold;"><?php echo e(Helper::cur_sym()); ?> <?php  echo $shipping_amt; ?></td>             

                </tr>      



                <?php 

                    $trans_wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$trans_id)->value('wallet_used');

                    $wallet = $trans_wallet;

                    if(count($wallet)!=0){

                        $get_subtotal = $get_subtotal - $wallet;

                ?>

                    <tr>

                        <td>&nbsp;</td> 

                        <td><?php echo e((Lang::has(Session::get('lang_file').'.WALLET_USED')!= '') ? trans(Session::get('lang_file').'.WALLET_USED') : trans($OUR_LANGUAGE.'.WALLET_USED')); ?> </td>

                        <td> - <?php echo e(Helper::cur_sym()); ?> <?php echo $wallet; ?></td>

                    </tr>

                    <?php }else $wallet = 0; ?>          	 

                    

                    <?php /*

                    <tr>               	  

                        <td>&nbsp;</td> 

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.TAX')!= '') { echo  trans(Session::get('lang_file').'.TAX');}  else { echo trans($OUR_LANGUAGE.'.TAX');} ?></td>                  

                        <td style="font-weight:bold;"> <?php  echo $get_tax;?> %</td>      			

                    </tr>*/?>

                    

                    

                    <tr>               	  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.TOTAL')!= '') { echo  trans(Session::get('lang_file').'.TOTAL');}  else { echo trans($OUR_LANGUAGE.'.TOTAL');} ?></td>                  

                        <td style="font-weight:bold;"><?php echo e(Helper::cur_sym()); ?> <?php $total = ($subtotal + $shipping_amt) -$wallet;  echo round($total,2);//number_format((float)$total, 2, '.', '');  ?></td>      			

                    </tr>

                	</tbody>

            </table>
          </div>
			

			

				 

				 <div class="special-product">

				 <h4><a class="link-all pull-right me_btn res-cont1" href="<?php echo e(url('index')); ?>"><?php if(Lang::has(Session::get('lang_file').'.CONTINUE_SHOPPING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONTINUE_SHOPPING')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONTINUE_SHOPPING')); ?> <?php endif; ?></a></h4>

				 </div>

				 

				 

				 <div class="clearfix"></div>

				

            </div>

           

          </div>

		  

		  

        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

          

          <div class="block shop-by-side">

           

            <div class="block-content">

              <p class="block-subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES')); ?></p>

              <div class="layered-Category">

                <div class="layered-content">

		

				<div id="divMenu">

					<ul>

					   <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                       <?php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>						

						<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>">

							 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

							  <?php $mc_name = 'mc_name'; ?>

							  <?php else: ?> <?php  $mc_name = 'mc_name_code'; ?> <?php endif; ?>

							  <?php echo e($fetch_main_cat->$mc_name); ?></a> 

							  <?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0): ?>  

						   <ul>

							 <?php $__currentLoopData = $sub_main_category[$fetch_main_cat->mc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                               <?php  $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>

                               <?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0): ?> 

								   

								<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"> 

								 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

									<?php	$smc_name = 'smc_name'; ?>

									<?php else: ?> <?php  $smc_name = 'smc_name_code'; ?> <?php endif; ?>

									<?php echo e($fetch_sub_main_cat->$smc_name); ?> </a>

										

								

									 <ul>

									   <?php $__currentLoopData = $second_main_category[$fetch_sub_main_cat->smc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                                       <?php  $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; ?>

                                        <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0): ?>

										<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"> 

										<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

										  <?php $sb_name = 'sb_name'; ?>

										  <?php else: ?> <?php  $sb_name = 'sb_name_langCode'; ?> <?php endif; ?>

										  <?php echo e($fetch_sub_cat->$sb_name); ?></a> 

											<ul>	

												<?php $__currentLoopData = $second_sub_main_category[$fetch_sub_cat->sb_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_secsub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                                                <?php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?> 

												<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)); ?>">

												<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

												<?php $ssb_name = 'ssb_name'; ?>

												<?php else: ?> <?php  $ssb_name = 'ssb_name_langCode'; ?> <?php endif; ?>

												<?php echo e($fetch_secsub_cat->$ssb_name); ?></a>

												</li>

												 

													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

											</ul>

											 <?php endif; ?>

										</li>

										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									</ul>

									 <?php endif; ?>

								</li>

								 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						   </ul>

						   <?php endif; ?>

						</li>

						 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					</ul>

					

				</div>

                </div>

              </div>

             

            </div>

          </div>

		  

		   <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3><?php if(Lang::has(Session::get('lang_file').'.MOST_VISITED_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MOST_VISITED_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MOST_VISITED_PRODUCTS')); ?> <?php endif; ?></h3>

            </div>

            <div class="block-content">

              <ul>

			 <?php foreach($most_visited_product as $fetch_most_visit_pro) {

			 $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

			 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

			 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);

			 ?>

			  

                <li class="item">

                  <div class="products-block-left"> 

				  <?php  $product_image     = $mostproduct_img[0];

                /* Image Path */

                $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                $img_data   = "public/assets/product/".$product_image;

                if($mostproduct_img !=''){

                if(file_exists($img_data) && $product_image !='')   //product image is not null and exists in folder

					{

                

                $prod_path = url('').'/public/assets/product/' .$product_image;                  

                }else{  

                    if(isset($DynamicNoImage['productImg'])){

                        $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg'];

                        if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))    //no image for product is not null and exists in folder

                            $prod_path = url('').'/'.$dyanamicNoImg_path;

                    }

                } 

				}

				else{  

                    if(isset($DynamicNoImage['productImg'])){

                        $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg'];

                        if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path))    //no image for product is not null and exists in folder

                            $prod_path = url('').'/'.$dyanamicNoImg_path;

                    }

                } ?>

			     <a title="Sample Product" class="product-image"><img  src="<?php echo e($prod_path); ?>" alt="<?php echo e($fetch_most_visit_pro->pro_title); ?>"></a>

				  

				  </div>

                  <div class="products-block-right">

                    <p class="product-name"> <a href="#product"><?php echo e(substr($fetch_most_visit_pro->pro_title,0,50)); ?>...</a> </p>

                    <span class="price"><?php echo e(Helper::cur_sym()); ?><?php echo e($fetch_most_visit_pro->pro_disprice); ?></span>

                   

				    <?php           

                $one_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 1)->count();

                $two_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 2)->count();

                $three_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 3)->count();

                $four_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 4)->count();

                $five_count = DB::table('nm_review')->where('product_id', '=', $fetch_most_visit_pro->pro_id)->where('ratings', '=', 5)->count();

                

                

                $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

                $multiple_countone = $one_count *1;

                $multiple_counttwo = $two_count *2;

                $multiple_countthree = $three_count *3;

                $multiple_countfour = $four_count *4;

                $multiple_countfive = $five_count *5;

                $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>



                         <div class="rating">

             <?php if($product_count): ?>

             <?php   $product_divide_count = $product_total_count / $product_count; ?>

             <?php if($product_divide_count <= '1'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '1'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '2'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

             <?php elseif($product_divide_count >= '3'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '4'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

             <?php elseif($product_divide_count >= '5'): ?> 

             

             <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

             <?php else: ?>

              

            <?php endif; ?>

          <?php else: ?>

             <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

          <?php endif; ?>  

           </div>

					

				<?php if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty) { ?>

					<a class="link-all"><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></a>

				<?php } else { ?>	

				<?php $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

					 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

					 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

					 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

					 $res = base64_encode($fetch_most_visit_pro->pro_id);

			   ?>	

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

					 <a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

					 <a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

					<?php endif; ?>

					

					 <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

					 <a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

				    <?php endif; ?>

				   

				   <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>  

					 <a class="link-all" href="<?php echo e(url('productview/'.$mcat.'/'.$res)); ?>"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

				   <?php endif; ?>

					

				<?php } ?>

				

				

                  </div>

                </li>

			 <?php } ?>

              </ul>

		   </div>

          </div>

         

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <?php echo $footer; ?>


</html>

