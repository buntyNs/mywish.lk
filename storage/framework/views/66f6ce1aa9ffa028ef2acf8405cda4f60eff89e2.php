

<?php $__currentLoopData = $get_contact_det; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $enquiry_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-3 col-xs-12">
		   <div class="footer-logo"> <a class="brand" href="<?php echo e(url('index')); ?>"><img src="<?php echo e($SITE_LOGO); ?>" alt="<?php if(Lang::has(Session::get('lang_file').'.LOGO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOGO')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOGO')); ?> <?php endif; ?>"/></a> </div>
          <p>
            <?php $lang_code = Session::get('lang_code');
            $get = 'gs_sitedescription_'.Session::get('lang_code');
            $get_desc = DB::table('nm_generalsetting')
            ->first(); ?>
            <?php if(Session::get('lang_code')=='' || Session::get('lang_code')=='en'): ?>
                  <?php echo e($get_desc->gs_sitedescription); ?>

            <?php else: ?>
                 <?php echo e($get_desc->$get); ?>

            <?php endif; ?>

          <!-- <?php if($SITEDESCRIPTION): ?> <?php echo e($SITEDESCRIPTION); ?>  <?php endif; ?> --></p>
		  
		  <?php if($get_social_media_url): ?>  <?php $__currentLoopData = $get_social_media_url; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $social_sttings_url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <div class="social">
            <ul class="inline-mode">
			  <?php if($social_sttings_url->sm_fb_page_url !=''): ?> 
              <li class="social-network fb"><a title="Connect us on Facebook" href="<?php echo e($social_sttings_url->sm_fb_page_url); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
              <!--<li class="social-network googleplus"><a title="Connect us on Google+" href="#"><i class="fa fa-google"></i></a></li>-->
			  <?php if($social_sttings_url->sm_twitter_url != ''): ?>  
              <li class="social-network tw"><a title="Connect us on Twitter" href="<?php echo e($social_sttings_url->sm_twitter_url); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
			  <?php if($social_sttings_url->sm_linkedin_url != ''): ?> 
              <li class="social-network linkedin"><a title="Connect us on linkedin" href="<?php echo e($social_sttings_url->sm_linkedin_url); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
			  <?php if($social_sttings_url->sm_youtube_url != ''): ?> 
              <li class="social-network googleplus"><a title="Connect us on Youtube" href="<?php echo e($social_sttings_url->sm_youtube_url); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li><?php endif; ?>
            </ul>
          </div>
		  <?php endif; ?>
      <br>
     
        <?php $__currentLoopData = $Active_Language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
        <?php $lang = $lang->lang_code; ?>  
         
      <?php if(count($Active_Language) > 1 && $lang !='en'): ?>
      <div class="language " id="language">
        <select class="form-control" name="lang_select" id="lang_select" onchange="set_lang_code(this)">       
          <option value="">   -- <?php echo e((Lang::has(Session::get('lang_file').'.SELECT_LANGUAGE')!= '') ?  trans(Session::get('lang_file').'.SELECT_LANGUAGE'): trans($OUR_LANGUAGE.'.SELECT_LANGUAGE')); ?> --
          </option>

          <?php $__currentLoopData = $Active_Language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>       
          <option value="<?php echo e($lang->lang_code); ?>" 
            <?php if(Session::get('lang_code') == $lang->lang_code) 
            { echo 'selected="selected"'; }else{    } ?> >
            <?php echo e($lang->lang_name); ?></option> 
           
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

        </select>        
      </div>
      <?php else: ?>

      <?php endif; ?>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
		  	
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 collapsed-block footer-2nd-sec">
          <div class="footer-links">
            <h5 class="links-title"><?php echo e((Lang::has(Session::get('lang_file').'.CONTACT')!= '') ?  trans(Session::get('lang_file').'.CONTACT'): trans($OUR_LANGUAGE.'.CONTACT')); ?><a class="expander visible-xs" href="#TabBlock-1">+</a></h5>
            <div class="tabBlock" id="TabBlock-1">
              <ul class="list-links list-unstyled">
                <li><a href="mailto:<?php echo e($enquiry_det->es_contactemail); ?>"><i class="icon-envelope"></i>&nbsp;&nbsp;<?php echo e($enquiry_det->es_contactemail); ?></a> </li>
				<?php if($enquiry_det->es_skype_email_id!=''): ?> 
                <li><a href="skype:<?php echo e($enquiry_det->es_skype_email_id); ?>"><img width="14" height="14" src="<?php echo e(url('')); ?>/themes/images/skype.png" title="skype" alt="skype"/> <?php echo e($enquiry_det->es_skype_email_id); ?></a> </li>
				<?php endif; ?>
                <li><i class="icon-phone"></i><?php echo e($enquiry_det->es_phone1); ?> </li>
                <li><i class="icon-phone"></i><?php echo e($enquiry_det->es_phone2); ?></li>
                <li> <a href="<?php echo e(url('contactus')); ?>"><i class="icon-map-marker"></i><?php echo e((Lang::has(Session::get('lang_file').'.CONTACT_US')!= '') ?  trans(Session::get('lang_file').'.CONTACT_US'): trans($OUR_LANGUAGE.'.CONTACT_US')); ?></a> </li>
                <?php $__currentLoopData = $getanl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php
					echo $getal->sm_analytics_code;
				?>			
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 collapsed-block">
          <div class="footer-links">
            <h5 class="links-title"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_COMPANY')!= '') ?  trans(Session::get('lang_file').'.ABOUT_COMPANY'): trans($OUR_LANGUAGE.'.ABOUT_COMPANY')); ?><a class="expander visible-xs" href="#TabBlock-3">+</a></h5>
            <div class="tabBlock" id="TabBlock-3">
              <ul class="list-links list-unstyled">
                <li><a href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '')  ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a></li>
                <li><a href="<?php echo e(url('blog')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.BLOG')!= '') ?  trans(Session::get('lang_file').'.BLOG') : trans($OUR_LANGUAGE.'.BLOG')); ?></a></li> 
                <li><a href="<?php echo e(url('aboutus')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_US')!= '') ?  trans(Session::get('lang_file').'.ABOUT_US'): trans($OUR_LANGUAGE.'.ABOUT_US')); ?></a></li>
				
				<?php if(isset($cms_page_title)): ?> <?php $__currentLoopData = $cms_page_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($cms->cp_title !="Help"): ?> 
                <li><a href="<?php echo e(url('cms/'.$cms->cp_id)); ?>">
					<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code')) == 'en') { 
						  $cp_title = 'cp_title';
					}else {  $cp_title = 'cp_title_langCode'; }
					echo $cms->$cp_title; ?></a>
				</li>
               <?php endif; ?>
			   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			   <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
<?php /*
        <div class="col-sm-3 col-md-2 col-xs-12 collapsed-block">
          <div class="footer-links">
            <h5 class="links-title">{{ (Lang::has(Session::get('lang_file').'.MERCHANT_LOGIN')!= '') ?  trans(Session::get('lang_file').'.MERCHANT_LOGIN') : trans($OUR_LANGUAGE.'.MERCHANT_LOGIN') }}<a class="expander visible-xs" href="#TabBlock-4">+</a></h5>
            <div class="tabBlock" id="TabBlock-4">
              <ul class="list-links list-unstyled">
                <li> <a href="{{ url('merchant_signup')}}"  >{{ (Lang::has(Session::get('lang_file').'.REGISTER')!= '') ?  trans(Session::get('lang_file').'.REGISTER') : trans($OUR_LANGUAGE.'.REGISTER') }}</a>/  <a href="{{ url('sitemerchant') }}" target="_blank" > {{ (Lang::has(Session::get('lang_file').'.LOGIN')!= '') ?  trans(Session::get('lang_file').'.LOGIN') : trans($OUR_LANGUAGE.'.LOGIN') }}</a></li>
              </ul>
            </div>
          </div>
        </div>
*/?>

        <div class="col-sm-6 col-md-3 col-xs-12 collapsed-block">
          <div class="footer-links">
            <h5 class="links-title"><?php echo e((Lang::has(Session::get('lang_file').'.PAYMENT_METHOD')!= '') ?  trans(Session::get('lang_file').'.PAYMENT_METHOD'): trans($OUR_LANGUAGE.'.PAYMENT_METHOD')); ?><a class="expander visible-xs" href="#TabBlock-5">+</a></h5>
			
            <div class="tabBlock" id="TabBlock-5">
              <div class="payment">
                <ul>
                  <li><a href="#"><img title="Visa" alt="Visa" src="<?php echo e(url('')); ?>/themes/images/footer_cod.png"></a></li>
                  <li><a href="#"><img title="Paypal" alt="Paypal" src="<?php echo e(url('')); ?>/themes/images/footer_paypol.png"></a></li>
                  <li><a href="#"><img title="Discover" alt="Discover" src="<?php echo e(url('')); ?>/themes/images/footer_payumoney.png"></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-links">
            <h5 class="links-title"><?php echo e((Lang::has(Session::get('lang_file').'.MERCHANT_LOGIN')!= '') ?  trans(Session::get('lang_file').'.MERCHANT_LOGIN') : trans($OUR_LANGUAGE.'.MERCHANT_LOGIN')); ?><a class="expander visible-xs" href="#TabBlock-4">+</a></h5>
            <div class="tabBlock" id="TabBlock-4">
              <ul class="list-links list-unstyled">
                <li> <a href="<?php echo e(url('merchant_signup')); ?>"  ><?php echo e((Lang::has(Session::get('lang_file').'.REGISTER')!= '') ?  trans(Session::get('lang_file').'.REGISTER') : trans($OUR_LANGUAGE.'.REGISTER')); ?></a>/  <a href="<?php echo e(url('sitemerchant')); ?>" target="_blank" > <?php echo e((Lang::has(Session::get('lang_file').'.LOGIN')!= '') ?  trans(Session::get('lang_file').'.LOGIN') : trans($OUR_LANGUAGE.'.LOGIN')); ?></a></li>
              </ul>
            </div>
          </div>
            <?php if($PLAYSTORE_URL!="" OR $APPLE_APPSTORE_URL!=""): ?>
          <div class="footer-links">
            <h5 class="links-title"><?php echo e((Lang::has(Session::get('lang_file').'.DOWNLOAD_OUR_APP')!= '') ?  trans(Session::get('lang_file').'.DOWNLOAD_OUR_APP') : trans($OUR_LANGUAGE.'.DOWNLOAD_OUR_APP')); ?>

              <a class="expander visible-xs" href="#TabBlock-4">+</a></h5>
            <div class="tabBlock" id="TabBlock-4">
              <ul class="list-links list-unstyled">
                <?php if($PLAYSTORE_URL!=""): ?>
                 <li> <a href="<?php echo e($PLAYSTORE_URL); ?>" ><?php echo e((Lang::has(Session::get('lang_file').'.PLAYSTORE')!= '') ?  trans(Session::get('lang_file').'.PLAYSTORE') : trans($OUR_LANGUAGE.'.PLAYSTORE')); ?></a></li>
                <?php endif; ?>
                <?php if($APPLE_APPSTORE_URL!=""): ?>
                 <li> <a href="<?php echo e($APPLE_APPSTORE_URL); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.APPLE_APP_STORE')!= '') ?  trans(Session::get('lang_file').'.APPLE_APP_STORE') : trans($OUR_LANGUAGE.'.APPLE_APP_STORE')); ?></a></li>
                <?php endif; ?>
                
              </ul>
            </div>
          </div>
		    <?php endif; ?>		  	
			
        </div>
      </div>
    </div>
    <div class="footer-coppyright">
      <div class="container">
        <div class="row">
           <?php $lang_code = Session::get('lang_code');
            $get = 'gs_sitename_'.Session::get('lang_code');
            $get_sitename = DB::table('nm_generalsetting')->first(); ?>
            
         
          <div class="col-sm-6 col-xs-12 coppyright"> <?php echo e((Lang::has(Session::get('lang_file').'.COPYRIGHT')!= '') ?  trans(Session::get('lang_file').'.COPYRIGHT') : trans($OUR_LANGUAGE.'.COPYRIGHT')); ?> © <?php echo e(date("Y")); ?> 
            <?php if(Session::get('lang_code')=='' || Session::get('lang_code')=='en'): ?>
                  <?php echo e($get_sitename->gs_sitename); ?>

            <?php else: ?>
                 <?php echo e($get_sitename->$get); ?>

            <?php endif; ?>
          . <?php echo e((Lang::has(Session::get('lang_file').'.ALL_RIGHTS_RESERVED')!= '') ?  trans(Session::get('lang_file').'.ALL_RIGHTS_RESERVED') : trans($OUR_LANGUAGE.'.ALL_RIGHTS_RESERVED')); ?>. </div>
          <div class="col-sm-6 col-xs-12">
            <ul class="footer-company-links">
				<li><a href="<?php echo e(url('termsandconditons')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.TERMS_&_CONDITIONS')!= '') ?  trans(Session::get('lang_file').'.TERMS_&_CONDITIONS') : trans($OUR_LANGUAGE.'.TERMS_&_CONDITIONS')); ?></a></li>
				<li><a href="<?php echo e(url('help')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HELP')!= '')  ?  trans(Session::get('lang_file').'.HELP') :  trans($OUR_LANGUAGE.'.HELP')); ?></a></li>
				<li><a href="<?php echo e(url('faq')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.FAQ')!= '')  ?  trans(Session::get('lang_file').'.FAQ') :  trans($OUR_LANGUAGE.'.FAQ')); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer> 
  
  
  <!-- jquery js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.min.js"></script>  

<!-- bootstrap js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/bootstrap.min.js"></script> 

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/owl.carousel.js"></script>  


<!-- jquery.mobile-menu js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/mobile-menu.js"></script> 

<!--jquery-ui.min js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery-ui.js"></script> 

<!-- main js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/main.js"></script> 

<!-- flexslider js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.flexslider.js"></script> 

<!-- countdown js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/countdown.js"></script> 

<!-- Slider Js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/revolution-slider.js"></script> 
<script type="text/javascript">
  function set_lang_code(e)
  {
       // var strUser = e.options[e.selectedIndex].value;
       // alert(strUser);exit;
     // window.location.href = strUser;
     var lang_code = $('#lang_select').val();
     //alert(lang_code);
       $.ajax({
             type : "POST",
             url:'<?php echo e(url("change_language")); ?>',
             data: {lang_code : lang_code},
             success : function(response)
             {
              
              if(response != 'fail')
              {
                
                location.reload();
              }
              else
              {
                alert('No response from Server');
                return false;
              }
             }



       });
    
  }
</script>
<script type='text/javascript'>

        jQuery(document).ready(function(){
            jQuery('#rev_slider_4').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 865,
	            startheight: 450,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',
                
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
            
                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'off',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'off',
                forceFullWidth: 'on',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,
            
                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,
					

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
        </script>
<!-- extension Js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/revolution-extension.js"></script> 

<!-- bxslider js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.bxslider.js"></script> 



<!-- carousel js --> 
<!-- <script type="text/javascript" src="<?php //echo url(''); ?>/public/themes/js/owl.carousel.rtl.js"></script> --> 

<!--cloud-zoom js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/cloud-zoom.js"></script> 


    <script type="text/javascript">
	$(window).load(function(){

$('#upload_add').click(function() {

var position=$("#ad_pos option:selected").val();
var page=$("#ad_pages option:selected").val();
 
	if($('#ad_title').val() == "")
	{
		$('#ad_title').css('border', '1px solid red'); 
		$('#ad_title').focus();
		return false;
	}
	else
	{
		$('#ad_title').css('border', ''); 
	}
	if(position==0)
	{
		$('#ad_pos').css('border', '1px solid red'); 
		$('#ad_pos').focus();
		return false;
	}
	else
	{
		$('#ad_pos').css('border', ''); 
	}
	if(page == 0)
	{
		$('#ad_pages').css('border', '1px solid red'); 
		$('#ad_pages').focus();
		return false;
	}
	else
	{
		$('#ad_pages').css('border', ''); 
	}
	if($('#ad_url').val() == "")
	{
		$('#ad_url').css('border', '1px solid red'); 
		$('#ad_url').focus();
		return false;
	}
	else
	{
		var txt = $('#ad_url').val();
		var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/
		if (re.test(txt)) 
		{
			 $('#ad_url').css('border', ''); 
		}
		else {
		 $('#ad_url').css('border', '1px solid red'); 
		$('#ad_url').focus();
		return false;
		}
		
	}
	if($('#file').val()=='')
	{
		 alert('Image field required!');
                 return false;
         }
	else
	 {
		
		var title=$('#ad_title').val();
		var pass="title="+title;
 			$.ajax( {
				type: 'get',
				 data: pass,
				 url: '<?php echo url('check_title'); ?>',
				 success: function(responseText){  
				 
					if(responseText=="success")
					{
				        $('#error_name').html('Thank You ,Your request should be processed soon');
						$("#uploadform").submit();	 
					}
					else
					{
						$('#ad_title').css('border', '1px solid red'); 
						$('#ad_title').focus();
						$('#error_name').html('Ad title already exists');
					}
					}	
				});

		

		
         }


});
    $('#news_result').hide();
	$('#newsletter').click(function()
      {
       var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      var sname4 = $('#sname4');
      if(sname4.val() == "" )
      {
       $(sname4).focus();
      $(sname4).css("border","red solid 1px");
      return false;
      }
      else if($.trim(sname4.val()) == "")
      {
      $(sname4).focus();
      $(sname4).css("border","red solid 1px");
      return false;
      }
       else if(!emailReg.test(sname4.val())) 
		 { 
		         $(sname4).focus();
     			 $(sname4).css("border","red solid 1px");
      return false;
      }
       
      else
      {
	   $('#newsletter').hide();
      $(sname4).css("border","#CCCCCC solid 1px");
      var passData = 'semail='+sname4.val();
		   $.ajax( {
			      type: 'GET',
				  data: passData,
				  url: '<?php echo url('front_newsletter_submit'); ?>',
				  success: function(responseText){  		 		
				 		//alert(responseText);
						  $('#news_result').show();
				 		 setTimeout(function() {  
        				 window.location.reload();
							},3000);
				 		
					   
				  						}		
			});
			return false;
    
      }
      });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
$(".search").keyup(function(e) 
{

var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;
if(searchbox=='')
{
$("#display").html("").hide();	
}
else
{
	var passData = 'searchword='+ searchbox+'&url=<?php echo Route::getCurrentRoute()->uri(); ?>';
	//alert(passData);
	 $.ajax( {
			      type: 'GET',
				  data: passData,
				  url: '<?php echo url('autosearch'); ?>',
				  success: function(responseText){  
				  $("#display").html(responseText).show();	
}
});
}
return false;    

});
});


/*var __lc = {};
__lc.license = 4302571;

(function() {
 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();*/

</script>

<script type="text/javascript">
/*var __lc = {};
__lc.license = 6132091;

(function() {
	var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
	lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();*/
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62671250-1', 'auto');
  ga('send', 'pageview');

</script>


<script>
	//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>



<script>
	$('document').ready(function(){
     console.clear();

		$('#demo').jplist({
		
			itemsBox: '.list' 
			,itemPath: '.list-item' 
			,panelPath: '.jplist-panel'
			
			//save plugin state
			,storage: 'localstorage' //'', 'cookies', 'localstorage'			
			,storageName: 'jplist-div-layout'
		});
		$(".loading_prnt").hide();
		$(".productLoading").css("opacity","1");

	});
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>
<script type="text/javascript">
$.ajaxSetup({
	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
<script type="text/javascript">
  

  $(document).on("click",".quick-view",function(){
    $(window).trigger('resize'); 

    setTimeout(function(){
      $(window).trigger('resize'); 
    });
    $(window).scrollTop(0);
  });
  
</script>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9922300;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/9922300/">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->
