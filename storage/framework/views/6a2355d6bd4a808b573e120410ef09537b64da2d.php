<?php //print_r(session::all());?>
<?php  $current_route = Route::getCurrentRoute()->uri(); ?>

<?php if(isset($routemenu)): ?>
<?php
$menu=$routemenu;
$menu = strtolower($menu); ?> <?php endif; ?>

<?php if(Session::get('merchantid')): ?>

 <?php   $merchantid=Session::get('merchantid'); ?>

<?php endif; ?>


<div oncontextmenu="return false"></div>   
<div id="top">

            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 10px;">
               
                <!-- LOGO SECTION -->
                <header class="navbar-header">

                    <a href="<?php echo e(url('sitemerchant_dashboard')); ?>" class="navbar-brand">
                      <img src="<?php echo e($SITE_LOGO); ?>" alt="<?php echo e((Lang::has(Session::get('mer_lang_file').'.LOGO')!= '') ? trans(Session::get('mer_lang_file').'.LOGO') : trans($MER_OUR_LANGUAGE.'.LOGO')); ?>" /></a>
                </header>
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-right">

                    <!-- MESSAGES SECTION -->
                  
                    <!--END MESSAGES SECTION -->

                    <!--TASK SECTION -->
                 
                    <!--END TASK SECTION -->

                    <!--ALERTS SECTION -->
                    
                    <!-- END ALERTS SECTION -->

                    <!--ADMIN SETTINGS SECTIONS -->
					<!--<select name="Language_change" id="Language_change" onchange="Lang_change()">
        <?php /*foreach($Mer_Active_Language as $Active_Lang) {?>
            <option value="{{$Active_Lang->lang_code}}" @if($mer_selected_lang_code==$Active_Lang->lang_code)selected @endif >{{$Active_Lang->lang_name}}</option>
        <?php }*/ ?>
    </select>-->


<?php if(Session::get('merchantid')): ?>

 <?php   $merchantid=Session::get('merchantid'); ?>
<?php endif; ?>


        <strong> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HI')!= '') ?  trans(Session::get('mer_lang_file').'.HI') : trans($MER_OUR_LANGUAGE.'.HI')); ?>, <?php echo e(Session::get('merchantname')); ?></strong>
                             <li><a href="<?php echo e(url('merchant_profile')); ?>" class="btn btn-default"><i class="icon-user"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.PROFILE') : trans($MER_OUR_LANGUAGE.'.PROFILE')); ?> </a>
                            </li>
                            <li><a href="<?php echo e(url('merchant_settings')); ?>" class="btn btn-default"><i class="icon-gear"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS')); ?> </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo e(url('merchant_logout')); ?>" class="btn btn-default"><i class="icon-signout"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.LOGOUT')!= '') ? trans(Session::get('mer_lang_file').'.LOGOUT') : trans($MER_OUR_LANGUAGE.'.LOGOUT')); ?> </a>
                            </li>
                        </a>

                      

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>
			<div class="mainmenu"> 
            <ul class="">
                <li><a href="<?php echo e(url('sitemerchant_dashboard')); ?>" <?php if($menu=="dashboard"){?> class="active"<?php } ?> ><?php echo e((Lang::has(Session::get('mer_lang_file').'.DASHBOARD')!= '') ? trans(Session::get('mer_lang_file').'.DASHBOARD') : trans($MER_OUR_LANGUAGE.'.DASHBOARD')); ?></a></li>
                <li><a href="<?php echo e(url('merchant_settings')); ?>" <?php if($menu=="settings"){?> class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS')); ?></a>  </li>
                <li><a href="<?php echo e(url('mer_add_deals')); ?>" <?php if($menu=="deals"){?> class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.DEALS')!= '') ? trans(Session::get('mer_lang_file').'.DEALS') : trans($MER_OUR_LANGUAGE.'.DEALS')); ?>  </a> </li>
                <li><a href="<?php echo e(url('mer_add_product')); ?>" <?php if($menu=="products"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.PRODUCTS')!= '') ? trans(Session::get('mer_lang_file').'.PRODUCTS') : trans($MER_OUR_LANGUAGE.'.PRODUCTS')); ?> </a>  </li> 
                <!--<li><a href="<?php echo url('mer_services_dashboard');?>" <?php if($menu=="services"){?>class="active"<?php } ?>> <?php if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SERVICES');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SERVICES');} ?></a>  </li>
                <li><a href="<?php //echo url('merchant_add_auction');?>" <?php //if($menu=="auction"){?>class="active"<?php //} ?>> Auction </a>  </li> -->
                <?php /*<li><a href="<?php echo url('show_merchant_transactions');?>" <?php if($menu=="transaction"){?>class="active"<?php } ?>> TRANSACTIONS  </a>  </li>*/ ?>
                <li><a href="<?php echo e(url('merchant_deals_all_orders')); ?>" <?php if($menu=="transaction"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.TRANSACTIONS')!= '') ?  trans(Session::get('mer_lang_file').'.TRANSACTIONS') : trans($MER_OUR_LANGUAGE.'.TRANSACTIONS')); ?>  </a>  </li>
                <li><a href="<?php echo e(url('fund_request')); ?>"  <?php if($menu=="funds"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.FUND_REQUESTS')!= '') ? trans(Session::get('mer_lang_file').'.FUND_REQUESTS') : trans($MER_OUR_LANGUAGE.'.FUND_REQUESTS')); ?> </a>  </li>
                <li><a href="<?php echo e(url('merchant_manage_shop/'.$merchantid)); ?>" <?php if($menu=="shop"){?>class="active"<?php } ?>>  <?php echo e((Lang::has(Session::get('mer_lang_file').'.STORES')!= '') ?  trans(Session::get('mer_lang_file').'.STORES') : trans($MER_OUR_LANGUAGE.'.STORES')); ?>  </a> </li>
                <li><a href="<?php echo e(url('commission_listing')); ?>" <?php if($menu=="commissions"){?>class="active"<?php } ?>>  <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_LISTING')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMISSION_LISTING') : trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_LISTING')); ?>  </a> </li>
           </ul>
            </div>


                 <div class="container" style="width: 98%; margin: auto; overflow: hidden;"><a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                        SUB MENU <i class="icon-align-justify"></i>
                </a></div>





        </div>
		<!--Loader & alert-->
<div id="loader" style="position: absolute; display: none;"><div class="loader-inner"></div>
 
  <div class="loader-section"></div>
</div>
<div id="loadalert" class="alert-success" style="margin-top:18px; display: none; position: fixed; z-index: 9999; width: 50%; text-align: center; left: 25%; padding: 10px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
  <strong>Success!</strong>
</div>
     <!--Loader & alert-->
		<script type="text/javascript">
	function Lang_change() 
	{
	
		var language_code = $("#Language_change option:selected").val();
		
		$.ajax
		({
			type:'POST',
            url:"<?php echo url('merchant_new_change_languages');?>",
            data:{'language_code':language_code},
            success:function(data)
			{
				
				window.location.reload();
            }
        });
	}
</script>