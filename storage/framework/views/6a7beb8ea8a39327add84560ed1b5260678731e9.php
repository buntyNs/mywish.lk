
<?php 
  if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
    $deal_title = 'deal_title';
    $pro_title = 'pro_title';
    $stor_name = 'stor_name';

  }else {   $deal_title = 'deal_title_'.Session::get('lang_code'); 
            $pro_title = 'pro_title_'.Session::get('lang_code');
              $stor_name = 'stor_name_'.Session::get('lang_code'); 
               }


?>

<section class="blog_post">
    <div class="container" id="three">
       <div class="row">
	    <div class="col-xs-12 col-sm-9 col-sm-push-3" id="center_column">
	   
	   <?php if(count($selected_city)>0)  {  ?>
		<div>
			 <div id="map" style="height: 428px;  margin: 2%; ">
			 </div>
		   <?php 
			if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
			$map_lang = 'en';
			}else {  
			$map_lang = Session::get('lang_code');
			}
		   ?>
			  
			<script type="text/javascript">
			var locations = [
		   <?php  
			foreach($get_store_all as $sg) { 
			 if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
			  $stor_name = 'stor_name';
			  $stor_address1 = 'stor_address1';
			  $stor_address2 = 'stor_address2';
			 }else {  
			  $stor_name = 'stor_name_'.Session::get('lang_code'); 
			  $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 
			  $stor_address2 = 'stor_address1_'.Session::get('lang_code'); 
			 } ?>        
			  ['<b><?php if (Lang::has(Session::get('lang_file').'.STORE_NAME')!= '') { echo  trans(Session::get('lang_file').'.STORE_NAME');}  else { echo trans($OUR_LANGUAGE.'.STORE_NAME');} ?>:</b>&nbsp;<?php echo $sg->$stor_name; ?>,<br/><b><?php if (Lang::has(Session::get('lang_file').'.ADDRESS1')!= '') { echo  trans(Session::get('lang_file').'.ADDRESS1');}  else { echo trans($OUR_LANGUAGE.'.ADDRESS1');} ?>:</b>&nbsp;<?php echo $sg->$stor_address1; ?>,<br/><?php echo $sg->$stor_address2; ?>,<br/><b><?php if (Lang::has(Session::get('lang_file').'.PHONE')!= '') { echo  trans(Session::get('lang_file').'.PHONE');}  else { echo trans($OUR_LANGUAGE.'.PHONE');} ?>:</b>&nbsp;<?php echo $sg->stor_phone; ?>',  <?php echo $sg->stor_latitude; ?>, <?php echo $sg->stor_longitude; ?>, 4],
			  <?php $stor_latitude=$sg->stor_latitude; $stor_longitude=$sg->stor_longitude;}  ?>
			];
			

			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 10,
			
			  center: new google.maps.LatLng(<?php echo $stor_latitude; ?>, <?php echo $stor_longitude; ?>),
			
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			var infowindow = new google.maps.InfoWindow();

			var marker, i;

			for (i = 0; i < locations.length; i++) { 
			  marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map
			  });

			  google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
				  infowindow.setContent(locations[i][0]);
				  infowindow.open(map, marker);
				}
			  })(marker, i));
			}
		  </script>
	  </div>
<?php } else { 
  
    if (Lang::has(Session::get('lang_file').'.Nearbyshop')!= '')
     { ?><div class="no-str-avail span8">
      <?php echo  trans(Session::get('lang_file').'.Nearbyshop'); ?>
      </div> <?php }  
    else { ?><div class="no-str-avail span8">
        <?php echo trans($OUR_LANGUAGE.'.Nearbyshop');?>
             </div> <?php }
       } ?>  
	   
	   </div>
	  
	     <aside class="sidebar col-xs-12 col-sm-3 col-sm-pull-9"> 
          <!-- Blog category -->
          <div class="block blog-module">
		  
		   <div class="sidebar-bar-title">
              <h3><?php echo e((Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') ? trans(Session::get('lang_file').'.SELECT_CITY') : trans($OUR_LANGUAGE.'.SELECT_CITY')); ?></h3>
            </div>
           
            <div class="block_content">
			   <?php $selected_cit=0; if(count($selected_city)>0) { $selected_cit=$selected_city[0]->ci_id;  } ?>
               <select name="selected_city" id="selected_city" required data-parsley-required="true" onchange="select_city(this.value)">
               <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?>--</option>
               <?php $__currentLoopData = $city_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code')=='en'): ?> 
                   <option value="<?php echo e($city->ci_id); ?>" <?php if($selected_cit==$city->ci_id): ?> selected="selected" <?php endif; ?>><?php echo e($city->ci_name); ?></option>
                 <?php else: ?>
                 <!-- if Selected Language city name Display -->
                   <?php $get_lang = 'ci_name_'.Session::get('lang_code'); ?>
                   <option value="<?php echo e($city->ci_id); ?>" <?php if($selected_cit==$city->ci_id): ?> selected="selected" <?php endif; ?>><?php echo e($city->$get_lang); ?></option>
                 <?php endif; ?>
                
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            </div>
          </div>
		  
		    <div class="block blog-module">
            <div class="sidebar-bar-title">
              <h3><?php if(Lang::has(Session::get('lang_file').'.STORES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORES')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORES')); ?> <?php endif; ?></h3>
            </div>
            <div class="block_content near-map"> 
              <!-- layered -->
              <div class="layered">
                <div class="layered-content">
				
                  <ul class="blog-list-sidebar">
				     <?php if(count($selected_city)>0)  {  
					  foreach($selected_city as $nearstore) { 
						$product_image =explode('/**/',$nearstore->stor_img);
						$name=$nearstore->stor_name;
						$product_img    =$product_image[0];

							/* Image Path */
							$prod_path  = url('').'/public/assets/default_image/No_image_product.png';
							$img_data   = "public/assets/storeimage/".$product_img;
							
							if(file_exists($img_data) && $product_img !='')   {
							
							$prod_path = url('').'/public/assets/storeimage/' .$product_img;       
									   
							}else{  
								if(isset($DynamicNoImage['store'])){
									$dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['store'];
									if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path))
									  $prod_path = url('').'/'.$dyanamicNoImg_path;
								}
							}   
							/* Image Path ends */   
							//Alt text
							$alt_text  = $nearstore->$stor_name;
						   // $alt_text=$store->$stor_name;
					   ?>
                    <li>
                      <div class="post-thumb" id="<?php echo e($nearstore->stor_city); ?>"> <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($nearstore->stor_id)))); ?>"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a> </div>
                      <div class="post-info">
                        <h5 class="entry_title"><a><?php echo e(substr($nearstore->$stor_name,0,25)); ?>

                                 <?php echo e(strlen($nearstore->$stor_name)>25?'..':''); ?></a></h5>
                      </div>
                    </li>
                    <?php  } }
					else { ?>
					<?php if (Lang::has(Session::get('lang_file').'.Nearbyshop')!= '') { echo  trans(Session::get('lang_file').'.Nearbyshop');}  else { echo trans($OUR_LANGUAGE.'.Nearbyshop');} ?>...<?php } ?>
                  </ul>
                </div>
              </div>
              <!-- ./layered --> 
            </div>
          </div>
          
         
      </aside>
         
</div>
</div>
</section>



<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->

 
  <!-- For Responsive menu-->
<script type="text/javascript">

    $(document).ready(function() {
        $(document).on("click", ".customCategories .topfirst b", function() {
            $(this).next("ul").css("position", "relative");

            $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");
            $(this).next("ul").toggle();
        });

        $(document).on("click", ".morePage", function() {
            $(".nextPage").slideToggle(200);
        });

        $(document).on("click", "#smallScreen", function() {
            $(this).toggleClass("customMenu");
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 250) {
                $('#comp_myprod').show();
            } else {
                $('#comp_myprod').hide();
            }
        });

    });
    

  function select_city(city_id)
  {
     var passData = 'city_id='+city_id;
    
     
     $.ajax({
            type:'get',
          data: passData,
          url: '<?php echo url('nearbystore_select_city'); ?>',
          success: function(data){  
          $("#three").html(data);
           
        }   
      });   
  }
</script>

</body>
</html>