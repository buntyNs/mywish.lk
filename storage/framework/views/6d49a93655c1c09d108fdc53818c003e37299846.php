<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


<!-- Header End====================================================================== -->


<head>

<link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

<!-- Basic page needs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('')); ?>"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a><span>&raquo;</span></li>

            <li><strong><?php if(Lang::has(Session::get('lang_file').'.PAYMENT_RESULT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PAYMENT_RESULT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PAYMENT_RESULT')); ?> <?php endif; ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <div class="category-description std">

            <div class="slider-items-products">

             

            </div>

          </div>

		  

		  

          <div class="shop-inner">

            <div class="page-title"> <div class="about-page"> 

			<?php if(Session::has('result')): ?>

              <h2><span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.YOUR_ORDER_SUCCESSFULLY_PLACED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.YOUR_ORDER_SUCCESSFULLY_PLACED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.YOUR_ORDER_SUCCESSFULLY_PLACED')); ?> <?php endif; ?>.</span></h2>

		    <?php endif; ?>



			<?php if(Session::has('fail')): ?>

              <h2><span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_FAILED')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_FAILED')); ?> <?php endif; ?>.</span></h2>

		    <?php endif; ?>

			

			<?php if(Session::has('error')): ?>

              <h2><span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR')); ?> <?php endif; ?>.</span></h2>

		    <?php endif; ?>

            </div></div>

            <div class="product-grid-area">

			

			<div class="clearfix"></div>	<hr class="soft"/>			 

				<h4><?php if(Lang::has(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.THANK_YOU_FOR_SHOPPING_WITH')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.THANK_YOU_FOR_SHOPPING_WITH')); ?> <?php endif; ?> <?php echo e($SITENAME); ?> . </h4>				

				<h5><?php if(Lang::has(Session::get('lang_file').'.TRANSACTION_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TRANSACTION_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TRANSACTION_DETAILS')); ?> <?php endif; ?></h5>

        <div class="table-responsive">	

				<table class="table table-bordered">              

					<thead><tr>                	

					<th><?php if(Lang::has(Session::get('lang_file').'.CUSTOMER_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CUSTOMER_NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CUSTOMER_NAME')); ?> <?php endif; ?></th>                   

					<th><?php if(Lang::has(Session::get('lang_file').'.TRANSACTION_ID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TRANSACTION_ID')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TRANSACTION_ID')); ?> <?php endif; ?></th>					

					<th><?php if(Lang::has(Session::get('lang_file').'.STATUS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STATUS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STATUS')); ?> <?php endif; ?></th>				

					</tr>              

                    </thead>              

                    <tbody>           			 

                    <?php  $coupon = 0;

                           $shipping_amt = 0; ?>

                    <?php if($orderdetails): ?> 				

                        <?php $__currentLoopData = $orderdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderdet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

						

                        <?php    $coupon+= $orderdet->coupon_amount;

                            $shipping_amt+= $orderdet->cod_shipping_amt; ?>    <!-- //shipping amount getting from order table -->

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php $coupon_amt = $orderdet->coupon_amount; ?>

                     <tr>                

                        <td><?php echo Session::get('user_name');?></td> 

                        <td><?php echo $orderdet->cod_transaction_id;?> </td>

                        <td><?php echo e((Lang::has(Session::get('lang_file').'.HOLD')!= '') ? trans(Session::get('lang_file').'.HOLD') : trans($OUR_LANGUAGE.'.HOLD')); ?></td>                                                     

                    </tr>		  

                    <?php endif; ?>			

                    </tbody>            

                </table></div>

				<h5> <?php if(Lang::has(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION')); ?> <?php endif; ?> </h5>	

          <div class="table-responsive">  

				<table class="table table-bordered">              

                    <thead>                

                    <tr>                

                        <th><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCT_DEAL_NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_DEAL_NAME')); ?> <?php endif; ?></th>                  <th><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCT_DEAL_QUANTITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_DEAL_QUANTITY')); ?> <?php endif; ?></th>                  <th><?php if(Lang::has(Session::get('lang_file').'.AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AMOUNT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AMOUNT')); ?> <?php endif; ?>

                        </th>                				

                    </tr>              

                    </thead>              

                    <tbody>										

                    <?php  $taxamount =0;   $wallet=0; $trans_id =0; $taxamount_total = 0; ?>

                    <?php if($orderdetails): ?> 

							

                        <?php $__currentLoopData = $orderdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderdet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

							
							
						 <?php $getcolor = DB::table('nm_color')->select('co_name')->where('co_id','=',$orderdet->cod_pro_color)->first();
						 $getsize = DB::table('nm_size')->select('si_name')->where('si_id','=',$orderdet->cod_pro_size)->first();
						 

						 $trans_id = $orderdet->cod_transaction_id;


                            $taxamount = (($orderdet->cod_amt*$orderdet->cod_tax)/100);



							$taxamount_total += $taxamount; ?>

															  

							<tr>               	  

								<td> 

								<?php if($orderdet->cod_order_type == 1): ?> 

									<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

									<?php	$pro_title = 'pro_title'; ?>

									<?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

									<?php echo e($orderdet->$pro_title); ?>

									
									
									<?php if( $orderdet->cod_pro_color!=''): ?>
									<br><b><?php if (Lang::has(Session::get('lang_file').'.COLOR')!= '') { echo  trans(Session::get('lang_file').'.COLOR');}  else { echo trans($OUR_LANGUAGE.'.COLOR');} ?>: </b><?php echo e($getcolor->co_name); ?>

									<?php endif; ?>
									
									<?php if( $orderdet->cod_pro_size!=''): ?>
									<br><b><?php if (Lang::has(Session::get('lang_file').'.SIZE')!= '') { echo  trans(Session::get('lang_file').'.SIZE');}  else { echo trans($OUR_LANGUAGE.'.SIZE');} ?> : </b><?php echo e($getsize->si_name); ?>

									<?php endif; ?>
									

								<?php else: ?>  

									<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

									<?php	$deal_title = 'deal_title'; ?>

									<?php else: ?> <?php  $deal_title = 'deal_title_'.Session::get('lang_code'); ?> <?php endif; ?>

									<?php echo e($orderdet->$deal_title); ?>


								<?php endif; ?></td>                  

								<td><?php echo e($orderdet->cod_qty); ?></td>                  

								<td><?php echo e(Helper::cur_sym()); ?> <?php echo round(($orderdet->cod_amt + $taxamount),2);?> (<?php if(Lang::has(Session::get('lang_file').'.INCLUDING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.INCLUDING')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.INCLUDING')); ?> <?php endif; ?> <?php echo e($orderdet->cod_tax); ?> % <?php echo e(round(($orderdet->cod_amt + $taxamount),2)); ?> (<?php if(Lang::has(Session::get('lang_file').'.TAXES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TAXES')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TAXES')); ?> <?php endif; ?>)</td>      			 

							</tr>  

                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	

				 

                    <?php endif; ?>

					

                    <?php if(($coupon)!=0): ?> 

                        <tr>

                        <td>&nbsp;</td> 

                        <td> <?php echo e((Lang::has(Session::get('lang_file').'.COUPON_VALUE')!= '') ? trans(Session::get('lang_file').'.COUPON_VALUE') : trans($OUR_LANGUAGE.'.COUPON_VALUE')); ?></td>

                        <td><?php echo e(Helper::cur_sym()); ?> <?php echo e(round($coupon,2)); ?></td>

                        </tr>

                    <?php endif; ?>



					

					<tr>                     

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if(Lang::has(Session::get('lang_file').'.SUB-TOTAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SUB-TOTAL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SUB-TOTAL')); ?> <?php endif; ?></td>                  

                        <td style="font-weight:bold;"><?php echo e(Helper::cur_sym()); ?> <?php $subtotal = ($get_subtotal+$taxamount_total)-$coupon; echo round($subtotal,2);?> </td>                   

                    </tr>

					

                    

                

                    <?php /*

                    <tr>               	  

                        <td>&nbsp;</td> 

                        <td style="font-weight:bold;"><?php if (Lang::has(Session::get('lang_file').'.TAX')!= '') { echo  trans(Session::get('lang_file').'.TAX');}  else { echo trans($OUR_LANGUAGE.'.TAX');} ?></td>                  

                        <td style="font-weight:bold;"> <?php  echo $get_tax;?> %</td>      			

                    </tr>*/?>

                    

                    <tr>               	  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if(Lang::has(Session::get('lang_file').'.SHIPPING_TOTAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHIPPING_TOTAL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHIPPING_TOTAL')); ?> <?php endif; ?></td>                  

                        <td style="font-weight:bold;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($shipping_amt); ?></td>      			 

                    </tr>



                    

                  <?php  $trans_wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$trans_id)->value('wallet_used');

                    $wallet = $trans_wallet; ?>

                   <?php if(count($wallet)!=0): ?>

                        

                    <tr>

                        <td>&nbsp;</td> 

                        <td><?php echo e((Lang::has(Session::get('lang_file').'.WALLET_USED')!= '') ? trans(Session::get('lang_file').'.WALLET_USED') : trans($OUR_LANGUAGE.'.WALLET_USED')); ?> </td>

                        <td> - <?php echo e(Helper::cur_sym()); ?> round($wallet,2); ?></td>

                    </tr>

                    <?php else: ?> <?php $wallet = 0; ?> <?php endif; ?>

                    

                    <tr>               	  

                        <td>&nbsp;</td>                  

                        <td style="font-weight:bold;"><?php if(Lang::has(Session::get('lang_file').'.TOTAL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOTAL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TOTAL')); ?> <?php endif; ?></td>                  

                        <td style="font-weight:bold;"><?php echo e(Helper::cur_sym()); ?> <?php $total = ($subtotal + $shipping_amt) - $wallet;  echo round($total,2);//number_format((float)$total, 2, '.', '');  ?></td>      			

                    </tr>



                  </tbody>            

                 </table> 

				          </div>

				 <div class="special-product">

				 <h4><a class="link-all pull-right me_btn res-cont1" href="<?php echo e(url('index')); ?>"><?php if(Lang::has(Session::get('lang_file').'.CONTINUE_SHOPPING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONTINUE_SHOPPING')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONTINUE_SHOPPING')); ?> <?php endif; ?></a></h4>

				 </div>

				 <div class="clearfix"></div>

				

            </div>

           

          </div>

		  

		  

        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

          

          <div class="block shop-by-side">

            <!--<div class="sidebar-bar-title">

              <h3>Shop By</h3>

            </div>-->

            <div class="block-content">

              <p class="block-subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES')); ?></p>

              <div class="layered-Category">

                <div class="layered-content">

		

				<div id="divMenu">

					<ul>

					   <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                       <?php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>						

						<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>">

							 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

							  <?php $mc_name = 'mc_name'; ?>

							  <?php else: ?> <?php  $mc_name = 'mc_name_code'; ?> <?php endif; ?>

							  <?php echo e($fetch_main_cat->$mc_name); ?></a> 

							  <?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0): ?>  

						   <ul>

							 <?php $__currentLoopData = $sub_main_category[$fetch_main_cat->mc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                               <?php  $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>

                               <?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0): ?> 

								   

								<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"> 

								 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

									<?php	$smc_name = 'smc_name'; ?>

									<?php else: ?> <?php  $smc_name = 'smc_name_code'; ?> <?php endif; ?>

									<?php echo e($fetch_sub_main_cat->$smc_name); ?> </a>
									 <ul>

									   <?php $__currentLoopData = $second_main_category[$fetch_sub_main_cat->smc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                                       <?php  $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; ?>

                                        <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0): ?>

										<li><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"> 

										<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

										  <?php $sb_name = 'sb_name'; ?>

										  <?php else: ?> <?php  $sb_name = 'sb_name_langCode'; ?> <?php endif; ?>

										  <?php echo e($fetch_sub_cat->$sb_name); ?></a> 

											<ul>	

												<?php $__currentLoopData = $second_sub_main_category[$fetch_sub_cat->sb_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_secsub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                                                <?php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?> 

												<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)); ?>">

												<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

												<?php $ssb_name = 'ssb_name'; ?>

												<?php else: ?> <?php  $ssb_name = 'ssb_name_langCode'; ?> <?php endif; ?>

												<?php echo e($fetch_secsub_cat->$ssb_name); ?></a>

												</li>

												 

													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

											</ul>

											 <?php endif; ?>

										</li>

										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									</ul>

									 <?php endif; ?>

								</li>

								 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						   </ul>

						   <?php endif; ?>

						</li>

						 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					</ul>

					

				</div>

                </div>

              </div>

             

            </div>

          </div>

		  

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  

  

  

  <?php echo $footer; ?>


</html>

