<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PRODUCT_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_PRODUCT_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PRODUCT_DETAILS')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


       <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->
        
	<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                                <li class="active"><a ><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_DETAILS')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_DETAILS')); ?></h5>
            
        </header>
      <?php $__currentLoopData = $product_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php
		 $title 		 = $products->pro_title;
                $category_get	 = $products->pro_mc_id;
	         $maincategory 	 = $products->pro_smc_id;
		 $subcategory 	 = $products->pro_sb_id;
		 $secondsubcategory= $products->pro_ssb_id;
		 $originalprice  = $products->pro_price;
		 $discountprice  = $products->pro_disprice;
		 $inctax=$products->pro_inctax;
		 $shippingamt=$products->pro_shippamt;
		 $description 	 = $products->pro_desc;
		 $description_fr 	 = $products->pro_desc_fr;
		 $deliverydays=$products->pro_delivery;
		 $metakeyword	 = $products->pro_mkeywords;
		 $metakeyword_fr	 = $products->pro_mkeywords_fr;
		 $metadescription= $products->pro_mdesc;
		 $metadescription_fr= $products->pro_mdesc_fr;
	     $file_get  = $products->pro_Img;
        $file_get_path =  explode("/**/",$file_get,-1); 
		 $img_count		 = $products->pro_image_count;



 
?>

        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    <?php echo e(Form::open()); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_DETAILS')); ?>

                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_TITLE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                       <?php echo e($title); ?>

					  
				<?php if(!empty($get_active_lang)): ?>
				 
					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php
						$get_lang_name = $get_lang->lang_code;
						$Product_Title_dynamic = 'pro_title_'.$get_lang->lang_code;
         ?>
            <?php if($products->$Product_Title_dynamic != ''): ?>
						 <?php echo '('.$products->$Product_Title_dynamic.')'; ?>
            <?php else: ?>
                <?php ?>
            <?php endif; ?>
				
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                    </div>
                </div>
                        </div>
					 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TOP_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TOP_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_TOP_CATEGORY')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo e($products->mc_name); ?>

                                          </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_MAIN_CATEGORY')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo e($products->smc_name); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SUB_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SUB_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SUB_CATEGORY')); ?><span class="text-sub"></span></label>
                    <div class="col-lg-4">
                    <?php echo e($products->sb_name); ?>

                    </div>
                </div>
                </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SECOND_SUB_CATEGORY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SECOND_SUB_CATEGORY') :  trans($MER_OUR_LANGUAGE.'.MER_SECOND_SUB_CATEGORY')); ?><span class="text-sub"></span></label>
                    <div class="col-lg-4">
                    <?php echo e($products->ssb_name); ?>

                    </div>
                </div>
                
                
                        </div>

					<div class="panel-body">
                       <div class="form-group">
                         <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SKU_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SKU_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SKU_NUMBER')); ?> <?php endif; ?><span class="text-sub">*</span></label>
                         <div class="col-lg-4">
                           <?php echo e($products->pro_sku_number); ?>

                         </div>
                       </div>
                    </div>
                    <div class="panel-body">
                       <div class="form-group">
                         <label class="control-label col-lg-2" for="text1">Product Quantity<span class="text-sub">*</span></label>
                         <div class="col-lg-4">
                           <?php echo e($products->pro_qty); ?>

                         </div>
                       </div>
                    </div>	
                       
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_PRICE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo e($originalprice); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT_PRICE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                          <?php echo e($discountprice); ?>

                    </div>
                </div>
                        </div>

		 <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Tax Percentage</label>
                        <div class="col-lg-4">
                           <?php echo e($products->pro_inctax); ?>

                        </div>
                    </div>
                </div>
				
						 
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                     <?php echo e($shippingamt); ?>

                    </div>
                </div>
                        </div>
                        
                 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DESCRIPTION')  : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_DESCRIPTION')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                            <?php echo e($description); ?> 
                    </div>
                </div>
</div>
				
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
                $get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				$Description_dynamic = 'pro_desc_'.$get_lang->lang_code; ?>
				<?php if($products->$Description_dynamic !=''): ?>
				
					
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> Description(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->$Description_dynamic); ?>

                        </div>
                    </div>
               </div>
			   
			   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>

			<?php if($description_fr !=''){?>
			   <!--<div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> Description(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php// echo $description_fr; ?> 
                        </div>
                    </div>
               </div>-->
			   <?php } ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> Specification<span class="text-sub"></span></label>
                        <div class="col-lg-8">
                            
                        
                <?php if(count($product_spec_details)!=0): ?>
              <table class="table table-bordered">
				<tbody>
					
    				<tr><th colspan="2"><?php echo e($product_spec_details[0]->spg_name); ?> 
					<?php if($product_spec_details[0]->spg_name_fr !=''): ?> <?php echo '('.$product_spec_details[0]->spg_name_fr.')';  ?> <?php endif; ?>
					</th></tr>
					<?php $__currentLoopData = $product_spec_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		
					<tr>
						<td>
						<?php echo e($spec->sp_name); ?>

						 
						
						<?php if(!empty($get_active_lang)): ?> 
						<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
						<?php
                        $get_lang_name = $get_lang->lang_code;
						$sp_name_dynamic = 'sp_name_'.$get_lang->lang_code;
                        ?>
						<?php if($spec->$sp_name_dynamic!=""): ?>
						(<?php echo e($spec->$sp_name_dynamic); ?>)
						<?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>
						</td>
						<td>
						<?php echo e($spec->spc_value); ?>

						
						<?php if(!empty($get_active_lang)): ?> 
						<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php 
						$get_lang_name = $get_lang->lang_code;
						$spc_value_dynamic = 'spc_value_'.$get_lang->lang_code;
                        ?>
						<?php if($spec->$spc_value_dynamic!=""): ?>
						
						
						(<?php echo e($spec->$spc_value_dynamic); ?>)
						<?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>
						</td>	
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    			</tbody>
			  </table>
			<?php else: ?> <?php echo e('-'); ?> <?php endif; ?>
                </div>
                    </div>
               </div>
               <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Product Color<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                           
                           <?php if(count($product_color_details) > 0 ): ?> 
                             <?php $__currentLoopData = $product_color_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
						        <?php echo e($product_color_det->co_name.","); ?>

						     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php else: ?> <?php echo e('-'); ?> <?php endif; ?>
                        </div>
                    </div>
               </div>
               <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Product Size<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                            
				   <?php if(count($product_size_details)!=0): ?> 
					 <?php
                       $size_name = $product_size_details[0]->si_name;
					   $return  = strcmp($size_name,'no size');
                       ?>
                       
				   	<?php if($return!=0): ?>
				
                    <?php $__currentLoopData = $product_size_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
					   <?php 	  echo $product_size_det->si_name.','; ?>
						 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?> <?php echo e('-'); ?> <?php endif; ?>
                  <?php endif; ?>
                        </div>
                    </div>
               </div>        
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Delivery Days<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                           <?php echo e($products->pro_delivery); ?> Days 
                        </div>
                    </div>
               </div>

               <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Merchant Name<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                            <?php echo e($products->mer_fname.' '.$products->mer_lname); ?>

                        </div>
                    </div>
               </div>        
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Store Name<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                           <?php echo e($products->stor_name); ?> <?php if($products->stor_name_fr): ?> <?php echo e('('.$products->stor_name_fr.')'); ?>  <?php endif; ?> 
                        </div>
                    </div>
               </div>

               <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Meta Keywords<span class="text-sub"></span></label>
                        <div class="col-lg-8">
                            <?php echo e($products->pro_mkeywords); ?>

                        </div>
                    </div>
               </div>	 
			    
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php
                $get_lang_name = $get_lang->lang_name;
				$Meta_Keywords_dynamic = 'pro_mkeywords_'.$get_lang->lang_code;
                ?>
				<?php if($products->$Meta_Keywords_dynamic !=''): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Meta Keywords(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>
                        <div class="col-lg-8">
                            <?php echo e($products->$Meta_Keywords_dynamic); ?>

                        </div>
                    </div>
               </div>
			   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>
			   <?php if($products->pro_mkeywords_fr !=''){ ?>
			   <!--<div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Meta Keywords(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                            <?php echo $products->pro_mkeywords_fr; ?>  
                        </div>
                    </div>
               </div>-->
			   <?php } ?>
                 <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')); ?><span class="text-sub"></span></label>
                        <div class="col-lg-8">
                            <?php echo e($products->pro_mdesc); ?>

                        </div>
                    </div>
               </div>
			   
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php 
                $get_lang_name = $get_lang->lang_code;
				$Meta_Description_dynamic = 'pro_mdesc_'.$get_lang->lang_code;
                ?>
				<?php if($products->$Meta_Description_dynamic !=''): ?>
			    <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')); ?>(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>
                        <div class="col-lg-8">
                            <?php echo e($products->$Meta_Description_dynamic); ?>

                        </div>
                    </div>
               </div>
			  <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
			    <?php if($products->pro_mdesc_fr !=''){?>
			   <!-- <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Meta Description(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                            <?php echo $products->pro_mdesc_fr; ?>  
                        </div>
                    </div>
               </div>-->
			   <?php } ?>
               <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Cash Back<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                           <?php echo e(Helper::cur_sym()); ?> <?php echo e($products->cash_pack); ?>

                        </div>
                    </div>
               </div>       
                          <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Apply Cancellation Policy<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                          <?php if($products->allow_cancel == 1 ): ?>  <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>
                        </div>
                    </div>
               </div>
			   
			    <?php if($products->allow_cancel == 1 ): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> Cancellation Policy <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->cancel_policy); ?> 
                        </div>
                    </div>
				</div><?php endif; ?>
			   
			    <?php if($products->allow_cancel == 1 ): ?>  
                <?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <?php
				$get_lang_name = $get_lang->lang_name;
				$cancel_policy_dynamic = 'cancel_policy_'.$get_lang->lang_code; 
				$descrip_cancel_policy = $products->$cancel_policy_dynamic;
                ?>
			    <?php if($descrip_cancel_policy !=''): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> Cancellation Policy(<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($descrip_cancel_policy); ?>

                        </div>
                    </div>
               </div>
			   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>
               <?php endif; ?>
			   
			    <?php if($products->allow_cancel == 1 ): ?> 
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> No of days Cancellation Applicable <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->cancel_days); ?>

                        </div>
                    </div>
			   </div><?php endif; ?>
				
				
				  <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Apply Return/Refund Policy<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                         <?php if($products->allow_return == 1 ): ?> <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>  
                        </div>
                    </div>
               </div>
			   
			    <?php if($products->allow_return == 1 ): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Return Policy <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->return_policy); ?>

                        </div>
                    </div>
				</div><?php endif; ?>
			   
			   <?php if($products->allow_return == 1 ): ?>  
               <?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php 
                $get_lang_name = $get_lang->lang_name;
				$return_policy_dynamic = 'return_policy_'.$get_lang->lang_code; 
				$return_policy = $products->$return_policy_dynamic;
                ?>
			    <?php if($return_policy !=''): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Return Policy(<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($return_policy); ?>

                        </div>
                    </div>
               </div>
			   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>
               <?php endif; ?>
			   
			    <?php if($products->allow_return == 1 ): ?> 
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> No of days Return Applicable <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->return_days); ?>

                        </div>
                    </div>
				</div><?php endif; ?>
				
				
				 <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Apply Replacement Policy<span class="text-sub">*</span></label>
                        <div class="col-lg-8">
                          <?php if($products->allow_replace == 1 ): ?> <?php echo e("Yes"); ?> <?php else: ?> <?php echo e("No"); ?> <?php endif; ?>  
                        </div>
                    </div>
               </div>
			   
			    <?php if($products->allow_replace == 1 ): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Replacement Policy <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->replace_policy); ?>

                        </div>
                    </div>
				</div> <?php endif; ?>
			   
			   <?php if($products->allow_replace == 1 ): ?>  
               <?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php 
                $get_lang_name = $get_lang->lang_name;
				$replace_policy_dynamic = 'replace_policy_'.$get_lang->lang_code; 
				$replace_policy = $products->$replace_policy_dynamic; ?>
			    <?php if($replace_policy !=''): ?>
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1">Replacement Policy (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($replace_policy); ?>

                        </div>
                    </div>
               </div>
			   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <?php endif; ?>
               <?php endif; ?>
			   
			    <?php if($products->allow_replace == 1 ): ?> 
			   <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="text1"> No of days Replacement Applicable<span class="text-sub">*</span></label>
                        <div class="col-lg-10">
                            <?php echo e($products->replace_days); ?>

                        </div>
                    </div>
				</div><?php endif; ?>
		  
					  
 <div class="panel-body">
                           
			 <div class="panel-body">
                 <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_IMAGE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php $img_count = count($file_get_path);  ?>
                       
                   <?php if(count($file_get_path) > 0  && $img_count != ''): ?> 
				
					  
					   <?php for($j=0 ; $j< $img_count; $j++): ?>
					   
						   <?php if($file_get_path[$j] !=''): ?>  
						    <?php
							   $pro_img = $file_get_path[$j];
							   $prod_path = url('').'/public/assets/default_image/No_image_product.png';
							   $img_data = "public/assets/product/".$pro_img; ?>
							    <?php if(file_exists($img_data)): ?>  
									<?php       
                                                                             $prod_path = url('').'/public/assets/product/'.$pro_img;
                                                            ?>
								<?php else: ?>  
										 <?php if(isset($DynamicNoImage['productImg'])): ?>
                                                    <?php                    
                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>    
                                                    <?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
                                                     <?php   
                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>  
                                                    <?php endif; ?>
                                                                        
                                                 <?php endif; ?>
                                                                     
                                                                     
                                                <?php endif; ?>
					       
					      
						   <?php else: ?>
						      <?php
							    $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
								<?php if(isset($DynamicNoImage['productImg'])): ?>
                                                    <?php                    
                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>    
                                                    <?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
                                                     <?php   
                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>  
                                                    <?php endif; ?>
                                                                        
                                                 <?php endif; ?>
                                                                     
                                                                     
                                                <?php endif; ?>
						    <img style="height:70px; width:50px;" src="<?php echo e($prod_path); ?>">
					  <?php endfor; ?>
			      
				  <?php else: ?>
				  
					 <?php
                                $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
                                <?php if(isset($DynamicNoImage['productImg'])): ?>
                                                    <?php                    
                                                    $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg']; ?>    
                                                    <?php if($DynamicNoImage['productImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
                                                     <?php   
                                                        $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['productImg'];  ?>  
                                                    <?php endif; ?>
                                                                        
                                                 <?php endif; ?>
                                                                     
                                                                     
                                               
                            <img style="height:70px; width:50px;" src="<?php echo e($prod_path); ?>">
				 <?php endif; ?>
				   	<!-- Image path ends -->	
                    </div>
                </div>
                        </div>
                    </div>
                        </div>
		<div class="form-group">
                    <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-2">
                    <a style="color:#fff" href="<?php echo e(url('mer_manage_product')); ?>" class="btn btn-success btn-sm btn-grad"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK') : trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?></a>
                    </div>
					  
                </div>
                
                <?php echo e(Form::close()); ?>

                </div>
        
        </div>
    </div>
</div>
   
    </div>
                     </div>
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->  
  <!-- FOOTER -->
      <?php echo $adminfooter; ?>

    <!--END FOOTER --> 
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
