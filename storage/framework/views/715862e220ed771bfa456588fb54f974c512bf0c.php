﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |COD Success Orders</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
     <?php
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
     <?php if(count($favi)>0): ?>  
     <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php endif; ?>	
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/success.css" />
	 <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

<!--Loader & alert-->
<div id="loader" style="position: absolute; display: none;"><div class="loader-inner"></div>
 
  <div class="loader-section"></div>
</div>
<div id="loadalert" class="alert-success" style="margin-top:18px; display: none; position: fixed; z-index: 9999; width: 50%; text-align: center; left: 25%; padding: 10px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
  <strong>Status Changed Successfully!</strong>
</div>
     <!--Loader & alert-->

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
<?php echo $adminheader; ?>

        <!-- MENU SECTION -->
<?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a>COD Success Orders</a></li>
                            </ul>
                    </div>
                </div>
				
				<center>
		<!--  <form  action="<?php echo action('TransactionController@cod_completed_orders'); ?>" method="POST"> -->
		 	<?php echo e(Form::open(array('action'=>'TransactionController@cod_completed_orders','method'=>'POST'))); ?>

							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
							 <div class="row">
							 <br>
							 
							 
							   <div class="col-sm-3">
							    <div class="item form-group">
							<div class="col-sm-6">From Date</div>
							<div class="col-sm-6">
								<?php echo e(Form::text('from_date',$from_date,array('id'=>'datepicker-8','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly'))); ?>

							<!--  <input type="text" name="from_date"  class="form-control" id="datepicker-8" value="<?php echo e($from_date); ?>" required readonly placeholder="DD/MM/YYYY"> -->
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-3">
							    <div class="item form-group">
							<div class="col-sm-6">To Date</div>
							<div class="col-sm-6">
								<?php echo e(Form::text('to_date',$to_date,array('id'=>'datepicker-9','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly'))); ?>

							<!--  <input type="text" name="to_date"  id="datepicker-9" class="form-control" value="<?php echo e($to_date); ?>" required readonly placeholder="DD/MM/YYYY"> -->
							 
							  </div>
							  </div>
							   </div>
							   
							   <div class="form-group">
							  
							     <div class="col-sm-2">
							 <input type="submit" name="submit" class="btn btn-block btn-success" value="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEARCH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH')); ?> <?php endif; ?>">
							
							 </div>
                             <div class="col-sm-2">
							    <a href="<?php echo e(url('').'/cod_completed_orders'); ?>">
<?php echo e(Form::button('Reset',['class'=>'btn btn-block btn-info'])); ?>

							    	<!-- <button type="button" name="reset" class="btn btn-block btn-info">Reset</button> --></a>
							   </div>
							</div>
							
							 <?php echo e(Form::close()); ?>

							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>COD Completed Orders</h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">

                <!--input type="hidden" id="return_url" value="<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" /-->
				<input type="hidden" id="return_url" value="<?php echo e(URL::to('/')); ?>" />
				
				
				 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
					 <div class="col-lg-4">
					 
		</div><label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>
					  <div class="col-lg-4"></div>
                        
                    </div>
                </div>

                <div class="form-group col-lg-12">
                    	<div id="div-1" class="accordion-body collapse in body">
           <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label>
		   
		   </label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter">
		   </div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 69px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SNO')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> <?php endif; ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMERS')); ?> <?php endif; ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '') ? trans(Session::get('admin_lang_file').'.BACK_MERCHANT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT')); ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 158px;" aria-label="Email: activate to sort column ascending"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_TITLE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PRODUCT_TITLE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_TITLE')); ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="City: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 84px;" aria-label="Joined Date: activate to sort column ascending"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAX')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAX')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX')); ?> <?php endif; ?></th>
					
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 84px;" aria-label="Joined Date: activate to sort column ascending">  <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPMENT_VALUE')); ?> <?php endif; ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 101px;" aria-label="Send Mail: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_STATUS')); ?> <?php endif; ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_DATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_DATE')); ?> <?php endif; ?></th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_TYPE')); ?> <?php endif; ?></th>
                    	<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS')); ?> <?php endif; ?></th>

				</tr>
                                    </thead>
                                    <tbody>
                                       <?php $i = 1 ; ?>
									  
<?php if(isset($_POST['submit'])): ?>
			
				
			<?php $__currentLoopData = $product_completedrep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allorders_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
					
				<?php	$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SUCCESS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS');
					$ordertype=""; ?>
					
					<?php if($allorders_list->cod_paytype==0): ?>
					
					<?php	$ordertype="COD"; ?>
					<?php endif; ?>
					
				<?php	$total_tax = ($allorders_list->cod_amt * $allorders_list->cod_tax)/100 ; ?>
					
			

					<tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
                                            <td class="     "><?php echo e($allorders_list->cus_name); ?></td>
											<td class="     "><?php echo e($allorders_list->mer_fname); ?></td>
                                            <td class="     ">
                                            <ul>
											<?php
											$product_data = DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_transaction_id', '=',$allorders_list->cod_transaction_id)->get();
                                           
											foreach($product_data as $product_datavalues){

											echo '<li>'. $product_datavalues->pro_title;


											}
											?>
											</ul>
											</td>
                                            <td class="center     "><?php echo e($allorders_list->cod_amt); ?></td>
                                            <td class="center     "><?php echo e($total_tax); ?></td>
											<td class="center"><?php echo e($allorders_list->cod_shipping_amt); ?></td>
                                            <td class="center     "><a href="#" class="colr3"><?php echo e($orderstatus); ?></a></td>
                                            <td class="center     "><?php echo e($allorders_list->cod_date); ?></td>
                                            <td class="center     "><a href="#" class="colr2"><?php echo e($ordertype); ?></a></td>
                                          	<td class="center     ">
                  			<?php if($allorders_list->delivery_status==5): ?>
                                           	 	<span>Cancel Request Pending</span>
                                           	 <?php elseif($allorders_list->delivery_status==7): ?>
                                           	 	<span>Return Request Pending</span>
                                           	<?php elseif($allorders_list->delivery_status==9): ?>
                                           	 	<span>Replace Request Pending</span>
                                           	 <?php else: ?>
                  <select name="<?php  echo 'status'.$allorders_list->cod_id;?>" class="btn btn-default" onchange="update_order_cod(this.value,'<?php echo $allorders_list->cod_transaction_id;?>','<?php echo $allorders_list->pro_id;?>')">
                                             <option value="1" <?php if($allorders_list->delivery_status==2 || $allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==1){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PLACED')); ?> <?php endif; ?></option>
                                               
                                                 <option value="6" <?php if($allorders_list->delivery_status==1 || $allorders_list->delivery_status==5) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==6){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCELLED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCELLED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCELLED')); ?> <?php endif; ?></option>

                                                  <option value="2" <?php if($allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==2){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PACKED')); ?> <?php endif; ?></option>
												  
												   <option value="3" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==3){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DISPATCHED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DISPATCHED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DISPATCHED')); ?> <?php endif; ?></option>
												   
												   <option value="4" <?php if($allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==4 ){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERED')); ?> <?php endif; ?></option>
												   
												   <option value="8" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==7) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==8){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RETURNED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RETURNED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURNED')); ?> <?php endif; ?></option>

												    <option value="10" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==10 || $allorders_list->delivery_status==9) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==10){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REPALCED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REPALCED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REPALCED')); ?> <?php endif; ?></option>
                                          </select>   <?php endif; ?>    
                                              </td>
                                           
                                        </tr>

<?php $i++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 	
				
			
<?php else: ?>
	
									  
						<?php $__currentLoopData = $completedorders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allorders_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
					
				<?php	$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SUCCESS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS');
					$ordertype="";
					$ordertype="";?>
					
					<?php if($allorders_list->cod_paytype==0): ?>
					
				<?php	 $ordertype="COD"; ?>
					<?php endif; ?>
					
				<?php	$total_tax = ($allorders_list->cod_amt * $allorders_list->cod_tax)/100 ; ?>
					
			

					<tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
                                            <td class="     "><?php echo e($allorders_list->cus_name); ?></td>
											<td class="     "><?php echo e($allorders_list->mer_fname); ?></td>
                                            <td class="     ">
                                            <ul>
											<?php
											$product_data = DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_transaction_id', '=',$allorders_list->cod_transaction_id)->get();
                                           
											foreach($product_data as $product_datavalues){

											echo '<li>'. $product_datavalues->pro_title;


											}
											?>
											</ul>
											</td>
                                            <td class="center     "><?php echo e($allorders_list->cod_amt); ?></td>
                                            <td class="center     "><?php echo e($total_tax); ?></td>
											<td class="center"><?php echo e($allorders_list->cod_shipping_amt); ?></td>
                                            <td class="center     "><a href="#" class="colr3"><?php echo e($orderstatus); ?></a></td>
                                            <td class="center     "><?php echo e($allorders_list->cod_date); ?></td>
                                            <td class="center     "><a href="#" class="colr2"><?php echo e($ordertype); ?></a></td>
                                          	<td class="center     ">
                 			 <?php if($allorders_list->delivery_status==5): ?>
                                           	  <span>Cancel Request Pending</span>
                                           	 <?php elseif($allorders_list->delivery_status==7): ?>{
                                           	 <span>Return Request Pending</span>
                                           	 <?php elseif($allorders_list->delivery_status==9): ?>{
                                           	 <span>Replace Request Pending</span>
                                           	 <?php else: ?>
                  <select name="<?php  echo 'status'.$allorders_list->cod_id;?>" class="btn btn-default" onchange="update_order_cod(this.value,'<?php echo $allorders_list->cod_transaction_id;?>','<?php echo $allorders_list->pro_id;?>')">
                                             <option value="1" <?php if($allorders_list->delivery_status==2 || $allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==1){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PLACED')); ?> <?php endif; ?></option>
                                               
                                                 <option value="6" <?php if($allorders_list->delivery_status==1 || $allorders_list->delivery_status==5) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==6){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCELLED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCELLED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCELLED')); ?> <?php endif; ?></option>

                                                  <option value="2" <?php if($allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==2){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PACKED')); ?> <?php endif; ?></option>
												  
												   <option value="3" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==3){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DISPATCHED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DISPATCHED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DISPATCHED')); ?> <?php endif; ?></option>
												   
												   <option value="4" <?php if($allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==4 ){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERED')); ?> <?php endif; ?></option>
												   
												   <option value="8" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==7) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==8){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RETURNED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RETURNED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURNED')); ?> <?php endif; ?></option>

												    <option value="10" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==10 || $allorders_list->delivery_status==9) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==10){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REPALCED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REPALCED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REPALCED')); ?> <?php endif; ?></option>
                                          </select>   <?php endif; ?>    
                                              </td>
                                           
                                        </tr>

<?php $i++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
									
										
										
										
								</tbody>
                                </table><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"></div></div></div></div><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"><ul class="pagination"></ul></div></div></div></div><div class="row">
								
								<div class="col-sm-6">
								<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
								<ul class="pagination">
								<li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">
								</li>
								</ul></div></div></div></div>
        </div>
                </div>

         </form>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   <?php echo $adminfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
 
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script> 
       <script>
	function update_order_cod(id,orderid)
	{
			 var passdata= 'id='+id+"&order_id="+orderid;
			 var pathnametemp =$('#return_url').val();
				if(id==1)
				{
					 var  urlrefresh=pathnametemp+"/dealscod_all_orders";
				}
				else if(id==2)
				{
					
					 var  urlrefresh=pathnametemp+"/dealscod_completed_orders";
				}
				else if(id==3)
				{
					 var  urlrefresh=pathnametemp+"/dealscod_hold_orders";
				}
				else if(id==4)
				{
					 var  urlrefresh=pathnametemp+"/dealscod_failed_orders";
					
				}
			  $.ajax( {
			      type: 'get',
				  data: passdata,
				  url: 'update_cod_order_status',
				   beforeSend: function() {
				        // setting a timeout
				        //$(placeholder).addClass('loading');
				       document.getElementById("loader").style.display = "block";
				    },
				  success: function(responseText){ 
				  document.getElementById("loader").style.display = "none";
				  document.getElementById("loadalert").style.display = "block";
			  if(responseText=="success")
				   { 	 
				 	  setTimeout(function () {
							 location.reload();  
						},1000);					   
				   }
				}		
			});	
	}

	
	</script>
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>  
</body>
     <!-- END BODY -->
</html>
