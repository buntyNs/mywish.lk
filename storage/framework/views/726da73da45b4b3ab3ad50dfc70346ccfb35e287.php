<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo $SITENAME; ?> | <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_INQUIRIES')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_MANAGE_INQUIRIES');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_INQUIRIES');} ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="public/assets/css/main.css" />
    <link rel="stylesheet" href="public/assets/css/theme.css" />
    <link rel="stylesheet" href="public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
     <?php if(count($favi)>0): ?>  
    <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php endif; ?>	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<STYLE>

.morecontent span {
  display: none;

}
</STYLE>
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        
        <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                                <li class="active"><a > <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_INQUIRIES')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MANAGE_INQUIRIES')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_INQUIRIES')); ?> <?php endif; ?>             </a></li>
                            </ul>
                    </div>
                </div>
			
  <center><div class="cal-search-filter">
		 <form  action="<?php echo action('CustomerController@manage_inquires'); ?>" method="POST">
							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
							 <div class="row">
							 <br>
							 
							 
							   <div class="col-sm-4 col-md-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FROM_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FROM_DATE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FROM_DATE')); ?> <?php endif; ?></div>
							
 <div class="col-sm-6 place-size">
 <span class="icon-calendar cale-icon"></span>
							 <input type="text" name="from_date" 
placeholder="DD/MM/YYYY" class="form-control" id="datepicker-8"  value="<?php echo e($from_date); ?>" required readonly>
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-4 col-md-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TO_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TO_DATE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TO_DATE')); ?> <?php endif; ?></div>
							
 <div class="col-sm-6 place-size">
 <span class="icon-calendar cale-icon"></span>
							 <input type="text" name="to_date" 
placeholder="DD/MM/YYYY" id="datepicker-9" class="form-control"  value="<?php echo e($to_date); ?>" required readonly>
							 
							  </div>
							  </div>
							   </div>
							   
							  <div class="form-group">
							   <div class="col-sm-2">
							 <input type="submit" name="submit" class="btn btn-block btn-success" value="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEARCH')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH')); ?> <?php endif; ?>">
							 </div>
                              <div class="col-sm-2">
								<a href="<?php echo e(url('').'/manage_inquires'); ?>"><button type="button" name="reset" class="btn btn-block btn-info"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RESET')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?> <?php endif; ?></button></a>
							 </div>
							</div>
							
							 </form></div>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_INQUIRIES')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MANAGE_INQUIRIES')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_INQUIRIES')); ?> <?php endif; ?>            </h5>
            
        </header>
        <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
         <button type="button" class="close closeAlert"  aria-hidden="true">×</button></div>
         <?php if(Session::has('success')): ?>
		<div class="alert alert-success alert-dismissable"><?php echo Session::get('success'); ?></div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<?php endif; ?>
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">


                <div class="form-group col-lg-12">

   <div class="table-responsive panel_marg_clr ppd">
                    	<table class="table table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th style="width:10%;"class="text-center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SNO')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> <?php endif; ?></th>
                  <th class="text-center"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NAME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?> <?php endif; ?></th>
				    <th class="text-center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EMAIL')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL')); ?> <?php endif; ?></th>
				   <th style="text-align:center;"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NUMBER')); ?> <?php endif; ?></th>
				  <th style="text-align:center;"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MESSAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MESSAGE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MESSAGE')); ?> <?php endif; ?></th>
			
							   
					  <th style="text-align:center;"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DATE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE')); ?> <?php endif; ?> </th>
					   
						  
                </tr>
              </thead>
               <tbody>
                <?php $i = 1; ?>
	        <?php if(isset($_POST['submit'])): ?>
			
				<?php if(count($enquiresrep)>0): ?>
                    <?php $__currentLoopData = $enquiresrep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $enquiry_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php $enquiry_date = date('d/m/Y', strtotime($enquiry_details->created_date));
								//$enquiry_date = $enquiry_details->created_date;?>
										<tr class="gradeA odd">
                                            <td class="text-center"><?php echo e($i); ?></td>
                                            <td class="text-center"><?php echo e($enquiry_details->name); ?></td>
                                            <td class=" text-center"><?php echo e($enquiry_details->email); ?></td>
                                            <td class="text-center"><?php echo e($enquiry_details->phone); ?></td>
                                            <td class="text-left "><div class="comment more"><?php echo e($enquiry_details->message); ?></div></td>
                                            <td class="text-center"><?php echo e($enquiry_date); ?></td>
                                        
                                        </tr>
            			<?php $i++;  ?> 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                <?php endif; ?>
            <?php else: ?>
                <?php if(count($enquires_list)>0): ?> 	
    		    <?php $__currentLoopData = $enquires_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $enquiry_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <?php $enquiry_date = date('d/m/Y', strtotime($enquiry_details->created_date));
					 // $enquiry_date = $enquiry_details->created_date;?>
    				<tr class="gradeA odd">
                        <td class="text-center"><?php echo e($i); ?> </td>
                        <td class="text-center"><?php echo e($enquiry_details->name); ?> </td>
                        <td class=" text-center"><?php echo e($enquiry_details->email); ?> </td>
                        <td class="text-center"><?php echo e($enquiry_details->phone); ?> </td>
                        <td class="text-left "><div class="comment more"><?php echo e($enquiry_details->message); ?></div></td>
                        <td class="text-center"><?php echo e($enquiry_date); ?></td>
                    
                    </tr>
			<?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            <?php endif; ?>
				</tbody>
            </table></div>
                </div>

         </form>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $adminfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>


    <script type="text/javascript">
       $(document).ready(function() {
          var showChar = 300;
          var ellipsestext = "...";
          var moretext = "Show more";
          var lesstext = "Show less";
          $('.more').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {

              var c = content.substr(0, showChar);
              var h = content.substr(showChar-1, content.length - showChar);

              var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

              $(this).html(html);
            }

          });

          $(".morelink").click(function(){
            if($(this).hasClass("less")) {
              $(this).removeClass("less");
              $(this).html(moretext);
            } else {
              $(this).addClass("less");
              $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
          });
        });
    
    </script>


    <!-- END GLOBAL SCRIPTS -->   
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
         /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/

$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
      </script> 
	  
	 <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
	</script>

    

	
	
</body>
     <!-- END BODY -->
</html>
