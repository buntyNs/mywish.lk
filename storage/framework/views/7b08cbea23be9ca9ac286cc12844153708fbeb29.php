<div class="jtv-service-area">
    <div class="container">
      <div class="row">
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper ship">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-paper-plane"></i></div>
              <div class="service-wrapper">
                <h3><?php if(Lang::has(Session::get('lang_file').'.WORLD_SHIP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WORLD_SHIP')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WORLD_SHIP')); ?> <?php endif; ?></h3>
                <p><?php if(Lang::has(Session::get('lang_file').'.GET_ANYWH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_ANYWH')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_ANYWH')); ?> <?php endif; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12 ">
          <div class="block-wrapper return">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-rotate-right"></i></div>
              <div class="service-wrapper">
                <h3><?php if(Lang::has(Session::get('lang_file').'.SECURE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SECURE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SECURE')); ?> <?php endif; ?></h3>
                <p><?php if(Lang::has(Session::get('lang_file').'.BANKNG')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BANKNG')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BANKNG')); ?> <?php endif; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper support">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-umbrella"></i></div>
              <div class="service-wrapper">
                <h3><?php if(Lang::has(Session::get('lang_file').'.SUPPORT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SUPPORT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SUPPORT')); ?> <?php endif; ?></h3>
               <?php $ad_phone = DB::table('nm_emailsetting')->select('es_phone1')->get();  ?>
               <?php $__currentLoopData = $ad_phone; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ph): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php $phone = $ph->es_phone1; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               
                <p><?php if(Lang::has(Session::get('lang_file').'.CALL_US')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CALL_US')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CALL_US')); ?> <?php endif; ?> : <?php echo e($phone); ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper user">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-tags"></i></div>
              <div class="service-wrapper">
                <h3><?php if(Lang::has(Session::get('lang_file').'.MEM_DISCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MEM_DISCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MEM_DISCOUNT')); ?> <?php endif; ?></h3>
                <p><?php if(Lang::has(Session::get('lang_file').'.USE_COUPON')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.USE_COUPON')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.USE_COUPON')); ?> <?php endif; ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>