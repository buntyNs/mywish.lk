<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


 <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/sidemenu.css">

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li> 

            <li><strong><?php echo e((Lang::has(Session::get('lang_file').'.DEALS')!= '') ?  trans(Session::get('lang_file').'.DEALS'): trans($OUR_LANGUAGE.'.DEALS')); ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>



  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <div class="main-container col2-left-layout">

    <div class="container">



	 <center> <?php if(Session::has('success1')): ?>

		<div class="alert alert-warning alert-dismissable"><?php echo Session::get('success1'); ?>


			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

		</div>

		<?php endif; ?>

	</center>

	

	 <div id="success_msg"></div>

					

      <div class="row">

        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

          <?php /* <div class="category-description std">

            <div class="slider-items-products">

              <div id="category-desc-slider" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 

                  

                  <!-- Item -->

                 <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img1.jpg"></a>

                    <div class="inner-info">

                      <div class="cat-img-title"> <span>Best Product 2017</span>

                        <h2 class="cat-heading">Best Selling Brand</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

                        <a class="info" href="#">Shop Now</a> </div>

                    </div>

                  </div>

                  <!-- End Item --> 

                  

                  <!-- Item -->

                  <div class="item"> <a href="#x"><img alt="HTML template" src="images/cat-slider-img2.jpg"></a> </div> 

                  

                  <!-- End Item --> 

                  

                </div>

              </div>

            </div>

          </div> */ ?>

          <div class="shop-inner" id="deals_ajax_display">

            <div class="page-title">

              <h2><?php echo e((Lang::has(Session::get('lang_file').'.DEALS')!= '') ?  trans(Session::get('lang_file').'.DEALS'): trans($OUR_LANGUAGE.'.DEALS')); ?></h2>

            </div>

            <?php if($maincategory_id == ''): ?> 

            <div class="toolbar">              

              <div class="sorter">

                <div class="short-by">

                  <label><?php if(Lang::has(Session::get('lang_file').'.SORT_BY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SORT_BY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SORT_BY')); ?> <?php endif; ?>:</label>

                  <select name="filtertypes" onchange="displayproductrecords('<?php echo $page_limit; ?>','<?php echo $pagenum; ?>',this.options[this.selectedIndex].value);">

                  	<option value=""><?php echo e((Lang::has(Session::get('lang_file').'.SORT_BY')!= '') ? trans(Session::get('lang_file').'.SORT_BY') : trans($OUR_LANGUAGE.'.SORT_BY')); ?></option>

                    <option value="1"><?php if(Lang::has(Session::get('lang_file').'.PRICE_LOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE_LOW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRICE_LOW')); ?> <?php endif; ?> - <?php if(Lang::has(Session::get('lang_file').'.HIGH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HIGH')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HIGH')); ?> <?php endif; ?></option>

                    <option value="2"><?php if(Lang::has(Session::get('lang_file').'.PRICE_HIGH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE_HIGH')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRICE_HIGH')); ?> <?php endif; ?> -<?php if(Lang::has(Session::get('lang_file').'.LOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOW')); ?> <?php endif; ?></option>

                    <option value="3"><?php if(Lang::has(Session::get('lang_file').'.TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TITLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TITLE')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?>-<?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?></option>

                    <option value="4"><?php if(Lang::has(Session::get('lang_file').'.TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TITLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TITLE')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?>-<?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?></option>

                    <option value="5"><?php if(Lang::has(Session::get('lang_file').'.DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DESCRIPTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DESCRIPTION')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?>- <?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?></option>

                    <option value="6"><?php if(Lang::has(Session::get('lang_file').'.DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DESCRIPTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DESCRIPTION')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.Z')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Z')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Z')); ?> <?php endif; ?>- <?php if(Lang::has(Session::get('lang_file').'.A')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.A')); ?>  else <?php echo e(trans($OUR_LANGUAGE.'.A')); ?> <?php endif; ?></option>

                  </select>

                </div>

                <div class="short-by page">

                  <label><?php if(Lang::has(Session::get('lang_file').'.SHOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHOW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHOW')); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.PER_PAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PER_PAGE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PER_PAGE')); ?> <?php endif; ?>:</label>

                 <select name="perpagenumber" onchange="displayproductrecords(this.options[this.selectedIndex].value,'<?php echo $pagenum; ?>','<?php echo $filter; ?>')" >

          					<option value="9">9 <?php echo e((Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE')); ?></option>

          					<option value="18">18 <?php echo e((Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE')); ?></option>

          					<option value="36">36 <?php echo e((Lang::has(Session::get('lang_file').'.PER_PAGE')!= '') ? trans(Session::get('lang_file').'.PER_PAGE') : trans($OUR_LANGUAGE.'.PER_PAGE')); ?></option>

          					<option value="all"><?php if(Lang::has(Session::get('lang_file').'.ALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ALL')); ?> <?php endif; ?> </option>

                  </select>

                </div>

              </div>

            </div>

			<?php endif; ?>

		
			

            <div class="product-grid-area">

              <ul class="products-grid">

			   <?php if(count($product_details) != 0): ?>

				<?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>				 

					

						<!-- //should not show this if datas -->

					<?php if($product_det->deal_no_of_purchase < $product_det->deal_max_limit): ?> 



					<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

					 <?php   $deal_title = 'deal_title'; ?>

					<?php else: ?> <?php   $deal_title = 'deal_title_langCode'; ?> <?php endif; ?>



				 <?php $mostproduct_img = explode('/**/',$product_det->deal_image);

					$product_discount_percentage = $product_det->deal_discount_percentage;

					$date = date('Y-m-d H:i:s');



					$product_image     = $mostproduct_img[0];

				 

					$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

					$img_data   = "public/assets/deals/".$product_image; ?>



					<?php if(file_exists($img_data) && $product_image !=''): ?>  



				  <?php  $prod_path = url('').'/public/assets/deals/' .$product_image; ?>                

					<?php else: ?>  

					<?php if(isset($DynamicNoImage['dealImg'])): ?>

					  <?php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

					   <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

						 <?php   $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

					<?php endif; ?>

					<?php endif; ?>  

				   <!--  /* Image Path ends */   

					//Alt text -->

				 <?php   $alt_text   = substr($product_det->$deal_title,0,25);

					$alt_text  .= strlen($product_det->$deal_title)>25?'..':''; ?>  



			  

                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

					  <?php if($product_det->deal_discount_percentage!='' && round($product_det->deal_discount_percentage)!=0): ?>

                        <div class="icon-sale-label sale-left"><?php echo e(substr($product_det->deal_discount_percentage,0,2)); ?>%</div><?php endif; ?>

					

						<?php 

						$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

						$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));

						$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));

						$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 

						$res = base64_encode($product_det->deal_id);



						$date2 = $product_det->deal_end_date;

						$deal_end_year = date('Y',strtotime($date2));

						$deal_end_month = date('m',strtotime($date2));

						$deal_end_date = date('d',strtotime($date2));

						$deal_end_hours = date('H',strtotime($date2));  

						$deal_end_minutes = date('i',strtotime($date2));    

						$deal_end_seconds = date('s',strtotime($date2));  ?>

					

                        <div class="pr-img-area">

						

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" alt="<?php echo e($alt_text); ?>"  src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>  

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" alt="<?php echo e($alt_text); ?>"  src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">

                          <figure> <img class="first-img" alt="<?php echo e($alt_text); ?>"  src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						<?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

						<a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>">

                          <figure> <img class="first-img" alt="<?php echo e($alt_text); ?>"  src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						

						</div>

                        <div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> 

							

						 <?php $prodInWishlist = DB::table('nm_wishlist')->where('ws_pro_id','=',$product_det->deal_id)->where('ws_cus_id','=',Session::get('customerid'))->first(); ?>

                   

							<?php if(Session::has('customerid')): ?>

								<?php if(count($prodInWishlist)==0): ?>

								<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

								<?php echo e(Form::hidden('pro_id','$product_det->deal_id')); ?>        

					   

								  <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">

								  <a href="" onclick="addtowish(<?php echo e($product_det->deal_id); ?>,<?php echo e(Session::get('customerid')); ?>)" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>" >

								  <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile')); ?>">

									<i class="fa fa-heart-o" aria-hidden="true"></i> 

								  </a>

									<?php else: ?>

									<?php /* remove wishlist */?>   

										

									<a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>" title="<?php if (Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST'); } ?>">

										 <i class="fa fa-heart" aria-hidden="true"></i> 

									</a> 

									<?php /*remove link:remove_wish_product/wishlist table_id*/ ?>

										

								<?php endif; ?>  

						   <?php else: ?> 

							  <a href="" role="button" data-toggle="modal" data-target="#loginpop" title="<?php if (Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= '') { echo trans(Session::get('lang_file').'.ADD_TO_WISHLIST'); } else { echo trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST'); } ?>">

							  

								<i class="fa fa-heart-o" aria-hidden="true"></i> 

							  

							  </a>

						  <?php endif; ?>



							</div>

                            <div class="mt-button quick-view"> <a href="" role="button" data-toggle="modal" data-target="#quick_view_popup-wrap<?php echo e($product_det->deal_id); ?>"> <i class="fa fa-search"></i> </a> </div>

                          </div>

                        </div>

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="" href=""><?php echo e(substr($product_det->$deal_title,0,25)); ?> <?php echo e(strlen($product_det->$deal_title)>25?'..':''); ?> </a> </div>

                          <div class="item-content">

						  

						  

						  <?php

						  

						  $one_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 1)->count();

						  $two_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 2)->count();

						  $three_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 3)->count();

						  $four_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 4)->count();

						  $five_count = DB::table('nm_review')->where('deal_id', '=', $product_det->deal_id)->where('ratings', '=', 5)->count();

						  

						  

						  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

						  $multiple_countone = $one_count *1;

						  $multiple_counttwo = $two_count *2;

						  $multiple_countthree = $three_count *3;

						  $multiple_countfour = $four_count *4;

						  $multiple_countfive = $five_count *5;

						  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

				  

                          <div class="rating">

						<?php if($product_count): ?>

							 <?php   $product_divide_count = $product_total_count / $product_count; ?>

							 <?php if($product_divide_count <= '1'): ?> 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 <?php elseif($product_divide_count >= '1'): ?> 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 <?php elseif($product_divide_count >= '2'): ?> 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

							 <?php elseif($product_divide_count >= '3'): ?> 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

							 <?php elseif($product_divide_count >= '4'): ?> 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>

							 <?php elseif($product_divide_count >= '5'): ?> 

							 

							 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>

							 <?php else: ?>

								

							<?php endif; ?>

						<?php else: ?>

						   <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					    <?php endif; ?>	

					  </div>

							

							

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?>


                                       <?php echo e($product_det->deal_discount_price); ?></span> </span> </div>

                            </div>

                            <div class="pro-action">

							   <?php if($date >= $product_det->deal_end_date): ?>  

								   <button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span> </button>

                                <?php else: ?>          

									<?php $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));

									 $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));

									 $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));

									 $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name)); 

									 $res = base64_encode($product_det->deal_id); ?>

							

								<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

								<a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

								<?php endif; ?>

							   

							    <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

							    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							    <?php endif; ?>

								

								<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

							    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							    <?php endif; ?>

								

								<?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>  

							    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							    <?php endif; ?>

								

						<script>

						$(function(){

							//year = <?php echo $deal_end_year;?>; month = <?php echo $deal_end_month;?>; day = <?php echo $deal_end_date;?>;hour= <?php echo $deal_end_hours;?>; min= <?php echo $deal_end_minutes;?>; sec= <?php echo $deal_end_seconds;?>;

						   

						countProcess(<?php echo $product_det->deal_id; ?>);

						});

						</script>

				            <?php endif; ?>

							

							

								

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </li>

				<?php endif; ?>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					  <?php else: ?> <center><div class="list box text-shadow"><?php echo e((Lang::has(Session::get('lang_file').'.NO_RESULTS_FOUND')!= '') ? trans(Session::get('lang_file').'.NO_RESULTS_FOUND') : trans($OUR_LANGUAGE.'.NO_RESULTS_FOUND')); ?></div></center>

				<?php endif; ?>

              </ul>

            </div>

			

            <?php if($maincategory_id==''): ?>

            <div class="pagination-area">			

              <?php

              if ( ($pagenum-1) > 0) 

              {

                $prpre =$pagenum-1;

              ?>

              <div class="" >

                <button style="vertical-align: top; margin-top: 0px;" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo 1; ?>','<?php echo $filter;  ?>');" data-type="first">«</button>

                <button style="vertical-align: top; margin-top: 0px;" data-number="0" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $pagenum-1; ?>','<?php echo $filter;  ?>');">‹</button> 

              </div>

              <?php

              } 

              ?>

              <span  class="">

              <?php

              $links=$pagenum+4; 

              for($i=$pagenum; $i<=$links; $i++)

              {

                if($i<=$last)

                {

                  if ($i == $pagenum ) 

                  {

                  ?>

                    <button style="vertical-align: top; margin-top: 0px;" type="button" class="active" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $i; ?>','<?php echo $filter;  ?>');" ><?php echo $i; ?></button> 

                  <?php

                  }

                  else

                  {

                  ?>

                    <button style="vertical-align: top; margin-top: 0px;" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $i; ?>','<?php echo $filter;  ?>');" ><?php echo $i; ?></button>  

                  <?php

                  }

                }

              }

              ?>  

              </span>

              

              <span class="pagina-nav" >

              <?php 

              if ( ($pagenum+1) <= $last)

              {

              ?>

                <button style="vertical-align: top;" data-number="1" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $pagenum+1; ?>','<?php echo $filter;  ?>');" >›</button>

                <button style="vertical-align: top;" data-number="3" type="button" class="" onclick="displayproductrecords('<?php echo $page_limit;  ?>','<?php echo $last; ?>','<?php echo $filter;  ?>');" >»</button>

              <?php

              }

              ?>

              </span>

            </div>

            <?php endif; ?>

          </div>







        </div>

        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

          

          <div class="block shop-by-side">

            <div class="sidebar-bar-title">

              <h3><?php echo e((Lang::has(Session::get('lang_file').'.SHOP_BY')!= '') ?  trans(Session::get('lang_file').'.SHOP_BY'): trans($OUR_LANGUAGE.'.SHOP_BY')); ?></h3>

            </div>

            <div class="block-content">

              <p class="block-subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.CATEGORIES')!= '') ?  trans(Session::get('lang_file').'.CATEGORIES'): trans($OUR_LANGUAGE.'.CATEGORIES')); ?></p>

               <?php if(count($main_category)!=0): ?>

				<div id="divMenu">

					<ul>

					 <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

						<?php $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>		

						<?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0): ?> 						

						<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id1)); ?>">

							<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

							<?php	$mc_name = 'mc_name'; ?>

							<?php else: ?>  <?php $mc_name = 'mc_name_code'; ?> <?php endif; ?>

							<?php echo e($fetch_main_cat->$mc_name); ?></a> 

						   <ul>

							<?php $__currentLoopData = $sub_main_category[$fetch_main_cat->mc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

								<?php $check_sub_deal_exists = DB::table('nm_deals')->where('deal_main_category', '=', $fetch_sub_main_cat->smc_id)->where('deal_status','=',1)->whereRaw('deal_no_of_purchase < deal_max_limit')->get(); ?>

								<?php if($check_sub_deal_exists): ?>

										

								<?php $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>

								<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id2)); ?>">

								 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

										<?php	$smc_name = 'smc_name'; ?>

										<?php else: ?> <?php  $smc_name = 'smc_name_code'; ?> <?php endif; ?>

										<?php echo e($fetch_sub_main_cat->$smc_name); ?> </a>

										

										 <?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0): ?> 

									 <ul>

									  <?php $__currentLoopData = $second_main_category[$fetch_sub_main_cat->smc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

													 

										<?php 

										$check_sec_main_deal_exists = DB::table('nm_deals')->where('deal_sub_category', '=', $fetch_sub_cat->sb_id)->where('deal_status','=',1)->whereRaw('deal_no_of_purchase < deal_max_limit')->get(); 

										?>

										<?php if($check_sec_main_deal_exists): ?>	

									    <?php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; ?>

										<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id3)); ?>">

										<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

										<?php	$sb_name = 'sb_name'; ?>

										<?php else: ?> <?php  $sb_name = 'sb_name_langCode'; ?> <?php endif; ?>

										<?php echo e($fetch_sub_cat->$sb_name); ?></a>

										

										 <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0): ?> 

											<ul>	

												<?php $__currentLoopData = $second_sub_main_category[$fetch_sub_cat->sb_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_secsub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

																

													<?php $check_sec_sub_main_deal_exists = DB::table('nm_deals')->where('deal_second_sub_category', '=', $fetch_secsub_cat->ssb_id)->where('deal_status','=',1)->whereRaw('deal_no_of_purchase < deal_max_limit')->get(); ?>

													<?php if($check_sec_sub_main_deal_exists): ?>	

													<?php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>

												<li><a href="<?php echo e(url('catdeals/viewcategorylist')."/".base64_encode($pass_cat_id4)); ?>">

												<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

													<?php $ssb_name = 'ssb_name'; ?>

														<?php else: ?> <?php  $ssb_name = 'ssb_name_langCode'; ?> <?php endif; ?>

														<?php echo e($fetch_secsub_cat->$ssb_name); ?></a>

												</li>

												 <?php endif; ?>

													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

											</ul>

											 <?php endif; ?>

										</li>

										<?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									</ul>

									 <?php endif; ?>

								</li>

								 <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						   </ul>

						   <?php endif; ?>

						</li>

						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					</ul>

					<?php else: ?> 

						<ul> <li><?php if(Lang::has(Session::get('lang_file').'.NO_CATEGORY_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_CATEGORY_FOUND')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_CATEGORY_FOUND')); ?>! <?php endif; ?> </li></ul> <?php endif; ?>

				</div>

              

            </div>

          </div>

		  

		  <?php $deals_avail = DB::table('nm_deals')->where('deal_status','=','1')->count(); ?>

		  <?php if(Request::segment(1)=="catdeals"): ?>

           <?php if($deals_avail>0): ?>

		   <div class="block product-price-range ">

            <div class="sidebar-bar-title">

              <h3><?php if(Lang::has(Session::get('lang_file').'.DISCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DISCOUNT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DISCOUNT')); ?>! <?php endif; ?></h3>

            </div>

			

			<?php	$discount_filter=array();

				$filter_discount="";

				$price_from="";

				$price_to=""; ?>

				<?php if(isset($_GET["filter_discount"])): ?>

				

				<?php	$filter_discount=base64_decode($_GET["filter_discount"]);

					$discount_filter=explode(",",$filter_discount); ?>

				<?php endif; ?>

				<?php if(isset($_GET["price_from"])): ?>

				

					<?php $price_from=$_GET["price_from"];

					$price_to=$_GET["price_to"]; ?>

				<?php endif; ?>

				

            <div class="block-content">

              <div class="slider-range">

                <ul class="check-box-list">

					<?php

					$label="";

					for($i=1;$i<=6;$i++)

					{

						if($i==1)

							$label=((Lang::has(Session::get('lang_file').'.UPTO')!= '') ? trans(Session::get('lang_file').'.UPTO') : trans($OUR_LANGUAGE.'.UPTO'))." 10%";

						if($i==2)

							$label="10% - 20%";

						if($i==3)

							$label="20% - 30%";

						if($i==4)

							$label="30% - 40%";

						if($i==5)

							$label="40% - 50%";

						if($i==6)

							$label="50% ".((Lang::has(Session::get('lang_file').'.AND_ABOVE')!= '') ? trans(Session::get('lang_file').'.AND_ABOVE') : trans($OUR_LANGUAGE.'.AND_ABOVE'));

					?>

                  <li>

                    <input type="radio" name="discount_filter" value="<?php echo $i; ?>" <?php if(in_array($i,$discount_filter)){echo "checked";} ?> onclick="javascript:make_filter()"> 

                    <label for="p1"> <span class="button"></span><?php echo e($label); ?></label>

                  </li>

				  <?php	} ?>

                </ul>

				

				<?php if(isset($_GET["filter_discount"]) AND $_GET["filter_discount"]!=""): ?>

				<button type="button" onclick="javascript:clear_filter('filter_for_values_discount')" class="button button-clear"> <span><?php if(Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CLEAR_ALL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CLEAR_ALL')); ?> <?php endif; ?></span></button>

			    <?php endif; ?> 

		

              </div>

            </div>

          </div>

		  

		  

         <div class="block product-price-range ">

		   <div class="sidebar-bar-title">

              <h3><?php echo e((Lang::has(Session::get('lang_file').'.PRICE_FILTER')!= '') ?  trans(Session::get('lang_file').'.PRICE_FILTER') : trans($OUR_LANGUAGE.'.PRICE_FILTER')); ?></h3>

           </div>

			 <div class="block-content">

			   <div class="slider-range">

					<form name="filter_form_to_generate" id="filter_form" method="get">

					<?php echo e(Form::hidden('filter_discount','$filter_discount',array('id'=>'filter_for_values_discount'))); ?>


					

					<ul>

					 <li class="pri-filter-input">

            <div class="pri-filter-sec">

          <span style="color:#e83f33;"><?php echo e((Lang::has(Session::get('lang_file').'.MIN')!= '') ? trans(Session::get('lang_file').'.MIN') : trans($OUR_LANGUAGE.'.MIN')); ?></span>

          <input type="text"  placeholder="100" name="price_from" id="price_from" value="<?php echo $price_from; ?>" style="width:75px"/></div>

          <div class="pri-filter-sec">

					<span style="color:#e83f33;"><?php echo e((Lang::has(Session::get('lang_file').'.MAX')!= '') ? trans(Session::get('lang_file').'.MAX') : trans($OUR_LANGUAGE.'.MAX')); ?></span><input type="text" placeholder="10000" name="price_to" id="price_to" value="<?php echo e($price_to); ?>" style="width:75px"/> </div>

					<div class="pri-filter-sec"><button type="button" onclick="javacript:make_filter();" class="button button-compare"> <span><?php echo e((Lang::has(Session::get('lang_file').'.GO')!= '') ? trans(Session::get('lang_file').'.GO') : trans($OUR_LANGUAGE.'.GO')); ?></span></button></div>

					 </li>

					</ul>

					<br>

					<?php if($price_from !="" && $price_to !=""): ?> <!-- //Clear all for discount price -->

					    <button type="button" onclick="javascript:clear_discount_filter('price_from','price_to')" class="button button-clear"> <span><?php if(Lang::has(Session::get('lang_file').'.CLEAR_ALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CLEAR_ALL')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CLEAR_ALL')); ?> <?php endif; ?></span></button>

					<?php endif; ?>

					<!--<input type="hidden" name="from" value="<?php // echo Request::segment(3); ?>">-->

				</form>

			  </div>

			</div>

		</div>

		<?php endif; ?>

		<?php endif; ?>	

        

		

		<?php if(count($get_special_product)!=0): ?>

			<?php $date = date('Y-m-d H:i:s'); ?>

          <div class="block special-product">

            <div class="sidebar-bar-title">

              <h3><?php if(Lang::has(Session::get('lang_file').'.MOST_VISITED_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MOST_VISITED_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MOST_VISITED_DEALS')); ?> <?php endif; ?></h3>

            </div>

            <div class="block-content">

			 <ul>

				<?php $__currentLoopData = $get_special_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   

				

				<?php if($fetch_most_visit_pro->deal_no_of_purchase < $fetch_most_visit_pro->deal_max_limit): ?>
				<?php

				$mostproduct_discount_percentage = $fetch_most_visit_pro->deal_discount_percentage;

				$mostproduct_img = explode('/**/', $fetch_most_visit_pro->deal_image);

				$mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				$smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				$sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				$ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

				$res = base64_encode($fetch_most_visit_pro->deal_id); ?>

			   

				<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				   <?php  $title = 'deal_title'; ?>

				<?php else: ?>  <?php  $title = 'deal_title_langCode'; ?> <?php endif; ?>



				<?php  $product_image     = $mostproduct_img[0];

				 

				$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				$img_data   = "public/assets/deals/".$product_image; ?>

				

				<?php if(file_exists($img_data) && $product_image !=''): ?>   

				

			   <?php $prod_path = url('').'/public/assets/deals/' .$product_image;   ?>                

				<?php else: ?>  

					<?php if(isset($DynamicNoImage['dealImg'])): ?>

					<?php    $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

					   <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

						 <?php   $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

					<?php endif; ?>

				<?php endif; ?>   

			   <!--  /* Image Path ends */   

				//Alt text -->

				<?php   $alt_text   = substr($fetch_most_visit_pro->$title,0,25);

				$alt_text  .= strlen($fetch_most_visit_pro->$title)>25?'..':'';    ?>

			

                <li class="item">

                  <div class="products-block-left">

				  <?php /* @if($fetch_most_visit_pro->deal_discount_percentage!='' && round($fetch_most_visit_pro->deal_discount_percentage)!=0)		

                  <div class="icon-sale-label sale-left">{{ substr($fetch_most_visit_pro->deal_discount_percentage,0,2) }}%</div>@endif */ ?>



				   <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  

				   <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a>

			       <?php endif; ?>

				  

				  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

				   <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a>

			      <?php endif; ?>

				  

				 <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

				   <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a>

			      <?php endif; ?>

				  

				   <?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

				   <a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>" class="product-image"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a>

			       <?php endif; ?>

				  

				  </div>

                  <div class="products-block-right">

                    <p class="product-name"> <a><?php echo e(substr($fetch_most_visit_pro->$title,0,25)); ?>    <?php echo e(strlen($fetch_most_visit_pro->$title)>25?'..':''); ?></a> </p>

                    <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_most_visit_pro->deal_discount_price); ?></span>

                   

					

				  <?php					  

				  $one_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 1)->count();

				  $two_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 2)->count();

				  $three_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 3)->count();

				  $four_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 4)->count();

				  $five_count = DB::table('nm_review')->where('deal_id', '=', $fetch_most_visit_pro->deal_id)->where('ratings', '=', 5)->count();

				  

				  

				  $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

				  $multiple_countone = $one_count *1;

				  $multiple_counttwo = $two_count *2;

				  $multiple_countthree = $three_count *3;

				  $multiple_countfour = $four_count *4;

				  $multiple_countfive = $five_count *5;

				  $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

					

					

				<div class="rating">

				<?php if($product_count): ?>

					 <?php   $product_divide_count = $product_total_count / $product_count; ?>

					 <?php if($product_divide_count <= '1'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '1'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '2'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>  

					 <?php elseif($product_divide_count >= '3'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '4'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

					 <?php elseif($product_divide_count >= '5'): ?> 

					 

					 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i>

					 <?php else: ?>

						

					<?php endif; ?>

				<?php else: ?>

				     <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>

				<?php endif; ?>	

			  </div>

			    <br>

				

					<?php if($date > $fetch_most_visit_pro->deal_end_date): ?>

				    <a class="link-all"><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></a>

				    <?php else: ?>

						

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  

				    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" class="link-all"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

				    <?php endif; ?>

					

					<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

				    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" class="link-all"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

				    <?php endif; ?>

					

					<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

				    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" class="link-all"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

				    <?php endif; ?>

					

					<?php if($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?>

				    <a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>" class="link-all"><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></a>

				    <?php endif; ?>

				

				<?php endif; ?>

				

                  </div>

                </li>

				 <?php endif; ?> 

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

              </ul>

			  <?php endif; ?>

              </div>

          </div>

          

        </aside>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  

  <!--popup deal quick view-->

			<?php if(count($quickview_details) != 0): ?> 

				<?php $__currentLoopData = $quickview_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

						<!-- //should not show this if datas -->

					<?php if($product_det->deal_no_of_purchase < $product_det->deal_max_limit): ?>



					<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

					 <?php   $deal_title = 'deal_title'; ?>

					<?php else: ?> <?php   $deal_title = 'deal_title_langCode'; ?> <?php endif; ?>

					

					<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

					 <?php   $deal_description = 'deal_description'; ?>

					<?php else: ?> <?php   $deal_description = 'deal_description_langCode'; ?> <?php endif; ?>

					

					

				 <?php $count = $product_det->deal_max_limit - $product_det->deal_no_of_purchase; ?>

				 

				  <?php   $product_img= explode('/**/',trim($product_det->deal_image,"/**/")); 

					$img_count = count($product_img); ?>

			

				 <?php $mostproduct_img = explode('/**/',$product_det->deal_image);

				    

					$product_discount_percentage = $product_det->deal_discount_percentage;

					$date = date('Y-m-d H:i:s');



					$product_image     = $mostproduct_img[0];

				 

					$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

					$img_data   = "public/assets/deals/".$product_image; ?>



					<?php if(file_exists($img_data) && $product_image !=''): ?>  



				  <?php  $prod_path = url('').'/public/assets/deals/' .$product_image; ?>                

					<?php else: ?>  

					<?php if(isset($DynamicNoImage['dealImg'])): ?>

					  <?php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

					   <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

						 <?php   $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

					<?php endif; ?>

					<?php endif; ?>   

				   <!--  /* Image Path ends */   

					//Alt text -->

				 <?php   $alt_text   = substr($product_det->$deal_title,0,25);

					$alt_text  .= strlen($product_det->$deal_title)>25?'..':''; ?> 
					

				 <input type="hidden" id="pro_qty_hidden_<?php echo e($product_det->deal_id); ?>" name="pro_qty_hidden" value="<?php echo  $product_det->deal_max_limit; ?>" />

				 <input type="hidden" id="pro_purchase_hidden_<?php echo e($product_det->deal_id); ?>" name="pro_purchase_hidden" value="<?php echo  $product_det->deal_no_of_purchase; ?>" />

					

					<div style="display: none;"  class="quick_view_popup-wrap deal-view-full" id="quick_view_popup-wrap<?php echo e($product_det->deal_id); ?>">

					<div id="quick_view_popup-overlay"></div>

					  <div id="quick_view_popup-outer">

						<div id="quick_view_popup-content">

						  <div style="width:auto;height:auto;overflow: auto;position:relative;">

							<div class="product-view-area">

							  <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">							

								<div class="large-image">

								<a href="<?php echo e($prod_path); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="" src="<?php echo e($prod_path); ?>"> 

								</a>


								</div>
								

								<div class="flexslider flexslider-thumb">

								  <ul class="previews-list slides">

								  

								   <?php for($i=0;$i < $img_count;$i++): ?>  

								   <?php $product_image     = $mostproduct_img[$i];

								   $prod_path  = url('').'/public/assets/default_image/No_image_deal.png';

								   $img_data   = "public/assets/deals/".$product_image; ?>

								   <?php if(file_exists($img_data) && $product_image !=''): ?>   

								   <?php $prod_path = url('').'/public/assets/deals/' .$product_image; ?>                

								   <?php else: ?>  

								   <?php if(isset($DynamicNoImage['dealImg'])): ?>

								   <?php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

								   <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

								   <?php    $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

								   <?php endif; ?>

								   <?php endif; ?> 

				   

									<li style="width: 95px; float: left; display: block;"><a href='<?php echo e($prod_path); ?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo e($prod_path); ?>' "><img src="<?php echo e($prod_path); ?>" alt = "Thumbnail 2"/></a></li>

									

									<?php endfor; ?>

								  </ul>

								</div>
								

								<!-- end: more-images --> 
							  </div>

							  <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">

								<div class="product-details-area">

								  <div class="product-name">

									<h1><?php echo e(substr($product_det->$deal_title,0,25)); ?> <?php echo e(strlen($product_det->$deal_title)>25?'..':''); ?> </h1>

								  </div>

								  <div class="price-box">

									<p class="special-price"> <span class="price-label"></span> <span class="price"> <?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->deal_discount_price); ?> </span> </p>

									<p class="old-price"> <span class="price-label"></span> <span class="price"> <?php echo e(Helper::cur_sym()); ?>  <?php echo e($product_det->deal_original_price); ?></span> </p>

								  </div>

								  <div class="ratings">

									<div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>

									<p class="availability in-stock pull-right"><?php if(Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AVAILABLE_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AVAILABLE_STOCK')); ?> <?php endif; ?> :  <span><?php echo e(($product_det->deal_max_limit)-($product_det->deal_no_of_purchase)); ?> <?php if(Lang::has(Session::get('lang_file').'.IN_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.IN_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.IN_STOCK')); ?> <?php endif; ?></span></p>

								  </div>

								  <div class="short-description">

									<h2><?php if(Lang::has(Session::get('lang_file').'.OVERVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OVERVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OVERVIEW')); ?> <?php endif; ?></h2>

										<p><?php echo e($product_det->$deal_description); ?></p> 

								  </div>

								  <div class="product-color-size-area">

									<div class="color-area">

									  <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.DEAL_ENDS_IN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DEAL_ENDS_IN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DEAL_ENDS_IN')); ?> <?php endif; ?></h2>

									  <div class="color">

										<p id="demo" class="stock-list"></p>

									  </div>

									</div>

								  </div>

								  <div class="product-variation">

								   <?php echo Form::open(array('url' => 'addtocart_deal','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')); ?>


									<form action="#" method="post">

									  <div class="cart-plus-minus">

										<label for="qty"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?>:</label>

										<div class="numbers-row">

										  <div onClick="remove_quantity('<?php echo e($product_det->deal_id); ?>')" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>

										  <input type="number" class="qty" min="1" value="1" max="<?php echo e(($product_det->deal_max_limit - $product_det->deal_no_of_purchase)); ?>" id="addtocart_qty_<?php echo e($product_det->deal_id); ?>" name="addtocart_qty" readonly required >

										  <?php echo e(Form::hidden('addtocart_deal_id',$product_det->deal_id)); ?>


										  <?php echo e(Form::hidden('addtocart_type','deals')); ?>


										  

										<div onClick="add_quantity('<?php echo e($product_det->deal_id); ?>')" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>

										

										</div>

									  </div>

									  

									  <input type="hidden" name="return_url" value="<?php //echo $product_det->mc_name.'/'.base64_encode(base64_encode(base64_encode($product_det->deal_category))); ?>" />

									  

									  <?php if(Session::has('customerid')): ?> 

									   <?php if($count > 0): ?>



								  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i>  <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span></button>

								  <?php else: ?> 

								  <button class="button pro-add-to-cart" title="Add to Cart" ><span>  <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?></span></button>

								  <?php endif; ?>

								  <?php else: ?>

								  <a href="" role="button" data-toggle="modal" data-target="#loginpop">

								  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i>  <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span></button></a>

								  <?php endif; ?>

								  <input type="hidden" id="enddate" name="enddate" value="<?php echo e($product_det->deal_end_date); ?>">

								  <input type="hidden" id="enddate_format" name="enddate_format" value="<?php echo date('F j, Y, G:i:s',strtotime($product_det->deal_end_date)); ?>">

							   <?php echo Form::close(); ?>


									 

								  </div>

								  

								 

								

								</div>

							  </div>

							</div>

							<!--product-view--> 

							

						  </div>

						</div>

						<a style="display: inline;" id="quick_view_popup-close" href="<?php echo e(url('')); ?>/deals"><i class="icon pe-7s-close"></i></a> </div>

					</div>

					

					<?php endif; ?>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				

				<?php endif; ?>


  <!--end popup deal quick view-->

  

<?php echo $footer; ?>




<script type="text/javascript">



    $(document).ready(function() {

        $(document).on("click", ".customCategories .topfirst b", function() {

            $(this).next("ul").css("position", "relative");



            $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

            $(this).next("ul").toggle();

        });



        $(document).on("click", ".morePage", function() {

            $(".nextPage").slideToggle(200);

        });



        $(document).on("click", "#smallScreen", function() {

            $(this).toggleClass("customMenu");

        });



        $(window).scroll(function() {

            if ($(this).scrollTop() > 250) {

                $('#comp_myprod').show();

            } else {

                $('#comp_myprod').hide();

            }

        });

		

		var price_from = $('#price_from');

		var price_to = $('#price_to');

		

		 $('#price_from').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            alert("<?php echo e((Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') ? trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED') : trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED')); ?>");

			return false;

        }

      });



	  $('#price_to').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            alert("<?php echo e((Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') ? trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED') : trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED')); ?>");

			return false;

        }

      });



    });

</script>

      

 <!-- Count Down Coding -->

<script type="text/javascript">

<?php if(count($product_details) != 0): ?>

function countProcess(deal_id){

    <?php 

foreach($product_details as $product_det){?>

var pro_deal_id = <?php echo $product_det->deal_id;?>;



if(deal_id==pro_deal_id){



<?php 

  $date2 = $product_det->deal_end_date;

  $deal_end_year = date('Y',strtotime($date2));

  $deal_end_month = date('m',strtotime($date2));

  $deal_end_date = date('d',strtotime($date2));

  $deal_end_hours = date('H',strtotime($date2));  

  $deal_end_minutes = date('i',strtotime($date2));    

  $deal_end_seconds = date('s',strtotime($date2)); 

?>

year = <?php echo $deal_end_year;?>; month = <?php echo $deal_end_month;?>; day = <?php echo $deal_end_date;?>;hour= <?php echo $deal_end_hours;?>; min= <?php echo $deal_end_minutes;?>; sec= <?php echo $deal_end_seconds;?>;

}

<?php 

}//foreach

?>



var timezone = new Date()

month        = --month;

dateFuture   = new Date(year,month,day,hour,min,sec);

//alert(deal_id);

        dateNow = new Date();                                                            

        amount  = dateFuture.getTime() - dateNow.getTime()+5;               

        delete dateNow;



        /* time is already past */

        //if(amount < 0){

                //output ="<span class='countDays'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='countDiv countDiv0'></span><span class='countHours'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span><span class='countDiv countDiv1'></span><span class='countMinutes'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span><span class='countDiv countDiv2'></span><span class='countSeconds'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span>" ;

                

 

        /* date is still good */

        //else{

                days=0; hours=0; mins=0; secs=0; output="";



                amount = Math.floor(amount/1000); /* kill the milliseconds */



                days   = Math.floor(amount/86400); /* days */

                amount = amount%86400;



                hours  = Math.floor(amount/3600); /* hours */

                amount = amount%3600;



                mins   = Math.floor(amount/60); /* minutes */

                amount = amount%60;



                secs   = Math.floor(amount); /* seconds */



				fdays = parseInt(days/10);	

				sdays = days%10;

				fhours = parseInt(hours/10);	

				shours = hours%10;

				fmins = parseInt(mins/10);	

				smins = mins%10;

				fsecs = parseInt(secs/10);	

				ssecs = secs%10;

               output="<span class='countDays'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>" + fdays +"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>" + sdays +"</span></span><span class='countDiv countDiv0'></span><span class='countHours'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fhours+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+shours+"</span></span></span><span class='countDiv countDiv1'></span><span class='countMinutes'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fmins+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+smins+"</span></span></span><span class='countDiv countDiv2'></span><span class='countSeconds'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fsecs+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+ssecs+"</span></span></span>" ;

                     
                $('#countdown'+deal_id).html(output);

            

           //alert(sdays); 



                setTimeout(function() { countProcess(deal_id); }, 1000);

        //}

        

}

 <?php endif; ?> 

function make_filter()

{

	var filter_items_by_discount=[];

	var price_from=$("#price_from").val();

	var price_to=$("#price_to").val();

	var price_from=parseInt(price_from);

	var price_to=parseInt(price_to);

	var price_radio=$("input[name='radio_price_filter']:checked").val();

	//alert(price_to);

	if(price_radio)

	{

		var min_max_price=price_radio.split('-');

		$("#price_from").val(min_max_price[0]);

		$("#price_to").val(min_max_price[1]);

		

	}

	

	$("input:radio[name=discount_filter]:checked").each(function(){

		filter_items_by_discount.push($(this).val());

	});

	//alert(price_to);

	var enc_filter_by_discount = window.btoa(filter_items_by_discount);

	$("#filter_for_values_discount").val(enc_filter_by_discount);

	if(price_from > price_to)

	{

		alert("<?php echo e((Lang::has(Session::get('lang_file').'.MAX_PRICE_SHOULD_LESS_MIN')!= '') ? trans(Session::get('lang_file').'.MAX_PRICE_SHOULD_LESS_MIN') : trans($OUR_LANGUAGE.'.MAX_PRICE_SHOULD_LESS_MIN')); ?>");

		$("#price_to").focus();

		return false;

	}

	

   document.filter_form_to_generate.submit();             // Submit the page

    

}

function clear_filter(id_to_clear)

{

	$("#"+id_to_clear).val("");

	document.filter_form_to_generate.submit();

}

/*clear filter for discount price*/

function clear_discount_filter(from_price,to_price)

{

	$("#"+from_price).val("");

	$("#"+to_price).val("");

	document.filter_form_to_generate.submit();

}

</script>



 <script type="text/javascript">

  function addtowish(pro_id,cus_id){

    var wishlisturl = document.getElementById('wishlisturl').value;



    $.ajax({

          type: "get",   

          url:"<?php echo url('addtowish'); ?>",

          data:{'pro_id':pro_id,'cus_id':cus_id},

          success:function(response){

            //alert(response);

            if(response==0){

              $('#success_msg').html('<p>Added to wishlist</p>')

            <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>

                        $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');

              //window.location=wishlisturl;

                            window.location.reload();

                            

            }else{

              alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');

              //window.location=wishlisturl;

            }

          }

        });

  }

</script>



  <script>

function add_quantity(id)

{

  //alert();

  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/

  var quantity=$("#addtocart_qty_"+id).val();

  var pro_qty_hidden=$("#pro_qty_hidden_"+id).val();

  var pro_purchase_hidden=$("#pro_purchase_hidden_"+id).val();

  var remaining_product=parseInt(pro_qty_hidden - pro_purchase_hidden);

  if(quantity<remaining_product)

  {

    var new_quantity=parseInt(quantity)+1;

    $("#addtocart_qty_"+id).val(new_quantity);

  }

  //alert();

}



function remove_quantity(id)

{

  //alert();

  /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/



  var quantity=$("#addtocart_qty_"+id).val();

  var quantity=parseInt(quantity);

  if(quantity>1)

  {

    var new_quantity=quantity-1;

    $("#addtocart_qty_"+id).val(new_quantity);

  }

  //alert();

}

</script>







<script>

// Set the date we're counting down to



var getenddate = document.getElementById("enddate").value;

var getenddate_format = document.getElementById("enddate_format").value;

var countDownDate = new Date(getenddate_format).getTime();



// Update the count down every 1 second

var x = setInterval(function() {



  // Get todays date and time

  var now = new Date().getTime();



  // Find the distance between now an the count down date

  var distance = countDownDate - now;



  // Time calculations for days, hours, minutes and seconds

  var days = Math.floor(distance / (1000 * 60 * 60 * 24));

  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

  var seconds = Math.floor((distance % (1000 * 60)) / 1000);



  // Display the result in the element with id="demo"

  document.getElementById("demo").innerHTML = days + "<span style='color:#e74c3c;'>d</span> -" + hours + "<span style='color:#e74c3c;'>h </span>-"

  + minutes + "<span style='color:#e74c3c;'>m</span> -" + seconds + "<span style='color:#e74c3c;'>s</span> ";



  // If the count down is finished, write some text 

  if (distance < 0) {

    clearInterval(x);

    document.getElementById("demo").innerHTML = "EXPIRED";

  }

}, 1000);

</script>

<script type="text/javascript"> 

     function displayproductrecords(numRecords,pageNum,filter)

     {  

        var path = '<?php echo url('deals_ajax_pagination'); ?>';

        $.ajax({

                type: "GET",

                url: path,

                data: "show=" + numRecords + "&pagenum=" + pageNum + "&filter=" + filter,

                cache: false,

                datatype: "html",

                success: function(result) {

                    $("#deals_ajax_display").html(result);

                }

        });

       

    }

</script>







</body>



</html>

