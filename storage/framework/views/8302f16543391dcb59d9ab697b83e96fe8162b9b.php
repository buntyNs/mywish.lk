<?php /* <body style="overflow-x: hidden;"> */ ?>
<?php echo $navbar; ?>

<!-- Navbar ================================================== -->
<?php echo $header; ?>

<!-- Header End====================================================================== -->
<div class="span12">
  <div class="breadcrumbs">
    <div class="container"> 
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li>
            <li><strong><?php if(Lang::has(Session::get('lang_file').'.BLOG')!= ''): ?>  <?php echo e(trans(Session::get('lang_file').'.BLOG')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BLOG')); ?>  <?php endif; ?></strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  </div>
<section class="blog_post">
    <div class="container"> 
      
      <!-- Center colunm-->
      <div class="blog-wrapper">
        <div class="page-title">
          <h2><?php echo e((Lang::has(Session::get('lang_file').'.BLOG_POST')!= '') ? trans(Session::get('lang_file').'.BLOG_POST') : trans($OUR_LANGUAGE.'.BLOG_POST')); ?></h2>
        </div>

        <ul class="blog-posts">

        <?php if(count($get_blog_list) > 0): ?>
        	<?php $__currentLoopData = $get_blog_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetchblog_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	        	<li class="post-item col-md-4">
	            <div class="blog-box">
	            	<?php  $product_image     = $fetchblog_list->blog_image;             
            		$prod_path  = url('').'/public/assets/default_image/No_image_blog.png';
            		$img_data   = "public/assets/blogimage/".$product_image; ?>            
           			<?php if(file_exists($img_data) && $product_image!='' ): ?>
           				<?php   $prod_path = url('public/assets/blogimage/').'/'.$product_image; ?>                 
            		<?php else: ?>  
                		<?php if(isset($DynamicNoImage['blog'])): ?>                
                 			<?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['blog']; ?>
                   			<?php if($DynamicNoImage['blog']!='' && file_exists($dyanamicNoImg_path)): ?> 
                      		<?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?> 
                      		<?php endif; ?>
                		<?php endif; ?>
            		<?php endif; ?> 
            		<img class="primary-img" width="320" height="190" src="<?php echo e($prod_path); ?>" alt="<?php echo e($fetchblog_list->blog_title); ?>">
	                <div class="blog-btm-desc">
	                	<div class="blog-top-desc">
	                    	<div class="blog-date" style="height: auto;">
	                    		<?php   $created_date =  $fetchblog_list->blog_created_date;
										$explode_date = explode(" ",$created_date);
										echo  date('jS \of M Y', strtotime($explode_date[0])); ?>
							</div>
	                    	<h4>
	                    		<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
								<?php	$blog_title = 'blog_title'; ?>
								<?php else: ?> <?php  $blog_title = 'blog_title_'.Session::get('lang_code'); ?> <?php endif; ?>
								<?php echo e(substr($fetchblog_list->$blog_title,0,20)); ?>

							</a></h4>
	                    	<div class="jtv-entry-meta"> 
	                    		<i class="fa fa-user-o"></i> <strong><?php if(Lang::has(Session::get('lang_file').'.POSTED_BY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.POSTED_BY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.POSTED_BY')); ?> <?php endif; ?>  <a><?php if(Lang::has(Session::get('lang_file').'.ADMIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADMIN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADMIN')); ?> <?php endif; ?></a></strong> <a href="<?php echo e(url('blog_comment/'. $fetchblog_list->blog_id)); ?>"><i class="fa fa-commenting-o"></i> <strong><?php echo e($get_blog_list_count[$fetchblog_list->blog_id]); ?> 
   								<?php if(Lang::has(Session::get('lang_file').'.COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COMMENTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COMMENTS')); ?> <?php endif; ?></strong></a>
   							</div>
	                  	</div>
	                  	<p> <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
						   		<?php  $blog_desc = 'blog_desc'; ?>
						 	<?php else: ?> <?php  $blog_desc = 'blog_desc_'.Session::get('lang_code'); ?> 
						 	<?php endif; ?>
   							<?php echo e(substr($fetchblog_list->$blog_desc,0,10)); ?><b>....</b>
   						</p>
	                  	<a class="read-more" href="<?php echo e(url('blog_view/'.$fetchblog_list->blog_id)); ?>">
	                  		<?php if(Lang::has(Session::get('lang_file').'.CONTINUE_READING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONTINUE_READING')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONTINUE_READING')); ?> <?php endif; ?>
	                  	</a> 
	                </div>
	            </div>
	            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
        <?php else: ?>
        	<h4 style="color:#F00;"><?php if(Lang::has(Session::get('lang_file').'.NO_BLOGS_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_BLOGS_AVAILABLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_BLOGS_AVAILABLE')); ?> <?php endif; ?>...</h4>
        <?php endif; ?>

        </ul>
        <div class="sortPagiBar">
          <div class="pagination-area">
            <ul>
         	<?php echo e($get_blog_list->render()); ?>


            </ul>
          </div>
        </div>
      </div>
      <!-- ./ Center colunm --> 
      
    </div>
  </section>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
<!-- <script src="<?php echo e(url('')); ?>/themes/js/jquery-1.10.0.min.js"></script> -->
	<?php echo $footer; ?>

<!-- Placed at the end of the document so the pages load faster ============================================= -->
<!-- <script src="<?php echo e(url('')); ?>/themes/js/jquery.js" type="text/javascript"></script> -->
 <?php if($get_blog_comment_check): ?> 
 <?php $__currentLoopData = $get_blog_comment_check; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $check_comment_by_admin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  <?php $check_comment_byadmin = $check_comment_by_admin->bs_postsppage;  ?>
 <?php else: ?> <?php $check_comment_byadmin=0;?> <?php endif; ?>
<script>
 $(function() {
 $('#log_grid_1.paginated').each(function() {
 var currentPage = 0;
 var numPerPage = <?php echo $check_comment_byadmin; ?>;
 var $grid = $(this);
 $grid.bind('repaginate', function() {
 $grid.find('.log_grid_1_record').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
 });
 $grid.trigger('repaginate');
 var numRows = $grid.find('.log_grid_1_record').length;
 var numPages = Math.ceil(numRows / numPerPage);
 var $pager = $('<div> <b style="color:#F60" > Pages >></b> </div>');
 for (var page = 0; page < numPages; page++) {
 $('<span style="cursor:pointer;padding-left:5px;color:green;font-weight:800;" ></span>').text(page + 1).bind('click', {
 newPage: page
 }, function(event) {
 currentPage = event.data['newPage'];
 $grid.trigger('repaginate');
 $(this).addClass('active').siblings().removeClass('active');
 }).appendTo($pager).addClass('clickable');
 }
 $pager.insertBefore($grid).find('span.page-number:first').addClass('active');
 });
 });
</script>
<!--     <script src="<?php echo e(url('')); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo e(url('')); ?>/themes/js/google-code-prettify/prettify.js"></script>
	
	<script src="<?php echo e(url('')); ?>/themes/js/bootshop.js"></script>
    <script src="<?php echo e(url('')); ?>/themes/js/jquery.lightbox-0.5.js"></script>  -->
    
<!--<script src="<?php echo e(url('')); ?>/plug-k/demo/js/jquery-1.8.0.min.js" type="text/javascript"></script> -->
     <script type="text/javascript">
$(document).ready(function(){

$("#searchbox").keyup(function(e) 
{

var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;
if(searchbox=='')
{
$("#display").html("").hide();	
}
else
{
	var passData = 'searchword='+ searchbox;
	 $.ajax( {
			      type: 'GET',
				  data: passData,
				  url: '<?php echo url('autosearch'); ?>',
				  success: function(responseText){  
				  $("#display").html(responseText).show();	
}
});
}
return false;    

});
});
</script>
    <script src="<?php echo e(url('')); ?>/plug-k/js/bootstrap-typeahead.js" type="text/javascript"></script>
    
    <script src="<?php echo e(url('')); ?>/plug-k/demo/js/demo.js" type="text/javascript"></script>
	

<!-- <script type="text/javascript" src="<?php echo e(url('')); ?>/themes/js/jquery-1.5.2.min.js"></script> -->
	<script type="text/javascript" src="<?php echo e(url('')); ?>/themes/js/scriptbreaker-multiple-accordion-1.js"></script> 
    <script language="JavaScript">
    
    $(document).ready(function() {
        $(".topnav").accordion({
            accordion:true,
            speed: 500,
            closedSign: '<span class="icon-chevron-right"></span>',
            openedSign: '<span class="icon-chevron-down"></span>'
        });
    });
    
    </script>
	
	<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
	</script>
</body>
</html>