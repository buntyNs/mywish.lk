<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


<!-- Header End====================================================================== -->

  <!-- Main Container -->

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li> 

            <li><strong><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <div class="main-container col2-left-layout">

    <div class="container">

      <div class="row">

        <div class="col-main col-sm-12 col-xs-12">

         

          <div class="shop-inner">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SOLD_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_PRODUCTS')); ?> <?php endif; ?></h2>

            </div>

           

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			  <?php $sold_product_error = "";

				 $sold_product_count=0; ?>

				 <?php if($get_store_product_by_id): ?>  

				 <?php $__currentLoopData = $get_store_product_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				 <?php if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty): ?> 

				 <?php  $sold_product_count++; 

				 $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

				 $res = base64_encode($fetch_most_visit_pro->pro_id);

				 $sold_product_error = 1; 

				 $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

				 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

				 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);?>

			  

                <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner sold-item">

                      <div class="product-thumbnail">

                        <div class="pr-img-area"> 

						 <span class="sold"></span>

					   <?php $product_img   = $mostproduct_img[0];

						$prod_path  = url('').'/public/assets/default_image/No_image_product.png';

						$img_data   = "public/assets/product/".$product_img; ?>

						<?php if(file_exists($img_data) && $product_img !=''): ?>   

						<?php  $prod_path = url('').'/public/assets/product/' .$product_img; ?>                 

						<?php else: ?>  

						<?php if(!isset($DynamicNoImage['productImg'])): ?>

						<?php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['productImg']; ?>

						<?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

						<?php   $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

						<?php endif; ?>

						<?php endif; ?>

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						 <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						<?php endif; ?> 

						

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						 <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						<?php endif; ?>

						

						<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>

						 <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						<?php endif; ?>

						  

						  </div>

						<div class="sold-img"><img src="images/sold-out.png"></div>

                       

                      </div>

					  

					    <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

						<?php  $title = 'pro_title'; ?>

						<?php else: ?>  <?php $title = 'pro_title_'.Session::get('lang_code'); ?>  <?php endif; ?>

						

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> 

						  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						  <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>"><?php echo e(substr($fetch_most_visit_pro->$title,0,25)); ?> <?php echo e(strlen($fetch_most_visit_pro->$title)>25?'..':''); ?></a> 

						  <?php endif; ?>

						  

						  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>

						  <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>"><?php echo e(substr($fetch_most_visit_pro->$title,0,25)); ?>    <?php echo e(strlen($fetch_most_visit_pro->$title)>25?'..':''); ?></a> 

						  <?php endif; ?>

						  

						  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						  <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><?php echo e(substr($fetch_most_visit_pro->$title,0,25)); ?>    <?php echo e(strlen($fetch_most_visit_pro->$title)>25?'..':''); ?></a> 

						  <?php endif; ?>

						  </div>

						 

                          <div class="item-content">

                            

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_most_visit_pro->pro_disprice); ?></span> </span> </div>

                            </div>

						   <div class="pro-action">

                                <button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span> </button>

                           </div>

                          </div>

                        </div>

                      </div>

					  

                    </div>

                  </div>

                </li>

				 <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				  <?php  if($sold_product_count==0)

					{

						echo "<center>No Products Found!</centre>";

					} ?>

			     <?php else: ?>  

					<h5 style="color:#933;" ><?php if(Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER_PRODUCTS')); ?> <?php endif; ?>.</h5>

				<?php endif; ?>

				

              </ul>

            </div>

           <!-- <div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

          </div>

		  

		  <!--for deals-->

		   <div class="shop-inner">

            <div class="page-title">

              <h2><?php if(Lang::has(Session::get('lang_file').'.SOLD_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_DEALS')); ?> <?php endif; ?></h2>

            </div>

           

            <div class="product-grid-area">

              <ul class="products-grid clearfix">

			  <?php  $sold_deal_error = "";

				 $sold_deals_count=0; ?>

				 <?php if($get_store_deal_by_id): ?>  

				 <?php $date = date('Y-m-d H:i:s'); ?>

				 <?php $__currentLoopData = $get_store_deal_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store_deal_by_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				 <?php if(($store_deal_by_id->deal_no_of_purchase >= $store_deal_by_id->deal_max_limit)): ?> 

				 <?php $sold_deals_count++; ?>

			 

				 <?php $sold_deal_error = 1; 

				 $dealdiscount_percentage = $store_deal_by_id->deal_discount_percentage;

				 $deal_img= explode('/**/', $store_deal_by_id->deal_image);

				 $mcat = strtolower(str_replace(' ','-',$store_deal_by_id->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$store_deal_by_id->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$store_deal_by_id->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$store_deal_by_id->ssb_name)); 

				 $res = base64_encode($store_deal_by_id->deal_id); ?>

				 <?php $product_image     = $deal_img[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/deals/".$product_image; ?>

				 <?php if(file_exists($img_data) && $product_image !=''): ?>   

				 <?php  $prod_path = url('').'/public/assets/deals/' .$product_image;  ?>                

				 <?php else: ?>  

				 <?php if(isset($DynamicNoImage['dealImg'])): ?>

				 <?php   $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['dealImg'];?>

				 <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

				 <?php $prod_path = url('').'/'.$dyanamicNoImg_path; ?>

				 <?php endif; ?>

				 <?php endif; ?>

				 <?php endif; ?> 

			  

                <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 ">

                  <div class="product-item">

                    <div class="item-inner sold-item">

                      <div class="product-thumbnail">

                        <div class="pr-img-area"> 

					    <span class="sold"></span>

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						 <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						 <?php elseif($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						 <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" >

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						<?php elseif($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						 <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a> 

						<?php elseif($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

						 <a href="<?php echo url('dealview').'/'.$mcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>">

						  <!--<img class="hover-img" src="images/products/product-1.jpg" alt="HTML template">--></figure>

                          </a>

						<?php endif; ?>

						  

						  </div>

                       <div class="sold-img" ><img src="images/sold-out.png"></div>

                      </div>

					  

					    <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

						<?php  $deal_title = 'deal_title'; ?>

						<?php else: ?> <?php  $deal_title = 'deal_title_'.Session::get('lang_code'); ?> <?php endif; ?>

						

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> 

						  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						  <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>"><?php echo e(substr($store_deal_by_id->$deal_title,0,25)); ?>    <?php echo e(strlen($store_deal_by_id->$deal_title)>25?'..':''); ?></a> 

						  <?php elseif($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						  <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>"><?php echo e(substr($store_deal_by_id->$deal_title,0,25)); ?>    <?php echo e(strlen($store_deal_by_id->$deal_title)>25?'..':''); ?></a> 

						  <?php elseif($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						  <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><?php echo e(substr($store_deal_by_id->$deal_title,0,25)); ?>    <?php echo e(strlen($store_deal_by_id->$deal_title)>25?'..':''); ?></a> 

						  <?php elseif($mcat != '' && $smcat == '' && $sbcat == '' && $ssbcat == ''): ?> 

						  <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><?php echo e(substr($store_deal_by_id->$deal_title,0,25)); ?>    <?php echo e(strlen($store_deal_by_id->$deal_title)>25?'..':''); ?></a>

						  <?php endif; ?>

						  </div>

						 

                          <div class="item-content">

                            

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($store_deal_by_id->deal_discount_price); ?></span> </span> </div>

                            </div>

							<div class="pro-action">

                                <button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD')); ?> <?php endif; ?></span> </button>

                           </div>

                          </div>

                        </div>

                      </div>

					  

                    </div>

                  </div>

                </li>

				 <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				  <?php  if($sold_deals_count==0)

					{

					?><center> <?php echo e((Lang::has(Session::get('lang_file').'.NO_DEALS_AVAILABLE')!= '') ? trans(Session::get('lang_file').'.NO_DEALS_AVAILABLE') : trans($OUR_LANGUAGE.'.NO_DEALS_AVAILABLE')); ?></centre>;

					<?php } ?>

			     <?php else: ?>  

					<h5 style="color:#933;" ><?php if(Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER_DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER_DEALS')); ?> <?php endif; ?>.</h5>

				<?php endif; ?>

				

              </ul>

            </div>

           <!-- <div class="pagination-area">

              <ul>

                <li><a class="active" href="#">1</a></li>

                <li><a href="#">2</a></li>

                <li><a href="#">3</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>

              </ul>

            </div>-->

          </div>

		

		  

        </div>

      </div>

    </div>

  </div>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <?php echo $footer; ?>


  



<script type="text/javascript">

   $(document).ready(function() {

       $(document).on("click", ".customCategories .topfirst b", function() {

           $(this).next("ul").css("position", "relative");

   

           $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

           $(this).next("ul").toggle();

       });

   

       $(document).on("click", ".morePage", function() {

           $(".nextPage").slideToggle(200);

       });

   

       $(document).on("click", "#smallScreen", function() {

           $(this).toggleClass("customMenu");

       });

   

       $(window).scroll(function() {

           if ($(this).scrollTop() > 250) {

               $('#comp_myprod').show();

           } else {

               $('#comp_myprod').hide();

           }

       });

   

   });

</script>



<script language="JavaScript">

   $(document).ready(function() {

       $(".topnav").accordion({

           accordion:false,

           speed: 500,

           closedSign: '<span class="icon-chevron-right"></span>',

           openedSign: '<span class="icon-chevron-down"></span>'

       });

   });

   

</script>

<script type="text/javascript">

   $.ajaxSetup({

   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

   });

</script>