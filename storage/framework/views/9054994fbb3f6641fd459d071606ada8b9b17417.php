   <?php  $current_route = Route::getCurrentRoute()->uri(); ?>
  <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"><?php if(Lang::has(Session::get('mer_lang_file').'.DEALS1')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DEALS1')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.DEALS1')); ?> <?php endif; ?></h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
               <!-- <li class="panel">
                    <a href="#">
                        <i class="icon-dashboard"></i>&nbsp;Deals Dashboard</a>                   
                </li>-->
                   <li <?php if($current_route == 'mer_add_deals' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?> >
                    <a href="<?php echo e(url('mer_add_deals')); ?>" >
                        <i class=" icon-plus-sign"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.ADD_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.ADD_DEALS') : trans($MER_OUR_LANGUAGE.'.ADD_DEALS')); ?>

	                </a>                   
                </li>
                 <li <?php if($current_route == 'mer_manage_deals' || $current_route == 'mer_deal_details/{id}' || $current_route == 'mer_edit_deals/{id}' )  { ?> class="panel active" <?php } else { echo 'class="panel"';  } ?>>
                    <a href="<?php echo e(url('mer_manage_deals')); ?>" >
                        <i class=" icon-edit"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.MANAGE_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MANAGE_DEALS') : trans($MER_OUR_LANGUAGE.'.MANAGE_DEALS')); ?>

                   </a>                   
                </li>
				 <li  <?php if($current_route == 'mer_expired_deals' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?>>
                    <a href="<?php echo e(url('mer_expired_deals')); ?>" >
                        <i class="icon-check-sign"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.EXPIRED_DEALS')!= '') ? trans(Session::get('mer_lang_file').'.EXPIRED_DEALS') : trans($MER_OUR_LANGUAGE.'.EXPIRED_DEALS')); ?>

                   </a>                   
                </li>
				<li  <?php if($current_route == 'mer_sold_deals' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?>>
                    <a href="<?php echo e(url('mer_sold_deals')); ?>" >
                        <i class="icon-check-sign"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.SOLD_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.SOLD_DEALS') : trans($MER_OUR_LANGUAGE.'.SOLD_DEALS')); ?>

                   </a>                   
                </li>
				<li  <?php if($current_route == 'mer_manage_deal_cashondelivery_details' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?>>
                    <a href="<?php echo e(url('mer_manage_deal_cashondelivery_details')); ?>" >
                        <i class="icon-check-sign"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ? trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY') : trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')); ?>

                   </a>                   
                </li>
				<li <?php if( $current_route == "mer_manage_deal_shipping_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo e(url('mer_manage_deal_shipping_details')); ?>" >
                        <i class="icon-ambulance"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.SHIPPING_AND_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.SHIPPING_AND_DELIVERY') : trans($MER_OUR_LANGUAGE.'.SHIPPING_AND_DELIVERY')); ?> 
                   </a>                   
                </li>
                <li <?php if( $current_route == "mer_manage_deal_payu_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo e(url('mer_manage_deal_payu_details')); ?>" >
                        <i class="icon-ambulance"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY') : trans($MER_OUR_LANGUAGE.'.PAYU_SHIPPING_DELIVERY')); ?> 
                   </a>                   
                </li>
			
				<!-- <li  <?php //if($current_route == 'mer_validate_coupon_code' ) { ?> class="panel active" <?php //} else { echo 'class="panel"';} ?>>
                    <a href="<?php //echo url('mer_validate_coupon_code'); ?>" >
                        <i class="icon-barcode"></i>&nbsp;Validate Coupon Code
                   </a>                   
                </li>
				 <li  <?php //if($current_route == 'mer_redeem_coupon_list' ) { ?> class="panel active" <?php //} else { echo 'class="panel"';} ?>>
                    <a href="<?php //echo url('mer_redeem_coupon_list'); ?>" >
                        <i class="icon-asterisk"></i>&nbsp;Redeem Coupon List
                   </a>                   
                </li>-->
            </ul>

        </div>
<!---Right Click Block Code---->



<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>