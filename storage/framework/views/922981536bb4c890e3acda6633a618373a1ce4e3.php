<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT'): trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')); ?> | <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_DETAILS')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


       <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->
        
	<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                                <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')  : trans($MER_OUR_LANGUAGE.'.MER_DEAL_DETAILS')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')  : trans($MER_OUR_LANGUAGE.'.MER_DEAL_DETAILS')); ?></h5>
            
        </header>
        
        <?php $__currentLoopData = $deal_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php
		$title 		 = $deals->deal_title;
		 $title_fr 		 = $deals->deal_title_fr;
         $category_get	 = $deals->deal_category;
	     $maincategory 	 = $deals->deal_main_category;
		 $subcategory 	 = $deals->deal_sub_category;
		 $secondsubcategory= $deals->deal_second_sub_category;
		 $originalprice  = $deals->deal_original_price;
		 $discountprice  = $deals->deal_discount_price;
		 $startdate 	 = $deals->deal_start_date;
		 $enddate 		 = $deals->deal_end_date;
		 $expirydate	 = $deals->deal_end_date;
		 $description 	 = $deals->deal_description;
		 $description_fr = $deals->deal_description_fr;
		 $merchant		 = $deals->deal_merchant_id;
		 $shop			 = $deals->deal_shop_id;
		 $metakeyword	 = $deals->deal_meta_keyword;
		 $metakeyword_fr = $deals->deal_meta_keyword_fr;
		 $metadescription= $deals->deal_meta_description;
		 $metadescription_fr= $deals->deal_meta_description_fr;
		 $minlimt		 = $deals->deal_min_limit;
		 $maxlimit		 = $deals->deal_max_limit;
		 $purchaselimit	 = $deals->deal_purchase_limit;
		 $file_get		     = $deals->deal_image;
		 $img_count		 = $deals->deal_image_count;
		 $file_get_path =  explode("/**/",$file_get,-1); 
		 $img_count =  count($file_get_path); 
		?>
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    <form >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')  : trans($MER_OUR_LANGUAGE.'.MER_DEAL_DETAILS')); ?>

                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_TITLE'): trans($MER_OUR_LANGUAGE.'.MER_DEAL_TITLE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					
					<?php echo e($title); ?>

					    
						<?php if(!empty($get_active_lang)): ?>
						 
							<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php
								$get_lang_name = $get_lang->lang_name;
								$deal_title_dynamic = 'deal_title_'.$get_lang->lang_code; ?>
								<?php if($deals->$deal_title_dynamic!=""): ?>
								<?php 	echo "(".$deals->$deal_title_dynamic.")"; ?>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
                    </div>
                </div>
                        </div>
						  <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TOP_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TOP_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_TOP_CATEGORY')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($deals->mc_name); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_MAIN_CATEGORY')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($deals->smc_name); ?> 
					
				<?php 	/* if($deals->smc_name_fr !=''){ echo '('.$deals->smc_name_fr.')'; } */ ?>
                    </div>
                </div>
                        </div>
				 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Sub Category<span class="text-sub"></span></label>
                    <div class="col-lg-4">
                    <?php echo e($deals->sb_name); ?>

					
					<?php /*  if($deals->sb_name_fr !=''){ echo '('.$deals->sb_name_fr.')'; } */?>
                    </div>
                </div>
                </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Second Sub Category<span class="text-sub"></span></label>
                    <div class="col-lg-4">
                    <?php echo e($deals->ssb_name); ?>

					<?php /*  if($deals->ssb_name_fr !=''){ echo '('.$deals->ssb_name_fr.')'; } */ ?>
                    </div>
                </div>
                
                
                        </div>
						 
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEALS_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEALS_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_DEALS_PRICE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($originalprice); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT_PRICE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($discountprice); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT_PERCENTAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNT_PERCENTAGE')  : trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT_PERCENTAGE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo e($deals->deal_discount_percentage."%"); ?>

                    </div>
                </div>
                        </div>
                <?php /*
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_SAVINGS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SAVINGS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SAVINGS');} ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo $deals->deal_saving_price; ?>
                    </div>
                </div>
                        </div> */?>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_START_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_START_DATE'): trans($MER_OUR_LANGUAGE.'.MER_START_DATE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e(date('M-d-Y h:m:s',strtotime($startdate))); ?>

                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_END_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_END_DATE'):trans($MER_OUR_LANGUAGE.'.MER_END_DATE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e(date('M-d-Y h:m:s',strtotime($enddate))); ?>

                    </div>
                </div>
                        </div>
                <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DESCRIPTION') :  trans($MER_OUR_LANGUAGE.'.MER_DEAL_DESCRIPTION')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($description); ?> 
                    </div>
                    </div>
                </div>
				 
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php 
				$get_lang_name = $get_lang->lang_name;
				$deal_description_dynamic = 'deal_description_'.$get_lang->lang_code; ?>
				<?php if($deals->$deal_description_dynamic !=''): ?> 
				<div class="panel-body">
                    <div class="form-group">
                     <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DESCRIPTION') :  trans($MER_OUR_LANGUAGE.'.MER_DEAL_DESCRIPTION')); ?>(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
                      <div class="col-lg-4">
					  <?php echo e($deals->$deal_description_dynamic); ?>

                       </div>
                    </div>
                </div>	
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
				<?php /* if($description_fr !=''){ ?>
				<!--
				<div class="panel-body">
                    <div class="form-group">
                     <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DESCRIPTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DEAL_DESCRIPTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DEAL_DESCRIPTION');} ?>({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
                      <div class="col-lg-4">
                            <?php echo $description_fr; ?> 
                       </div>
                    </div>
                </div>	-->
				<?php }  */?>
						 <!--div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Deal Expiry Date<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php //echo date('M-d-Y h:m:s',strtotime($expirydate)); ?>
                    </div>
                </div>
                        </div-->
                <?php /*
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_MINIMUM_DEAL_LIMIT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MINIMUM_DEAL_LIMIT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MINIMUM_DEAL_LIMIT');} ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                   <?php echo  $minlimt; ?>
                    </div>
                </div>
                        </div>*/?>
						 <div class="panel-body">
                           <div class="form-group">
							<label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAXIMUM_DEAL_LIMIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAXIMUM_DEAL_LIMIT')  :  trans($MER_OUR_LANGUAGE.'.MER_MAXIMUM_DEAL_LIMIT')); ?><span class="text-sub">*</span></label>
							<div class="col-lg-4">
							<?php echo e($maxlimit); ?>

							</div>
						</div>
                        </div>
						<?php /*?> <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Deal Purchase Limit Per Customer<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo  $purchaselimit; ?>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_PURCHASE_COUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PURCHASE_COUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PURCHASE_COUNT');} ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                 <?php echo $deals->deal_no_of_purchase; ?>
                    </div>
                </div>
                        </div><?php */?>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_NAME') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_NAME')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					<?php echo e($deals->mer_fname); ?>

                    </div>
                </div>
                        </div>
						
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SHOP_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHOP_NAME'): trans($MER_OUR_LANGUAGE.'.MER_SHOP_NAME')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-8">
                            <?php echo $deals->stor_name; if($deals->stor_name_fr !=''){ echo ' ('.$deals->stor_name_fr.')';} ?>
                    </div>
                </div>
                        </div>
				     <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"> Meta Keywords<span class="text-sub">*</span></label>
                    <div class="col-lg-8">
					<?php echo e($metakeyword); ?>

                    </div>
                </div>
                        </div>
						
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
				$get_lang_name = $get_lang->lang_name;
				$deal_meta_keyword_dynamic = 'deal_meta_keyword_'.$get_lang->lang_code; ?>
				<?php if($deals->$deal_meta_keyword_dynamic!=""): ?>
				
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1"> Meta Keywords( <?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>
						<div class="col-lg-8"><?php echo e($deals->$deal_meta_keyword_dynamic); ?></div>
					</div>
                </div>
               
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
                  <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"> Meta Description<span class="text-sub">*</span></label>
                    <div class="col-lg-8">
          <?php echo e($metadescription); ?>

                    </div>
                </div>
                        </div>  
         
        <?php if(!empty($get_active_lang)): ?> 
        <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
        <?php
        $get_lang_name = $get_lang->lang_name;
        $deal_meta_description_dynamic = 'deal_meta_description_'.$get_lang->lang_code;
        ?>
        <?php if($deals->$deal_meta_description_dynamic!="" ): ?>
        
        
        <div class="panel-body">
                    <div class="form-group">
           <label class="control-label col-lg-2" for="text1"> Meta Description( <?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>
            <div class="col-lg-8"><?php echo e($deals->$deal_meta_description_dynamic); ?></div>
          </div>
                </div>
        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <?php /* if($metadescription_fr !=''){?>
        <!--<div class="panel-body">
                    <div class="form-group">
           <label class="control-label col-lg-2" for="text1"> Meta Description({{ Helper::lang_name() }})<span class="text-sub"></span></label>
            <div class="col-lg-8"><?php echo $metadescription_fr; ?></div>
          </div>
                </div>-->
        <?php }  */?> 
             <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEALS_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEALS_IMAGE') : trans($MER_OUR_LANGUAGE.'.MER_DEALS_IMAGE')); ?><span class="text-sub">*</span></label>
                    <div class="col-lg-4">
          
            <?php if(count($file_get_path) > 0  && $img_count != ''): ?> 
           
            
             <?php for($j=0 ; $j< $img_count; $j++): ?>
              
               <?php if($file_get_path[$j] !=''): ?>  
                <?php
                 $pro_img = $file_get_path[$j];
                 $prod_path = url('').'/public/assets/default_image/No_image_deal.png';
                $img_data = "public/assets/deals/".$pro_img; 
                ?>
                  <?php if(file_exists($img_data)): ?>  
                  <?php
                               $prod_path = url('').'/public/assets/deals/'.$pro_img;
                     ?>
                <?php else: ?>  
                     <?php if(isset($DynamicNoImage['dealImg'])): ?> 
                     <?php       
                      $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg']; ?>
                        <?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
                        <?php
                          $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
                          ?>
                        <?php endif; ?>
                    
                       <?php endif; ?>
                     
               <?php endif; ?>
                 
               
               <?php else: ?>
                <?php
                  $prod_path = url('').'/public/assets/default_image/No_image_product.png'; ?>
                <?php if(isset($DynamicNoImage['dealImg'])): ?> 
                                
                      $dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
                        <?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
                         <?php
                          $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
                          ?>
                        <?php endif; ?>
                    
                       <?php endif; ?>
                     
               <?php endif; ?>
                <img style="height:40px;" src="<?php echo e($prod_path); ?>">
            <?php endfor; ?>
            
          <?php else: ?>
           <?php
            $prod_path = url('').'/public/assets/default_image/No_image_deal.png';
            ?>
             <?php if(isset($DynamicNoImage['dealImg'])): ?>
                            
                      <?php
                       $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
                      ?>
                      
                        <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
                      <?php
                        
                         $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
                        ?> 
                      <?php endif; ?>
                      
                      <?php endif; ?>

                   
                 
            
           <img style="height:40px;" src="<?php echo e($prod_path); ?>">
         <?php endif; ?>       
                            
                    </div>
                </div>
                        </div>
                    </div>
                     </div>
                    <?php /*
          <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION_DETAILS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TRANSACTION_DETAILS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION_DETAILS');} ?> 
                        </div>                     
             <div class="panel-body">
                           <div class="form-group">
                    <label for="text1" class="control-label col-lg-3"><span class="text-sub"></span></label>
                    <div class="col-lg-4">
                        <?php if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DEALS_FOUND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_NO_DEALS_FOUND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_NO_DEALS_FOUND');} ?>(<?php if (Lang::has(Session::get('mer_lang_file').'.MER_WAITNIGN_FOR_TABLE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_WAITNIGN_FOR_TABLE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_WAITNIGN_FOR_TABLE');} ?>)
                    </div>
                </div>
                        </div>                       
                    </div>
                */ ?>
          <div class="form-group">
                    <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-2">
                    <a style="color:#fff" href="<?php echo e(URL::previous()); ?>" class="btn btn-warning btn-sm btn-grad"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK') :  trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?></a>
                    </div>
            
                </div>
                
				<?php /*if($metakeyword_fr !=''){?>
				<!-- 
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1"> Meta Keywords({{ Helper::lang_name() }})<span class="text-sub">*</span></label>
						<div class="col-lg-8"><?php echo $metakeyword_fr; ?></div>
					</div>
                </div> -->
				<?php }  */?>
          
                <?php echo e(Form::close()); ?>

                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->  
  <!-- FOOTER -->
      <?php echo $adminfooter; ?>

    <!--END FOOTER --> 
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
