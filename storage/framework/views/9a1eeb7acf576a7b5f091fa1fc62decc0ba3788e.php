<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CASH_ON_DELIVERY')); ?> <?php endif; ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/success.css" />
     <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
      <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>		

    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


           <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
         <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_HOME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?></a></li>
                                <li class="active"><a > <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CASH_ON_DELIVERY')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CASH_ON_DELIVERY')); ?> <?php endif; ?></h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
            
			<?php echo Form::open(['class' => 'form-horizontal']); ?>

   <div class="panel_marg_clr ppd">
       <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                       <tr role="row">
                                        <th aria-sort="ascending" aria-label="S.No: activate to sort column ascending" style="width: 99px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?></th>
                                        <th aria-label="Customers: activate to sort column ascending" style="width: 111px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_NAME')); ?> <?php endif; ?></th>
                                        <th aria-label="Product Title: activate to sort column ascending" style="width: 219px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?></th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID')); ?> <?php endif; ?></th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE')); ?> <?php endif; ?></th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?> <?php endif; ?></th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PRODUCT_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_DETAILS')); ?> <?php endif; ?></th>
                                       <!-- <th aria-label="Transaction Date: activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS');} ?></th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                  <?php $i=1; ?>
								   	<?php $__currentLoopData = $coddetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								     
								   <?php
								   $orderstatus="";
								   $status=$cod->cod_status; ?>
										     	<?php if($cod->cod_status==1): ?>
											 <?php
											$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SUCCESS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS');
												
											 ?>
											<?php elseif($cod->cod_status==2): ?> 
											 <?php
											$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETED')!= ''))? trans(Session::get('admin_lang_file').'.BACK_COMPLETED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETED');
												
											 ?>
											<?php elseif($cod->cod_status==3): ?> 
											 <?php
											$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_HOLD')!= ''))? trans(Session::get('admin_lang_file').'.BACK_HOLD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOLD');
												
											 ?>
											<?php elseif($cod->cod_status==4): ?> 
											 <?php
											$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_FAILED')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FAILED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED');
												
											 ?>
											<?php endif; ?>
			
									
										
								<tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
                                            <td class="     ">
                                            <ul>
											<?php
											$product_data = DB::table('nm_ordercod')
											->orderBy('cod_date', 'desc')
											->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
											->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
											->where('nm_ordercod.cod_transaction_id', '=',$cod->cod_transaction_id)
											->where('nm_ordercod.cod_order_type', '=',1)
											->get(); ?>
                                        
											<?php $__currentLoopData = $product_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_datavalues): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												
											 <?php
												echo '<li style="list-style:upper-roman; text-align:left;">'. $product_datavalues->pro_title.'.</li>';   ?>
																							
											


											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</ul>
											</td>
                                            <td class="       "><?php echo e($cod->cus_name); ?></td>
                                            <td class="center"><?php echo e($cod->cus_email); ?></td>
                                            <td class="center"><?php echo e($cod->cod_date); ?></td>
                                            <td class="center"><?php echo e($cod->ship_address1); ?></td>
											   
                                            <td class="center"><a class="btn btn-success" data-target="<?php echo '#uiModal'.$i;?>" data-toggle="modal"><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_VIEW_DETAILS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_VIEW_DETAILS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_VIEW_DETAILS');} ?></a></td>
                                              <!--<td class="center       "><?php echo $orderstatus;?></td>-->
									  </tr>
                                      
                                     <?php   $i=$i+1; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                </table>
                              </div>

                               
        </div>
           
		   <?php echo Form::close(); ?>

        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    <div class="modal fade in" id="formModal"  style="display:none;">
     <div class="modal-dialog">
                                    <div class="modal-content">
                                        
                                        <div class="modal-body">
                                          <?php echo e(Form::open(array('role' => 'form'))); ?>

                                        <div class="form-group">
                                            <label><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID')); ?></label>
                                            <input class="form-control">
                                            <p class="help-block"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK_LEVEL_HELP')!= ''))? trans(Session::get('admin_lang_file').'.BACK_BLOCK_LEVEL_HELP') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK_LEVEL_HELP')); ?>.</p>
                                        </div>
										 <div class="form-group">
                                            <label><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_COUPEN_CODE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ENTER_COUPEN_CODE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ENTER_COUPEN_CODE')); ?> <?php endif; ?></label>
                                            <input class="form-control">
                                           
                                        </div>
                                        <div class="form-group">
                                            <label><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?> <?php endif; ?>&nbsp;:&nbsp;</label>
                                          asdsa,,California,Canadian
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_MESSAGE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_MESSAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MESSAGE')); ?></label>
											
											<?php echo e(Form::textarea('','', array('class' => 'form-control','id' => 'text4'))); ?>

                                         
                                        </div>
                                        
                                       
                                    <?php echo e(Form::close()); ?>

                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CLOSE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE')); ?></button>
                                            <button class="btn btn-success" type="button"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SEND')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SEND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEND')); ?><</button>
                                        </div>
                                    </div>
                                </div>  </div>
     <!--END MAIN WRAPPER -->
 <?php $i=1; ?>
<?php $__currentLoopData = $coddetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coddetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <?php  

$status=$coddetails->cod_status; ?>
<?php if($coddetails->cod_status==1): ?>
<?php
$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SUCCESS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS');
?>
<?php elseif($coddetails->cod_status==2): ?> 
<?php
$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETED')!= ''))? trans(Session::get('admin_lang_file').'.BACK_COMPLETED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETED');
?>
<?php elseif($coddetails->cod_status==3): ?> 
<?php
$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_HOLD')!= ''))? trans(Session::get('admin_lang_file').'.BACK_HOLD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOLD');
?>
<?php elseif($coddetails->cod_status==4): ?> 
<?php
$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_FAILED')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FAILED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED');
?>
<?php endif; ?>
<?php
$all_tax_amt  =0 ;
$all_shipping_amt =0;
$item_total=0;  $coupon_amount=0;
$subtotal =0;
$wallet_amt=0;

    $tax_amt = (($coddetails->cod_amt * $coddetails->cod_tax)/100);
    $all_tax_amt+=   (($coddetails->cod_amt * $coddetails->cod_tax)/100);

    $cod_shipping_amt = $coddetails->cod_shipping_amt;
    $all_shipping_amt+= $coddetails->cod_shipping_amt; ?>
   
   

    <?php if($coddetails->coupon_amount !=0): ?>{
      <?php  $coupon_value = $coddetails->coupon_amount; ?>
        
    <?php else: ?>
		<?php
         $coupon_value = 0; ?>
    <?php endif; ?>

   
<?php
    $wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$coddetails->cod_transaction_id)->get();
   ?>
    <?php if(count($wallet)!=0): ?>
    <?php    $wallet_amt = $wallet[0]->wallet_used; ?>
    <?php else: ?>
		<?php
        $wallet_amt = 0;
    ?>
	<?php endif; ?>
    
	<?php
    $item_total = $coddetails->cod_amt;
    $sub_total = ($item_total+$tax_amt+$cod_shipping_amt);
    $sub_total = ($sub_total - $coupon_value);
    $grand_total = ($sub_total - $wallet_amt);

?>




<div  id="<?php echo 'uiModal'.$i?>" class="modal fade in invoice-top" style="display:none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">
                                             <div class="col-lg-4"><img src="<?php echo $SITE_LOGO; ?>" alt="<?php if(Lang::has(Session::get('admin_lang_file').'.LOGO')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.LOGO')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.LOGO')); ?> <?php endif; ?>"></div>
                                                 <div class="col-lg-4" style="padding-top: 10px; text-align: center;"><strong><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_TAX_INVOICE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_TAX_INVOICE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX_INVOICE')); ?> </strong></div>
                                                  <div class="col-lg-4" style="text-align: right;"><a href="" style="color:#d9534f" class="" data-dismiss="modal" CLASS="pull-right"><i class="icon-remove-sign icon-2x"></i></a></div>
                                        </div>
                                    <hr style="margin-top: 0px;">
                                      <div class="row">
                                      <div class="col-lg-12">
                                      <div class="col-lg-6">
                                      <h4><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CASH_ON_DELIVERY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CASH_ON_DELIVERY')); ?></h4>
									  <?php if($coddetails->cod_status == 1): ?> 
									  <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT_PAID')!= ''))? trans(Session::get('admin_lang_file').'.BACK_AMOUNT_PAID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT_PAID')); ?>

									  <?php else: ?> 
                                      <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT_TO_PAY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_AMOUNT_TO_PAY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT_TO_PAY')); ?>

									  <?php endif; ?>
									  
									  :<?php echo e(Helper::cur_sym()); ?> <?php
									  $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')
									  ->where('cod_transaction_id', '=', $coddetails->cod_transaction_id)
									  ->where('nm_ordercod.cod_order_type', '=',1)
									  ->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0;  ?>
									  <?php $__currentLoopData = $product_titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prd_tit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
										  	<?php 
																$status=$prd_tit->cod_status; 
											
										  $subtotal=$prd_tit->cod_amt; 
																$tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
																$total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
																$total_ship_amt+= $prd_tit->cod_shipping_amt;
																$total_item_amt+=$prd_tit->cod_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->cod_pro_id;
																
														$item_amt = $prd_tit->cod_amt + (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
														
															 $ship_amt = $prd_tit->cod_shipping_amt; ?>
															
														
														
															<?php if($prd_tit->coupon_amount != 0): ?>
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
															?>
															<?php else: ?>
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt); ?>
															<?php endif; ?>		
															
									  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  <?php echo e($grand_total); ?></b>
                                      <br>
									  
                                      <span>(<?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_INCLUSIVE_OF_ALL_CHARGES')!= ''))? trans(Session::get('admin_lang_file').'.BACK_INCLUSIVE_OF_ALL_CHARGES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INCLUSIVE_OF_ALL_CHARGES')); ?>)</span>
                                     <br>
                                     <span><b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_ID')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ORDER_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_ID')); ?> : </b> <?php echo e($coddetails->cod_transaction_id); ?></span><br>
                                       <br>
 <span><b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_DATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ORDER_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_DATE')); ?>: </b> <?php echo e($coddetails->cod_date); ?></span>
                                      </div>
                                          <div class="col-lg-6" style="border-left:1px solid #eeeeee;">
                                          <h4><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SHIPPING_DETAILS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SHIPPING_DETAILS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPPING_DETAILS')); ?></h4>
                                          
										  <?php $explode = explode(',',$coddetails->cod_ship_addr);
                                          foreach($explode as $addr){
                                            //echo $addr.'<br>';
                                          }?>
										   <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?>: </b>  <?php echo e($coddetails->ship_name); ?><br>
										   
										   <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL')); ?>  : </b>  <?php echo e($coddetails->ship_email); ?><br>
										   
                                          <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ADDRESS1') :   trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1')); ?> : </b> <?php echo e($coddetails->ship_address1); ?><br>
										  
										    <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ADDRESS2') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2')); ?> : </b>  <?php echo e($coddetails->ship_address2); ?><br>
										  
										   <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CITY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY')); ?> :</b> <?php echo e($coddetails->ship_ci_id); ?><br>
										   
                        <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_STATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_STATE') :   trans($ADMIN_OUR_LANGUAGE.'.BACK_STATE')); ?>  : </b> <?php echo e($coddetails->ship_state); ?><br>
						
                          <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_COUNTRY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY')); ?> : </b>  <?php echo e($coddetails->ship_country); ?><br>
										   
										  <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ZIPCODE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ZIPCODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ZIPCODE')); ?>: </b>  <?php echo e($coddetails->ship_postalcode); ?><br>
                                          <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_PHONE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE')); ?> : </b>  <?php echo e($coddetails->ship_phone); ?>

                                          </div>
                                            
                                             
                                      </div>
                                      </div>
                                      <hr>
                                      <div class="row">
                                      <div class="span12 text-center">
                                      <h4 class="text-center"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_INVOICE_DETAILS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_INVOICE_DETAILS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INVOICE_DETAILS')); ?></h4>
                                      <span><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SHIPMENT_CONTAINS_ITEMS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SHIPMENT_CONTAINS_ITEMS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPMENT_CONTAINS_ITEMS')); ?></span>
                                      </div>
                                      </div>
                                      <br>
                 <div class="table-responsive">
							   <table class="inv-table" style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">
                                     <tr style="border-bottom:1px solid #e6e6e6; background:#f5f5f5;">
									<th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_NAME')); ?> <?php endif; ?></th>
                                    <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COLOR')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COLOR')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COLOR')); ?> <?php endif; ?></th>
                                      <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SIZE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SIZE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SIZE')); ?> <?php endif; ?></th>
                                        <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_QUANTITY')); ?>  <?php else: ?> {@ trans($ADMIN_OUR_LANGUAGE.'.BACK_QUANTITY')}} <?php endif; ?></th>
                                         <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PRICE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PRICE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PRICE')); ?> <?php endif; ?></th>
                                            <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_TOTAL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SUB_TOTAL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_TOTAL')); ?> <?php endif; ?></th>
											
															<th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COUPON_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COUPON_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COUPON_AMOUNT')); ?> <?php endif; ?></th>
															 <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_STATUS')); ?> <?php endif; ?></th>
                                             <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS')); ?> <?php endif; ?></th>
                                         <?php //$subtotal=$coddetails->cod_qty*$coddetails->cod_amt;  ?>   
                                    </tr>
									  <?php
									  $product_titles=DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $coddetails->cod_transaction_id)
									  ->where('nm_ordercod.cod_order_type', '=',1)->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0;  ?>
									  <?php $__currentLoopData = $product_titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prd_tit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
										  	
													<?php			$status=$prd_tit->delivery_status; ?>
											<?php if($prd_tit->delivery_status==1): ?>
											<?php
											$orderstatus="Order Placed";
											?>
											<?php elseif($prd_tit->delivery_status==2): ?> 
											<?php
											$orderstatus="Order Packed";
											?>
											<?php elseif($prd_tit->delivery_status==3): ?> 
											<?php
											$orderstatus="Dispatched";
											?>
											<?php elseif($prd_tit->delivery_status==4): ?> 
											<?php
											$orderstatus="Delivered";
											?>
											<?php elseif($prd_tit->delivery_status==5): ?>
                      <?php
                        $orderstatus="cancel pending";
                      ?>
                      <?php elseif($prd_tit->delivery_status==6): ?> 
                      <?php
                      $orderstatus="cancelled";
                      ?>
                      <?php elseif($prd_tit->delivery_status==7): ?> 
                      <?php
                      $orderstatus="return pending";
                      ?>
                      <?php elseif($prd_tit->delivery_status==8): ?> 
                      <?php
                      $orderstatus="Returned";
                      ?>
					  <?php elseif($prd_tit->delivery_status==9): ?> 
                      <?php
                      $orderstatus="Replace Pending";
                      ?>
                      <?php elseif($prd_tit->delivery_status==10): ?> 
                     <?php
                      $orderstatus="Replaced";
                      ?>
					  <?php else: ?>
					  <?php $orderstatus = '';  ?>
				      <?php endif; ?>
                      
											
											
											<?php if($prd_tit->cod_status==1): ?>
											<?php
											$payment_status="Success";
											?>
											<?php elseif($prd_tit->cod_status==2): ?> 
											<?php
											$payment_status="Order Packed";
											?>
											<?php elseif($prd_tit->cod_status==3): ?> 
											<?php
											$payment_status="Hold";
											?>
											<?php elseif($prd_tit->cod_status==4): ?> 
											<?php
											$payment_status="Faild";
											?>
											<?php endif; ?>
											<?php
										  $subtotal=$prd_tit->cod_amt; 
																$tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
																$total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
																$total_ship_amt+= $prd_tit->cod_shipping_amt;
																$total_item_amt+=$prd_tit->cod_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->cod_pro_id;
																
														$item_amt = $prd_tit->cod_amt + (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
														
															 $ship_amt = $prd_tit->cod_shipping_amt;
															?>
														
															 
															<?php if($prd_tit->coupon_amount != 0): ?>
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
															?>
															<?php else: ?>
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
															?>
															<?php endif; ?>		
									 
                                   
                                    
                                    
                                     <tr>	
									 <td  width="" align="center"><?php echo e($prd_tit->pro_title); ?></td>
                                    <td  width="" align="center"><?php echo e($prd_tit->co_name); ?></td>
                                      <td  width="" align="center"><?php echo e($prd_tit->si_name); ?></td>
                                        <td  width="" align="center"><?php echo e($prd_tit->cod_qty); ?> </td>
                                          <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?> <?php echo e($prd_tit->cod_prod_unitPrice); ?> <?php //echo $prd_tit->pro_disprice;?> </td>
                                            <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?><?php echo e($subtotal - $prd_tit->coupon_amount); ?> </td>
											<?php if($prd_tit->coupon_amount != 0): ?> 
															<td  width="" align="center"><?php echo e(Helper::cur_sym()); ?> <?php echo e($prd_tit->coupon_amount); ?> </td>
															<?php else: ?>  <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?> <?php echo e($prd_tit->coupon_amount); ?> </td><?php endif; ?>
															<td  width="" align="center"><?php echo e($payment_status); ?></td>
											<td  width="" align="center"><?php echo e($orderstatus); ?></td>											</td>
                                             
                                              
                                    </tr>
									 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									 
                                    </table>
                                    </div>
                                    <br>
                                    <hr>
                                    <div class="row">
                                      <div class="col-lg-12">
                                    <div class="col-lg-6"></div>
                                     <div class="col-lg-6">

                               <div><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPMENT_VALUE')); ?> <?php endif; ?><p class="pull-right" style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($total_ship_amt); ?> </p></div><br>
                                        <div><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAX')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAX')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX')); ?> <?php endif; ?><p class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($total_tax_amt); ?></p></div>
                                        <hr>


                                     <?php if(count($wallet)!=0): ?>
                                        <span>wallet : <b class="pull-right"style="margin-right:15px;">- <?php echo e(Helper::cur_sym()); ?><?php echo e($wallet_amt); ?></b></span><br>
                                     <?php endif; ?>

                                     <!--<?php   if($coddetails->coupon_amount !=0){ ?>
                                         <span>coupon Code : <b class="pull-right"style="margin-right:15px;">- <?php echo e(Helper::cur_sym()); ?> <?php echo $coupon_value;?></b></span><br>
<?php                                }
                                     ?>-->
                                       <strong><div><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?><b class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($grand_total); ?></b></div></strong>
                                     </div>
                                    </div>
                        </div>
                                        
                                        <div class="modal-footer">
										
                                            <button data-dismiss="modal" class="btn btn-danger" type="button"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLOSE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE')); ?> <?php endif; ?></button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

  <?php $i=$i+1;  ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
    <!-- FOOTER -->
    <div id="footer">
        <?php echo $adminfooter; ?>

    </div>
    <!--END FOOTER -->
  <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
       <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

		  <script type="text/javascript">
	   $.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	   });
	</script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable({
              "scrollX": true
             });
         });
    </script>
  <script type="text/javascript">

  </script>
</body>
     <!-- END BODY -->
</html>
