<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9">
      <![endif]-->
      <!--[if !IE]><!--> 
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <meta charset="UTF-8" />
            <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FAILED_ORDERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FAILED_ORDERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED_ORDERS')); ?> <?php endif; ?></title>
            <meta content="width=device-width, initial-scale=1.0" name="viewport" />
            <meta content="" name="description" />
            <meta content="" name="author" />
            <meta name="_token" content="<?php echo csrf_token(); ?>"/>
            <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
            <!-- GLOBAL STYLES -->
            <!-- GLOBAL STYLES -->
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
            <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/success.css" />
            <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
            <?php 
            $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
            <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
            <?php endif; ?>	
            <!--END GLOBAL STYLES -->
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->
            <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
         </head>
         <!-- END HEAD -->
         <!-- BEGIN BODY -->
         <body class="padTop53 " >
            <!--Loader & alert-->
            <div id="loader" style="position: absolute; display: none;">
               <div class="loader-inner"></div>
               <div class="loader-section"></div>
            </div>
            <div id="loadalert" class="alert-success" style="margin-top:18px; display: none; position: fixed; z-index: 9999; width: 50%; text-align: center; left: 25%; padding: 10px;">
               <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
               <strong>Status Changed Successfully!</strong>
            </div>
            <!--Loader & alert-->
            <!-- MAIN WRAPPER -->
            <div id="wrap">
               <!-- HEADER SECTION -->
               <?php echo $adminheader; ?>

               <!-- MENU SECTION -->
               <?php echo $adminleftmenus; ?>

               <!--END MENU SECTION -->
               <div></div>
               <!--PAGE CONTENT -->
               <div id="content">
                  <div class="inner">
                     <div class="row">
                        <div class="col-lg-12">
                           <ul class="breadcrumb">
                              <li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                              <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FAILED_ORDERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FAILED_ORDERS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED_ORDERS')); ?> <?php endif; ?></a></li>
                           </ul>
                        </div>
                     </div>
                     <center>
                        <div class="cal-search-filter">
                           <form  action="<?php echo action('TransactionController@deals_payu_failed_orders'); ?>" method="POST">
                              <input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
                              <div class="row">
                                 <br>
                                 <div class="col-sm-3">
                                    <div class="item form-group">
                                       <div class="col-sm-6"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FROM_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FROM_DATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FROM_DATE')); ?> <?php endif; ?></div>
                                       <div class="col-sm-6 place-size">
                                          <span class="icon-calendar cale-icon"></span>
                                          <?php echo e(Form::text('from_date',$from_date,array('id'=>'datepicker-8','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly'))); ?>

                                          <!-- <input type="text" name="from_date" 
                                             placeholder="DD/MM/YYYY"  class="form-control" id="datepicker-8" value="<?php echo e($from_date); ?>" required readonly> -->
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-3">
                                    <div class="item form-group">
                                       <div class="col-sm-6"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TO_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TO_DATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TO_DATE')); ?> <?php endif; ?></div>
                                       <div class="col-sm-6 place-size">
                                          <span class="icon-calendar cale-icon"></span>
                                          <?php echo e(Form::text('to_date',$to_date,array('id'=>'datepicker-9','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly'))); ?>

                                          <!-- <input type="text" name="to_date" 
                                             placeholder="DD/MM/YYYY"  id="datepicker-9" class="form-control" value="<?php echo e($to_date); ?>" required readonly> -->
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-sm-2">
                                       <input type="submit" name="submit" class="btn btn-block btn-success" value="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEARCH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH')); ?> <?php endif; ?>">
                                    </div>
                                    <div class="col-sm-2">
                                       <a href="<?php echo e(url('').'/deals_payu_failed_orders'); ?>"><button type="button" name="reset" class="btn btn-block btn-info"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RESET')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?> <?php endif; ?></button></a>
                                    </div>
                                 </div>
                                 <?php echo e(Form::close()); ?>

                              </div>
                     </center>
                     <div class="row">
                     <div class="col-lg-12">
                     <div class="box dark">
                     <header>
                     <div class="icons"><i class="icon-edit"></i></div>
                     <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FAILED_ORDERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FAILED_ORDERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED_ORDERS')); ?> <?php endif; ?></h5>
                     </header>
                     <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
                     <?php echo e(Form::button('x',['class'=>'close closeAlert','aria-hidden'=>'true'])); ?>	
                     <!-- <button type="button" class="close closeAlert"  aria-hidden="true">×</button> --></div>
                     <div id="div-1" class="accordion-body collapse in body">
                     <form class="form-horizontal">
                     <div class="table-responsive panel_marg_clr ppd">
                     <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                     <thead>
                     <tr role="row">
                     <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 69px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SNO')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMERS')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MERCHANT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 158px;" aria-label="Email: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DEAL_TITLE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_TITLE')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="City: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 84px;" aria-label="Joined Date: activate to sort column ascending"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAX')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAX')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 84px;" aria-label="Joined Date: activate to sort column ascending"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPMENT_VALUE')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 101px;" aria-label="Send Mail: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_STATUS')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_DATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_DATE')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_TYPE')); ?> <?php endif; ?></th>
                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS')); ?> <?php endif; ?></th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php $i = 1 ; ?>
                     <?php if(isset($_POST['submit'])): ?>
                     <?php $__currentLoopData = $failedrep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                     <?php	$orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_FAILED')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FAILED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED');
                     $ordertype = ''; ?>
                     <?php if($row->order_paytype == 1): ?>
                     <?php	$ordertype = ((Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY'); ?>
                     <?php elseif($row->order_paytype == 2): ?>
                     <?php	$ordertype = ((Lang::has(Session::get('admin_lang_file').'.BACK_WALLET')!= ''))? trans(Session::get('admin_lang_file').'.BACK_WALLET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WALLET');	 ?>
                     <?php else: ?> 
                     <?php	$ordertype = "--"; ?>
                     <?php endif; ?>
                     <?php	$total_tax = ($row->order_amt * $row->order_tax)/100 ; ?>
                     <tr class="gradeA odd">
                     <td class="sorting_1"><?php echo e($i); ?></td>
                     <td class="     "><?php echo e($row->cus_name); ?></td>
                     <td class="     "><?php echo e($row->mer_fname); ?></td>
                     <td class="     "><?php echo e($row->deal_title); ?></td>
                     <td class="center     "><?php echo e($row->order_amt); ?></td>
                     <td class="center     "><?php echo e($total_tax); ?></td>
                     <td class="center"><?php echo e($row->order_shipping_amt); ?></td>
                     <td class="center     "><a href="#" class="colr3"><?php echo e($orderstatus); ?>

                     </a></td>
                     <td class="center     "><?php echo e($row->order_date); ?></td>
                     <td class="center     "><a href="#" class="colr2"><?php echo e($ordertype); ?></a></td>
                     <td class="center     ">
                     <?php if($row->delivery_status==5): ?>
                     <span>Cancel Request Pending</span>
                     <?php elseif($row->delivery_status==7): ?>
                     <span>Return Request Pending</span>
                     <?php elseif($row->delivery_status==9): ?>
                     <span>Replace Request Pending</span>
                     <?php else: ?>
                     <select name="<?php  echo 'status'.$row->order_id;?>" class="btn btn-default" disabled onchange="update_order_paypal(this.value,'<?php echo $row->transaction_id;?>','<?php echo $row->deal_id;?>')">
                     <option value="1" <?php if($row->delivery_status==2 || $row->delivery_status==3 || $row->delivery_status==4 || $row->delivery_status==6 || $row->delivery_status==8 || $row->delivery_status==10) {?>style="display:none" <?php } ?><?php if($row->delivery_status==1){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PLACED')); ?> <?php endif; ?></option>
                     <option value="6" <?php if($row->delivery_status==1 || $row->delivery_status==6 || $row->delivery_status==5) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($row->delivery_status==6){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCELLED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCELLED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCELLED')); ?> <?php endif; ?></option>
                     <option value="2" <?php if($row->delivery_status==3 || $row->delivery_status==4 || $row->delivery_status==6 || $row->delivery_status==8 || $row->delivery_status==10) {?>style="display:none" <?php } ?><?php if($row->delivery_status==2){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PACKED')); ?> <?php endif; ?></option>
                     <option value="3" <?php if($row->delivery_status==4 || $row->delivery_status==6 || $row->delivery_status==8 || $row->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($row->delivery_status==3){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DISPATCHED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DISPATCHED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DISPATCHED')); ?> <?php endif; ?></option>
                     <option value="4" <?php if($row->delivery_status==6 || $row->delivery_status==8 || $row->delivery_status==10) {?>style="display:none" <?php } ?><?php if($row->delivery_status==4){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERED')); ?> <?php endif; ?></option>
                     <option value="8" <?php if($row->delivery_status==4 || $row->delivery_status==8 || $row->delivery_status==7) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($row->delivery_status==8){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RETURNED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RETURNED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURNED')); ?> <?php endif; ?></option>
                     <option value="10" <?php if($row->delivery_status==4 || $row->delivery_status==10 || $row->delivery_status==9) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($row->delivery_status==10){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REPALCED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REPALCED')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REPALCED')); ?> <?php endif; ?></option>
                     </select>    <?php endif; ?>
                     </td>
                     </tr>
                     <?php $i++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <?php $__currentLoopData = $failedorders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allorders_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                     <?php $orderstatus = ((Lang::has(Session::get('admin_lang_file').'.BACK_FAILED')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FAILED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FAILED');
                     $ordertype = ''; ?>
                     <?php if($allorders_list->order_paytype==1): ?>
                     <?php	$ordertype = ((Lang::has(Session::get('admin_lang_file').'.BACK_PAYUMONEY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_PAYUMONEY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYUMONEY'); ?>
                     <?php elseif($allorders_list->order_paytype==2): ?>
                     <?php	$ordertype = ((Lang::has(Session::get('admin_lang_file').'.BACK_WALLET')!= ''))? trans(Session::get('admin_lang_file').'.BACK_WALLET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WALLET');	?>
                     <?php else: ?> 
                     <?php	$ordertype = "--"; ?>
                     <?php endif; ?>
                     <?php	$total_tax = ($allorders_list->order_amt * $allorders_list->order_tax)/100 ; ?>
                     <tr class="gradeA odd">
                     <td class="sorting_1"><?php echo e($i); ?></td>
                     <td class=" "><?php echo e($allorders_list->cus_name); ?></td>
                     <td class="     "><?php echo e($allorders_list->mer_fname); ?>

                     <td class="     "><?php echo e($allorders_list->deal_title); ?>

                     <td class="center     "><?php echo e($allorders_list->order_amt); ?></td>
                     <td class="center     "><?php echo e($total_tax); ?></td>
                     <td class="center"><?php echo e($allorders_list->order_shipping_amt); ?></td>
                     <td class="center     "><a href="#" class="colr3"><?php echo e($orderstatus); ?></a></td>
                     <td class="center     "><?php echo e($allorders_list->order_date); ?></td>
                     <td class="center     "><a href="#" class="colr2"><?php echo e($ordertype); ?></a></td>
                     <td class="center     ">
                     <?php if($allorders_list->delivery_status==5): ?>
                     <span>Cancel Request Pending</span>
                     <?php elseif($allorders_list->delivery_status==7): ?>
                     <span>Return Request Pending</span>
                     <?php elseif($allorders_list->delivery_status==9): ?>
                     <span>Replace Request Pending</span>
                     <?php else: ?>
                     <select name="<?php  echo 'status'.$allorders_list->order_id;?>" class="btn btn-default" disabled onchange="update_order_paypal(this.value,'<?php echo $allorders_list->transaction_id;?>','<?php echo $allorders_list->deal_id;?>')">
                     <option value="1" <?php if($allorders_list->delivery_status==2 || $allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==1){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PLACED')); ?> <?php endif; ?></option>
                     <option value="6" <?php if($allorders_list->delivery_status==1 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==5) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==6){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCELLED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCELLED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCELLED')); ?> <?php endif; ?></option>
                     <option value="2" <?php if($allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==2){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PACKED')); ?> <?php endif; ?></option>
                     <option value="3" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==3){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DISPATCHED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DISPATCHED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DISPATCHED')); ?> <?php endif; ?></option>
                     <option value="4" <?php if($allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==4){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERED')); ?> <?php endif; ?></option>
                     <option value="8" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==7) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==8){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RETURNED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RETURNED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURNED')); ?> <?php endif; ?></option>
                     <option value="10" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==10 || $allorders_list->delivery_status==9) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==10){?> selected <?php } ?>><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REPALCED')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REPALCED')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REPALCED')); ?> <?php endif; ?></option>
                     </select>   <?php endif; ?>
                     </td>
                     </tr>
                     <?php $i++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>			
                     </tbody>
                     </table></div>
                     </div>
                     </div>
                     </form>
                     </div>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--END PAGE CONTENT -->
            </div>
            <!--END MAIN WRAPPER -->
            <!-- FOOTER -->
            <?php echo $adminfooter; ?>

            <!--END FOOTER -->
            <!-- GLOBAL SCRIPTS -->
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
            <!-- PAGE LEVEL SCRIPTS -->
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
            <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
            <script>
               $(document).ready(function () {
                   $('#dataTables-example').dataTable();
               });
            </script> 
            <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
            <script>
               $(function() {
                  $( "#datepicker-8" ).datepicker({
                     prevText:"click for previous months",
                     nextText:"click for next months",
                     showOtherMonths:true,
                     selectOtherMonths: false
                  });
                  $( "#datepicker-9" ).datepicker({
                     prevText:"click for previous months",
                     nextText:"click for next months",
                     showOtherMonths:true,
                     selectOtherMonths: true
                  });
               });
               /** Check start date and end date**/
               $("#datepicker-8,#datepicker-9").change(function() {
               var startDate = document.getElementById("datepicker-8").value;
               var endDate = document.getElementById("datepicker-9").value;
               if (this.id == 'datepicker-8') {
                    if ((Date.parse(endDate) <= Date.parse(startDate))) {
                          $('#datepicker-8').val('');
                         $(".date-select1").css({"display" : "block"});
                          return false;
                      }
                  } 
               
                   if(this.id == 'datepicker-9') {
                      if ((Date.parse(endDate) <= Date.parse(startDate))) {
                          $('#datepicker-9').val('');
                           $(".date-select1").css({"display" : "block"});
                           return false;
                          //alert("End date should be greater than Start date");
                      }
                      }
                      
                  
               //document.getElementById("ed_endtimedate").value = "";
               
               });
               /*Start date end date check ends*/
               
               
               $(".closeAlert").click(function(){
               $(".alert-success").hide();
               });
            </script>
            <script type="text/javascript">
               $.ajaxSetup({
                   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
               });
            </script>
            <script>
               function update_order_paypal(id,orderid,proid)
               {
               		 var passdata= 'id='+id+"&order_id="+orderid+"&proid="+proid;
               		 var pathnametemp =$('#return_url').val();
               		  $.ajax( {
               		      type: 'get',
               			  data: passdata,
               			  url: 'update_deal_order_payumoney_admin',
               			   beforeSend: function() {
               			        // setting a timeout
               			        //$(placeholder).addClass('loading');
               			       document.getElementById("loader").style.display = "block";
               			    },
               			  success: function(responseText){ 
               				document.getElementById("loader").style.display = "none";
               				document.getElementById("loadalert").style.display = "block";
               				if(responseText=="success")
               				{ 	 
               				   setTimeout(function () {
               						 location.reload();  
               					},1000);
               				}
               			}		
               		});	
               }
            </script>
         </body>
         <!-- END BODY -->
      </html>