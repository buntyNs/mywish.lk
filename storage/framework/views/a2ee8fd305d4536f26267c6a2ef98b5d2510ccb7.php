<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
   <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MANAGE_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_DEALS')); ?> <?php endif; ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
   <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


        <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_HOME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?></a></li>
                               <li class="active"><a><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EXPIRED_DEALS')); ?></a></li>
                            </ul>
                    </div>
                </div>
			  <center><div class="cal-search-filter">
		 
		 <?php echo e(Form::open(array('action' => 'DealsController@expired_deals','method'=> 'POST'))); ?>

							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
							 <div class="row">
							 <br>
							 
							 
							   <div class="col-sm-4 col-md-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_FROM_DATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_FROM_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FROM_DATE')); ?></div>
						 <div class="col-sm-6 place-size">
 						<span class="icon-calendar cale-icon"></span>
							 <input type="text" name="from_date" placeholder="DD/MM/YYYY" class="form-control" id="datepicker-8" value="<?php echo e($from_date); ?>" required readonly>
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-4 col-md-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_TO_DATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_TO_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TO_DATE')); ?></div>
							 <div class="col-sm-6 place-size">
 <span class="icon-calendar cale-icon"></span>
							 <input type="text" name="to_date" placeholder="DD/MM/YYYY" id="datepicker-9" class="form-control" value="<?php echo e($to_date); ?>" required readonly>
							 
							  </div>
							  </div>
							   </div>
							   
							   <div class="form-group">
							   <div class="col-sm-2">
							 <input type="submit" name="submit" class="btn btn-block btn-success" value="<?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SEARCH') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH')); ?>">
							 </div>
                             <div class="col-sm-2">
								<a href="<?php echo url('').'/expired_deals';?>"><button type="button" name="reset" class="btn btn-block btn-info"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= ''))? trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button></a>
							 </div>
							</div>
							
							<?php echo e(Form::close()); ?></div>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EXPIRED_DEALS')); ?></h5>
            
         </header>
         <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
		 <?php echo e(Form::button('x', array('class' => 'close closeAlert','aria-hidden' => 'true'))); ?>

         </div>
           <?php if(Session::has('block_message')): ?>
		<div class="alert alert-success alert-dismissable"><?php echo Session::get('block_message'); ?>

        <?php echo e(Form::button('x', array('class' => 'close','aria-hidden' => 'true' ,'data-dismiss' => 'alert'))); ?> </div>
		<?php endif; ?>
        <div id="div-1" class="accordion-body collapse in body">
 	
        <div class="table-responsive panel_marg_clr ppd">
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
										<th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?></th>
										<th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_DEALS_NAME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_DEALS_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DEALS_NAME')); ?></th>
										<th aria-label="Platform(s): activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CITY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY')); ?></th>
										<th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_STORE_NAME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_STORE_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_NAME')); ?></th>
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ORIGINAL_PRICE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ORIGINAL_PRICE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORIGINAL_PRICE')); ?>(<?php echo e(Helper::cur_sym()); ?>)</th>
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_DISCOUNTED_PRICE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_DISCOUNTED_PRICE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DISCOUNTED_PRICE')); ?> (<?php echo e(Helper::cur_sym()); ?>)</th>
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"> <?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_IMAGE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_DEAL_IMAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_IMAGE')); ?> </th>	
									 
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_DEAL_DETAILS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DEAL_DETAILS')); ?></th>
                                       
										</tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = 1 ;
									   ?>
                                    
<?php if(isset($_POST['submit'])): ?>

    <?php if(count($exdeals_rep)): ?>	
	    <?php $__currentLoopData = $exdeals_rep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
        	<?php $deal_get_img = explode("/**/",$deal_list->deal_image); ?>
        	
            <tr class="gradeA odd">
                <td class="sorting_1"><?php echo e($i); ?></td>
                <td class="  "><?php echo e(substr($deal_list->deal_title,0,45)); ?></td>
           		<td class="  center"><?php echo e($deal_list->ci_name); ?></td>
                <td class="center  "><?php echo e($deal_list->stor_name); ?></td>
                <td class="center  "><?php echo e($deal_list->deal_original_price); ?></td>
                <td class="center  "><?php echo e($deal_list->deal_discount_price); ?></td>
                <td class="center  "><a >
				
				
				<?php $pro_img = $deal_get_img[0];
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
							
							  <?php if($deal_get_img !=''): ?> 
								
					  
								
						   <?php if($deal_get_img[0] !=''): ?>  
						    
							<?php  
							   $img_data = "public/assets/deals/".$pro_img;  ?>
							    <?php if(file_exists($img_data)): ?>  
									 <?php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img;
								     ?>
								<?php else: ?>
									<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 					
											<?php
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												 <?php
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
									     <?php endif; ?>
										 
										 
                                <?php endif; ?>
					       
					      
						   <?php else: ?>
						   <?php
							    $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
								<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 	<?php					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												<?php 
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
										
									     <?php endif; ?>
										 
					<?php endif; ?>
						    
					  
			      
				  <?php else: ?>
				  
					 <?php  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; ?>
					   <?php if(isset($DynamicNoImage['dealImg'])): ?>
										 				
							<?php				
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
											
											  <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
											
												<?php
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											    ?>
											<?php endif; ?>
											
									     <?php endif; ?>

									 
								 
					  
					
				 <?php endif; ?>
				
				
				
				
				<img style="height:40px;" src="<?php echo e($prod_path); ?>"></a></td>
                <td class="center  "><a href=" <?php echo e(url('deal_details')."/".$deal_list->deal_id); ?>"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_VIEW')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_VIEW')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_VIEW')); ?> <?php endif; ?></a>&nbsp;|&nbsp;<a href="<?php echo e(url('edit_deals')."/".$deal_list->deal_id); ?>"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT')); ?><?php endif; ?></a></td>
               <!--  <td class="center"><?php // echo $process;  ?></td> -->
            </tr>
<?php $i++;  ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

<?php else: ?>
	 
    <?php if(count($deal_details)): ?>
    	<?php $__currentLoopData = $deal_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
          <?php  $deal_get_img = explode("/**/",$deal_list->deal_image); ?>
			
            <tr class="gradeA odd">
                <td class="sorting_1"><?php echo e($i); ?></td>
                <td class="  "><?php echo e(substr($deal_list->deal_title,0,45)); ?></td>
           		<td class="  center"><?php echo e($deal_list->ci_name); ?></td>
                <td class="center  "> <?php echo e($deal_list->stor_name); ?></td>
                <td class="center  "> <?php echo e($deal_list->deal_original_price); ?></td>
                <td class="center  "> <?php echo e($deal_list->deal_discount_price); ?></td>
                <td class="center  ">
				
				
				<?php $pro_img = $deal_get_img[0];
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
							
							  <?php if($deal_get_img !=''): ?> 
								
					  
								
						   <?php if($deal_get_img[0] !=''): ?>  
						    
							<?php  
							   $img_data = "public/assets/deals/".$pro_img;  ?>
							    <?php if(file_exists($img_data)): ?>  
									 <?php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img;
								     ?>
								<?php else: ?>
									<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 					
											<?php
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												 <?php
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
									     <?php endif; ?>
										 
										 
                                <?php endif; ?>
					       
					      
						   <?php else: ?>
						   <?php
							    $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
								<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 	<?php					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												<?php 
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
										
									     <?php endif; ?>
										 
					<?php endif; ?>
						    
					  
			      
				  <?php else: ?>
				  
					 <?php  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; ?>
					   <?php if(isset($DynamicNoImage['dealImg'])): ?>
										 				
							<?php				
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
											
											  <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
											
												<?php
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											    ?>
											<?php endif; ?>
											
									     <?php endif; ?>

									 
								 
					  
					
				 <?php endif; ?>
				
				<a ><img style="height:40px;" src="<?php echo e($prod_path); ?>"></a></td>
                <td class="center  "><a href="<?php echo e(url('deal_details')."/".$deal_list->deal_id); ?>"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_VIEW')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_VIEW')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_VIEW')); ?> <?php endif; ?></a>&nbsp;|&nbsp;<a href="<?php echo e(url('edit_deals')."/".$deal_list->deal_id); ?>"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT')); ?> <?php endif; ?></a></td>
               <!--  <td class="center"><?php // echo $process;  ?></td> -->
            </tr>
<?php $i++; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?> <?php endif; ?>
									
			</tbody>
            </table></div>


        </div>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

   <!-- FOOTER -->
      <?php echo $adminfooter; ?>

    <!--END FOOTER --> 
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->   
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLICK_FOR_PREVIOUS_MONTHS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLICK_FOR_PREVIOUS_MONTHS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLICK_FOR_PREVIOUS_MONTHS')); ?> <?php endif; ?>",
               nextText:"<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLICK_FOR_NEXT_MONTHS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLICK_FOR_NEXT_MONTHS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLICK_FOR_NEXT_MONTHS')); ?> <?php endif; ?>",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLICK_FOR_PREVIOUS_MONTHS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLICK_FOR_PREVIOUS_MONTHS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLICK_FOR_PREVIOUS_MONTHS')); ?> <?php endif; ?>",
               nextText:"<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLICK_FOR_NEXT_MONTHS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLICK_FOR_NEXT_MONTHS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLICK_FOR_NEXT_MONTHS')); ?><?php endif; ?>",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
         /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/


$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
      </script>
</body><script type="text/javascript">
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
</script>  
     <!-- END BODY -->
</html>
