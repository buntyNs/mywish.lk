<?php  $current_route = Route::getCurrentRoute()->uri(); ?> 
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.TRANSACTIONS1')!= '') ?  trans(Session::get('mer_lang_file').'.TRANSACTIONS1') : trans($MER_OUR_LANGUAGE.'.TRANSACTIONS1')); ?> </h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                <?php /*<li <?php if($current_route=="deals_all_orders") { echo 'class="panel active"'; }else { echo 'class="panel"';} ?>>
                    <a href="#" >
                        <i class="icon-dashboard"></i>&nbsp; Transaction Dashboard</a>                   
                </li>*/ ?>
                   <li <?php if($current_route=="merchant_deals_all_orders" || $current_route=="merchant_deals_success_orders" || $current_route=="merchant_deals_completed_orders" || $current_route=="merchant_deals_hold_orders" || $current_route=="merchant_deals_failed_orders"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav">
                        <i class="icon-resize-small"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.DEALS_TRANSACTION')!= '') ?  trans(Session::get('mer_lang_file').'.DEALS_TRANSACTION') : trans($MER_OUR_LANGUAGE.'.DEALS_TRANSACTION')); ?>

                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
	                </a>
                     <ul <?php if( $current_route == "merchant_deals_all_orders" || $current_route == "merchant_deals_success_orders" || $current_route == "merchant_deals_hold_orders" || $current_route == "merchant_deals_failed_orders"|| $current_route == "mer_deal_replacement_orders"|| $current_route == "mer_deal_return_orders"|| $current_route == "mer_deal_cancel_orders" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav">
                         <li <?php if( $current_route == "merchant_deals_all_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_deals_all_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.ALL_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.ALL_ORDERS') : trans($MER_OUR_LANGUAGE.'.ALL_ORDERS')); ?> </a></li>
                      <li <?php if( $current_route == "merchant_deals_success_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_deals_success_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SUCCESS_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.SUCCESS_ORDERS') : trans($MER_OUR_LANGUAGE.'.SUCCESS_ORDERS')); ?></a></li>
                   <!-- <li class=""><a href="<?php //echo url('merchant_deals_completed_orders');?>"><i class="icon-angle-right"></i><?php //if (Lang::has(Session::get('mer_lang_file').'.COMPLETED_ORDERS')!= '') { echo  trans(Session::get('mer_lang_file').'.COMPLETED_ORDERS');}  else { echo trans($MER_OUR_LANGUAGE.'.COMPLETED_ORDERS');} ?> </a></li>-->
                      <li <?php if( $current_route == "merchant_deals_hold_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href=" <?php echo e(url('merchant_deals_hold_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.HOLD_ORDERS')); ?></a></li>
                      <li <?php if( $current_route == "merchant_deals_failed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_deals_failed_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.FAILED_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.FAILED_ORDERS') : trans($MER_OUR_LANGUAGE.'.FAILED_ORDERS')); ?></a></li>


                       <li <?php if( $current_route == "mer_deal_cancel_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_deal_cancel_orders')); ?>"><i class="icon-angle-right"></i> Cancel Orders </a></li>
                         <li <?php if( $current_route == "mer_deal_return_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_deal_return_orders')); ?>"><i class="icon-angle-right"></i> Return Orders </a></li>
                         <li <?php if( $current_route == "mer_deal_replacement_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_deal_replacement_orders')); ?>"><i class="icon-angle-right"></i> Replacement Orders </a></li>



                    </ul>    
								
                </li>
                <li <?php if($current_route=="payu_deals_all_orders" || $current_route=="payu_deals_success_orders" || $current_route=="merchant_deals_completed_orders" || $current_route=="payu_deals_hold_orders" || $current_route=="mer_payu_deals_failed_orders"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav-ts">
                        <i class="icon-resize-small"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.DEALS_PAYUMONEY')!= '') ?  trans(Session::get('mer_lang_file').'.DEALS_PAYUMONEY') : trans($MER_OUR_LANGUAGE.'.DEALS_PAYUMONEY')); ?>

                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>
                     <ul <?php if( $current_route == "payu_deals_all_orders" || $current_route == "payu_deals_success_orders" || $current_route == "payu_deals_hold_orders" || $current_route == "mer_payu_deals_failed_orders"|| $current_route == "mer_payu_deal_replacement_orders"|| $current_route == "mer_payu_deal_return_orders"|| $current_route == "mer_payu_deal_cancel_orders" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav-ts">
                         <li <?php if( $current_route == "payu_deals_all_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('payu_deals_all_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.ALL_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.ALL_ORDERS') : trans($MER_OUR_LANGUAGE.'.ALL_ORDERS')); ?> </a></li>
                      <li <?php if( $current_route == "payu_deals_success_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('payu_deals_success_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SUCCESS_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.SUCCESS_ORDERS') : trans($MER_OUR_LANGUAGE.'.SUCCESS_ORDERS')); ?></a></li>
                   <!-- <li class=""><a href="<?php //echo url('merchant_deals_completed_orders');?>"><i class="icon-angle-right"></i><?php //if (Lang::has(Session::get('mer_lang_file').'.COMPLETED_ORDERS')!= '') { echo  trans(Session::get('mer_lang_file').'.COMPLETED_ORDERS');}  else { echo trans($MER_OUR_LANGUAGE.'.COMPLETED_ORDERS');} ?> </a></li>-->
                      <li <?php if( $current_route == "payu_deals_hold_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href=" <?php echo e(url('payu_deals_hold_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.HOLD_ORDERS')); ?></a></li>
                      <li <?php if( $current_route == "mer_payu_deals_failed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_deals_failed_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.FAILED_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.FAILED_ORDERS') : trans($MER_OUR_LANGUAGE.'.FAILED_ORDERS')); ?></a></li>


                       <li <?php if( $current_route == "mer_payu_deal_cancel_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_deal_cancel_orders')); ?>"><i class="icon-angle-right"></i> Cancel Orders </a></li>
                         <li <?php if( $current_route == "mer_payu_deal_return_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_deal_return_orders')); ?>"><i class="icon-angle-right"></i> Return Orders </a></li>
                         <li <?php if( $current_route == "mer_payu_deal_replacement_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_deal_replacement_orders')); ?>"><i class="icon-angle-right"></i> Replacement Orders </a></li>



                    </ul>    
                                
                </li>
                 <?php $general=DB::table('nm_generalsetting')->get();  ?>
                 <?php $__currentLoopData = $general; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                 <li <?php if($current_route=="merchant_dealscod_all_orders" ||  $current_route=="merchant_dealscod_completed_orders" || $current_route=="merchant_dealscod_hold_orders" || $current_route=="merchant_dealscod_hold_orders" ){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav1">
                        <i class="icon-money"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.DEALS_COD')!= '') ? trans(Session::get('mer_lang_file').'.DEALS_COD') : trans($MER_OUR_LANGUAGE.'.DEALS_COD')); ?>

                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
	                </a>
                     <ul <?php if( $current_route == "merchant_dealscod_all_orders" || $current_route == "merchant_dealscod_completed_orders" || $current_route == "merchant_dealscod_hold_orders" || $current_route == "merchant_dealscod_failed_orders"|| $current_route == "mer_dealcod_replacement_orders"|| $current_route == "mer_dealcod_cancel_orders"|| $current_route == "mer_dealcod_return_orders" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav1">
                         <li <?php if( $current_route == "merchant_dealscod_all_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_dealscod_all_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.ALL_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.ALL_ORDERS') : trans($MER_OUR_LANGUAGE.'.ALL_ORDERS')); ?></a></li>
                       <li <?php if( $current_route == "merchant_dealscod_completed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_dealscod_completed_orders')); ?>"><i class="icon-angle-right"></i>  <?php echo e((Lang::has(Session::get('mer_lang_file').'.SUCCESS_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.SUCCESS_ORDERS') : trans($MER_OUR_LANGUAGE.'.SUCCESS_ORDERS')); ?> </a></li>
                         <li <?php if( $current_route == "merchant_dealscod_hold_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_dealscod_hold_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HOLD_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.HOLD_ORDERS')); ?></a></li>
                        <li <?php if( $current_route == "merchant_dealscod_failed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_dealscod_failed_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.FAILED_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.FAILED_ORDERS') : trans($MER_OUR_LANGUAGE.'.FAILED_ORDERS')); ?></a></li>

                         <li <?php if( $current_route == "mer_dealcod_cancel_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_dealcod_cancel_orders')); ?>"><i class="icon-angle-right"></i> Cancel Orders </a></li>
                         <li <?php if( $current_route == "mer_dealcod_return_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_dealcod_return_orders')); ?>"><i class="icon-angle-right"></i> Return Orders </a></li>
                         <li <?php if( $current_route == "mer_dealcod_replacement_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_dealcod_replacement_orders')); ?>"><i class="icon-angle-right"></i> Replacement Orders </a></li>

                    </ul>                   
                </li><?php    // } ?>
				
                <li <?php if($current_route=="merchant_product_all_orders" || $current_route=="merchant_product_success_orders" || $current_route=="merchant_product_completed_orders" || $current_route=="merchant_product_failed_orders" || $current_route=="merchant_product_hold_orders" ){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav2">
                        <i class="icon-dropbox"></i>&nbsp; <?php echo e((Lang::has(Session::get('mer_lang_file').'.PRODUCTS_TRANSACTION')!= '') ?  trans(Session::get('mer_lang_file').'.PRODUCTS_TRANSACTION') : trans($MER_OUR_LANGUAGE.'.PRODUCTS_TRANSACTION')); ?>

                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
	                </a>
                     <ul <?php if( $current_route == "merchant_product_all_orders" || $current_route == "merchant_product_success_orders" || $current_route == "merchant_product_hold_orders" || $current_route == "merchant_product_failed_orders"|| $current_route == "mer_cancel_orders"|| $current_route == "mer_return_orders"|| $current_route == "mer_replacement_orders" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav2">
                         <li <?php if( $current_route == "merchant_product_all_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_all_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.ALL_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.ALL_ORDERS') : trans($MER_OUR_LANGUAGE.'.ALL_ORDERS')); ?> </a></li>
                      <li <?php if( $current_route == "merchant_product_success_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('merchant_product_success_orders');?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SUCCESS_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.SUCCESS_ORDERS') : trans($MER_OUR_LANGUAGE.'.SUCCESS_ORDERS')); ?></a></li>
                    <!--<li class=""><a href="<?php echo url('merchant_product_completed_orders');?>"><i class="icon-angle-right"></i><?php if (Lang::has(Session::get('mer_lang_file').'.COMPLETED_ORDERS')!= '') { echo  trans(Session::get('mer_lang_file').'.COMPLETED_ORDERS');}  else { echo trans($MER_OUR_LANGUAGE.'.COMPLETED_ORDERS');} ?> </a></li>-->
                      <li <?php if( $current_route == "merchant_product_hold_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_hold_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.HOLD_ORDERS')); ?></a></li>
                      <li <?php if( $current_route == "merchant_product_failed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_failed_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.FAILED_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.FAILED_ORDERS') : trans($MER_OUR_LANGUAGE.'.FAILED_ORDERS')); ?></a></li>

                       <li <?php if( $current_route == "mer_cancel_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_cancel_orders')); ?>"><i class="icon-angle-right"></i>Cancel Orders </a></li>
                         <li <?php if( $current_route == "mer_return_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_return_orders')); ?>"><i class="icon-angle-right"></i>Return Orders </a></li>
                         <li <?php if( $current_route == "mer_replacement_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_replacement_orders')); ?>"><i class="icon-angle-right"></i>Replacement Orders </a></li>


                    </ul>                   
                </li>
                <li <?php if($current_route=="merchant_payu_product_all_orders" || $current_route=="merchant_payu_product_success_orders" || $current_route=="merchant_product_completed_orders" || $current_route=="merchant_payu_product_failed_orders" || $current_route=="merchant_payu_product_hold_orders" ){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav-tp">
                        <i class="icon-dropbox"></i>&nbsp; <?php echo e((Lang::has(Session::get('mer_lang_file').'.PRODUCTS_PAYUMONEY')!= '') ?  trans(Session::get('mer_lang_file').'.PRODUCTS_PAYUMONEY') : trans($MER_OUR_LANGUAGE.'.PRODUCTS_PAYUMONEY')); ?>

                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>
                     <ul <?php if( $current_route == "merchant_payu_product_all_orders" || $current_route == "merchant_payu_product_success_orders" || $current_route == "merchant_payu_product_hold_orders" || $current_route == "merchant_payu_product_failed_orders"|| $current_route == "mer_payu_cancel_orders"|| $current_route == "mer_payu_return_orders"|| $current_route == "mer_payu_replacement_orders" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav-tp">
                         <li <?php if( $current_route == "merchant_payu_product_all_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_payu_product_all_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.ALL_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.ALL_ORDERS') : trans($MER_OUR_LANGUAGE.'.ALL_ORDERS')); ?> </a></li>
                      <li <?php if( $current_route == "merchant_payu_product_success_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('merchant_payu_product_success_orders');?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SUCCESS_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.SUCCESS_ORDERS') : trans($MER_OUR_LANGUAGE.'.SUCCESS_ORDERS')); ?></a></li>
                    <!--<li class=""><a href="<?php echo url('merchant_product_completed_orders');?>"><i class="icon-angle-right"></i><?php if (Lang::has(Session::get('mer_lang_file').'.COMPLETED_ORDERS')!= '') { echo  trans(Session::get('mer_lang_file').'.COMPLETED_ORDERS');}  else { echo trans($MER_OUR_LANGUAGE.'.COMPLETED_ORDERS');} ?> </a></li>-->
                      <li <?php if( $current_route == "merchant_payu_product_hold_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_payu_product_hold_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.HOLD_ORDERS')); ?></a></li>
                      <li <?php if( $current_route == "merchant_payu_product_failed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_payu_product_failed_orders')); ?>"><i class="icon-angle-right"></i><?php echo e((Lang::has(Session::get('mer_lang_file').'.FAILED_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.FAILED_ORDERS') : trans($MER_OUR_LANGUAGE.'.FAILED_ORDERS')); ?></a></li>

                       <li <?php if( $current_route == "mer_payu_cancel_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_cancel_orders')); ?>"><i class="icon-angle-right"></i>Cancel Orders </a></li>
                         <li <?php if( $current_route == "mer_payu_return_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_return_orders')); ?>"><i class="icon-angle-right"></i>Return Orders </a></li>
                         <li <?php if( $current_route == "mer_payu_replacement_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_payu_replacement_orders')); ?>"><i class="icon-angle-right"></i>Replacement Orders </a></li>


                    </ul>                   
                </li>
              <?php $general=DB::table('nm_generalsetting')->get(); foreach($general as $gs) {} //if($gs->gs_payment_status == 'COD') { ?>   <li <?php if($current_route=="merchant_product_cod_all_orders" || $current_route=="merchant_product_cod_success_orders" || $current_route=="merchant_product_cod_completed_orders" || $current_route=="merchant_product_cod_failed_orders" || $current_route=="merchant_product_cod_hold_orders" ){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav3">
                        <i class="icon-money"></i>&nbsp;<?php echo e((Lang::has(Session::get('mer_lang_file').'.PRODUCTS_COD')!= '') ?  trans(Session::get('mer_lang_file').'.PRODUCTS_COD') : trans($MER_OUR_LANGUAGE.'.PRODUCTS_COD')); ?>

                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
	                </a>
                     <ul <?php if( $current_route == "merchant_product_cod_all_orders" || $current_route == "merchant_product_cod_completed_orders" || $current_route == "merchant_product_cod_hold_orders" || $current_route == "merchant_product_cod_failed_orders" ||$current_route == "mer_cod_cancel_orders"||$current_route == "mer_cod_return_orders" ||$current_route == "mer_cod_replacement_orders"   ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav3">
                         <li <?php if( $current_route == "merchant_product_cod_all_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_cod_all_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.ALL_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.ALL_ORDERS') : trans($MER_OUR_LANGUAGE.'.ALL_ORDERS')); ?></a></li>
                       <li <?php if( $current_route == "merchant_product_cod_completed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_cod_completed_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SUCCESS_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.SUCCESS_ORDERS') : trans($MER_OUR_LANGUAGE.'.SUCCESS_ORDERS')); ?> </a></li>
                         <li <?php if( $current_route == "merchant_product_cod_hold_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_cod_hold_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.HOLD_ORDERS')!= '') ?  trans(Session::get('mer_lang_file').'.HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.HOLD_ORDERS')); ?></a></li>
                        <li <?php if( $current_route == "merchant_product_cod_failed_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('merchant_product_cod_failed_orders')); ?>"><i class="icon-angle-right"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.FAILED_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.FAILED_ORDERS') : trans($MER_OUR_LANGUAGE.'.FAILED_ORDERS')); ?></a></li>

                         <li <?php if( $current_route == "mer_cod_cancel_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_cod_cancel_orders')); ?>"><i class="icon-angle-right"></i>Cancel Orders </a></li>
                         <li <?php if( $current_route == "mer_cod_return_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_cod_return_orders')); ?>"><i class="icon-angle-right"></i>Return Orders </a></li>
                         <li <?php if( $current_route == "mer_cod_replacement_orders" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo e(url('mer_cod_replacement_orders')); ?>"><i class="icon-angle-right"></i>Replacement Orders </a></li>
                    </ul>                   
                </li> <?php //} ?>
            </ul>
            
             <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
               <?php /* <div class="media-body">
                    <h5 class="media-heading"> FUND REQUESTS</h5>
                    
                </div> */ ?>
                <br />
            </div>

           <?php /* <ul id="menu" class="collapse">
                <li class="panel">
                    <a href="<?php echo url('all_fund_request'); ?>" >
                        <i class="icon-arrow-right"></i>&nbsp; All Fund requests</a>                   
                </li>
                   <li class="panel ">
                    <a href="#">
                        <i class="icon-ok"></i>&nbsp;Approved Fund requests                         
	                </a>                                        
                </li>
                 <li class="panel ">
                    <a href="#">
                        <i class="icon-ban-circle"></i>&nbsp;Rejected Fund requests   
                    </a>
                 </li>
				
                <li class="panel">
                    <a href="#">
                        <i class="icon-ok-circle"></i>&nbsp;Success Fund requests   
                    </a>
                    
                </li>
                
                <li class="panel ">
                    <a href="#">
                        <i class="icon-mail-reply-all"></i>&nbsp;Failed Fund requests   
                    </a>
                </li>
            </ul>*/ ?>

			 
        </div>
        
        
<!---Right Click Block Code---->
<!--<script language="javascript">
document.onmousedown=disableclick;
status="Cannot Access for this mode";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;    
   }
}
</script>-->


<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>