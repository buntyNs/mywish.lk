
<?php echo $navbar; ?>

<!-- Navbar ================================================== -->
<?php echo $header; ?>

<!-- Header End====================================================================== -->
<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li> 
            <li><strong><?php echo e((Lang::has(Session::get('lang_file').'.HELP')!= '') ?  trans(Session::get('lang_file').'.HELP'): trans($OUR_LANGUAGE.'.HELP')); ?></strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<div id="mainBody faq_main">
<div class="container cms-page">
<?php if($cms_result){ 

foreach($cms_result as $cms) { 
if($cms->cp_title =='Help') { 
	if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
		$cp_title = 'cp_title';
		$cp_description = 'cp_description';
	}else {  
		$cp_title = 'cp_title_'.Session::get('lang_code'); 
		$cp_description = 'cp_description_'.Session::get('lang_code'); 
	}
$cms_desc = stripslashes($cms->$cp_description);  ?>
<h1 style="color:#ff8400;"><?php echo $cms->$cp_title; ?> </h1>
<legend></legend>
<div id="legalNotice">
	<?php echo $cms_desc; ?>

</div>
<?php } } } else { ?>
<h1 style="color:#ff8400;"></h1>

<div id="legalNotice" >
	 <?php if(Lang::has(Session::get('lang_file').'.NO_DATA_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_DATA_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_DATA_FOUND')); ?> <?php endif; ?> !
	</div>	
</div>
<?php }  ?>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ======================================== -->

	<?php echo $footer; ?>	

</body>
</html>