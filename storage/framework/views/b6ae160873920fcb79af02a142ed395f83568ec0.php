<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')); ?>  |<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FUND_REQUEST_REPORT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FUND_REQUEST_REPORT') : trans($MER_OUR_LANGUAGE.'.MER_FUND_REQUEST_REPORT')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="<?php echo e(url('')); ?>/public/assets/css/datepicker.css" rel="stylesheet" />	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />

	

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53">

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        
        
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                
    <div class="inner"> 
      <div class="row"> 
        <div class="col-lg-12"> 
          <ul class="breadcrumb">
            <li class=""><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
            <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COMMSSION_TO_PAID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMSSION_TO_PAID') : trans($MER_OUR_LANGUAGE.'.MER_COMMSSION_TO_PAID')); ?></a></li>
          </ul>
        </div>
      </div>
      <div class="row"> 

        <div class="col-lg-12">
          <?php if($session_msg!=''): ?>

            <div class="alert alert-danger alert-dismissable"  > <?php echo e($session_msg); ?>

			<?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

               
            </div>
          <?php endif; ?> 
        </div>

        <div class="col-lg-12"> 
          <div class="box dark"> <header> 
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COMMSSION_TO_PAID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMSSION_TO_PAID') : trans($MER_OUR_LANGUAGE.'.MER_COMMSSION_TO_PAID')); ?></h5>
            </header> 
            <div class="row">
                <div class="col-lg-12">                     

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                             <div class="panel_marg_clr ppd">        
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO')); ?></th>
                                            <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_NAME')  : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_NAME')); ?></th>
                                            <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION_ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TRANSACTION_ID'): trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION_ID')); ?></th>
                                            <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT') :  trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')); ?></th>
                                            <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DATE')  :  trans($MER_OUR_LANGUAGE.'.MER_DATE')); ?></th>
                                             <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_TYPE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PAYMENT_TYPE')  : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_TYPE')); ?></th>
                                             <th><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STATUS')   : trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; ?>
                                     <?php if(count($commissionPaidDetails)>0): ?>
									                   <?php $__currentLoopData = $commissionPaidDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $funddetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo e($i); ?></td>
                                            <td><?php echo e($funddetails->com_mer_name); ?> </td>
                                            <td><?php echo e($funddetails->transaction_id); ?> </td>
                                            <td class="center"> <?php echo e($funddetails->paidAmount); ?></td>
                                            <td class="center"><?php echo e($funddetails->com_date); ?></td>
                                            <td class="center"> <?php if($funddetails->payment_type ==0): ?> OFFLINE <?php elseif($funddetails->com_status ==1): ?> ONLINE <?php else: ?> INVALID <?php endif; ?></td>
                                            <td class="center"> <?php if($funddetails->com_status ==0): ?> HOLD <?php elseif($funddetails->com_status ==1): ?> PAID <?php else: ?> INVALID <?php endif; ?></td>
                                        </tr>
                                       <?php $i++; ?>
									   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									   <?php endif; ?>
                                    </tbody>
                                </table></div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   <?php echo $adminfooter; ?>

    <!--END FOOTER -->

	


    
    
    
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
