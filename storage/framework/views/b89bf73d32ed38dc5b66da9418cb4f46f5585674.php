<?php echo $navbar; ?>

<!-- Navbar ================================================== -->
<?php echo $header; ?>

<!-- Header -->
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]--> 
<div id="page">
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <ul>
               <li class="home"> <a title="" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li>
               <li class=""> <a title="" href="<?php echo e(url('products')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '') ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?></a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<!-- Breadcrumbs End --> 
<!-- Main Container -->
<center>
   <?php if(Session::has('success1')): ?>
   <div class="alert alert-warning alert-dismissable"><?php echo Session::get('success1'); ?>

      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   </div>
   <?php endif; ?>
</center>
<div class="main-container col1-layout">
   <div class="container">
      <div class="row">
         <div class="col-main">
            <div class="product-view-area">
               <?php if(count($product_details_by_id) > 0): ?>
               <?php $__currentLoopData = $product_details_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_details_by_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
               
               <?php if((Session::get('lang_code'))=='' || (Session::get('lang_code'))== 'en'): ?>  
               <?php    $pro_title = 'pro_title';
               $pro_mdesc = 'pro_mdesc';
               $pro_mkeywords = 'pro_mkeywords'; ?>
               <?php else: ?>   
               <?php   $pro_title = 'pro_title_'.Session::get('lang_code');
               $pro_mdesc = 'pro_mdesc_'.Session::get('lang_code');
               $pro_mkeywords = 'pro_mkeywords_'.Session::get('lang_code'); ?>
               <?php endif; ?> 
               
               <?php   $product_img= explode('/**/',trim($pro_details_by_id->pro_Img,"/**/")); 
               $img_count = count($product_img); ?>
               <?php   $product_image     = $product_img[0];
               $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
               $img_data   = "public/assets/product/".$product_image; ?>
               <?php if($product_img !='' && $img_count !=''): ?>
               <?php if(file_exists($img_data) && $product_image !=''): ?>   <!-- //product image is not null and exists in folder -->
               <?php  $prod_path = url('').'/public/assets/product/' .$product_image;   ?>               
               <?php else: ?>  
               <?php if(isset($DynamicNoImage['productImg'])): ?>
               <?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>
               <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>    <!-- //no image for product is not null and exists in folder -->
               <?php   $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>
               <?php endif; ?>
               <?php endif; ?>
               <?php else: ?>  
               <?php if(isset($DynamicNoImage['productImg'])): ?>
               <?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>
               <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>    <!-- //no image for product is not null and exists in folder -->
               <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>
               <?php endif; ?>
               <?php endif; ?> 
               
               <?php    $alt_text   = substr($pro_details_by_id->$pro_title,0,25);
               $alt_text  .= strlen($pro_details_by_id->$pro_title)>25?'..':''; ?>
               
               <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
               <?php  $pro_desc = 'pro_desc'; ?>
               <?php else: ?>
               <?php  
               $pro_desc = 'pro_desc_'.Session::get('lang_code'); ?> 
               <?php endif; ?>
               
               <?php
               $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;
               $multiple_countone   = $one_count *1;
               $multiple_counttwo   = $two_count *2;
               $multiple_countthree = $three_count *3;
               $multiple_countfour  = $four_count *4;
               $multiple_countfive  = $five_count *5;
               $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>
               
               <?php $delivery_date = '+'.$pro_details_by_id->pro_delivery.'days'; ?>
               <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                  
                  <div class="large-image"> 
                     <a href="<?php echo e($prod_path); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> 
                     <img class="zoom-img" src="<?php echo e($prod_path); ?>" alt="products"> 
                     </a>
                  </div>
                  <div class="flexslider flexslider-thumb">
                     <ul class="previews-list slides">
                        <?php for($i=0;$i <$img_count;$i++): ?>
                        <?php  $product_image     = $product_img[$i];
                        $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_image; ?>
                        <?php if(file_exists($img_data) && $product_image !=''): ?> <!-- //product image is not null and exists in folder -->
                        <?php $prod_path = url('').'/public/assets/product/' .$product_image;  ?>                 
                        <?php else: ?>  
                        <?php if(isset($DynamicNoImage['productImg'])): ?>
                        <?php   $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>
                        <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?> <!-- //no image for product is not null and exists in folder -->
                        <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?> 
                        <li style="width: 100px; float: left; display: block;">
                           <a href='<?php echo e($prod_path); ?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo e($prod_path); ?>' "><img src="<?php echo e($prod_path); ?>" alt = "Thumbnail 2"/></a>    
                        </li>
                        <?php endfor; ?>
                     </ul>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
                  <div class="product-name">
                     <h1><?php echo e($pro_details_by_id->$pro_title); ?></h1>
                  </div>
                  <div class="price-box">
                     <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo e(Helper::cur_sym()); ?> <?php echo e($pro_details_by_id->pro_disprice); ?></span> </p>
                     <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo e(Helper::cur_sym()); ?> <?php echo e($pro_details_by_id->pro_price); ?></span> 
                        <?php if($pro_details_by_id->pro_discount_percentage!=''): ?> 
                        <span class="special-price">(<?php echo round($pro_details_by_id->pro_discount_percentage);?>% off)</span>
                        <?php endif; ?>
                     </p>
                  </div>
                  <div class="ratings">
                     <div class="rating"> 
                        <?php if($product_count): ?>
                        <?php  $product_divide_count = $product_total_count / $product_count;
                        $product_divide_count = round($product_divide_count); ?>
                        <?php if($product_divide_count <= '1'): ?> 
                        <img src='<?php echo e(url("./images/stars-1.png")); ?>' style='margin-bottom:10px;'>
                        <?php elseif($product_divide_count >= '1'): ?> 
                        <img src='<?php echo e(url("./images/stars-1.png")); ?>' style='margin-bottom:10px;'> 
                        <?php elseif($product_divide_count >= '2'): ?>
                        <img src='<?php echo e(url("./images/stars-2.png")); ?>' style='margin-bottom:10px;'>  
                        <?php elseif($product_divide_count >= '3'): ?>
                        <img src='<?php echo e(url("./images/stars-3.png")); ?>' style='margin-bottom:10px;'> 
                        <?php elseif($product_divide_count >= '4'): ?> 
                        <img src='<?php echo url('./images/stars-4.png'); ?>' style='margin-bottom:10px;'> 
                        <?php elseif($product_divide_count >= '5'): ?>
                        <img src='<?php echo url('./images/stars-5.png'); ?>' style='margin-bottom:10px;'>
                        <?php else: ?>
                        <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php endif; ?>
                        <?php endif; ?>      
                        <?php elseif($product_count): ?>
                        <?php  $product_divide_count = $product_total_count / $product_count; ?>
                        <?php else: ?>
                        <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php endif; ?>
                        <?php endif; ?>  
                     </div>
                     <p class="rating-links"> <?php echo e($count_review_rating); ?> <?php if(Lang::has(Session::get('lang_file').'.REVIEWS_AND_RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEWS_AND_RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEWS_AND_RATINGS')); ?> <?php endif; ?>  </p>
                     <p class="availability in-stock pull-right"> 
                        <?php if(Lang::has(Session::get('lang_file').'.AVAILABLE_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.AVAILABLE_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.AVAILABLE_STOCK')); ?> <?php endif; ?>: <span>
                        <?php echo e($pro_details_by_id->pro_qty-$pro_details_by_id->pro_no_of_purchase); ?>

                        <?php if(Lang::has(Session::get('lang_file').'.IN_STOCK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.IN_STOCK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.IN_STOCK')); ?> <?php endif; ?>
                        </span>
                     </p>
                  </div>
                  <div class="short-description">
                     <h2><?php if(Lang::has(Session::get('lang_file').'.OVERVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OVERVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OVERVIEW')); ?> <?php endif; ?></h2>
                     <p><?php if($pro_details_by_id->$pro_desc != ''): ?>
                        <?php echo html_entity_decode(substr($pro_details_by_id->$pro_desc,0,200)); ?>

                        <?php endif; ?> 
                     </p>
                  </div>
                  <?php if($GENERAL_SETTING->gs_store_status != 'Store'): ?>
                  <div class="store-name">
                     <h5><?php if(Lang::has(Session::get('lang_file').'.STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORE_NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORE_NAME')); ?> <?php endif; ?></h5>
                     <p>
                        <?php $__currentLoopData = $get_store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $storerow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                        <?php  $stor_name = 'stor_name';  ?>
                        <?php else: ?>   
                        <?php  $stor_name = 'stor_name_'.Session::get('lang_code'); ?> 
                        <?php endif; ?>
                        <?php echo e($storerow->$stor_name); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </p>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>
                  <div>
                     <?php $__currentLoopData = $product_details_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                     <?php if($row->cash_pack != 0): ?> 
                     <b><?php if(Lang::has(Session::get('lang_file').'.CASHBACK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CASHBACK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CASHBACK')); ?> <?php endif; ?> :</b>  
                     <?php echo e(Helper::cur_sym()); ?> <?php echo e($row->cash_pack); ?> 
                     <?php elseif($row->cash_pack == ""): ?> 
                     <?php elseif($row->cash_pack == 0): ?> <?php endif; ?>
                     <?php $count = $pro_details_by_id->pro_qty - $pro_details_by_id->pro_no_of_purchase; ?>
                     <p class="left-label">
                        <?php if(isset($coupon_details)): ?>
                        <?php  $current_date = date('Y-m-d H:i');
                        $start_date = $coupon_details->start_date;
                        $end_date = $coupon_details->end_date;
                        $new_start_date = date("Y-m-d H:i", strtotime($start_date));
                        $new_end_date = date("Y-m-d H:i", strtotime($end_date)); ?>
                        <?php if(($new_start_date <= $current_date) && ($new_end_date >= $current_date)): ?>
                        <?php $total_of_product_count = $productview_check_coupon_purchase_count_cod + $productview_check_coupon_purchase_count_paypal; ?>
                        <?php if(($productview_check_coupon_purchase_count_cod >= $coupon_details->coupon_per_product) || ($productview_check_coupon_purchase_count_paypal >= $coupon_details->coupon_per_product) || ($total_of_product_count >= $coupon_details->coupon_per_product)): ?> 
                        <?php else: ?>
                        <?php if(Session::has('customerid')): ?>
                        <?php $total_coupon_of_user = $productview_check_coupon_purchase_count_cod_user + $productview_check_coupon_purchase_count_paypal_user; ?>
                        <?php if(($productview_check_coupon_purchase_count_cod_user >= $coupon_details->coupon_per_user) || ($productview_check_coupon_purchase_count_paypal_user >= $coupon_details->coupon_per_user) || ($total_coupon_of_user >= $coupon_details->coupon_per_user) ): ?>
                        <?php echo e("Your Coupon Limit Exit"); ?>

                        <?php else: ?> 
                     <div style="padding-right:0;margin-top:5px;font-size:18px; color:#FF5722; font-weight: bold;">  
                        <?php echo e((Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '') ? trans(Session::get('lang_file').'.COUPON_CODE') : trans($OUR_LANGUAGE.'.COUPON_CODE')); ?> :
                        &nbsp
                        <?php echo e($coupon_details->coupon_code); ?>

                     </div>
                     <?php endif; ?> 
                     <?php else: ?> 
                     <a href="" role="button" data-toggle="modal" data-target="#loginpop" style="padding-right:0; margin-top:5px;"><button class="wish-but" id="login_a_click"  value="<?php echo e((Lang::has(Session::get('lang_file').'.LOGIN')!= '') ? trans(Session::get('lang_file').'.LOGIN') : trans($OUR_LANGUAGE.'.LOGIN')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.CLICK_TO_VIEW_COUPON_CODE')!= '') ?  trans(Session::get('lang_file').'.CLICK_TO_VIEW_COUPON_CODE') : trans($OUR_LANGUAGE.'.CLICK_TO_VIEW_COUPON_CODE')); ?> </button></a>
                     <?php endif; ?> 
                     <?php endif; ?> <!-- //else -->
                     <?php endif; ?>
                     <?php endif; ?>      
                  </div>
                  <?php echo Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')); ?>

                  <?php if($count > 0): ?>
                  <div class="product-color-size-area">
                     <?php if(count($product_color_details)>0): ?>
                     <div class="color-area">
                        <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COLOR')); ?> <?php endif; ?> </h2>
                        <div class="color">
                           <select name="addtocart_color" id="addtocart_color" required>
                              <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COLOR')); ?> <?php endif; ?>--</option>
                              <?php $__currentLoopData = $product_color_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                              <option value="<?php echo e($product_color_det->co_id); ?>">
                                 <?php echo e($product_color_det->co_name); ?>

                              </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                           </select>
                        </div>
                     </div>
                     <?php endif; ?>
                     <?php if(count($product_size_details) > 0): ?>
                     <?php  $size_name = $product_size_details[0]->si_name;
                     $return  = strcmp($size_name,'no size');  ?>
                     <?php if($return!=0): ?> 
                     <div class="size-area">
                        <h2 class="saider-bar-title"><?php if(Lang::has(Session::get('lang_file').'.SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIZE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIZE')); ?> <?php endif; ?></h2>
                        <div class="size">
                           <select name="addtocart_size" id="addtocart_size" required>
                              <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_SIZE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_SIZE')); ?> <?php endif; ?>--</option>
                              <?php $__currentLoopData = $product_size_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                              <option value="<?php echo e($product_size_det->ps_si_id); ?>">
                                 <?php echo e($product_size_det->si_name); ?>

                              </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                     </div>
                     <?php else: ?> 
                     <input type="hidden" name="addtocart_size" value="<?php echo e($product_size_details[0]->ps_si_id); ?>">
                     <?php endif; ?>
                     <?php endif; ?>
                  </div>
                  <?php endif; ?>
                  <div class="product-variation">
                     <?php echo Form::open(array('url' => 'addtocart','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')); ?>

                     <?php if($count > 0): ?>
                     <div class="cart-plus-minus">
                        <label for="qty"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?> :</label>
                        <div class="numbers-row">
                           <div onClick="remove_quantity()" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                           <input type="number" class="qty" min="1" value="1" max="<?php echo e(($pro_details_by_id->pro_qty - $pro_details_by_id->pro_no_of_purchase)); ?>" id="addtocart_qty" name="addtocart_qty" readonly required >
                           <div onClick="add_quantity()" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                        </div>
                     </div>
                     <?php endif; ?>
                     
                     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                     <?php echo e(Form::hidden('addtocart_type','product')); ?>

                     <?php echo e(Form::hidden('addtocart_pro_id',$pro_details_by_id->pro_id)); ?>

                     <?php if(count($product_size_details) > 0): ?>
                     <!-- <input type="hidden" name="addtocart_size" value="<?php echo e($product_size_details[0]->ps_si_id); ?>">-->
                     <?php endif; ?>
                     <?php if(count($product_color_details) > 0): ?>
                     <!-- <input type="hidden" name="addtocart_color" value="<?php echo e($product_color_details[0]->co_id); ?>">-->
                     <?php endif; ?>
                     <?php if(Session::has('customerid')): ?> 
                     <?php if($count > 0): ?>
                     <button class="button pro-add-to-cart" title="Add to Cart" type="submit" id="add_to_cart_session"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span></button>
                     <?php else: ?> 
                     <button type="button" class="btn btn-danger">
                      
                     <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>
                     </button>  
                    
                     <?php endif; ?> 

                     <?php else: ?>

                     <?php if($count > 0): ?>
                     <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                     <button type="button" class=" button pro-add-to-cart">
                     <span><i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                     <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span>
                     </button> 
                     </a>
                     <?php else: ?>
                     <button type="button" class="btn btn-danger">
                        
                     <?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>
                     </button> 
                     

                     <?php endif; ?> 

                     <?php endif; ?>

                     <?php echo e(Form::close()); ?>

                  </div>
                  <div class="product-cart-option">
                     <ul>
                        <li>
                           
                           <?php if($count > 0): ?>  
                           <?php if(Session::has('customerid')): ?>
                           <?php if(count($prodInWishlist)==0): ?>
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                           <?php echo e(Form::hidden('pro_id','$pro_details_by_id->pro_id')); ?>        
                           <!-- <input type="hidden" name="pro_id" value="<?php echo $pro_details_by_id->pro_id; ?>"> -->
                           <input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
                           <a href="" onclick="addtowish(<?php echo e($pro_details_by_id->pro_id); ?>,<?php echo e(Session::get('customerid')); ?>)">
                           <input type="hidden" id="wishlisturl" value="<?php echo e(url('user_profile?id=4')); ?>">
                           <i class="fa fa-heart-o" aria-hidden="true"></i><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST')); ?> <?php endif; ?> 
                           </a>
                           <?php else: ?>
                           <?php /* remove wishlist */?>   
                           <a href="<?php echo url('remove_wish_product').'/'.$prodInWishlist->ws_id; ?>">
                           <i class="fa fa-heart" aria-hidden="true"></i>    <?php if(Lang::has(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REMOVE_FROM_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REMOVE_FROM_WISHLIST')); ?> <?php endif; ?>
                           </a> 
                           <?php /*remove link:remove_wish_product/wishlist table_id*/ ?>
                           <?php endif; ?>  
                           <?php else: ?> 
                           <a href="" role="button" data-toggle="modal" data-target="#loginpop">
                           <i class="fa fa-heart-o" aria-hidden="true"></i> <?php if(Lang::has(Session::get('lang_file').'.ADD_TO_WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_WISHLIST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_WISHLIST')); ?> <?php endif; ?>
                           </a>
                           <?php endif; ?>
                           <?php endif; ?>
                        </li>
                        
                     </ul>
                  </div>
                  <div class="pro-tags">
                     <div class="pro-tags-inner">
                        <div class="pro-tags-title"><?php if(Lang::has(Session::get('lang_file').'.DELIVERY_WITH_IN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DELIVERY_WITH_IN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DELIVERY_WITH_IN')); ?> <?php endif; ?>:</div>
                        <?php echo e(date('D d, M Y', strtotime($delivery_date))); ?>

                     </div>
                  </div>
				
				
				   <div class="pro-tags">
                     <div class="pro-tags-inner">
                        <div class="pro-tags-title"><?php if(Lang::has(Session::get('lang_file').'.SHARE_THIS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHARE_THIS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SHARE_THIS')); ?> <?php endif; ?>:</div>
						
						 <?php $__currentLoopData = $product_details_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                     <?php $product_image = explode('/**/',$product_det->pro_Img);
                       $product_img    = $product_image[0]; ?>
                     <?php if($product_img != ''): ?>
                       <?php $prod_path = url('').'/public/assets/product/'.$product_img; ?>                    
                     <?php else: ?>
                       <?php $prod_path  = url('').'/public/assets/default_image/No_image_product.png'; ?>

                     <?php endif; ?>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e($prod_path); ?>">
							<img width="32px" height="30px" src="<?php echo e(url('')); ?>/public/themes/image/Socialmedia/facebook.png" title="share in facebook">
						</a>
						
						<a target="_blank" href="https://twitter.com/home?status=<?php echo e(Request::url()); ?>')">
							<img width="32px" height="30px" src="<?php echo e(url('')); ?>/public/themes/image/Socialmedia/twitter.png" title="share in twitter">
						</a>
						
						  <!-- Share in Google + -->
						<a target="_blank" href="https://plus.google.com/share?url=<?php echo e($prod_path); ?>">
                         <img src="<?php echo e(url('')); ?>/public/themes/image/Socialmedia/google.png" width="30" title="share in G+">
                      </a>
                      
                      

                  <!-- Share in Linked In -->
                  <!--<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo e(Request::url()); ?>">-->
                      <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo e($prod_path); ?>">
                       <img src="<?php echo e(url('')); ?>/public/themes/image/Socialmedia/linkdin.png" width="30" title="share in LinkedIn">
                  </a>
						
						
						
                     </div>
                  </div>
				  
				
				  
                  
                  <?php if($pro_details_by_id->allow_cancel=='1' || $pro_details_by_id->allow_return=='1' || $pro_details_by_id->allow_replace=='1'): ?>
                  <div class="share-box">
                     <div class="title"><?php if(Lang::has(Session::get('lang_file').'.POLICY_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.POLICY_DETAILS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.POLICY_DETAILS')); ?> <?php endif; ?> :</div>
                     <div class="socials-box">
                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                        <?php   $cancel_policy  = 'cancel_policy';
                        $return_policy  = 'return_policy';
                        $replace_policy = 'replace_policy'; ?>
                        <?php else: ?>   
                        <?php    $cancel_policy  = 'cancel_policy_'.Session::get('lang_code'); 
                        $return_policy  = 'return_policy_'.Session::get('lang_code'); 
                        $replace_policy = 'replace_policy_'.Session::get('lang_code');  ?>
                        <?php endif; ?> 
                        <div class="cancel-policy">
                           <?php if($pro_details_by_id->allow_cancel=='1'): ?>
                           <p class="policy-title"> <?php if($pro_details_by_id->cancel_days>0): ?> 
                              <?php echo e($pro_details_by_id->cancel_days); ?>

                              <?php if($pro_details_by_id->cancel_days>1): ?> 
                              <?php echo e((Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS')); ?>

                              <?php else: ?> 
                              <?php echo e((Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY')); ?>

                              <?php endif; ?>
                              <?php if(Lang::has(Session::get('lang_file').'.CANCELLATION_ONLY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CANCELLATION_ONLY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CANCELLATION_ONLY')); ?> <?php endif; ?>
                              <span class="policy-quest" id="policyclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                              <br>
                              <?php if($pro_details_by_id->$cancel_policy!=''): ?>
                           <div class="policy-container dev_cancel" style="display: none;">
                              <p><?php echo $pro_details_by_id->$cancel_policy; ?>   </p>
                           </div>
                           <?php endif; ?>
                           <?php endif; ?>
                           </p>
                           <?php endif; ?>
                           <?php if($pro_details_by_id->allow_return=='1'): ?>
                           <p class="policy-title"> <?php if($pro_details_by_id->return_days>0): ?> 
                              <?php echo e($pro_details_by_id->return_days); ?>

                              <?php if($pro_details_by_id->return_days>1): ?> 
                              <?php echo e((Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS')); ?>

                              <?php else: ?> 
                              <?php echo e((Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY')); ?>

                              <?php endif; ?>
                              <?php if(Lang::has(Session::get('lang_file').'.RETURN_ONLY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RETURN_ONLY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RETURN_ONLY')); ?> <?php endif; ?>
                              <span class="policy-quest" id="returnclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                              <br>
                              <?php if($pro_details_by_id->$return_policy!=''): ?>
                           <div class="policy-container dev_return" style="display: none;"> 
                              <?php echo $pro_details_by_id->$return_policy; ?>    
                           </div>
                           <?php endif; ?>    
                           <?php endif; ?>
                           </p>
                           <?php endif; ?> 
                           <?php if($pro_details_by_id->allow_replace=='1'): ?>
                           <p class="policy-title"> <?php if($pro_details_by_id->replace_days>0): ?> 
                              <?php echo e($pro_details_by_id->replace_days); ?>

                              <?php if($pro_details_by_id->replace_days>1): ?> 
                              <?php echo e((Lang::has(Session::get('lang_file').'.DAYS')!= '') ? trans(Session::get('lang_file').'.DAYS') : trans($OUR_LANGUAGE.'.DAYS')); ?>

                              <?php else: ?> 
                              <?php echo e((Lang::has(Session::get('lang_file').'.DAY')!= '') ? trans(Session::get('lang_file').'.DAY') : trans($OUR_LANGUAGE.'.DAY')); ?>

                              <?php endif; ?>
                              <?php if(Lang::has(Session::get('lang_file').'.REPLACEMENT_ONLY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REPLACEMENT_ONLY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REPLACEMENT_ONLY')); ?> <?php endif; ?>
                              <span class="policy-quest" id="replaceclick"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></span>
                              <br>
                              <?php if($pro_details_by_id->$replace_policy!=''): ?>
                           <div class="policy-container dev_replace" style="display: none;"> 
                              <?php echo $pro_details_by_id->$replace_policy; ?>    
                           </div>
                           <?php endif; ?>    
                           <?php endif; ?>
                           </p>                        
                           <?php endif; ?>    
                        </div>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
         </div>
         
         <?php endif; ?> 
         <div class="product-overview-tab">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="product-tab-inner">
                        <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                           <li class="active"> <a href="#description" data-toggle="tab"> <?php if(Lang::has(Session::get('lang_file').'.DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DESCRIPTION')); ?> <?php endif; ?></a> </li>
                           <li> <a href="#specification" data-toggle="tab"><?php if(Lang::has(Session::get('lang_file').'.SPECIFICATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SPECIFICATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SPECIFICATION')); ?> <?php endif; ?></a> </li>
                           <li><a href="#reviews" data-toggle="tab"><?php if(Lang::has(Session::get('lang_file').'.REVIEWS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEWS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEWS')); ?> <?php endif; ?></a></li>
                           
                        </ul>
                        <div id="productTabContent" class="tab-content">
                           <div class="tab-pane fade in active" id="description" style="max-height: 300px;overflow-y: auto;">
                              <div class="std">
                                 <p> <?php echo $pro_details_by_id->$pro_desc; ?> </p>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="specification">
                              <div >
                                 <div >
                                    <form  action="#" method="get">
                                       <div >
                                          <?php if(count($product_specGroupList) > 0): ?>
                                          <?php $__currentLoopData = $product_specGroupList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $specgp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <?php if(count($product_spec_details[$specgp->spg_id])>0): ?>
                                          <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                                          <?php    $spg_name = 'spg_name';
                                          $sp_name = 'sp_name';
                                          $spc_value = 'spc_value'; ?>
                                          <?php else: ?>   
                                          <?php    $spg_name = 'spg_name_'.Session::get('lang_code'); 
                                          $sp_name = 'sp_name_'.Session::get('lang_code');
                                          $spc_value = 'spc_value_langCode'; ?>
                                          <?php endif; ?>
                                          <ul>
                                             <li>
                                                <?php echo e($specgp->$spg_name); ?>

                                                <br> 
                                                <?php $__currentLoopData = $product_spec_details[$specgp->spg_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prodSpec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($prodSpecAr[$prodSpec->sp_id]): ?> 
                                                <div class="spec-top">
                                                   <?php echo e($prodSpec->$sp_name); ?>: 
                                                   <?php $i=1; ?>
                                                   <?php $__currentLoopData = $prodSpecAr[$prodSpec->sp_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spcVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <span class=""> <?php echo e($spcVal->$spc_value); ?> 
                                                   <?php if($i!= count($prodSpecAr[$prodSpec->sp_id])): ?>
                                                   <?php $i++; ?>
                                                   <?php endif; ?> </span>
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <?php endif; ?>    
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                             </li>
                                          </ul>
                                          <?php endif; ?>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <?php else: ?>
                                          <p> <?php if(Lang::has(Session::get('lang_file').'.NO_SPECIFICATION_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_SPECIFICATION_AVAILABLE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_SPECIFICATION_AVAILABLE')); ?> <?php endif; ?>
                                          </p>
                                          <?php endif; ?>
                                       </div>
                                    </form>
                                 </div>
                                 <!--tags-->
                              </div>
                           </div>
                           <div id="reviews" class="tab-pane fade">
                              <div class="col-sm-5 col-lg-5 col-md-5">
                                 <div class="reviews-content-left">
                                    <h2><?php if(Lang::has(Session::get('lang_file').'.CUST_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CUST_REVIEW')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CUST_REVIEW')); ?> <?php endif; ?></h2>
                                    <?php if(count($review_details)!=0): ?>                   
                                    <?php $r=0; 
                                    ?>
                                    <?php $__currentLoopData = $review_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $col): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php  $customer_name = $col->cus_name;
                                    $customer_mail = $col->cus_email;
                                    $customer_img = $col->cus_pic;
                                    $review_title = $col->title;
                                    $customer_comments = $col->comments;
                                    $customer_date = $col->review_date;
                                    $customer_product = $col->product_id;
                                    $final_date = date('M j, Y', strtotime($col->review_date) );
                                    $customer_title = $col->title;
                                    $customer_name_arr = str_split($customer_name);
                                    $start_letter = strtolower($customer_name_arr[0]);
                                    $customer_ratings = $col->ratings;
                                    $r++; ?> 
                                    <div class="review-ratting">
                                       <p style="font-weight: 600;"> <?php echo e($review_title); ?> by <span style="color:#e83f33;"><?php echo e($customer_name); ?></span></p>
                                       <table>
                                          <tbody>
                                             <tr>
                                                <th><?php if(Lang::has(Session::get('lang_file').'.RATING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATING')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATING')); ?> <?php endif; ?></th>
                                                <td>
                                                   <div class="rating"> 
                                                      <?php if($customer_ratings=='1'): ?>
                                                      <img src='<?php echo e(url('./images/stars-1.png')); ?>' style=''> 
                                                      <?php elseif($customer_ratings=='2'): ?>
                                                      <img src='<?php echo e(url('./images/stars-2.png')); ?>' style=''> 
                                                      <?php elseif($customer_ratings=='3'): ?>
                                                      <img src='<?php echo e(url('./images/stars-3.png')); ?>' style=''> 
                                                      <?php elseif($customer_ratings=='4'): ?>
                                                      <img src='<?php echo e(url('./images/stars-4.png')); ?>' style=''> 
                                                      <?php elseif($customer_ratings=='5'): ?>
                                                      <img src='<?php echo e(url('./images/stars-5.png')); ?>' style=''>
                                                      <?php endif; ?>
                                                   </div>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       <p class=""> <?php echo e($customer_comments); ?>.<small> (Posted on <?php echo e($final_date); ?>)</small> </p>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php endif; ?> <?php endif; ?>
                                 </div>
                              </div>
                              <div class="col-sm-7 col-lg-7 col-md-7">
                                 <div class="reviews-content-right">
                                    <h2><?php if(Lang::has(Session::get('lang_file').'.WRITE_A_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WRITE_A_REVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WRITE_A_REVIEW')); ?> <?php endif; ?></h2>
                                    <?php echo Form::open(array('url'=>'productcomments','class'=>'form-horizontal loginFrm')); ?>

                                    
                                    <h4><?php if(Lang::has(Session::get('lang_file').'.RATE_FOR_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATE_FOR_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATE_FOR_PRODUCT')); ?> <?php endif; ?><em>*</em></h4>
                                    
                                    <div class="table-responsive reviews-table">
                                       <fieldset class="rating disableRating addReviews">
                                          <input id="review_ratings_5" name="ratings" type="radio" value="5" > 
                                          <label for="review_ratings_5" title="Awesome - 5 stars"> </label>
                                          <input id="review_ratings_4" name="ratings" type="radio" value="4" > 
                                          <label for="review_ratings_4" title="Pretty good - 4 stars"> </label>
                                          <input id="review_ratings_3" name="ratings" type="radio" value="3" > 
                                          <label for="review_ratings_3" title="Not bad - 3 stars"> </label>
                                          <input id="review_ratings_2" name="ratings" type="radio" value="2" > 
                                          <label for="review_ratings_2" title="Bad - 2 stars"> </label>
                                          <input id="review_ratings_1" name="ratings" type="radio" value="1" > 
                                          <label for="review_ratings_1" title="Very Bad - 1 stars"> </label>
                                       </fieldset >
                                    </div>
                                    <div class="form-area">
                                       <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                       <input type="hidden" name="customer_id" value="<?php echo Session::get('customerid');?>">
                                       <input type="hidden" name="product_id" value="<?php echo $pro_details_by_id->pro_id; ?>">
                                       <div class="form-element">
                                          <label><?php if(Lang::has(Session::get('lang_file').'.REVIEW_SUMMARY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEW_SUMMARY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEW_SUMMARY')); ?> <?php endif; ?><em>*</em></label>
                                          <input type="text" name="title" value="" placeholder="<?php if(Lang::has(Session::get('lang_file').'.REVIEW_TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEW_TITLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEW_TITLE')); ?> <?php endif; ?>" required>
                                       </div>
                                       <div class="form-element">
                                          <label><?php echo e((Lang::has(Session::get('lang_file').'.REVIEWS')!= '') ? trans(Session::get('lang_file').'.REVIEWS') : trans($OUR_LANGUAGE.'.REVIEWS')); ?> <em>*</em></label>
                                          <textarea name="comments" placeholder="<?php if(Lang::has(Session::get('lang_file').'.REVIEW_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEW_DESCRIPTION')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEW_DESCRIPTION')); ?> <?php endif; ?>" required></textarea>
                                       </div>
                                       <div class="buttons-set">
                                          <?php if(Session::has('customerid')): ?>  
                                          <button class="button submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;<?php if(Lang::has(Session::get('lang_file').'.SUBMIT_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SUBMIT_REVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SUBMIT_REVIEW')); ?> <?php endif; ?></span></button>
                                          <?php else: ?> 
                                          <a href="" role="button" data-toggle="modal" data-target="#loginpop"><button class="button submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;<?php if(Lang::has(Session::get('lang_file').'.SUBMIT_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SUBMIT_REVIEW')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SUBMIT_REVIEW')); ?> <?php endif; ?></span></button></a>
                                          <?php endif; ?>
                                       </div>
                                    </div>
                                    <?php echo Form::close(); ?>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Main Container End --> 
<!-- Related Product Slider -->
<div class="container pdt-view-page">
   <div class="row">
      <div class="col-xs-12">
         <div class="related-product-area">
            <div class="page-header">
               <h2><?php if(Lang::has(Session::get('lang_file').'.RELATED_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RELATED_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RELATED_PRODUCTS')); ?> <?php endif; ?></h2>
            </div>
            <?php if(count($get_related_product)!=0): ?>  
            <?php  $i=0; ?> 
            <div class="related-products-pro">
               <div class="slider-items-products">
                  <div id="related-product-slider" class="product-flexslider hidden-buttons">
                     <div class="re-pr-slider owl-carousel owl-theme fadeInUp">
                        <?php $__currentLoopData = $get_related_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php  $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                        $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                        $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                        $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
                        $res = base64_encode($product_det->pro_id);
                        $product_image = explode('/**/',$product_det->pro_Img);
                        $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
                        $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2); ?>
                        <?php if((Session::get('lang_code'))=='' || (Session::get('lang_code'))== 'en'): ?>  
                        <?php    $pro_title = 'pro_title'; ?>
                        <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code') ?> <?php endif; ?>
                        <?php  $product_img    = $product_image[0]; ?>
                        <!-- /* Image Path */ -->
                        <?php    $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                        $img_data   = "public/assets/product/".$product_img; ?>
                        <?php if(file_exists($img_data) && $product_img !=''): ?> 
                        <?php   $prod_path = url('').'/public/assets/product/' .$product_img; ?>                  
                        <?php else: ?>  
                        <?php if(isset($DynamicNoImage['productImg'])): ?>
                        <?php    $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['productImg']; ?>
                        <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>
                        <?php    $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>   
                        <!--  /* Image Path ends */   
                           //Alt text -->
                        <?php   $alt_text   = substr($product_det->$pro_title,0,25); ?>
                        <?php if($product_det->pro_no_of_purchase < $product_det->pro_qty): ?> 
                        <?php    $i++; ?>
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="pr-img-area">
                                    <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  
                                    <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>">
                                       <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> </figure>
                                    </a>
                                    <?php endif; ?>
                                    <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>  
                                    <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>">
                                       <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> </figure>
                                    </a>
                                    <?php endif; ?>
                                    <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>  
                                    <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>">
                                       <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> </figure>
                                    </a>
                                    <?php endif; ?>
                                 </div>
                                 
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 
                                       <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)); ?>">
                                       <?php endif; ?> 
                                       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>  
                                       <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res)); ?>">
                                       <?php endif; ?>
                                       <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>  
                                       <a href="<?php echo e(url('productview/'.$mcat.'/'.$smcat.'/'.$res)); ?>">
                                       <?php endif; ?>
                                       <?php echo e(substr ($product_det->$pro_title,0,20)); ?>

                                       </a> 
                                    </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($product_det->pro_disprice); ?></span> </span> </div>
                                       </div>
                                       
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </div>
                  </div>
               </div>
            </div>
            <?php else: ?>
            <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS')); ?> <?php endif; ?>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <!-- Related Product Slider End --> 
   <!-- Upsell Product Slider -->
   
   <!-- Upsell Product Slider End --> 
   <!-- Store section -->
   <?php $store_lat =''; $store_lan =''; ?>
   <?php if($GENERAL_SETTING->gs_store_status == 'Store'): ?>
   <div class="store-details">
      <h5 class="page-header" style="font-size: 18px;font-weight: 600;"><?php if(Lang::has(Session::get('lang_file').'.STORE_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORE_DETAILS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORE_DETAILS')); ?> <?php endif; ?></h5>
      <?php $store_lat =''; $store_lan =''; ?>
      <?php $__currentLoopData = $get_store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $storerow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
      <?php  $stor_name = 'stor_name';
      $stor_address1 = 'stor_address1';
      $stor_address2 = 'stor_address2'; ?>
      <?php else: ?>   
      <?php  $stor_name = 'stor_name_'.Session::get('lang_code'); 
      $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 
      $stor_address2 = 'stor_address2_'.Session::get('lang_code'); ?> 
      <?php endif; ?>
      <?php  $store_name = $storerow->$stor_name;
      $store_address = $storerow->$stor_address1;
      $store_address2 = $storerow->$stor_address2;
      $store_zip = $storerow->stor_zipcode;
      $store_phone = $storerow->stor_phone;
      $store_web = $storerow->stor_website;
      $store_lat = $storerow->stor_latitude;
      $store_lan = $storerow->stor_longitude; 
      $store_image = $storerow->stor_img; ?>
      <?php $prod_path  = url('').'/public/assets/default_image/No_image_store.png';
      $img_data   = "public/assets/storeimage/".$store_image; ?>
      <?php if(file_exists($img_data) && $store_image !=''): ?>  
      <?php
      $prod_path = url('').'/public/assets/storeimage/' .$store_image; ?>                 
      <?php else: ?>  
      <?php if(isset($DynamicNoImage['store'])): ?>
      <?php    $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; ?>
      <?php if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path)): ?>  
      <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?>
      <?php endif; ?>
      <?php endif; ?>
      <?php endif; ?>   
      <?php  $alt_text   = substr($storerow->$stor_name,0,25);
      $alt_text  .= strlen($storerow->$stor_name)>25?'..':''; ?>
      <div class="main container">
         <div class="row">
            <div class="col-xs-12 col-sm-6">
               <h1 style="font-weight: 600;font-size: 22px;"> <span style="color: #e83f33;"><?php echo e($store_name); ?></span></h1>
               <p> <a title="View Store" target="_blank" href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($storerow->stor_id)))); ?>">
                  <img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>" style="width:70px; height:70px;">
                  </a> 
               </p>
               <ul style="color: #999;  font-size: 15px; list-style-type:none;display: block;  margin: 1.2em 0 0;">
                  <li><?php echo e($store_address); ?>,</li>
                  <li><?php echo e($store_address2); ?>,</li>
                  <li><?php echo e($store_zip); ?></li>
                  <li><?php if(Lang::has(Session::get('lang_file').'.MOBILE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MOBILE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MOBILE')); ?> <?php endif; ?> : <?php echo e($store_phone); ?></li>
                  <li><?php if(Lang::has(Session::get('lang_file').'.WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WEBSITE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WEBSITE')); ?> <?php endif; ?> : <?php echo e($store_web); ?></li>
               </ul>
            </div>
            <div class="col-xs-12 col-sm-6">
               <div class="">
                  <div >
                     <div >
                        <div id="us3" style="width: 100% !important; height: 240px;margin-bottom:10px;background-size: cover;  text-align: center;  padding: 80px 0 100px;"> </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   </div>
   <?php else: ?>
   <?php endif; ?>
   <!-- Store section -->
   <!-- service section -->
   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<!-- Footer -->
<a href="" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
<!-- End Footer --> 
<?php 
   if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
   
   $map_lang = 'en';
   
   }else {  
   
   $map_lang = 'fr';
   
   }
   
   ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>&language=<?php echo $map_lang; ?>'></script>
<script src="<?php echo url(''); ?>/public/assets/js/locationpicker.jquery.js"></script>
<script>
   $('#us3').locationpicker({
   
         
   
           location: {latitude: <?php echo $store_lat; ?>, longitude: <?php echo $store_lan; ?>},
   
         
   
           radius: 200,
   
           inputBinding: {
   
               latitudeInput: $('#us3-lat'),
   
               longitudeInput: $('#us3-lon'),
   
               radiusInput: $('#us3-radius'),
   
               locationNameInput: $('#us3-address')
   
           },
   
           enableAutocomplete: true,
   
           onchanged: function (currentLocation, radius, isMarkerDropped) {
   
               // Uncomment line below to show alert on each Location Changed event
   
               //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
   
           }
   
       });
   
</script>
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
   $('#add_to_cart_session').click(function(){
   
     var pro_purchase1 = '<?php echo $pro_details_by_id->pro_no_of_purchase; ?>' ;
   
     var pro_purchase = parseInt($('#addtocart_qty').val()) + parseInt(pro_purchase1);
   
     var pro_qty = '<?php echo $pro_details_by_id->pro_qty; ?>';
   
     if(pro_purchase > parseInt(pro_qty))
   
     {
   
       $('#addtocart_qty').focus();
   
       $('#addtocart_qty').css('border-color', 'red');
   
       $('#addtocart_qty_error').html('<?php if (Lang::has(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE')!= '') { echo  trans(Session::get('lang_file').'.LIMITED_QUANTITY_AVAILABLE');}  else { echo trans($OUR_LANGUAGE.'.LIMITED_QUANTITY_AVAILABLE');} ?>');
   
       return false;
   
     }
   
     else
   
     {
   
       $('#addtocart_qty').css('border-color', '');
   
       $('#addtocart_qty_error').html('');
   
     }
   
     if($('#addtocart_color').val() ==0) 
   
     {
   
       $('#addtocart_color').focus();
   
       $('#addtocart_color').css('border-color', 'red');
   
       $('#size_color_error').html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_COLOR')!= '') { echo  trans(Session::get('lang_file').'.SELECT_COLOR');}  else { echo trans($OUR_LANGUAGE.'.SELECT_COLOR');} ?>');
   
       return false;
   
     }
   
     else
   
     {
   
       $('#addtocart_color').css('border-color', '');
   
       $('#size_color_error').html('');
   
     }
   
     if($('#addtocart_size').val() ==0)
   
     {
   
       $('#addtocart_size').focus();
   
       $('#addtocart_size').css('border-color', 'red');
   
       $('#size_color_error').html('<?php if (Lang::has(Session::get('lang_file').'.SELECT_SIZE')!= '') { echo  trans(Session::get('lang_file').'.SELECT_SIZE');}  else { echo trans($OUR_LANGUAGE.'.SELECT_SIZE');} ?>');
   
       return false;
   
     }
   
     else
   
     {
   
       $('#addtocart_size').css('border-color', '');
   
       $('#size_color_error').html('');
   
     }
   
   });
   
   $("#searchbox").keyup(function(e) 
   
   {
   
   
   
   var searchbox = $(this).val();
   
   var dataString = 'searchword='+ searchbox;
   
   if(searchbox=='')
   
   {
   
   $("#display").html("").hide();  
   
   }
   
   else
   
   {
   
     var passData = 'searchword='+ searchbox;
   
      $.ajax( {
   
               type: 'GET',
   
             data: passData,
   
             url: '<?php echo url('autosearch'); ?>',
   
             success: function(responseText){  
   
             $("#display").html(responseText).show();  
   
   }
   
   });
   
   }
   
   return false;    
   
   
   
   });
   
   });
   
   
   
</script>
<script type="text/javascript">
   $(document).ready(function()
   
   
   
   { 
   
   $('#policyclick').click(function(event)
   
   
   
   {  
   
   $('.dev_cancel').slideToggle("fast");
   
   event.stopPropagation();
   
   // $(".dev_cancel").css({"background":"#ffffff", "position":"absolute"});
   
   });
   
   $('#returnclick').click(function(event)
   
   
   
   {
   
   $('.dev_return').slideToggle("fast");
   
   event.stopPropagation();
   
   // $(".dev_return").css({"background":"#ffffff", "position":"absolute"});
   
   });
   
   $('#replaceclick').click(function(event)
   
   
   
   {
   
   $('.dev_replace').slideToggle("fast");
   
   event.stopPropagation();
   
   });
   
   
   
   
   
   $(".policy-container").on("click", function (event) {
   
   event.stopPropagation();
   
   });
   
   
   
   });
   
   
   
   
   
   $(document).on("click", function () {
   
   $(".policy-container").hide();
   
   });
   
   
   
</script>
<script>
   function add_quantity()
   
   {
   
     //alert();
   
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;*/
   
     var quantity=$("#addtocart_qty").val(); 
   
     var remaining_product=parseInt(<?php echo  $pro_details_by_id->pro_qty-$pro_details_by_id->pro_no_of_purchase;?>);
   
    
   
     if(quantity<remaining_product)
   
     {
   
       var new_quantity=parseInt(quantity)+1;
   
       $("#addtocart_qty").val(new_quantity);
   
     }
   
     //alert();
   
   }
   
   
   
   function remove_quantity()
   
   {
   
     //alert();
   
     /*var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;*/
   
   
   
     var quantity=$("#addtocart_qty").val();
   
     var quantity=parseInt(quantity);
   
     if(quantity>1)
   
     {
   
       var new_quantity=quantity-1;
   
       $("#addtocart_qty").val(new_quantity);
   
     }
   
     //alert();
   
   }
   
</script>
<script type="text/javascript">
   function addtowish(pro_id,cus_id){
   
     //alert();
   
     var wishlisturl = document.getElementById('wishlisturl').value;
   
   
   
     $.ajax({
   
           type: "get",   
   
           url:"<?php echo url('addtowish'); ?>",
   
           data:{'pro_id':pro_id,'cus_id':cus_id},
   
             success:function(response){
   
             //alert(response); return false;
   
             if(response==0){
   
             <?php /*  alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ADDED_TO_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ADDED_TO_WISHLIST');} ?>');*/?>
   
                         $(".add-to-wishlist").fadeIn('slow').delay(5000).fadeOut('slow');
   
               //window.location=wishlisturl;
   
                             window.location.reload();
   
                             
   
             }else{
   
               alert('<?php if (Lang::has(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST')!= '') { echo  trans(Session::get('lang_file').'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');}  else { echo trans($OUR_LANGUAGE.'.PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST');} ?>');
   
               //window.location=wishlisturl;
   
             }
   
             
   
             
   
           }
   
         });
   
   }
   
</script>
<script src="<?php echo e(url('')); ?>/themes/js/handleCounter.js"></script>
<script>
   $(function ($) {
   
       var options = {
   
           minimum: 1,
   
           maximize: 10,
   
           onChange: valChanged,
   
           onMinimum: function(e) {
   
               console.log('reached minimum: '+e)
   
           },
   
           onMaximize: function(e) {
   
               console.log('reached maximize'+e)
   
           }
   
       }
   
       $('#handleCounter').handleCounter(options)
   
       $('#handleCounter2').handleCounter(options)
   
   $('#handleCounter3').handleCounter({maximize: 100})
   
   })
   
   function valChanged(d) {
   
   //            console.log(d)
   
   }
   
</script>
<!-- For Responsive menu-->
<script type="text/javascript">
   $(document).ready(function() {
   
       $(document).on("click", ".customCategories .topfirst b", function() {
   
           $(this).next("ul").css("position", "relative");
   
   
   
           $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");
   
           $(this).next("ul").toggle();
   
       });
   
   
   
       $(document).on("click", ".morePage", function() {
   
           $(".nextPage").slideToggle(200);
   
       });
   
   
   
       $(document).on("click", "#smallScreen", function() {
   
           $(this).toggleClass("customMenu");
   
       });
   
   
   
       $(window).scroll(function() {
   
           if ($(this).scrollTop() > 250) {
   
               $('#comp_myprod').show();
   
           } else {
   
               $('#comp_myprod').hide();
   
           }
   
       });
   
   
   
   });
   
</script>



<?php echo $footer; ?>



<script>
  $('.re-pr-slider').owlCarousel({                
                margin: 10,
                nav: true,
                loop: false,
                dots: false,
                <?php if(Session::get('lang_code')=='' || Session::get('lang_code') == 'ar'){ ?>
                  rtl : true,
               <?php }else{ ?>
                   rtl:false,   
              <?php } ?>                  
                autoplay:false,           
                responsive: {
                  0: {
                    items: 1
                  },
                  480: {
                    items: 2
                  },
                  768: {
                    items: 3
                  },
                  992: {
                    items: 4
                  },
                  1200: {
                    items: 4
                  }
                }
     })
</script>

</body>
</html>