﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADD_BLOG')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADD_BLOG')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_BLOG')); ?> <?php endif; ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php endif; ?>	
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-wysihtml5-hack.css" />
     <style>
                        ul.wysihtml5-toolbar > li {
                            position: relative;
                        }
                    </style>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

<!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                                <li class="active"><a><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADD_BLOG')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADD_BLOG')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_BLOG')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADD_BLOG')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADD_BLOG')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_BLOG')); ?> <?php endif; ?></h5>
            
        </header>
         <?php if(Session::has('message')): ?>
		<div class="alert alert-danger alert-dismissable"><?php echo Session::get('message'); ?>

      <?php echo e(Form::button('x',['class' => 'close' , 'data-dismiss' => 'alert' , 'aria-hidden' => 'true'])); ?>

        </div>
		<?php endif; ?>
	  <?php if($errors->any()): ?>
         <div class="alert alert-warning alert-dismissable"><?php echo implode('', $errors->all('<li>:message</li>')); ?>

        <?php echo e(Form::button('x',['class' => 'close' , 'data-dismiss' => 'alert' , 'aria-hidden' => 'true'])); ?>

        </div>
		<?php endif; ?>
        
        
        <div id="div-1" class="accordion-body collapse in body">
             <?php echo Form::open(array('url'=>'add_blog_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_BLOG_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_BLOG_TITLE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOG_TITLE')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="text1" maxlength="200" placeholder="Enter Blog Title <?php echo e($default_lang); ?>" class="form-control" type="text" name="blog_title" value="<?php echo Input::old('blog_title'); ?>" >
                    </div>
                </div>
				
				<?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
        $get_lang_name = $get_lang->lang_name;
				?>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Blog Title(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="text1" maxlength="200" placeholder="Enter Blog Title in <?php echo e($get_lang_name); ?>" class="form-control" type="text" name="blog_title_<?php echo $get_lang_name; ?>" value="<?php echo Input::old('blog_title_'.$get_lang_name); ?>" >
                    </div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
				
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DESCRIPTION')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <textarea id="wysihtml5" class="wysihtml5 form-control" placeholder="Enter Blog Description <?php echo e($default_lang); ?>" rows="10" name="blog_description" ><?php echo Input::old('blog_description'); ?></textarea>
                    </div>
                </div>
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
        $get_lang_name = $get_lang->lang_name;
				?>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Description(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <textarea id="wysihtml5" class="wysihtml5 form-control" placeholder="Enter Blog Description in <?php echo e($get_lang_name); ?>" rows="10" name="blog_description_<?php echo $get_lang_name; ?>" ><?php echo Input::old('blog_description_'.$get_lang_name); ?></textarea>
                    </div>
                </div>
				 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         <?php endif; ?>
				  <div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CATEGORY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORY')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <select class="form-control" name="blog_category" value="<?php echo Input::old('blog_category'); ?>" >
							<option value="0">-- <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT')); ?> <?php endif; ?> --</option>						
                          <?php $__currentLoopData = $categoryresult; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categorydetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          				   <option value="<?php echo e($categorydetails->mc_id); ?>"><?php echo e($categorydetails->mc_name); ?> </option>
           				   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE')); ?> <?php endif; ?> <span class="text-sub">*</span></label>
					<span class="errortext red logo-size" style="color:red"><em><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_IMAGE')); ?> <?php endif; ?></em></span>
                   <div class="col-lg-8">

				   <?php echo e(Form::file('file', array('id' => 'blog_img'))); ?>

		   
		 
							
                    </div>
                </div>
				  <div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                    <!-- Select a snapshot (png,jpg,jpeg less than 1M).-->
					
                    </div>
                </div>
				<div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_META_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_META_TITLE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_META_TITLE')); ?> <?php endif; ?></label>

                    <div class="col-lg-8">
                       <input type="text" class="form-control" placeholder="Enter Meta Title <?php echo e($default_lang); ?>" id="text1" name="meta_title" value="<?php echo Input::old('meta_title'); ?>" >
                    </div>
              </div>
				
				 <?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Meta Title(<?php echo e($get_lang_name); ?>)</label>

                    <div class="col-lg-8">
                       <input type="text" class="form-control" placeholder="Enter Meta Title In <?php echo e($get_lang_name); ?>" id="text1" name="meta_title_<?php echo e($get_lang_name); ?>" value="<?php echo Input::old('meta_title_'.$get_lang_name); ?>" >
                    </div>
                </div>
				
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
				
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_META_DESCRIPTION')); ?> <?php endif; ?> </label>

                    <div class="col-lg-8">
                       <textarea id="text4" class="form-control" placeholder="Enter Meta Description <?php echo e($default_lang); ?>" name="meta_description"   ><?php echo Input::old('meta_description'); ?></textarea>
                    </div>
                </div>
				 <?php if(!empty($get_active_lang)): ?>  
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Meta Description(<?php echo e($get_lang_name); ?>)</label>

                    <div class="col-lg-8">
                       <textarea id="text4" class="form-control" placeholder="Enter Meta Description in <?php echo e($get_lang_name); ?>" name="meta_description_<?php echo e($get_lang_name); ?>"   ><?php echo Input::old('meta_description_'.$get_lang_name); ?></textarea>
                    </div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_META_KEYWORDS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_META_KEYWORDS')); ?> <?php endif; ?></label>

                    <div class="col-lg-8">
                       <input type="text" id="text1" placeholder="Enter Meta Keywords <?php echo e($default_lang); ?>" class="form-control" name="meta_keywords" value="<?php echo Input::old('meta_keywords'); ?>" >
                    </div>
                </div>
				
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php $get_lang_name = $get_lang->lang_name;
				?>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Meta Keyword(<?php echo e($get_lang_name); ?>)</label>

                    <div class="col-lg-8">
                       <input type="text" id="text1" placeholder="Enter Meta Keywords In <?php echo e($get_lang_name); ?>" class="form-control" name="meta_keywords_<?php echo e($get_lang_name); ?>" value="<?php echo Input::old('meta_keywords_'.$get_lang_name); ?>" >
                    </div>
                </div>
				
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
				
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAGS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAGS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAGS')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
					<?php echo e(Form::text('tags', Input::old('tags') , array('id' => 'text1',  'placeholder' =>'Enter Tags', 'class' => 'form-control'))); ?>

                       
                    </div>
                </div>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ALLOW_COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ALLOW_COMMENTS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ALLOW_COMMENTS')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <input type="checkbox" checked="" value="1" name="allow_comments" value="<?php echo Input::old('allow_comments'); ?>" >
                    </div>
                </div>
				<div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STATUS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?> <?php endif; ?><span class="text-sub">*</span></label>

                   <div class="col-lg-8">
					           <input type="radio" name="blogstatus" checked="checked" title="Active" value="1"> <label class="sample"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PUBLISH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PUBLISH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PUBLISH')); ?> <?php endif; ?>                                                      </label>
					<input type="radio" name="blogstatus" checked="checked" title="Active" value="2"> <label class="sample"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DRAFT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DRAFT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DRAFT')); ?> <?php endif; ?>	                                                                                   </label>
						<label class="sample"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-2"><span  class="text-sub"></span></label>

                    <div class="col-lg-8">
                     <button type="submit" class="btn btn-warning btn-sm btn-grad" style="color:#fff"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SUBMIT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?> <?php endif; ?></button>
                     <button type="reset" class="btn btn-danger btn-sm btn-grad" style="color:#ffffff"> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RESET')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?> <?php endif; ?></button>
                   
                    </div>
					  
                </div>

                
				<?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   <?php echo $adminfooter; ?>

    <!--END FOOTER -->

 
       <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

         <!-- PAGE LEVEL SCRIPTS -->
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo e(url('')); ?>public/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/editorInit.js"></script>
    <script>
       // $(function () { formWysiwyg(); });
	   $(document).ready(function () {
	$('.wysihtml5').wysihtml5();
});
        </script>
<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>
<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
</body>
     <!-- END BODY -->
</html>
