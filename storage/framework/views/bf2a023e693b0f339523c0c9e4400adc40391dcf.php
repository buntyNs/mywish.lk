<!DOCTYPE html>

<html lang="en">

<?php echo $navbar; ?>


<?php echo $header; ?>




<body class="compare_page">

<!--[if lt IE 8]>

      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

  <![endif]--> 



<!-- mobile menu -->



<!-- end mobile menu -->

<div id="page"> 

  <!-- Header -->

  

  <!-- end header -->

  

  

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="index-2.html"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li>

            <li><strong><?php echo e((Lang::has(Session::get('lang_file').'.COMPARE')!= '') ?  trans(Session::get('lang_file').'.COMPARE'): trans($OUR_LANGUAGE.'.COMPARE')); ?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="col-main">

        <div class="compare-list">

          <div class="page-title">

            <h2><?php if(Lang::has(Session::get('lang_file').'.COMPARE_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COMPARE_PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COMPARE_PRODUCTS')); ?> <?php endif; ?></h2>

          </div>

          <div class="table-responsive">

            

              <table class="table table-bordered table-compare"> 

             

             

             <?php if(count($product_details)!=0): ?>

              

              <tr>

                <td class="compare-label"><?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT_IMAGE')!= '') ? trans(Session::get('lang_file').'.PRODUCT_IMAGE') : trans($OUR_LANGUAGE.'.PRODUCT_IMAGE')); ?></td> 

                

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                <td class="compare-pro">

                  

                  <?php      $product_image = explode('/**/',$pro_det->pro_Img); ?>

                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                     <?php $pro_title = 'pro_title'; ?>

                     <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

                    

                  </div>

                  <?php   $product_image   = $product_image[0];

                  $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                  $img_data   = "public/assets/product/".$product_image; ?>

                  <?php if(file_exists($img_data) && $product_image!='' ): ?>  

                  <?php  $prod_path = url('public/assets/product/').'/'.$product_image;  ?>       

                  <?php else: ?> 

                  <?php if(isset($DynamicNoImage['productImg'])): ?> 

                  <?php  $dyanamicNoImg_path = "public/assets/noimage/" .$DynamicNoImage['productImg']; ?>

                  <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?> 

                  <?php  $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

                  <?php endif; ?>

                  <?php endif; ?>

                 

                  <a href="#"><img src="<?php echo e($prod_path); ?>" alt="" width="260"></a></td>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                

              </tr>  

              <tr>

                <td class="compare-label" ><?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT_NAMES')!= '') ? trans(Session::get('lang_file').'.PRODUCT_NAMES') : trans($OUR_LANGUAGE.'.PRODUCT_NAMES')); ?></td>

                

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <td>

                  

                  <?php      $product_image = explode('/**/',$pro_det->pro_Img); ?>

                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                     <?php $pro_title = 'pro_title'; ?>

                     <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

                  <a href="#"><?php echo e($pro_det->$pro_title); ?> </a></td>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  

               

                

              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.RATING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATING')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATING')); ?> <?php endif; ?></td>

                

               

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php    $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

                     $multiple_countone = $one_count *1;

                     $multiple_counttwo = $two_count *2;

                     $multiple_countthree = $three_count *3;

                     $multiple_countfour = $four_count *4;

                     $multiple_countfive = $five_count *5;

                     $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

                     <?php if($product_count): ?>

                     <?php $product_divide_count = $product_total_count / $product_count; ?>

                      <?php if($product_divide_count <= '1'): ?>

                <td><div class="rating"> <img src="<?php echo e(url('images/stars-1.png')); ?>" alt="" width=""></div></td>

               

                 <?php elseif($product_divide_count >= '1'): ?>

                 <td><div class="rating"> <img src="<?php echo e(url('images/stars-1.png')); ?>" alt="" width=""></div></td>

                

                <?php elseif($product_divide_count >= '2'): ?>

                 <td><div class="rating"> <img src="<?php echo e(url('images/stars-2.png')); ?>" alt="" width=""></div></td>

                

                 <?php elseif($product_divide_count >= '3'): ?>

                 <td><div class="rating"> <img src="<?php echo e(url('images/stars-3.png')); ?>" alt="" width=""></div></td>

               

                 <?php elseif($product_divide_count >= '4'): ?>

                 <td><div class="rating"> <img src="<?php echo e(url('images/stars-4.png')); ?>" alt="" width=""></div></td>

                

                <?php elseif($product_divide_count >= '5'): ?>

                <td><div class="rating"> <img src="<?php echo e(url('images/stars-5.png')); ?>" alt="" width=""></div></td>

               

                 <?php else: ?>

                 <td><div class="rating"> <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php endif; ?></div></td>

                <?php endif; ?>

                <?php elseif($product_count): ?>

                     <?php $product_divide_count = $product_total_count / $product_count; ?>

                     <?php else: ?>  

                     <td> <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_PRODUCT')); ?> <?php endif; ?>

                     <?php endif; ?> </td>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

               

               

              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.DISCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DISCOUNT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DISCOUNT')); ?> <?php endif; ?></td>

               

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                <td class="price"><?php echo e($pro_det->pro_disprice); ?></td>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.DESC')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DESC')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DESC')); ?> <?php endif; ?></td>              

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <td><?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                           <?php $pro_desc = 'pro_desc'; ?>

                           <?php else: ?> <?php  $pro_desc = 'pro_desc_'.Session::get('lang_code'); ?> <?php endif; ?>

                           <?php echo html_entity_decode($pro_det->$pro_desc); ?></td>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>            

              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORE_NAME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORE_NAME')); ?> <?php endif; ?></td>
               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <td><?php $__currentLoopData = $store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                     <?php $stor_name = 'stor_name'; ?>

                     <?php else: ?> <?php  $stor_name = 'stor_name_'.Session::get('lang_code'); ?> <?php endif; ?>

                     <?php echo e($store_det->$stor_name); ?> 

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.SIZES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SIZES')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SIZES')); ?> <?php endif; ?>  </td>

              

               

                 <?php if(count($product_color_details) > 0): ?> 

                 <?php $__currentLoopData = $product_size_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                  <td>  

                     <?php $__currentLoopData = $product_size_det; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                   <?php echo e($size_det->si_name.','); ?>


                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </td>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                     <?php endif; ?> 

              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.COLOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COLOR')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COLOR')); ?> <?php endif; ?></td>

                <?php if(count($product_color_details) > 0): ?> 

                     <?php $__currentLoopData = $product_color_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                     <td>

                       <?php $__currentLoopData = $product_color_det; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                      <?php echo e($color_det->co_name.','); ?>


                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                       </td>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                      <?php endif; ?>


              </tr>

              <tr>

                <td class="compare-label" > <?php if(Lang::has(Session::get('lang_file').'.SPEC')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SPEC')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SPEC')); ?> <?php endif; ?></td>


               <?php $__currentLoopData = $product_spec_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                     <?php $sp_name = 'sp_name';

                     $spc_value = 'spc_value'; ?>

                     <?php else: ?>   

                     <?php $sp_name = 'sp_name_'.Session::get('lang_code'); 

                     $spc_value = 'spc_value_'.Session::get('lang_code');  ?>

                     <?php endif; ?> 

                     <td><ul>

                       <?php $__currentLoopData = $product_det; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                       <li>

                      <?php echo e($product->$sp_name.' : '.$product->$spc_value); ?> </li> 

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      </ul>

                       </td>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </tr>

              <tr>

                <td class="compare-label" ><?php if(Lang::has(Session::get('lang_file').'.DELIVERY_WITH_IN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DELIVERY_WITH_IN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DELIVERY_WITH_IN')); ?> <?php endif; ?></td>

               

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                <td><?php echo e($pro_det->pro_delivery.'days'); ?></td>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </tr>

              <tr>

                <td class="compare-label" ><?php echo e((Lang::has(Session::get('lang_file').'.ACTION')!= '') ? trans(Session::get('lang_file').'.ACTION') : trans($OUR_LANGUAGE.'.ACTION')); ?></td>

                

               <?php $__currentLoopData = $product_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



               <td class="action">

                 

                  <?php if(in_array($pro_det->pro_id, $_SESSION['compare_product'])): ?> 

                  <button class="button button-sm close_compare" onclick="comparefunc(<?php echo $pro_det->pro_id.','.'0'; ?>);" value="0" name="compare" id="compare"><i class="fa fa-close"></i></button></td>

                   <?php endif; ?>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </tr>

              <?php else: ?>

               <?php if(Lang::has(Session::get('lang_file').'.NO_PRODUCTS_INCOMPARE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_PRODUCTS_INCOMPARE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_PRODUCTS_INCOMPARE')); ?>



                <?php endif; ?> 

               <?php endif; ?>

             </table>

          </div>

        </div>

      </div>

    </div>

  </section>

  

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <!-- Footer -->

 

  <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a> </div>



<!-- End Footer --> 

<?php echo $footer; ?>


<!-- JS --> 

<script>

   function comparefunc(pid,value){

    //var value = ('#compare').val();

    //alert(value);

    

    var pid = pid;

         $.ajax( {

              type: 'get',

            data: 'pid='+pid + '&value=' +value,

            url: 'remove_compare_product',

            success: function(responseText){  

             if(responseText)

             {  

              alert(responseText);

              location.reload();

            //$('#compare').html(responseText);            

             }

          }   

        }); 

    

   }

</script>


</html>