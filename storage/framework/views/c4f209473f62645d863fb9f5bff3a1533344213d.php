<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo $SITENAME; ?> | <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_DASHBOARD')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_DASHBOARD');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_DASHBOARD');} ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
     <?php if(count($favi)>0): ?>  
        <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
 	<?php endif; ?>
    <link href="<?php echo e(url('')); ?>/public/assets/css/datepicker.css" rel="stylesheet">	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />

 <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->


		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>
                                <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMERS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMERS')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-dashboard"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_DASHBOARD')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_DASHBOARD')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_DASHBOARD')); ?> <?php endif; ?></h5>
            
        </header>
        <br>
	<div class="">
                    <div class="col-lg-12">
                 <a style="color:#fff" href="<?php echo e(url('')); ?>" class="btn btn-success btn-sm btn-grad" target="_blank" ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_GO_TO_LIVE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_GO_TO_LIVE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_GO_TO_LIVE')); ?> <?php endif; ?></a>
                
                </div>
                </div>
                
            
        
        
      			  <div class="row">
                <div class="col-lg-6" style="padding:30px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMERS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_CUSTOMERS')); ?> <?php endif; ?></strong> 
                            <a href="#"><i class="icon icon-align-justify pull-right"></i></a>
                        </div>
                        <div class="panel-body">
                            <?php if(($website_users+$fb_users+$admin_user)>0): ?>
                           <div id="chart6" style="margin-top:20px; margin-left:20px; width:450px; height:450px;"></div>
                           <div class="table-responsive">
                            <table width="100%" border="0">
                              <tr>
                                
                                <td style="background:#eaa228">
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_USER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_WEBSITE_USER')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_USER')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($website_users); ?></span>
                                </td>
                                <td style="background:#4bb2c5"> 
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FACEBOOK_USER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FACEBOOK_USER')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FACEBOOK_USER')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($fb_users); ?></span>
                                </td>
                                
                                <td style="background:#C5B47F">
                                    <label class="label label-active"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_USER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADMIN_USER')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_USER')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($admin_user); ?></span>
                                </td>  
                              </tr>
                            </table></div>
                            <?php else: ?>
                                No Details Found.
                            <?php endif; ?>    
                        </div>
                       
                        	

                        
                    </div>
                </div>
                <div class="col-lg-5 " style="padding-top:30px"> 
                    <div class="panel panel-default">
                        <div class="panel-heading">	
                            <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_DETAILS')); ?> <?php endif; ?></strong>
                            
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="" width="100%">
                                    
                                    <tbody>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_CUSTOMER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_WEBSITE_CUSTOMER')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_CUSTOMER')); ?> <?php endif; ?> </td>
                                            <td><?php echo e($website_users); ?></td>
                                            
                                        </tr>
                                        <tr>
                                            <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$website_users.'%"'; ?>> 
		  <span class="sr-only">60% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?> <?php endif; ?></span>
		</div>
	      </div></td>
                                            
                                            
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADMIN_CUSTOMERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_CUSTOMERS')); ?> <?php endif; ?></td>
                                            <td><?php echo $admin_user; ?></td>
                                            
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$admin_user.'%"'; ?>;> 
		  <span class="sr-only">60% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?> <?php endif; ?></span>
		</div>
	      </div></td>
                                                         
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FACEBOOK_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FACEBOOK_CUSTOMERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FACEBOOK_CUSTOMERS')); ?> <?php endif; ?></td>
                                            <td><?php echo e($fb_users); ?></td>
                                            
                                        </tr>
                                         <tr>
                                             <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"  <?php echo 'style="width:'.$fb_users.'%"'; ?>;>
		  <span class="sr-only">40% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?> <?php endif; ?></span>
		</div>
	      </div></td>  
 <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_CUSTOMERS')); ?> <?php endif; ?></td>
                                            <td><?php echo e($totalcus=$website_users+$admin_user+$fb_users); ?></td>
                                         
                                        </tr>
                                         <tr>
                                           <td colspan="2"><div class="progress progress-striped active">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$totalcus.'%"'; ?>;> 
		  <span class="sr-only">60% <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COMPLETE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COMPLETE')); ?> <?php endif; ?></span>
		</div>
	      </div></td>
                                            
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-5"  style="padding-left:12px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_LOGIN_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_LOGIN_DETAILS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_LOGIN_DETAILS')); ?> <?php endif; ?></strong>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_WEBSITE_CUSTOMERS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_CUSTOMERS')); ?> <?php endif; ?></th>
                                            <th><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COUNT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNT')); ?> <?php endif; ?></th>
					</tr>
                                    </thead>
                                    <tbody>
					 <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TODAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TODAY')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TODAY')); ?> <?php endif; ?></td>
                                            <td><?php echo e($logintoday[0]->count); ?></td>
                                           
                                            
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST')); ?> <?php endif; ?> 7 <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS')); ?> <?php endif; ?></td>
                                            <td><?php echo e($login7days[0]->count); ?></td>
                                           
                                            
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST')); ?> <?php endif; ?> 30 <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DAYS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DAYS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DAYS')); ?> <?php endif; ?></td>
                                            <td><?php echo e($login30days[0]->count); ?></td>
                                           
                                            
                                        </tr>
                                        <tr>
                                            <td><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST')); ?> <?php endif; ?> 12 <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MONTHS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MONTHS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MONTHS')); ?> <?php endif; ?> </td>
                                            <td><?php echo e($login12mnth[0]->count); ?></td>
                                           
                                            
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    </div>
</div>
   
    </div>
                    
                    
                    <div class="row">
                    	<div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STATISTICS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STATISTICS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STATISTICS')); ?> <?php endif; ?></strong> 
                            </div>
                             
                            <div class="panel-body">
                              <ul class="nav nav-pills">
                                <li class="active"><a href="#home-pills" data-toggle="tab"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_WEB_CUST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_WEB_CUST')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_ONE_YEAR_WEB_CUST')); ?> <?php endif; ?></a>
                                </li>
                              </ul>
                            <br>
                            <div class="tab-content">
                                <?php if($ordermnth_count!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
                              <div id="chart1" style="margin-top:20px; margin-left:20px; width:950px; height:470px;"></div>
                              <?php else: ?>
                                No Transaction Found.
                              <?php endif; ?>
                                                               
                            </div>
                            
                            
			
			
		</div>
		</div>
                             
		
                    </div>
                    </div>
                    </div>
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
   
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   <?php echo $adminfooter; ?>

    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
  
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
    <?php if(($fb_users+$website_users+$admin_user)): ?>
    <script>
	$(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[<?php echo e($fb_users); ?>,<?php echo e($website_users); ?>,<?php echo e($admin_user); ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } });
		});
	</script>
    <?php endif; ?>
    <?php if($ordermnth_count!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$ordermnth_count. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_JANUARY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_FEBRUARY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_MARCH');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_APRIL');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_MAY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY');} ?>','<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_JUNE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_JULY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_AUGUST');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_OCTOBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_NOVEMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER');} ?>', '<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_DECEMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER');} ?>'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    
	<?php endif; ?>
	<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
	</script>

    <!--- Chart Plugins -->
      <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    
    <!-- -->
    


<script src="<?php echo e(url('')); ?>/public/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="<?php echo e(url('')); ?>/public/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="<?php echo e(url('')); ?>/public/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>

        
    
	<script src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="<?php echo e(url('')); ?>/public/assets/js/bar_chart.js"></script>
        
     
</body>
     <!-- END BODY -->
</html>
