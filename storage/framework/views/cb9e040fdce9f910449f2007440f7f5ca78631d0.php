﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_ADD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_ADD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_ADD_PRODUCTS')); ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/new_css.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
	<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/multi-select.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-wysihtml5-hack.css" />
     <style>
                        ul.wysihtml5-toolbar > li {
                            position: relative;
                        }
                    </style>
     <style type="text/css">
    .multiselect-container {
      height: 200px !important;
      overflow-y: scroll !important;
    }</style>                
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

       
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                                <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCTS')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCTS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCTS') : trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCTS')); ?></h5>
            
        </header>
		<?php if($errors->any()): ?>
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

            <?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

         
        </div>
		</ul>	
		<?php endif; ?>
        <?php if(Session::has('message')): ?>
		<p style="background-color:green;color:#fff;"><?php echo Session::get('message'); ?></p>
		<?php endif; ?>
        <div id="div-1" class="accordion-body collapse in body">
               <?php echo Form::open(array('url'=>'mer_add_product_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

		<div id="error_msg"></div>
                 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_TITLE')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Title" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ENTER_YOUR_PRODUCT_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_YOUR_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_YOUR_PRODUCT_TITLE')); ?> <?php echo e($default_lang); ?>" name="Product_Title" class="form-control" type="text" onChange="check();">
						<div id="title_error_msg"  style="color:#F00;font-weight:800"  > </div>
                    </div>
                </div>
				
				
				<?php if(!empty($get_active_lang)): ?>
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
                $get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">Product Title (<?php echo e($get_lang_name); ?>) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Title_<?php echo e($get_lang_name); ?>"  name="Product_Title_<?php echo e($get_lang_name); ?>" placeholder="Enter Product Title In  <?php echo e($get_lang_name); ?>" class="form-control" type="text" onChange="check();">
                        <div id="title_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
                        
					</div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
				<!--
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Title(<?php echo e(Helper::lang_name()); ?>)<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="text" name="Product_Title_fr" id="Product_Title_fr" placeholder="Enter Product Name in <?php echo e(Helper::lang_name()); ?>"  class="form-control" onChange="check();" >
						<div id="title_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
					</div>
                </div>-->
				
                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TOP_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TOP_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_TOP_CATEGORY')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
		 		<select class="form-control" id="Product_Category" name="Product_Category" onChange="get_maincategory(this.value);get_specification_details();">
             	<option value="0">--- <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> ---</option>
           <?php $__currentLoopData = $productcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_mc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
              			<option value="<?php echo e($product_mc->mc_id); ?>"> <?php echo e($product_mc->mc_name); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        		</select>
				<div id="category_error_msg"  style="color:#F00;font-weight:800"  > </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAIN_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_MAIN_CATEGORY')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <select class="form-control" id="Product_MainCategory" name="Product_MainCategory" onChange="get_subcategory(this.value);get_specification_details();">
						<option value="0">-<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_MAIN_CATEGORY')); ?>-</option>    
        			   </select>
					   <div id="main_cat_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SUB_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SUB_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SUB_CATEGORY')); ?><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                        <select class="form-control" id="Product_SubCategory"name="Product_SubCategory" onChange="get_second_subcategory(this.value)">
           					<option value="0">-<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_SUB_CATEGORY')); ?>-</option>   
        				</select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SECOND_SUB_CATEGORY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SECOND_SUB_CATEGORY') :  trans($MER_OUR_LANGUAGE.'.MER_SECOND_SUB_CATEGORY')); ?><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <select class="form-control" id="Product_SecondSubCategory" name="Product_SecondSubCategory">
                <option value="0">-<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_SELECT_SECOND_SUB_CATEGORY')); ?>-</option>   
        </select>
                    </div>
                </div>

				<?php /*brand
				<div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_BRAND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_BRAND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_BRAND');} ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <select class="form-control" id="product_brand" name="product_brand">
                			<option value="0">--<?php if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_BRAND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SELECT_BRAND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SELECT_BRAND');} ?>--</option>   
							<?php foreach($brand_details as $brand_data){?>
							<option value="<?php echo $brand_data->brand_id;?>"><?php echo $brand_data->brand_name; ?></option>
							<?php } ?>
        				</select>
						<div id="brand_error_msg" style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				brand*/ ?>
                <!--  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SKU_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SKU_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SKU_NUMBER')); ?> <?php endif; ?>
                        <span class="text-sub">*</span>
                    </label>

                    <div class="col-lg-8">
                        <input   placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ENTER_SKU_NUMBER')); ?> <?php endif; ?>" class="form-control" type="text" value="<?php echo e(old('sku_no')); ?>" id="sku_no" name="sku_no" maxlength="20" onkeyup="check_sku_no()">
                    <div id="skuno_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div> -->

 		 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_QUANTITY') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_QUANTITY')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input   placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ENTER_QUANTITY_OF_PRODUCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_QUANTITY_OF_PRODUCT') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_QUANTITY_OF_PRODUCT')); ?>" class="form-control" type="text" maxlength="5" id="Quantity_Product" name="Quantity_Product">
						<div id="qty_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ? trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') :  trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE')); ?> (<?php echo e(Helper::cur_sym()); ?>)<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input   placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" class="form-control" type="text"  maxlength="10" id="Original_Price" name="Original_Price">
						<div id="org_price_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_DISCOUNTED_PRICE')); ?> (<?php echo e(Helper::cur_sym()); ?>)<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" class="form-control" type="text"  maxlength="10" id="Discounted_Price" name="Discounted_Price">
						<div id="dis_price_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				 <div class="form-group">
                    <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>


                    <div class="col-lg-8">
                             <input type="checkbox" onclick="if(this.checked){$('#inctax').show();}else{$('#inctax').hide();$('#inctax').val(0)}"> ( <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_INCLUDING_TAX_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INCLUDING_TAX_AMOUNT') :  trans($MER_OUR_LANGUAGE.'.MER_INCLUDING_TAX_AMOUNT')); ?> ) 
                             <input placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY')); ?>" style="display:none;" class="form-control" min="1" max="99" type="number" id="inctax" name="inctax"> %
							 <div id="tax_error_msg"  style="color:#F00;font-weight:800"> </div>			
                    </div>
                </div>
		<div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ? trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT')); ?><span class="text-sub">*</span></label>

                   <div class="col-lg-8">
<input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'none');" value="1" checked > <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FREE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FREE') : trans($MER_OUR_LANGUAGE.'.MER_FREE')); ?></label>
<input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'block');" value="2"  ><label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT') :  trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')); ?></label>
					
					
						<?php echo e(Form::label('','',['class'=>'sample'])); ?>

                    </div>
                </div>
		
				  <div class="form-group" id="showship" style="display:none;">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT')); ?> (<?php echo e(Helper::cur_sym()); ?>)<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input  placeholder="" class="form-control" type="text"  maxlength="10" id="Shipping_Amount" name="Shipping_Amount">
						<div id="ship_amt_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				  
				 
				 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2" id="desc"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION') :  trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8" >
                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10" placeholder="Enter Your Description <?php echo e($default_lang); ?>" name="Description"></textarea>
					   <div id="desc_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php 
                $get_lang_name = $get_lang->lang_name;
				$get_lang_code = $get_lang->lang_code;
				?>
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION') :  trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
						<textarea id="wysihtml5" placeholder="Enter Description in <?php echo e($get_lang_name); ?>" name="Description_<?php echo e($get_lang_name); ?>" class="wysihtml5 form-control" rows="10" ></textarea><div id="Description_<?php echo e($get_lang_name); ?>_error_msg"  style="color:#F00;font-weight:800"  >
						<?php if($errors->has('Description_'.$get_lang_code.'')): ?> <?php echo e($errors->first('Description_'.$get_lang_code.'')); ?><?php endif; ?></div>
					</div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
				<?php /*<!--
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION');} ?>({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

                    <div class="col-lg-8" id="description_fr">
                       <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="Description_fr" placeholder="Enter Your Description in French"></textarea>
					   <div id="desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div> 
                </div>-->   */ ?>
				
		 <div class="form-group">
                    
			<label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_WANT_TO_ADD_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_WANT_TO_ADD_SPECIFICATION'): trans($MER_OUR_LANGUAGE.'.MER_WANT_TO_ADD_SPECIFICATION')); ?><span class="text-sub">*</span></label>

                   	<div class="col-lg-8">
					<input type="radio"  id="specification" name="specification"  onClick="setVisibility('sub4', 'block');get_specification_details();" value="1"> 
					<label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES')); ?></label>
					<input type="radio" name="specification"  id="specification" onClick="setVisibility('sub4', 'none');" value="2" checked> 
					<label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> </label>
					
                   	 </div>
                </div>
                
                
                <div class="form-group spcfctn-more" id="sub4" style="display:none">
                    <label for="text2" class="control-label col-lg-2" style=""><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SPECIFICATION') : trans($MER_OUR_LANGUAGE.'.MER_SPECIFICATION')); ?></label>

                    <!--<div class="col-lg-12">
                   <?php /** manage_specification
                   <label>
                    <?php if (Lang::has(Session::get('mer_lang_file').'.MER_TEXT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TEXT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TEXT');} ?> ( <?php if (Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SPECIFICATION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SPECIFICATION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_SPECIFICATION');} ?> - <a href="<?php echo url(); ?>/manage_specification"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_ADD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_ADD');} ?></a> )</label> **/ 
                    ?>
</div>-->


					<div class="col-lg-2" style="">
                       <select class="form-control" name="spec_grp0" id="spec_grp0" onChange='spcfunction(0,this.value);'>
                        <option  value="0">--<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SPECIFICATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_SPECIFICATION') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_SPECIFICATION')); ?>--</option>
            			<?php $__currentLoopData = $spec_group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro_spec_group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
           		 		<option  value="<?php echo e($pro_spec_group->spg_id); ?>"><?php echo $pro_spec_group->spg_name; ?></option>
          		 		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        				</select>
						
                    </div>
					<div class="col-lg-2">
                       <select class="form-control" name="spec0" id="pro_spec0">
                        <option  value="0">--<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> --</option>
            			<?php $__currentLoopData = $productspecification; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $specification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
           		 		<option value="<?php echo e($specification->sp_id); ?>"><?php echo $specification->sp_name; ?></option>
          				 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        				</select>
						
                    </div>
                    
                    <div class="col-lg-2">
                       <input type="text" class="form-control" id="spectext0" name="spectext0" placeholder="Specification <?php echo e($default_lang); ?>">
                    </div>
                    <?php if(!empty($get_active_lang)): ?> 
                <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <?php
                $get_lang_name = $get_lang->lang_code;
                $get_lang_code = $get_lang->lang_name;
                ?>
                    <div class="col-lg-2">
                        <input id="<?php echo e($get_lang_code); ?>_spectext0"  name="<?php echo e($get_lang_name); ?>_spectext0" placeholder="Enter Specification in In <?php echo e($get_lang_code); ?>" class="form-control" type="text" >
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
              <div class="col-lg-2">
            <p id="addmore" style="display:none;" ><a class="pro-des-add" onClick="addspecificationFormField();"  style="cursor:pointer;color:#ffffff;width: 100%; display: inline-block; text-align: center;"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_MORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_MORE'): trans($MER_OUR_LANGUAGE.'.MER_ADD_MORE')); ?></a> </p></div>

					 
				
				
                <!--
					<div class="col-lg-3">
                       <input type="text" class="form-control" id="fr_spectext0" name="fr_spectext0" placeholder="Specification in French">
                    </div>-->
					
                    <?php echo e(Form::hidden('','1',array('id'=>'specificationid1'))); ?>

                    
                    <?php echo e(Form::hidden('specificationcount','0',array('id'=>'specificationcount'))); ?>

		    
                    
                </div>


 		<div class="add-expand">
 		<div  id="divspecificationTxt" >
        

		</div>  
        
        </div>

		 <div class="form-group"  >

		 <div class="col-lg-3" style=""> </div>
            
		 <div id="spec_error_msg"  style="color:#F00;font-weight:800"> </div>	
		</div> 
<div class="form-group">
                    
			<label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCT_SIZE')); ?><span class="text-sub"></span></label>

                   	<div class="col-lg-8">
			<input type="radio" id="pro_siz" name="pro_siz"  onClick="product_siz('pro_size', 'block');" value="0"> <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES')); ?></label>
			<input type="radio" name="pro_siz"  id="pro_siz" onClick="product_siz('pro_size', 'none');" value="1" checked> <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?>  </label>
			<?php echo e(Form::label('','',['class'=>'sample'])); ?>

                   	 </div>
                </div>
<div class="form-group" id="pro_size" style="display:none">

                    <label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_SIZE')); ?><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <select class="form-control" name="Product_Size[]" id="Product_Size" multiple="multiple" >
                       
 			     <?php $__currentLoopData = $productsize; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				  <?php if($size->si_name!='no size'): ?>
           		 <option  value="<?php echo e($size->si_id); ?>"><?php echo $size->si_name; ?></option>
				  <?php endif; ?>
          		 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         
        			</select>
					<label><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SIZES')!= '') ? trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_SIZES') : trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_SIZES')); ?> <a href="<?php echo e(url('')); ?>/manage_size"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD') : trans($MER_OUR_LANGUAGE.'.MER_ADD')); ?></a></label>
                    <div id="size_error_msg"  style="color:#F00;font-weight:800"> </div>
					</div>
                </div>

	<div class="form-group" id="sizediv" style="display:none;">

 <input type="hidden" id="productsizecount" name="productsizecount" value="0">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YOUR_SELECT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YOUR_SELECT_SIZE') :  trans($MER_OUR_LANGUAGE.'.MER_YOUR_SELECT_SIZE')); ?><span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showsize" ></p>
                       <?php echo e(Form::hidden('si','0',array('id'=>'si'))); ?>

                      
                    </div>
</div>
<div class="form-group" id="quantitydiv" style="display:none;">
  		 <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_QUANTITY') : trans($MER_OUR_LANGUAGE.'.MER_QUANTITY')); ?><span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showquantity" ></p>
                      <?php echo e(Form::hidden('','0',array('id'=>'sq'))); ?>

                       
                    </div>
</div>
				

				<div class="form-group" >
                    <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_COLOR_FIELD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_COLOR_FIELD') : trans($MER_OUR_LANGUAGE.'.MER_ADD_COLOR_FIELD')); ?><span class="text-sub"></span></label>

                   <div class="col-lg-8">
						<input type="radio" id="productcolor" name="productcolor" onClick="setVisibility1('sub3', 'block');" value="0"   ><label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> </label>
					    <input type="radio" id="productcolor" name="productcolor" onClick="setVisibility1('sub3', 'none');" value="1" checked> <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?>  </label>
					
						<?php echo e(Form::label('','',['class'=>'sample'])); ?>

                    </div>
                </div>

 				<div class="form-group" id="sub3" style="display: none;">
                    <label for="text2" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_COLOR')); ?></label>

                   <div class="col-lg-3">

   	 				<select class="form-control" id="selectprocolor" name="selectprocolor" onchange="getcolorname(this.value)">
     
       				<option value="0">-<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_PRODUCT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_PRODUCT_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_PRODUCT_COLOR')); ?>-</option>   
          			<?php $__currentLoopData = $productcolor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
           			 <option style="background:<?php echo e($color->co_code); ?>"  value="<?php echo e($color->co_id); ?>"><?php echo $color->co_name; ?></option>
          		    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        			</select>
  					<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_COLORS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MORE_CUSTOM_COLORS') : trans($MER_OUR_LANGUAGE.'.MER_MORE_CUSTOM_COLORS')); ?> <a href="<?php echo e(url('')); ?>/mer_manage_color"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD'): trans($MER_OUR_LANGUAGE.'.MER_ADD')); ?></a>
					  <div id="color_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
			<div class="form-group" id="colordiv" style="display:none;">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YOUR_SELECT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YOUR_SELECT_COLOR') : trans($MER_OUR_LANGUAGE.'.MER_YOUR_SELECT_COLOR')); ?><span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showcolor"></p>
                       <?php echo e(Form::hidden('co','0',array('id'=>'co'))); ?>

                       
                    </div>
                </div>
			<div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_DAYS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DELIVERY_DAYS') : trans($MER_OUR_LANGUAGE.'.MER_DELIVERY_DAYS')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <input type="text"  maxlength="3" class="form-control" placeholder="" id="Delivery_Days" name="Delivery_Days" onKeyPress="return onlyNumbers(this);"> Days
					   <br><div id="delivery_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				
				 
				
 <input type="hidden" name="Select_Merchant" id="Select_Merchant" value="<?php echo Session::get('merchantid'); ?>" />
				 
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_STORE') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_STORE')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                      <select class="form-control" name="Select_Shop" id="Select_Shop" onChange="check();">
						<option value="">-<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_STORE') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_STORE')); ?>-</option>    
					  </select>
					  <div id="address"></div>
					  <div id="store_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
                <?php echo e(Form::hidden('exist','',array('id'=>'exist'))); ?>

                
                
			  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS') : trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS')); ?><span class="text-sub"></span></label>

                    <div class="col-lg-8">

                       <textarea class="form-control" id="Meta_Keywords" name="Meta_Keywords" placeholder="Enter Meta Keywords <?php echo e($default_lang); ?>"></textarea>
					  <div id="meta_key_error_msg"  style="color:#F00;font-weight:800"> </div>
					   
                    </div>
					
                </div>
				 
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php
                $get_lang_name = $get_lang->lang_code;
				$get_lang_code = $get_lang->lang_name;
				?>
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS') : trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS')); ?>(<?php echo e($get_lang_code); ?>) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
						<textarea id="Meta_Keywords_<?php echo e($get_lang_name); ?>" placeholder="Enter Keywords in <?php echo e($get_lang_code); ?>" name="Meta_Keywords_<?php echo e($get_lang_name); ?>" class="form-control" ></textarea>
						<div id="meta_key_error_msg"  style="color:#F00;font-weight:800"  ></div>
					</div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
				<?php /*<!--
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS');} ?>({{ Helper::lang_name() }})<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Keywords_fr" placeholder="Enter Meta Keywords in French" name="Meta_Keywords_fr"></textarea>
					   <div id="meta_key_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                  </div>--> */?>
				 <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')); ?><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Description" name="Meta_Description" placeholder="Enter Meta Description <?php echo e($default_lang); ?>"></textarea>
					   <div id="meta_desc_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				 
				<?php if(!empty($get_active_lang)): ?> 
				<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
				<?php
                $get_lang_name = $get_lang->lang_code;
				$get_lang_code = $get_lang->lang_name;
				?>
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')); ?>(<?php echo e($get_lang_code); ?>) :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
						<textarea id="Meta_Description_<?php echo e($get_lang_name); ?>" placeholder="Enter Meta Description in <?php echo e($get_lang_code); ?>" name="Meta_Description_<?php echo e($get_lang_name); ?>" class="form-control" ></textarea>
						<div id="meta_desc_ <?php echo e($get_lang_name); ?>_error_msg"  style="color:#F00;font-weight:800"  > </div>
					</div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
				<?php /*
                <!--
				<div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION');} ?>({{ Helper::lang_name() }})<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Description_fr" name="Meta_Description_fr" placeholder="Enter Meta Description in French"></textarea>
					   <div id="meta_desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				--> */ ?>
				<?php echo e(Form::hidden('mer_commission',$mer_commission,array('id'=>'mer_commission'))); ?>

			   
				<div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Cash Back (<?php echo e(Helper::cur_sym()); ?>)<span class="text-sub">*</span></label>
					<div class="col-lg-8">
                       <input class="form-control" type="text"  maxlength="5" id="cash_back" name="cash_price" value="0" onKeyPress="return onlyNumbers(this);">
 					</div>
               </div>


                <?php /*  Product Policy details */ ?>
                
               
                <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_APPLY_CANCEL')!= '') ? trans(Session::get('mer_lang_file').'.BACK_APPLY_CANCEL') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_CANCEL')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="radio" id="allow_cancel" name="allow_cancel"  value="1" onClick="setPolicyDisplay('cancel_tab', 'block')"> <label class="sample"  ><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES')); ?></label>
                        <input type="radio" id="notallow_cancel" name="allow_cancel"  value="0" checked  onclick="setPolicyDisplay('cancel_tab', 'none')"><label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> </label>

                            <?php echo e(Form::label('','',['class' => 'sample'])); ?>

                        
                    </div>
                </div>


                <div id="cancel_tab" style="display:none;">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION')); ?> <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="cancellation_policy" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION')!= '') ? trans(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_CANCEL_DESCRIPTION')); ?>"><?php echo e(old('cancellation_policy')); ?></textarea>
                           <div id="cancellation_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                     
                 <?php if(!empty($get_active_lang)): ?> 
                 <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php
                  $get_lang_name = $get_lang->lang_name;
                 ?>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_CANCEL_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="cancellation_policy_<?php echo e($get_lang_name); ?>" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION')!= '') ? trans(Session::get('mer_lang_file').'.BACK_ENTER_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_CANCEL_DESCRIPTION')); ?>"><?php echo e(old('cancellation_policy_'.$get_lang_name)); ?></textarea>
                           <div id="cancellation_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   <?php endif; ?>
                    <div class="form-group">
                        <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>


                        <div class="col-lg-8">
                                <label for="text1" class="control-label "><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION')); ?><span class="text-sub"></span></label>     
                                <input placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_CANCEL_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_CANCEL_DESCRIPTION')); ?>"  class="form-control" type="text" maxlength="3" id="cancellation_days" name="cancellation_days" value="<?php echo e(old('cancellation_days')); ?>">
                                    <div id="cancellation_days_error_msg"  style="color:#F00;font-weight:800"> </div>         
                        </div>
                    </div>

                </div> 

                
                
                <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_APPLY_RETURN')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_APPLY_RETURN') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_RETURN')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="radio" id="allow_return" name="allow_return"  value="1" onclick="setPolicyDisplay('return_tab', 'block')" > <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES')); ?></label>
                        <input type="radio" id="notallow_return" name="allow_return"  value="0" checked onclick="setPolicyDisplay('return_tab', 'none')" ><label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> </label>

                            <?php echo e(Form::label('','',['class'=>'sample'])); ?>

                       
                    </div>
                </div>


                <div id="return_tab" style="display:none;">
                     <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION')); ?>  <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="return_policy" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_RETURN_DESCRIPTION')); ?>"><?php echo e(old('return_policy')); ?></textarea>
                           <div id="return_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                    
                     <?php if(!empty($get_active_lang)): ?> 
                     <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                      <?php
                      $get_lang_name = $get_lang->lang_name;
                     ?>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_RETURN_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>) <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="return_policy_<?php echo e($get_lang_name); ?>" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_RETURN_DESCRIPTION')); ?>"><?php echo e(old('return_policy_'.$get_lang_name)); ?></textarea>
                           <div id="return_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>  
                    <div class="form-group">
                        <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>


                        <div class="col-lg-8">
                                    <label for="text1" class="control-label"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION')); ?><span class="text-sub"></span></label>
                                    <input placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_RETURN_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_RETURN_DESCRIPTION')); ?>"  class="form-control" type="text" maxlength="3"  id="return_days" name="return_days" value="<?php echo e(old('return_days')); ?>">
                                    <div id="return_days_error_msg"  style="color:#F00;font-weight:800"> </div>         
                        </div>
                    </div>

                </div> 


                
                
                <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_APPLY_REPLACEMENT')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_APPLY_REPLACEMENT') : trans($MER_OUR_LANGUAGE.'.BACK_APPLY_REPLACEMENT')); ?> <span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="radio" id="allow_replace" name="allow_replace"  value="1" onclick="setPolicyDisplay('replace_tab', 'block')"  > <label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YES') : trans($MER_OUR_LANGUAGE.'.MER_YES')); ?></label>
                        <input type="radio" id="notallow_replace" name="allow_replace"  value="0" checked  onclick="setPolicyDisplay('replace_tab', 'none')" ><label class="sample"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NO') : trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> </label>


                        <?php echo e(Form::label('','',['class'=>'sample'])); ?>

                    </div>
                </div>

                <div id="replace_tab" style="display:none;">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')); ?> <span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="replacement_policy" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_REPLACE_DESCRIPTION')); ?>"><?php echo e(old('replacement_policy')); ?></textarea>
                           <div id="replacement_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                    
                     <?php if(!empty($get_active_lang)): ?> 
                     <?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                      <?php
                     $get_lang_name = $get_lang->lang_name;
                     ?>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_REPLACE_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.BACK_REPLACE_DESCRIPTION')); ?> (<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>

                        <div class="col-lg-8" id="description">
                           <textarea id="wysihtml5" class="wysihtml5 form-control" rows="10"  name="replacement_policy_<?php echo e($get_lang_name); ?>" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.BACK_ENTER_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_ENTER_REPLACE_DESCRIPTION')); ?>"><?php echo e(old('replacement_policy_'.$get_lang_name)); ?></textarea>
                           <div id="replacement_policy_error_msg"  style="color:#F00;font-weight:800"> </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>  
                    <div class="form-group">
                        <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])); ?>


                        <div class="col-lg-8">
                                     <label for="text1" class="control-label "><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION')); ?><span class="text-sub"></span></label><br>
                                    <input placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.BACK_DAYS_REPLACE_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.BACK_DAYS_REPLACE_DESCRIPTION')); ?>"  class="form-control" type="text" maxlength="3" id="replace_days" name="replace_days" value="<?php echo e(old('Replace_days')); ?>">
                                    <div id="replace_days_error_msg"  style="color:#F00;font-weight:800"> </div>         
                        </div>
                    </div>

                </div> 
                

                <?php /*  Product Policy details ends */ ?>



		<div class="form-group">
           <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_IMAGE')); ?> <span class="text-sub">*</span><br>(Max 5)</label>
			<span class="errortext red" style="color:red;"><em><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS') : trans($MER_OUR_LANGUAGE.'.MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS')); ?> <?php echo e($PRODUCT_WIDTH); ?> x <?php echo e($PRODUCT_HEIGHT); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PIXELS') : trans($MER_OUR_LANGUAGE.'.MER_PIXELS')); ?></em></span>
               <div class="col-lg-8" id="img_upload">
            	   		<div style="display: block; overflow: hidden;">
               				<input type="file" name="file[]" id="fileUpload1" value="" onchange="imageval(1)"  required />
               				<a href="javascript:void(0);"  title="Add field" class="chose-file-add" ><span id="add_button" style="cursor:pointer;width:84px;"><?php echo e((Lang::has(Session::get('mer_lang_file').'.BACK_ADD')!= '') ? trans(Session::get('mer_lang_file').'.BACK_ADD'): trans($MER_OUR_LANGUAGE.'.BACK_ADD')); ?></span></a>
           	       		</div>
        	 	</div> 
  		   			<?php echo e(Form::hidden('count','1',array('id'=>'count'))); ?>

				

			    <div id="img_error_msg"  style="color:#F00;font-weight:800"></div>
		</div>
                </div>
				  <div class="form-group" style="display:flex;">
                    <?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'pass1'])); ?>

                    

                    <div class="col-lg-8">
                   	 	<button class="btn btn-warning btn-sm btn-grad" id="submit_product" ><a style="color:#fff"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCT') : trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCT')); ?></a></button>
                     	<button type="reset" class="btn btn-danger btn-sm btn-grad" style="color:#fffff"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET') : trans($MER_OUR_LANGUAGE.'.MER_RESET')); ?></button>
                   </div>
				 </div>

                
         <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     <?php echo $adminfooter; ?>

    <!--END FOOTER -->

 
 <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

	<script type="text/javascript">
     function check_sku_no()
     {
        var sku_no    = $('#sku_no').val();
       // alert(sku_no);
        var sku_pro_id = "";
        $.ajax({
            type: "POST",
            url : "<?php echo e(url('check_sku_no_exist')); ?>",
            data : {'sku_no':sku_no,'sku_pro_id':sku_pro_id},
            success:function(response)
            {
                console.log(response);
              if(response == 2)
              {   
                $("#sku_no").css('border', '1px solid red').focus();                 
                 $("#skuno_error_msg").html('SKU Number Already Exist');
                 return false;
              }
              else
              {
                $("#sku_no").css('border','');                
                 $("#skuno_error_msg").html('');
                 return true;
              }
            }
        });
     }
	$("#cash_back").on("change",function() {
		
    	var cash_back = (this.value);
		
		var mer_commission = $('#mer_commission').val();
		var Discounted_Price = $('#Discounted_Price').val();

		var commission=0;
		if(Discounted_Price!=0){
			commission = ((Discounted_Price * mer_commission)/100);
		}

		if(commission < cash_back){
			alert("Cash Back should be lesser than admin commission amount of this product.");
            
		}
	});


$(document).ready(function(){
    var maxField = 5; //Input fields increment limitation
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#img_upload'); //Input field wrapper //div
	
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
		    x++; //Increment field counter
			var fieldHTML = '<div style="display:block; width:auto; margin-top:15px;"><input type="file" name="file[]" id="fileUpload'+x+'" onchange="imageval('+x+')"  value="" required/><div id="remove_button"><a href="javascript:void(0);"  title="Remove field" style="color:#ffffff;">Remove</a></div></div>'; //New input field html 
            
            $(wrapper).append(fieldHTML); // Add field html
			
			document.getElementById('count').value = parseInt(x);
        }
    });
    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
		document.getElementById('count').value = parseInt(x);
    });
});

   function addproductimageFormField(){

	var id = document.getElementById("aid").value;
	var count_id = document.getElementById("count").value;	  
	if(count_id < 4)
	{
	document.getElementById('count').value = parseInt(count_id)+1;
	jQuery.noConflict()
	jQuery("#divTxt").append("<tr style='height:5px;' > <td> </td> </tr><tr id='row" + count_id + "' style='width:100%'><td width='20%'><input type='file' name='file_more"+count_id+"' /></td><td>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + count_id + "\"); return false;' style='color:#F60;' >Remove</a></td></tr>");     
	jQuery('#row' + id).highlightFade({    speed:1000 });
	id = (id - 1) + 2;
	document.getElementById("aid").value = id;	 
	}
}
        
   function removeFormField(id)
 {
	 //alert(id);
	var count_id = document.getElementById("count").value;	
	document.getElementById('count').value = parseInt(count_id)-1;
	jQuery(id).remove();
}

    </script>
    

<script language="JavaScript">
function get_specification_details()
{
	var main_cat=$("#Product_Category").val();
	var sec_main_cat=$("#Product_MainCategory").val();
	/*Top Category*/	
	if(main_cat == "0"){
		$("#Product_Category").css('border', '1px solid red'); 
		$('#category_error_msg').html('Please Select Category');
		$("#Product_Category").focus();
		return false;
	}else{
		$("#Product_Category").css('border', ''); 
		$('#category_error_msg').html('');
	}
	/*Main Category*/	
	if(sec_main_cat == "0")
	{
		$("#Product_MainCategory").css('border', '1px solid red'); 
		$('#main_cat_error_msg').html('Please Select Main Category');
		$("#Product_MainCategory").focus();
		return false;
	}else{
		$("#Product_MainCategory").css('border', ''); 
		$('#main_cat_error_msg').html('');
	}
	if(sec_main_cat!="" && main_cat!=""&& sec_main_cat!="0" && main_cat!="0")
	{
		$.post("<?php echo url(''); ?>/get_spec_related_to_cat_mer",
		{
			main_cat: main_cat,
			sec_main_cat: sec_main_cat
		},
		function(data, status){
			//alert(data);
			var count_id = document.getElementById("specificationcount").value;
			var count_id=parseInt(count_id);
			{
				$("#spec_grp0").html(data);
				$("#pro_spec0").html("<option value='0'>-- Select --</option>");
				for(var i=1;i<=count_id;i++)
				{
					$("#spec_grp"+i).html(data);
					$("#pro_spec"+i).html("<option value='0'>-- Select --</option>");
				} 
			}
		});
	}
	else
	{
		$("input[name='specification'][value='2']").attr('checked', true);
		return false;
	}
}
function setVisibility(id, visibility)
 {
document.getElementById(id).style.display = visibility;
document.getElementById('addmore').style.display =visibility;
document.getElementById('divspecificationTxt').style.display =visibility;
}
function setVisibility1(id, visibility) 
{
document.getElementById(id).style.display = visibility;
document.getElementById('colordiv').style.display =visibility;
}
function setshipVisibility(id, visibility) 
{
document.getElementById(id).style.display = visibility;
 
}
function product_siz(id, visibility){
	document.getElementById(id).style.display = visibility;
	document.getElementById('pro_size').style.display =visibility;
}
function product_col(id, visibility){
	document.getElementById(id).style.display = visibility;
	document.getElementById('pro_col').style.display =visibility;
}
</script>

<script type="text/javascript">
  function addspecificationFormField(){

		var count_id = document.getElementById("specificationcount").value;
		var selectspec=$("#pro_spec"+count_id+" option:selected").val();
		var spectext=$("#spectext"+count_id).val();
		var fr_spectext=$("#fr_spectext"+count_id).val();

		if(selectspec!=0  && spectext!="" && fr_spectext!=""){
			var id = document.getElementById("specificationid1").value;
			var count_id = document.getElementById("specificationcount").value;
			var nameid=parseInt(count_id)+1;
			var result=$("#spec_grp0").html();
            lang_div ='';
             <?php if(!empty($get_active_lang)) { 
            foreach($get_active_lang as $get_lang) {
            $get_lang_name = $get_lang->lang_name;
            $get_lang_code = $get_lang->lang_code; 
            $spc_value_dynamic = 'spc_value_'.$get_lang_code; 
            ?>  
          lang_div += "<div class='col-lg-2'><input type='text' class='form-control' name='<?php echo $get_lang_code;?>_spectext"+ nameid  + "' placeholder='Specification in <?php echo $get_lang_name;?>' required/></div>";
          <?php } } ?>
			//if(count_id < 2){

			document.getElementById('specificationcount').value = parseInt(count_id)+1;
			jQuery("#divspecificationTxt").append(" <div class='form-group '  id='spec"+ nameid + "' '><div class='col-lg-2 void_one'></div><div class='col-lg-2'><select name='spec_grp"+ nameid  + "' id='spec_grp"+ nameid  + "' onChange='spcfunction("+nameid+",this.value);' class='form-control' required>"+result+"</select></div><div class='col-lg-2'><select name='spec"+ nameid  + "' id='pro_spec"+ nameid  + "' class='form-control' required><option  value='0'>-- select specification--</option></select></div><div class='col-lg-2'><input type='text' class='form-control' name='spectext"+ nameid  + "' placeholder='Specification <?php echo $default_lang; ?>' required/></div>"+lang_div+"<div class='col-lg-2'><a style='width:100%;display:inline-block; text-align:center;' class='pro-des-rem' href='#' onClick='removespecFormField(\"#spec" + nameid + "\"); return false;' style='color:#ffffff;cursor:pointer;' >Remove</a></div></div>");     
			id = (id - 1) + 1;
			document.getElementById("specificationid1").value = id;

			//}
			//else{	
			//alert("<?php if (Lang::has(Session::get('mer_lang_file').'.MER_MAXIMUM_LIMIT_REACHED')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MAXIMUM_LIMIT_REACHED');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_MAXIMUM_LIMIT_REACHED');} ?>");
			//return false;
			//}

		}
		else
		{
			alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE')); ?>");
		}	


	}
function removespecFormField(id) 
{
	var count_id = document.getElementById("specificationcount").value;
	count_id=count_id-1;

	document.getElementById("specificationcount").value=count_id;	

	jQuery(id).remove();
}
   

function adddeliverypolicyFormField()
	{
	 var id = document.getElementById("deliverypolicyid1").value;
	  var count_id = document.getElementById("deliverypolicycount").value;

 
	  if(count_id < 10){
 			document.getElementById('deliverypolicycount').value = parseInt(count_id)+1;
		 jQuery("#divdeliverypolicy").append(" <div class='form-group' id='delivery"+ id + "'><label for='text1' class='control-label col-lg-2'>Delivery Policy <span class='text-sub'></span></label><div class='col-lg-8'> <input  class='form-control' type='text' id='Delivery_Policy'"+id+"' name='Delivery_Policy'"+id+"'><div class='col-lg-8'><a href='#' onClick='removedeliveryFormField(\"#delivery" + id + "\"); return false;' style='color:#F60;' >Remove</a></div></div></div>");     
		 id = (id - 1) + 1;
     document.getElementById("deliverypolicyid1").value = id;

			}
		else
		{	
		alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MAXIMUM_LIMIT_REACHED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAXIMUM_LIMIT_REACHED') : trans($MER_OUR_LANGUAGE.'.MER_MAXIMUM_LIMIT_REACHED')); ?>");
		return false;
		}
		
	}
function removedeliveryFormField(id) {
	var count_id = document.getElementById("deliverypolicycount").value;
	 count_id=count_id-1;

document.getElementById("deliverypolicycount").value=count_id;	

        jQuery(id).remove();
    }
   

</script>	

 

<script>
$(document).ready(function(){
	
	 var passmerchantid = 'id='+<?php echo Session::get('merchantid');?>;
		   $.ajax( {
			      type: 'get',
				  data: passmerchantid,
				  url: 'mer_product_getmerchantshop',
				  success: function(responseText){  
				   if(responseText)
				   {  
					$('#Select_Shop').html(responseText);					   
				   }
				}		
			});		
	
	});

 	function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode        
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
            
         return true;
          
      }

	function getshop_ajax(id)
	{
		 
		 var passmerchantid = 'id='+id;
		   $.ajax( {
			      type: 'get',
				  data: passmerchantid,
				  url: 'product_getmerchantshop',
				  success: function(responseText){  
				   if(responseText)
				   {  
					$('#Select_Shop').html(responseText);					   
				   }
				}		
			});		
	}



	function getcolorname(id){
	
		 var passcolorid = 'id='+id+"&co_text_box="+$('#co').val();
		   $.ajax( {
			      type: 'get',
				  data: passcolorid,
				  url: 'product_getcolor',
				  success: function(responseText){  
				   if(responseText)
				   { 	 
				  // alert(responseText)
					var get_result = responseText.split(',');
					if(get_result[3]=="success")
					{
						$('#colordiv').css('display', 'block'); 
						
					$('#showcolor').append('<span style="width:195px;padding:3px;border:4px solid '+get_result[2]+';margin:5px; display:inline-table">'+get_result[0]+'<input type="checkbox"  name="colorcheckbox'+get_result[1]+'"style="padding-left:30px;" checked="checked" value="1" ></span>&nbsp;&nbsp;');	

					 
		
					var co_name_js = $('#co').val();	
					$('#co').val(get_result[1]+","+co_name_js);	  
                    $('#selectprocolor').find('option:first').attr('selected', 'selected'); 
						
					}
					else if(get_result[3]=="failed")
					{
						alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ALREADY_COLOR_IS_CHOOSED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ALREADY_COLOR_IS_CHOOSED')  : trans($MER_OUR_LANGUAGE.'.MER_ALREADY_COLOR_IS_CHOOSED')); ?>.");
                         
					}
					else
					{
							alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')  : trans($MER_OUR_LANGUAGE.'.MER_SOMETHING_WENT_WRONG')); ?>.");
					}
					
				   }
				}		
			});		
		
	}
	function getsizename(id)
	{
		 var passsizeid = 'id='+id+"&si_text_box="+$('#si').val();
		   $.ajax( {
			      type: 'get',
				  data: passsizeid,
				  url: 'product_getsize',
				  success: function(responseText){  
				   if(responseText)
				   { 	 //alert(responseText);
					var get_result = responseText.split(',');
					if(get_result[3]=="success")
					{
                                  		var count=parseInt($('#productsizecount').val())+1;
						$("#productsizecount").val(count);
						$('#sizediv').css('display', 'block'); 
						//$('#quantitydiv').css('display', 'block'); 
						
					$('#showsize').append('<span style="padding-right:10px;">Select Size:</span><span style="color:#ff0099;padding-right:90px">'+get_result[2]+'<input type="checkbox"  name="sizecheckbox'+get_result[1]+'"style="padding-left:30px;" checked="checked" value="1" ></span>');
					$('#showquantity').append('<input type="text" name="quantity'+get_result[1]+'" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input> ');
		
					var co_name_js = $('#si').val();	
					$('#si').val(get_result[1]+","+co_name_js);	  
						
					//alert($('#si').val());
					}
					else if(get_result[3]=="failed")
					{
						alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ALREADY_SIZE_IS_CHOOSED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ALREADY_SIZE_IS_CHOOSED') : trans($MER_OUR_LANGUAGE.'.MER_ALREADY_SIZE_IS_CHOOSED')); ?>.");
					}
					else
					{
							alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SOMETHING_WENT_WRONG')  : trans($MER_OUR_LANGUAGE.'.MER_SOMETHING_WENT_WRONG')); ?> .");
					}
					
				   }
				}		
			});		
		
	}
	function get_maincategory(id)
	{
		 var passcategoryid = 'id='+id;
		   $.ajax( {
			      type: 'get',
				  data: passcategoryid,
				  url: 'mer_product_getmaincategory',
				  success: function(responseText){  
				   if(responseText)
				   { 	 
					$('#Product_MainCategory').html(responseText);	
					$('#Product_SubCategory').html(0);	
					$('#Product_SecondSubCategory').html(0);					
				   }
				}		
			});		
	}

	function get_subcategory(id)
	{
		 var passsubcategoryid = 'id='+id;
		   $.ajax( {
			         type: 'get',
				  data: passsubcategoryid,
				  url: 'mer_product_getsubcategory',
				  success: function(responseText){  
				   if(responseText)
				   { 	 
					$('#Product_SubCategory').html(responseText);					   
				   }
				}		
			});		
	}

	function get_second_subcategory(id)
	{
		 var passsecondsubcategoryid = 'id='+id;
		   $.ajax( {
			      type: 'get',
				  data: passsecondsubcategoryid,
				  url: 'mer_product_getsecondsubcategory',
				  success: function(responseText){  
				   if(responseText)
				   {    
					$('#Product_SecondSubCategory').html(responseText);					   
				   }
				}		
			});		
	}

	function check(){

			var mer_id   = $('#Select_Merchant').val();
			var store_id = $('#Select_Shop').val();
			var title    = $('#Product_Title').val();
			var passdata = "mer_id="+mer_id+"&store_id="+store_id+"&title="+title;
			$.ajax( {
			      type: 'get',
				  data: passdata,
				  url: 'check_product_exists',
				  success: function(responseText){  
				   if(responseText==1){  //already exist
					  alert("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE') : trans($MER_OUR_LANGUAGE.'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE')); ?>");
					  $("#exist").val("1"); //already exist
					  $("#Product_Title").css('border', '1px solid red'); 
					  $('#Select_Shop').css('border', '1px solid red');
					  $('#title_error_msg').html("<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE') : trans($MER_OUR_LANGUAGE.'.MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE')); ?>");	
					  $("#Product_Title").focus();
					  return false;				   
				   }else if(responseText==0){
					  $("#exist").val("0");
				   }
				}		
			});	
     }
$("#Select_Shop").change(function(){
    	var shop_id  = $(this).find('option:selected').val();
		var passdata = "shop_id="+shop_id;
		$.ajax({
			      type: 'get',
				  data: passdata,
				  url: 'store_details',
				  success: function(responseText){ 
					$('#address').html(responseText);
				  }		
		});	  
	});

/*Form Validation*/
function imageval(i){	/*Image size validation*/
	 
	//var img_count = $("#count").val();
 	//Get reference of FileUpload.
	// var i;
	//for (i = 0; i <=img_count; i++) {
  		var fileUpload = document.getElementById("fileUpload"+i);
//	}

	//Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
    //if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        //if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (width <= 800 || height <= 800) {
                        //alert("Image Height and Width should have <?php echo $PRODUCT_WIDTH; ?> * <?php echo $PRODUCT_HEIGHT; ?> px.");
                        return false;
                    }
                   // alert("Uploaded image has valid Height and Width.");
                    return true;
                };
 
            }
       // } else {
           // alert("This browser does not support HTML5.");
           // return false;
        //}
    //} else {
       // alert("Please select a valid Image file.");
      //  return false;
   // }
}
function spcfunction(count,spc_grop_id){
	
	
	//var spc_grop_id = $('spec_grp'+count).val();
	var pass_spc_grp_id = 'spc_group_id='+spc_grop_id;
		   $.ajax( {
			      type: 'get',
				  data: pass_spc_grp_id,
				  url: 'product_get_spec',
				  success: function(responseText){  
				   if(responseText)
				   { 	
					  // alert(responseText); 
					$('#pro_spec'+count).html(responseText);					   
				   }
				}		
			});		
}

$( document ).ready(function() {


			$('#specificationdetails').hide();
			<?php 
			/* print_r($get_active_lang); */
			if(!empty($get_active_lang)) { 
			foreach($get_active_lang as $get_lang) {
			$get_lang_code = $get_lang->lang_code;
			$get_lang_name = strtolower($get_lang->lang_name);
			?>
			var title_<?php echo $get_lang_code; ?>  = $('#Product_Title_<?php echo $get_lang_name; ?>');
			var store_add_one_<?php echo $get_lang_code; ?>  = $('#store_add_one_<?php echo $get_lang_name; ?>');
			var store_add_two_<?php echo $get_lang_code; ?>  = $('#store_add_two_<?php echo $get_lang_name; ?>');
			var meta_keyword_<?php echo $get_lang_code; ?>  = $('#meta_keyword_<?php echo $get_lang_name; ?>');
			var meta_description_<?php echo $get_lang_code; ?>  = $('#meta_description_<?php echo $get_lang_name; ?>');
			<?php } }?>

			var title 		     = $('#Product_Title');
			var category		 = $('#Product_Category');
			var maincategory 	 = $('#Product_MainCategory');
			var subcategory 	 = $('#Product_SubCategory');
			var secondsubcategory= $('#Product_SecondSubCategory');
			var originalprice 	 = $('#Original_Price');
			var inctax           = $('#inctax');
			var discountprice 	 = $('#Discounted_Price');
			var shippingamt      = $('#Shipping_Amount');
			var description      = $('#Description');
			var wysihtml5 		 = $('#wysihtml5');
			var delivery_days    = $('#Delivery_Days');
			var shop		     = $('#Select_Shop');
			var metakeyword		 = $('#Meta_Keywords');
			var metadescription	 = $('#Meta_Description');
			var file		     = $('#file');
            var pquantity        = $('#Quantity_Product');
		    var sku_no		     = $('#sku_no');
			var desc             = $('#desc');
			var product_size     = $("#Product_Size");
			var cash_back		 = $("#cash_back");

	
	/*Product Quantity*/
	$('#Quantity_Product').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    pquantity.css('border', '1px solid red'); 
			$('#qty_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
			pquantity.focus();
			return false;
		}else{			
            pquantity.css('border', ''); 
			$('#qty_error_msg').html('');	        
		}
    });

	/*Product Original Price*/
	$('#Original_Price').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    originalprice.css('border', '1px solid red'); 
				$('#org_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
				originalprice.focus();
				return false;
		}else{			
            originalprice.css('border', ''); 
			$('#org_price_error_msg').html('');	        
		}
    });

	/*Product Discount Price*/
	$('#Discounted_Price').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    discountprice.css('border', '1px solid red'); 
			$('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
			discountprice.focus();
			return false;
		}else{			
            discountprice.css('border', ''); 
			$('#dis_price_error_msg').html('');	        
		}
    }); 

	/*Product Shipping Amount*/
	$('#Shipping_Amount').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    shippingamt.css('border', '1px solid red'); 
			$('#ship_amt_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
			shippingamt.focus();
			return false;
		}else{			
            shippingamt.css('border', ''); 
			$('#ship_amt_error_msg').html('');	        
		}
    });

	/*Delivery Days*/
	$('#Delivery_Days').keyup(function() { 
		if (this.value.match(/[^0-9-()\s]/g)){ 
		this.value = this.value.replace(/[^0-9-()\s]/g, ''); 
		} 
	});

	
	/*cash back*/
	$('#cash_back').keyup(function() { 
		if (this.value.match(/[^0-9-()\s]/g)){ 
		this.value = this.value.replace(/[^0-9-()\s]/g, ''); 
		} 
	});
	
    /*including tax*/
	$('#inctax').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
		    inctax.css('border', '1px solid red'); 
			$('#tax_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
			inctax.focus();
			return false;
		}else{			
            inctax.css('border', ''); 
			$('#tax_error_msg').html('');	        
		}
    });	
	
	
$('#submit_product').click(function() {

	 var sizeid             = $("#Product_Size").val();
	 var count              = $("#Product_Size :selected").length;
	 var checkedoptioncolor = $('input:radio[name=productcolor]:checked').val();
	 var shipamtchecked     = $('input:radio[name=shipamt]:checked').val();
	 var colorid            = $("#selectprocolor option:selected").val();
     var checkspec          = $('input:radio[name=specification]:checked').val();
	
	/*Product Title*/
		if($.trim(title.val()) == ""){
			title.css('border', '1px solid red'); 
			$('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_TITLE')); ?> <?php echo e($default_lang); ?>');
			title.focus();
			return false;
		}else{
			title.css('border', ''); 
			$('#title_error_msg').html('');
		}

        <?php 
        if(!empty($get_active_lang)) { 
        foreach($get_active_lang as $get_lang) {
        $get_lang_name = $get_lang->lang_name;
        ?>
        var title_fren = $('#Product_Title_<?php echo $get_lang_name; ?>');
       
        if($.trim(title_fren.val()) == ""){
            title_fren.css('border', '1px solid red'); 
            $('#title_fr_error_msg').html('Please Enter Title In <?php echo $get_lang_name; ?>');
            title_fren.focus();
            return false;
        }else{
            title_fren.css('border', ''); 
            $('#title_error_msg').html('');
        }
    <?php } } ?>
		
	/*Product Title french*/
		
		
	/*Top Category*/	
		if(category.val() == 0){
			category.css('border', '1px solid red'); 
			$('#category_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') :  trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY')); ?>');
			category.focus();
			return false;
		}else{
			category.css('border', ''); 
			$('#category_error_msg').html('');
		}
	/*Main Category*/	
		if(maincategory.val() == 0)
		{
			maincategory.css('border', '1px solid red'); 
			$('#main_cat_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_MAIN_CATEGORY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_MAIN_CATEGORY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_MAIN_CATEGORY')); ?>');
			maincategory.focus();
			return false;
		}else{
			maincategory.css('border', ''); 
			$('#main_cat_error_msg').html('');
		}
		
		/*if(subcategory.val() == 0)
		{
			subcategory.css('border', '1px solid red'); 
			$('#error_msg').html('Please Select Sub Category');
			subcategory.focus();
			return false;
		}
		else
		{
		subcategory.css('border', ''); 
		$('#error_msg').html('');
		}
		if(secondsubcategory.val() == 0)
		{
			secondsubcategory.css('border', '1px solid red'); 
			$('#error_msg').html('Please Enter Select Sub Category');
			secondsubcategory.focus();
			return false;
		}
		else
		{
		secondsubcategory.css('border', ''); 
		$('#error_msg').html('');
		}
		*/

		/*product_brand
		if($('#product_brand').val() == 0){
			$('#product_brand').css('border', '1px solid red'); 
			$('#brand_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_BRAND')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_BRAND');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_BRAND');}*/ ?>');
			$('#product_brand').focus();
			return false;
		}else{
			$('#product_brand').css('border', ''); 
			$('#brand_error_msg').html('');
		}*/
       /*SKU Number*/
        if(sku_no.val() == ''){
            sku_no.css('border', '1px solid red'); 
            $('#skuno_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_SKU_NUMBER'): trans($MER_OUR_LANGUAGE.'.MER_ENTER_SKU_NUMBER')); ?>');
            sku_no.focus();
            return false;
        }else{
            sku_no.css('border', ''); 
            $('#skuno_error_msg').html('');
        }
		/*Product Qunatity*/
        if(pquantity.val() == 0){
            pquantity.css('border', '1px solid red'); 
            $('#qty_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_PRODUCT_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_PRODUCT_QUANTITY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_PRODUCT_QUANTITY')); ?>');
            pquantity.focus();
            return false;
        }else{
            pquantity.css('border', ''); 
            $('#qty_error_msg').html('');
        }


		/*Product Original Price*/
		if(originalprice.val() == 0){
			originalprice.css('border', '1px solid red'); 
			$('#org_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_ORIGINAL_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_ORIGINAL_PRICE')); ?>');
			originalprice.focus();
			return false;
		}else if(isNaN(originalprice.val()) == true){
			originalprice.css('border', '1px solid red'); 
			$('#org_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
			originalprice.focus();
			return false;
		}else{
			originalprice.css('border', ''); 
			$('#org_price_error_msg').html('');
		}

		/*Product Discount Price*/
		if(discountprice.val() == 0){
			discountprice.css('border', '1px solid red'); 
			$('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DISCOUNT_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DISCOUNT_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DISCOUNT_PRICE')); ?>');
			discountprice.focus();
			return false;
		}else if(isNaN(discountprice.val()) == true){
			discountprice.css('border', '1px solid red'); 
			$('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED') : trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')); ?>');
			discountprice.focus();
			return false;
		}else if(parseInt(discountprice.val()) > parseInt(originalprice.val()) ){
			discountprice.css('border', '1px solid red'); 
			$('#dis_price_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE')); ?>');
			discountprice.focus();
			return false;
		}else{
			discountprice.css('border', ''); 
			$('#dis_price_error_msg').html('');
		}
		if(parseInt($('#inctax').val()) > 100){
			inctax.css('border', '1px solid red'); 
			$('#tax_error_msg').html('Tax Should Less than 100%');
			inctax.focus();
			return false;
		}else{
			inctax.css('border', ''); 
			$('#tax_error_msg').html('');
		}
		
		/*Shipping Amount*/
		if(shipamtchecked==2){
			if(shippingamt.val()==""){
				shippingamt.css('border', '1px solid red'); 
				$('#ship_amt_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_SHIPPING_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_SHIPPING_AMOUNT'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_SHIPPING_AMOUNT')); ?>');
				shippingamt.focus();
				return false;
			}else{
				shippingamt.css('border', ''); 
				$('#ship_amt_error_msg').html('');
			}
		}else if(shipamtchecked==1){
				shippingamt.css('border', ''); 
				$('#ship_amt_error_msg').html('');
		}

		/*Description*/
		if($.trim(wysihtml5.val()) == ''){
			wysihtml5.css('border', '1px solid red'); 
			$('#desc_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DESCRIPTION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DESCRIPTION'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DESCRIPTION')); ?> <?php echo e($default_lang); ?>');
			wysihtml5.focus();
			return false;
		}else{
			wysihtml5.css('border', ''); 
			$('#desc_error_msg').html('');
		}
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = $get_lang->lang_name;
		?>
		if($.trim(wysihtml5.val()) == ''){
			
			wysihtml5.css('border', '1px solid red'); 
			$('#desc_<?php echo $get_lang_code; ?>_error_msg').html('Please Enter Description in <?php echo $get_lang_name; ?>');
			wysihtml5.focus();
			return false;
		}else{
			wysihtml5.css('border', ''); 
			$('#desc_<?php echo $get_lang_code; ?>_error_msg').html('');
		}
		<?php } }?>
		/*Description in french*/
		
		
		/*Specification*/
		if(checkspec==1){ 
			var cntspec=$('#specificationcount').val();
			var i=0;
			for(i=0;i<=cntspec;i++){
			 	var specid=$("#spec"+i+" option:selected").val();
			 	var spectxt=$("#spec"+i).val();
			 	if(specid!=0 && spectxt!==""){
			 		var success=1;	
			 	}else{
			  		var success=0;	
			 	}
			} //for

			/*if(success==0){
				i=i-1;
 		        $('#spec_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT');}*/ ?>');
		        $('#spec'+i).css('border', '1px solid red');
		        $('#spectext'+i).css('border', '1px solid red');	
		        return false;
			}else{
				i=i-1;
			 	$('#spec_error_msg').html(' ');
		      	$('#spec'+i).css('border', '1px solid lightgray');
		        $('#spectext'+i).css('border', '1px solid lightgray');	
			}*/
		}
	
	/*Product Size*/
		//if(count == 0){
		if($('input:radio[name=pro_siz]:checked').val()==0){
			if(count<1){
			$('#Product_Size').focus();
			$('#size_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_PRODUCT_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_PRODUCT_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_PRODUCT_SIZE')); ?>');
			$('#Product_Size').css('border', '1px solid red'); 
			
			return false;
		}else{
			$('#Product_Size').css('border', '');
			$('#size_error_msg').html('');
		}
	}
	/*Product Color*/
		if($('input:radio[name=productcolor]:checked').val()==0){
			if($("#selectprocolor option:selected").val()<1){
 				$('#color_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_COLOR') :  trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_COLOR')); ?>');
				$("#selectprocolor").css('border', '1px solid red'); 
				return false;
			}else{ 
				$("#selectprocolor").css('border', ''); 
				$('#color_error_msg').html('');
			}
		}
		
	/*Delivery Days*/
		if($.trim(delivery_days.val()) == ""){
			delivery_days.css('border', '1px solid red'); 
			$('#delivery_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DELIVERY_DAYS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DELIVERY_DAYS') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DELIVERY_DAYS')); ?>');
			delivery_days.focus();
			return false;
		}else{
			delivery_days.css('border', ''); 
			$('#delivery_error_msg').html('');
		}	 
	/*Store*/	 
		if(shop.val() == 0){
			shop.css('border', '1px solid red'); 
			$('#store_error_msg').html(' <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SHOP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SHOP') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SHOP')); ?>');
			shop.focus();
			return false;
		}else{
			shop.css('border', ''); 
			$('#store_error_msg').html('');
		}

		/*PRoduct title check*/
		if(($('#exist').val())==""){
			$('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE')); ?>');
			title.css('border', '1px solid red'); 
			title.focus();
			return false;
		}else if(($('#exist').val())==1){
			$('#title_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE')); ?>');
			title.css('border', '1px solid red'); 
			title.focus();
			return false;
		}else{
			title.css('border', ''); 
			$('#title_error_msg').html('');
		}

	/*Meta Keyword
		if($.trim(metakeyword.val()) == ""){
			metakeyword.css('border', '1px solid red'); 
			$('#meta_key_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METAKEYWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_METAKEYWORD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_METAKEYWORD');}*/ ?>');
			metakeyword.focus();
			return false;
		}else{
			metakeyword.css('border', ''); 
			$('#meta_key_error_msg').html('');
		}*/

	/*Meta description
		if($.trim(metadescription.val()) == ""){
			metadescription.css('border', '1px solid red'); 
			$('#meta_desc_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_META_DESCRIPTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_META_DESCRIPTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_META_DESCRIPTION');}*/ ?>');
			metadescription.focus();
			return false;
		}else{
			metadescription.css('border', ''); 
			$('#meta_desc_error_msg').html('');
		}*/

   /*Product Image*/
		var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == ""){
 			file.focus();
			file.css('border', '1px solid red'); 
			$('#img_error_msg').html('Please choose image');
			return false;
		}else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
			file.focus();
			file.css('border', '1px solid red'); 
			$('#img_error_msg').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_CHOOSE_VALID_IMAGE')); ?>');
			return false;
		}else{
			file.css('border', ''); 
			$('#img_error_msg').html('');				
		}
		

	});	
	
});
	/*multiselect*/
	$(function() {
    	$('#Product_Size').multiselect({
        includeSelectAllOption: true
    	});
	});
</script>
	<script src="<?php echo e(url('')); ?>/public/assets/js/multi_select_dropdown.js"></script>
  	<script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <?php /*END GLOBAL SCRIPTS
	
<script src="{{ url('') }}/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="{{ url('') }}/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="{{ url('') }}/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="{{ url('') }}/public/assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="{{ url('') }}/public/assets/js/formsInit.js"></script>
<script>
   //  $(function () { formInit(); });
</script>*/?>

         <!-- PAGE LEVEL SCRIPTS -->
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/editorInit.js"></script>
    <script>
        //$(function () { formWysiwyg(); });
		$(document).ready(function () {
			$('.wysihtml5').wysihtml5();
		});
	</script>


<script type="text/javascript">
function setPolicyDisplay(id, displayOption) 
{
    $("#"+id).css('display',displayOption);
} 
</script>



	<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

<script>
	$('#replace_days').keydown(function (e) 
	{
		 if (e.shiftKey || e.ctrlKey || e.altKey)
		 {
			e.preventDefault();
		 } 
		 else 
		 {
			var key = e.keyCode;
			if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
			 {
				e.preventDefault();
			 }
		}
	});
	
	$('#return_days').keydown(function (e) 
	{
		 if (e.shiftKey || e.ctrlKey || e.altKey)
		 {
			e.preventDefault();
		 } 
		 else 
		 {
			var key = e.keyCode;
			if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
			 {
				e.preventDefault();
			 }
		}
	});
	
	$('#cancellation_days').keydown(function (e) 
	{
		 if (e.shiftKey || e.ctrlKey || e.altKey)
		 {
			e.preventDefault();
		 } 
		 else 
		 {
			var key = e.keyCode;
			if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
			 {
				e.preventDefault();
			 }
		}
	});
	
	
	</script>

</body>
     <!-- END BODY -->
</html>
