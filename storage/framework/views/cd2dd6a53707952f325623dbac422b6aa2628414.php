<!DOCTYPE html>

<html lang="en">


<head>

 <meta charset="utf-8">  
	  <?php if(count($get_store_merchant_by_id) >0): ?> 

      <?php $__currentLoopData = $get_store_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_store_details_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

      <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

      <?php    $stor_name            = 'stor_name';

      $stor_metakeywords    = 'stor_metakeywords';

      $stor_metadesc        = 'stor_metadesc'; ?>

      <?php else: ?>   

      <?php  $stor_name            = 'stor_name_'.Session::get('lang_code');

      $stor_metakeywords    = 'stor_metakeywords_'.Session::get('lang_code');

      $stor_metadesc        = 'stor_metadesc_'.Session::get('lang_code'); ?>

      <?php endif; ?>

      <?php  $metaname     = $get_store_details_id->$stor_name;

      $metakeywords = $get_store_details_id->$stor_metakeywords;

      $metadesc     = $get_store_details_id->$stor_metadesc;    ?>  

      <?php else: ?>

      <?php if($metadetails): ?>

      <?php $__currentLoopData = $metadetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $metainfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

      <?php  $metaname   = $metainfo->gs_metatitle;

      $metakeywords = $metainfo->gs_metakeywords;

      $metadesc     = $metainfo->gs_metadesc; ?>

      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      <?php else: ?>

      <?php

      $metaname="";

      $metakeywords="";

      $metadesc=""; ?>

      <?php endif; ?>

      <?php endif; ?>

 

	  <title><?php echo e($metaname); ?></title>

      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <meta name="description" content="<?php echo e($metadesc); ?>">

      <meta name="keywords" content="<?php echo e($metakeywords); ?>">

      <meta name="author" content="<?php echo e($metaname); ?>">

 

</head>



<body class="about_us_page">



<?php echo $navbar; ?>


<!-- Navbar ================================================== -->

<?php echo $header; ?>


		 

  <!-- Main Container -->

      <?php if($get_store_merchant_by_id): ?>  

      <?php $__currentLoopData = $get_store_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_store_details_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

      <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

      <?php  $stor_name = 'stor_name'; ?>

      <?php else: ?> <?php  $stor_name = 'stor_name_'.Session::get('lang_code'); ?> <?php endif; ?> 

      <?php $product_image     = $get_store_details_id->stor_img; 

      $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

      $img_data   = "public/assets/storeimage/".$product_image;?>

      <?php if(file_exists($img_data) && $product_image !=''): ?>    

      <?php $prod_path = url('').'/public/assets/storeimage/' .$product_image;  ?>                

      <?php else: ?>  

      <?php if(isset($DynamicNoImage['store'])): ?>

      <?php  $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; ?>

      <?php if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path)): ?>  

      <?php    $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

      <?php endif; ?>

      <?php endif; ?> 

      <?php $alt_text   = $get_store_details_id->$stor_name; ?> 

  <div class="main container">

  

			<center>

               <?php if(Session::has('success_store')): ?>

               <div class="alert alert-warning alert-dismissable"><?php echo Session::get('success_store'); ?>


                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

               </div>

               <?php endif; ?>

            </center>

 

     <div class="about-page">

	   <div class="row">

	   <div class="col-xs-12 col-sm-6">

          <div class="single-img-add sidebar-add-slider">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 

              <!-- Wrapper for slides -->

              <div class="carousel-inner" role="listbox">

                <div class="item active"> <img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"> </div>  

              </div>

            </div>

          </div>

        </div>

		

        <div class="col-xs-12 col-sm-6"> 

		

			   <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?> 

               <?php $stor_name = 'stor_name';

               $stor_address1 = 'stor_address1';

               $stor_address2 = 'stor_address2'; ?>

               <?php else: ?>   

               <?php $stor_name = 'stor_name_'.Session::get('lang_code'); 

               $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 

               $stor_address2 = 'stor_address2_'.Session::get('lang_code');  ?>

               <?php endif; ?>

			   

          <h1><span class="text_color"><?php echo e($get_store_details_id->$stor_name); ?></span></h1>

		  

			   <?php

               $product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

               $product_count;

               $multiple_countone = $one_count *1;

               $multiple_counttwo = $two_count *2;

               $multiple_countthree = $three_count *3;

               $multiple_countfour = $four_count *4;

               $multiple_countfive = $five_count *5;

               $product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive; ?>

               <?php if($product_count): ?>

               <?php $product_divide_count = $product_total_count / $product_count; 

			   $product_divide_count = round($product_divide_count);?>

               <?php if(($product_divide_count != '0')&&($product_divide_count <= 1)): ?>

				   

		  <div class="rating"> 

			   <?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?> <img src='<?php echo e(url('images/stars-1.png')); ?>' style='margin-bottom:10px;'>

               <?php elseif($product_divide_count <= '2'): ?>

               <?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?> : <img src='<?php echo e(url('./images/stars-2.png')); ?>' style='margin-bottom:10px;'>

               <?php elseif($product_divide_count <= 3): ?>

               <?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?> : <img src='<?php echo e(url('./images/stars-3.png')); ?>' style='margin-bottom:10px;'>

               <?php elseif($product_divide_count <= '4'): ?>

               <?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?> : <img src='<?php echo e(url('./images/stars-4.png')); ?>' style='margin-bottom:10px;'>

               <?php elseif($product_divide_count <= '5'): ?>

               <?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?> : <img src='<?php echo e(url('./images/stars-5.png')); ?>' style='margin-bottom:10px;'>

               <?php else: ?> 

               <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_STORE')); ?> <?php endif; ?>

               <?php endif; ?>

               <?php elseif($product_count): ?>

               <?php $product_divide_count = $product_total_count / $product_count; ?>

               <?php else: ?>  

               <?php if(Lang::has(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RATING_FOR_THIS_STORE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RATING_FOR_THIS_STORE')); ?> <?php endif; ?>

		  

		  &nbsp; <span>( <?php if(isset($count_review_rating)): ?> <?php echo e($count_review_rating); ?> <?php endif; ?> <?php if(Lang::has(Session::get('lang_file').'.REVIEWS_AND_RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEWS_AND_RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEWS_AND_RATINGS')); ?> <?php endif; ?> )</span>

		   <?php endif; ?> 

		  

          <ul>

            <li><?php if(Lang::has(Session::get('lang_file').'.ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS1')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS1')); ?> <?php endif; ?></span> &nbsp;:&nbsp; <a><?php echo e($get_store_details_id->$stor_address1); ?>.</a></li>

			

			 <li><?php if(Lang::has(Session::get('lang_file').'.ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS2')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS2')); ?> <?php endif; ?></span> &nbsp;:&nbsp; <a><?php echo e($get_store_details_id->$stor_address2); ?>.</a></li>

			 

			 <li><?php if(Lang::has(Session::get('lang_file').'.ZIP_CODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ZIP_CODE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ZIP_CODE')); ?> <?php endif; ?></span> &nbsp;:&nbsp; <a><?php echo e($get_store_details_id->stor_zipcode); ?>.</a></li>

			  

			 <li><?php if(Lang::has(Session::get('lang_file').'.MOBILE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MOBILE_NO')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MOBILE_NO')); ?> <?php endif; ?></span> &nbsp;:&nbsp; <a><?php echo e($get_store_details_id->stor_phone); ?>.</a></li>

			  

			 <li><?php if(Lang::has(Session::get('lang_file').'.WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WEBSITE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WEBSITE')); ?> <?php endif; ?></span> &nbsp;:&nbsp; <a><?php echo e($get_store_details_id->stor_website); ?>.</a></li>

			   

			 <li><div id="somecomponent" style="width: 100%; height: 300px;" class="store-map"></div></li>

			 

          </ul>

		  </div>

        </div>

       

      </div>

    </div>

  </div>

  

  

  <!---store products-->

  

  <div class="inner-box">

    <div class="container">

      <div class="row store_branch"> 

       

        <!-- Best Sale -->

        <div class="col-sm-12 col-md-12 jtv-best-sale special-pro">

          <div class="jtv-best-sale-list">

            <div class="wpb_wrapper">

              <div class="best-title text-left">

                 <h3><?php echo ($get_store_details_id->$stor_name);  ?> - <span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCTS')); ?> <?php endif; ?></span></h3>

              </div>

            </div>

           

            <div class="slider-items-products">

              <div id="jtv-best-sale-slider" class="product-flexslider">

                <div class="slider-items">

				

				 <?php if(count($get_store_product_by_id) > 0 ): ?>  

				 <?php $__currentLoopData = $get_store_product_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_most_visit_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

				 

				 

				 <?php $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;

				 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);

				 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);

				 $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name)); 

				 $res = base64_encode($fetch_most_visit_pro->pro_id); ?>

				 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				 <?php $pro_title = 'pro_title'; ?>

				 <?php else: ?> <?php  $pro_title = 'pro_title_'.Session::get('lang_code'); ?> <?php endif; ?>

				 <?php   $product_img    = $mostproduct_img[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/product/".$product_img; ?>

				 <?php if(file_exists($img_data) && $product_img !=''): ?>   

				 <?php  $prod_path = url('').'/public/assets/product/' .$product_img;  ?>                 

				 <?php else: ?>  

				 <?php if(isset($DynamicNoImage['productImg'])): ?>

				 <?php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['productImg']; ?>

				 <?php if($DynamicNoImage['productImg']!='' && file_exists($dyanamicNoImg_path)): ?>

				 <?php    $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

				 <?php endif; ?>

				 <?php endif; ?>   

				 <?php   $alt_text   = substr($fetch_most_visit_pro->$pro_title,0,25); ?>

		 

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">  

                        <div class="pr-img-area">

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						<a  href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> 

						  <img class="first-img" alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						

						<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						<a  href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">

                          <figure> 

						  <img class="first-img" alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

						

						<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						<a  href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>">

                          <figure> 

						  <img class="first-img" alt="<?php echo e($alt_text); ?>" src="<?php echo e($prod_path); ?>"> 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                        </a> 

						<?php endif; ?>

									 

						</div>

                        <!--<div class="pr-info-area">

                          <div class="pr-button">

                            <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>

                            <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>

                            <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>

                          </div>

                        </div>-->

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="" href="">  <?php echo e(substr($fetch_most_visit_pro->$pro_title,0,20)); ?>... </a> 



                            </div>

                          <div class="item-content">

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($fetch_most_visit_pro->pro_disprice); ?></span> </span> </div>

                            </div>

						

                            <div class="pro-action">

                              <?php if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty): ?> 

							  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>  

                             <a  href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>" ><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							  <?php endif; ?>

							  

							  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>

                              <a  href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>" ><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							  <?php endif; ?>

							  

							  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>

                              <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							  <?php endif; ?>

							  <?php else: ?>

                <a href="<?php echo url('productview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?></span> </button></a>

                <?php endif; ?>



                            </div>

							

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                  

				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			<?php else: ?>  

				<h5 class="no_record_fis" ><?php if(Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER')); ?> <?php endif; ?> <?php echo ($get_store_details_id->$stor_name);  ?> <?php if(Lang::has(Session::get('lang_file').'.PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCTS')); ?> <?php endif; ?>.</h5>

			<?php endif; ?>

                </div>

              </div>

            </div>

          </div>

        </div>

		

		<!--Deal-->

		 <!-- Best Sale -->

        <div class="col-sm-12 col-md-12 jtv-best-sale special-pro">

          <div class="jtv-best-sale-list">

            <div class="wpb_wrapper">

              <div class="best-title text-left">

                 <h2><?php echo ($get_store_details_id->$stor_name);  ?> - <span class="text_color"><?php if(Lang::has(Session::get('lang_file').'.DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DEALS')); ?> <?php endif; ?></span></h2>

              </div>

            </div>

            <div class="slider-items-products">

              <div id="jtv-best-sale-slider" class="product-flexslider">

                <div class="slider-items">

				

				 <?php if(count($get_store_deal_by_id) > 0): ?>  

				 <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				 <?php $deal_title = 'deal_title'; ?>

				 <?php else: ?> <?php  $deal_title = 'deal_title_'.Session::get('lang_code');  ?> <?php endif; ?>



				 <?php $__currentLoopData = $get_store_deal_by_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store_deal_by_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

				  <?php if($store_deal_by_id->deal_no_of_purchase < $store_deal_by_id->deal_max_limit): ?>

				 <?php $dealdiscount_percentage = $store_deal_by_id->deal_discount_percentage;

				 $deal_img= explode('/**/', $store_deal_by_id->deal_image);

				 $mcat = strtolower(str_replace(' ','-',$store_deal_by_id->mc_name));

				 $smcat = strtolower(str_replace(' ','-',$store_deal_by_id->smc_name));

				 $sbcat = strtolower(str_replace(' ','-',$store_deal_by_id->sb_name));

				 $ssbcat = strtolower(str_replace(' ','-',$store_deal_by_id->ssb_name)); 

				 $res = base64_encode($store_deal_by_id->deal_id);

				 $product_img     = $deal_img[0];

				 $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

				 $img_data   = "public/assets/deals/".$product_img; ?>

				 <?php if(file_exists($img_data) && $product_img !=''): ?>   

				 <?php   $prod_path = url('').'/public/assets/deals/' .$product_img;  ?>                 

				 <?php else: ?>  

				 <?php if(isset($DynamicNoImage['dealImg'])): ?>

				 <?php   $dyanamicNoImg_path = url('').'/public/assets/noimage/' .$DynamicNoImage['dealImg']; ?>

				 <?php if($DynamicNoImage['dealImg']!='' && file_exists($dyanamicNoImg_path)): ?>

				 <?php  $prod_path = $dyanamicNoImg_path; ?> <?php endif; ?>

				 <?php endif; ?>

				 <?php endif; ?>   

				 <?php   $alt_text   = substr($store_deal_by_id->$deal_title,0,25);

				 $alt_text  .= strlen($store_deal_by_id->$deal_title)>25?'..':''; ?>

                  <div class="product-item">

                    <div class="item-inner">

                      <div class="product-thumbnail">

                      

                        <div class="pr-img-area"> 

						

						  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

						  <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>" > 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                          </a> 

						  <?php endif; ?>

						  

						 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

						  <a  href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>">

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>" > 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                          </a> 

						  <?php endif; ?>

						  

						  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

						  <a href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>" >

                          <figure> <img class="first-img" src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>" > 

						  <!--<img class="hover-img" src="images/products/product-2.jpg" alt="HTML template">--></figure>

                          </a> 

						   <?php endif; ?>

						   

						   

						</div>

                      </div>

                      <div class="item-info">

                        <div class="info-inner">

                          <div class="item-title"> <a title="" href=""><?php echo e(substr($store_deal_by_id->$deal_title,0,20)); ?>... </a> </div>

                          <div class="item-content">

                            <div class="item-price">

                              <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo e(Helper::cur_sym()); ?> <?php echo e($store_deal_by_id->deal_discount_price); ?></span> </span> </div>

                            </div>

                            <div class="pro-action">

							 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?> 

                              <a  href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							 <?php endif; ?>

							 

							 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?> 

                              <a  href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							 <?php endif; ?>



							  <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?> 

                              <a   href="<?php echo url('dealview').'/'.$mcat.'/'.$smcat.'/'.$res; ?>"><button type="button" class="add-to-cart"><span><?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?></span> </button></a>

							 <?php endif; ?>

							 

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

				   <?php else: ?> 

				  <?php endif; ?>

         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	  <?php else: ?>  

         <h5 class="no_record_fis" ><?php if(Lang::has(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORDS_FOUND_UNDER')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORDS_FOUND_UNDER')); ?> <?php endif; ?> <?php echo ($get_store_details_id->$stor_name);  ?> <?php if(Lang::has(Session::get('lang_file').'.DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DEALS')); ?> <?php endif; ?>.</h5>

         <?php endif; ?>

                </div>

              </div>

            </div>

          </div>

        </div>

		

	<!--deal end-->

      </div>

    </div>

  </div>

  

  <!--end store products-->

  <!--store branched-->

   <section class="blog_post">

    <div class="container">

      <div class="row">

        <div class="col-xs-12 col-sm-12">

		

		<div class="single-box">

          <div class="best-title text-left">

            <h3><?php if(Lang::has(Session::get('lang_file').'.BRANCHES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BRANCHES')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.BRANCHES')); ?> <?php endif; ?></h3></div>

            <div class="slider-items-products">

              <div id="related-posts" class="product-flexslider hidden-buttons">

                <div class="slider-items slider-width-col4 fadeInUp">

					 <?php $store_count=count($get_storebranch);

                     $i=0; ?>

                     <?php $__currentLoopData = $get_storebranch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                     <?php  $i++; ?>

                     <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

                     <?php $stor_name = 'stor_name';

                     $stor_address1 = 'stor_address1';

                     $stor_address2 = 'stor_address2'; ?>

                     <?php else: ?>   

                     <?php $stor_name = 'stor_name_'.Session::get('lang_code'); 

                     $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 

                     $stor_address2 = 'stor_address2_'.Session::get('lang_code');  ?>

                     <?php endif; ?>

                     <?php $store_img = $row_store->stor_img;

                     $store_name = $row_store->$stor_name; 

                     $product_image     = $row_store->stor_img;

                     $prod_path  = url('').'/public/assets/default_image/No_image_product.png';

                     $img_data   = "public/assets/storeimage/".$product_image; ?>

                     <?php if(file_exists($img_data) && $product_image !=''): ?>  

                     <?php  $prod_path = url('').'/public/assets/storeimage/' .$product_image;  ?>                

                     <?php else: ?>  

                     <?php if(isset($DynamicNoImage['store'])): ?>

                     <?php $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; ?>

                     <?php if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path)): ?>  

                     <?php   $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

                     <?php endif; ?>

                     <?php endif; ?> 

                     <?php  $alt_text   = $row_store->$stor_name; ?>

				

                  <div class="product-item">

                    <article class="entry">

                      <div class="entry-thumb image-hover2">  <img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>">  </div>

                      <div class="entry-info">

                        <h3 class="entry-title"><a><?php echo e($store_name); ?></a></h3>

                       

                        <div class="entry-more"> <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($row_store->stor_id)))); ?>"><?php if(Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VIEW_DETAILS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?> <?php endif; ?></a> </div>

						

                      </div>

                    </article>

                  </div>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   

                </div>

              </div>

            </div>

          </div>

		  

		  <!---Leave a comment-->

		  <?php if(Session::has('customerid')): ?>

		  <div class="single-box comment-box">

          <div class="best-title text-left">

            <h2><?php if(Lang::has(Session::get('lang_file').'.WRITE_A_POST_COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WRITE_A_POST_COMMENTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WRITE_A_POST_COMMENTS')); ?> <?php endif; ?></h2></div>

			 <?php echo Form::open(array('url'=>'storecomments','class'=>'form-horizontal loginFrm')); ?>


            <div class="coment-form">

             

              <div class="row">

                <input type="hidden" name="customer_id" value="<?php echo e(Session::get('customerid')); ?>">

                <input type="hidden" name="store_id" value="<?php echo $get_store_details_id->stor_id; ?>">

				

                <div class="col-sm-12">

                  <label for="website"><?php if(Lang::has(Session::get('lang_file').'.ENTER_COMMENT_TITLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_COMMENT_TITLE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_COMMENT_TITLE')); ?> <?php endif; ?></label>

                  <input id="title" name="title" type="text" class="form-control" value="<?php echo e(Input::old('title')); ?>" required>

                </div>

                <div class="col-sm-12">

                  <label for="message"><?php if(Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES')); ?> <?php endif; ?></label>

                  <textarea name="comments" id="comments" rows="8" required class="form-control" value="<?php echo e(Input::old('comments')); ?>"></textarea>

                </div>

				 <div class="col-sm-12">

				  <fieldset>

                     <span class="star-cb-group">

                     <input type="radio" id="rating-5"  name="ratings" value="5"/><label for="rating-5"></label>

                     <input type="radio" id="rating-4" name="ratings" value="4"/><label for="rating-4"></label>

                     <input type="radio" id="rating-3"  name="ratings" value="3"/><label for="rating-3"></label>

                     <input type="radio" id="rating-2"  name="ratings" value="2"/><label for="rating-2"></label>

                     <input type="radio" id="rating-1"  name="ratings" value="1"/><label for="rating-1"></label>

                     </span>

                  </fieldset>

				 

				 </div>

              </div>

              <button type="submit" class="button"><span><?php if(Lang::has(Session::get('lang_file').'.POST_COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.POST_COMMENTS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.POST_COMMENTS')); ?> <?php endif; ?></span></button>

            </div>

			</form>

          </div>

		  <?php else: ?>   

		<div class="special-product">

		  <a href="" class="link-all" value="<?php if(Lang::has(Session::get('lang_file').'.LOGIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOGIN')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOGIN')); ?> <?php endif; ?>" role="button" data-toggle="modal" data-target="#loginpop"><?php if(Lang::has(Session::get('lang_file').'.WRITE_A_REVIEW_POST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WRITE_A_REVIEW_POST')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WRITE_A_REVIEW_POST')); ?> <?php endif; ?></a></div>

        <?php endif; ?>

		   <!---Leave a comment end--->

		   <!-- Comment -->

		  <br>

          <div class="single-box ">

          <div class="best-title text-left">

            <h2><?php if(Lang::has(Session::get('lang_file').'.REVIEWS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.REVIEWS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.REVIEWS')); ?> <?php endif; ?></h2></div>

            <div class="comment-list str-review">

			  <?php if(count($review_details)!=0): ?>

              <ul>

		     <?php $__currentLoopData = $review_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $col): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

			  <?php  $customer_name = $col->cus_name;

			  $customer_mail = $col->cus_email;

			  $customer_img = $col->cus_pic;

			  $customer_comments = $col->comments;

			  $customer_date = $col->review_date;

			  $customer_product = $col->store_id;

			  $change_format = date('d/m/Y', strtotime($col->review_date) );

			  $review_date = date('F j, Y', strtotime($col->review_date) );

			  $customer_title = $col->title;

			  $customer_name_arr = str_split($customer_name);

			  $start_letter = strtolower($customer_name_arr[0]);

			  $customer_ratings = $col->ratings;

			  $date_exp=explode('/',$change_format); ?>

                <li>

                  <div class="avartar"> 

				  

			<?php if($start_letter =='a'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='b'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='c'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='d'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='e'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='f'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='g'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='h'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='i'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='j'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='k'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='m'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='n'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='o'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='p'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='q'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='r'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='s'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='t'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='u'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			  <?php elseif($start_letter=='v'): ?>

			  <?php echo "

			  <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			<?php elseif($start_letter=='w'): ?>

				 <?php echo "

			  <div class='userimg'>

			 <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			<?php elseif($start_letter=='x'): ?>

					<?php echo "

			 <div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			<?php elseif($start_letter=='y'): ?>

					   <?php echo "

			<div class='userimg'>

			  <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			 <?php elseif($start_letter=='z'): ?>

						  <?php echo "

			<div class='userimg'>

			 <span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</span>"; ?>


			<?php endif; ?>

			</div>		

				

				  </div>

                  <div class="comment-body">

                    <div class="comment-meta"> <span class="author"><p><?php echo e($customer_name); ?></p></span> <span class="date"><?php echo e($review_date); ?></span> </div>

					   <?php if($customer_ratings=='1'): ?>

                        <img src='<?php echo e(url('./images/stars-1.png')); ?>' style='margin-bottom:10px;'><?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?>

                        <?php elseif($customer_ratings=='2'): ?>

                        <img src='<?php echo e(url('./images/stars-2.png')); ?>' style='margin-bottom:10px;'><?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?>

                        <?php elseif($customer_ratings=='3'): ?>

                        <img src='<?php echo e(url('./images/stars-3.png')); ?>' style='margin-bottom:10px;'><?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?>

                        <?php elseif($customer_ratings=='4'): ?>

                        <img src='<?php echo e(url('./images/stars-4.png')); ?>' style='margin-bottom:10px;'><?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?>

                        <?php elseif($customer_ratings=='5'): ?>

                        <img src='<?php echo e(url('./images/stars-5.png')); ?>' style='margin-bottom:10px;'><?php if(Lang::has(Session::get('lang_file').'.RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RATINGS')); ?> <?php endif; ?>

                        <?php endif; ?>

                    <div class="comment-title"><?php echo e($customer_title); ?> </div>

                    <div  class="comment"><?php echo e($customer_comments); ?> </div>

                  </div>

                </li>

				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </ul>

			  <?php else: ?> <?php if(Lang::has(Session::get('lang_file').'.NO_REVIEW_RATINGS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_REVIEW_RATINGS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_REVIEW_RATINGS')); ?> <?php endif; ?>.<br>

            <?php endif; ?>

            </div>

          </div>

		  <!--end comment-->

		  

		</div>

       </div>

     </div>

  

  <!--end store branches-->

  

 

  

   <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  

   <?php else: ?> 

      <h4 style="color:#f00;">

         <center>Seems the store you are looking is not active, please contact us for more details.</center>

      </h4>

   <?php endif; ?>

  <?php echo $footer; ?>


  

	<?php 

	 if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 

	 $map_lang = 'en';

	 }else {  

	 $map_lang = 'fr';

	 }

	 ?>

  <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>&language=<?php echo $map_lang; ?>'></script>

      <script src="<?php echo url(''); ?>/public/assets/js/locationpicker.jquery.js"></script>

      <script>

         $('#somecomponent').locationpicker({

         location: {latitude: <?php echo  $get_store_details_id->stor_latitude;?>, longitude: <?php echo  $get_store_details_id->stor_longitude;?>},

         radius: 300,

         zoom: 12,

         });

  </script>

 <script type="text/javascript">

         var logID = 'log',

          log = $('<div id="'+logID+'"></div>');

         $('body').append(log);

          $('[type*="radio"]').change(function () {

            var me = $(this);

            log.html(me.attr('value'));

          });

 </script>