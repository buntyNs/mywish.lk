<?php echo $navbar; ?>




<!-- Navbar ================================================== -->

<?php echo $header; ?>




<div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="" href="<?php echo e(url('index')); ?>"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a><span>&raquo;</span></li>

            <li class=""><a title="Go to Home Page" href="<?php echo e(url('stores')); ?>"><strong> <?php if(Lang::has(Session::get('lang_file').'.STORES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORES')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORES')); ?> <?php endif; ?></strong></a></li>

      

          </ul>

        </div>

      </div>

    </div>

</div>



	<center> <?php if(Session::has('success_store')): ?>

	<div class="alert alert-warning alert-dismissable"><?php echo Session::get('success_store'); ?>


	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>

	<?php endif; ?></center>

  

  <section class="blog_post">

    <div class="container"> 

      

      <!-- Center colunm-->

      <div class="blog-wrapper store-blg-wrpr">

        <div class="page-title">

          <h2><?php if(Lang::has(Session::get('lang_file').'.STORES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORES')); ?>  <?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.STORES')); ?> <?php endif; ?></h2>

        </div>

		

		<?php

		$get_store_count=DB::table('nm_store')

		->join('nm_merchant','nm_merchant.mer_id','=','nm_store.stor_merchant_id')

		->where('nm_merchant.mer_staus','=',1)

		->where('nm_store.stor_status', '=', 1)

		->groupby('stor_merchant_id')

		->orderby('nm_store.stor_id','desc')

		->get()

		->count();

	 

		$i=1;  ?>

	  

	   <?php if(count($get_store_details)>0): ?>

				   

        <ul class="blog-posts">

			 <?php $__currentLoopData = $get_store_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  

				<?php  $stor_name = 'stor_name'; ?>

				<?php else: ?> <?php  $stor_name = 'stor_name_langCode'; ?> <?php endif; ?> 



			    <?php $product_image     = $store->stor_img;

			   

				$prod_path  = url('').'/public/assets/default_image/No_image_store.png';

				$img_data   = "public/assets/storeimage/".$product_image; ?>

				

				<?php if(file_exists($img_data) && $product_image !=''): ?>   

			

			   <?php $prod_path = url('').'/public/assets/storeimage/' .$product_image;  ?>              

			   <?php else: ?>  

					

					<?php if(isset($DynamicNoImage['store'])): ?>

					  <?php  $dyanamicNoImg_path = 'public/assets/noimage/'.$DynamicNoImage['store']; ?>



						<?php if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path)): ?>

						   <?php $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>

					<?php endif; ?>

				<?php endif; ?>

				

			   <?php $alt_text   = $store->$stor_name;



			  $store_comment = DB::table('nm_review')->where('store_id', '=', $store->stor_id)->count();



			   ?>

			   

          <li class="post-item col-md-3 col-sm-6">

           <div class="blog-box"> <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($store->stor_id)))); ?>"> <img class="primary-img" src="<?php echo e($prod_path); ?>" alt='<?php echo e($alt_text); ?>'></a>

                <div class="blog-btm-desc">

                  <div class="blog-top-desc">

                    <div class=""></div>

                   <center> <h4> <?php echo e(substr($store->$stor_name,0,25)); ?>


					<?php echo e(strlen($store->$stor_name)>25?'..':''); ?></h4>

                    <div class="jtv-entry-meta">

					<!--<i class="fa fa-user-o"></i> <strong>Admin</strong> <a>--><i class="fa fa-commenting-o"></i> 

					<strong><?php echo e($store_comment); ?> <?php if(Lang::has(Session::get('lang_file').'.COMMENT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COMMENT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COMMENT')); ?> <?php endif; ?></strong></a></div></center>

                  </div>

				  <center style="margin-bottom: 20px;"> <table border="0" class="table table-hover">

				  <tr>

					<td><?php if(Lang::has(Session::get('lang_file').'.DEALS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DEALS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DEALS')); ?> <?php endif; ?></td>

					 <td>:</td>

					  <td><?php if($get_store_deal_count[$store->stor_id] != 0): ?> 

				 <?php  $sold_count=0;

				 $store->stor_id;

				 $date = date('Y-m-d H:i:s');

				$store_result = DB::table('nm_deals')->where('deal_status', '=', 1)->where('deal_shop_id','=',$store->stor_id)->where('deal_start_date','<',$date)->where('deal_end_date', '>', $date)->get(); ?>

			  

				<?php $__currentLoopData = $store_result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sold_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				  <?php if($sold_pro->deal_no_of_purchase < $sold_pro->deal_max_limit): ?> 



				  

					 <?php $sold_count++; ?>

				  <?php endif; ?>

				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					<?php if($sold_count!=0): ?><?php echo e($sold_count); ?> 

				

				

				<?php else: ?> <?php echo e('N/A'); ?> <?php endif; ?>

				 

			   <?php else: ?> <?php echo e('N/A'); ?> <?php endif; ?></td>

				  </tr>

				  <tr>

					<td><?php if(Lang::has(Session::get('lang_file').'.PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRODUCTS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PRODUCTS')); ?> <?php endif; ?></td>

					 <td>:</td>

					  <td><?php if($get_store_product_count[$store->stor_id] != 0): ?> 

			<?php  $sold_count=0;

			$store->stor_id;

        

			$store_result = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_sh_id', '=', $store->stor_id)->get(); ?>

        

			<?php $__currentLoopData = $store_result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sold_pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

			<?php if($sold_pro->pro_no_of_purchase < $sold_pro->pro_qty): ?> 



          

             <?php $sold_count++; ?>

          <?php endif; ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

       <!--  //echo $sold_count;

			// echo $get_store_product_count[$store->stor_id]; 

     

		//$get_store_product_by_id      = HomeController::get_store_product_by_id($store->stor_id);-->

        <?php if($sold_count!=0): ?><?php echo e($sold_count); ?> 

        <?php else: ?> <?php echo e('N/A'); ?> <?php endif; ?>     

		<?php else: ?> <?php echo e('N/A'); ?> <?php endif; ?></td>

				  </tr></table>

				  </center>

                  <a class="read-more str-det" href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($store->stor_id)))); ?>"> <?php if(Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VIEW_DETAILS')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?> <?php endif; ?></a> </div>

              </div>

          </li>

		   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  

          

        </ul><?php else: ?>

			<h5 style="color:#933;" ><?php if(Lang::has(Session::get('lang_file').'.NO_STORES_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_STORES_FOUND')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_STORES_FOUND')); ?> <?php endif; ?>.</h5>

         <?php endif; ?>

		

        <div class="sortPagiBar">

          <div class="pagination-area" >

            <ul>

              <?php echo $get_store_details->render(); ?>


            </ul>

          </div>

        </div>

      </div>

      <!-- ./ Center colunm --> 

      

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- service section -->

   <?php echo $__env->make('service_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

  <?php echo $footer; ?>


  

  <script type="text/javascript">



    $(document).ready(function() {

        $(document).on("click", ".customCategories .topfirst b", function() {

            $(this).next("ul").css("position", "relative");



            $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");

            $(this).next("ul").toggle();

        });



        $(document).on("click", ".morePage", function() {

            $(".nextPage").slideToggle(200);

        });



        $(document).on("click", "#smallScreen", function() {

            $(this).toggleClass("customMenu");

        });



        $(window).scroll(function() {

            if ($(this).scrollTop() > 250) {

                $('#comp_myprod').show();

            } else {

                $('#comp_myprod').hide();

            }

        });



    });

</script>

  <!-- For Responsive menu-->

</body>

</html>



<script>

var pagenumber=$("#page_number").val();

var i=0;

var store_count='<?php echo $get_store_count; ?>';

var pagination=Math.round(store_count/4);

$(window).scroll(function(){

  if($(window).scrollTop()+$(window).height() >= $(document).height())  

  {

      $(".myLoader").show();

  pagenumber++; 

  $("#page_number").val(pagenumber);



if(pagination<=pagenumber)

{

   $(".myLoader").hide();

  return false;

}

  $.ajax({

    url:'<?php echo url("stores_ajax"); ?>?page='+pagenumber,

    type:"get",

    success:function(data,status){

      if(data.trim()=='' && i==0){ 

      $('img#thumb').removeAttr('id');

      i++;

      $(".myLoader").hide();

    } else {

        $(".myLoader").hide();

        $('#storpg').append(data);

      }

      }

    });

  }

});

</script>