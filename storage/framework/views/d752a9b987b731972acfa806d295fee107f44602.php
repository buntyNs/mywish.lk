<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADMIN_ADD_MERCHANT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADMIN_ADD_MERCHANT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_ADMIN_ADD_MERCHANT_ACCOUNT')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         <?php echo $merchantheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      <?php echo $merchantleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="<?php echo e(url('sitemerchant_dashboard')); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>
                                <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_ACCOUNT')); ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_ACCOUNT')); ?></h5>
            
        </header>
        <?php if($errors->any()): ?>
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

			<?php echo e(Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ])); ?>

        
        </div>
		</ul>	
		<?php endif; ?>
         <?php if(Session::has('mail_exist')): ?>
		<div class="alert alert-warning alert-dismissable"><?php echo Session::get('mail_exist'); ?>

	<?php echo e(Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ])); ?>

        </div>
		<?php endif; ?>
<?php if(Session::has('result')): ?>
		<div class="alert alert-warning alert-dismissable"><?php echo Session::get('result'); ?>

	<?php echo e(Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ])); ?>

        </div>
		<?php endif; ?>
        
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    <?php $__currentLoopData = $merchant_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_mer_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php echo Form::open(array('url'=>'edit_merchant_account_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_ACCOUNT')  : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_ACCOUNT')); ?>

                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FIRST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::hidden('mer_id',$fetch_mer_details->mer_id)); ?>

					<?php echo e(Form::text('first_name',$fetch_mer_details->mer_fname,['class' => 'form-control','id' => 'first_name'])); ?>

                    	
                         
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_LAST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LAST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_LAST_NAME')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('last_name',$fetch_mer_details->mer_lname,['class' => 'form-control','id' => 'last_name'])); ?>

                        
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('email_id',$fetch_mer_details->mer_email,['class' => 'form-control','id' => 'email_id'])); ?>

                        
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY')!= '')   ? trans(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_COUNTRY')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4"> 
                       <select class="form-control" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" >
                        <option value="">-- <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> --</option>
                          <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          				 <option value="<?php echo e($country_fetch->co_id); ?>"  <?php if($fetch_mer_details->mer_co_id == $country_fetch->co_id): ?> <?php echo e('selected'); ?> <?php endif; ?> ><?php echo e($country_fetch->co_name); ?></option>
           				   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       					 </select>
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       <select class="form-control" name="select_mer_city" id="select_mer_city" >
           				<option value="">--<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> --</option>
                  </select>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PHONE') : trans($MER_OUR_LANGUAGE.'.MER_PHONE')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('phone_no',$fetch_mer_details->mer_phone,['class' => 'form-control','id' => 'phone_no'])); ?>

                        
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('addreess_one',$fetch_mer_details->mer_address1,['class' => 'form-control','id' => 'addreess_one'])); ?>

                        
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('address_two',$fetch_mer_details->mer_address2,['class' => 'form-control','id' => 'address_two'])); ?>

                        
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_ACCOUNT')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_PAYMENT_ACCOUNT')  : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_ACCOUNT')); ?><span class="text-sub"></span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('payment_account',$fetch_mer_details->mer_payment,['class' => 'form-control','id' => 'payment_account'])); ?>

                        
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COMMISSION'): trans($MER_OUR_LANGUAGE.'.MER_COMMISSION')); ?><span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					<?php echo e(Form::text('commission',$fetch_mer_details->mer_commission,['class' => 'form-control','id' => 'commission','readonly'])); ?>

                       
                    </div>
                    <div class="col-md-1" style="margin-top: 5px;"><span>%</span></div>
                </div>

                        </div>
                        	<div class="panel-body">
							<?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-3', 'for' => 'pass1'])); ?>

                   

                    <div class="col-lg-8">
                     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" ><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE')); ?></a></button>
                     <a href="<?php echo url('merchant_settings'); ?>" class="btn btn-info btn-sm btn-grad" style="color:#ffffff;"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK'): trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?></a>
                   
                    </div>
					  
                </div>
					<br>
                    </div>
                    
					<?php echo e(Form::close()); ?>

                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      <?php echo $merchantfooter; ?>

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
    
    <script>
	$( document ).ready(function() {
	
	$('#submit').click(function() {
    var file		 	 = $('#file');
	var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == "")
 		{
 		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else
		{
		file.css('border', ''); 				
		}
	});
	
	 var passData = 'city_id_ajax=<?php echo $fetch_mer_details->mer_ci_id; ?>&country_id_ajax=<?php echo $fetch_mer_details->mer_co_id; ?>' ;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_edit_merchant'); ?>',
				  success: function(responseText){  
				  //alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
			
	});
	</script>
     <script>
	

	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_merchant'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}
	</script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
    <script type="text/javascript">
    var map;

    function initialize() {


 		var myLatlng = new google.maps.LatLng(11.0195253,76.9195069);
        var mapOptions = {

           zoom: 10,
                center: myLatlng,
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	 		  var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				draggable:true,    
            });	
		google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longtitude').val(lng);
			});
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
 
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
 
            var place = autocomplete.getPlace();
	
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
				var myLatlng = place.geometry.location;	
				//alert(place.geometry.location);			
				var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				draggable:true,    
            });	
			google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longtitude').val(lng);
			});
            } else {
                map.setCenter(place.geometry.location);	
				
                map.setZoom(17);
            }
        });



    }


    google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>
