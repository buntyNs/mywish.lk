<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->



 <!-- BEGIN HEAD -->

<head>

    <meta charset="UTF-8" />

    <title><?php echo e($SITENAME); ?> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT'): trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')); ?> | <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_DEALS'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_DEALS')); ?></title>

     <meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>

     <!--[if IE]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <![endif]-->

    <!-- GLOBAL STYLES -->

    <!-- GLOBAL STYLES -->

   <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main-merchant.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />

<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
 <?php endif; ?>

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />

	 <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <!--END GLOBAL STYLES -->

       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

 <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

</head>

     <!-- END HEAD -->



     <!-- BEGIN BODY -->

<body class="padTop53 " >



    <!-- MAIN WRAPPER -->

    <div id="wrap">





        <!-- HEADER SECTION -->

         <?php echo $adminheader; ?>


        <!-- END HEADER SECTION -->

        <!-- MENU SECTION -->

       <?php echo $adminleftmenus; ?>


        <!--END MENU SECTION -->

		<div></div>



         <!--PAGE CONTENT -->

        <div id="content">

           

                <div class="inner">

                    <div class="row">

                    <div class="col-lg-12">

                        	<ul class="breadcrumb">

                            	<li class=""><a href="<?php echo url('sitemerchant_dashboard'); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')); ?></a></li>

                                <li class="active"><a href="#"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_DEALS'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_DEALS')); ?></a></li>

                            </ul>

                    </div>

                </div>

				

				<center>
<div class="cal-search-filter">
		 <?php echo e(Form::open(['action' => 'MerchantdealsController@manage_deals', 'method' => 'POST'])); ?>

		 

							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">

							 <div class="row">

							 <br>

							 

							 

							   <div class="col-sm-4">

							    <div class="item form-group">

							<div class="col-sm-6 date-top"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FROM_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FROM_DATE') : trans($MER_OUR_LANGUAGE.'.MER_FROM_DATE')); ?></div>

							<div class="col-sm-6 place-size">
								 <span class="icon-calendar cale-icon"></span>
								 <?php echo e(Form::text('from_date',$from_date,array('id'=>'datepicker-8','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly'))); ?>

							 

							 

							  </div>

							  </div>

							   </div>

							    <div class="col-sm-4">

							    <div class="item form-group">

							<div class="col-sm-6 date-top"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_TO_DATE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_TO_DATE')  : trans($MER_OUR_LANGUAGE.'.MER_TO_DATE')); ?></div>

							<div class="col-sm-6 place-size">
								 <span class="icon-calendar cale-icon"></span>
								 <?php echo e(Form::text('to_date',$to_date,array('id'=>'datepicker-9','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly'))); ?>

							
                            </div>

							  </div>

							   </div>

							   

							   <div class="form-group">

							   <div class="col-sm-2">

							 <input type="submit" name="submit" class="btn btn-block btn-success" value="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SEARCH') : trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?>" required>

							 </div>
                             <div class="col-sm-2">

							    <a href="<?php echo e(url('').'/mer_manage_deals'); ?>"><button type="button" name="reset" class="btn btn-block btn-info"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET')  : trans($MER_OUR_LANGUAGE.'.MER_RESET')); ?></button></a>
 
							 </div>

							</div>

							

							<?php echo e(Form::close()); ?>

							 	</div>
							 </center>

            <div class="row">

<div class="col-lg-12">

    <div class="box dark">

        <header>

            <div class="icons"><i class="icon-edit"></i></div>

           <h5><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_DEALS'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_DEALS')); ?></h5>

            

         </header>
 <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
         <?php echo e(Form::button('×',['class' => 'close closeAlert' , 'aria-hidden' => 'true'])); ?>

         </div>
           <?php if(Session::has('block_message')): ?>

		<div class="alert alert-success alert-dismissable"><?php echo Session::get('block_message'); ?>

		<?php echo e(Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true'])); ?>

        </div>

		<?php endif; ?>

        <div id="div-1" class="accordion-body collapse in body">
           <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">

		   	<div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div>

        
		     <div class="table-responsive panel_marg_clr ppd">
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">

                                    <thead>

                                        <tr role="row">

										<th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO')); ?></th>

										<th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEALS_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEALS_NAME')  : trans($MER_OUR_LANGUAGE.'.MER_DEALS_NAME')); ?></th>

										

										<th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_NAME')  :  trans($MER_OUR_LANGUAGE.'.MER_STORE_NAME')); ?></th>

							<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE')); ?>(<?php echo e(Helper::cur_sym()); ?>)</th>

										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_DISCOUNTED_PRICE')); ?>(<?php echo e(Helper::cur_sym()); ?>)</th>

										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_DEAL_IMAGE')); ?></th>

										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ACTIONS')  : trans($MER_OUR_LANGUAGE.'.MER_ACTIONS')); ?></th>

										<!-- <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Preview</th> -->

										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_DEAL_DETAILS')); ?></th>



										</tr>

                                    </thead>

                                    <tbody>

                                      <?php $i = 1 ; ?>

									  

<?php if(isset($_POST['submit'])): ?>

			
<?php if(count($merchant_dealrep)>0): ?> 
				<?php $__currentLoopData = $merchant_dealrep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
									<?php
									  $deal_get_img = explode("/**/",$deal_list->deal_image);
										?>
								  <?php if($deal_list->deal_status == 1): ?>
										<?php 
										  $process = "<a href='".url('mer_block_deals/'.$deal_list->deal_id.'/0')."' data-tooltip='click to Block'> <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>"; ?>

								 <?php elseif($deal_list->deal_status == 0): ?> 
										<?php 
										   $process = "<a href='".url('mer_block_deals/'.$deal_list->deal_id.'/1')."' data-tooltip='Click to Unblock'> <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>"; ?>
								 <?php endif; ?>

                                    <tr class="gradeA odd">

                                            <td class="sorting_1"><?php echo e($i); ?></td>

                                            <td class="center  "><?php echo e(substr($deal_list->deal_title,0,45)); ?></td>

                                            

				<td class="center  "><?php echo e($deal_list->stor_name); ?></td>

                                            <td class="center  "><?php echo e($deal_list->deal_original_price); ?></td>

                                            <td class="center  "><?php echo e($deal_list->deal_discount_price); ?></td>

                                            <td class="center  ">
											
											<?php $pro_img = $deal_get_img[0];
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
							
							  <?php if($deal_get_img !=''): ?> 
								
					  
								
						   <?php if($deal_get_img[0] !=''): ?>  
						    
							<?php  
							   $img_data = "public/assets/deals/".$pro_img;  ?>
							    <?php if(file_exists($img_data)): ?>  
									 <?php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img;
								     ?>
								<?php else: ?>
									<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 					
											<?php
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												 <?php
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
									     <?php endif; ?>
										 
										 
                                <?php endif; ?>
					       
					      
						   <?php else: ?>
						   <?php
							    $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
								<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 	<?php					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												<?php 
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
										
									     <?php endif; ?>
										 
					<?php endif; ?>
						    
					  
			      
				  <?php else: ?>
				  
					 <?php  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; ?>
					   <?php if(isset($DynamicNoImage['dealImg'])): ?>
										 				
							<?php				
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
											
											  <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
											
												<?php
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											    ?>
											<?php endif; ?>
											
									     <?php endif; ?>

									 
								 
					  
					
				 <?php endif; ?>		
											
											<a href="#"><img style="height:40px;" src="<?php echo e($prod_path); ?>"></a></td>

                                           <?php 
											$starttime = strtotime($deal_list->deal_start_date); 
											$status = 0;
											$endtime = strtotime($deal_list->deal_end_date);
											$time_val = time(); ?>
											<?php if(($time_val) < $endtime): ?>
													
														<?php if(($time_val) > $starttime): ?>
														<?php 
															$status = 1;
															//$OpenClose=((Lang::has(Session::get('admin_lang_file').'.BACK_OPEN')!= ''))? trans(Session::get('admin_lang_file').'.BACK_OPEN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_OPEN');
															 ?>
														
														<?php else: ?>
														
													<?php 	//$OpenClose=((Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CLOSE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE');
															?>
														<?php endif; ?>
													
														<?php else: ?>
														
															<?php
															//$OpenClose=((Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CLOSE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE');
															?>
														<?php endif; ?>
												
										
											
											

                                            <td class="center  ">
<a  <?php if($status != 1): ?> href="<?php echo e(url('mer_edit_deals/'.$deal_list->deal_id)); ?>" data-tooltip="Edit"> <i class="icon icon-edit icon-1x" style="margin-left:15px;"><?php else: ?> <i class="icon icon-edit icon-1x" style="margin-left:15px;" onClick="alert('Deal is inprogress')"><?php endif; ?></i></a>

                                         	<?php if($status != 1): ?>
											<?php 	echo
											"<span >".$process."</span>";?>  <?php else: ?> <?php  echo
											"<span class='statusValiation'>".$process."</span>"; ?> <?php endif; ?>
											<?php /*
                                             @if($deal_list->deal_no_of_purchase == 0)  
											 @if($status != 1)
											 <a href="{{ url('mer_delete_deals/'.$deal_list->deal_id)}}" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_DELETE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DELETE'): trans($MER_OUR_LANGUAGE.'.MER_DELETE')}}">
											 <i class="icon icon-trash icon-1x" style="margin-left:15px;">
											 </i>
											 </a> 
											 @else   <i class="icon icon-trash icon-1x" style="margin-left:15px;" onClick="alert('Deals in process')"> @endif  @endif */ ?>

                                            </td>
                                                <td class="center  "><a href="<?php echo e(url('mer_deal_details')."/".$deal_list->deal_id); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS')); ?></a></td>

                                        </tr>

			<?php $i++; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>
			

			<?php else: ?>

			
<?php if(count($deal_details)>0): ?> 
									  <?php $__currentLoopData = $deal_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
										<?php 
									  $deal_get_img = explode("/**/",$deal_list->deal_image);
											?>
									  <?php if($deal_list->deal_status == 1): ?>

									  <?php

										  $process = "<a href='".url('mer_block_deals/'.$deal_list->deal_id.'/0')."' data-tooltip='click to Block'> <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>"; ?>

								       <?php elseif($deal_list->deal_status == 0): ?> 
										<?php
										   $process = "<a href='".url('mer_block_deals/'.$deal_list->deal_id.'/1')."' data-tooltip='click to Unblock' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
											?>
										<?php endif; ?>	
                                    <tr class="gradeA odd">

                                            <td class="sorting_1"><?php echo e($i); ?></td>

                                            <td class="center  "><?php echo e(substr($deal_list->deal_title,0,45)); ?></td>

                                           

                                            <td class="center  "><?php echo e($deal_list->stor_name); ?></td>

                                            <td class="center  "><?php echo e($deal_list->deal_original_price); ?></td>

                                            <td class="center  "><?php echo e($deal_list->deal_discount_price); ?></td>

                                            <td class="center  ">
										<?php $pro_img = $deal_get_img[0];
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
							
							  <?php if($deal_get_img !=''): ?> 
								
					  
								
						   <?php if($deal_get_img[0] !=''): ?>  
						    
							<?php  
							   $img_data = "public/assets/deals/".$pro_img;  ?>
							    <?php if(file_exists($img_data)): ?>  
									 <?php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img;
								     ?>
								<?php else: ?>
									<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 					
											<?php
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												 <?php
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
									     <?php endif; ?>
										 
										 
                                <?php endif; ?>
					       
					      
						   <?php else: ?>
						   <?php
							    $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  ?>
								<?php if(isset($DynamicNoImage['dealImg'])): ?> 
										 	<?php					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];
											?>
												<?php if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path)): ?>
												<?php 
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												?>	
												<?php endif; ?>
										
									     <?php endif; ?>
										 
					<?php endif; ?>
						    
					  
			      
				  <?php else: ?>
				  
					 <?php  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; ?>
					   <?php if(isset($DynamicNoImage['dealImg'])): ?>
										 				
							<?php				
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											?>
											
											  <?php if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !=''): ?>
											
												<?php
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											    ?>
											<?php endif; ?>
											
									     <?php endif; ?>

									 
								 
					  
					
				 <?php endif; ?>					
											
											<a href="#"><img style="height:40px;" src="<?php echo e($prod_path); ?>"></a></td>
<?php 
											$starttime = strtotime($deal_list->deal_start_date); 
											$status = 0;
											$endtime = strtotime($deal_list->deal_end_date);
											$time_val = time(); ?>
											<?php if(($time_val) < $endtime): ?>
													
														<?php if(($time_val) > $starttime): ?>
														 <?php 
															$status = 1;
															//$OpenClose=((Lang::has(Session::get('admin_lang_file').'.BACK_OPEN')!= ''))? trans(Session::get('admin_lang_file').'.BACK_OPEN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_OPEN');
															 ?>
														
														<?php else: ?>
														
															<?php //$OpenClose=((Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CLOSE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE');
															?>
														<?php endif; ?>
													
														<?php else: ?>
														
														<?php 	//$OpenClose=((Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CLOSE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE');
															 ?>
														<?php endif; ?>
												
										
												
												
											
											
                                           

                                            <td class="center  ">
<?php /*
                                           <a href="<?php echo url('mer_edit_deals/'.$deal_list->deal_id); ?>" > <i class="icon icon-edit" style="margin-left:15px;" title="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_EDIT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_EDIT');} ?>"></i></a>

                                         	<?php echo  $process; ?>

                                            <?php 
                                            if(count($delete_deals) == "") { ?>
                                                <a href="<?php echo url('mer_delete_deals/'.$deal_list->deal_id); ?>" > 
                                                    <i class="icon icon-trash icon-1x" style="margin-left:15px;"></i>
                                                </a> 
                                      <?php } else { 
                                                foreach($delete_deals as $dd) { } 
                                                if($dd->order_pro_id != $deal_list->deal_id) { ?>  
                                                <a href="<?php echo url('mer_delete_deals/'.$deal_list->deal_id); ?>" > 
                                                    <i class="icon icon-trash icon-1x" style="margin-left:15px;"title="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_DELETE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DELETE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DELETE');} ?>"></i>
                                                </a>
                                         <?php  } 
                                           } */?>


                                            <a  <?php if($status != 1): ?> href="<?php echo e(url('mer_edit_deals/'.$deal_list->deal_id)); ?>" data-tooltip="Edit"> <i class="icon icon-edit icon-1x" style="margin-left:15px;"><?php else: ?> <i class="icon icon-edit icon-1x" style="margin-left:15px;" onClick="alert('Deal is inprogress')"><?php endif; ?></i></a>

                                         	<?php if($status != 1): ?>
											<?php	echo
											"<span >".$process."</span>"; ?> <?php else: ?> <?php echo
											"<span class='statusValiation'>".$process."</span>"; ?> <?php endif; ?>
											<?php /*
                                             @if($deal_list->deal_no_of_purchase == 0) 
											 @if($status != 1)
											 <a href="{{ url('mer_delete_deals/'.$deal_list->deal_id)}}" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_DELETE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DELETE'): trans($MER_OUR_LANGUAGE.'.MER_DELETE')}}">
											 <i class="icon icon-trash icon-1x" style="margin-left:15px;">
											 </i>
											 </a> 
											 @else   <i class="icon icon-trash icon-1x" style="margin-left:15px;" onClick="alert('Deals in process')">@endif  @endif */ ?>



                                            </td>

                                          

                                            <td class="center  "><a href="<?php echo e(url('mer_deal_details')."/".$deal_list->deal_id); ?>"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS')); ?></a></td>

                                        </tr>

			<?php $i++; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>
			<?php endif; ?>


									

										</tbody>

                                </table>
                            </div>

                                <!-- <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"></div></div></div></div><div class="row">

								

								<div class="col-sm-6">

								<div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">

								<ul class="pagination">

								<li id="dataTables-example_previous" tabindex="0" aria-controls="dataTables-example" class="paginate_button previous disabled">

								</li>

								</ul></div></div></div></div> -->

        </div>

        </div>

</div>

   

    </div>

                    

                    </div>

                    

                    

                    



                </div>

            <!--END PAGE CONTENT -->

 

        </div>

    

     <!--END MAIN WRAPPER -->



   <!-- FOOTER -->

      <?php echo $adminfooter; ?>


    <!--END FOOTER --> 

     <!-- GLOBAL SCRIPTS -->

    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>

     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 

    <!-- END GLOBAL SCRIPTS -->

        <!-- PAGE LEVEL SCRIPTS -->

    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>

    <!-- END GLOBAL SCRIPTS -->   

     <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	   <script>

         $(function() {

            $( "#datepicker-8" ).datepicker({

               prevText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')); ?>",

               nextText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS')); ?>",

               showOtherMonths:true,

               selectOtherMonths: false

            });

            $( "#datepicker-9" ).datepicker({

               prevText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')); ?>",

               nextText:"<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') :  trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS')); ?>",

               showOtherMonths:true,

               selectOtherMonths: true

            });

         });
        $(document).on("click",".statusValiation", function(){
			alert("Deal is in Progress"); 
			return false;
		 });
         /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/
$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
      </script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>

     <!-- END BODY -->

</html>

