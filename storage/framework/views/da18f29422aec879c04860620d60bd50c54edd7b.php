<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->



 <!-- BEGIN HEAD -->

<head>

    <meta charset="UTF-8" />

    <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT_STORE')); ?> <?php endif; ?></title>

     <meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />

		<meta name="_token" content="<?php echo csrf_token(); ?>"/>

     <!--[if IE]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <![endif]-->

    <!-- GLOBAL STYLES -->

    <!-- GLOBAL STYLES -->

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />

	  <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/plan.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />

     <?php 

     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>

    <?php if(count($favi)>0): ?>  

    <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">

<?php endif; ?>	

    <!--END GLOBAL STYLES -->

       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->



</head>

     <!-- END HEAD -->



     <!-- BEGIN BODY -->

<body class="padTop53 " >



    <!-- MAIN WRAPPER -->

    <div id="wrap">





         <!-- HEADER SECTION -->

         <?php echo $adminheader; ?>


        <!-- END HEADER SECTION -->

        <!-- MENU SECTION -->

      <?php echo $adminleftmenus; ?>


        <!--END MENU SECTION -->



		<div></div>



         <!--PAGE CONTENT -->

        <div id="content">

           

                <div class="inner">

                    <div class="row">

                    <div class="col-lg-12">

                        	<ul class="breadcrumb">

                            	<li class=""><a href="#"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_HOME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?> <?php endif; ?></a></li>

                                <li class="active"><a href="#"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT_STORE')); ?> <?php endif; ?></a></li>

                            </ul>

                    </div>

                </div>

            <div class="row">

<div class="col-lg-12">

    <div class="box dark">

        <header>

            <div class="icons"><i class="icon-edit"></i></div>

            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT_STORE')); ?> <?php endif; ?></h5>

            

        </header>

        <?php if($errors->any()): ?>

         <br>

		 <ul style="color:red;">

		<div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>


         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        </div>

		</ul>	

		<?php endif; ?>

         <?php if(Session::has('mail_exist')): ?>

		<div class="alert alert-warning alert-dismissable"><?php echo Session::get('mail_exist'); ?>


        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>

		<?php endif; ?>

        

        <div class="row">

        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">

                    

                    <?php echo Form::open(array('url'=>'add_store_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>


                   

					

					 <div class="panel panel-default">

                        <div class="panel-heading">

                      <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STORE_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STORE_DETAILS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_DETAILS')); ?> <?php endif; ?>

                        </div>

                    <div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STORE_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STORE_NAME')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_NAME')); ?> <?php endif; ?><span class="text-sub">*</span></label>



						<div class="col-lg-4">

              <?php echo e(Form::hidden('store_merchant_id',$id,array('id'=>'store_merchant_id'))); ?>


							<!-- <input type="hidden" name="store_merchant_id" id="store_merchant_id" value="<?php echo $id; ?>"> -->

							<input type="text" maxlength="150" class="form-control" placeholder="Enter Store Name <?php echo e($default_lang); ?>" id="store_name" name="store_name" value="<?php echo Input::old('store_name'); ?>"  >

						</div>

						</div>

                    </div>

					<?php if(!empty($get_active_lang)): ?>  

					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				<?php	$get_lang_name = $get_lang->lang_name; ?>

					

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1">Store Name(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>



						<div class="col-lg-4">

							<input type="text" maxlength="150" class="form-control" placeholder="Enter Store Name In <?php echo e($get_lang_name); ?>" id="store_name_<?php echo e($get_lang_name); ?>" name="store_name_<?php echo e($get_lang_name); ?>" value="<?php echo Input::old('store_name_'.$get_lang_name); ?>"  >

						</div>

						</div>

                    </div>

					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

						 <div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PHONE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?> <span class="text-sub">*</span></label>



                    <div class="col-lg-4">

                      <?php echo e(Form::text('store_pho',Input::old('store_pho'),array('id'=>'store_pho','class'=>'form-control','placeholder'=>'Enter Phone Number','maxlength'=>'16'))); ?>


                        <!-- <input type="text" maxlength="16" class="form-control" placeholder="Enter Phone Number" id="store_pho" name="store_pho" value="<?php echo Input::old('store_pho'); ?>"  > -->

                    </div>

                    <div id="store_pho_error_msg" style="color:#F00;font-weight:800"></div>

                </div>

                        </div>

						

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS1')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1')); ?> <?php endif; ?><span class="text-sub">*</span></label>



							<div class="col-lg-4">



								<input type="text" class="form-control" placeholder="Enter Store Address one <?php echo e($default_lang); ?>" id="store_add_one" name="store_add_one" value="<?php echo Input::old('store_add_one'); ?>"  >

							</div>



						</div>

                    </div>

					

					<?php if(!empty($get_active_lang)): ?>  

					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				<?php	$get_lang_name = $get_lang->lang_name; ?>

					

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1">Address One(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>



							<div class="col-lg-4">

								<input type="text" class="form-control" placeholder="Enter Store Address one In <?php echo e(Helper::lang_name()); ?>" id="store_add_one_<?php echo e($get_lang_name); ?>" name="store_add_one_<?php echo e($get_lang_name); ?>" value="<?php echo Input::old('store_add_one_'.$get_lang_name); ?>"  >

							</div>

						</div>

                    </div>

					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS2')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2')); ?> <?php endif; ?><span class="text-sub">*</span></label>



							<div class="col-lg-4">

								<input type="text" class="form-control" placeholder="Enter Store Address two <?php echo e($default_lang); ?>" id="store_add_two" name="store_add_two" value="<?php echo Input::old('store_add_two'); ?>"   >

							</div>

						</div>

                    </div>

					

					<?php if(!empty($get_active_lang)): ?>  

					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				<?php	$get_lang_name = $get_lang->lang_name; ?>

					

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1">Address two(<?php echo e($get_lang_name); ?>)<span class="text-sub">*</span></label>



							<div class="col-lg-4">

								<input type="text" class="form-control" placeholder="Enter Store Address two In <?php echo e($get_lang_name); ?>" id="store_add_two_<?php echo e($get_lang_name); ?>" name="store_add_two_<?php echo e($get_lang_name); ?>" value="<?php echo Input::old('store_add_two_'.$get_lang_name); ?>" >

							</div>

						</div>

                    </div>

                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

						 <div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY')); ?> <?php endif; ?><span class="text-sub">*</span></label>



                    <div class="col-lg-4"> 

                       <select class="form-control" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >

                        <option value="">-- <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT')); ?> <?php endif; ?> --</option>

                         <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

          				 <option value="<?php echo e($country_fetch->co_id); ?>" <?php if(Input::old('select_country')==$country_fetch->co_id) echo 'selected';?>><?php echo $country_fetch->co_name; ?></option>

           				   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

       					 </select>

                    </div>

                </div>

                        </div>

						 <div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT_CITY')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_CITY')); ?> <?php endif; ?> <span class="text-sub">*</span></label>



                    <div class="col-lg-4">

                       <select class="form-control" name="select_city" id="select_city" required>

           				<option value="">-- <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SELECT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT')); ?> <?php endif; ?> --</option>

                  </select>

                    </div>

                      <div id="city_error_msg" style="color:#F00;font-weight:800"></div>

                </div>

                        </div>

						<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ZIPCODE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ZIPCODE')); ?> <?php endif; ?><span class="text-sub">*</span></label>



                    <div class="col-lg-4">

                       <?php echo e(Form::text('zip_code',Input::old('zip_code'),array('id'=>'zip_code','class'=>'form-control','placeholder'=>'Enter Zipcode','maxlength'=>'12'))); ?>


                       <!--  <input type="text" maxlength="12" class="form-control" placeholder="Enter Zipcode" id="zip_code" name="zip_code" value="<?php echo Input::old('zip_code'); ?>"  > -->

                    </div>

                     <div id="zip_code_error_msg" style="color:#F00;font-weight:800"></div>

                </div>

                        </div>

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_META_KEYWORDS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_META_KEYWORDS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_META_KEYWORDS')); ?> <?php endif; ?><span class="text-sub"></span></label>



							<div class="col-lg-4">

								<textarea  class="form-control" name="meta_keyword" placeholder="Enter Meta Keywords <?php echo e($default_lang); ?>" id="meta_keyword" ><?php echo Input::old('meta_keyword'); ?></textarea>

							</div>

						</div>

                    </div>

					

					<?php if(!empty($get_active_lang)): ?>  

					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

				<?php	$get_lang_name = $get_lang->lang_name; ?>

					

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1">Meta Keywords(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>



							<div class="col-lg-4">

								<textarea  class="form-control" name="meta_keyword_<?php echo e($get_lang_name); ?>" id="meta_keyword_<?php echo e($get_lang_name); ?>" placeholder="Enter Meta Keywords In <?php echo e($get_lang_name); ?>" ><?php echo Input::old('meta_keyword_'.$get_lang_name); ?></textarea>

							</div>

						</div>

                    </div>

					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_META_DESCRIPTION')); ?> <?php endif; ?><span class="text-sub"></span></label>



							<div class="col-lg-4">

								<textarea id="meta_description"  name="meta_description" placeholder="Enter Meta Description <?php echo e($default_lang); ?>" class="form-control"><?php echo Input::old('meta_description'); ?></textarea>

							</div>

						</div>

                    </div>

					

					<?php if(!empty($get_active_lang)): ?>  

					<?php $__currentLoopData = $get_active_lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

					$get_lang_name = $get_lang->lang_name;

					

					<div class="panel-body">

                        <div class="form-group">

							<label class="control-label col-lg-2" for="text1">Meta Description(<?php echo e($get_lang_name); ?>)<span class="text-sub"></span></label>



							<div class="col-lg-4">

								<textarea id="meta_description_<?php echo e($get_lang_name); ?>"  name="meta_description_<?php echo e($get_lang_name); ?>" placeholder=" Enter Meta Description In <?php echo e($get_lang_name); ?>" class="form-control"><?php echo Input::old('meta_description_'.$get_lang_name); ?></textarea>

							</div>

						</div>

                    </div>

					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

							<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_WEBSITE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE')); ?> <?php endif; ?><span class="text-sub"></span></label>



                    <div class="col-lg-4">

                        <input type="url" class="form-control" id="website" name="website" value="<?php echo Input::old('website'); ?>"  placeholder="https://www.example.com" >

                    </div>

                </div>

                        </div>

						<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><span class="text-sub"></span></label>



                    <div class="col-lg-4">

                        <input id="pac-input" onclick="check();" class="form-control" type="text" placeholder="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TYPE_YOUR_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TYPE_YOUR_LOCATION')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TYPE_YOUR_LOCATION')); ?> <?php endif; ?>">



                    </div>

					<div class="col-lg-4"></div>

                </div>

                        </div>

                        <input type="hidden" name="exist" id="exist" value="">

						<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEARCH_LOCATION')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH_LOCATION')); ?> <?php endif; ?><span class="text-sub">*</span><br><span  style="color:#999">( <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DRAG_MARKER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DRAG_MARKER')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DRAG_MARKER')); ?> <?php endif; ?> & <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LONGITUDE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LONGITUDE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LONGITUDE')); ?> <?php endif; ?> )</span></label>



                    <div class="col-lg-4">

                        <div id="map_canvas" style="width:300px;height:250px;" ></div>

                        <div id="location_error_msg"  style="color:#F00;font-weight:800" ></div>

                    </div>

                    

                </div>

                        </div>

							<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LATITUDE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LATITUDE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LATITUDE')); ?> <?php endif; ?><span class="text-sub">*</span></label>



                    <div class="col-lg-4">

                      <?php echo e(Form::text('latitude',Input::old('latitude'),array('id'=>'latitude','class'=>'form-control','readonly'=>'readonly'))); ?>


                       <!--  <input type="text" class="form-control" placeholder="" id="latitude" name="latitude" value="<?php echo Input::old('latitude'); ?>"  readonly> -->

                    </div>

                </div>

                        </div>

							<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LONGITUDE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LONGITUDE')); ?>  else <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LONGITUDE')); ?> <?php endif; ?><span class="text-sub">*</span></label>



                    <div class="col-lg-4">

                      <?php echo e(Form::text('longitude',Input::old('longitude'),array('id'=>'longitude','class'=>'form-control','readonly'=>'readonly'))); ?>


                       <!--  <input type="text" class="form-control" placeholder="" id="longitude" name="longitude" value="<?php echo Input::old('longitude'); ?>"  readonly > -->

                    </div>

                </div>

                        </div>

                        <div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SLOGAN')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SLOGAN')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SLOGAN')); ?> <?php endif; ?><span class="text-sub">*</span></label>



                    <div class="col-lg-4">

                      <?php echo e(Form::text('slogan',Input::old('slogan'),array('id'=>'slogan','class'=>'form-control'))); ?>


                       <!--  <input type="text" class="form-control" placeholder="" id="longitude" name="longitude" value="<?php echo Input::old('longitude'); ?>"  readonly > -->

                    </div>

                </div>

                        </div>

							

						<div class="panel-body">

                           <div class="form-group">

                    <label class="control-label col-lg-2" for="text1"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_STORE_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_STORE_IMAGE')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_IMAGE')); ?> <?php endif; ?><span class="text-sub">*</span></label>

				

                    <div class="col-lg-4">

                       <input type="file" id="file" name="file" placeholder="Fruit ball" required>

                        <span class="errortext red" style="color:red"><em><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE_SIZE_MUST_BE')); ?> <?php endif; ?> <?php echo e($STORE_WIDTH); ?> x <?php echo e($STORE_HEIGHT); ?> <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PIXELS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PIXELS')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PIXELS')); ?> <?php endif; ?></em></span>

                    </div>

                </div>



                        </div>

                        

                    </div>

					

                    	

				

					<div class="form-group">

                    <label class="control-label col-lg-3" for="pass1"><span class="text-sub"></span></label>



                    <div class="col-lg-8">

                     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SUBMIT')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?> <?php endif; ?></a></button>

                     <button class="btn btn-default btn-sm btn-grad" type="reset" ><a style="color:#000"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_RESET')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?> <?php endif; ?></a></button>

                   

                    </div>

					  

                </div>

                

                </form>

                </div>

        

        </div>

    </div>

</div>

   

    </div>

                    

                    </div>

                    

                    

                    



                </div>

            <!--END PAGE CONTENT -->

 

        </div>

    

     <!--END MAIN WRAPPER -->



    <!-- FOOTER -->

      <?php echo $adminfooter; ?>


    <!--END FOOTER -->





     <!-- GLOBAL SCRIPTS -->

    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>

    <script>



	$( document ).ready(function() {

	$('#submit').click(function() {



    var longitude  = $('#longitude').val();

    var latitude   = $('#latitude').val();

    var select_city = $('#select_city');

        /*Store Address check*/

		if(($('#exist').val())==""){

      

			$('#location_error_msg').html('Please Choose Different Adrress, Store already exists in this same address');

			title.css('border', '1px solid red'); 

			title.focus();

			return false;

		}else if(($('#exist').val())==1){

     

			$('#location_error_msg').html('Please Choose Different Adrress, Store already exists in this same address');

			title.css('border', '1px solid red'); 

			title.focus();

			return false;

		}else{

			title.css('border', ''); 

			$('#location_error_msg').html('');

		}



    if(longitude.val() == ''){

            longitude.css('border', '1px solid red'); 

            $('#longitude_error_msg').html('Please Select longitude');

            longitude.focus();

            return false;

        }else{

            longitude.css('border', ''); 

            $('#longitude_error_msg').html('');

        }



        if(latitude.val() == ''){

        latitude.css('border', '1px solid red'); 

            $('#latitude_msg').html('Please Select latitude');

            latitude.focus();

            return false;

        }else{

            latitude.css('border', ''); 

            $('#latitude_error_msg').html('');

        }







    var file		 	 = $('#file');

	  var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

      	if(file.val() == "")

 		{

 		file.focus();

		file.css('border', '1px solid red'); 		

		return false;

		}			

		else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				

		file.focus();

		file.css('border', '1px solid red'); 		

		return false;

		}			

		else

		{

		file.css('border', ''); 				

		}

	});

	});



	    

function check() { 

			var mer_id     = $('#store_merchant_id').val();

			var longitude  = $('#longitude').val();

			var latitude   = $('#latitude').val();

			var datas      = "mer_id="+mer_id+"&latitude="+latitude+"&longitude="+longitude;

      

            $.ajax({

			      type: 'GET',

                  url: '<?php echo url('check_store_exists'); ?>',

				  data: datas,

				  success: function(response){  

                    

                     if(response==1){  //already exist

					  alert("This Store Already Exist with this same address");

					  $("#exist").val("1"); //already exist

            $('#latitude').val('');

            $('#longitude').val('');

					  $("#latitude").css('border', '1px solid red'); 

					  $('#longitude').css('border', '1px solid red');

            $('#pac-input').css('border', '1px solid red');

					  $('#location_error_msg').html("Store Already Exist with this same address");	

					  $("#pac-input").focus();

					  return false;				   

				   }else

           {

            $("#latitude").css('border', ''); 

            $('#longitude').css('border', '');

            $('#pac-input').css('border', '');

            $('#location_error_msg').html(""); 

           // $("#pac-input").blur(); 

           }

				 }		

			});

    

}

	function select_city_ajax(city_id)

	{

		 var passData = 'city_id='+city_id;

		 //alert(passData);

		   $.ajax( {

			      type: 'get',

				  data: passData,

				  url: '<?php echo url('ajax_select_city'); ?>',

				  success: function(responseText){  

				 // alert(responseText);

				   if(responseText)

				   { 

					$('#select_city').html(responseText);					   

				   }

				}		

			});		

	}

	

	function select_mer_city_ajax(city_id)

	{

		 var passData = 'city_id='+city_id;

		

		   $.ajax( {

			      type: 'get',

				  data: passData,

				  url: '<?php echo url('ajax_select_city'); ?>',

				  success: function(responseText){  

				  if(responseText){ 

					$('#select_mer_city').html(responseText);					   

				  }

				}		

			});	

	}



  



	</script>

     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

    <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>

    <?php $city_det = DB::table('nm_emailsetting')->first(); ?>

    <script type="text/javascript">

    var map;



    function initialize() {





 		var myLatlng = new google.maps.LatLng(<?php echo $city_det->es_latitude; ?>,<?php echo $city_det->es_longitude; ?>);

        var mapOptions = {



           zoom: 10,

                center: myLatlng,

                disableDefaultUI: true,

                panControl: true,

                zoomControl: true,

                mapTypeControl: true,

                streetViewControl: true,

                mapTypeId: google.maps.MapTypeId.ROADMAP



        };



        map = new google.maps.Map(document.getElementById('map_canvas'),

            mapOptions);

	 		  var marker = new google.maps.Marker({

                position: myLatlng,

                map: map,

				draggable:true,    

            });	

		google.maps.event.addListener(marker, 'dragend', function(e) {

   			 

			 var lat = this.getPosition().lat();

  			 var lng = this.getPosition().lng();

			 $('#latitude').val(lat);

			 $('#longitude').val(lng);

           check();

			});

        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

 

        google.maps.event.addListener(autocomplete, 'place_changed', function () {

 

            var place = autocomplete.getPlace();

                var latitude = place.geometry.location.lat();

                var longitude = place.geometry.location.lng();

                

          $('#latitude').val(latitude);

			    $('#longitude').val(longitude);

                check();

               

            if (place.geometry.viewport) {

                map.fitBounds(place.geometry.viewport);

				var myLatlng = place.geometry.location;	

				var latlng = new google.maps.LatLng(latitude, longitude);

                marker.setPosition(latlng);

                

			google.maps.event.addListener(marker, 'dragend', function(e) {

   			 

			 var lat = this.getPosition().lat();

  			 var lng = this.getPosition().lng();

			 $('#latitude').val(lat);

			 $('#longitude').val(lng);

            check();

            });

            } else {

                map.setCenter(place.geometry.location);	

				

                map.setZoom(17);

            }

        });







    }





    google.maps.event.addDomListener(window, 'load', initialize);

	</script>

    <!-- END GLOBAL SCRIPTS -->   

     <!---Right Click Block Code---->

<script language="javascript">

document.onmousedown=disableclick;

status="Cannot Access for this mode";

function disableclick(event)

{

  if(event.button==2)

   {

     alert(status);

     return false;    

   }

}

</script>



	<script type="text/javascript">

   $.ajaxSetup({

       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

   });

</script>





<!---F12 Block Code---->

<script type='text/javascript'>



$('#store_name').bind('keyup blur',function(){ 

    var node = $(this);

    node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }

);



$( document ).ready(function() {

  var phone_no         = $('#phone_no');

  var store_pho        = $('#store_pho');

  var zip_code         = $('#zip_code');

   $('#phone_no').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            phone_no.css('border', '1px solid red'); 

            $('#phone_no_error_msg').html('Numbers Only Allowed');

            phone_no.focus();

            return false;

        }else{          

            phone_no.css('border', ''); 

            $('#phone_no_error_msg').html('');          

        }

    });

    /*Store Phone Number*/

    $('#store_pho').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            store_pho.css('border', '1px solid red'); 

            $('#store_pho_error_msg').html('Numbers Only Allowed');

            store_pho.focus();

            return false;

        }else{          

            store_pho.css('border', ''); 

            $('#store_pho_error_msg').html('');         

        }

    });

   

    /*Store Zipcode*/

    $('#zip_code').keypress(function (e){

        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){

            zip_code.css('border', '1px solid red'); 

            $('#zip_code_error_msg').html('Numbers Only Allowed');

            zip_code.focus();

            return false;

        }else{          

            zip_code.css('border', ''); 

            $('#zip_code_error_msg').html('');          

        }

    });





         $.ajax( {

            type: 'get',

         data: {'city_id_ajax':'<?php echo Input::old('select_city'); ?>','country_id_ajax':'<?php echo Input::old('select_country'); ?>'},

         url: '<?php echo url('ajax_select_city_edit'); ?>',

         success: function(responseText){  

           if(responseText)

           { 

               

          $('#select_city').html(responseText);             

           }

        }   

      }); 



});





/*

$(document).keydown(function(event){

    if(event.keyCode==123){

    return false;

   }

else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        

      return false;  //Prevent from ctrl+shift+i

   }

});*/

</script>

</body>

     <!-- END BODY -->

</html>

