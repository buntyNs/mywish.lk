<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
     <title><?php echo e($SITENAME); ?> | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_REVIEW')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_REVIEW')); ?> <?php endif; ?> </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>	
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-wysihtml5-hack.css" />
     <style>
                        ul.wysihtml5-toolbar > li {
                            position: relative;
                        }
                    </style>
    <!-- END PAGE LEVEL  STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_HOME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?></a></li>
                                <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_REVIEW')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_REVIEW')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
                
   
    <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_REVIEW')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EDIT_REVIEW')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_REVIEW')); ?> <?php endif; ?></h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
         <?php if($errors->any()): ?>
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>')); ?>

		<?php echo e(Form::button('x', array('class' => 'close','aria-hidden' => 'true', 'data-dismiss' =>'alert'))); ?>

         
        </div>
		</ul>	
		<?php endif; ?>
         <?php if(Session::has('error_message')): ?>
		<div class="alert alert-danger alert-dismissable"><?php echo Session::get('error_message'); ?></div>
		<?php endif; ?>
        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             <?php echo Form::open(array('url'=>'edit_deal_review_submit','class'=>'form-horizontal')); ?>


                <div class="form-group">
                    <label for="text1" class="control-label col-lg-5"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REVIEW_TITLE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_TITLE')); ?><?php endif; ?> :<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="hidden" name="comment_id" value="<?php echo $info->comment_id; ?>"?>
                        <input id="text1" placeholder="<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TITLE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TITLE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TITLE')); ?> <?php endif; ?>" class="form-control review_title" type="text" id="review_title" name="review_title" value="<?php echo $info->title; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                <label for="text1" class="control-label col-lg-2"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_DESCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REVIEW_DESCRIPTION')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_DESCRIPTION')); ?> <?php endif; ?> :<span class="text-sub">*</span></label>
                        <div class="col-lg-11">
                        
                            <div class="">
                               
                                <div class="body collapse in">
								<?php echo Form::open(); ?>

                                        <textarea id="wysihtml5"  required class="form-control review_comment" rows="10" id="review_comment" name="review_comment" ><?php echo $info->comments; ?></textarea>

                                        <div class="form-actions">
                                            <br />
                                           <button class="btn btn-warning btn-sm btn-grad" onclick="error();"><a  style="color:#fff"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_UPDATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_UPDATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_UPDATE')); ?> <?php endif; ?></a></button>
                     
											<button class="btn btn-default btn-sm btn-grad"><a href=" <?php echo e(url('manage_deal_review')); ?> " style="color:#000"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CANCEL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL')); ?> <?php endif; ?></button>
                                        </div>
										<?php echo Form::close(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
				<?php echo Form::close(); ?>

        </div>
    </div>
</div>
   
    </div>
    
    
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <?php echo $adminfooter; ?>

    <!--END FOOTER -->


   <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

         <!-- PAGE LEVEL SCRIPTS -->
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/editorInit.js"></script>
    <script>
        $(function () { formWysiwyg(); });
        </script>
<script type="text/javascript">

function error() 
{
	var Title = $('.review_title').val()
	
	var review_comment = $('.review_comment').val()
		
	if(Title == '')
	{
		alert("<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_TITLE_FIELD_IS_EMPTY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REVIEW_TITLE_FIELD_IS_EMPTY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_TITLE_FIELD_IS_EMPTY')); ?> <?php endif; ?>")
		return false;
	}
	if(review_comment == '')
	{
		alert("<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_DESCRIPTION_FIELD_IS_EMPTY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_REVIEW_DESCRIPTION_FIELD_IS_EMPTY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_DESCRIPTION_FIELD_IS_EMPTY')); ?> <?php endif; ?>")
		return false;
	}
}

</script>
   <!--
    

     <!--END PAGE LEVEL SCRIPTS -->

</body>
     <!-- END BODY -->
</html>
