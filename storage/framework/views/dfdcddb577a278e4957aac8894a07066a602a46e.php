<?php echo $navbar; ?>

<!-- Navbar ================================================== -->
<?php echo $header; ?>

<!-- Header End====================================================================== -->
<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
<?php $deal_title = 'deal_title';
$pro_title = 'pro_title';
$stor_name = 'stor_name'; ?>
<?php else: ?> <?php   $deal_title = 'deal_title_'.Session::get('lang_code'); 
$pro_title = 'pro_title_'.Session::get('lang_code');
$stor_name = 'stor_name_'.Session::get('lang_code');  ?>
<?php endif; ?>    
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li> 
            <li><strong><?php echo e((Lang::has(Session::get('lang_file').'.NEAR_BY_STORE')!= '') ?  trans(Session::get('lang_file').'.NEAR_BY_STORE'): trans($OUR_LANGUAGE.'.NEAR_BY_STORE')); ?></strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
   <section class="blog_post nearmemap">
    <div class="container">
       <div class="row" id="three">
	     <div class="col-xs-12 col-sm-9 col-sm-push-3" id="center_column">
         <div>
            <div id="map" style="height: 428px;  margin: 2%; ">
            </div>
            <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?> 
            <?php $map_lang = 'en'; ?>
            <?php else: ?>   
            <?php $map_lang = Session::get('lang_code'); ?>
            <?php endif; ?>
            <?php $stor_latitude = '0.0'; $stor_longitude ='0.0'; ?>
            <script type="text/javascript" src='https://maps.google.com/maps/api/js?libraries=places&key=<?php echo e($GOOGLE_KEY); ?>&language=<?php echo e($map_lang); ?>'></script>
            <script type="text/javascript">
               var locations = [
               <?php if(count($get_store_all) > 0) {
                  foreach($get_store_all as $sg) { 
                  
                   if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
                    $stor_name = 'stor_name';
                    $stor_address1 = 'stor_address1';
                    $stor_address2 = 'stor_address2';
                   }else {  
                    $stor_name = 'stor_name_'.Session::get('lang_code'); 
                    $stor_address1 = 'stor_address1_'.Session::get('lang_code'); 
                    $stor_address2 = 'stor_address1_'.Session::get('lang_code'); 
                   } ?>        
                 ['<b><?php if (Lang::has(Session::get('lang_file').'.STORE_NAME')!= '') { echo  trans(Session::get('lang_file').'.STORE_NAME');}  else { echo trans($OUR_LANGUAGE.'.STORE_NAME');} ?>:</b>&nbsp;<?php echo $sg->$stor_name; ?>,<br/><b><?php if (Lang::has(Session::get('lang_file').'.ADDRESS1')!= '') { echo  trans(Session::get('lang_file').'.ADDRESS1');}  else { echo trans($OUR_LANGUAGE.'.ADDRESS1');} ?>:</b>&nbsp;<?php echo $sg->$stor_address1; ?>,<br/><?php echo $sg->$stor_address2; ?>,<br/><b><?php if (Lang::has(Session::get('lang_file').'.PHONE')!= '') { echo  trans(Session::get('lang_file').'.PHONE');}  else { echo trans($OUR_LANGUAGE.'.PHONE');} ?>:</b>&nbsp;<?php echo $sg->stor_phone; ?>',  <?php echo $sg->stor_latitude; ?>, <?php echo $sg->stor_longitude; ?>, 4],
                 <?php $stor_latitude=$sg->stor_latitude; $stor_longitude=$sg->stor_longitude; } } ?>
               ];
               
               var map = new google.maps.Map(document.getElementById('map'), {
                 zoom: 10,
              
               
                 center: new google.maps.LatLng(<?php echo $stor_latitude; ?>, <?php echo $stor_longitude; ?>),
               
                 mapTypeId: google.maps.MapTypeId.ROADMAP
               });
               
               var infowindow = new google.maps.InfoWindow();
               
               var marker, i;
               
               for (i = 0; i < locations.length; i++) { 
                 marker = new google.maps.Marker({
                   position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                   map: map
                 });
               
                 google.maps.event.addListener(marker, 'click', (function(marker, i) {
                   return function() {
                     infowindow.setContent(locations[i][0]);
                     infowindow.open(map, marker);
                   }
                 })(marker, i));
               }
            </script>
         </div>
      </div>

	 
	  <aside class="sidebar col-xs-12 col-sm-3 col-sm-pull-9"> 
          <!-- Blog category -->
          <div class="block blog-module">
		  
		   <div class="sidebar-bar-title">
              <h3><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></h3>
            </div>
           
            <div class="block_content">
			  <?php $__currentLoopData = $get_default_city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $default_city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php $default_city_id=$default_city->ci_id; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <select name="selected_city" id="selected_city" required data-parsley-required="true" onchange="select_city(this.value)">
               <option value="">--<?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?>--</option>
               <?php $__currentLoopData = $city_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               
                 <?php if(Session::get('lang_code')=='' || Session::get('lang_code')=='en'): ?> 
                   <option value="<?php echo e($city->ci_id); ?>" <?php if($default_city_id==$city->ci_id): ?> selected="selected" <?php endif; ?>><?php echo e($city->ci_name); ?></option>
                 <?php else: ?>
                 <!-- if Selected Language city name Display -->
                   <?php $get_lang = 'ci_name_'.Session::get('lang_code'); ?>
                   <option value="<?php echo e($city->ci_id); ?>" <?php if($default_city_id==$city->ci_id): ?> selected="selected" <?php endif; ?>><?php echo e($city->$get_lang); ?></option>
                 <?php endif; ?>

               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            </div>
          </div>
		  
		    <div class="block blog-module">
            <div class="sidebar-bar-title">
              <h3><?php if(Lang::has(Session::get('lang_file').'.STORES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.STORES')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.STORES')); ?> <?php endif; ?></h3>
            </div>
            <div class="block_content near-map"> 
              <!-- layered -->
              <div class="layered">
                <div class="layered-content">
				
                  <ul class="blog-list-sidebar">
				     <?php if(count($nearbystore)>0): ?>    
                     <?php $__currentLoopData = $nearbystore; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nearstore): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                     <?php $product_image =explode('/**/',$nearstore->stor_img);
                     $name=$nearstore->stor_name;
                     $product_img    =$product_image[0];
                     $prod_path  = url('').'/public/assets/default_image/No_image_product.png';
                     $img_data   = "public/assets/storeimage/".$product_img; ?>
                     <?php if(file_exists($img_data) && $product_img !=''): ?>   
                     <?php $prod_path = url('').'/public/assets/storeimage/' .$product_img;  ?>   
                     <?php else: ?>  
                     <?php if(isset($DynamicNoImage['store'])): ?>
                     <?php  $dyanamicNoImg_path = 'public/assets/noimage/' .$DynamicNoImage['store']; ?>
                     <?php if($DynamicNoImage['store']!='' && file_exists($dyanamicNoImg_path)): ?>
                     <?php $prod_path = url('').'/'.$dyanamicNoImg_path; ?> <?php endif; ?>
                     <?php endif; ?>
                     <?php endif; ?>   
                     <?php $alt_text  = $nearstore->$stor_name; ?>
                    <li>
                      <div class="post-thumb" id="<?php echo e($nearstore->stor_id); ?>"> <a href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($nearstore->stor_id)))); ?>"><img src="<?php echo e($prod_path); ?>" alt="<?php echo e($alt_text); ?>"></a> </div>
                      <div class="post-info">
                        <h5 class="entry_title"><a><?php echo e(substr($nearstore->$stor_name,0,25)); ?>

                                 <?php echo e(strlen($nearstore->$stor_name)>25?'..':''); ?></a></h5>
                      </div>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  <?php else: ?>
                    <?php if(Lang::has(Session::get('lang_file').'.Nearbyshop')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Nearbyshop')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Nearbyshop')); ?> <?php endif; ?>...
				    <?php endif; ?> 
                  </ul>
                </div>
              </div>
              <!-- ./layered --> 
            </div>
          </div>
          
         
      </aside>
  </div>
 </div>
  </section>

<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
<?php echo $footer; ?>

<!-- For Responsive menu-->
<script type="text/javascript">
   $(document).ready(function() {
       $(document).on("click", ".customCategories .topfirst b", function() {
           $(this).next("ul").css("position", "relative");
   
           $(".topfirst ul").not($(this).parents(".topfirst").find("ul")).css("display", "none");
           $(this).next("ul").toggle();
       });
   
       $(document).on("click", ".morePage", function() {
           $(".nextPage").slideToggle(200);
       });
   
       $(document).on("click", "#smallScreen", function() {
           $(this).toggleClass("customMenu");
       });
   
       $(window).scroll(function() {
           if ($(this).scrollTop() > 250) {
               $('#comp_myprod').show();
           } else {
               $('#comp_myprod').hide();
           }
       });
   
   });

   
   
   function select_city(city_id)
   { 
    var passData = 'city_id='+city_id;
   
    //alert(passData);
    $.ajax({
           type:'get',
         data: passData,
         url: '<?php echo url('nearbystore_select_city'); ?>',
         success: function(data){  

         $("#three").html(data);
           //alert(data);
        
       }   
     });   
   }
   function initialize() {
      var myOptions = {
          zoom: 10,
   
          zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_CENTER
          },
          center: new google.maps.LatLng(<?php echo $stor_latitude; ?>, <?php echo $stor_longitude; ?>),
          mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      var map = new google.maps.Map(document.getElementById('map'), myOptions);
     
      setMarkers(map, three);
   }
   
</script>

</body>
</html>