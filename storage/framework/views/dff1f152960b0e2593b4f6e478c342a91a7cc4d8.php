<!DOCTYPE>
<html lang="en">
   <head>
    <meta charset="utf-8">
	
	 <?php 
    $table_data = DB::table('nm_social_media')->first();

    if($metadetails){
    foreach($metadetails as $metainfo) {
      if(Session::get('lang_code') == '' || Session::get('lang_code') == 'en')
            {
                $metaname = $metainfo->gs_metatitle;
                $metakeywords = $metainfo->gs_metakeywords;
                $metadesc = $metainfo->gs_metadesc;
            }
            else
            {
                $get_lang_title = 'gs_metatitle_'.Session::get('lang_code');
                $get_lang_keywords = 'gs_metakeywords_'.Session::get('lang_code');
                $get_lang_desc = 'gs_metadesc_'.Session::get('lang_code');
                $metaname = $metainfo->$get_lang_title;
                $metakeywords = $metainfo->$get_lang_keywords;
                $metadesc = $metainfo->$get_lang_desc;
            }
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
	
	 <title><?php echo $metaname  ;?></title>
  
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="description" content="<?php echo $metadesc  ;?>">
     <meta name="keywords" content="<?php echo  $metakeywords ;?>">
     <meta name="author" content="<?php echo  $metaname ;?>">
     <meta name="_token" content="<?php echo csrf_token(); ?>"/>
	 <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
	 <meta property="og:title" content="<?php echo $metaname;?>" />
    <meta name="image" property="og:image" content="<?php echo e(Request::url()); ?>" />
    <meta property="og:url" content="<?php echo url('')  ;?>" />
    <meta property="og:description" content="<?php echo $metadesc  ;?>" />
	 <script>
	 $(function () {
		$.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
		});
	});
	 </script>
<!-- Bootstrap style --> 

<!-- CSS Style -->
	
	<link href="<?php echo url(''); ?>/public/themes/css/bootstrap.min.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/simple-line-icons.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/font-awesome.min.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/jquery-ui.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/owl.carousel.min.css" rel="stylesheet"/>
    <link href="<?php echo url(''); ?>/public/themes/css/owl.theme.default.min.css" rel="stylesheet"/> 
    <link href="<?php echo url(''); ?>/public/themes/css/owl.transitions.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/pe-icon-7-stroke.min.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/animate.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/blog.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/flexslider.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/quick_view_popup.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/revolution-slider.css" rel="stylesheet"/>  
    <link href="<?php echo url(''); ?>/public/themes/css/shortcode.css" rel="stylesheet"/>  

    
     <link href="<?php echo url(''); ?>/public/themes/css/responsive.css" rel="stylesheet"/>  
    <?php if(Session::get('lang_code') == '' || Session::get('lang_code') == 'ar'): ?>
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/style-arabic.css">
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/bootstrap-rtl.css">       
    <?php else: ?>
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/themes/css/style.css">
    <?php endif; ?>

     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); 
     foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">

</head>

<body>

<script src="<?php echo url('')?>/public/assets/js/facebook_sdk.js" type="text/javascript"></script>
<div id="fb-root"></div>
<!--<script src="//connect.facebook.net/en_US/sdk.js" type="text/javascript"></script>-->
<script type="text/javascript">
window.fbAsyncInit = function() {
    FB.init({
        appId   : '1906185466333117',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true, // parse XFBML
        version    : 'v2.8' // use graph api version 2.8
    });

  };

function fb_login()
{ 
  console.log( 'fb_login function initiated' );
	  FB.login(function(response) {

      console.log( 'login response' );
      console.log( response );
      console.log( 'Response Status' + response.status );
		//top.location.href=http://demo.Sundaroecommerce.com/;
      if (response.authResponse) {

        console.log( 'Auth success' );

    		FB.api("/me",'GET',{'fields':'id,email,verified,name'}, function(me){

      		if (me.id) {


            //console.log( 'Retrived user details from FB.api', me );

             var id = me.id; 
		var email = me.email;
            	var name = me.name;
                var live ='';
				if (me.hometown!= null)
				{			
					var live = me.hometown.name;
				}        
            	
    var passData = 'fid='+ id + '&email='+ email + '&name='+ name + '&live='+ live ;
 //alert(passData);
            //console.log('data', passData);
          
            $.ajax({
             type: 'GET',
            data: passData,
             //data: $.param(passData),
             global: false,
             url: '<?php echo url('facebooklogin'); ?>',
             success: function(responseText){ 
              console.log(responseText); 
              //window.location.href = '<?php echo url('');?>';
              location.reload();
             },
             error: function(xhr,status,error){
               console.log(status, status.responseText);
             }
           }); 

        }else{

          console.log('There was a problem with FB.api', me);

        }
      });

    }else{
      console.log( 'Auth Failed' );
    }

  }, {scope: 'email'});
}
function logout() {

        try {
        if (FB.getAccessToken() != null) {
            FB.logout(function(response) {
                // user is now logged out from facebook do your post request or just redirect
                window.location = "<?php echo url('facebook_logout'); ?>";
            });
        } else {
            // user is not logged in with facebook, maybe with something else
            window.location = "<?php echo url('facebook_logout'); ?>";
        }
    } catch (err) {
        // any errors just logout
        window.location = "<?php echo url('facebook_logout'); ?>";
    }
           /*  FB.logout(function(response) {
				    
                FB.getAuthResponse(null, 'unknown');
                //FB.Auth.getAuthResponse(null, 'unknown');
                 window.location = "<?php echo url('facebook_logout'); ?>";
              //FB.logout();
               				console.log(response);

            }); */
}
</script>

<?php if(Session::has('login_message')): ?>
<center><p class="alert alert-success" id="success"><?php echo e(Session::get('login_message')); ?> </p></center>
<?php Session::forget('login_message');?>
<?php endif; ?>
 <div id="mobile-menu">
        
  <ul>

    <?php
          $prod_path_loader = url('').'/public/assets/noimage/product_loading.gif';
          $image_exist_count="";
        $i=1; ?> 

  <?php if(count($main_category_header)>0): ?>
        <?php $__currentLoopData = $main_category_header; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php  $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>
        <?php if($i<=7): ?>
                <li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1)); ?>">
           <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
           <?php  $mc_name = 'mc_name'; ?>
           <?php else: ?>   <?php $mc_name = 'mc_name_'.Session::get('lang_code'); ?> <?php endif; ?>
           <?php echo e($fetch_main_cat->mc_name); ?>

           </a>
           
          <?php if(count($sub_main_category_header[$fetch_main_cat->mc_id])> 0): ?> 
          
                      <ul class="">
             <?php $__currentLoopData = $sub_main_category_header[$fetch_main_cat->mc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_main_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <?php $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>
                        <li><a href=" <?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2)); ?> ">
                           <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
               <?php $smc_name = 'smc_name'; ?>
               <?php else: ?> <?php $smc_name = 'smc_name_code'; ?> <?php endif; ?>   
               <?php echo e($fetch_sub_main_cat->$smc_name); ?>

                           </a>
             <?php if(count($second_main_category_header[$fetch_sub_main_cat->smc_id])> 0): ?>
              
                  <ul class="">
                  <?php $__currentLoopData = $second_main_category_header[$fetch_sub_main_cat->smc_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id; ?>
                  <li><a href=" <?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3)); ?> ">
                  <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                  <?php $sb_name = 'sb_name'; ?>
                  <?php else: ?>  <?php $sb_name = 'sb_name_langCode'; ?> <?php endif; ?>
                  <?php echo e($fetch_sub_cat->$sb_name); ?>

                  </a>
                    <?php if(count($second_sub_main_category_header[$fetch_sub_cat->sb_id])> 0): ?>
                    
                        <ul class="">
                        <?php $__currentLoopData = $second_sub_main_category_header[$fetch_sub_cat->sb_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_secsub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                        <?php $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>
                        <li><a href="<?php echo e(url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4)); ?>">
                        <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?>  
                        <?php  $ssb_name = 'ssb_name'; ?>
                        <?php else: ?>  <?php $ssb_name = 'ssb_name_langCode'; ?> <?php endif; ?>
                        <?php echo e($fetch_secsub_cat->$ssb_name); ?>

                        </a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                     <?php endif; ?>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
             <?php endif; ?>
            </li>  
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </ul>
         <?php endif; ?>
                </li>
                 <?php endif; ?>
         <?php $i++; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
            <li><a href=" <?php echo e(url('category_list_all')); ?> ">
                <?php if(Lang::has(Session::get('lang_file').'.MORE_CATEGORIES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE_CATEGORIES')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MORE_CATEGORIES')); ?> <?php endif; ?>
                </a>
            </li>

         <?php endif; ?>

    <!-- <li><a href="index-2.html" class="home1">Home Pages</a>
      <ul>
        <li><a href="index-2.html"><span>Home Version 1</span></a></li>
        <li><a href="version2/index.html"><span>Home Version 2</span></a></li>
        <li><a href="version3/index.html"><span>Home Version 3</span></a></li>
        <li><a href="version1rtl/index.html"><span>Home Version 1 RTL</span></a></li>
      </ul>
    </li>
    <li><a href="#">Pages</a>
      <ul>
        <li><a href="#">Shop Pages </a>
          <ul>
            <li><a href="shop_grid.html"><span>Grid View Category Page</span></a></li>
            <li><a href="shop_grid_full_width.html"><span>Grid View Full Width</span></a></li>
            <li><a href="shop_list.html"><span>List View Category Page</span></a></li>
            <li><a href="single_product.html"><span>Full Width Product Page</span> </a></li>
            <li><a href="single_product_sidebar.html"><span>Product Page With Sidebar</span> </a></li>
            <li><a href="single_product_magnify_zoom.html"><span>Product Page Magnify Zoom</span> </a></li>
            <li><a href="shopping_cart.html"><span>Shopping Cart</span></a></li>
            <li><a href="wishlist.html"><span>Wishlist</span></a></li>
            <li><a href="compare.html"><span>Compare Products</span></a></li>
            <li><a href="checkout.html"><span>Checkout</span></a></li>
            <li><a href="sitemap.html"><span>Sitemap</span></a></li>
          </ul>
        </li>
        <li><a href="#">Static Pages </a>
          <ul>
            <li><a href="about_us.html"><span>About Us</span></a></li>
            <li><a href="contact_us.html"><span>Contact Us</span></a></li>
            <li><a href="orders_list.html"><span>Orders List</span></a></li>
            <li><a href="order_details.html"><span>Order Details</span></a></li>
            <li><a href="404error.html"><span>404 Error</span> </a></li>
            <li><a href="faq.html"><span>FAQ Page</span></a></li>
            <li><a href="manufacturers.html"><span>Manufacturers</span></a></li>
            <li><a href="quick_view.html"><span>Quick View</span></a></li>
            <li><a href="dashboard.html"><span>Account Dashboard</span></a></li>
            <li><a href="shortcodes.html"><span>Shortcodes</span> </a></li>
            <li><a href="typography.html"><span>Typography</span></a></li>
          </ul>
        </li>
        <li><a href="#"> Blog Pages </a>
          <ul>
            <li><a href="blog_right_sidebar.html">Blog – Right sidebar </a></li>
            <li><a href="blog_left_sidebar.html">Blog – Left sidebar </a></li>
            <li><a href="blog_full_width.html">Blog - Full width</a></li>
            <li><a href="blog_single_post.html">Single post </a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="blog.html">Blog</a>
      <ul>
        <li><a href="blog_right_sidebar.html"> Blog – Right Sidebar </a></li>
        <li><a href="blog_left_sidebar.html"> Blog – Left Sidebar </a></li>
        <li><a href="blog_full_width.html"> Blog – Full-Width </a></li>
        <li><a href="blog_single_post.html"> Single post </a></li>
      </ul>
    </li>
    <li><a href="shop_grid.html">TV &amp; Audio</a>
      <ul>
        <li><a href="#" class="">Headphones</a>
          <ul>
            <li><a href="shop_grid.html">Receivers &amp; Amplifier</a></li>
            <li><a href="shop_grid.html">Speakers Sports</a></li>
            <li><a href="shop_grid.html">Audio CD Players</a></li>
            <li><a href="shop_grid.html">Turntables</a></li>
          </ul>
        </li>
        <li><a href="#">Gaming Headsets</a>
          <ul>
            <li><a href="shop_grid.html">Accessories</a></li>
            <li><a href="shop_grid.html">Smart TVs Speakers</a></li>
            <li><a href="shop_grid.html">Cases &amp; Towers</a></li>
            <li><a href="shop_grid.html">Gaming Budget</a></li>
          </ul>
        </li>
        <li><a href="#">Home Theater Systems</a>
          <ul>
            <li><a href="shop_grid.html">TV &amp; Cinema</a></li>
            <li><a href="shop_grid.html">4K Ultra HD TVs</a></li>
            <li><a href="shop_grid.html">Curved TVs</a></li>
            <li><a href="shop_grid.html">Sound Bars</a></li>
          </ul>
        </li>
        <li><a href="#">Appliances</a>
          <ul>
            <li><a href="shop_grid.html">Refrigerators</a></li>
            <li><a href="shop_grid.html">Vacuum Cleaners</a></li>
            <li><a href="shop_grid.html">Irons &amp; Steamers</a></li>
            <li><a href="shop_grid.html">Washers Dryers</a></li>
          </ul>
        </li>
        <li><a href="#">Photo &amp; Camera</a>
          <ul>
            <li><a href="shop_grid.html">Cameras Digital SLR</a></li>
            <li><a href="shop_grid.html">Instant Film</a></li>
            <li><a href="shop_grid.html">Point &amp; Shoot</a></li>
            <li><a href="shop_grid.html">Waterproof</a></li>
          </ul>
        </li>
        <li><a href="#">Accessories</a>
          <ul>
            <li><a href="shop_grid.html">Cables Chargers</a></li>
            <li><a href="shop_grid.html">Cases Backup Battery</a></li>
            <li><a href="shop_grid.html">Meebox</a></li>
            <li><a href="shop_grid.html">Packs Screen</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="shop_grid.html">Apple Store</a>
      <ul>
        <li><a href="#" class="">Notebooks</a>
          <ul class="level1">
            <li><a href="shop_grid.html">Gradiente</a></li>
            <li><a href="shop_grid.html">Samsung</a></li>
            <li><a href="shop_grid.html">Toshiba</a></li>
            <li><a href="shop_grid.html">Nintendo DS</a></li>
          </ul>
        </li>
        <li><a href="#">Televisions</a>
          <ul class="level1">
            <li><a href="shop_grid.html">LCD Televisions</a></li>
            <li><a href="shop_grid.html">Projection Televisions</a></li>
            <li><a href="shop_grid.html">Play Stations</a></li>
            <li><a href="shop_grid.html">Video Accessories</a></li>
          </ul>
        </li>
        <li><a href="#" class="">Computer</a>
          <ul class="level1">
            <li><a href="shop_grid.html">Laptop</a></li>
            <li><a href="shop_grid.html">Mobile Electronics</a></li>
            <li><a href="shop_grid.html">Movies &amp; Music</a></li>
            <li><a href="shop_grid.html">Audio &amp; Video</a></li>
          </ul>
        </li>
        <li><a href="#">Home Theater</a>
          <ul class="level1">
            <li><a href="shop_grid.html">LED &amp; LCD TVs</a></li>
            <li><a href="shop_grid.html">Smart TVs</a></li>
            <li><a href="shop_grid.html">Speakers Sound Bars</a></li>
            <li><a href="shop_grid.html">Audio CD Players</a></li>
          </ul>
        </li>
        <li><a href="#">Accesories</a>
          <ul class="level1">
            <li><a href="shop_grid.html">Theater Systems</a></li>
            <li><a href="shop_grid.html">Turntables</a></li>
            <li><a href="shop_grid.html">Laptops Bags</a></li>
            <li><a href="shop_grid.html">Gaming Headsets</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="shop_grid.html">Photo &amp; Camera</a>
      <ul>
        <li><a href="shop_grid.html">Mobiles</a>
          <ul>
            <li><a href="shop_grid.html">iPhone</a></li>
            <li><a href="shop_grid.html">Samsung</a></li>
            <li><a href="shop_grid.html">Nokia</a></li>
            <li><a href="shop_grid.html">Sony</a></li>
          </ul>
        </li>
        <li><a href="shop_grid.html" class="">Music &amp; Audio</a>
          <ul>
            <li><a href="shop_grid.html">Audio</a></li>
            <li><a href="shop_grid.html">Cameras</a></li>
            <li><a href="shop_grid.html">Appling</a></li>
            <li><a href="shop_grid.html">Car Music</a></li>
          </ul>
        </li>
        <li><a href="shop_grid.html">Accessories</a>
          <ul>
            <li><a href="shop_grid.html">Home &amp; Office</a></li>
            <li><a href="shop_grid.html">TV &amp; Home Theater</a></li>
            <li><a href="shop_grid.html">Phones &amp; Radio</a></li>
            <li><a href="shop_grid.html">All Electronics</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="shop_grid.html">Smartphone</a></li>
    <li><a href="shop_grid.html">Games &amp; Video</a></li>
    <li><a href="shop_grid.html">Laptop</a></li>
    <li><a href="shop_grid.html">Accessories</a></li> -->
  </ul>
</div>
<!-- end mobile menu -->
<div id="page"> 
<header>
<div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-4 col-md-4 col-xs-12"> 
              <!-- Default Welcome Message -->
              <div class="welcome-msg hidden-xs hidden-sm"><span class=""> <?php if (Lang::has(Session::get('lang_file').'.WELCOME')!= '') { echo  trans(Session::get('lang_file').'.WELCOME');}  else { echo trans($OUR_LANGUAGE.'.WELCOME');} ?></span>&nbsp;<?php echo Session::get('user_name');?>!</div>
              <!-- Language &amp; Currency wrapper -->
              <div class="language-currency-wrapper">
                <div class="inner-cl">
                  <!--<div class="block block-language form-language">
                    <div class="lg-cur"><span><img src="images/flag-default.jpg" alt="French"><span class="lg-fr">French</span><i class="fa fa-angle-down"></i></span></div>
                    <ul>
                      <li><a class="selected" href="#"><img src="images/flag-english.jpg" alt="english"><span>English</span></a></li>
                      <li><a href="#"><img src="images/flag-default.jpg" alt="French"><span>French</span></a></li>
                      <li><a href="#"><img src="images/flag-german.jpg" alt="German"><span>German</span></a></li>
                      <li><a href="#"><img src="images/flag-brazil.jpg" alt="Brazil"><span>Brazil</span></a></li>
                      <li><a href="#"><img src="images/flag-chile.jpg" alt="Chile"><span>Chile</span></a></li>
                      <li><a href="#"><img src="images/flag-spain.jpg" alt="Spain"><span>Spain</span></a></li>
                    </ul>
                  </div>
                  <div class="block block-currency">
                    <div class="item-cur"><span>USD</span><i class="fa fa-angle-down"></i></div>
                    <ul>
                      <li><a href="#"><span class="cur_icon">€</span>EUR</a></li>
                      <li><a href="#"><span class="cur_icon">¥</span>JPY</a></li>
                      <li><a class="selected" href="#"><span class="cur_icon">$</span>USD</a></li>
                    </ul>
                  </div>-->
                </div>
              </div>
            </div>
            
            <!-- top links -->
            <div class="headerlinkmenu col-md-8 col-sm-8 col-xs-12"> <span class="phone  hidden-xs hidden-sm"><?php if (Lang::has(Session::get('lang_file').'.CALL_US')!= '') { echo  trans(Session::get('lang_file').'.CALL_US');}  else { echo trans($OUR_LANGUAGE.'.CALL_US');} ?>:<?php echo e(Helper::customer_support_number()); ?></span>
              <ul class="links">
                <li class="hidden-xs"><a title="Help Center" href="<?php echo e(url('help')); ?>"><span><?php if (Lang::has(Session::get('lang_file').'.HELP_CENTER')!= '') { echo  trans(Session::get('lang_file').'.HELP_CENTER');}  else { echo trans($OUR_LANGUAGE.'.HELP_CENTER');} ?></span></a></li>
                <!-- check store status to view the store details -->
                <?php if($GENERAL_SETTING->gs_store_status == 'Store'): ?>
                <li><a title="Store Locator" href="<?php echo url('nearbystore');?>"><span><?php echo e((Lang::has(Session::get('lang_file').'.STORE_LOCATOR')!= '') ? trans(Session::get('lang_file').'.STORE_LOCATOR') : trans($OUR_LANGUAGE.'.STORE_LOCATOR')); ?></span></a></li> 
                <?php else: ?>

                <?php endif; ?>
                 
                <!-- <li><a href="#merchant"><span><?php echo e((Lang::has(Session::get('lang_file').'.MERCHANT')!= '') ?  trans(Session::get('lang_file').'.MERCHANT') : trans($OUR_LANGUAGE.'.MERCHANT')); ?></span></a>
                  (<a title="Merchant Login" href="<?php echo url('sitemerchant');?>"><?php echo e((Lang::has(Session::get('lang_file').'.LOGIN')!= '') ?  trans(Session::get('lang_file').'.LOGIN') : trans($OUR_LANGUAGE.'.LOGIN')); ?>

                  </a>
                     <a title="Merchant Registers" href="<?php echo url('merchant_signup');?>"><span><?php echo e((Lang::has(Session::get('lang_file').'.REGISTER')!= '') ?  trans(Session::get('lang_file').'.REGISTER') : trans($OUR_LANGUAGE.'.REGISTER')); ?></span>
                    </a>)
                </li> -->
                <li><a title="Checkout" href="<?php echo url('checkout'); ?>"><span><?php echo e((Lang::has(Session::get('lang_file').'.CHECKOUT')!= '') ? trans(Session::get('lang_file').'.CHECKOUT') : trans($OUR_LANGUAGE.'.CHECKOUT')); ?></span></a></li>
                <li>
                  <div class="dropdown"><a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href=""><span><?php if(Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MY_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?> <?php endif; ?>
</span> <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="<?php echo e(url('acc_dashboard')); ?>"><?php if(Lang::has(Session::get('lang_file').'.ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ACCOUNT')); ?> <?php endif; ?>
</a></li>
                      <li><a href="<?php echo e(url('user_wishlist')); ?>"><?php if(Lang::has(Session::get('lang_file').'.WISHLIST')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WISHLIST')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WISHLIST')); ?> <?php endif; ?>
</a></li>
                      <li><a href="<?php echo e(url('product_paypal')); ?>"><?php if(Lang::has(Session::get('lang_file').'.ORDER_TRACKING')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ORDER_TRACKING')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ORDER_TRACKING')); ?> <?php endif; ?></a></li>
                      <li><a href="<?php echo e(url('aboutus')); ?>"><?php if(Lang::has(Session::get('lang_file').'.ABOUT_US')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ABOUT_US')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ABOUT_US')); ?> <?php endif; ?></a></li>
                      <li class="divider"></li>
                      <li>
					  
					   <?php $id = Session::get('customerid'); 
						$connect=DB::table('nm_customer')->where('cus_id','=',$id)->first();
							  $status=$connect->cus_logintype;
							
							if($status == '3')
						{
						?>
						<a href="<?php echo url('facebook_logout'); ?>"><span><?php if (Lang::has(Session::get('lang_file').'.LOG_OUT')!= '') { echo  trans(Session::get('lang_file').'.LOG_OUT');}  else { echo trans($OUR_LANGUAGE.'.LOG_OUT');} ?></span></a>
						<?php } 
						elseif($status == '2'){?>
						 <a href="<?php echo url('user_logout');?>"><?php if (Lang::has(Session::get('lang_file').'.LOG_OUT')!= '') { echo  trans(Session::get('lang_file').'.LOG_OUT');}  else { echo trans($OUR_LANGUAGE.'.LOG_OUT');} ?></a>
						<?php } 

						elseif($status == '1'){ ?>
						 <a href="<?php echo url('user_logout');?>"><?php if (Lang::has(Session::get('lang_file').'.LOG_OUT')!= '') { echo  trans(Session::get('lang_file').'.LOG_OUT');}  else { echo trans($OUR_LANGUAGE.'.LOG_OUT');} ?></a>
						<?php } ?>
					  
					 </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/jquery.min.js"></script> 

<!-- countdown js --> 
<script type="text/javascript" src="<?php echo url(''); ?>/public/themes/js/countdown.js"></script> 


<script>
$("#success").fadeIn(1300).delay(1500).fadeOut(1400);
</script>
<!--language user changes-->
<script type="text/javascript">
	function Lang_change() 
	{
		var language_code = $("#Language_change option:selected").val();
		$.ajax
		({
			type:'POST',
			url:"<?php echo url('new_change_languages');?>",
			data:{'language_code':language_code},
			success:function(data)
			{
				///alert(data);
				window.location.reload();
			}
        });
	}
</script>

<!-- End language user changes-->