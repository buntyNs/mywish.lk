<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
     <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?>  <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_DASHBOARD_TEMPLATE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_DASHBOARD_TEMPLATE') :  trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_DASHBOARD_TEMPLATE')); ?> | <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_LOGIN_PAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LOGIN_PAGE') :  trans($MER_OUR_LANGUAGE.'.MER_LOGIN_PAGE')); ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
     <!-- PAGE LEVEL STYLES -->
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/login.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/magic/magic.css" />
	<?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?> 
     <!-- END PAGE LEVEL STYLES -->
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body style="background:url(assets/img/bg1.jpg) no-repeat center !important; -webkit-background-size: cover;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  background-size: cover !important;" oncontextmenu="return false" >

   <!-- PAGE CONTENT --> 
    <div class="container">
    <div class="text-center">
        <img src="<?php echo e($SITE_LOGO); ?>" alt="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_LOGO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LOGO') :  trans($MER_OUR_LANGUAGE.'.MER_LOGO')); ?>" /></a>
    </div>

    <div class="tab-content">
    
        <div id="login" class="tab-pane active"  >
        
   	<?php echo Form::open(array('url'=>'mer_login_check','class'=>'form-signin')); ?>

                 <?php if(Session::has('login_success')): ?>
		<div class="alert alert-success alert-dismissable" id="success_div" align="center" style="height:50px;width:298px;"><?php echo Session::get('login_success'); ?></div>
		<?php endif; ?>
         <?php if(Session::has('error')): ?>
		<div class="alert alert-danger alert-dismissable" id="error_div" align="center" style="height:50px;width:270px;"><?php echo Session::get('error'); ?></div>
		<?php endif; ?>

		<?php if(Session::has('login_error')): ?>
		<div class="alert alert-danger alert-dismissable" id="error_div" align="center" style="height:50px;width:270px;"><?php echo Session::get('login_error'); ?></div>
		<?php endif; ?>
                <p class="text-muted text-center btn-block  btn-primary    disabled">
                    MERCHANT LOGIN
                </p>
             <!--   <input type="text" name="mer_user" value="<?php echo Input::old('mer_user');; ?>"  placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL') : trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?>" class="form-control" />-->
                 <input type="text" name="mer_user" value="" class="form-control" />
              <!--  <input type="password" name="mer_pwd" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_PASSWORD')); ?>" class="form-control" />-->
              
              <input type="password" name="mer_pwd" value="" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_PASSWORD')); ?>" class="form-control" />
                <button class="btn text-muted text-center  btn-warning" type="submit"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SIGN_IN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SIGN_IN') : trans($MER_OUR_LANGUAGE.'.MER_SIGN_IN')); ?></button>
					<?php echo e(Form::close()); ?>

   
        </div>
 
 
 <div id="forgot" class="tab-pane"  >
            <?php echo Form::open(array('url'=>'merchant_forgot_check','class'=>'form-signin')); ?>

            <?php if(Session::has('forgot_error')): ?>
		<div class="alert alert-danger alert-dismissable" id="error_div" align="center" style="height:50px;width:298px;"><?php echo Session::get('forgot_error'); ?></div>
		<?php endif; ?>
        <?php if(Session::has('forgot_success')): ?>
		<div class="alert alert-success alert-dismissable" id="success_div" align="center" style="height:50px;width:298px;"><?php echo Session::get('forgot_success'); ?></div>
		<?php endif; ?>
<div class="alert alert-danger alert-dismissable" id="error_name" align="center" style="height:50px;width:298px;display:none;"></div>
<div class="alert alert-success alert-dismissable" id="success_name" align="center" style="height:50px;width:298px;display:none;"></div>
                <p class="text-muted text-center btn-block btn-primary btn-rect disabled"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ENTER_YOUR_VALID_E-MAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_YOUR_VALID_E-MAIL')   : trans($MER_OUR_LANGUAGE.'.MER_ENTER_YOUR_VALID_E-MAIL')); ?></p>
                <input type="email"  required="required" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YOUR_E-MAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YOUR_E-MAIL') : trans($MER_OUR_LANGUAGE.'.MER_YOUR_E-MAIL')); ?>" name="merchant_email"  id="merchant_email" class="form-control" />
                <br />
                <button class="btn text-muted text-center btn-success"   id="recover_password"  type="submit"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_RECOVER_PASSWORD')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_RECOVER_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_RECOVER_PASSWORD')); ?></button>
            </form>
        </div>
        <div id="signup" class="tab-pane">
            <form action="index.html" class="form-signin">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_FILL_DETAILS_TO_REGISTER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_FILL_DETAILS_TO_REGISTER')   : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_FILL_DETAILS_TO_REGISTER')); ?></p>
                 <input type="text" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FIRST_NAME'): trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME')); ?>" class="form-control" />
                 <input type="text" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_LAST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LAST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_LAST_NAME')); ?>" class="form-control" />
                <input type="text" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_USERNAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_USERNAME'): trans($MER_OUR_LANGUAGE.'.MER_USERNAME')); ?>" class="form-control" />
                <input type="email" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_YOUR_E-MAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_YOUR_E-MAIL'): trans($MER_OUR_LANGUAGE.'.MER_YOUR_E-MAIL')); ?>" class="form-control" />
                <input type="password" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PASSWORD'): trans($MER_OUR_LANGUAGE.'.MER_PASSWORD')); ?>>" class="form-control" />
                <input type="password" placeholder="<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_RE_TYPE_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_RE_TYPE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_RE_TYPE_PASSWORD')); ?>" class="form-control" />
                <button class="btn text-muted text-center btn-success" type="submit"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_REGISTER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_REGISTER'): trans($MER_OUR_LANGUAGE.'.MER_REGISTER')); ?></button>
            </form>
        </div>
    </div>
    <div class="text-center ">
        <ul class="list-inline">
            <!--<li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>-->
 		<li><a class="text-muted" href="#login" style="display:none;" id="login_click" data-toggle="tab"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_BACK_TO_LOGIN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK_TO_LOGIN'): trans($MER_OUR_LANGUAGE.'.MER_BACK_TO_LOGIN')); ?></a></li>
            <li><strong><a class="text-muted" id="forgot_click" href="#forgot" data-toggle="tab"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FORGOT_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_FORGOT_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_FORGOT_PASSWORD')); ?></a></strong></li>
           <!-- <li><a class="text-muted" href="#signup" data-toggle="tab">Signup</a></li>-->
        </ul>
    </div>


</div>

	  <!--END PAGE CONTENT -->     
	      
      <!-- PAGE LEVEL SCRIPTS -->
   <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>

  <script>
   $(document).ready(function(){
		



	   $('#forgot_click').click(function(){
		   $('#login_click').show();
		   $('#forgot_click').hide();
		  

		   $('#error_div').hide();
	  	 $('#success_div').hide();
		   });
	  $('#login_click').click(function(){
		   $('#forgot_click').show();
		   $('#login_click').hide();
		 

		   $('#error_div').hide();
	   		$('#success_div').hide();
     	  });
	   $('#error_div').fadeOut(3000);
	   $('#success_div').fadeOut(3000);
	    
	   
	  $('#recover_password').click(function(){
		  $('#recover_password').prop('disabled', true);
			var merchant_email = $('#merchant_email');
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if(merchant_email.val() == '')
			{
				$('#error_name').show();
				merchant_email.css({border:'1px solid red'});
				merchant_email.focus();
				$('#error_name').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ENTER_YOUR_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_YOUR_EMAIL'): trans($MER_OUR_LANGUAGE.'.MER_ENTER_YOUR_EMAIL')); ?>');
				 $('#error_name').fadeOut(3000);
				 $('#recover_password').prop('disabled', false);
				return false;
			}
			else if(!emailReg.test(merchant_email.val()))
			{
				$('#error_name').show();
				merchant_email.css({border:'1px solid red'});
				merchant_email.focus();
				$('#error_name').html('<?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ENTER_YOUR_VALID_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ENTER_YOUR_VALID_EMAIL') : trans($MER_OUR_LANGUAGE.'.MER_ENTER_YOUR_VALID_EMAIL')); ?>');
				$('#error_name').fadeOut(3000);
				$('#recover_password').prop('disabled', false);
				return false;
			}
			else
			{
				$('#error_name').hide();
				merchant_email.css({border:''});
				$.post("<?php echo url(''); ?>/merchant_forgot_check",
				{
					merchant_email: merchant_email.val()
				},
				function(data, status){
					var result=data.split(":");
					if(result[1]=="0")
					{
						$('#success_name').show();
						merchant_email.css({border:'1px solid red'});
						merchant_email.focus();
						$('#success_name').html(result[0]);
						$('#success_name').fadeOut(3000);
						$('#recover_password').prop('disabled', true);
						$('#recover_password').prop('disabled', false);
						return false;
					}
					else if(result[1]=="1")
					{
						$('#error_name').show();
						merchant_email.css({border:'1px solid red'});
						merchant_email.focus();
						$('#error_name').html(result[0]);
						$('#error_name').fadeOut(3000);
						$('#recover_password').prop('disabled', false);
						return false;
					}
					//alert("Data: " + data + "\nStatus: " + status);
					//alert("Data: " + result[0] + "\nStatus: " + result[1]);
				});
				return false;
			}
		  
		  });
	   });
   
   </script>

  
   <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.js"></script>
   <script src="<?php echo e(url('')); ?>/public/assets/js/login.js"></script>
      <!--END PAGE LEVEL SCRIPTS -->
	<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
    <!-- END BODY -->
</html>
