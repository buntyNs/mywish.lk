<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?>  | <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPPING_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPPING_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPPING_DELIVERY')); ?> <?php endif; ?>  </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
     <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>		
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/success.css" />
     <link href="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         <?php echo $adminheader; ?>

        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
         <?php echo $adminleftmenus; ?>

        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a ><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_HOME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME')); ?></a></li>
                                <li class="active"><a ><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPPING_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPPING_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPPING_DELIVERY')); ?> <?php endif; ?></a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPPING_DELIVERY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPPING_DELIVERY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPPING_DELIVERY')); ?> <?php endif; ?></h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">

   <div class="table-responsive panel_marg_clr ppd">
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
                                        <th aria-sort="ascending" aria-label="S.No: activate to sort column ascending" style="width: 99px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?></th>
                                        <th aria-label="Customers: activate to sort column ascending" style="width: 111px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_NAME')); ?> <?php endif; ?></th>
                                        <th aria-label="Product Title: activate to sort column ascending" style="width: 219px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?></th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID')); ?></th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DATE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE')); ?> <?php endif; ?></th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?> <?php endif; ?></th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DETAILS')); ?> <?php endif; ?></th>
                                        <!--<th aria-label="Transaction Date: activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php //if (Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_STATUS');} ?></th>
                                        </tr>-->
                                    </thead>
                                    <tbody>
                                   
										<?php $i=1; ?>
										<?php $__currentLoopData = $shippingdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										  
										    
										   	<?php if($shipping->order_status==1): ?>
											<?php
												$orderstatus="success";
											?>
											<?php elseif($shipping->order_status==2): ?> 
											<?php
												$orderstatus="completed";
											?>
											<?php elseif($shipping->order_status==3): ?> 
											<?php
												$orderstatus="Hold";
											?>
											<?php elseif($shipping->order_status==4): ?> 
											<?php
												$orderstatus="failed";
											?>

											<?php endif; ?>
										
										
										
										
										<tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo e($i); ?></td>
											<td class="center     ">
                                            <ul>
											<?php
												$product_data = DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.transaction_id', '=',$shipping->transaction_id)->where('nm_order.order_type', '=',1)->get(); ?>

												<?php $__currentLoopData = $product_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_datavalues): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php	echo '<li style="list-style:upper-roman; text-align:left;">'. $product_datavalues->pro_title.'.</li>';
												?>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</td>
											</ul>
                                            <td class=" "><?php echo e($shipping->cus_name); ?> </td>
                                            <td class=" "><?php echo e($shipping->cus_email); ?> </td>
                                            <td class="center"><?php echo e($shipping->order_date); ?></td>
                                            <td class="center"><?php echo e($shipping->ship_address1); ?></td>
                                     
                                      
                            <td class="center"><a class="btn btn-success" data-target="<?php echo '#uiModal'.$i;?>" data-toggle="modal"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_VIEW_DETAILS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_VIEW_DETAILS')); ?> <?php endif; ?></a></td>
                                              <!--<td class="center"><?php //echo $orderstatus;?></td>-->
                                            
                                          
                                           
                                        </tr>
                                        
                                        <?php $i=$i+1;  ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</tbody>
                                </table></div>

                               
        </div>
           
         </form>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    <div class="modal fade in" id="formModal"  style="display:none;">
     <div class="modal-dialog">
                                    <div class="modal-content">
                                        
                                        <div class="modal-body">
                                           <?php echo e(Form::open(array('role' => 'form'))); ?>

                                        <div class="form-group">
                                            <label><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL_ID')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EMAIL_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL_ID')); ?></label>
                                            <input class="form-control">
                                            <p class="help-block"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_BLOCK_LEVEL_HELP')!= ''))? trans(Session::get('admin_lang_file').'.BACK_BLOCK_LEVEL_HELP') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOCK_LEVEL_HELP')); ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?> <?php endif; ?>&nbsp;:&nbsp;</label>
         
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_MESSAGE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_MESSAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MESSAGE')); ?></label>
                                          <textarea id="text4" class="form-control"></textarea>
                                        </div>
                                        
                                       
                                    <?php echo e(Form::close()); ?>

                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLOSE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE')); ?> <?php endif; ?></button>
                                            <button class="btn btn-success" type="button"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SEND')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SEND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEND')); ?></button>
                                        </div>
                                    </div>
                                </div>  </div>
     <!--END MAIN WRAPPER -->
<?php $i=1; $all_tax_amt=0; $all_shipping_amt=0; $wallet =0; ?>
<?php $__currentLoopData = $shippingdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php	
$item_total=0;  $coupon_amount=0;
$subtotal =0;
$wallet_amt=0;

    $tax_amt = (($shipping->order_amt * $shipping->order_tax)/100);
    $all_tax_amt+=   (($shipping->order_amt * $shipping->order_tax)/100);

    $shipping_amt = $shipping->order_shipping_amt;
    $all_shipping_amt+= $shipping->order_shipping_amt; ?>
   
    <?php if($shipping->coupon_amount !=0): ?>
        <?php $coupon_value = $shipping->coupon_amount; ?>
    <?php else: ?>
         <?php $coupon_value = 0; ?>
    <?php endif; ?>
    <?php
    /*$wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$shipping->transaction_id)->get();
   
    if(count($wallet)!=0){
        $wallet_amt = $wallet[0]->wallet_used;
    }else{
        $wallet_amt = 0;
    }
    */ ?>

    <?php

    $item_total = $shipping->order_amt;
    
    $grand_total = (($item_total+$tax_amt+$shipping_amt)-$wallet_amt)-$coupon_value; ?>
   
										  


<div  id="<?php echo 'uiModal'.$i?>" class="modal fade in invoice-top" style="display:none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">
                                                <div class="col-lg-4"><img src="<?php echo $SITE_LOGO; ?>" alt="<?php if(Lang::has(Session::get('admin_lang_file').'.LOGO')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.LOGO')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.LOGO')); ?> <?php endif; ?>"></div>
                                                 <div class="col-lg-4" style="padding-top: 10px; text-align: center;"><strong><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAX_INVOICE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAX_INVOICE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX_INVOICE')); ?> <?php endif; ?></strong></div>
                                            <div class="col-lg-4" style="text-align: right;"><a href=class="btn btn-default" style="color:#d9534f" data-dismiss="modal" CLASS="pull-right"><i class="icon-remove-sign icon-2x"></i></a></div>
                                        </div>
                                      <hr style="margin-top: 0px;">
                                      <div class="row">
                                      <div class="col-lg-12">
                                      <div class="col-lg-6">
									   <?php if($shipping->order_paytype == 1): ?> 
                                      <h4><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYPAL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYPAL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYPAL')); ?> <?php endif; ?></h4>
									  <?php else: ?>
									  <h4><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_WALLET')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_WALLET')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_WALLET')); ?> <?php endif; ?></h4>
									  <?php endif; ?>
									  
                                      <?php if($shipping->order_status == 1): ?>
										<b><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT_PAID')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT_PAID')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT_PAID')); ?> <?php endif; ?>
										 <?php else: ?>
                                      <b><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT_TO_PAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT_TO_PAY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT_TO_PAY')); ?> <?php endif; ?>
										<?php endif; ?> 
									  
									  
									  :<?php echo e(Helper::cur_sym()); ?> <?php
									  $product_titles=DB::table('nm_order')
									  ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
									  ->leftjoin('nm_color', 'nm_order.order_pro_color', '=', 'nm_color.co_id')
									  ->leftjoin('nm_size', 'nm_order.order_pro_size', '=', 'nm_size.si_id')
									  ->where('transaction_id', '=', $shipping->transaction_id)
									  ->where('nm_order.order_type', '=',1)
									  ->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
									  <?php $__currentLoopData = $product_titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prd_tit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
										  <?php	
										  $subtotal=$prd_tit->order_amt; 
																$tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
																$total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 
																$total_ship_amt+= $prd_tit->order_shipping_amt;
																$total_item_amt+=$prd_tit->order_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->order_pro_id;
																
														$item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
														
															 $ship_amt = $prd_tit->order_shipping_amt;
															
														
															 //$item_tax = $codorderdet->cod_tax;
															
														    $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
															
                                                            $wallet_amt +=  $prd_tit->wallet_amount;
                                                            $wallet     += $prd_tit->wallet_amount;
										?>
									  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  <?php echo e($grand_total-$wallet_amt); ?></b>
                                      <br>
                                      <span>(<?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_INCLUSIVE_OF_ALL_CHARGES')!= ''))? trans(Session::get('admin_lang_file').'.BACK_INCLUSIVE_OF_ALL_CHARGES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INCLUSIVE_OF_ALL_CHARGES')); ?>)</span>
                                     <br><br>
									 <span> <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_ID')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ORDER_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_ID')); ?>: </b> <?php echo $shipping->transaction_id;?></span>
                                     <br>
                                       
					 <span> <b> <?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_DATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ORDER_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_DATE')); ?>: </b> <?php echo $shipping->order_date;?></span>
                                      </div>
                                        <div class="col-lg-6" style="border-left:1px solid #eeeeee;">
										  
										  
                                          <h4><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SHIPPING_DETAILS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SHIPPING_DETAILS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPPING_DETAILS')); ?></h4>
                                          
										  <?php $explode = explode(',',$shipping->order_shipping_add);
                                          foreach($explode as $addr){
                                            //echo $addr.'<br>';
                                          }?>
										  <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= ''))? trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?> :</b> <?php echo e($shipping->ship_name); ?><br>
										  
										  <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= ''))? trans(Session::get('admin_lang_file').'.BACK_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL')); ?> :</b> <?php echo e($shipping->ship_email); ?><br>
										  
										  <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ADDRESS1') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1')); ?> :</b><?php echo e($shipping->ship_address1); ?>

                                          <br>
										  
										   <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ADDRESS2') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2')); ?> :</b><?php echo e($shipping->ship_address2); ?>

                                          <br> 
										  
                                          <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_CITY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY')); ?> :</b> <?php echo e($shipping->ship_ci_id); ?><br>
										  
										  
										  <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_STATE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_STATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STATE')); ?>   :</b> <?php echo e($shipping->ship_state); ?><br>
										  
										   <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= ''))? trans(Session::get('admin_lang_file').'.BACK_COUNTRY') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY')); ?> :</b> <?php echo e($shipping->ship_country); ?><br>
										   
										   <b><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_ZIPCODE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_ZIPCODE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ZIPCODE')); ?> :</b> <?php echo e($shipping->ship_postalcode); ?><br>
										   
                                         <b> <?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= ''))? trans(Session::get('admin_lang_file').'.BACK_PHONE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE')); ?>: </b><?php echo e($shipping->ship_phone); ?>

                                          </div>
                                            
                                             
                                      </div>
                                      </div>
                                      <hr>
                                      <div class="row">
                                      <div class="span12 text-center">
                                      <h4 class="text-center"><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_INVOICE_DETAILS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_INVOICE_DETAILS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INVOICE_DETAILS')); ?></h4>
                                      <span><?php echo e(((Lang::has(Session::get('admin_lang_file').'.BACK_SHIPMENT_CONTAINS_ITEMS')!= ''))? trans(Session::get('admin_lang_file').'.BACK_SHIPMENT_CONTAINS_ITEMS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPMENT_CONTAINS_ITEMS')); ?></span>
                                      </div>
                                      </div>
                                  <div class="table-responsive"> 
                                  <table class="inv-table" style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">
                                   <tr style="border-bottom:1px solid #e6e6e6; background:#f5f5f5;">
									<th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PRODUCT_NAME')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_NAME')); ?> <?php endif; ?></th>
                                    <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COLOR')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COLOR')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COLOR')); ?> <?php endif; ?></th>
                                      <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SIZE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SIZE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SIZE')); ?> <?php endif; ?></th>
                                        <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_QUANTITY')); ?>  <?php else: ?> {@ trans($ADMIN_OUR_LANGUAGE.'.BACK_QUANTITY')}} <?php endif; ?></th>
                                         <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ORIGINAL_PRICE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ORIGINAL_PRICE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ORIGINAL_PRICE')); ?> <?php endif; ?></th>
                                            <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_TOTAL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SUB_TOTAL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_TOTAL')); ?> <?php endif; ?></th>
                                            <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_COUPON_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_COUPON_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_COUPON_AMOUNT')); ?> <?php endif; ?></th>
											 <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_PAYMENT_STATUS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_STATUS')); ?> <?php endif; ?></th>
															
                                             <th  width="" align="center"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS')); ?> <?php endif; ?></th> 
                                         <?php //$subtotal=$shipping->order_qty*$shipping->order_amt;  ?>   
                                    </tr>
                                    <?php
									  $product_titles=DB::table('nm_order')
									  ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
									  ->leftjoin('nm_color', 'nm_order.order_pro_color', '=', 'nm_color.co_id')
									  ->leftjoin('nm_size', 'nm_order.order_pro_size', '=', 'nm_size.si_id')
									  ->where('transaction_id', '=', $shipping->transaction_id)
									  ->where('nm_order.order_type', '=',1)
									  ->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
									  <?php $__currentLoopData = $product_titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prd_tit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
										  	
														<?php		$status=$prd_tit->delivery_status; ?>
											<?php if($prd_tit->delivery_status==1): ?>
											<?php
											$orderstatus="Order Placed";
											?>
											<?php elseif($prd_tit->delivery_status==2): ?> 
											<?php
											$orderstatus="Order Packed";
											?>
											<?php elseif($prd_tit->delivery_status==3): ?> 
											<?php
											$orderstatus="Dispatched";
											?>
											<?php elseif($prd_tit->delivery_status==4): ?> 
											<?php
											$orderstatus="Delivered";
											?>
											<?php elseif($prd_tit->delivery_status==5): ?>
											  <?php
												$orderstatus="cancel pending";
											  ?>
											  <?php elseif($prd_tit->delivery_status==6): ?> 
											  <?php
											  $orderstatus="cancelled";
											  ?>
											  <?php elseif($prd_tit->delivery_status==7): ?> 
											  <?php
											  $orderstatus="return pending";
											  ?>
											  <?php elseif($prd_tit->delivery_status==8): ?> 
											  <?php
											  $orderstatus="Returned";
											  ?>
											  <?php elseif($prd_tit->delivery_status==9): ?> 
											  <?php
											  $orderstatus="Replace Pending";
											  ?>
											  <?php elseif($prd_tit->delivery_status==10): ?> 
											 <?php
											  $orderstatus="Replaced";
											  ?>
											  <?php else: ?>
											  <?php $orderstatus = '';  ?>
											  <?php endif; ?>
                                                                
											
											
											<?php if($prd_tit->order_status==1): ?>
											<?php
											$payment_status="Success";
											?>
											<?php elseif($prd_tit->order_status==2): ?> 
											<?php
											$payment_status="Order Packed";
											?>
											<?php elseif($prd_tit->order_status==3): ?> 
											<?php
											$payment_status="Hold";
											?>
											<?php elseif($prd_tit->order_status==4): ?> 
											<?php
											$payment_status="Faild";
											?>
											<?php endif; ?>
											<?php
										  $subtotal=$prd_tit->order_amt; 
																$tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
																$total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 
																$total_ship_amt+= $prd_tit->order_shipping_amt;
																$total_item_amt+=$prd_tit->order_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->order_pro_id;
																
														$item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
														
															 $ship_amt = $prd_tit->order_shipping_amt;
															
														
															 //$item_tax = $codorderdet->cod_tax;
															
														$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
											?>					
									 
                                    <tr>	
									<td  width="" align="center"><?php echo e($prd_tit->pro_title); ?></td>&nbsp;
                                        <td  width="" align="center"><?php echo e($prd_tit->co_name); ?></td>&nbsp;
                                        <td  width="" align="center"><?php echo e($prd_tit->si_name); ?></td>
                                        <td  width="" align="center"><?php echo e($prd_tit->order_qty); ?> </td>
                                        <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?> <?php echo e($prd_tit->order_prod_unitPrice); ?> <?php //echo $prd_tit->pro_disprice; ?> </td>
                                        <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?><?php echo $subtotal - $prd_tit->coupon_amount;?> </td>
										<?php if($prd_tit->coupon_amount != 0): ?> 	
															<td  width="" align="center"><?php echo e(Helper::cur_sym()); ?> <?php echo $prd_tit->coupon_amount ;?> </td>
															 <?php else: ?> <td  width="" align="center"><?php echo e(Helper::cur_sym()); ?> <?php echo $prd_tit->coupon_amount ;?> </td><?php endif; ?>
															<td  width="" align="center"><?php echo e($payment_status); ?></td>
											<td  width="" align="center"><?php echo e($orderstatus); ?></td>											</td>
                                    </tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </table></div>
                                    <br>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12">
                                    <div class="col-lg-6"></div>
                                     <div class="col-lg-6">
                               <div><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SHIPMENT_VALUE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SHIPMENT_VALUE')); ?> <?php endif; ?><p class="pull-right" style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($total_ship_amt); ?> </p></div>
                                        <div class="clear"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TAX')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TAX')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TAX')); ?> <?php endif; ?><p class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($total_tax_amt); ?></p></div>
                                    
                                     <?php if(count($wallet)!=0): ?>

                                        <div class="clear">wallet :<p class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> -<?php echo $wallet_amt;?></p></div>
                                     <?php endif; ?>

                                     <?php  /* if($shipping->coupon_amount !=0){ ?>
                                         <span>coupon Code : <p class="pull-right"style="margin-right:15px;"> <?php echo $coupon_amount;?></p></span><br>
								<?php   } */?>
                                     
                                        <hr>
<?php //$totamt=round($subtotal + ($subtotal*($shipping->pro_inctax/100)));?>

                                      <strong> <div class="clear"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AMOUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?><p class="pull-right"style="margin-right:15px;"><?php echo e(Helper::cur_sym()); ?> <?php echo e($grand_total-$wallet_amt); ?></p></div></strong>
                                     
                                     
                                     </div></div>
                                    </div>
                        
                                        
                                        <div class="modal-footer" style="background: #f5f5f5;">
                                            <button data-dismiss="modal" class="btn btn-danger" type="button"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CLOSE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CLOSE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CLOSE')); ?> <?php endif; ?></button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

   <?php $i=$i+1; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
    <!-- FOOTER -->
    <div id="footer">
        <?php echo $adminfooter; ?>

    </div>
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
       <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
		  <script type="text/javascript">
	   $.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	   });
	</script>
</body>
     <!-- END BODY -->
</html>
