<?php echo $navbar; ?>



<!-- Navbar ================================================== -->
<?php echo $header; ?>

<!-- Header End====================================================================== -->
<div class="breadcrumbs">
    <div class="container">	
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo e(url('index')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '') ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a><span>&raquo;</span></li>
           
          </ul>
        </div>
      </div>
    </div>
  </div>
<div id="mainBody faq_main">
<div class="container cms-page">
<?php if($cms_result): ?> 
 <?php $__currentLoopData = $cms_result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
	<?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?> 
		<?php $cp_title = 'cp_title'; 
		$cp_description = 'cp_description'; ?>
	 <?php else: ?>  
		<?php $cp_title = 'cp_title_'.Session::get('lang_code'); 
		$cp_description = 'cp_description_'.Session::get('lang_code');  ?>
	<?php endif; ?>	
   <?php $cms_desc = $cms->$cp_description;   ?>

   <br>
   <div class="page-title">
          <h2><?php echo  $cms->$cp_title; ?></h2>
        </div>
  
  <div id="legalNotice">
	
  <?php echo $cms_desc; ?>

  </div>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?> 
 <h1 style="color:#ff8400;"></h1>
 <legend></legend>
 <div id="legalNotice">
	 <?php if(Lang::has(Session::get('lang_file').'.NO_DATA_FOUND')!= ''): ?> 
	  <?php echo e(trans(Session::get('lang_file').'.CATEGORIES')); ?>  
	  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_DATA_FOUND')); ?> !
	 <?php endif; ?>
	</div>	
 </div>
<?php endif; ?> 
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->

	<?php echo $footer; ?>


	

</body>
</html>